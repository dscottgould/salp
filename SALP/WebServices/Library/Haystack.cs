﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Runtime.Serialization.Json;
using System.Text;

using SALP.WebServices.DataTypes;


namespace SALP.WebServices.Library
{
    public static class Haystack
    {
        public static CookieCollection GetHaystackCookie(string username, string password)
        {
            if (username == null || username.Length == 0)
            {
                username = "XNOCOBI";
            }

            if (password == null || password.Length == 0)
            {
                password = "XNOCOBI";
            }

            String url = "http://haystack.cable.comcast.com/api/login?username=" + username + "&password=" + password + "&submit=Login";

            CookieContainer container = new CookieContainer();
            CookieCollection cookies = new CookieCollection();

            HttpWebRequest request = WebRequest.Create(url) as HttpWebRequest;
            request.Method = "POST";
            request.CookieContainer = container;

            //String JSessionID = null;

            try
            {
                using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
                {
                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        cookies = response.Cookies;

                        //JSessionID = cookies[0].ToString().Split('=')[1];

                        Stream receiveStream = response.GetResponseStream();
                        StreamReader readStream = new StreamReader(receiveStream, Encoding.UTF8);

                        String test = readStream.ReadToEnd();

                        readStream.Close();
                    }
                }
            }
            catch (System.Exception error)
            {
                ErrorMessage errorMessage = new ErrorMessage();
                errorMessage.status = error.Source;
                errorMessage.message = error.Message;
            }

            return cookies;
        }




        public static ReturnData<List<HaystackItem>> GetHaystackCalls(CookieCollection cookies, string username, string password, string segment, string filter, string fromDate, string toDate)
        {
            ReturnData<List<HaystackItem>> returnData = new ReturnData<List<HaystackItem>>();

            try
            {
                String jSessionID = cookies[0].ToString().Split('=')[1];
                DateTime fromTime;
                DateTime toTime;
                String binSize = "60";
                Double days;

                String url = "http://haystack.cable.comcast.com/api/bins/calls?";
                //url += "fromDate=2016-08-04T00:00:00.000Z&toDate=2016-08-05T00:00:00.000Z&";
                //url += "JSESSIONID=" + jSessionID + "&binSize=HOUR&baselineType=FiveWeekRelative";

                if ((fromDate == null) || fromDate.Trim().Length == 0)
                {
                    //  Get Data 24hours From Now
                    toTime = DateTime.Now.ToUniversalTime();
                    fromTime = toTime.AddDays(-1);
                }
                else
                {
                    fromTime = DateTime.Parse(fromDate).ToUniversalTime();
                    toTime = DateTime.Parse(toDate).ToUniversalTime();

                    days = (toTime - fromTime).TotalDays;

                    if (days >= 7)
                    {
                        binSize = "1440";
                    }

                    if(days >= 30)
                    {
                        binSize = "10080";
                    }
                }


                var startingTime = fromTime.ToString("yyyy-MM-dd") + "T" + fromTime.ToString("HH") + ":00:00.000Z";
                var endingTime = toTime.ToString("yyyy-MM-dd") + "T" + toTime.ToString("HH") + ":59:59.999Z";

                url += "fromDate=" + startingTime + "&toDate=" + endingTime;
                url += "&JSESSIONID=" + jSessionID + "&binSize=" + binSize + "&baselineCycles=5";

                // Add Segment
                if (segment != null && segment.Length > 0)
                {
                    url += "&wildcard=" + segment;
                }

                // Get User Filters
                if (filter != null && filter.Length > 0)
                {
                    FilterList userFilterList = Haystack.GetUserFilters(cookies, username, password, filter);

                    if(userFilterList.FilterElement.Count > 0)
                    {
                        foreach (var filterElement in userFilterList.FilterElement)
                        {

                            url += "&" + filterElement.type + '=';

                            if (filterElement.not == true)
                            {
                                url += "Does Not Equal$";
                            }

                            for (int index = 0; index < filterElement.value.Count; index ++)
                            {
                                url += filterElement.value[index];

                                if (filterElement.value.Count - 1 > index)
                                {
                                    url += '$';
                                }
                            }
                        }
                    }
                }

                HttpWebRequest request = WebRequest.Create(url) as HttpWebRequest;

                // Add Cookies
                var container = new CookieContainer();
                container.Add(cookies);
                request.CookieContainer = container;


                using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
                {
                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        //Stream receiveStream = response.GetResponseStream();
                        //StreamReader readStream = new StreamReader(receiveStream, Encoding.UTF8);

                        //String test = readStream.ReadToEnd();

                        DataContractJsonSerializer jsonSerializer = new DataContractJsonSerializer(typeof(HaystackData));
                        HaystackData responseData = jsonSerializer.ReadObject(response.GetResponseStream()) as HaystackData;

                        returnData.data = new List<HaystackItem>();

                        foreach (var record in responseData.bins)
                        {
                            HaystackItem item = new HaystackItem();

                            int[] arraySorted = new int[5];

                            arraySorted[0] = int.Parse(record.eventCountBaselines[0]);
                            arraySorted[1] = int.Parse(record.eventCountBaselines[1]);
                            arraySorted[2] = int.Parse(record.eventCountBaselines[2]);
                            arraySorted[3] = int.Parse(record.eventCountBaselines[3]);
                            arraySorted[4] = int.Parse(record.eventCountBaselines[4]);

                            Array.Sort(arraySorted);

                            item.baseline = arraySorted[2].ToString();
                            item.baseline1 = arraySorted[0].ToString();
                            item.baseline2 = arraySorted[1].ToString();
                            item.baseline3 = arraySorted[2].ToString();
                            item.baseline4 = arraySorted[3].ToString();
                            item.baseline5 = arraySorted[4].ToString();
                            item.count = record.eventCount;
                            item.startDate = record.startDate;
                            item.endDate = record.endDate;

                            returnData.data.Add(item);
                        }

                        returnData.success = true;
                    }
                }
            }
            catch (System.Exception error)
            {
                ErrorMessage errorMessage = new ErrorMessage();
                errorMessage.status = error.Source;
                errorMessage.message = error.Message;

                returnData.errors.Add(errorMessage);

                returnData.success = false;
            }

            return returnData;
        }

        public static ReturnData<List<HaystackItem>> GetHaystackTickets(CookieCollection cookies, string username, string password, string segment, string filter, string fromDate, string toDate)
        {
            String jSessionID = cookies[0].ToString().Split('=')[1];
            ReturnData<List<HaystackItem>> returnData = new ReturnData<List<HaystackItem>>();
            DateTime fromTime;
            DateTime toTime;
            String binSize = "60";
            Double days;

            String url = "http://haystack.cable.comcast.com/api/bins/tickets?";
            //url += "fromDate=2016-08-04T00:00:00.000Z&toDate=2016-08-05T00:00:00.000Z&";
            //url += "JSESSIONID=" + jSessionID + "&binSize=HOUR&baselineType=FiveWeekRelative";

            if ((fromDate == null) || fromDate.Trim().Length == 0)
            {
                //  Get Data 24hours From Now
                toTime = DateTime.Now.ToUniversalTime();
                fromTime = toTime.AddDays(-1);
            }
            else
            {
                fromTime = DateTime.Parse(fromDate).ToUniversalTime();
                toTime = DateTime.Parse(toDate).ToUniversalTime();

                days = (toTime - fromTime).TotalDays;

                if (days >= 7)
                {
                    binSize = "1440";
                }

                if (days >= 30)
                {
                    binSize = "10080";
                }
            }


            var startingTime = fromTime.ToString("yyyy-MM-dd") + "T" + fromTime.ToString("HH") + ":00:00.000Z";
            var endingTime = toTime.ToString("yyyy-MM-dd") + "T" + toTime.ToString("HH") + ":59:59.999Z";

            url += "fromDate=" + startingTime + "&toDate=" + endingTime;
            url += "&JSESSIONID=" + jSessionID + "&binSize=" + binSize + "&baselineCycles=5";

            // Add Segment
            if (segment != null && segment.Length > 0)
            {
                url += "&wildcard=" + segment;
            }

            // Get User Filters
            if (filter != null && filter.Length > 0)
            {
                FilterList userFilterList = Haystack.GetUserFilters(cookies, username, password, filter);

                if (userFilterList.FilterElement.Count > 0)
                {
                    foreach (var filterElement in userFilterList.FilterElement)
                    {

                        url += "&" + filterElement.type + '=';

                        if (filterElement.not == true)
                        {
                            url += "Does Not Equal$";
                        }

                        for (int index = 0; index < filterElement.value.Count; index++)
                        {
                            url += filterElement.value[index];

                            if (filterElement.value.Count - 1 > index)
                            {
                                url += '$';
                            }
                        }
                    }
                }
            }


            HttpWebRequest request = WebRequest.Create(url) as HttpWebRequest;

            // Add Cookies
            var container = new CookieContainer();
            container.Add(cookies);
            request.CookieContainer = container;

            try
            {
                using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
                {
                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        //Stream receiveStream = response.GetResponseStream();
                        //StreamReader readStream = new StreamReader(receiveStream, Encoding.UTF8);

                        //String test = readStream.ReadToEnd();

                        DataContractJsonSerializer jsonSerializer = new DataContractJsonSerializer(typeof(HaystackData));
                        HaystackData responseData = jsonSerializer.ReadObject(response.GetResponseStream()) as HaystackData;

                        returnData.data = new List<HaystackItem>();

                        foreach (var record in responseData.bins)
                        {
                            HaystackItem item = new HaystackItem();

                            Array.Sort(record.eventCountBaselines);

                            item.baseline = record.eventCountBaselines[2];
                            item.baseline1 = record.eventCountBaselines[0];
                            item.baseline2 = record.eventCountBaselines[1];
                            item.baseline3 = record.eventCountBaselines[2];
                            item.baseline4 = record.eventCountBaselines[3];
                            item.baseline5 = record.eventCountBaselines[4];
                            item.count = record.eventCount;
                            item.startDate = record.startDate;
                            item.endDate = record.endDate;

                            returnData.data.Add(item);
                        }

                        returnData.success = true;
                    }
                }
            }
            catch (System.Exception error)
            {
                ErrorMessage errorMessage = new ErrorMessage();
                errorMessage.status = error.Source;
                errorMessage.message = error.Message;

                returnData.errors.Add(errorMessage);

                returnData.success = false;
            }

            return returnData;
        }

        public static ReturnData<List<HaystackItem>> GetHaystackTruckRolls(CookieCollection cookies, string username, string password, string segment, string filter, string fromDate, string toDate)
        {
            String jSessionID = cookies[0].ToString().Split('=')[1];
            ReturnData<List<HaystackItem>> returnData = new ReturnData<List<HaystackItem>>();
            DateTime fromTime;
            DateTime toTime;
            String binSize = "60";
            Double days;

            String url = "http://haystack.cable.comcast.com/api/bins/truckrolls?";
            //url += "fromDate=2016-08-04T00:00:00.000Z&toDate=2016-08-05T00:00:00.000Z&";
            //url += "JSESSIONID=" + jSessionID + "&binSize=HOUR&baselineType=FiveWeekRelative";

            if ((fromDate == null) || fromDate.Trim().Length == 0)
            {
                //  Get Data 24hours From Now
                toTime = DateTime.Now.ToUniversalTime();
                fromTime = toTime.AddDays(-1);
            }
            else
            {
                fromTime = DateTime.Parse(fromDate).ToUniversalTime();
                toTime = DateTime.Parse(toDate).ToUniversalTime();

                days = (toTime - fromTime).TotalDays;

                if (days >= 7)
                {
                    binSize = "1440";
                }

                if (days >= 30)
                {
                    binSize = "10080";
                }
            }


            var startingTime = fromTime.ToString("yyyy-MM-dd") + "T" + fromTime.ToString("HH") + ":00:00.000Z";
            var endingTime = toTime.ToString("yyyy-MM-dd") + "T" + toTime.ToString("HH") + ":59:59.999Z";

            url += "fromDate=" + startingTime + "&toDate=" + endingTime;
            url += "&JSESSIONID=" + jSessionID + "&binSize=" + binSize + "&baselineCycles=5";

            // Add Segment
            if (segment != null && segment.Length > 0)
            {
                url += "&wildcard=" + segment;
            }

            // Get User Filters
            if (filter != null && filter.Length > 0)
            {
                FilterList userFilterList = Haystack.GetUserFilters(cookies, username, password, filter);

                if (userFilterList.FilterElement.Count > 0)
                {
                    foreach (var filterElement in userFilterList.FilterElement)
                    {

                        url += "&" + filterElement.type + '=';

                        if (filterElement.not == true)
                        {
                            url += "Does Not Equal$";
                        }

                        for (int index = 0; index < filterElement.value.Count; index++)
                        {
                            url += filterElement.value[index];

                            if (filterElement.value.Count - 1 > index)
                            {
                                url += '$';
                            }
                        }
                    }
                }
            }


            HttpWebRequest request = WebRequest.Create(url) as HttpWebRequest;

            // Add Cookies
            var container = new CookieContainer();
            container.Add(cookies);
            request.CookieContainer = container;

            try
            {
                using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
                {
                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        //Stream receiveStream = response.GetResponseStream();
                        //StreamReader readStream = new StreamReader(receiveStream, Encoding.UTF8);

                        //String test = readStream.ReadToEnd();

                        DataContractJsonSerializer jsonSerializer = new DataContractJsonSerializer(typeof(HaystackData));
                        HaystackData responseData = jsonSerializer.ReadObject(response.GetResponseStream()) as HaystackData;

                        returnData.data = new List<HaystackItem>();

                        foreach (var record in responseData.bins)
                        {
                            HaystackItem item = new HaystackItem();

                            Array.Sort(record.eventCountBaselines);

                            item.baseline = record.eventCountBaselines[2];
                            item.baseline1 = record.eventCountBaselines[0];
                            item.baseline2 = record.eventCountBaselines[1];
                            item.baseline3 = record.eventCountBaselines[2];
                            item.baseline4 = record.eventCountBaselines[3];
                            item.baseline5 = record.eventCountBaselines[4];
                            item.count = record.eventCount;
                            item.startDate = record.startDate;
                            item.endDate = record.endDate;

                            returnData.data.Add(item);
                        }

                        returnData.success = true;
                    }
                }
            }
            catch (System.Exception error)
            {
                ErrorMessage errorMessage = new ErrorMessage();
                errorMessage.status = error.Source;
                errorMessage.message = error.Message;

                returnData.errors.Add(errorMessage);

                returnData.success = false;
            }

            return returnData;
        }

        public static FilterList GetUserFilters(CookieCollection cookies, string username, string password, string filter)
        {
            String jSessionID = cookies[0].ToString().Split('=')[1];
            FilterList userFilterList = new FilterList();

            //http://haystack.cable.comcast.com/api/savedfilters/Test4?cacheBuster=1471006299445&username=ewagne200

            String url = "http://haystack.cable.comcast.com/api/savedfilters/" + filter + "?username=" + username;

            HttpWebRequest request = WebRequest.Create(url) as HttpWebRequest;

            // Add Cookies
            var container = new CookieContainer();
            container.Add(cookies);
            request.CookieContainer = container;

            try
            {
                using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
                {
                    if (response.StatusCode == HttpStatusCode.OK)
                    {

                        //Stream receiveStream = response.GetResponseStream();
                        //StreamReader readStream = new StreamReader(receiveStream, Encoding.UTF8);
                        //String test = readStream.ReadToEnd();

                        DataContractJsonSerializer jsonSerializer = new DataContractJsonSerializer(typeof(FilterList));
                        userFilterList = jsonSerializer.ReadObject(response.GetResponseStream()) as FilterList;
                    }
                }
            }
            catch (System.Exception error)
            {
                ErrorMessage errorMessage = new ErrorMessage();
                errorMessage.status = error.Source;
                errorMessage.message = error.Message;
            }

            return userFilterList;
        }

        public static ReturnData<List<UserFilter>> GetUserFilterList(CookieCollection cookies, string username)
        {
            ReturnData<List<UserFilter>> returnData = new ReturnData<List<UserFilter>>();

            try
            {
                String jSessionID = cookies[0].ToString().Split('=')[1];
                String url = "http://haystack.cable.comcast.com/api/savedfilters?cacheBuster=1471005600263&username=" + username;

                HttpWebRequest request = WebRequest.Create(url) as HttpWebRequest;

                // Add Cookies
                var container = new CookieContainer();
                container.Add(cookies);
                request.CookieContainer = container;


            
                using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
                {
                    if (response.StatusCode == HttpStatusCode.OK)
                    {

                        //Stream receiveStream = response.GetResponseStream();
                        //StreamReader readStream = new StreamReader(receiveStream, Encoding.UTF8);
                        //String test = readStream.ReadToEnd();

                        DataContractJsonSerializer jsonSerializer = new DataContractJsonSerializer(typeof(HaystackFilters));
                        HaystackFilters responseData = jsonSerializer.ReadObject(response.GetResponseStream()) as HaystackFilters;

                        returnData.data = new List<UserFilter>();

                        foreach (var item in responseData.UserFilter)
                        {
                            UserFilter record = new UserFilter();

                            record.userFilter = item;

                            returnData.data.Add(record);
                        }

                        returnData.success = true;
                    }
                }
            }
            catch (System.Exception error)
            {
                ErrorMessage errorMessage = new ErrorMessage();
                errorMessage.status = error.Source;
                errorMessage.message = error.Message;

                returnData.errors.Add(errorMessage);

                returnData.success = false;
            }

            return returnData;
        }


        public static ReturnData<List<HaystackSegmentRecord>> GetHaystackSegments(CookieCollection cookies)
        {
            ReturnData<List<HaystackSegmentRecord>> returnData = new ReturnData<List<HaystackSegmentRecord>>();

            try
            {
                String jSessionID = cookies[0].ToString().Split('=')[1];

                String url = "http://haystack.cable.comcast.com/api/filters/wildcard";
                //url += "fromDate=2016-08-04T00:00:00.000Z&toDate=2016-08-05T00:00:00.000Z&";
                //url += "JSESSIONID=" + jSessionID + "&binSize=HOUR&baselineType=FiveWeekRelative";

                HttpWebRequest request = WebRequest.Create(url) as HttpWebRequest;

                // Add Cookies
                var container = new CookieContainer();
                container.Add(cookies);
                request.CookieContainer = container;

                using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
                {
                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        //Stream receiveStream = response.GetResponseStream();
                        //StreamReader readStream = new StreamReader(receiveStream, Encoding.UTF8);
                        //String test = readStream.ReadToEnd();

                        DataContractJsonSerializer jsonSerializer = new DataContractJsonSerializer(typeof(HaystackSegment));
                        HaystackSegment responseData = jsonSerializer.ReadObject(response.GetResponseStream()) as HaystackSegment;

                        returnData.data = new List<HaystackSegmentRecord>();

                        foreach (var item in responseData.value)
                        {
                            HaystackSegmentRecord record = new HaystackSegmentRecord();

                            record.name = item;
                            record.value = item;

                            returnData.data.Add(record);
                        }

                        returnData.success = true;
                    }
                }
            }
            catch (System.Exception error)
            {
                ErrorMessage errorMessage = new ErrorMessage();
                errorMessage.status = error.Source;
                errorMessage.message = error.Message;

                returnData.errors.Add(errorMessage);

                returnData.success = false;
            }

            return returnData;
        }


        public static ReturnData<List<HaystackDivision>> GetDivisionsList(CookieCollection cookies, string username)
        {
            ReturnData<List<HaystackDivision>> returnData = new ReturnData<List<HaystackDivision>>();

            try
            {
                String jSessionID = cookies[0].ToString().Split('=')[1];
                String url = "http://haystack.cable.comcast.com/api/filters/level1?username=" + username;

                HttpWebRequest request = WebRequest.Create(url) as HttpWebRequest;

                // Add Cookies
                var container = new CookieContainer();
                container.Add(cookies);
                request.CookieContainer = container;



                using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
                {
                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        //Stream receiveStream = response.GetResponseStream();
                        //StreamReader readStream = new StreamReader(receiveStream, Encoding.UTF8);
                        //String test = readStream.ReadToEnd();

                        DataContractJsonSerializer jsonSerializer = new DataContractJsonSerializer(typeof(HaystackSegment));
                        HaystackSegment responseData = jsonSerializer.ReadObject(response.GetResponseStream()) as HaystackSegment;

                        returnData.data = new List<HaystackDivision>();

                        foreach (var item in responseData.value)
                        {
                            HaystackDivision haystackDivision = new HaystackDivision();

                            haystackDivision.division = item;

                            returnData.data.Add(haystackDivision);
                        }

                        returnData.success = true;
                    }
                }
            }
            catch (System.Exception error)
            {
                ErrorMessage errorMessage = new ErrorMessage();
                errorMessage.status = error.Source;
                errorMessage.message = error.Message;

                returnData.errors.Add(errorMessage);

                returnData.success = false;
            }

            return returnData;
        }

        public static ReturnData<List<HaystackNode>> GetHaystackNodes(CookieCollection cookies, string username)
        {
            ReturnData<List<HaystackNode>> returnData = new ReturnData<List<HaystackNode>>();

            try
            {
                String jSessionID = cookies[0].ToString().Split('=')[1];
                String url = "http://haystack.cable.comcast.com/api/filters/level10?username=" + username;

                HttpWebRequest request = WebRequest.Create(url) as HttpWebRequest;

                // Add Cookies
                var container = new CookieContainer();
                container.Add(cookies);
                request.CookieContainer = container;



                using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
                {
                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        //Stream receiveStream = response.GetResponseStream();
                        //StreamReader readStream = new StreamReader(receiveStream, Encoding.UTF8);
                        //String test = readStream.ReadToEnd();

                        DataContractJsonSerializer jsonSerializer = new DataContractJsonSerializer(typeof(HaystackSegment));
                        HaystackSegment responseData = jsonSerializer.ReadObject(response.GetResponseStream()) as HaystackSegment;

                        returnData.data = new List<HaystackNode>();

                        foreach (var value in responseData.value)
                        {
                            HaystackNode node = new HaystackNode();

                            returnData.data.Add(node);
                        }

                        returnData.success = true;
                    }
                }
            }
            catch (System.Exception error)
            {
                ErrorMessage errorMessage = new ErrorMessage();
                errorMessage.status = error.Source;
                errorMessage.message = error.Message;

                returnData.errors.Add(errorMessage);

                returnData.success = false;
            }

            return returnData;
        }

        public static ReturnData<String> SaveHaystackFilter(CookieCollection cookies, String accountName, String filterName, String division, String nodes)
        {
            ReturnData<String> returnData = new ReturnData<String>();

            String divisionEncoded = WebUtility.HtmlEncode(division).Replace(" ", "").Replace(",", "%24");
            String nodesEncoded = WebUtility.HtmlEncode(nodes).Replace(" ", "").Replace(",", "%24");


            try
            {
                String jSessionID = cookies[0].ToString().Split('=')[1];
                String url = "http://haystack/api/savedfilters/" + filterName + "/create?username=" + accountName + "&level1=" + division;

                if (nodesEncoded.Length != 0)
                {
                    url += "&level10=" + nodesEncoded;
                }


                HttpWebRequest request = WebRequest.Create(url) as HttpWebRequest;
                request.Method = "POST";


                // Add Cookies
                var container = new CookieContainer();
                container.Add(cookies);
                request.CookieContainer = container;



                using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
                {
                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        Stream receiveStream = response.GetResponseStream();
                        StreamReader readStream = new StreamReader(receiveStream, Encoding.UTF8);
                        String test = readStream.ReadToEnd();

                        //DataContractJsonSerializer jsonSerializer = new DataContractJsonSerializer(typeof(HaystackSegment));
                        //HaystackSegment responseData = jsonSerializer.ReadObject(response.GetResponseStream()) as HaystackSegment;

                        //returnData.data = new List<HaystackNode>();

                        //foreach (var value in responseData.value)
                        //{
                        //    HaystackNode node = new HaystackNode();

                        //    returnData.data.Add(node);
                        //}

                        returnData.success = true;
                    }
                }
            }
            catch (System.Exception error)
            {
                ErrorMessage errorMessage = new ErrorMessage();
                errorMessage.status = error.Source;
                errorMessage.message = error.Message;

                returnData.errors.Add(errorMessage);

                returnData.success = false;
            }

            return returnData;
        }

        public static ReturnData<List<HaystackBreakDownCall>> GetHaystackBinsByBreakdownCalls(CookieCollection cookies, String breakdown, String segment, String filter, String username, String password, Boolean showOthers, String fromDate, String toDate)
        {
            ReturnData<List<HaystackBreakDownCall>> returnData = new ReturnData<List<HaystackBreakDownCall>>();
            returnData.data = new List<HaystackBreakDownCall>();

            Int32 baselineCycles = 5;
            String binSize = "60";
            String overlay = "None";
            String repeated = "false";
            Int32 topNumber = 10;
            String withPopulation = "false";
            //DateTime toTime = DateTime.Now.ToUniversalTime();
            //DateTime fromTime = toTime.AddDays(-1);
            DateTime fromTime;
            DateTime toTime;
            Double days;

            if ((fromDate == null) || fromDate.Trim().Length == 0)
            {
                //  Get Data 24hours From Now
                toTime = DateTime.Now.ToUniversalTime();
                fromTime = toTime.AddDays(-1);
            }
            else
            {
                fromTime = DateTime.Parse(fromDate).ToUniversalTime();
                toTime = DateTime.Parse(toDate).ToUniversalTime();

                days = (toTime - fromTime).TotalDays;

                if (days >= 7)
                {
                    binSize = "1440";
                }

                if (days >= 30)
                {
                    binSize = "10080";
                }
            }

            var startingTime = fromTime.ToString("yyyy-MM-dd") + "T" + fromTime.ToString("HH") + ":00:00.000Z";
            var endingTime = toTime.ToString("yyyy-MM-dd") + "T" + toTime.ToString("HH") + ":59:59.999Z";

            if (username == null)
            {
                username = "XNOCOBI";
            }

            try
            {
                String jSessionID = cookies[0].ToString().Split('=')[1];
                String url = "http://haystack.cable.comcast.com/api/stackedbins/calls?username=" + username;

                url += String.Format("&baselineCycles={0}&binSize={1}&breakdown={2}&fromDate={3}&overlay={4}", baselineCycles, binSize, breakdown, startingTime, overlay);
                url += String.Format("&repeated={0}&toDate={1}&topNumber={2}&withPopulation{3}", repeated, endingTime, topNumber, withPopulation);

                // Add Segment
                if (segment != null && segment.Length > 0)
                {
                    url += "&wildcard=" + segment;
                }

                // Get User Filters
                if (filter != null && filter.Length > 0)
                {
                    FilterList userFilterList = Haystack.GetUserFilters(cookies, username, password, filter);

                    if (userFilterList.FilterElement.Count > 0)
                    {
                        foreach (var filterElement in userFilterList.FilterElement)
                        {

                            url += "&" + filterElement.type + '=';

                            if (filterElement.not == true)
                            {
                                url += "Does Not Equal$";
                            }

                            for (int index = 0; index < filterElement.value.Count; index++)
                            {
                                url += filterElement.value[index];

                                if (filterElement.value.Count - 1 > index)
                                {
                                    url += '$';
                                }
                            }
                        }
                    }
                }

                HttpWebRequest request = WebRequest.Create(url) as HttpWebRequest;

                // Add Cookies
                var container = new CookieContainer();
                container.Add(cookies);
                request.CookieContainer = container;



                using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
                {
                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        //Stream receiveStream = response.GetResponseStream();
                        //StreamReader readStream = new StreamReader(receiveStream, Encoding.UTF8);
                       // String test = readStream.ReadToEnd();

                        DataContractJsonSerializer jsonSerializer = new DataContractJsonSerializer(typeof(StackedBinsBase));
                        StackedBinsBase responseData = jsonSerializer.ReadObject(response.GetResponseStream()) as StackedBinsBase;

                        List<HaystackBreakDownCall> breakdownCalls = new List<HaystackBreakDownCall>();

                        for (var index = 0; index < responseData.StackedBins[0].bins.Count - 1; index++)
                        {
                            HaystackBreakDownCall breakdownCall = new HaystackBreakDownCall();
                            breakdownCall.fields = new List<string>();
                            breakdownCall.fieldNames = new List<string>();

                            breakdownCall.eventDate = responseData.StackedBins[0].bins[index].startDate;
                            breakdownCall.data1 = responseData.StackedBins[0].bins[index].eventCount;

                            breakdownCall.fieldNames.Add(responseData.StackedBins[0].name);
                            breakdownCall.fields.Add("data1");

                            breakdownCalls.Add(breakdownCall);
                        }

                        if (responseData.StackedBins.Count > 1)
                        {
                            for (var index = 0; index < breakdownCalls.Count - 1; index++)
                            {
                                breakdownCalls[index].data2 = responseData.StackedBins[1].bins[index].eventCount;

                                breakdownCalls[index].fieldNames.Add(responseData.StackedBins[1].name);
                                breakdownCalls[index].fields.Add("data2");
                            }
                        }

                        if (responseData.StackedBins.Count > 2)
                        {
                            for (var index = 0; index < breakdownCalls.Count - 1; index++)
                            {
                                breakdownCalls[index].data3 = responseData.StackedBins[2].bins[index].eventCount;

                                breakdownCalls[index].fieldNames.Add(responseData.StackedBins[2].name);
                                breakdownCalls[index].fields.Add("data3");
                            }
                        }

                        if (responseData.StackedBins.Count > 3)
                        {
                            for (var index = 0; index < breakdownCalls.Count - 1; index++)
                            {
                                breakdownCalls[index].data4 = responseData.StackedBins[3].bins[index].eventCount;

                                breakdownCalls[index].fieldNames.Add(responseData.StackedBins[3].name);
                                breakdownCalls[index].fields.Add("data4");
                            }
                        }

                        if (responseData.StackedBins.Count == 7)
                        {
                            for (var index = 0; index < breakdownCalls.Count - 1; index++)
                            {
                                breakdownCalls[index].data5 = responseData.StackedBins[4].bins[index].eventCount;
                                breakdownCalls[index].data6 = responseData.StackedBins[5].bins[index].eventCount;
                                breakdownCalls[index].data7 = responseData.StackedBins[6].bins[index].eventCount;

                                breakdownCalls[index].fieldNames.Add(responseData.StackedBins[4].name);
                                breakdownCalls[index].fieldNames.Add(responseData.StackedBins[5].name);
                                breakdownCalls[index].fieldNames.Add(responseData.StackedBins[6].name);

                                breakdownCalls[index].fields.Add("data5");
                                breakdownCalls[index].fields.Add("data6");
                                breakdownCalls[index].fields.Add("data7");
                            }
                        }

                        if (responseData.StackedBins.Count == 11)
                        {
                            for (var index = 0; index < breakdownCalls.Count - 1; index++)
                            {
                                breakdownCalls[index].data5 = responseData.StackedBins[4].bins[index].eventCount;
                                breakdownCalls[index].data6 = responseData.StackedBins[5].bins[index].eventCount;
                                breakdownCalls[index].data7 = responseData.StackedBins[6].bins[index].eventCount;
                                breakdownCalls[index].data8 = responseData.StackedBins[7].bins[index].eventCount;
                                breakdownCalls[index].data9 = responseData.StackedBins[8].bins[index].eventCount;
                                breakdownCalls[index].data10 = responseData.StackedBins[9].bins[index].eventCount;

                                breakdownCalls[index].fieldNames.Add(responseData.StackedBins[4].name);
                                breakdownCalls[index].fieldNames.Add(responseData.StackedBins[5].name);
                                breakdownCalls[index].fieldNames.Add(responseData.StackedBins[6].name);
                                breakdownCalls[index].fieldNames.Add(responseData.StackedBins[7].name);
                                breakdownCalls[index].fieldNames.Add(responseData.StackedBins[8].name);
                                breakdownCalls[index].fieldNames.Add(responseData.StackedBins[9].name);

                                if (showOthers)
                                {
                                    breakdownCalls[index].data11 = responseData.StackedBins[10].bins[index].eventCount;
                                    breakdownCalls[index].fieldNames.Add(responseData.StackedBins[10].name);
                                }

                                breakdownCalls[index].fields.Add("data5");
                                breakdownCalls[index].fields.Add("data6");
                                breakdownCalls[index].fields.Add("data7");
                                breakdownCalls[index].fields.Add("data8");
                                breakdownCalls[index].fields.Add("data9");
                                breakdownCalls[index].fields.Add("data10");
                                breakdownCalls[index].fields.Add("data11");
                            }
                        }

                        returnData.data = breakdownCalls;

                        returnData.success = true;
                    }
                }
            }
            catch (System.Exception error)
            {
                ErrorMessage errorMessage = new ErrorMessage();
                errorMessage.status = error.Source;
                errorMessage.message = error.Message;

                returnData.errors.Add(errorMessage);

                returnData.success = false;
            }

            return returnData;
        }

        public static ReturnData<List<HaystackBreakDownCall>> GetHaystackBinsByBreakdownTickets(CookieCollection cookies, String breakdown, String segment, String filter, String username, String password, Boolean showOthers, String fromDate, String toDate)
        {
            ReturnData<List<HaystackBreakDownCall>> returnData = new ReturnData<List<HaystackBreakDownCall>>();
            returnData.data = new List<HaystackBreakDownCall>();

            Int32 baselineCycles = 5;
            String binSize = "60";
            String overlay = "None";
            String repeated = "false";
            Int32 topNumber = 10;
            String withPopulation = "false";
            //DateTime toTime = DateTime.Now.ToUniversalTime();
            //DateTime fromTime = toTime.AddDays(-1);
            DateTime fromTime;
            DateTime toTime;
            Double days;

            if ((fromDate == null) || fromDate.Trim().Length == 0)
            {
                //  Get Data 24hours From Now
                toTime = DateTime.Now.ToUniversalTime();
                fromTime = toTime.AddDays(-1);
            }
            else
            {
                fromTime = DateTime.Parse(fromDate).ToUniversalTime();
                toTime = DateTime.Parse(toDate).ToUniversalTime();

                days = (toTime - fromTime).TotalDays;

                if (days >= 7)
                {
                    binSize = "1440";
                }

                if (days >= 30)
                {
                    binSize = "10080";
                }
            }

            var startingTime = fromTime.ToString("yyyy-MM-dd") + "T" + fromTime.ToString("HH") + ":00:00.000Z";
            var endingTime = toTime.ToString("yyyy-MM-dd") + "T" + toTime.ToString("HH") + ":59:59.999Z";

            if (username.Length == 0)
            {
                username = "XNOCOBI";
            }

            try
            {
                String jSessionID = cookies[0].ToString().Split('=')[1];
                String url = "http://haystack.cable.comcast.com/api/stackedbins/tickets?username=" + username;

                url += String.Format("&baselineCycles={0}&binSize={1}&breakdown={2}&fromDate={3}&overlay={4}", baselineCycles, binSize, breakdown, startingTime, overlay);
                url += String.Format("&repeated={0}&toDate={1}&topNumber={2}&withPopulation{3}", repeated, endingTime, topNumber, withPopulation);

                // Add Segment
                if (segment != null && segment.Length > 0)
                {
                    url += "&wildcard=" + segment;
                }

                // Get User Filters
                if (filter != null && filter.Length > 0)
                {
                    FilterList userFilterList = Haystack.GetUserFilters(cookies, username, password, filter);

                    if (userFilterList.FilterElement.Count > 0)
                    {
                        foreach (var filterElement in userFilterList.FilterElement)
                        {

                            url += "&" + filterElement.type + '=';

                            if (filterElement.not == true)
                            {
                                url += "Does Not Equal$";
                            }

                            for (int index = 0; index < filterElement.value.Count; index++)
                            {
                                url += filterElement.value[index];

                                if (filterElement.value.Count - 1 > index)
                                {
                                    url += '$';
                                }
                            }
                        }
                    }
                }

                HttpWebRequest request = WebRequest.Create(url) as HttpWebRequest;

                // Add Cookies
                var container = new CookieContainer();
                container.Add(cookies);
                request.CookieContainer = container;



                using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
                {
                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        //Stream receiveStream = response.GetResponseStream();
                        //StreamReader readStream = new StreamReader(receiveStream, Encoding.UTF8);
                        // String test = readStream.ReadToEnd();

                        DataContractJsonSerializer jsonSerializer = new DataContractJsonSerializer(typeof(StackedBinsBase));
                        StackedBinsBase responseData = jsonSerializer.ReadObject(response.GetResponseStream()) as StackedBinsBase;

                        List<HaystackBreakDownCall> breakdownCalls = new List<HaystackBreakDownCall>();

                        for (var index = 0; index < responseData.StackedBins[0].bins.Count - 1; index++)
                        {
                            HaystackBreakDownCall breakdownCall = new HaystackBreakDownCall();
                            breakdownCall.fields = new List<string>();
                            breakdownCall.fieldNames = new List<string>();

                            breakdownCall.eventDate = responseData.StackedBins[0].bins[index].startDate;
                            breakdownCall.data1 = responseData.StackedBins[0].bins[index].eventCount;

                            breakdownCall.fieldNames.Add(responseData.StackedBins[0].name);
                            breakdownCall.fields.Add("data1");

                            breakdownCalls.Add(breakdownCall);
                        }

                        if (responseData.StackedBins.Count > 1)
                        {
                            for (var index = 0; index < breakdownCalls.Count - 1; index++)
                            {
                                breakdownCalls[index].data2 = responseData.StackedBins[1].bins[index].eventCount;

                                breakdownCalls[index].fieldNames.Add(responseData.StackedBins[1].name);
                                breakdownCalls[index].fields.Add("data2");
                            }
                        }

                        if (responseData.StackedBins.Count > 2)
                        {
                            for (var index = 0; index < breakdownCalls.Count - 1; index++)
                            {
                                breakdownCalls[index].data3 = responseData.StackedBins[2].bins[index].eventCount;

                                breakdownCalls[index].fieldNames.Add(responseData.StackedBins[2].name);
                                breakdownCalls[index].fields.Add("data3");
                            }
                        }

                        if (responseData.StackedBins.Count > 3)
                        {
                            for (var index = 0; index < breakdownCalls.Count - 1; index++)
                            {
                                breakdownCalls[index].data4 = responseData.StackedBins[3].bins[index].eventCount;

                                breakdownCalls[index].fieldNames.Add(responseData.StackedBins[3].name);
                                breakdownCalls[index].fields.Add("data4");
                            }
                        }

                        if (responseData.StackedBins.Count == 7)
                        {
                            for (var index = 0; index < breakdownCalls.Count - 1; index++)
                            {
                                breakdownCalls[index].data5 = responseData.StackedBins[4].bins[index].eventCount;
                                breakdownCalls[index].data6 = responseData.StackedBins[5].bins[index].eventCount;
                                breakdownCalls[index].data7 = responseData.StackedBins[6].bins[index].eventCount;

                                breakdownCalls[index].fieldNames.Add(responseData.StackedBins[4].name);
                                breakdownCalls[index].fieldNames.Add(responseData.StackedBins[5].name);
                                breakdownCalls[index].fieldNames.Add(responseData.StackedBins[6].name);

                                breakdownCalls[index].fields.Add("data5");
                                breakdownCalls[index].fields.Add("data6");
                                breakdownCalls[index].fields.Add("data7");
                            }
                        }

                        if (responseData.StackedBins.Count == 11)
                        {
                            for (var index = 0; index < breakdownCalls.Count - 1; index++)
                            {
                                breakdownCalls[index].data5 = responseData.StackedBins[4].bins[index].eventCount;
                                breakdownCalls[index].data6 = responseData.StackedBins[5].bins[index].eventCount;
                                breakdownCalls[index].data7 = responseData.StackedBins[6].bins[index].eventCount;
                                breakdownCalls[index].data8 = responseData.StackedBins[7].bins[index].eventCount;
                                breakdownCalls[index].data9 = responseData.StackedBins[8].bins[index].eventCount;
                                breakdownCalls[index].data10 = responseData.StackedBins[9].bins[index].eventCount;

                                breakdownCalls[index].fieldNames.Add(responseData.StackedBins[4].name);
                                breakdownCalls[index].fieldNames.Add(responseData.StackedBins[5].name);
                                breakdownCalls[index].fieldNames.Add(responseData.StackedBins[6].name);
                                breakdownCalls[index].fieldNames.Add(responseData.StackedBins[7].name);
                                breakdownCalls[index].fieldNames.Add(responseData.StackedBins[8].name);
                                breakdownCalls[index].fieldNames.Add(responseData.StackedBins[9].name);

                                if (showOthers)
                                {
                                    breakdownCalls[index].data11 = responseData.StackedBins[10].bins[index].eventCount;
                                    breakdownCalls[index].fieldNames.Add(responseData.StackedBins[10].name);
                                }

                                breakdownCalls[index].fields.Add("data5");
                                breakdownCalls[index].fields.Add("data6");
                                breakdownCalls[index].fields.Add("data7");
                                breakdownCalls[index].fields.Add("data8");
                                breakdownCalls[index].fields.Add("data9");
                                breakdownCalls[index].fields.Add("data10");
                                breakdownCalls[index].fields.Add("data11");
                            }
                        }

                        returnData.data = breakdownCalls;

                        returnData.success = true;
                    }
                }
            }
            catch (System.Exception error)
            {
                ErrorMessage errorMessage = new ErrorMessage();
                errorMessage.status = error.Source;
                errorMessage.message = error.Message;

                returnData.errors.Add(errorMessage);

                returnData.success = false;
            }

            return returnData;
        }

        public static ReturnData<List<HaystackBreakDownCall>> GetHaystackBinsByBreakdownTruckRolls(CookieCollection cookies, String breakdown, String segment, String filter, String username, String password, Boolean showOthers, String fromDate, String toDate)
        {
            ReturnData<List<HaystackBreakDownCall>> returnData = new ReturnData<List<HaystackBreakDownCall>>();
            returnData.data = new List<HaystackBreakDownCall>();

            Int32 baselineCycles = 5;
            String binSize = "60";
            String overlay = "None";
            String repeated = "false";
            Int32 topNumber = 10;
            String withPopulation = "false";
            //DateTime toTime = DateTime.Now.ToUniversalTime();
            //DateTime fromTime = toTime.AddDays(-1);
            DateTime fromTime;
            DateTime toTime;
            Double days;

            if ((fromDate == null) || fromDate.Trim().Length == 0)
            {
                //  Get Data 24hours From Now
                toTime = DateTime.Now.ToUniversalTime();
                fromTime = toTime.AddDays(-1);
            }
            else
            {
                fromTime = DateTime.Parse(fromDate).ToUniversalTime();
                toTime = DateTime.Parse(toDate).ToUniversalTime();

                days = (toTime - fromTime).TotalDays;

                if (days >= 7)
                {
                    binSize = "1440";
                }

                if (days >= 30)
                {
                    binSize = "10080";
                }
            }

            var startingTime = fromTime.ToString("yyyy-MM-dd") + "T" + fromTime.ToString("HH") + ":00:00.000Z";
            var endingTime = toTime.ToString("yyyy-MM-dd") + "T" + toTime.ToString("HH") + ":59:59.999Z";

            if(username.Length == 0)
            {
                username = "XNOCOBI";
            }

            try
            {
                String jSessionID = cookies[0].ToString().Split('=')[1];
                String url = "http://haystack.cable.comcast.com/api/stackedbins/truckrolls?username=" + username;

                url += String.Format("&baselineCycles={0}&binSize={1}&breakdown={2}&fromDate={3}&overlay={4}", baselineCycles, binSize, breakdown, startingTime, overlay);
                url += String.Format("&repeated={0}&toDate={1}&topNumber={2}&withPopulation{3}", repeated, endingTime, topNumber, withPopulation);

                // Add Segment
                if (segment != null && segment.Length > 0)
                {
                    url += "&wildcard=" + segment;
                }

                // Get User Filters
                if (filter != null && filter.Length > 0)
                {
                    FilterList userFilterList = Haystack.GetUserFilters(cookies, username, password, filter);

                    if (userFilterList.FilterElement.Count > 0)
                    {
                        foreach (var filterElement in userFilterList.FilterElement)
                        {

                            url += "&" + filterElement.type + '=';

                            if (filterElement.not == true)
                            {
                                url += "Does Not Equal$";
                            }

                            for (int index = 0; index < filterElement.value.Count; index++)
                            {
                                url += filterElement.value[index];

                                if (filterElement.value.Count - 1 > index)
                                {
                                    url += '$';
                                }
                            }
                        }
                    }
                }

                HttpWebRequest request = WebRequest.Create(url) as HttpWebRequest;

                // Add Cookies
                var container = new CookieContainer();
                container.Add(cookies);
                request.CookieContainer = container;



                using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
                {
                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        //Stream receiveStream = response.GetResponseStream();
                        //StreamReader readStream = new StreamReader(receiveStream, Encoding.UTF8);
                        // String test = readStream.ReadToEnd();

                        DataContractJsonSerializer jsonSerializer = new DataContractJsonSerializer(typeof(StackedBinsBase));
                        StackedBinsBase responseData = jsonSerializer.ReadObject(response.GetResponseStream()) as StackedBinsBase;

                        List<HaystackBreakDownCall> breakdownCalls = new List<HaystackBreakDownCall>();

                        for (var index = 0; index < responseData.StackedBins[0].bins.Count - 1; index++)
                        {
                            HaystackBreakDownCall breakdownCall = new HaystackBreakDownCall();
                            breakdownCall.fields = new List<string>();
                            breakdownCall.fieldNames = new List<string>();

                            breakdownCall.eventDate = responseData.StackedBins[0].bins[index].startDate;
                            breakdownCall.data1 = responseData.StackedBins[0].bins[index].eventCount;

                            breakdownCall.fieldNames.Add(responseData.StackedBins[0].name);
                            breakdownCall.fields.Add("data1");

                            breakdownCalls.Add(breakdownCall);
                        }

                        if (responseData.StackedBins.Count > 1)
                        {
                            for (var index = 0; index < breakdownCalls.Count - 1; index++)
                            {
                                breakdownCalls[index].data2 = responseData.StackedBins[1].bins[index].eventCount;

                                breakdownCalls[index].fieldNames.Add(responseData.StackedBins[1].name);
                                breakdownCalls[index].fields.Add("data2");
                            }
                        }

                        if (responseData.StackedBins.Count > 2)
                        {
                            for (var index = 0; index < breakdownCalls.Count - 1; index++)
                            {
                                breakdownCalls[index].data3 = responseData.StackedBins[2].bins[index].eventCount;

                                breakdownCalls[index].fieldNames.Add(responseData.StackedBins[2].name);
                                breakdownCalls[index].fields.Add("data3");
                            }
                        }

                        if (responseData.StackedBins.Count > 3)
                        {
                            for (var index = 0; index < breakdownCalls.Count - 1; index++)
                            {
                                breakdownCalls[index].data4 = responseData.StackedBins[3].bins[index].eventCount;

                                breakdownCalls[index].fieldNames.Add(responseData.StackedBins[3].name);
                                breakdownCalls[index].fields.Add("data4");
                            }
                        }

                        if (responseData.StackedBins.Count == 7)
                        {
                            for (var index = 0; index < breakdownCalls.Count - 1; index++)
                            {
                                breakdownCalls[index].data5 = responseData.StackedBins[4].bins[index].eventCount;
                                breakdownCalls[index].data6 = responseData.StackedBins[5].bins[index].eventCount;
                                breakdownCalls[index].data7 = responseData.StackedBins[6].bins[index].eventCount;

                                breakdownCalls[index].fieldNames.Add(responseData.StackedBins[4].name);
                                breakdownCalls[index].fieldNames.Add(responseData.StackedBins[5].name);
                                breakdownCalls[index].fieldNames.Add(responseData.StackedBins[6].name);

                                breakdownCalls[index].fields.Add("data5");
                                breakdownCalls[index].fields.Add("data6");
                                breakdownCalls[index].fields.Add("data7");
                            }
                        }

                        if (responseData.StackedBins.Count == 11)
                        {
                            for (var index = 0; index < breakdownCalls.Count - 1; index++)
                            {
                                breakdownCalls[index].data5 = responseData.StackedBins[4].bins[index].eventCount;
                                breakdownCalls[index].data6 = responseData.StackedBins[5].bins[index].eventCount;
                                breakdownCalls[index].data7 = responseData.StackedBins[6].bins[index].eventCount;
                                breakdownCalls[index].data8 = responseData.StackedBins[7].bins[index].eventCount;
                                breakdownCalls[index].data9 = responseData.StackedBins[8].bins[index].eventCount;
                                breakdownCalls[index].data10 = responseData.StackedBins[9].bins[index].eventCount;

                                breakdownCalls[index].fieldNames.Add(responseData.StackedBins[4].name);
                                breakdownCalls[index].fieldNames.Add(responseData.StackedBins[5].name);
                                breakdownCalls[index].fieldNames.Add(responseData.StackedBins[6].name);
                                breakdownCalls[index].fieldNames.Add(responseData.StackedBins[7].name);
                                breakdownCalls[index].fieldNames.Add(responseData.StackedBins[8].name);
                                breakdownCalls[index].fieldNames.Add(responseData.StackedBins[9].name);

                                if (showOthers)
                                {
                                    breakdownCalls[index].data11 = responseData.StackedBins[10].bins[index].eventCount;
                                    breakdownCalls[index].fieldNames.Add(responseData.StackedBins[10].name);
                                }

                                breakdownCalls[index].fields.Add("data5");
                                breakdownCalls[index].fields.Add("data6");
                                breakdownCalls[index].fields.Add("data7");
                                breakdownCalls[index].fields.Add("data8");
                                breakdownCalls[index].fields.Add("data9");
                                breakdownCalls[index].fields.Add("data10");
                                breakdownCalls[index].fields.Add("data11");
                            }
                        }

                        returnData.data = breakdownCalls;

                        returnData.success = true;
                    }
                }
            }
            catch (System.Exception error)
            {
                ErrorMessage errorMessage = new ErrorMessage();
                errorMessage.status = error.Source;
                errorMessage.message = error.Message;

                returnData.errors.Add(errorMessage);

                returnData.success = false;
            }

            return returnData;
        }
    }
}