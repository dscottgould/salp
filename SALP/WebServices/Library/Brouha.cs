﻿using System.Data.SqlClient;
using System.Data;
using System.Collections.Generic;


using SALP.WebServices.DataTypes;


namespace SALP.WebServices.Library
{
    public static class Brouha
    {
        public static ReturnData<List<BrouhaRecord>> GetBrouhaData()
        {
            ReturnData<List<BrouhaRecord>> returnData = new ReturnData<List<BrouhaRecord>>();
            string connectionString = @"Data Source=OBIDB-WC-P01;Initial Catalog=CPG;User Id=OBIRead;Password=OB1R3@d;";
            SqlConnection sqlConnection = new SqlConnection(connectionString);

            SqlCommand cmd = new SqlCommand("CPG.dbo.PortalOpenBrouha", sqlConnection);
            cmd.CommandType = CommandType.StoredProcedure;

            SqlDataAdapter dataAdaptor = new SqlDataAdapter(cmd);
            DataSet dataSet = new DataSet();

            try
            {
                using (sqlConnection)
                {
                    sqlConnection.Open();

                    using (cmd)
                    {
                        dataAdaptor.Fill(dataSet, "dataSet");
                    }

                }

                sqlConnection.Close();

                returnData.data = new List<BrouhaRecord>();

                foreach (DataRow row in dataSet.Tables["dataSet"].Rows)
                {
                    BrouhaRecord record = new BrouhaRecord();

                    record.id = row["id"].ToString();
                    record.title = row["title"].ToString();
                    record.severity = row["Severity"].ToString();
                    record.createDate = row["created_at"].ToString();

                    returnData.data.Add(record);
                }

                returnData.success = true;
            }
            catch (System.Exception error)
            {
                ErrorMessage errorMessage = new ErrorMessage();
                errorMessage.status = error.Source;
                errorMessage.message = error.Message;

                returnData.errors.Add(errorMessage);
                returnData.success = false;
            }

            return returnData;
        }
    }
}