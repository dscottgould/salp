﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Web;
using SALP.WebServices.DataTypes;
using System.Xml;
using System.Globalization;
using System.Data.SqlTypes;

namespace SALP.WebServices.Library
{
    public static class Comcast
    {
        public static ReturnData<String> GetDashboardByID(String dashboardId)
        {

            ReturnData<String> returnData = new ReturnData<String>();

            SqlCommand sqlCommand = new SqlCommand("Get_DashboardCookie");
            sqlCommand.CommandType = CommandType.StoredProcedure;

            sqlCommand.Parameters.Add("@Dashboard_ID", SqlDbType.Int).Value = dashboardId;

            sqlCommand.Connection = new SqlConnection("Persist Security Info=False; User ID=salpuser; Password=5@lpuser; Initial Catalog=SALP; Server=epssadb-po-01p");

            SqlDataAdapter dataAdapter = new SqlDataAdapter(sqlCommand);
            DataSet dataSet = new DataSet();

            try
            {
                using (sqlCommand.Connection)
                {
                    sqlCommand.Connection.Open();

                    using (sqlCommand)
                    {
                        dataAdapter.Fill(dataSet, "dataSet");
                    }

                    sqlCommand.Connection.Close();

                    returnData.data = dataSet.Tables[0].Rows[0]["Cookie_Value"].ToString();

                    returnData.success = true;
                }
            }
            catch (System.Exception error)
            {
                ErrorMessage errorMessage = new ErrorMessage();
                errorMessage.status = error.Source;
                errorMessage.message = error.Message;

                returnData.errors.Add(errorMessage);
                returnData.success = false;
            }

            return returnData;
        }

        public static ReturnData<List<UserDashboard>> GetUserDashboards(String accountName)
        {

            ReturnData<List<UserDashboard>> returnData = new ReturnData<List<UserDashboard>>();
            returnData.data = new List<UserDashboard>();

            SqlCommand sqlCommand = new SqlCommand("Get_Dashboard_List");
            sqlCommand.CommandType = CommandType.StoredProcedure;

            sqlCommand.Parameters.Add("@NT_Login", SqlDbType.VarChar).Value = accountName;

            sqlCommand.Connection = new SqlConnection("Persist Security Info=False; User ID=salpuser; Password=5@lpuser; Initial Catalog=SALP; Server=epssadb-po-01p");

            SqlDataAdapter dataAdapter = new SqlDataAdapter(sqlCommand);
            DataSet dataSet = new DataSet();

            try
            {
                using (sqlCommand.Connection)
                {
                    sqlCommand.Connection.Open();

                    using (sqlCommand)
                    {
                        dataAdapter.Fill(dataSet, "dataSet");
                    }

                    sqlCommand.Connection.Close();

                    for (int indexRow = 0; indexRow <= dataSet.Tables[0].Rows.Count - 1; indexRow++)
                    {
                        UserDashboard dashboard = new UserDashboard();

                        dashboard.dashboardId = dataSet.Tables[0].Rows[indexRow]["Dashboard_ID"].ToString();
                        dashboard.dashboardName = dataSet.Tables[0].Rows[indexRow]["Dashboard_Name"].ToString();
                        //dashboard.cookieValue = dataSet.Tables[0].Rows[indexRow]["Cookie_Value"].ToString();

                        returnData.data.Add(dashboard);
                    }

                    returnData.success = true;
                }
            }
            catch (System.Exception error)
            {
                ErrorMessage errorMessage = new ErrorMessage();
                errorMessage.status = error.Source;
                errorMessage.message = error.Message;

                returnData.errors.Add(errorMessage);
                returnData.success = false;
            }

            return returnData;
        }

        public static ReturnData<UserDashboard> GetUserCookie(String accountName)
        {
            ReturnData<UserDashboard> returnData = new ReturnData<UserDashboard>();

            SqlCommand sqlCommand = new SqlCommand("Get_Or_Create_User_All");
            sqlCommand.CommandType = CommandType.StoredProcedure;

            sqlCommand.Parameters.Add("@NT_Login", SqlDbType.VarChar).Value = accountName;

            sqlCommand.Connection = new SqlConnection("Persist Security Info=False; User ID=salpuser; Password=5@lpuser; Initial Catalog=SALP; Server=epssadb-po-01p");

            SqlDataAdapter dataAdapter = new SqlDataAdapter(sqlCommand);
            DataSet dataSet = new DataSet();

            try
            {
                using (sqlCommand.Connection)
                {
                    sqlCommand.Connection.Open();

                    using (sqlCommand)
                    {
                        dataAdapter.Fill(dataSet, "dataSet");
                    }

                    sqlCommand.Connection.Close();

                    UserDashboard dashboard = new UserDashboard();
                    dashboard.dashboardId = dataSet.Tables[0].Rows[0]["Dashboard_ID"].ToString();
                    dashboard.dashboardName = dataSet.Tables[0].Rows[0]["Dashboard_Name"].ToString();
                    dashboard.cookieValue = dataSet.Tables[0].Rows[0]["Cookie_Value"].ToString();

                    returnData.data = dashboard;

                    returnData.success = true;
                }
            }
            catch (System.Exception error)
            {
                ErrorMessage errorMessage = new ErrorMessage();
                errorMessage.status = error.Source;
                errorMessage.message = error.Message;

                returnData.errors.Add(errorMessage);
                returnData.success = false;
            }

            return returnData;
        }

        public static ReturnData<String> SaveUserCookie(String accountName, String dashboardName, String cookie)
        {
            ReturnData<String> returnData = new ReturnData<String>();

            SqlCommand sqlCommand = new SqlCommand("Update_User_All");
            sqlCommand.CommandType = CommandType.StoredProcedure;

            sqlCommand.Parameters.Add("@NT_Login", SqlDbType.VarChar).Value = accountName;
            sqlCommand.Parameters.Add("@Dashboard_Name", SqlDbType.VarChar).Value = dashboardName;
            sqlCommand.Parameters.Add("@Cookie_Value", SqlDbType.VarChar).Value = cookie;

            sqlCommand.Connection = new SqlConnection("Persist Security Info=False; User ID=salpuser; Password=5@lpuser; Initial Catalog=SALP; Server=epssadb-po-01p");

            SqlDataAdapter dataAdapter = new SqlDataAdapter(sqlCommand);
            DataSet dataSet = new DataSet();

            try
            {
                using (sqlCommand.Connection)
                {
                    sqlCommand.Connection.Open();
                    //sqlCommand.ExecuteNonQuery();

                    using (sqlCommand)
                    {
                        dataAdapter.Fill(dataSet, "dataSet");
                    }

                    sqlCommand.Connection.Close();

                    returnData.data = dataSet.Tables[0].Rows[0]["Dashboard_ID"].ToString();

                    returnData.success = true;
                }

                returnData.success = true;
            }
            catch (System.Exception error)
            {
                ErrorMessage errorMessage = new ErrorMessage();
                errorMessage.status = error.Source;
                errorMessage.message = error.Message;

                returnData.errors.Add(errorMessage);
                returnData.success = false;
            }

            return returnData;
        }


        public static ReturnData<String> RenameDashboard(String dashboardId, String dashboardName)
        {
            ReturnData<String> returnData = new ReturnData<String>();

            SqlCommand sqlCommand = new SqlCommand("Rename_Dashboard_By_ID");
            sqlCommand.CommandType = CommandType.StoredProcedure;

            sqlCommand.Parameters.Add("@Dashboard_ID", SqlDbType.Int).Value = dashboardId;
            sqlCommand.Parameters.Add("@Dashboard_Name", SqlDbType.VarChar).Value = dashboardName;

            sqlCommand.Connection = new SqlConnection("Persist Security Info=False; User ID=salpuser; Password=5@lpuser; Initial Catalog=SALP; Server=epssadb-po-01p");

            SqlDataAdapter dataAdapter = new SqlDataAdapter(sqlCommand);
            DataSet dataSet = new DataSet();

            try
            {
                sqlCommand.Connection.Open();
                sqlCommand.ExecuteNonQuery();

                returnData.success = true;
            }
            catch (System.Exception error)
            {
                ErrorMessage errorMessage = new ErrorMessage();
                errorMessage.status = error.Source;
                errorMessage.message = error.Message;

                returnData.errors.Add(errorMessage);
                returnData.success = false;
            }

            return returnData;
        }

        public static ReturnData<String> SetDefaultDashboard(String accountName, String dashboardId)
        {
            ReturnData<String> returnData = new ReturnData<String>();

            SqlCommand sqlCommand = new SqlCommand("Set_Default_Dashboard_By_ID");
            sqlCommand.CommandType = CommandType.StoredProcedure;

            sqlCommand.Parameters.Add("@NT_LOGIN", SqlDbType.VarChar).Value = accountName;
            sqlCommand.Parameters.Add("@Dashboard_ID", SqlDbType.Int).Value = dashboardId;

            sqlCommand.Connection = new SqlConnection("Persist Security Info=False; User ID=salpuser; Password=5@lpuser; Initial Catalog=SALP; Server=epssadb-po-01p");

            SqlDataAdapter dataAdapter = new SqlDataAdapter(sqlCommand);
            DataSet dataSet = new DataSet();

            try
            {
                sqlCommand.Connection.Open();
                sqlCommand.ExecuteNonQuery();

                returnData.success = true;
            }
            catch (System.Exception error)
            {
                ErrorMessage errorMessage = new ErrorMessage();
                errorMessage.status = error.Source;
                errorMessage.message = error.Message;

                returnData.errors.Add(errorMessage);
                returnData.success = false;
            }

            return returnData;
        }

        public static ReturnData<String> DeleteDashboard(String accountName, String dashboardId)
        {
            ReturnData<String> returnData = new ReturnData<String>();

            SqlCommand sqlCommand = new SqlCommand("Delete_Cookie_Dash");
            sqlCommand.CommandType = CommandType.StoredProcedure;

            sqlCommand.Parameters.Add("@NT_Login", SqlDbType.VarChar).Value = accountName;
            sqlCommand.Parameters.Add("@Dashboard_ID", SqlDbType.VarChar).Value = dashboardId;

            sqlCommand.Connection = new SqlConnection("Persist Security Info=False; User ID=salpuser; Password=5@lpuser; Initial Catalog=SALP; Server=epssadb-po-01p");

            SqlDataAdapter dataAdapter = new SqlDataAdapter(sqlCommand);
            DataSet dataSet = new DataSet();

            try
            {
                sqlCommand.Connection.Open();
                sqlCommand.ExecuteNonQuery();

                returnData.success = true;
            }
            catch (System.Exception error)
            {
                ErrorMessage errorMessage = new ErrorMessage();
                errorMessage.status = error.Source;
                errorMessage.message = error.Message;

                returnData.errors.Add(errorMessage);
                returnData.success = false;
            }

            return returnData;
        }

        public static ReturnData<Stocks> GetComcastStock()
        {
            String url = "https://sitecore.comcastnow.com/stockquotes/api/stockquotes";
            String uri = "https://sitecore.comcastnow.com";

            ReturnData<Stocks> returnData = new ReturnData<Stocks>();

            HttpWebRequest request = WebRequest.Create(url) as HttpWebRequest;
            //request.Method = "GET";
            request.CookieContainer = new CookieContainer();

            CredentialCache cache = new CredentialCache();
            //cache.Add(new Uri(uri), "NTLM", new NetworkCredential("dgould001c", "Q1w2e3r4!", "CABLE"));
            request.Credentials = cache;

            // Locally getting user domain
            //var test = System.Environment.UserDomainName;
            //var test2 = System.Net.NetworkInformation.IPGlobalProperties.GetIPGlobalProperties().DomainName;

            try
            {
                using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
                {
                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        // Testing
                        //Stream receiveStream = response.GetResponseStream();
                        //StreamReader readStream = new StreamReader(receiveStream, Encoding.UTF8);
                        //String test = readStream.ReadToEnd();
                        //readStream.Close();

                        DataContractJsonSerializer jsonSerializer = new DataContractJsonSerializer(typeof(List<Stocks>));
                        List<Stocks> responseData = jsonSerializer.ReadObject(response.GetResponseStream()) as List<Stocks>;

                        Stocks stocks = new Stocks();
                        stocks.symbol = responseData[0].symbol;
                        stocks.last = responseData[0].last;
                        stocks.change = responseData[0].change;

                        returnData.data = stocks;
                        returnData.success = true;
                    }
                }
            }
            catch (System.Exception error)
            {
                ErrorMessage errorMessage = new ErrorMessage();
                errorMessage.status = error.Source;
                errorMessage.message = error.Message;

                returnData.errors.Add(errorMessage);
                returnData.success = false;
            }

            return returnData;
        }

        public static ReturnData<List<CNArticle>> GetComcastNow()
        {
            ReturnData<List<CNArticle>> returnData = new ReturnData<List<CNArticle>>();
            returnData.data = new List<CNArticle>();

            try
            {
                returnData.data = GetArticles(1);
                returnData.data.AddRange(GetArticles(2));

                returnData.success = true;
            }
            catch (System.Exception error)
            {
                ErrorMessage errorMessage = new ErrorMessage();
                errorMessage.status = error.Source;
                errorMessage.message = error.Message;

                returnData.errors.Add(errorMessage);
                returnData.success = false;
            }

            return returnData;
        }


        private static List<CNArticle> GetArticles(int page)
        {
            List<CNArticle> articles = new List<CNArticle>();

            String url = "https://sitecore.comcastnow.com/content_delivery/api/feed/{D1230B7D-0D5E-4EFA-92A3-5FCCFB9BDA07}?page=" + page;
            String uri = "https://sitecore.comcastnow.com";

            HttpWebRequest request = WebRequest.Create(url) as HttpWebRequest;
            //request.Method = "GET";
            request.CookieContainer = new CookieContainer();

            CredentialCache cache = new CredentialCache();
            //cache.Add(new Uri(uri), "NTLM", new NetworkCredential("dgould001c", "Q1w2e3r4!", "CABLE"));
            request.Credentials = cache;

            // Locally getting user domain
            //var test = System.Environment.UserDomainName;
            //var test2 = System.Net.NetworkInformation.IPGlobalProperties.GetIPGlobalProperties().DomainName;

            using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
            {
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    // Testing
                    //Stream receiveStream = response.GetResponseStream();
                    //StreamReader readStream = new StreamReader(receiveStream, Encoding.UTF8);
                    //String test = readStream.ReadToEnd();
                    //readStream.Close();

                    DataContractJsonSerializer jsonSerializer = new DataContractJsonSerializer(typeof(ComcastNowBase));
                    ComcastNowBase responseData = jsonSerializer.ReadObject(response.GetResponseStream()) as ComcastNowBase;

                    CNBase CNData = new CNBase();

                    foreach (ComcastNowItem item in responseData.items)
                    {
                        CNArticle article = new CNArticle();

                        foreach (ComcastNowTag tag in item.tags)
                        {
                            CNTag newTag = new CNTag();

                            newTag.name = tag.name;
                            newTag.url = "https://www.comcastnow.com" + tag.url;

                            article.tags.Add(newTag);
                        }

                        article.title = item.title;
                        article.url = "https://www.comcastnow.com" + item.url;

                        articles.Add(article);

                        //CNData.articles.Add(article);
                    }
                }
            }

            return articles;
        }


        public static ReturnData<List<HSDRecord>> GetHSDMetrics()
        {
            ReturnData<List<HSDRecord>> returnData = new ReturnData<List<HSDRecord>>();


            string connectionString = @"Data Source=OBIDB-WC-P01;Initial Catalog=OBI;Integrated Security=False;User Id=OBIRead;Password=OB1R3@d";
            SqlConnection sqlConnection = new SqlConnection(connectionString);

            SqlCommand cmd = new SqlCommand("SQR_CR_Daily_Count_by_Product_Timeframe_Pivot", sqlConnection);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@Product", "HSD"));
            cmd.Parameters.Add(new SqlParameter("@EndDate", "7/27/16"));
            cmd.Parameters.Add(new SqlParameter("@Timeframe", "DP30"));

            SqlDataAdapter dataAdaptor = new SqlDataAdapter(cmd);
            DataSet dataSet = new DataSet();

            using (sqlConnection)
            {
                sqlConnection.Open();

                using (cmd)
                {
                    dataAdaptor.Fill(dataSet, "dataSet");
                }

            }

            sqlConnection.Close();

            returnData.data = new List<HSDRecord>();

            foreach (DataRow row in dataSet.Tables["dataSet"].Rows)
            {
                HSDRecord record = new HSDRecord();

                record.date = row["Create_Date"].ToString();
                record.apps = row["Apps/Tools/Sites"].ToString();
                record.connect = row["Connect"].ToString();
                record.cpe = row["CPE"].ToString();
                record.degradedConectivity = row["Degraded_Connectivity"].ToString();
                record.generalInquiry = row["General_Inquiry"].ToString();
                record.homeNetwork = row["HomeNetwork"].ToString();
                record.noConectivity = row["No_Connectivity"].ToString();
                record.callCount = row["Call_Count"].ToString();

                returnData.data.Add(record);
            }

            returnData.success = true;

            return returnData;
        }

        public static ReturnData<List<SubscriberCounts>> GetSubscriberCounts()
        {
            ReturnData<List<SubscriberCounts>> returnData = new ReturnData<List<SubscriberCounts>>();

            string connectionString = @"Data Source=OBIDB-WC-P01;Initial Catalog=OBI;Integrated Security=False;User Id=OBIRead;Password=OB1R3@d";
            SqlConnection sqlConnection = new SqlConnection(connectionString);

            SqlCommand cmd = new SqlCommand("OBI.dbo.PortalSubCounts", sqlConnection);
            //cmd.CommandType = CommandType.StoredProcedure;
            //cmd.Parameters.Add(new SqlParameter("@Product", "HSD"));
            //cmd.Parameters.Add(new SqlParameter("@EndDate", "7/27/16"));
            //cmd.Parameters.Add(new SqlParameter("@Timeframe", "DP30"));

            SqlDataAdapter dataAdaptor = new SqlDataAdapter(cmd);
            DataSet dataSet = new DataSet();

            using (sqlConnection)
            {
                sqlConnection.Open();

                using (cmd)
                {
                    dataAdaptor.Fill(dataSet, "dataSet");
                }

            }

            sqlConnection.Close();

            returnData.data = new List<SubscriberCounts>();

            foreach (DataRow row in dataSet.Tables["dataSet"].Rows)
            {
                SubscriberCounts record = new SubscriberCounts();

                record.productService = row["Product_Service"].ToString();
                record.countDate = row["Count_Date"].ToString();
                record.subCount = row["Sub_Count"].ToString();
                record.comp = row["Comp"].ToString();

                returnData.data.Add(record);
            }

            returnData.success = true;

            return returnData;
        }


        public static ReturnData<List<Portlet>> GetPorletsList()
        {

            ReturnData<List<Portlet>> returnData = new ReturnData<List<Portlet>>();
            returnData.data = new List<Portlet>();

            SqlCommand sqlCommand = new SqlCommand("Get_Portlet_List");
            sqlCommand.CommandType = CommandType.StoredProcedure;

            //sqlCommand.Parameters.Add("@NT_Login", SqlDbType.VarChar).Value = accountName;

            sqlCommand.Connection = new SqlConnection("Persist Security Info=False; User ID=salpuser; Password=5@lpuser; Initial Catalog=SALP; Server=epssadb-po-01p");

            SqlDataAdapter dataAdapter = new SqlDataAdapter(sqlCommand);
            DataSet dataSet = new DataSet();

            try
            {
                using (sqlCommand.Connection)
                {
                    sqlCommand.Connection.Open();

                    using (sqlCommand)
                    {
                        dataAdapter.Fill(dataSet, "dataSet");
                    }

                    sqlCommand.Connection.Close();

                    for (int indexRow = 0; indexRow <= dataSet.Tables[0].Rows.Count - 1; indexRow++)
                    {
                        Portlet portlet = new Portlet();

                        portlet.id = dataSet.Tables[0].Rows[indexRow]["Id"].ToString();
                        portlet.displayName = dataSet.Tables[0].Rows[indexRow]["Display_Name"].ToString();
                        portlet.objectClass = dataSet.Tables[0].Rows[indexRow]["Class"].ToString();
                        portlet.category = dataSet.Tables[0].Rows[indexRow]["Category"].ToString();

                        returnData.data.Add(portlet);
                    }

                    returnData.success = true;
                }
            }
            catch (System.Exception error)
            {
                ErrorMessage errorMessage = new ErrorMessage();
                errorMessage.status = error.Source;
                errorMessage.message = error.Message;

                returnData.errors.Add(errorMessage);
                returnData.success = false;
            }

            return returnData;
        }

        public static ReturnData<String> GetUserPortlets(String accountName)
        {
            ReturnData<String> returnData = new ReturnData<String>();

            SqlCommand sqlCommand = new SqlCommand("Get_User_Portlets");
            sqlCommand.CommandType = CommandType.StoredProcedure;

            sqlCommand.Parameters.Add("@NT_Login", SqlDbType.VarChar).Value = accountName;

            sqlCommand.Connection = new SqlConnection("Persist Security Info=False; User ID=salpuser; Password=5@lpuser; Initial Catalog=SALP; Server=epssadb-po-01p");

            SqlDataAdapter dataAdapter = new SqlDataAdapter(sqlCommand);
            DataSet dataSet = new DataSet();

            try
            {
                using (sqlCommand.Connection)
                {
                    sqlCommand.Connection.Open();

                    using (sqlCommand)
                    {
                        dataAdapter.Fill(dataSet, "dataSet");
                    }

                    sqlCommand.Connection.Close();

                    returnData.data = dataSet.Tables[0].Rows[0]["Portlets"].ToString();

                    returnData.success = true;
                }
            }
            catch (System.Exception error)
            {
                ErrorMessage errorMessage = new ErrorMessage();
                errorMessage.status = error.Source;
                errorMessage.message = error.Message;

                returnData.errors.Add(errorMessage);
                returnData.success = false;
            }

            return returnData;
        }

        public static ReturnData<String> SaveUserPortlets(String accountName, String porletsList)
        {
            ReturnData<String> returnData = new ReturnData<String>();

            SqlCommand sqlCommand = new SqlCommand("Update_User_Portlets");
            sqlCommand.CommandType = CommandType.StoredProcedure;

            sqlCommand.Parameters.Add("@NT_Login", SqlDbType.VarChar).Value = accountName;
            sqlCommand.Parameters.Add("@Portlets", SqlDbType.VarChar).Value = porletsList;

            sqlCommand.Connection = new SqlConnection("Persist Security Info=False; User ID=salpuser; Password=5@lpuser; Initial Catalog=SALP; Server=epssadb-po-01p");

            SqlDataAdapter dataAdapter = new SqlDataAdapter(sqlCommand);
            DataSet dataSet = new DataSet();

            try
            {
                sqlCommand.Connection.Open();
                sqlCommand.ExecuteNonQuery();

                returnData.success = true;
            }
            catch (System.Exception error)
            {
                ErrorMessage errorMessage = new ErrorMessage();
                errorMessage.status = error.Source;
                errorMessage.message = error.Message;

                returnData.errors.Add(errorMessage);
                returnData.success = false;
            }

            return returnData;
        }

        public static ReturnData<List<SelectData>> GetSelectData(String accountName, int categoryId)
        {
            ReturnData<List<SelectData>> returnData = new ReturnData<List<SelectData>>();
            returnData.data = new List<SelectData>();

            SqlCommand sqlCommand = new SqlCommand("Get_User_Portlets_By_Category");
            sqlCommand.CommandType = CommandType.StoredProcedure;

            sqlCommand.Parameters.Add("@NT_Login", SqlDbType.VarChar).Value = accountName;
            sqlCommand.Parameters.Add("@CategoryID", SqlDbType.VarChar).Value = categoryId;

            sqlCommand.Connection = new SqlConnection("Persist Security Info=False; User ID=salpuser; Password=5@lpuser; Initial Catalog=SALP; Server=epssadb-po-01p");

            SqlDataAdapter dataAdapter = new SqlDataAdapter(sqlCommand);
            DataSet dataSet = new DataSet();

            try
            {
                using (sqlCommand.Connection)
                {
                    sqlCommand.Connection.Open();

                    using (sqlCommand)
                    {
                        dataAdapter.Fill(dataSet, "dataSet");
                    }

                    sqlCommand.Connection.Close();

                    for (int indexRow = 0; indexRow <= dataSet.Tables[0].Rows.Count - 1; indexRow++)
                    {
                        SelectData selectData = new SelectData();

                        selectData.displayName = dataSet.Tables[0].Rows[indexRow]["Display_Name"].ToString();
                        selectData.className = dataSet.Tables[0].Rows[indexRow]["Class"].ToString();

                        returnData.data.Add(selectData);
                    }

                    returnData.success = true;
                }
            }
            catch (System.Exception error)
            {
                ErrorMessage errorMessage = new ErrorMessage();
                errorMessage.status = error.Source;
                errorMessage.message = error.Message;

                returnData.errors.Add(errorMessage);
                returnData.success = false;
            }

            return returnData;
        }

        public static ReturnData<List<Ticket>> GetTickets()
        {
            ReturnData<List<Ticket>> returnData = new ReturnData<List<Ticket>>();
            returnData.data = new List<Ticket>();

            SqlCommand sqlCommand = new SqlCommand("Get_bandaid_alltickets");
            sqlCommand.CommandType = CommandType.StoredProcedure;

            sqlCommand.Connection = new SqlConnection("Persist Security Info=False; User ID=OBIRead; Password=OB1R3@d; Initial Catalog=Tickets; Server=obidb-wc-p01");

            SqlDataAdapter dataAdapter = new SqlDataAdapter(sqlCommand);
            DataSet dataSet = new DataSet();

            try
            {
                using (sqlCommand.Connection)
                {
                    sqlCommand.Connection.Open();

                    using (sqlCommand)
                    {
                        dataAdapter.Fill(dataSet, "dataSet");
                    }

                    sqlCommand.Connection.Close();

                    for (int indexRow = 0; indexRow <= dataSet.Tables[0].Rows.Count - 1; indexRow++)
                    {
                        Ticket ticket = new Ticket();


                        ticket.ticketId = dataSet.Tables[0].Rows[indexRow]["tt_Ticket_id"].ToString();
                        ticket.calcTTR = dataSet.Tables[0].Rows[indexRow]["CALC_TTR"].ToString();
                        ticket.smRelatedTicket = dataSet.Tables[0].Rows[indexRow]["SM_RELATED_TICKET"].ToString();

                        ticket.causeDescription = dataSet.Tables[0].Rows[indexRow]["tt_CAUSE_DESCRIPTION"].ToString();
                        ticket.solutionDescription = dataSet.Tables[0].Rows[indexRow]["tt_SOLUTION_DESCRIPTION"].ToString();
                        ticket.severity = dataSet.Tables[0].Rows[indexRow]["tt_SEVERITY"].ToString();

                        ticket.alarmStartDate = dataSet.Tables[0].Rows[indexRow]["TT_ALARM_START"].ToString();
                        ticket.alarmStartTime = dataSet.Tables[0].Rows[indexRow]["TT_ALARM_START"].ToString();

                        ticket.workingStartDate = dataSet.Tables[0].Rows[indexRow]["tts_working"].ToString();
                        ticket.workingStartTime = dataSet.Tables[0].Rows[indexRow]["tts_working"].ToString();

                        ticket.resolvedDate = dataSet.Tables[0].Rows[indexRow]["tts_RESOLVED"].ToString();
                        ticket.resolvedTime = dataSet.Tables[0].Rows[indexRow]["tts_RESOLVED"].ToString();

                        ticket.actualStartDate = dataSet.Tables[0].Rows[indexRow]["TT_ACTUAL_START"].ToString();
                        ticket.actualStartTime = dataSet.Tables[0].Rows[indexRow]["TT_ACTUAL_START"].ToString();

                        ticket.actualEndDate = dataSet.Tables[0].Rows[indexRow]["tt_ACTUAL_END"].ToString();
                        ticket.actualEndTime = dataSet.Tables[0].Rows[indexRow]["tt_ACTUAL_END"].ToString();

                        ticket.modifiedDate = dataSet.Tables[0].Rows[indexRow]["tt_MODIFIED_DATE"].ToString();

                        returnData.data.Add(ticket);
                    }


                    returnData.success = true;
                }
            }
            catch (System.Exception error)
            {
                ErrorMessage errorMessage = new ErrorMessage();
                errorMessage.status = error.Source;
                errorMessage.message = error.Message;

                returnData.errors.Add(errorMessage);
                returnData.success = false;
            }

            return returnData;
        }

        public static ReturnData<Tickets> GetTicket(String ticketNumber)
        {
            ReturnData<Tickets> returnData = new ReturnData<Tickets>();
            returnData.data = new Tickets();

            SqlCommand sqlCommand = new SqlCommand("Get_Bandaid_ticket");
            sqlCommand.CommandType = CommandType.StoredProcedure;

            sqlCommand.Parameters.Add("@Ticket_id", SqlDbType.VarChar).Value = ticketNumber;

            sqlCommand.Connection = new SqlConnection("Persist Security Info=False; User ID=OBIRead; Password=OB1R3@d; Initial Catalog=Tickets; Server=obidb-wc-p01");

            SqlDataAdapter dataAdapter = new SqlDataAdapter(sqlCommand);
            DataSet dataSet = new DataSet();

            try
            {
                using (sqlCommand.Connection)
                {
                    sqlCommand.Connection.Open();

                    using (sqlCommand)
                    {
                        dataAdapter.Fill(dataSet, "dataSet");
                    }

                    sqlCommand.Connection.Close();

                    //Old Ticket
                    returnData.data.oldTicket.ticketId = dataSet.Tables[0].Rows[0]["tt_Ticket_id"].ToString();
                    returnData.data.oldTicket.calcTTR = dataSet.Tables[0].Rows[0]["CALC_TTR"].ToString();
                    returnData.data.oldTicket.causeDescription = dataSet.Tables[0].Rows[0]["tt_CAUSE_DESCRIPTION"].ToString();
                    returnData.data.oldTicket.solutionDescription = dataSet.Tables[0].Rows[0]["tt_SOLUTION_DESCRIPTION"].ToString();
                    returnData.data.oldTicket.smRelatedTicket = dataSet.Tables[0].Rows[0]["SM_RELATED_TICKET"].ToString();
                    returnData.data.oldTicket.severity = dataSet.Tables[0].Rows[0]["tt_SEVERITY"].ToString();

                    returnData.data.oldTicket.alarmStartDate = dataSet.Tables[0].Rows[0]["TT_ALARM_START"].ToString();
                    returnData.data.oldTicket.alarmStartTime = dataSet.Tables[0].Rows[0]["TT_ALARM_START"].ToString();

                    returnData.data.oldTicket.workingStartDate = dataSet.Tables[0].Rows[0]["tts_working"].ToString();
                    returnData.data.oldTicket.workingStartTime = dataSet.Tables[0].Rows[0]["tts_working"].ToString();

                    returnData.data.oldTicket.resolvedDate = dataSet.Tables[0].Rows[0]["tts_RESOLVED"].ToString();
                    returnData.data.oldTicket.resolvedTime = dataSet.Tables[0].Rows[0]["tts_RESOLVED"].ToString();

                    returnData.data.oldTicket.actualStartDate = dataSet.Tables[0].Rows[0]["TT_ACTUAL_START"].ToString();
                    returnData.data.oldTicket.actualStartTime = dataSet.Tables[0].Rows[0]["TT_ACTUAL_START"].ToString();

                    returnData.data.oldTicket.actualEndDate = dataSet.Tables[0].Rows[0]["tt_ACTUAL_END"].ToString();
                    returnData.data.oldTicket.actualEndTime = dataSet.Tables[0].Rows[0]["tt_ACTUAL_END"].ToString();

                    returnData.data.oldTicket.modifiedDate = dataSet.Tables[0].Rows[0]["tt_MODIFIED_DATE"].ToString();


                    // New Ticket
                    returnData.data.newTicket.ticketId = dataSet.Tables[0].Rows[0]["TICKET_ID"].ToString();
                    returnData.data.newTicket.adjDurationMinutes = dataSet.Tables[0].Rows[0]["ADJ_DURATION_MINUTES"].ToString();
                    returnData.data.newTicket.causeDescription = dataSet.Tables[0].Rows[0]["CAUSE_DESCRIPTION"].ToString();
                    returnData.data.newTicket.solutionDescription = dataSet.Tables[0].Rows[0]["SOLUTION_DESCRIPTION"].ToString();
                    returnData.data.newTicket.smId = dataSet.Tables[0].Rows[0]["SM_ID"].ToString();
                    returnData.data.newTicket.severity = dataSet.Tables[0].Rows[0]["SEVERITY"].ToString();

                    returnData.data.newTicket.alarmStartDate = dataSet.Tables[0].Rows[0]["ALARM_START"].ToString();
                    returnData.data.newTicket.alarmStartTime = dataSet.Tables[0].Rows[0]["ALARM_START"].ToString();

                    returnData.data.newTicket.workingStartDate = dataSet.Tables[0].Rows[0]["WORKING_START_TIME"].ToString();
                    returnData.data.newTicket.workingStartTime = dataSet.Tables[0].Rows[0]["WORKING_START_TIME"].ToString();

                    returnData.data.newTicket.resolvedDate = dataSet.Tables[0].Rows[0]["RESOLVED_TIME"].ToString();
                    returnData.data.newTicket.resolvedTime = dataSet.Tables[0].Rows[0]["RESOLVED_TIME"].ToString();

                    returnData.data.newTicket.actualStartDate = dataSet.Tables[0].Rows[0]["ACTUAL_START_TIME"].ToString();
                    returnData.data.newTicket.actualStartTime = dataSet.Tables[0].Rows[0]["ACTUAL_START_TIME"].ToString();

                    returnData.data.newTicket.actualEndDate = dataSet.Tables[0].Rows[0]["ACTUAL_END_TIME"].ToString();
                    returnData.data.newTicket.actualEndTime = dataSet.Tables[0].Rows[0]["ACTUAL_END_TIME"].ToString();

                    returnData.data.newTicket.changeReason = dataSet.Tables[0].Rows[0]["CHANGE_REASON"].ToString();
                    returnData.data.newTicket.modifiedDate = dataSet.Tables[0].Rows[0]["MODIFIED_DATE"].ToString();

                    returnData.success = true;
                }
            }
            catch (System.Exception error)
            {
                ErrorMessage errorMessage = new ErrorMessage();
                errorMessage.status = error.Source;
                errorMessage.message = error.Message;

                returnData.errors.Add(errorMessage);
                returnData.success = false;
            }

            return returnData;
        }


        public static ReturnData<String> UpdateTicket(String actualEndDate, String actualEndTime, String actualStartDate, String actualStartTime,
            String adjDurationMinutes, String alarmStartDate, String alarmStartTime, String causeDescription, String changeReason, String modifiedDate,
            String resolvedDate, String resolvedTime, String severity, String smId, String solutionDescription, String ticketId,
            String workingStartDate, String workingStartTime)
        {
            ReturnData<String> returnData = new ReturnData<String>();

            SqlCommand sqlCommand = new SqlCommand("Insert_Update_Bandaid");
            sqlCommand.CommandType = CommandType.StoredProcedure;

            String alarmStart = (alarmStartDate + " " + alarmStartTime).Trim();
            String workingStart = (workingStartDate + " " + workingStartTime).Trim();
            String resolved = (resolvedDate + " " + resolvedTime).Trim();
            String actualStart = (actualStartDate + " " + actualStartTime).Trim();
            String actualEnd = (actualEndDate + " " + actualEndTime).Trim();

            if (alarmStart.Length == 0) alarmStart = null;
            if (workingStart.Length == 0) workingStart = null;
            if (resolved.Length == 0) resolved = null;
            if (actualStart.Length == 0) actualStart = null;
            if (actualEnd.Length == 0) actualEnd = null;

            if (adjDurationMinutes.Trim().Length == 0) adjDurationMinutes = null;
            if (smId.Trim().Length == 0) smId = null;
            if (severity.Trim().Length == 0) severity = null;
            if (causeDescription.Trim().Length == 0) causeDescription = null;
            if (solutionDescription.Trim().Length == 0) solutionDescription = null;
            if (changeReason.Trim().Length == 0) changeReason = null;

            sqlCommand.Parameters.Add("@Ticket_id", SqlDbType.VarChar).Value = ticketId;
            sqlCommand.Parameters.Add("@SM_Id", SqlDbType.VarChar).Value = smId;
            sqlCommand.Parameters.Add("@Severity", SqlDbType.VarChar).Value = severity;
            sqlCommand.Parameters.Add("@ADJ_DURATION_MINUTES", SqlDbType.Int).Value = adjDurationMinutes;
            sqlCommand.Parameters.Add("@Cause_description", SqlDbType.VarChar).Value = causeDescription;
            sqlCommand.Parameters.Add("@solution_description", SqlDbType.VarChar).Value = solutionDescription;
            sqlCommand.Parameters.Add("@alarm_start", SqlDbType.DateTime).Value = alarmStart;
            sqlCommand.Parameters.Add("@Working_start_time", SqlDbType.DateTime).Value = workingStart;
            sqlCommand.Parameters.Add("@Resolved_time", SqlDbType.DateTime).Value = resolved;
            sqlCommand.Parameters.Add("@actual_start_time", SqlDbType.DateTime).Value = actualStart;
            sqlCommand.Parameters.Add("@actual_end_time ", SqlDbType.DateTime).Value = actualEnd;
            sqlCommand.Parameters.Add("@change_reason", SqlDbType.VarChar).Value = changeReason;
            sqlCommand.Parameters.Add("@Modified_date", SqlDbType.DateTime).Value = modifiedDate;

            sqlCommand.Connection = new SqlConnection("Persist Security Info=False; User ID=OBIRead; Password=OB1R3@d; Initial Catalog=Tickets; Server=obidb-wc-p01");

            SqlDataAdapter dataAdapter = new SqlDataAdapter(sqlCommand);
            DataSet dataSet = new DataSet();

            try
            {
                using (sqlCommand.Connection)
                {
                    sqlCommand.Connection.Open();
                    sqlCommand.ExecuteNonQuery();
                }

                returnData.success = true;
            }
            catch (System.Exception error)
            {
                ErrorMessage errorMessage = new ErrorMessage();
                errorMessage.status = error.Source;
                errorMessage.message = error.Message;

                returnData.errors.Add(errorMessage);
                returnData.success = false;
            }

            return returnData;
        }


        public static ReturnData<NetworkLensIncident> GetIncidentTicket(String ticketNumber)
        {
            ReturnData<NetworkLensIncident> returnData = new ReturnData<NetworkLensIncident>();
            returnData.data = new NetworkLensIncident();

            SqlCommand sqlCommand = new SqlCommand("Get_networkLens_ticket");
            sqlCommand.CommandType = CommandType.StoredProcedure;

            sqlCommand.Parameters.Add("@Ticket_id", SqlDbType.VarChar).Value = ticketNumber;

            sqlCommand.Connection = new SqlConnection("Persist Security Info=False; User ID=OBIRead; Password=OB1R3@d; Initial Catalog=OBI; Server=obidb-wc-p01");

            SqlDataAdapter dataAdapter = new SqlDataAdapter(sqlCommand);
            DataSet dataSet = new DataSet();

            try
            {
                using (sqlCommand.Connection)
                {
                    sqlCommand.Connection.Open();

                    using (sqlCommand)
                    {
                        dataAdapter.Fill(dataSet, "dataSet");
                    }

                    sqlCommand.Connection.Close();

                    returnData.data.ticketId = dataSet.Tables[0].Rows[0]["TTS_ID"].ToString();
                    returnData.data.incidentWeek = dataSet.Tables[0].Rows[0]["Incident_Week"].ToString();
                    returnData.data.networkLens = dataSet.Tables[0].Rows[0]["Network_Lens"].ToString();
                    returnData.data.lensGroup = dataSet.Tables[0].Rows[0]["Lens_Group"].ToString();
                    returnData.data.rootCauseGroup = dataSet.Tables[0].Rows[0]["Root_Cause_Group"].ToString();
                    returnData.data.actualStartDate = dataSet.Tables[0].Rows[0]["TT_ACTUAL_START"].ToString();
                    returnData.data.alarmStartDate = dataSet.Tables[0].Rows[0]["TT_ALARM_START"].ToString();
                    returnData.data.workingDate = dataSet.Tables[0].Rows[0]["TTS_WORKING"].ToString();
                    returnData.data.resolvedDate = dataSet.Tables[0].Rows[0]["TTS_RESOLVED"].ToString();
                    returnData.data.causeCategory = dataSet.Tables[0].Rows[0]["TT_CAUSE_CATEGORY"].ToString();
                    returnData.data.causeDescription = dataSet.Tables[0].Rows[0]["TT_CAUSE_DESCRIPTION"].ToString();
                    returnData.data.solutionCategory = dataSet.Tables[0].Rows[0]["TT_SOLUTION_CATEGORY"].ToString();
                    returnData.data.solutionDescription = dataSet.Tables[0].Rows[0]["TT_SOLUTION_DESCRIPTION"].ToString();
                    returnData.data.problemCategory = dataSet.Tables[0].Rows[0]["TT_PROBLEM_CATEGORY"].ToString();
                    returnData.data.problemSummary = dataSet.Tables[0].Rows[0]["TT_PROBLEM_SUMMARY"].ToString();
                    returnData.data.fixemIncidentSummary = dataSet.Tables[0].Rows[0]["FIXEM_INCIDENT_SUMMARY"].ToString();
                    returnData.data.detectedBy = dataSet.Tables[0].Rows[0]["TT_DETECTED_BY"].ToString();
                    returnData.data.source = dataSet.Tables[0].Rows[0]["TT_SOURCE"].ToString();
                    returnData.data.editLensGroup = dataSet.Tables[0].Rows[0]["EDIT_Lens_Group"].ToString();
                    returnData.data.editRootCauseGroup = dataSet.Tables[0].Rows[0]["EDIT_Root_Cause_Group"].ToString();
                    returnData.data.editIncidentSummary = dataSet.Tables[0].Rows[0]["EDITIncident_Summary"].ToString();
                    returnData.data.editIncidentWeek = dataSet.Tables[0].Rows[0]["EDIT_Incident_Week"].ToString();

                    returnData.success = true;
                }
            }
            catch (System.Exception error)
            {
                ErrorMessage errorMessage = new ErrorMessage();
                errorMessage.status = error.Source;
                errorMessage.message = error.Message;

                returnData.errors.Add(errorMessage);
                returnData.success = false;
            }

            return returnData;
        }

        public static ReturnData<String> UpdateIncidentTicket(String ticketId, String editIncidentWeek, String editLensGroup, String editRootCauseGroup, String editIncidentSummary)
        {
            ReturnData<String> returnData = new ReturnData<String>();

            SqlCommand sqlCommand = new SqlCommand("Insert_Update_NetworkLens");
            sqlCommand.CommandType = CommandType.StoredProcedure;


            if (editIncidentWeek.Trim().Length == 0) editIncidentWeek = null;
            if (editLensGroup.Trim().Length == 0) editLensGroup = null;
            if (editRootCauseGroup.Trim().Length == 0) editRootCauseGroup = null;
            if (editIncidentSummary.Trim().Length == 0) editIncidentSummary = null;

            sqlCommand.Parameters.Add("@Ticket_id", SqlDbType.VarChar).Value = ticketId;
            sqlCommand.Parameters.Add("@Lens_Group", SqlDbType.VarChar).Value = editLensGroup;
            sqlCommand.Parameters.Add("@Root_Cause_Group", SqlDbType.VarChar).Value = editRootCauseGroup;
            sqlCommand.Parameters.Add("@Incident_Summary", SqlDbType.VarChar).Value = editIncidentSummary;
            sqlCommand.Parameters.Add("@Incident_Week", SqlDbType.VarChar).Value = editIncidentWeek;

            sqlCommand.Connection = new SqlConnection("Persist Security Info=False; User ID=SALPUSER; Password=OBI@dm1n; Initial Catalog=Tickets; Server=obidb-wc-p01");

            SqlDataAdapter dataAdapter = new SqlDataAdapter(sqlCommand);
            DataSet dataSet = new DataSet();

            try
            {
                using (sqlCommand.Connection)
                {
                    sqlCommand.Connection.Open();
                    sqlCommand.ExecuteNonQuery();
                }

                returnData.success = true;
            }
            catch (System.Exception error)
            {
                ErrorMessage errorMessage = new ErrorMessage();
                errorMessage.status = error.Source;
                errorMessage.message = error.Message;

                returnData.errors.Add(errorMessage);
                returnData.success = false;
            }

            return returnData;
        }

        public static ReturnData<List<Incident>> GetIncidentTickets(String week)
        {
            ReturnData<List<Incident>> returnData = new ReturnData<List<Incident>>();
            returnData.data = new List<Incident>();
            int incidentWeek = new int();

            SqlCommand sqlCommand = new SqlCommand("dbo.IncidentReview_Details");
            sqlCommand.CommandType = CommandType.StoredProcedure;

            if (week == null) incidentWeek = 1;
            if (week == "this") incidentWeek = 1;
            if (week == "last") incidentWeek = 8;
            if (week == "2weeks") incidentWeek = 15;

            sqlCommand.Parameters.Add("@week", SqlDbType.Int).Value = incidentWeek;

            sqlCommand.Connection = new SqlConnection("Persist Security Info=False; User ID=SALPUser; Password=OBI@dm1n; Initial Catalog=OBI; Server=obidb-wc-p01");

            SqlDataAdapter dataAdapter = new SqlDataAdapter(sqlCommand);
            DataSet dataSet = new DataSet();

            try
            {
                using (sqlCommand.Connection)
                {
                    sqlCommand.Connection.Open();

                    using (sqlCommand)
                    {
                        dataAdapter.Fill(dataSet, "dataSet");
                    }

                    sqlCommand.Connection.Close();

                    for (int indexRow = 0; indexRow <= dataSet.Tables[0].Rows.Count - 1; indexRow++)
                    {
                        Incident incident = new Incident();

                        incident.networkLens = dataSet.Tables[0].Rows[indexRow]["Network_Lens"].ToString();
                        incident.ttsId = dataSet.Tables[0].Rows[indexRow]["TTS_ID"].ToString();
                        incident.lensGroup = dataSet.Tables[0].Rows[indexRow]["Lens_Group"].ToString();
                        incident.rootCauseGroup = dataSet.Tables[0].Rows[indexRow]["Root_Cause_Group"].ToString();
                        incident.maintMismatch = dataSet.Tables[0].Rows[indexRow]["Maint_Mismatch"].ToString();
                        incident.ttsChangeRelated = dataSet.Tables[0].Rows[indexRow]["TTS Change Related"].ToString();
                        incident.fixEmChangeRelated = dataSet.Tables[0].Rows[indexRow]["FixEM_Change_Related"].ToString();
                        incident.ttActualStart = dataSet.Tables[0].Rows[indexRow]["TT_ACTUAL_START"].ToString();
                        incident.defectTimeIssue = dataSet.Tables[0].Rows[indexRow]["Defect Time Issue"].ToString();
                        incident.detect = dataSet.Tables[0].Rows[indexRow]["Detect"].ToString();
                        incident.ttAlarmStart = dataSet.Tables[0].Rows[indexRow]["TT_ALARM_START"].ToString();
                        incident.engageTimeIssue = dataSet.Tables[0].Rows[indexRow]["Engage Time Issue"].ToString();
                        incident.engage = dataSet.Tables[0].Rows[indexRow]["Engage"].ToString();
                        incident.ttsWorking = dataSet.Tables[0].Rows[indexRow]["TTS_WORKING"].ToString();
                        incident.resolvedTimeIssue = dataSet.Tables[0].Rows[indexRow]["Resolve Time Issue"].ToString();
                        incident.resolve = dataSet.Tables[0].Rows[indexRow]["Resolve"].ToString();
                        incident.ttActualEnd = dataSet.Tables[0].Rows[indexRow]["TT_ACTUAL_END"].ToString();
                        incident.ttCauseDescription = dataSet.Tables[0].Rows[indexRow]["TT_CAUSE_DESCRIPTION"].ToString();
                        incident.ttSolutionDescription = dataSet.Tables[0].Rows[indexRow]["TT_SOLUTION_DESCRIPTION"].ToString();
                        incident.ttProblemSummary = dataSet.Tables[0].Rows[indexRow]["TT_PROBLEM_SUMMARY"].ToString();
                        incident.incidentSummary = dataSet.Tables[0].Rows[indexRow]["Incident Summary"].ToString();
                        incident.siUserImpactCount = dataSet.Tables[0].Rows[indexRow]["SI_user_Impact_count"].ToString();

                        returnData.data.Add(incident);
                    }


                    returnData.success = true;
                }
            }
            catch (System.Exception error)
            {
                ErrorMessage errorMessage = new ErrorMessage();
                errorMessage.status = error.Source;
                errorMessage.message = error.Message;

                returnData.errors.Add(errorMessage);
                returnData.success = false;
            }

            return returnData;
        }

        public static ReturnData<TicketDetailsGeneral> GetTicketDetailsGeneral(String ticketNumber)
        {
            ReturnData<TicketDetailsGeneral> returnData = new ReturnData<TicketDetailsGeneral>();
            returnData.data = new TicketDetailsGeneral();

            SqlCommand sqlCommand = new SqlCommand("rxrSITktDetailGen");
            sqlCommand.CommandType = CommandType.StoredProcedure;

            sqlCommand.Parameters.Add("@TicketID", SqlDbType.VarChar).Value = ticketNumber;

            sqlCommand.Connection = new SqlConnection("Persist Security Info=False; User ID=OBIRead; Password=OB1R3@d; Initial Catalog=OBI; Server=obidb-wc-p01");

            SqlDataAdapter dataAdapter = new SqlDataAdapter(sqlCommand);
            DataSet dataSet = new DataSet();

            try
            {
                using (sqlCommand.Connection)
                {
                    sqlCommand.Connection.Open();

                    using (sqlCommand)
                    {
                        dataAdapter.Fill(dataSet, "dataSet");
                    }

                    sqlCommand.Connection.Close();

                    returnData.data.ticketId = dataSet.Tables[0].Rows[0]["TICKET_ID"].ToString();
                    returnData.data.incidentSummary = dataSet.Tables[0].Rows[0]["INCIDENT_SUMMARY"].ToString().Trim();
                    returnData.data.supportAreaName = dataSet.Tables[0].Rows[0]["SUPPORT_AREA_NAME"].ToString();
                    returnData.data.submittingName = dataSet.Tables[0].Rows[0]["SUBMITTING_NAME"].ToString();
                    returnData.data.queueName = dataSet.Tables[0].Rows[0]["QUEUE_NAME"].ToString();
                    returnData.data.fullName = dataSet.Tables[0].Rows[0]["FULL_NAME"].ToString();
                    returnData.data.siSeverity = dataSet.Tables[0].Rows[0]["SI_SEVERITY"].ToString();
                    returnData.data.severity = dataSet.Tables[0].Rows[0]["SEVERITY"].ToString();
                    returnData.data.siStatus = dataSet.Tables[0].Rows[0]["SI_STATUS"].ToString();
                    returnData.data.siPriority = dataSet.Tables[0].Rows[0]["SI_PRIORITY"].ToString();
                    returnData.data.priority = dataSet.Tables[0].Rows[0]["PRIORITY"].ToString();
                    returnData.data.product = dataSet.Tables[0].Rows[0]["PRODUCT"].ToString();
                    returnData.data.createDate = dataSet.Tables[0].Rows[0]["CREATE_DATE"].ToString();
                    returnData.data.assignedDate = dataSet.Tables[0].Rows[0]["ASSIGNED_DATE"].ToString();
                    returnData.data.telephonyAffected = dataSet.Tables[0].Rows[0]["TELEPHONY_AFFECTED"].ToString();
                    returnData.data.videoAffected = dataSet.Tables[0].Rows[0]["VIDEO_AFFECTED"].ToString();
                    returnData.data.hsdAffected = dataSet.Tables[0].Rows[0]["HSD_AFFECTED"].ToString();
                    returnData.data.totalCustomersAffected = dataSet.Tables[0].Rows[0]["TOTAL_CUST_AFFECTED"].ToString();
                    returnData.data.nextAction = dataSet.Tables[0].Rows[0]["NEXT_ACTION"].ToString();
                    returnData.data.problemCode = dataSet.Tables[0].Rows[0]["PROBLEM_CODE"].ToString();
                    returnData.data.source = dataSet.Tables[0].Rows[0]["SOURCE"].ToString();
                    returnData.data.category = dataSet.Tables[0].Rows[0]["CATEGORY"].ToString();
                    returnData.data.subCategory = dataSet.Tables[0].Rows[0]["SUBCATEGORY"].ToString();
                    returnData.data.causeCode = dataSet.Tables[0].Rows[0]["CAUSE_CODE"].ToString();
                    returnData.data.causeDescription = dataSet.Tables[0].Rows[0]["CAUSE_DESCRIPTION"].ToString();
                    returnData.data.causeCategory = dataSet.Tables[0].Rows[0]["CAUSE_CATEGORY"].ToString();
                    returnData.data.causeSubCategory = dataSet.Tables[0].Rows[0]["CAUSE_SUBCATEGORY"].ToString();
                    returnData.data.solutionCode = dataSet.Tables[0].Rows[0]["SOLUTION_CODE"].ToString();
                    returnData.data.solutionDescription = dataSet.Tables[0].Rows[0]["SOLUTION_DESCRIPTION"].ToString();
                    returnData.data.solutionCategory = dataSet.Tables[0].Rows[0]["SOLUTION_CATEGORY"].ToString();
                    returnData.data.solutionSubCategory = dataSet.Tables[0].Rows[0]["SOLUTION_SUBCATEGORY"].ToString();
                    returnData.data.externalAssignee = dataSet.Tables[0].Rows[0]["EXTERNAL_ASSIGNEE"].ToString();
                    returnData.data.externalReference = dataSet.Tables[0].Rows[0]["EXTERNAL_REFERENCE"].ToString();
                    returnData.data.alarmDate = dataSet.Tables[0].Rows[0]["ALARM_DATE"].ToString();
                    returnData.data.ttrStart = dataSet.Tables[0].Rows[0]["TTR_START"].ToString();
                    returnData.data.ttrStop = dataSet.Tables[0].Rows[0]["TTR_STOP"].ToString();
                    returnData.data.indicatorCity = dataSet.Tables[0].Rows[0]["INDICATOR_CITY"].ToString();
                    returnData.data.indicatorState = dataSet.Tables[0].Rows[0]["INDICATOR_STATE"].ToString();
                    returnData.data.networkElement = dataSet.Tables[0].Rows[0]["NETWORK_ELEMENT"].ToString();
                    returnData.data.correlationType = dataSet.Tables[0].Rows[0]["CORRELATION_TYPE"].ToString();
                    returnData.data.serviceCondition = dataSet.Tables[0].Rows[0]["SERVICE_CONDITION"].ToString();
                    returnData.data.careImpact = dataSet.Tables[0].Rows[0]["CARE_IMPACT"].ToString();
                    returnData.data.estimatedServiceRestore = dataSet.Tables[0].Rows[0]["ESTIMATED_SERVICE_RESTORE"].ToString();
                    returnData.data.siLastModifiedBy = dataSet.Tables[0].Rows[0]["SI_LAST_MODIFIED_BY"].ToString();
                    returnData.data.lastModifiedBy = dataSet.Tables[0].Rows[0]["LAST_MODIFIED_BY"].ToString();
                    returnData.data.modifiedDate = dataSet.Tables[0].Rows[0]["MODIFIED_DATE"].ToString();
                    returnData.data.siResolvedBy = dataSet.Tables[0].Rows[0]["SI_RESOLVED_BY"].ToString();
                    returnData.data.resolvedBy = dataSet.Tables[0].Rows[0]["RESOLVED_BY"].ToString();
                    returnData.data.contactDepartment = dataSet.Tables[0].Rows[0]["CONTACT_DEPARTMENT"].ToString();
                    returnData.data.contactPhone = dataSet.Tables[0].Rows[0]["CONTACT_PHONE"].ToString();
                    returnData.data.regionName = dataSet.Tables[0].Rows[0]["REGION_NAME"].ToString();
                    returnData.data.duration = dataSet.Tables[0].Rows[0]["DURATION"].ToString();


                    returnData.success = true;
                }
            }
            catch (System.Exception error)
            {
                ErrorMessage errorMessage = new ErrorMessage();
                errorMessage.status = error.Source;
                errorMessage.message = error.Message;

                returnData.errors.Add(errorMessage);
                returnData.success = false;
            }

            return returnData;
        }


        public static ReturnData<TicketDetailsAttributes> GetTicketDetailsAttributes(String ticketNumber)
        {
            ReturnData<TicketDetailsAttributes> returnData = new ReturnData<TicketDetailsAttributes>();
            returnData.data = new TicketDetailsAttributes();

            SqlCommand sqlCommand = new SqlCommand("rxrSITktDetailDataEl");
            sqlCommand.CommandType = CommandType.StoredProcedure;

            sqlCommand.Parameters.Add("@TicketID", SqlDbType.VarChar).Value = ticketNumber;

            sqlCommand.Connection = new SqlConnection("Persist Security Info=False; User ID=OBIRead; Password=OB1R3@d; Initial Catalog=OBI; Server=obidb-wc-p01");

            SqlDataAdapter dataAdapter = new SqlDataAdapter(sqlCommand);
            DataSet dataSet = new DataSet();

            try
            {
                using (sqlCommand.Connection)
                {
                    sqlCommand.Connection.Open();

                    using (sqlCommand)
                    {
                        dataAdapter.Fill(dataSet, "dataSet");
                    }

                    sqlCommand.Connection.Close();

                    for (int indexRow = 0; indexRow <= dataSet.Tables[0].Rows.Count - 1; indexRow++)
                    {
                        String currentAttribute = dataSet.Tables[0].Rows[indexRow]["ATTR_LABEL"].ToString();
                        String currentValue = dataSet.Tables[0].Rows[indexRow]["VALUE_CHAR"].ToString();

                        switch (currentAttribute)
                        {
                            case "Contact Info Department":
                                returnData.data.contactInfoDepartment = currentValue;
                                break;

                            case "Contact Info Phone":
                                returnData.data.contactInfoPhone = currentValue;
                                break;

                            case "Indicator State":
                                returnData.data.indicatorState = currentValue;
                                break;

                            case "Management Bridge":
                                returnData.data.managementBridge = currentValue;
                                break;

                            case "Detected By":
                                returnData.data.detectedBy = currentValue;
                                break;

                            case "Outage Description":
                                returnData.data.outageDescription = currentValue;
                                break;

                            case "Customer Experience":
                                returnData.data.customerExperience = currentValue;
                                break;

                            case "Indicator City":
                                returnData.data.indicatorCity = currentValue;
                                break;

                            case "Technical Bridge":
                                returnData.data.technicalBridge = currentValue;
                                break;
                        }
                    }

                    returnData.success = true;
                }
            }
            catch (System.Exception error)
            {
                ErrorMessage errorMessage = new ErrorMessage();
                errorMessage.status = error.Source;
                errorMessage.message = error.Message;

                returnData.errors.Add(errorMessage);
                returnData.success = false;
            }

            return returnData;
        }


        public static ReturnData<List<CustomerTicket>> GetCustomerTickets(String ticketNumber)
        {
            ReturnData<List<CustomerTicket>> returnData = new ReturnData<List<CustomerTicket>>();
            returnData.data = new List<CustomerTicket>();

            SqlCommand sqlCommand = new SqlCommand("dbo.rxrSITktDetailCust");
            sqlCommand.CommandType = CommandType.StoredProcedure;

            sqlCommand.Parameters.Add("@TicketID", SqlDbType.VarChar).Value = ticketNumber;

            sqlCommand.Connection = new SqlConnection("Persist Security Info=False; User ID=SALPUser; Password=OBI@dm1n; Initial Catalog=OBI; Server=obidb-wc-p01");

            SqlDataAdapter dataAdapter = new SqlDataAdapter(sqlCommand);
            DataSet dataSet = new DataSet();

            try
            {
                using (sqlCommand.Connection)
                {
                    sqlCommand.Connection.Open();

                    using (sqlCommand)
                    {
                        dataAdapter.Fill(dataSet, "dataSet");
                    }

                    sqlCommand.Connection.Close();

                    for (int indexRow = 0; indexRow <= dataSet.Tables[0].Rows.Count - 1; indexRow++)
                    {
                        CustomerTicket customerTicket = new CustomerTicket();

                        customerTicket.customerTicketId = dataSet.Tables[0].Rows[indexRow]["CUST_TICKET_ID"].ToString();
                        customerTicket.problemSummary = dataSet.Tables[0].Rows[indexRow]["PROBLEM_SUMMARY"].ToString();
                        customerTicket.createDate = dataSet.Tables[0].Rows[indexRow]["CREATE_DATE"].ToString();
                        customerTicket.contactName = dataSet.Tables[0].Rows[indexRow]["CONTACT_NAME"].ToString();
                        customerTicket.status = dataSet.Tables[0].Rows[indexRow]["STATUS"].ToString();
                        customerTicket.node = dataSet.Tables[0].Rows[indexRow]["NODE"].ToString();

                        returnData.data.Add(customerTicket);
                    }


                    returnData.success = true;
                }
            }
            catch (System.Exception error)
            {
                ErrorMessage errorMessage = new ErrorMessage();
                errorMessage.status = error.Source;
                errorMessage.message = error.Message;

                returnData.errors.Add(errorMessage);
                returnData.success = false;
            }

            return returnData;
        }

        public static ReturnData<List<TicketDetailsAffected>> GetTicketDetailsAffected(String ticketNumber)
        {
            ReturnData<List<TicketDetailsAffected>> returnData = new ReturnData<List<TicketDetailsAffected>>();
            returnData.data = new List<TicketDetailsAffected>();

            SqlCommand sqlCommand = new SqlCommand("rxrSITktDetailAffEle");
            sqlCommand.CommandType = CommandType.StoredProcedure;

            sqlCommand.Parameters.Add("@TicketID", SqlDbType.VarChar).Value = ticketNumber;

            sqlCommand.Connection = new SqlConnection("Persist Security Info=False; User ID=OBIRead; Password=OB1R3@d; Initial Catalog=OBI; Server=obidb-wc-p01");

            SqlDataAdapter dataAdapter = new SqlDataAdapter(sqlCommand);
            DataSet dataSet = new DataSet();

            try
            {
                using (sqlCommand.Connection)
                {
                    sqlCommand.Connection.Open();

                    using (sqlCommand)
                    {
                        dataAdapter.Fill(dataSet, "dataSet");
                    }

                    sqlCommand.Connection.Close();

                    for (int indexRow = 0; indexRow <= dataSet.Tables[0].Rows.Count - 1; indexRow++)
                    {
                        TicketDetailsAffected detailsAffected = new TicketDetailsAffected();

                        detailsAffected.entryId = dataSet.Tables[0].Rows[0]["ENTRY_ID"].ToString();
                        detailsAffected.serviceAffected = dataSet.Tables[0].Rows[0]["SERVICE_AFFECT"].ToString().Trim();
                        detailsAffected.elementName = dataSet.Tables[0].Rows[0]["ELEMENT_NAME"].ToString();
                        detailsAffected.elementType = dataSet.Tables[0].Rows[0]["ELEMENT_TYPE"].ToString();
                        detailsAffected.systemName = dataSet.Tables[0].Rows[0]["SYSTEM_NAME"].ToString();
                        detailsAffected.telephonyAffectedSubscriptions = dataSet.Tables[0].Rows[0]["TELEPHONY_AFF_SUBS"].ToString();
                        detailsAffected.videoAffectedSubscriptions = dataSet.Tables[0].Rows[0]["VIDEO_AFF_SUBS"].ToString();
                        detailsAffected.hsdAffectedSubscriptions = dataSet.Tables[0].Rows[0]["HSD_AFF_SUBS"].ToString();
                        detailsAffected.totalAffectedSubscriptions = dataSet.Tables[0].Rows[0]["TOTAL_AFF_SUBS"].ToString();
                        detailsAffected.aeStatus = dataSet.Tables[0].Rows[0]["AE_STATUS"].ToString();
                        detailsAffected.actualStart = dataSet.Tables[0].Rows[0]["ACTUAL_START"].ToString();
                        detailsAffected.actualEnd = dataSet.Tables[0].Rows[0]["ACTUAL_END"].ToString();

                        returnData.data.Add(detailsAffected);
                    }

                    returnData.success = true;
                }
            }
            catch (System.Exception error)
            {
                ErrorMessage errorMessage = new ErrorMessage();
                errorMessage.status = error.Source;
                errorMessage.message = error.Message;

                returnData.errors.Add(errorMessage);
                returnData.success = false;
            }

            return returnData;
        }

        public static ReturnData<List<WorkLogTicket>> GetWorkLogTickets(String ticketNumber, String source)
        {
            ReturnData<List<WorkLogTicket>> returnData = new ReturnData<List<WorkLogTicket>>();
            returnData.data = new List<WorkLogTicket>();

            SqlCommand sqlCommand = new SqlCommand("rxrSITktDetailWorklog");
            sqlCommand.CommandType = CommandType.StoredProcedure;

            sqlCommand.Parameters.Add("@TicketID", SqlDbType.VarChar).Value = ticketNumber;
            sqlCommand.Parameters.Add("@Source", SqlDbType.VarChar).Value = source;

            sqlCommand.Connection = new SqlConnection("Persist Security Info=False; User ID=OBIRead; Password=OB1R3@d; Initial Catalog=OBI; Server=obidb-wc-p01");

            SqlDataAdapter dataAdapter = new SqlDataAdapter(sqlCommand);
            DataSet dataSet = new DataSet();

            try
            {
                using (sqlCommand.Connection)
                {
                    sqlCommand.Connection.Open();

                    using (sqlCommand)
                    {
                        dataAdapter.Fill(dataSet, "dataSet");
                    }

                    sqlCommand.Connection.Close();

                    for (int indexRow = 0; indexRow <= dataSet.Tables[0].Rows.Count - 1; indexRow++)
                    {
                        WorkLogTicket workLogTicket = new WorkLogTicket();

                        workLogTicket.ticketNumber = ticketNumber;
                        workLogTicket.createDate = dataSet.Tables[0].Rows[indexRow]["CREATE_DATE"].ToString();
                        workLogTicket.submitter = dataSet.Tables[0].Rows[indexRow]["SUBMITTER"].ToString();
                        workLogTicket.source = dataSet.Tables[0].Rows[indexRow]["SOURCE"].ToString();
                        workLogTicket.subject = dataSet.Tables[0].Rows[indexRow]["SUBJECT"].ToString();
                        workLogTicket.details = dataSet.Tables[0].Rows[indexRow]["DETAILS"].ToString();

                        returnData.data.Add(workLogTicket);
                    }

                    returnData.success = true;
                }
            }
            catch (System.Exception error)
            {
                ErrorMessage errorMessage = new ErrorMessage();
                errorMessage.status = error.Source;
                errorMessage.message = error.Message;

                returnData.errors.Add(errorMessage);
                returnData.success = false;
            }

            return returnData;
        }


        public static ReturnData<TicketTimeline> GetTicketTimeline(String ticketNumber)
        {
            ReturnData<TicketTimeline> returnData = new ReturnData<TicketTimeline>();
            returnData.data = new TicketTimeline();

            SqlCommand sqlCommand = new SqlCommand("Get_Bandaid_ticket");
            sqlCommand.CommandType = CommandType.StoredProcedure;

            sqlCommand.Parameters.Add("@Ticket_id", SqlDbType.VarChar).Value = ticketNumber;

            sqlCommand.Connection = new SqlConnection("Persist Security Info=False; User ID=OBIRead; Password=OB1R3@d; Initial Catalog=Tickets; Server=obidb-wc-p01");

            SqlDataAdapter dataAdapter = new SqlDataAdapter(sqlCommand);
            DataSet dataSet = new DataSet();

            try
            {
                using (sqlCommand.Connection)
                {
                    sqlCommand.Connection.Open();

                    using (sqlCommand)
                    {
                        dataAdapter.Fill(dataSet, "dataSet");
                    }

                    sqlCommand.Connection.Close();

                    returnData.data.ticketId = dataSet.Tables[0].Rows[0]["tt_Ticket_id"].ToString();

                    returnData.data.alarmStartDate = dataSet.Tables[0].Rows[0]["TT_ALARM_START"].ToString();
                    returnData.data.workingStartDate = dataSet.Tables[0].Rows[0]["tts_working"].ToString();
                    returnData.data.resolvedDate = dataSet.Tables[0].Rows[0]["tts_RESOLVED"].ToString();
                    returnData.data.actualStartDate = dataSet.Tables[0].Rows[0]["TT_ACTUAL_START"].ToString();
                    returnData.data.actualEndDate = dataSet.Tables[0].Rows[0]["tt_ACTUAL_END"].ToString();

                    returnData.success = true;
                }
            }
            catch (System.Exception error)
            {
                ErrorMessage errorMessage = new ErrorMessage();
                errorMessage.status = error.Source;
                errorMessage.message = error.Message;

                returnData.errors.Add(errorMessage);
                returnData.success = false;
            }

            return returnData;
        }


        public static ReturnData<List<WorkLogNotification>> GetWorkLogNotifications(String ticketNumber, String eventType)
        {
            ReturnData<List<WorkLogNotification>> returnData = new ReturnData<List<WorkLogNotification>>();
            returnData.data = new List<WorkLogNotification>();

            SqlCommand sqlCommand = new SqlCommand("dbo.rxrSITktNotification");
            sqlCommand.CommandType = CommandType.StoredProcedure;

            String ticketType = eventType.Substring(0, 1);

            switch (ticketType)
            {
                case "CM":
                    sqlCommand = new SqlCommand("dbo.rxrCMTktDetailWorklog");
                    break;
            }

            sqlCommand.Parameters.Add("@TicketID", SqlDbType.VarChar).Value = ticketNumber;
            sqlCommand.Parameters.Add("@Event_type", SqlDbType.VarChar).Value = eventType;

            sqlCommand.Connection = new SqlConnection("Persist Security Info=False; User ID=OBIRead; Password=OB1R3@d; Initial Catalog=OBI; Server=obidb-wc-p01");

            SqlDataAdapter dataAdapter = new SqlDataAdapter(sqlCommand);
            DataSet dataSet = new DataSet();

            try
            {
                using (sqlCommand.Connection)
                {
                    sqlCommand.Connection.Open();

                    using (sqlCommand)
                    {
                        dataAdapter.Fill(dataSet, "dataSet");
                    }

                    sqlCommand.Connection.Close();

                    for (int indexRow = 0; indexRow <= dataSet.Tables[0].Rows.Count - 1; indexRow++)
                    {
                        WorkLogNotification notification = new WorkLogNotification();
                        XmlDocument messageBody = new XmlDocument();
                        String messageBodyXML = dataSet.Tables[0].Rows[indexRow]["MESSAGE_BODY"].ToString();
                        String messageBodyText = "";

                        messageBody.LoadXml(messageBodyXML);

                        foreach (XmlNode node in messageBody.DocumentElement.ChildNodes)
                        {
                            messageBodyText += "<b>" + node.Name + ": </b>" + node.InnerText + "<br/>";
                        }

                        notification.logId = dataSet.Tables[0].Rows[indexRow]["NOTIFICATION_LOG_ID"].ToString();
                        notification.message = messageBodyText;

                        returnData.data.Add(notification);

                    }

                    returnData.success = true;
                }
            }
            catch (System.Exception error)
            {
                ErrorMessage errorMessage = new ErrorMessage();
                errorMessage.status = error.Source;
                errorMessage.message = error.Message;

                returnData.errors.Add(errorMessage);
                returnData.success = false;
            }

            return returnData;
        }


        public static ReturnData<String> AddUserFavoriteLink(String accountName, String description, String link, String group)
        {
            ReturnData<String> returnData = new ReturnData<String>();

            SqlCommand sqlCommand = new SqlCommand("dbo.Create_User_fav_link");
            sqlCommand.CommandType = CommandType.StoredProcedure;


            sqlCommand.Parameters.Add("@user_id", SqlDbType.VarChar).Value = accountName;
            sqlCommand.Parameters.Add("@Link_desc", SqlDbType.VarChar).Value = description;
            sqlCommand.Parameters.Add("@link", SqlDbType.VarChar).Value = link;
            sqlCommand.Parameters.Add("@Group_Name", SqlDbType.VarChar).Value = group;
            sqlCommand.Parameters.Add("@Display ", SqlDbType.Bit).Value = 1;

            sqlCommand.Connection = new SqlConnection("Persist Security Info=False; User ID=salpuser; Password=5@lpuser; Initial Catalog=SALP; Server=epssadb-po-01p");

            SqlDataAdapter dataAdapter = new SqlDataAdapter(sqlCommand);
            DataSet dataSet = new DataSet();

            try
            {
                using (sqlCommand.Connection)
                {
                    sqlCommand.Connection.Open();
                    sqlCommand.ExecuteNonQuery();
                }

                returnData.success = true;
            }
            catch (System.Exception error)
            {
                ErrorMessage errorMessage = new ErrorMessage();
                errorMessage.status = error.Source;
                errorMessage.message = error.Message;

                returnData.errors.Add(errorMessage);
                returnData.success = false;
            }

            return returnData;
        }


        public static ReturnData<List<UserFavoriteLink>> GetUserFavoriteLinks(String accountName)
        {
            ReturnData<List<UserFavoriteLink>> returnData = new ReturnData<List<UserFavoriteLink>>();
            returnData.data = new List<UserFavoriteLink>();

            SqlCommand sqlCommand = new SqlCommand("dbo.Get_User_fav_link");
            sqlCommand.CommandType = CommandType.StoredProcedure;

            sqlCommand.Parameters.Add("@user_id", SqlDbType.VarChar).Value = accountName;

            sqlCommand.Connection = new SqlConnection("Persist Security Info=False; User ID=salpuser; Password=5@lpuser; Initial Catalog=SALP; Server=epssadb-po-01p");

            SqlDataAdapter dataAdapter = new SqlDataAdapter(sqlCommand);
            DataSet dataSet = new DataSet();

            try
            {
                using (sqlCommand.Connection)
                {
                    sqlCommand.Connection.Open();

                    using (sqlCommand)
                    {
                        dataAdapter.Fill(dataSet, "dataSet");
                    }

                    sqlCommand.Connection.Close();

                    for (int indexRow = 0; indexRow <= dataSet.Tables[0].Rows.Count - 1; indexRow++)
                    {
                        UserFavoriteLink link = new UserFavoriteLink();

                        link.linkId = dataSet.Tables[0].Rows[indexRow]["Link_id"].ToString();
                        link.description = dataSet.Tables[0].Rows[indexRow]["Link_desc"].ToString();
                        link.link = dataSet.Tables[0].Rows[indexRow]["link"].ToString();
                        link.group = dataSet.Tables[0].Rows[indexRow]["Group_Name"].ToString();

                        returnData.data.Add(link);
                    }

                    returnData.success = true;
                }
            }
            catch (System.Exception error)
            {
                ErrorMessage errorMessage = new ErrorMessage();
                errorMessage.status = error.Source;
                errorMessage.message = error.Message;

                returnData.errors.Add(errorMessage);
                returnData.success = false;
            }

            return returnData;
        }


        public static ReturnData<String> EditUserFavoriteLink(String linkId, String accountName, String description, String link, String group)
        {
            ReturnData<String> returnData = new ReturnData<String>();

            SqlCommand sqlCommand = new SqlCommand("update_User_fav_link");
            sqlCommand.CommandType = CommandType.StoredProcedure;


            sqlCommand.Parameters.Add("@Link_id", SqlDbType.VarChar).Value = linkId;
            sqlCommand.Parameters.Add("@user_id", SqlDbType.VarChar).Value = accountName;
            sqlCommand.Parameters.Add("@Link_desc", SqlDbType.VarChar).Value = description;
            sqlCommand.Parameters.Add("@link", SqlDbType.VarChar).Value = link;
            sqlCommand.Parameters.Add("@Group_Name", SqlDbType.VarChar).Value = group;
            sqlCommand.Parameters.Add("@Display ", SqlDbType.Bit).Value = 1;

            sqlCommand.Connection = new SqlConnection("Persist Security Info=False; User ID=salpuser; Password=5@lpuser; Initial Catalog=SALP; Server=epssadb-po-01p");

            SqlDataAdapter dataAdapter = new SqlDataAdapter(sqlCommand);
            DataSet dataSet = new DataSet();

            try
            {
                using (sqlCommand.Connection)
                {
                    sqlCommand.Connection.Open();
                    sqlCommand.ExecuteNonQuery();
                }

                returnData.success = true;
            }
            catch (System.Exception error)
            {
                ErrorMessage errorMessage = new ErrorMessage();
                errorMessage.status = error.Source;
                errorMessage.message = error.Message;

                returnData.errors.Add(errorMessage);
                returnData.success = false;
            }

            return returnData;
        }


        public static ReturnData<String> DeleteUserFavoriteLink(String linkId)
        {
            ReturnData<String> returnData = new ReturnData<String>();

            SqlCommand sqlCommand = new SqlCommand("dbo.Delete_User_fav_link");
            sqlCommand.CommandType = CommandType.StoredProcedure;


            sqlCommand.Parameters.Add("@Link_id", SqlDbType.VarChar).Value = linkId;

            sqlCommand.Connection = new SqlConnection("Persist Security Info=False; User ID=salpuser; Password=5@lpuser; Initial Catalog=SALP; Server=epssadb-po-01p");

            SqlDataAdapter dataAdapter = new SqlDataAdapter(sqlCommand);
            DataSet dataSet = new DataSet();

            try
            {
                using (sqlCommand.Connection)
                {
                    sqlCommand.Connection.Open();
                    sqlCommand.ExecuteNonQuery();
                }

                returnData.success = true;
            }
            catch (System.Exception error)
            {
                ErrorMessage errorMessage = new ErrorMessage();
                errorMessage.status = error.Source;
                errorMessage.message = error.Message;

                returnData.errors.Add(errorMessage);
                returnData.success = false;
            }

            return returnData;
        }

        public static ReturnData<List<SITicketTimeline>> GetSITicketTimeline(String ticketNumber)
        {
            ReturnData<List<SITicketTimeline>> returnData = new ReturnData<List<SITicketTimeline>>();
            returnData.data = new List<SITicketTimeline>();

            SqlCommand sqlCommand = new SqlCommand("Get_SI_timeline");
            sqlCommand.CommandType = CommandType.StoredProcedure;

            sqlCommand.Parameters.Add("@Ticket_id", SqlDbType.VarChar).Value = ticketNumber;

            sqlCommand.Connection = new SqlConnection("Persist Security Info=False; User ID=salpuser; Password=5@lpuser; Initial Catalog=SALP; Server=epssadb-po-01p");

            SqlDataAdapter dataAdapter = new SqlDataAdapter(sqlCommand);
            DataSet dataSet = new DataSet();

            try
            {
                using (sqlCommand.Connection)
                {
                    sqlCommand.Connection.Open();

                    using (sqlCommand)
                    {
                        dataAdapter.Fill(dataSet, "dataSet");
                    }

                    sqlCommand.Connection.Close();

                    for (int indexRow = 0; indexRow <= dataSet.Tables[0].Rows.Count - 1; indexRow++)
                    {
                        SITicketTimeline timeline = new SITicketTimeline();

                        int durationNeg = int.Parse(dataSet.Tables[0].Rows[indexRow]["DurationNeg"].ToString());

                        timeline.ticketNumber = dataSet.Tables[0].Rows[indexRow]["tt_ticket_id"].ToString();
                        timeline.name = dataSet.Tables[0].Rows[indexRow]["SegName"].ToString();
                        timeline.start = dataSet.Tables[0].Rows[indexRow]["Start"].ToString();
                        timeline.duration = dataSet.Tables[0].Rows[indexRow]["Duration"].ToString();
                        timeline.durationNeg = dataSet.Tables[0].Rows[indexRow]["DurationNeg"].ToString();

                        returnData.data.Add(timeline);
                    }

                    returnData.success = true;
                }
            }
            catch (System.Exception error)
            {
                ErrorMessage errorMessage = new ErrorMessage();
                errorMessage.status = error.Source;
                errorMessage.message = error.Message;

                returnData.errors.Add(errorMessage);
                returnData.success = false;
            }

            return returnData;
        }

        public static ReturnData<List<ConflictTicket>> GetConflictTickets(String months)
        {
            ReturnData<List<ConflictTicket>> returnData = new ReturnData<List<ConflictTicket>>();
            returnData.data = new List<ConflictTicket>();

            SqlCommand sqlCommand = new SqlCommand("dbo.GetConflictTickets_bySubmitter");
            sqlCommand.CommandType = CommandType.StoredProcedure;
            sqlCommand.CommandTimeout = 600;

            sqlCommand.Connection = new SqlConnection("Persist Security Info=False; User ID=OBIRead; Password=OB1R3@d; Initial Catalog=iJobs; Server=obidb-wc-p01");

            SqlDataAdapter dataAdapter = new SqlDataAdapter(sqlCommand);
            DataSet dataSet = new DataSet();

            try
            {
                using (sqlCommand.Connection)
                {
                    sqlCommand.Connection.Open();

                    using (sqlCommand)
                    {
                        dataAdapter.Fill(dataSet, "dataSet");
                    }

                    sqlCommand.Connection.Close();

                    for (int indexRow = 0; indexRow <= dataSet.Tables[0].Rows.Count - 1; indexRow++)
                    {
                        ConflictTicket conflictTicket = new ConflictTicket();

                        DateTime currentDateTime = DateTime.Parse(dataSet.Tables[0].Rows[indexRow]["CREATE_MONTH_GMT"].ToString());

                        String currentMonth = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(currentDateTime.Month);

                        int test = months.IndexOf(currentMonth);

                        if (months.IndexOf(currentMonth) >= 0)
                        {
                            conflictTicket.submitterName = dataSet.Tables[0].Rows[indexRow]["SUBMITTER_NAME"].ToString();
                            conflictTicket.supervisorName = dataSet.Tables[0].Rows[indexRow]["SUPERVISOR_NAME"].ToString();
                            conflictTicket.bypassed = dataSet.Tables[0].Rows[indexRow]["BYPASSED_COUNT"].ToString();
                            conflictTicket.notBypassed = dataSet.Tables[0].Rows[indexRow]["NOT_BYPASSED_COUNT"].ToString();
                            conflictTicket.total = dataSet.Tables[0].Rows[indexRow]["TOTAL_COUNT"].ToString();

                            float percentNotBypassed = float.Parse(dataSet.Tables[0].Rows[indexRow]["PCT_NOT_BYPASSED"].ToString()) * 100;
                            conflictTicket.percentNotBypassed = percentNotBypassed.ToString("0.00");

                            if (percentNotBypassed > 38.89)
                            {
                                returnData.data.Add(conflictTicket);
                            }
                        }
                    }

                    returnData.success = true;
                }
            }
            catch (System.Exception error)
            {
                ErrorMessage errorMessage = new ErrorMessage();
                errorMessage.status = error.Source;
                errorMessage.message = error.Message;

                returnData.errors.Add(errorMessage);
                returnData.success = false;
            }

            return returnData;
        }

        public static ReturnData<List<UserFavoriteLink>> GetAllUserFavoriteLinks(String accountName)
        {
            ReturnData<List<UserFavoriteLink>> returnData = new ReturnData<List<UserFavoriteLink>>();
            returnData.data = new List<UserFavoriteLink>();

            SqlCommand sqlCommand = new SqlCommand("dbo.Get_User_fav_link");
            sqlCommand.CommandType = CommandType.StoredProcedure;

            sqlCommand.Parameters.Add("@user_id", SqlDbType.VarChar).Value = accountName;
            sqlCommand.Parameters.Add("@ALL", SqlDbType.VarChar).Value = "All";

            sqlCommand.Connection = new SqlConnection("Persist Security Info=False; User ID=salpuser; Password=5@lpuser; Initial Catalog=SALP; Server=epssadb-po-01p");

            SqlDataAdapter dataAdapter = new SqlDataAdapter(sqlCommand);
            DataSet dataSet = new DataSet();

            try
            {
                using (sqlCommand.Connection)
                {
                    sqlCommand.Connection.Open();

                    using (sqlCommand)
                    {
                        dataAdapter.Fill(dataSet, "dataSet");
                    }

                    sqlCommand.Connection.Close();

                    for (int indexRow = 0; indexRow <= dataSet.Tables[0].Rows.Count - 1; indexRow++)
                    {
                        UserFavoriteLink link = new UserFavoriteLink();

                        link.linkId = dataSet.Tables[0].Rows[indexRow]["Link_id"].ToString();
                        link.description = dataSet.Tables[0].Rows[indexRow]["Link_desc"].ToString();
                        link.link = dataSet.Tables[0].Rows[indexRow]["link"].ToString();
                        link.group = dataSet.Tables[0].Rows[indexRow]["Group_Name"].ToString();
                        link.display = dataSet.Tables[0].Rows[indexRow]["Display"].ToString();

                        returnData.data.Add(link);
                    }

                    returnData.success = true;
                }
            }
            catch (System.Exception error)
            {
                ErrorMessage errorMessage = new ErrorMessage();
                errorMessage.status = error.Source;
                errorMessage.message = error.Message;

                returnData.errors.Add(errorMessage);
                returnData.success = false;
            }

            return returnData;
        }

        public static ReturnData<String> SaveUserFavoriteLinks(String accountName, String selectionList)
        {
            ReturnData<String> returnData = new ReturnData<String>();

            SqlCommand sqlCommand = new SqlCommand("dbo.Update_User_fav_link_BULK");
            sqlCommand.CommandType = CommandType.StoredProcedure;


            sqlCommand.Parameters.Add("@NT_Login", SqlDbType.VarChar).Value = accountName;
            sqlCommand.Parameters.Add("@Link_ID", SqlDbType.VarChar).Value = selectionList;

            sqlCommand.Connection = new SqlConnection("Persist Security Info=False; User ID=salpuser; Password=5@lpuser; Initial Catalog=SALP; Server=epssadb-po-01p");

            SqlDataAdapter dataAdapter = new SqlDataAdapter(sqlCommand);
            DataSet dataSet = new DataSet();

            try
            {
                using (sqlCommand.Connection)
                {
                    sqlCommand.Connection.Open();
                    sqlCommand.ExecuteNonQuery();
                }

                returnData.success = true;
            }
            catch (System.Exception error)
            {
                ErrorMessage errorMessage = new ErrorMessage();
                errorMessage.status = error.Source;
                errorMessage.message = error.Message;

                returnData.errors.Add(errorMessage);
                returnData.success = false;
            }

            return returnData;
        }

        public static ReturnData<List<IncidentReview>> GetNetworkOperationsIncidentReview()
        {
            ReturnData<List<IncidentReview>> returnData = new ReturnData<List<IncidentReview>>();
            returnData.data = new List<IncidentReview>();

            SqlCommand sqlCommand = new SqlCommand("dbo.IncidentReview_Current");
            sqlCommand.CommandType = CommandType.StoredProcedure;

            sqlCommand.Connection = new SqlConnection("Persist Security Info=False; User ID=OBIRead; Password=OB1R3@d; Initial Catalog=OBI; Server=obidb-wc-p01");

            SqlDataAdapter dataAdapter = new SqlDataAdapter(sqlCommand);
            DataSet dataSet = new DataSet();

            try
            {
                using (sqlCommand.Connection)
                {
                    sqlCommand.Connection.Open();

                    using (sqlCommand)
                    {
                        dataAdapter.Fill(dataSet, "dataSet");
                    }

                    sqlCommand.Connection.Close();

                    for (int indexRow = 0; indexRow <= dataSet.Tables[0].Rows.Count - 1; indexRow++)
                    {
                        IncidentReview review = new IncidentReview();

                        review.label = dataSet.Tables[0].Rows[indexRow]["Label"].ToString();
                        review.mtbf = dataSet.Tables[0].Rows[indexRow]["MTBF"].ToString();
                        review.frequency = dataSet.Tables[0].Rows[indexRow]["Frequency"].ToString();
                        review.wowFrequency = dataSet.Tables[0].Rows[indexRow]["WOWFreq"].ToString();
                        review.changeRelated = dataSet.Tables[0].Rows[indexRow]["Change Related"].ToString();
                        review.detect = dataSet.Tables[0].Rows[indexRow]["Detect"].ToString();
                        review.engage = dataSet.Tables[0].Rows[indexRow]["Engage"].ToString();
                        review.resolve = dataSet.Tables[0].Rows[indexRow]["Resolve"].ToString();
                        review.wowResolve = dataSet.Tables[0].Rows[indexRow]["WOWResolve"].ToString();
                        review.config = dataSet.Tables[0].Rows[indexRow]["Config"].ToString();
                        review.hwSw = dataSet.Tables[0].Rows[indexRow]["HW/SW"].ToString();
                        review.asDesign = dataSet.Tables[0].Rows[indexRow]["As Design"].ToString();
                        review.executionDefect = dataSet.Tables[0].Rows[indexRow]["Execution Defect"].ToString();
                        review.impact = dataSet.Tables[0].Rows[indexRow]["Impact"].ToString();

                        returnData.data.Add(review);
                    }

                    returnData.success = true;
                }
            }
            catch (System.Exception error)
            {
                ErrorMessage errorMessage = new ErrorMessage();
                errorMessage.status = error.Source;
                errorMessage.message = error.Message;

                returnData.errors.Add(errorMessage);
                returnData.success = false;
            }

            return returnData;
        }



        public static ReturnData<List<WeeklyReviewMeasure>> GetWeeklyReviewMeasures(String lens, String lensGroup)
        {
            ReturnData<List<WeeklyReviewMeasure>> returnData = new ReturnData<List<WeeklyReviewMeasure>>();
            returnData.data = new List<WeeklyReviewMeasure>();

            SqlCommand sqlCommand = new SqlCommand("dbo.NetworkQuality_Weekly_Review_Measures_SELECT");
            sqlCommand.CommandType = CommandType.StoredProcedure;


            sqlCommand.Parameters.Add("@Lens", SqlDbType.VarChar).Value = lens;
            sqlCommand.Parameters.Add("@LensGroup", SqlDbType.VarChar).Value = lensGroup;

            sqlCommand.Connection = new SqlConnection("Persist Security Info=False; User ID=OBIRead; Password=OB1R3@d; Initial Catalog=OBI; Server=obidb-wc-p01");

            SqlDataAdapter dataAdapter = new SqlDataAdapter(sqlCommand);
            DataSet dataSet = new DataSet();


            try
            {
                using (sqlCommand.Connection)
                {
                    sqlCommand.Connection.Open();

                    using (sqlCommand)
                    {
                        dataAdapter.Fill(dataSet, "dataSet");
                    }

                    sqlCommand.Connection.Close();

                    for (int indexRow = 0; indexRow <= dataSet.Tables[0].Rows.Count - 1; indexRow++)
                    {
                        WeeklyReviewMeasure measure = new WeeklyReviewMeasure();

                        DateTime weekStarting = DateTime.Parse(dataSet.Tables[0].Rows[indexRow]["WeekStarting"].ToString());

                        measure.weekStarting = weekStarting.ToString();
                        measure.change = dataSet.Tables[0].Rows[indexRow]["change"].ToString();
                        measure.detect = dataSet.Tables[0].Rows[indexRow]["Detect"].ToString();
                        measure.engage = dataSet.Tables[0].Rows[indexRow]["Engage"].ToString();
                        measure.frequency = dataSet.Tables[0].Rows[indexRow]["Frequency"].ToString();
                        measure.restore = dataSet.Tables[0].Rows[indexRow]["Restore"].ToString();
                        measure.toEngage = dataSet.Tables[0].Rows[indexRow]["ToEngage"].ToString();
                        measure.toRestore = dataSet.Tables[0].Rows[indexRow]["ToRestore"].ToString();
                        measure.hwSw = dataSet.Tables[0].Rows[indexRow]["HW/SW"].ToString();
                        measure.mtbf = dataSet.Tables[0].Rows[indexRow]["MTBF"].ToString();

                        returnData.data.Add(measure);
                    }

                    returnData.success = true;
                }
            }
            catch (System.Exception error)
            {
                ErrorMessage errorMessage = new ErrorMessage();
                errorMessage.status = error.Source;
                errorMessage.message = error.Message;

                returnData.errors.Add(errorMessage);
                returnData.success = false;
            }

            return returnData;
        }




        public static ReturnData<List<FCCThresholdBreachedItem>> GetFCCThresholdBreachedData()
        {
            ReturnData<List<FCCThresholdBreachedItem>> returnData = new ReturnData<List<FCCThresholdBreachedItem>>();
            returnData.data = new List<FCCThresholdBreachedItem>();

            SqlCommand sqlCommand = new SqlCommand("dbo.FCC_RCDashboardWeb02");
            sqlCommand.CommandType = CommandType.StoredProcedure;


            sqlCommand.Connection = new SqlConnection("Persist Security Info=False; User ID=OBIRead; Password=OB1R3@d; Initial Catalog=FCC; Server=OBIVDB-PO-01P");

            SqlDataAdapter dataAdapter = new SqlDataAdapter(sqlCommand);
            DataSet dataSet = new DataSet();


            try
            {
                using (sqlCommand.Connection)
                {
                    sqlCommand.Connection.Open();

                    using (sqlCommand)
                    {
                        dataAdapter.Fill(dataSet, "dataSet");
                    }

                    sqlCommand.Connection.Close();

                    for (int indexRow = 0; indexRow <= dataSet.Tables[0].Rows.Count - 1; indexRow++)
                    {
                        FCCThresholdBreachedItem fCCThresholdBreachedItem = new FCCThresholdBreachedItem();

                        fCCThresholdBreachedItem.ticketId = dataSet.Tables[0].Rows[indexRow]["TICKET"].ToString();
                        fCCThresholdBreachedItem.monitor = dataSet.Tables[0].Rows[indexRow]["MONITOR"].ToString();
                        fCCThresholdBreachedItem.threshhold = dataSet.Tables[0].Rows[indexRow]["THRESHOLD"].ToString();
                        fCCThresholdBreachedItem.thStatus = dataSet.Tables[0].Rows[indexRow]["TH_STATUS"].ToString();
                        fCCThresholdBreachedItem.monitorDiscovery = dataSet.Tables[0].Rows[indexRow]["MONITOR_DISCOVERY"].ToString();
                        fCCThresholdBreachedItem.ces = dataSet.Tables[0].Rows[indexRow]["CES"].ToString();
                        fCCThresholdBreachedItem.email = dataSet.Tables[0].Rows[indexRow]["EMAIL"].ToString();
                        fCCThresholdBreachedItem.rcorStatus = dataSet.Tables[0].Rows[indexRow]["RCOR_STATUS"].ToString();
                        fCCThresholdBreachedItem.rcorDate = dataSet.Tables[0].Rows[indexRow]["RCOR_DATE"].ToString();
                        fCCThresholdBreachedItem.workedBy = dataSet.Tables[0].Rows[indexRow]["WORKED_BY"].ToString();

                        returnData.data.Add(fCCThresholdBreachedItem);
                    }

                    returnData.success = true;
                }
            }
            catch (System.Exception error)
            {
                ErrorMessage errorMessage = new ErrorMessage();
                errorMessage.status = error.Source;
                errorMessage.message = error.Message;

                returnData.errors.Add(errorMessage);
                returnData.success = false;
            }

            return returnData;
        }


        public static ReturnData<List<MaintenanceResolveStatus>> GetMaintanceResoveStatus()
        {

            ReturnData<List<MaintenanceResolveStatus>> returnData = new ReturnData<List<MaintenanceResolveStatus>>();
            returnData.data = new List<MaintenanceResolveStatus>();

            SqlCommand sqlCommand = new SqlCommand();
            sqlCommand.CommandType = CommandType.Text;
            sqlCommand.CommandTimeout = 600;


            sqlCommand.CommandText = "SELECT * ";
            //sqlCommand.CommandText = "SELECT[MAINT_Month], ";
            //sqlCommand.CommandText += "[MAINT_Year], [Comcast_Division], [VP_Dept], [VP_Name], ";
            //sqlCommand.CommandText += "[DIR_Group], [DIR_Name], [Fail_Count], [Fail_Pct], [Sev1_Count], [Sev1_Pct]";
            //sqlCommand.CommandText += "[Success_Count], [Success_Pct], [Total_Count] ";
            sqlCommand.CommandText += "FROM[Tickets].[dbo].[MAINT_RESOLVE_STATUS]";


            sqlCommand.Connection = new SqlConnection("Persist Security Info=False; User ID=OBIRead; Password=OB1R3@d; Initial Catalog=Tickets; Server=obidb-wc-p01");

            SqlDataAdapter dataAdapter = new SqlDataAdapter(sqlCommand);
            DataSet dataSet = new DataSet();

            try
            {
                using (sqlCommand.Connection)
                {
                    sqlCommand.Connection.Open();

                    using (sqlCommand)
                    {
                        dataAdapter.Fill(dataSet, "dataSet");
                    }

                    sqlCommand.Connection.Close();

                    for (int indexRow = 0; indexRow <= dataSet.Tables[0].Rows.Count - 1; indexRow++)
                    {
                        MaintenanceResolveStatus maintenanceResolveStatus = new MaintenanceResolveStatus();

                        maintenanceResolveStatus.month = dataSet.Tables[0].Rows[indexRow]["MAINT_Month"].ToString();
                        maintenanceResolveStatus.year = dataSet.Tables[0].Rows[indexRow]["MAINT_Year"].ToString();
                        maintenanceResolveStatus.comcastDivision = dataSet.Tables[0].Rows[indexRow]["Comcast_Division"].ToString();
                        maintenanceResolveStatus.vpDepartment = dataSet.Tables[0].Rows[indexRow]["VP_Dept"].ToString();
                        maintenanceResolveStatus.vpName = dataSet.Tables[0].Rows[indexRow]["VP_Name"].ToString();
                        maintenanceResolveStatus.dirGroup = dataSet.Tables[0].Rows[indexRow]["DIR_Group"].ToString();
                        maintenanceResolveStatus.dirName = dataSet.Tables[0].Rows[indexRow]["DIR_Name"].ToString();
                        maintenanceResolveStatus.failCount = dataSet.Tables[0].Rows[indexRow]["Fail_Count"].ToString();
                        maintenanceResolveStatus.failPct = (Convert.ToDecimal(dataSet.Tables[0].Rows[indexRow]["Fail_Pct"].ToString())).ToString("##.##");
                        maintenanceResolveStatus.sev1Count = dataSet.Tables[0].Rows[indexRow]["Sev1_Count"].ToString();
                        maintenanceResolveStatus.sev1Pct = (Convert.ToDecimal(dataSet.Tables[0].Rows[indexRow]["Sev1_Pct"].ToString())).ToString("##.##");
                        maintenanceResolveStatus.sev1TicketCount = dataSet.Tables[0].Rows[indexRow]["Sev1_Tkt_Count"].ToString();
                        maintenanceResolveStatus.successCount = dataSet.Tables[0].Rows[indexRow]["Success_Count"].ToString();
                        maintenanceResolveStatus.successPct = (Convert.ToDecimal(dataSet.Tables[0].Rows[indexRow]["Success_Pct"].ToString())).ToString("##.##");
                        maintenanceResolveStatus.totalCount = dataSet.Tables[0].Rows[indexRow]["Total_Count"].ToString();

                        returnData.data.Add(maintenanceResolveStatus);
                    }

                    returnData.success = true;
                }
            }
            catch (System.Exception error)
            {
                ErrorMessage errorMessage = new ErrorMessage();
                errorMessage.status = error.Source;
                errorMessage.message = error.Message;

                returnData.errors.Add(errorMessage);
                returnData.success = false;
            }

            return returnData;
        }


        public static ReturnData<List<MaintenanceCausedSev1MonthOverMonth>> GetMaintanceCausedSev1MonthOverMonth()
        {

            ReturnData<List<MaintenanceCausedSev1MonthOverMonth>> returnData = new ReturnData<List<MaintenanceCausedSev1MonthOverMonth>>();
            returnData.data = new List<MaintenanceCausedSev1MonthOverMonth>();

            SqlCommand sqlCommand = new SqlCommand("OBI.dbo.MaintCausedSev1MonthOverMonth ");
            sqlCommand.CommandType = CommandType.StoredProcedure;
            sqlCommand.CommandTimeout = 600;


            sqlCommand.Connection = new SqlConnection("Persist Security Info=False; User ID=OBIRead; Password=OB1R3@d; Initial Catalog=OBI; Server=obidb-wc-p01");

            SqlDataAdapter dataAdapter = new SqlDataAdapter(sqlCommand);
            DataSet dataSet = new DataSet();

            try
            {
                using (sqlCommand.Connection)
                {
                    sqlCommand.Connection.Open();

                    using (sqlCommand)
                    {
                        dataAdapter.Fill(dataSet, "dataSet");
                    }

                    sqlCommand.Connection.Close();

                    for (int indexRow = 0; indexRow <= dataSet.Tables[0].Rows.Count - 1; indexRow++)
                    {
                        MaintenanceCausedSev1MonthOverMonth maintenanceCausedSev1MonthOverMonth = new MaintenanceCausedSev1MonthOverMonth();

                        maintenanceCausedSev1MonthOverMonth.maintenanceMonth = dataSet.Tables[0].Rows[indexRow]["MAINT_Month"].ToString();
                        maintenanceCausedSev1MonthOverMonth.failCountPreviousYear = dataSet.Tables[0].Rows[indexRow]["Fail_Count_Pre_Year"].ToString();
                        maintenanceCausedSev1MonthOverMonth.failCountCurrentYear = dataSet.Tables[0].Rows[indexRow]["Fail_Count_Cur_Year"].ToString();
                        maintenanceCausedSev1MonthOverMonth.failPctPreviousYear = (Convert.ToDecimal(dataSet.Tables[0].Rows[indexRow]["Fail_Pct_Pre_Year"].ToString())).ToString("##.##");
                        maintenanceCausedSev1MonthOverMonth.failPctCurrentYear = (Convert.ToDecimal(dataSet.Tables[0].Rows[indexRow]["Fail_Pct_Cur_Year"].ToString())).ToString("##.##");
                        maintenanceCausedSev1MonthOverMonth.totalCountPreviousYear = dataSet.Tables[0].Rows[indexRow]["Total_Count_Pre_Year"].ToString();
                        maintenanceCausedSev1MonthOverMonth.totalCountCurrentYear = dataSet.Tables[0].Rows[indexRow]["Total_Count_Cur_Year"].ToString();

                        returnData.data.Add(maintenanceCausedSev1MonthOverMonth);
                    }

                    returnData.success = true;
                }
            }
            catch (System.Exception error)
            {
                ErrorMessage errorMessage = new ErrorMessage();
                errorMessage.status = error.Source;
                errorMessage.message = error.Message;

                returnData.errors.Add(errorMessage);
                returnData.success = false;
            }

            return returnData;
        }

        public static ReturnData<List<MaintenanceResolveStatus>> GetMaintanceCausedSuccessFailByMonth(String month, String year)
        {
            ReturnData<List<MaintenanceResolveStatus>> returnData = new ReturnData<List<MaintenanceResolveStatus>>();
            returnData.data = new List<MaintenanceResolveStatus>();

            SqlCommand sqlCommand = new SqlCommand("OBI.dbo.MaintCausedSuccess_FailByMonth");
            sqlCommand.CommandType = CommandType.StoredProcedure;
            sqlCommand.CommandTimeout = 600;

            sqlCommand.Parameters.Add("@Month", SqlDbType.VarChar).Value = month;
            sqlCommand.Parameters.Add("@Year", SqlDbType.VarChar).Value = year;

            sqlCommand.Connection = new SqlConnection("Persist Security Info=False; User ID=OBIRead; Password=OB1R3@d; Initial Catalog=OBI; Server=obidb-wc-p01");

            SqlDataAdapter dataAdapter = new SqlDataAdapter(sqlCommand);
            DataSet dataSet = new DataSet();

            try
            {
                using (sqlCommand.Connection)
                {
                    sqlCommand.Connection.Open();

                    using (sqlCommand)
                    {
                        dataAdapter.Fill(dataSet, "dataSet");
                    }

                    sqlCommand.Connection.Close();

                    for (int indexRow = 0; indexRow <= dataSet.Tables[0].Rows.Count - 1; indexRow++)
                    {
                        MaintenanceResolveStatus maintenanceResolveStatus = new MaintenanceResolveStatus();

                        maintenanceResolveStatus.month = dataSet.Tables[0].Rows[indexRow]["MAINT_Month"].ToString();
                        maintenanceResolveStatus.year = dataSet.Tables[0].Rows[indexRow]["MAINT_Year"].ToString();
                        maintenanceResolveStatus.comcastDivision = dataSet.Tables[0].Rows[indexRow]["Comcast_Division"].ToString();
                        maintenanceResolveStatus.vpDepartment = dataSet.Tables[0].Rows[indexRow]["VP_Dept"].ToString();
                        maintenanceResolveStatus.vpName = dataSet.Tables[0].Rows[indexRow]["VP_Name"].ToString();
                        maintenanceResolveStatus.dirGroup = dataSet.Tables[0].Rows[indexRow]["DIR_Group"].ToString();
                        maintenanceResolveStatus.dirName = dataSet.Tables[0].Rows[indexRow]["DIR_Name"].ToString();
                        maintenanceResolveStatus.failCount = dataSet.Tables[0].Rows[indexRow]["Fail_Count"].ToString();
                        maintenanceResolveStatus.failPct = (Convert.ToDecimal(dataSet.Tables[0].Rows[indexRow]["Fail_Pct"].ToString())).ToString("##.##");
                        maintenanceResolveStatus.causedIncidentCount = dataSet.Tables[0].Rows[indexRow]["Incident_Count"].ToString();
                        maintenanceResolveStatus.incidentPct = (Convert.ToDecimal(dataSet.Tables[0].Rows[indexRow]["Incident_Pct"].ToString())).ToString("##.##");
                        maintenanceResolveStatus.incidentCount = dataSet.Tables[0].Rows[indexRow]["Incident_Tkt_Count"].ToString();
                        maintenanceResolveStatus.sev1Count = dataSet.Tables[0].Rows[indexRow]["Sev1_Count"].ToString();
                        maintenanceResolveStatus.sev1Pct = (Convert.ToDecimal(dataSet.Tables[0].Rows[indexRow]["Sev1_Pct"].ToString())).ToString("##.##");
                        maintenanceResolveStatus.sev1TicketCount = dataSet.Tables[0].Rows[indexRow]["Sev1_Tkt_Count"].ToString();
                        maintenanceResolveStatus.successCount = dataSet.Tables[0].Rows[indexRow]["Success_Count"].ToString();
                        maintenanceResolveStatus.successPct = (Convert.ToDecimal(dataSet.Tables[0].Rows[indexRow]["Success_Pct"].ToString())).ToString("##.##");
                        maintenanceResolveStatus.totalCount = dataSet.Tables[0].Rows[indexRow]["Total_Count"].ToString();

                        returnData.data.Add(maintenanceResolveStatus);
                    }

                    returnData.success = true;
                }
            }
            catch (System.Exception error)
            {
                ErrorMessage errorMessage = new ErrorMessage();
                errorMessage.status = error.Source;
                errorMessage.message = error.Message;

                returnData.errors.Add(errorMessage);
                returnData.success = false;
            }

            return returnData;
        }


        public static ReturnData<List<Sev1CausedByMaintDetail>> GetSev1CausedByMaintDetails(String directorName, String month, String year, String reportLevel)
        {
            ReturnData<List<Sev1CausedByMaintDetail>> returnData = new ReturnData<List<Sev1CausedByMaintDetail>>();
            returnData.data = new List<Sev1CausedByMaintDetail>();

            SqlCommand sqlCommand = new SqlCommand("dbo.Get_AOG_Sev1_SI_by_Detail");
            sqlCommand.CommandType = CommandType.StoredProcedure;
            sqlCommand.CommandTimeout = 600;

            sqlCommand.Parameters.Add("@dirname", SqlDbType.VarChar).Value = directorName;
            sqlCommand.Parameters.Add("@maint_month", SqlDbType.VarChar).Value = month;
            sqlCommand.Parameters.Add("@main_year", SqlDbType.VarChar).Value = year;
            sqlCommand.Parameters.Add("@rptLevel", SqlDbType.VarChar).Value = reportLevel;

            sqlCommand.Connection = new SqlConnection("Persist Security Info=False; User ID=OBIRead; Password=OB1R3@d; Initial Catalog=OBI; Server=obidb-wc-p01");

            SqlDataAdapter dataAdapter = new SqlDataAdapter(sqlCommand);
            DataSet dataSet = new DataSet();

            try
            {
                using (sqlCommand.Connection)
                {
                    sqlCommand.Connection.Open();

                    using (sqlCommand)
                    {
                        dataAdapter.Fill(dataSet, "dataSet");
                    }

                    sqlCommand.Connection.Close();

                    for (int indexRow = 0; indexRow <= dataSet.Tables[0].Rows.Count - 1; indexRow++)
                    {
                        Sev1CausedByMaintDetail sev1CausedByMaintDetail = new Sev1CausedByMaintDetail();

                        sev1CausedByMaintDetail.maintTicketId = dataSet.Tables[0].Rows[indexRow]["Maint_ticket_id"].ToString();
                        sev1CausedByMaintDetail.cmSummary = dataSet.Tables[0].Rows[indexRow]["CM Summary"].ToString();
                        sev1CausedByMaintDetail.resolutionDescription = dataSet.Tables[0].Rows[indexRow]["Resolution_Description"].ToString();
                        sev1CausedByMaintDetail.ttsId = dataSet.Tables[0].Rows[indexRow]["TTS_ID"].ToString();
                        sev1CausedByMaintDetail.rcaTitle = dataSet.Tables[0].Rows[indexRow]["RCA_Title"].ToString();
                        sev1CausedByMaintDetail.actualStart = dataSet.Tables[0].Rows[indexRow]["TT_ACTUAL_START"].ToString();
                        sev1CausedByMaintDetail.actualEnd = dataSet.Tables[0].Rows[indexRow]["TT_ACTUAL_END"].ToString();
                        sev1CausedByMaintDetail.causeDescription = dataSet.Tables[0].Rows[indexRow]["TT_CAUSE_DESCRIPTION"].ToString();
                        sev1CausedByMaintDetail.solutionDescription = dataSet.Tables[0].Rows[indexRow]["TT_SOLUTION_DESCRIPTION"].ToString();
                        sev1CausedByMaintDetail.problemDescription = dataSet.Tables[0].Rows[indexRow]["TT_PROBLEM_DESCRIPTION"].ToString();
                        sev1CausedByMaintDetail.queueName = dataSet.Tables[0].Rows[indexRow]["TT_QUEUE_NAME"].ToString();
                        sev1CausedByMaintDetail.selfInflicted = dataSet.Tables[0].Rows[indexRow]["Self-Inflicted"].ToString();
                        sev1CausedByMaintDetail.execSummary = dataSet.Tables[0].Rows[indexRow]["Exec_Summary"].ToString();
                        sev1CausedByMaintDetail.customerImpact = dataSet.Tables[0].Rows[indexRow]["Customer_Impact"].ToString();
                        sev1CausedByMaintDetail.careImpact = dataSet.Tables[0].Rows[indexRow]["care_impact"].ToString();
                        sev1CausedByMaintDetail.platform = dataSet.Tables[0].Rows[indexRow]["Platform"].ToString();
                        sev1CausedByMaintDetail.rootCause = dataSet.Tables[0].Rows[indexRow]["Root_Cause"].ToString();
                        sev1CausedByMaintDetail.responsibleParty = dataSet.Tables[0].Rows[indexRow]["Responsible_Party"].ToString();
                        sev1CausedByMaintDetail.detect = dataSet.Tables[0].Rows[indexRow]["Detect"].ToString();
                        sev1CausedByMaintDetail.redundacy = dataSet.Tables[0].Rows[indexRow]["Redundancy"].ToString();
                        sev1CausedByMaintDetail.responsiblePerson = dataSet.Tables[0].Rows[indexRow]["Responsible_Person"].ToString();

                        returnData.data.Add(sev1CausedByMaintDetail);
                    }

                    returnData.success = true;
                }
            }
            catch (System.Exception error)
            {
                ErrorMessage errorMessage = new ErrorMessage();
                errorMessage.status = error.Source;
                errorMessage.message = error.Message;

                returnData.errors.Add(errorMessage);
                returnData.success = false;
            }

            return returnData;
        }


        public static ReturnData<SMTicketGeneralDetails> GetSMTicketGeneralDetails(String ticketNumber)
        {

            ReturnData<SMTicketGeneralDetails> returnData = new ReturnData<SMTicketGeneralDetails>();
            returnData.data = new SMTicketGeneralDetails();

            SqlCommand sqlCommand = new SqlCommand("dbo.rxrSMTkDetailGen");
            sqlCommand.CommandType = CommandType.StoredProcedure;
            sqlCommand.CommandTimeout = 600;

            sqlCommand.Parameters.Add("@TicketID", SqlDbType.VarChar).Value = ticketNumber;


            sqlCommand.Connection = new SqlConnection("Persist Security Info=False; User ID=OBIRead; Password=OB1R3@d; Initial Catalog=OBI; Server=obidb-wc-p01");

            SqlDataAdapter dataAdapter = new SqlDataAdapter(sqlCommand);
            DataSet dataSet = new DataSet();

            try
            {
                using (sqlCommand.Connection)
                {
                    sqlCommand.Connection.Open();

                    using (sqlCommand)
                    {
                        dataAdapter.Fill(dataSet, "dataSet");
                    }

                    sqlCommand.Connection.Close();


                    returnData.data.ticketId = dataSet.Tables[0].Rows[0]["TICKET_ID"].ToString();
                    returnData.data.parentTicketId = dataSet.Tables[0].Rows[0]["PARENT_ID"].ToString();
                    returnData.data.projectName = dataSet.Tables[0].Rows[0]["PROJECT_NAME"].ToString();
                    returnData.data.regionName = dataSet.Tables[0].Rows[0]["REGION_NAME"].ToString();
                    returnData.data.ticketType = dataSet.Tables[0].Rows[0]["TICKET_TYPE"].ToString();
                    returnData.data.product = dataSet.Tables[0].Rows[0]["PRODUCT"].ToString();
                    returnData.data.smStatus = dataSet.Tables[0].Rows[0]["SM_STATUS"].ToString();
                    returnData.data.createDate = dataSet.Tables[0].Rows[0]["CREATE_DATE"].ToString();
                    returnData.data.nextAction = dataSet.Tables[0].Rows[0]["NEXT_ACTION"].ToString();
                    returnData.data.maintenanceType = dataSet.Tables[0].Rows[0]["MAINTENANCE_TYPE"].ToString();
                    returnData.data.maintenanceDescription = dataSet.Tables[0].Rows[0]["MAINTENANCE_DESCRIPTION"].ToString();
                    returnData.data.maintenaceCategory = dataSet.Tables[0].Rows[0]["MAINTENANCE_CATEGORY"].ToString();
                    returnData.data.maintenanceDetail = dataSet.Tables[0].Rows[0]["MAINTENANCE_DETAIL"].ToString();
                    returnData.data.smFrequency = dataSet.Tables[0].Rows[0]["SM_FREQUENCY"].ToString();
                    returnData.data.videoAffected = dataSet.Tables[0].Rows[0]["VIDEO_AFFECTED"].ToString();
                    returnData.data.hsdAffected = dataSet.Tables[0].Rows[0]["HSD_AFFECTED"].ToString();
                    returnData.data.telephonyAffected = dataSet.Tables[0].Rows[0]["TELEPHONY_AFFECTED"].ToString();
                    returnData.data.totalCustomersAffected = dataSet.Tables[0].Rows[0]["TOTAL_CUST_AFFECTED"].ToString();
                    returnData.data.communitiesAffected = dataSet.Tables[0].Rows[0]["COMMUNITIES_AFFECTED"].ToString();
                    returnData.data.actualStart = dataSet.Tables[0].Rows[0]["ACTUAL_START"].ToString();
                    returnData.data.actualEnd = dataSet.Tables[0].Rows[0]["ACTUAL_END"].ToString();
                    returnData.data.smDuration = dataSet.Tables[0].Rows[0]["SM_DURATION"].ToString();
                    returnData.data.resolutionDescription = dataSet.Tables[0].Rows[0]["RESOLUTION_DESCRIPTION"].ToString();
                    returnData.data.indicatorCity = dataSet.Tables[0].Rows[0]["INDICATOR_CITY"].ToString();
                    returnData.data.indicatorState = dataSet.Tables[0].Rows[0]["INDICATOR_STATE"].ToString();
                    returnData.data.localManagementApprover = dataSet.Tables[0].Rows[0]["LOCAL_MANAGEMENT_APPROVER"].ToString();
                    returnData.data.localManagementPhone = dataSet.Tables[0].Rows[0]["LOCAL_MANAGEMENT_PHONE"].ToString();
                    returnData.data.supportAreaName = dataSet.Tables[0].Rows[0]["SUPPORT_AREA_NAME"].ToString();
                    returnData.data.fullName = dataSet.Tables[0].Rows[0]["FULL_NAME"].ToString();
                    returnData.data.apCreateDate = dataSet.Tables[0].Rows[0]["AP_CREATE_DATE"].ToString();
                    returnData.data.plannedStart = dataSet.Tables[0].Rows[0]["PLANNED_START"].ToString();
                    returnData.data.plannedEnd = dataSet.Tables[0].Rows[0]["PLANNED_END"].ToString();


                    returnData.success = true;
                }
            }
            catch (System.Exception error)
            {
                ErrorMessage errorMessage = new ErrorMessage();
                errorMessage.status = error.Source;
                errorMessage.message = error.Message;

                returnData.errors.Add(errorMessage);
                returnData.success = false;
            }

            return returnData;
        }

        public static ReturnData<List<SMTicketAffectedElement>> GetSMTicketAffectedElements(String ticketNumber)
        {
            ReturnData<List<SMTicketAffectedElement>> returnData = new ReturnData<List<SMTicketAffectedElement>>();
            returnData.data = new List<SMTicketAffectedElement>();

            SqlCommand sqlCommand = new SqlCommand("dbo.rxrSMTktDetailAffEle");
            sqlCommand.CommandType = CommandType.StoredProcedure;
            sqlCommand.CommandTimeout = 600;

            sqlCommand.Parameters.Add("@TicketID", SqlDbType.VarChar).Value = ticketNumber;

            sqlCommand.Connection = new SqlConnection("Persist Security Info=False; User ID=OBIRead; Password=OB1R3@d; Initial Catalog=OBI; Server=obidb-wc-p01");

            SqlDataAdapter dataAdapter = new SqlDataAdapter(sqlCommand);
            DataSet dataSet = new DataSet();

            try
            {
                using (sqlCommand.Connection)
                {
                    sqlCommand.Connection.Open();

                    using (sqlCommand)
                    {
                        dataAdapter.Fill(dataSet, "dataSet");
                    }

                    sqlCommand.Connection.Close();

                    for (int indexRow = 0; indexRow <= dataSet.Tables[0].Rows.Count - 1; indexRow++)
                    {
                        SMTicketAffectedElement sMTicketAffectedElement = new SMTicketAffectedElement();

                        sMTicketAffectedElement.ticketId = dataSet.Tables[0].Rows[indexRow]["TICKET_ID"].ToString();
                        sMTicketAffectedElement.entryId = dataSet.Tables[0].Rows[indexRow]["ENTRY_ID"].ToString();
                        sMTicketAffectedElement.serviceAffected = dataSet.Tables[0].Rows[indexRow]["SERVICE_AFFECT"].ToString();
                        sMTicketAffectedElement.elementName = dataSet.Tables[0].Rows[indexRow]["ELEMENT_NAME"].ToString();
                        sMTicketAffectedElement.actualStart = dataSet.Tables[0].Rows[indexRow]["ACTUAL_START"].ToString();
                        sMTicketAffectedElement.actualEnd = dataSet.Tables[0].Rows[indexRow]["ACTUAL_END"].ToString();

                        returnData.data.Add(sMTicketAffectedElement);
                    }

                    returnData.success = true;
                }
            }
            catch (System.Exception error)
            {
                ErrorMessage errorMessage = new ErrorMessage();
                errorMessage.status = error.Source;
                errorMessage.message = error.Message;

                returnData.errors.Add(errorMessage);
                returnData.success = false;
            }

            return returnData;
        }

        public static ReturnData<List<WorkLogTicket>> GetSMTicketWorkLog(String ticketNumber, String source)
        {
            ReturnData<List<WorkLogTicket>> returnData = new ReturnData<List<WorkLogTicket>>();
            returnData.data = new List<WorkLogTicket>();

            SqlCommand sqlCommand = new SqlCommand("dbo.rxrSMTktDetailWorklog");
            sqlCommand.CommandType = CommandType.StoredProcedure;

            sqlCommand.Parameters.Add("@TicketID", SqlDbType.VarChar).Value = ticketNumber;
            sqlCommand.Parameters.Add("@Source", SqlDbType.VarChar).Value = source;

            sqlCommand.Connection = new SqlConnection("Persist Security Info=False; User ID=OBIRead; Password=OB1R3@d; Initial Catalog=OBI; Server=obidb-wc-p01");

            SqlDataAdapter dataAdapter = new SqlDataAdapter(sqlCommand);
            DataSet dataSet = new DataSet();

            try
            {
                using (sqlCommand.Connection)
                {
                    sqlCommand.Connection.Open();

                    using (sqlCommand)
                    {
                        dataAdapter.Fill(dataSet, "dataSet");
                    }

                    sqlCommand.Connection.Close();

                    for (int indexRow = 0; indexRow <= dataSet.Tables[0].Rows.Count - 1; indexRow++)
                    {
                        WorkLogTicket workLogTicket = new WorkLogTicket();

                        workLogTicket.ticketNumber = ticketNumber;
                        workLogTicket.createDate = dataSet.Tables[0].Rows[indexRow]["CREATE_DATE"].ToString();
                        workLogTicket.submitter = dataSet.Tables[0].Rows[indexRow]["SUBMITTER"].ToString();
                        workLogTicket.source = dataSet.Tables[0].Rows[indexRow]["SOURCE"].ToString();
                        workLogTicket.subject = dataSet.Tables[0].Rows[indexRow]["SUBJECT"].ToString();
                        workLogTicket.details = dataSet.Tables[0].Rows[indexRow]["DETAILS"].ToString();

                        returnData.data.Add(workLogTicket);
                    }

                    returnData.success = true;
                }
            }
            catch (System.Exception error)
            {
                ErrorMessage errorMessage = new ErrorMessage();
                errorMessage.status = error.Source;
                errorMessage.message = error.Message;

                returnData.errors.Add(errorMessage);
                returnData.success = false;
            }

            return returnData;
        }

        public static ReturnData<SRTicketGeneralDetails> GetSRTicketGeneralDetails(String ticketNumber)
        {

            ReturnData<SRTicketGeneralDetails> returnData = new ReturnData<SRTicketGeneralDetails>();
            returnData.data = new SRTicketGeneralDetails();

            SqlCommand sqlCommand = new SqlCommand("dbo.rxrSrvcRqstTkDetailGen");
            sqlCommand.CommandType = CommandType.StoredProcedure;
            sqlCommand.CommandTimeout = 600;

            sqlCommand.Parameters.Add("@TicketID", SqlDbType.VarChar).Value = ticketNumber;


            sqlCommand.Connection = new SqlConnection("Persist Security Info=False; User ID=OBIRead; Password=OB1R3@d; Initial Catalog=OBI; Server=obidb-wc-p01");

            SqlDataAdapter dataAdapter = new SqlDataAdapter(sqlCommand);
            DataSet dataSet = new DataSet();

            try
            {
                using (sqlCommand.Connection)
                {
                    sqlCommand.Connection.Open();

                    using (sqlCommand)
                    {
                        dataAdapter.Fill(dataSet, "dataSet");
                    }

                    sqlCommand.Connection.Close();


                    returnData.data.ticketId = dataSet.Tables[0].Rows[0]["TICKET_ID"].ToString();
                    returnData.data.statusCode = dataSet.Tables[0].Rows[0]["STATUS_CODE"].ToString();
                    returnData.data.status = dataSet.Tables[0].Rows[0]["STATUS"].ToString();
                    returnData.data.priorityCode = dataSet.Tables[0].Rows[0]["PRIORITY_CODE"].ToString();
                    returnData.data.priority = dataSet.Tables[0].Rows[0]["PRIORITY"].ToString();
                    returnData.data.submitter = dataSet.Tables[0].Rows[0]["SUBMITTER"].ToString();
                    returnData.data.assignedDate = dataSet.Tables[0].Rows[0]["ASSIGNED_DATE"].ToString();
                    returnData.data.createDate = dataSet.Tables[0].Rows[0]["CREATE_DATE"].ToString();
                    returnData.data.assignedDivision = dataSet.Tables[0].Rows[0]["ASSIGNED_DIVISION"].ToString();
                    returnData.data.supportAreaName = dataSet.Tables[0].Rows[0]["SUPPORT_AREA_NAME"].ToString();
                    returnData.data.assignedQName = dataSet.Tables[0].Rows[0]["ASSIGNED_Q_NAME"].ToString();
                    returnData.data.fullName = dataSet.Tables[0].Rows[0]["FULL_NAME"].ToString();
                    returnData.data.problemCode = dataSet.Tables[0].Rows[0]["PROBLEM_CODE"].ToString();
                    returnData.data.problemDescription = dataSet.Tables[0].Rows[0]["PROBLEM_DESCRIPTION"].ToString();
                    returnData.data.problemCategory = dataSet.Tables[0].Rows[0]["PROBLEM_CATEGORY"].ToString();
                    returnData.data.problemSubcategory = dataSet.Tables[0].Rows[0]["PROBLEM_SUBCATEGORY"].ToString();
                    returnData.data.solutionCode = dataSet.Tables[0].Rows[0]["SOLUTION_CODE"].ToString();
                    returnData.data.solutionDescription = dataSet.Tables[0].Rows[0]["SOLUTION_DESCRIPTION"].ToString();
                    returnData.data.solutionCategory = dataSet.Tables[0].Rows[0]["SOLUTION_CATEGORY"].ToString();
                    returnData.data.solutionSubcategory = dataSet.Tables[0].Rows[0]["SOLUTION_SUBCATEGORY"].ToString();
                    returnData.data.requestForName = dataSet.Tables[0].Rows[0]["REQUEST_FOR_NAME"].ToString();
                    returnData.data.requestForPhone = dataSet.Tables[0].Rows[0]["REQUEST_FOR_PHONE"].ToString();
                    returnData.data.requestForLocation = dataSet.Tables[0].Rows[0]["REQUEST_FOR_LOCATION"].ToString();
                    returnData.data.requestForEmail = dataSet.Tables[0].Rows[0]["REQUEST_FOR_EMAIL"].ToString();
                    returnData.data.workLocationStreet = dataSet.Tables[0].Rows[0]["WORK_LOCATION_STREET"].ToString();
                    returnData.data.workLocationCity = dataSet.Tables[0].Rows[0]["WORK_LOCATION_CITY"].ToString();
                    returnData.data.contactName = dataSet.Tables[0].Rows[0]["CONTACT_NAME"].ToString();
                    returnData.data.contactPhone = dataSet.Tables[0].Rows[0]["CONTACT_PHONE"].ToString();
                    returnData.data.contactEmail = dataSet.Tables[0].Rows[0]["CONTACT_EMAIL"].ToString();


                    returnData.success = true;
                }
            }
            catch (System.Exception error)
            {
                ErrorMessage errorMessage = new ErrorMessage();
                errorMessage.status = error.Source;
                errorMessage.message = error.Message;

                returnData.errors.Add(errorMessage);
                returnData.success = false;
            }

            return returnData;
        }

        public static ReturnData<List<SRTicketAttribute>> GetSRTicketAttributes(String ticketNumber)
        {
            ReturnData<List<SRTicketAttribute>> returnData = new ReturnData<List<SRTicketAttribute>>();
            returnData.data = new List<SRTicketAttribute>();

            SqlCommand sqlCommand = new SqlCommand("dbo.rxrSrvcRqstTkDetailAttr");
            sqlCommand.CommandType = CommandType.StoredProcedure;
            sqlCommand.CommandTimeout = 600;

            sqlCommand.Parameters.Add("@TicketID", SqlDbType.VarChar).Value = ticketNumber;

            sqlCommand.Connection = new SqlConnection("Persist Security Info=False; User ID=OBIRead; Password=OB1R3@d; Initial Catalog=OBI; Server=obidb-wc-p01");

            SqlDataAdapter dataAdapter = new SqlDataAdapter(sqlCommand);
            DataSet dataSet = new DataSet();

            try
            {
                using (sqlCommand.Connection)
                {
                    sqlCommand.Connection.Open();

                    using (sqlCommand)
                    {
                        dataAdapter.Fill(dataSet, "dataSet");
                    }

                    sqlCommand.Connection.Close();

                    for (int indexRow = 0; indexRow <= dataSet.Tables[0].Rows.Count - 1; indexRow++)
                    {
                        SRTicketAttribute sRTicketAttribute = new SRTicketAttribute();

                        sRTicketAttribute.ticketId = dataSet.Tables[0].Rows[indexRow]["TICKET_ID"].ToString();
                        sRTicketAttribute.attributeLabel = dataSet.Tables[0].Rows[indexRow]["ATTR_LABEL"].ToString();
                        sRTicketAttribute.attributeValue = dataSet.Tables[0].Rows[indexRow]["VALUE_CHAR"].ToString();
                        sRTicketAttribute.attributeDisplay = dataSet.Tables[0].Rows[indexRow]["ATTR_DISPLAY"].ToString();

                        returnData.data.Add(sRTicketAttribute);
                    }

                    returnData.success = true;
                }
            }
            catch (System.Exception error)
            {
                ErrorMessage errorMessage = new ErrorMessage();
                errorMessage.status = error.Source;
                errorMessage.message = error.Message;

                returnData.errors.Add(errorMessage);
                returnData.success = false;
            }

            return returnData;
        }

        public static ReturnData<List<WorkLogTicket>> GetSRTicketWorkLog(String ticketNumber, String source)
        {
            ReturnData<List<WorkLogTicket>> returnData = new ReturnData<List<WorkLogTicket>>();
            returnData.data = new List<WorkLogTicket>();

            SqlCommand sqlCommand = new SqlCommand("dbo.rxrSrvcRqstTkDetailWorklog");
            sqlCommand.CommandType = CommandType.StoredProcedure;

            sqlCommand.Parameters.Add("@TicketID", SqlDbType.VarChar).Value = ticketNumber;
            sqlCommand.Parameters.Add("@Source", SqlDbType.VarChar).Value = source;

            sqlCommand.Connection = new SqlConnection("Persist Security Info=False; User ID=OBIRead; Password=OB1R3@d; Initial Catalog=OBI; Server=obidb-wc-p01");

            SqlDataAdapter dataAdapter = new SqlDataAdapter(sqlCommand);
            DataSet dataSet = new DataSet();

            try
            {
                using (sqlCommand.Connection)
                {
                    sqlCommand.Connection.Open();

                    using (sqlCommand)
                    {
                        dataAdapter.Fill(dataSet, "dataSet");
                    }

                    sqlCommand.Connection.Close();

                    for (int indexRow = 0; indexRow <= dataSet.Tables[0].Rows.Count - 1; indexRow++)
                    {
                        WorkLogTicket workLogTicket = new WorkLogTicket();

                        workLogTicket.ticketNumber = dataSet.Tables[0].Rows[indexRow]["TICKET_ID"].ToString();
                        workLogTicket.createDate = dataSet.Tables[0].Rows[indexRow]["WORKLOG_DATE"].ToString();
                        workLogTicket.submitter = dataSet.Tables[0].Rows[indexRow]["SUBMITTER"].ToString();
                        workLogTicket.source = dataSet.Tables[0].Rows[indexRow]["SOURCE"].ToString();
                        workLogTicket.subject = dataSet.Tables[0].Rows[indexRow]["SUBJECT"].ToString();
                        workLogTicket.details = dataSet.Tables[0].Rows[indexRow]["DETAILS"].ToString();

                        returnData.data.Add(workLogTicket);
                    }

                    returnData.success = true;
                }
            }
            catch (System.Exception error)
            {
                ErrorMessage errorMessage = new ErrorMessage();
                errorMessage.status = error.Source;
                errorMessage.message = error.Message;

                returnData.errors.Add(errorMessage);
                returnData.success = false;
            }

            return returnData;
        }

        public static ReturnData<CMTicketGeneralDetails> GetCMTicketGeneralDetails(String ticketNumber)
        {

            ReturnData<CMTicketGeneralDetails> returnData = new ReturnData<CMTicketGeneralDetails>();
            returnData.data = new CMTicketGeneralDetails();

            SqlCommand sqlCommand = new SqlCommand("dbo.rxrCMTktDetailGen");
            sqlCommand.CommandType = CommandType.StoredProcedure;
            sqlCommand.CommandTimeout = 600;

            sqlCommand.Parameters.Add("@TicketID", SqlDbType.VarChar).Value = ticketNumber;


            sqlCommand.Connection = new SqlConnection("Persist Security Info=False; User ID=OBIRead; Password=OB1R3@d; Initial Catalog=OBI; Server=obidb-wc-p01");

            SqlDataAdapter dataAdapter = new SqlDataAdapter(sqlCommand);
            DataSet dataSet = new DataSet();

            try
            {
                using (sqlCommand.Connection)
                {
                    sqlCommand.Connection.Open();

                    using (sqlCommand)
                    {
                        dataAdapter.Fill(dataSet, "dataSet");
                    }

                    sqlCommand.Connection.Close();


                    returnData.data.ticketId = dataSet.Tables[0].Rows[0]["TICKET_ID"].ToString();
                    returnData.data.changeSummary = dataSet.Tables[0].Rows[0]["CHANGE_SUMMARY"].ToString();
                    returnData.data.regionName = dataSet.Tables[0].Rows[0]["REGION_NAME"].ToString();
                    returnData.data.supportAreaName = dataSet.Tables[0].Rows[0]["SUPPORT_AREA_NAME"].ToString();
                    returnData.data.queueName = dataSet.Tables[0].Rows[0]["QUEUE_Name"].ToString();
                    returnData.data.fullName = dataSet.Tables[0].Rows[0]["FULL_NAME"].ToString();
                    returnData.data.status = dataSet.Tables[0].Rows[0]["STATUS"].ToString();
                    returnData.data.ndStatus = dataSet.Tables[0].Rows[0]["ND_STATUS"].ToString();
                    returnData.data.product = dataSet.Tables[0].Rows[0]["PRODUCT"].ToString();
                    returnData.data.assignedDate = dataSet.Tables[0].Rows[0]["ASSIGNED_DATE"].ToString();
                    returnData.data.videoAffected = dataSet.Tables[0].Rows[0]["VIDEO_AFFECTED"].ToString();
                    returnData.data.hsdAffected = dataSet.Tables[0].Rows[0]["HSD_AFFECTED"].ToString();
                    returnData.data.telephonyAffected = dataSet.Tables[0].Rows[0]["TELEPHONY_AFFECTED"].ToString();
                    returnData.data.totalCustomersAffected = dataSet.Tables[0].Rows[0]["TOTAL_CUST_AFFECTED"].ToString();
                    returnData.data.nextAction = dataSet.Tables[0].Rows[0]["NEXT_ACTION"].ToString();
                    returnData.data.maintenanceType = dataSet.Tables[0].Rows[0]["MAINTENANCE_TYPE"].ToString();
                    returnData.data.projectName = dataSet.Tables[0].Rows[0]["PROJECT_NAME"].ToString();
                    returnData.data.changeCategory = dataSet.Tables[0].Rows[0]["CHANGE_CATEGORY"].ToString();
                    returnData.data.changeSubcategory = dataSet.Tables[0].Rows[0]["CHANGE_SUBCATEGORY"].ToString();
                    returnData.data.impactLevel = dataSet.Tables[0].Rows[0]["IMPACT_LEVEL"].ToString();
                    returnData.data.indicatorCity = dataSet.Tables[0].Rows[0]["INDICATOR_CITY"].ToString();
                    returnData.data.changeDetail = dataSet.Tables[0].Rows[0]["CHANGE_DETAIL"].ToString();
                    returnData.data.plannedStart = dataSet.Tables[0].Rows[0]["PLANNED_START"].ToString();
                    returnData.data.plannedEnd = dataSet.Tables[0].Rows[0]["PLANNED_END"].ToString();

                    returnData.success = true;
                }
            }
            catch (System.Exception error)
            {
                ErrorMessage errorMessage = new ErrorMessage();
                errorMessage.status = error.Source;
                errorMessage.message = error.Message;

                returnData.errors.Add(errorMessage);
                returnData.success = false;
            }

            return returnData;
        }

        public static ReturnData<List<CMTicketContactDetail>> GetCMContactDetails(String ticketNumber)
        {
            ReturnData<List<CMTicketContactDetail>> returnData = new ReturnData<List<CMTicketContactDetail>>();
            returnData.data = new List<CMTicketContactDetail>();

            SqlCommand sqlCommand = new SqlCommand("dbo.rxrCMTktDetailContact");
            sqlCommand.CommandType = CommandType.StoredProcedure;
            sqlCommand.CommandTimeout = 600;

            sqlCommand.Parameters.Add("@TicketID", SqlDbType.VarChar).Value = ticketNumber;

            sqlCommand.Connection = new SqlConnection("Persist Security Info=False; User ID=OBIRead; Password=OB1R3@d; Initial Catalog=OBI; Server=obidb-wc-p01");

            SqlDataAdapter dataAdapter = new SqlDataAdapter(sqlCommand);
            DataSet dataSet = new DataSet();

            try
            {
                using (sqlCommand.Connection)
                {
                    sqlCommand.Connection.Open();

                    using (sqlCommand)
                    {
                        dataAdapter.Fill(dataSet, "dataSet");
                    }

                    sqlCommand.Connection.Close();

                    for (int indexRow = 0; indexRow <= dataSet.Tables[0].Rows.Count - 1; indexRow++)
                    {
                        CMTicketContactDetail cMContactDetail = new CMTicketContactDetail();

                        cMContactDetail.ticketId = dataSet.Tables[0].Rows[indexRow]["TICKET_ID"].ToString();
                        cMContactDetail.createDate = dataSet.Tables[0].Rows[indexRow]["CREATE_DATE"].ToString();
                        cMContactDetail.contactName = dataSet.Tables[0].Rows[indexRow]["CONTACT_NAME"].ToString();
                        cMContactDetail.emailAddress = dataSet.Tables[0].Rows[indexRow]["EMAIL_ADDRESS"].ToString();
                        cMContactDetail.primaryPhone = dataSet.Tables[0].Rows[indexRow]["PRIMARY_PHONE"].ToString();
                        cMContactDetail.contactRole = dataSet.Tables[0].Rows[indexRow]["CONTACT_ROLE"].ToString();

                        returnData.data.Add(cMContactDetail);
                    }

                    returnData.success = true;
                }
            }
            catch (System.Exception error)
            {
                ErrorMessage errorMessage = new ErrorMessage();
                errorMessage.status = error.Source;
                errorMessage.message = error.Message;

                returnData.errors.Add(errorMessage);
                returnData.success = false;
            }

            return returnData;
        }

        public static ReturnData<List<CMTicketAttribute>> GetCMTicketAttributes(String ticketNumber)
        {
            ReturnData<List<CMTicketAttribute>> returnData = new ReturnData<List<CMTicketAttribute>>();
            returnData.data = new List<CMTicketAttribute>();

            SqlCommand sqlCommand = new SqlCommand("dbo.rxrCMTktDetailDataEl");
            sqlCommand.CommandType = CommandType.StoredProcedure;
            sqlCommand.CommandTimeout = 600;

            sqlCommand.Parameters.Add("@TicketID", SqlDbType.VarChar).Value = ticketNumber;

            sqlCommand.Connection = new SqlConnection("Persist Security Info=False; User ID=OBIRead; Password=OB1R3@d; Initial Catalog=OBI; Server=obidb-wc-p01");

            SqlDataAdapter dataAdapter = new SqlDataAdapter(sqlCommand);
            DataSet dataSet = new DataSet();

            try
            {
                using (sqlCommand.Connection)
                {
                    sqlCommand.Connection.Open();

                    using (sqlCommand)
                    {
                        dataAdapter.Fill(dataSet, "dataSet");
                    }

                    sqlCommand.Connection.Close();

                    for (int indexRow = 0; indexRow <= dataSet.Tables[0].Rows.Count - 1; indexRow++)
                    {
                        CMTicketAttribute cMTicketAttribute = new CMTicketAttribute();

                        cMTicketAttribute.ticketId = dataSet.Tables[0].Rows[indexRow]["TICKET_ID"].ToString();
                        cMTicketAttribute.attributeLabel = dataSet.Tables[0].Rows[indexRow]["ATTR_LABEL"].ToString();
                        cMTicketAttribute.attributeValue = dataSet.Tables[0].Rows[indexRow]["VALUE_CHAR"].ToString();

                        returnData.data.Add(cMTicketAttribute);
                    }

                    returnData.success = true;
                }
            }
            catch (System.Exception error)
            {
                ErrorMessage errorMessage = new ErrorMessage();
                errorMessage.status = error.Source;
                errorMessage.message = error.Message;

                returnData.errors.Add(errorMessage);
                returnData.success = false;
            }

            return returnData;
        }

        public static ReturnData<List<CMTicketStep>> GetCMTicketSteps(String ticketNumber)
        {
            ReturnData<List<CMTicketStep>> returnData = new ReturnData<List<CMTicketStep>>();
            returnData.data = new List<CMTicketStep>();

            SqlCommand sqlCommand = new SqlCommand("dbo.rxrCMTktDetailStep");
            sqlCommand.CommandType = CommandType.StoredProcedure;
            sqlCommand.CommandTimeout = 600;

            sqlCommand.Parameters.Add("@TicketID", SqlDbType.VarChar).Value = ticketNumber;

            sqlCommand.Connection = new SqlConnection("Persist Security Info=False; User ID=OBIRead; Password=OB1R3@d; Initial Catalog=OBI; Server=obidb-wc-p01");

            SqlDataAdapter dataAdapter = new SqlDataAdapter(sqlCommand);
            DataSet dataSet = new DataSet();

            try
            {
                using (sqlCommand.Connection)
                {
                    sqlCommand.Connection.Open();

                    using (sqlCommand)
                    {
                        dataAdapter.Fill(dataSet, "dataSet");
                    }

                    sqlCommand.Connection.Close();

                    for (int indexRow = 0; indexRow <= dataSet.Tables[0].Rows.Count - 1; indexRow++)
                    {
                        CMTicketStep cMTicketStep = new CMTicketStep();

                        cMTicketStep.ticketId = dataSet.Tables[0].Rows[indexRow]["TICKET_ID"].ToString();
                        cMTicketStep.stepDescription = dataSet.Tables[0].Rows[indexRow]["STEP_DESCRIPTION"].ToString();
                        cMTicketStep.stepLabel = dataSet.Tables[0].Rows[indexRow]["STEP_LABEL"].ToString();
                        cMTicketStep.stepSequence = dataSet.Tables[0].Rows[indexRow]["STEP_SEQUENCE"].ToString();
                        cMTicketStep.procedureId = dataSet.Tables[0].Rows[indexRow]["PROCEDURE_ID"].ToString();
                        cMTicketStep.plannedStart = dataSet.Tables[0].Rows[indexRow]["PLANNED_START"].ToString();
                        cMTicketStep.plannedEnd = dataSet.Tables[0].Rows[indexRow]["PLANNED_END"].ToString();
                        cMTicketStep.actualStart = dataSet.Tables[0].Rows[indexRow]["ACTUAL_START"].ToString();
                        cMTicketStep.actualEnd = dataSet.Tables[0].Rows[indexRow]["ACTUAL_END"].ToString();
                        cMTicketStep.estimatedDuration = dataSet.Tables[0].Rows[indexRow]["ESTIMATED_DURATION"].ToString();
                        cMTicketStep.actualDuration = dataSet.Tables[0].Rows[indexRow]["ACTUAL_DURATION"].ToString();

                        returnData.data.Add(cMTicketStep);
                    }

                    returnData.success = true;
                }
            }
            catch (System.Exception error)
            {
                ErrorMessage errorMessage = new ErrorMessage();
                errorMessage.status = error.Source;
                errorMessage.message = error.Message;

                returnData.errors.Add(errorMessage);
                returnData.success = false;
            }

            return returnData;
        }

        public static ReturnData<List<CMTicketAffectedElement>> GetCMTicketAffectedElements(String ticketNumber)
        {
            ReturnData<List<CMTicketAffectedElement>> returnData = new ReturnData<List<CMTicketAffectedElement>>();
            returnData.data = new List<CMTicketAffectedElement>();

            SqlCommand sqlCommand = new SqlCommand("dbo.rxrCMTktDetailAffEle");
            sqlCommand.CommandType = CommandType.StoredProcedure;
            sqlCommand.CommandTimeout = 600;

            sqlCommand.Parameters.Add("@TicketID", SqlDbType.VarChar).Value = ticketNumber;

            sqlCommand.Connection = new SqlConnection("Persist Security Info=False; User ID=OBIRead; Password=OB1R3@d; Initial Catalog=OBI; Server=obidb-wc-p01");

            SqlDataAdapter dataAdapter = new SqlDataAdapter(sqlCommand);
            DataSet dataSet = new DataSet();

            try
            {
                using (sqlCommand.Connection)
                {
                    sqlCommand.Connection.Open();

                    using (sqlCommand)
                    {
                        dataAdapter.Fill(dataSet, "dataSet");
                    }

                    sqlCommand.Connection.Close();

                    for (int indexRow = 0; indexRow <= dataSet.Tables[0].Rows.Count - 1; indexRow++)
                    {
                        CMTicketAffectedElement cMTicketAffectedElement = new CMTicketAffectedElement();

                        cMTicketAffectedElement.ticketId = dataSet.Tables[0].Rows[indexRow]["TICKET_ID"].ToString();
                        cMTicketAffectedElement.entryId = dataSet.Tables[0].Rows[indexRow]["ENTRY_ID"].ToString();
                        cMTicketAffectedElement.serviceAffected = dataSet.Tables[0].Rows[indexRow]["SERVICE_AFFECT"].ToString();
                        cMTicketAffectedElement.elementName = dataSet.Tables[0].Rows[indexRow]["ELEMENT_NAME"].ToString();
                        cMTicketAffectedElement.videoAffected = dataSet.Tables[0].Rows[indexRow]["VIDEO_AFF_SUBS"].ToString();
                        cMTicketAffectedElement.hsdAffected = dataSet.Tables[0].Rows[indexRow]["HSD_AFF_SUBS"].ToString();
                        cMTicketAffectedElement.telephonyAffected = dataSet.Tables[0].Rows[indexRow]["TELEPHONY_AFF_SUBS"].ToString();
                        cMTicketAffectedElement.totalAffected = dataSet.Tables[0].Rows[indexRow]["TOTAL_AFF_SUBS"].ToString();
                        cMTicketAffectedElement.status = dataSet.Tables[0].Rows[indexRow]["AE_STATUS"].ToString();
                        cMTicketAffectedElement.actualStart = dataSet.Tables[0].Rows[indexRow]["ACTUAL_START"].ToString();
                        cMTicketAffectedElement.actualEnd = dataSet.Tables[0].Rows[indexRow]["ACTUAL_END"].ToString();


                        returnData.data.Add(cMTicketAffectedElement);
                    }

                    returnData.success = true;
                }
            }
            catch (System.Exception error)
            {
                ErrorMessage errorMessage = new ErrorMessage();
                errorMessage.status = error.Source;
                errorMessage.message = error.Message;

                returnData.errors.Add(errorMessage);
                returnData.success = false;
            }

            return returnData;
        }


        public static ReturnData<List<CMTicketWorkLog>> GetCMTicketWorkLog(String ticketNumber, String source)
        {
            ReturnData<List<CMTicketWorkLog>> returnData = new ReturnData<List<CMTicketWorkLog>>();
            returnData.data = new List<CMTicketWorkLog>();

            SqlCommand sqlCommand = new SqlCommand("dbo.rxrCMTktDetailWorklog");
            sqlCommand.CommandType = CommandType.StoredProcedure;
            sqlCommand.CommandTimeout = 600;

            sqlCommand.Parameters.Add("@TicketID", SqlDbType.VarChar).Value = ticketNumber;
            sqlCommand.Parameters.Add("@Source", SqlDbType.VarChar).Value = source;

            sqlCommand.Connection = new SqlConnection("Persist Security Info=False; User ID=OBIRead; Password=OB1R3@d; Initial Catalog=OBI; Server=obidb-wc-p01");

            SqlDataAdapter dataAdapter = new SqlDataAdapter(sqlCommand);
            DataSet dataSet = new DataSet();

            try
            {
                using (sqlCommand.Connection)
                {
                    sqlCommand.Connection.Open();

                    using (sqlCommand)
                    {
                        dataAdapter.Fill(dataSet, "dataSet");
                    }

                    sqlCommand.Connection.Close();

                    for (int indexRow = 0; indexRow <= dataSet.Tables[0].Rows.Count - 1; indexRow++)
                    {
                        CMTicketWorkLog cMTicketWorkLog = new CMTicketWorkLog();

                        cMTicketWorkLog.entryId = dataSet.Tables[0].Rows[indexRow]["ENTRY_ID"].ToString();
                        cMTicketWorkLog.createDate = dataSet.Tables[0].Rows[indexRow]["CREATE_DATE"].ToString();
                        cMTicketWorkLog.submitter = dataSet.Tables[0].Rows[indexRow]["SUBMITTER"].ToString();
                        cMTicketWorkLog.source = dataSet.Tables[0].Rows[indexRow]["SOURCE"].ToString();
                        cMTicketWorkLog.subject = dataSet.Tables[0].Rows[indexRow]["SUBJECT"].ToString();
                        cMTicketWorkLog.details = dataSet.Tables[0].Rows[indexRow]["DETAILS"].ToString();

                        returnData.data.Add(cMTicketWorkLog);
                    }

                    returnData.success = true;
                }
            }
            catch (System.Exception error)
            {
                ErrorMessage errorMessage = new ErrorMessage();
                errorMessage.status = error.Source;
                errorMessage.message = error.Message;

                returnData.errors.Add(errorMessage);
                returnData.success = false;
            }

            return returnData;
        }

        public static ReturnData<CRTicketGeneralDetails> GetCRTicketGeneralDetails(String ticketNumber)
        {

            ReturnData<CRTicketGeneralDetails> returnData = new ReturnData<CRTicketGeneralDetails>();
            returnData.data = new CRTicketGeneralDetails();

            SqlCommand sqlCommand = new SqlCommand("dbo.rxrCRTktDetailGen");
            sqlCommand.CommandType = CommandType.StoredProcedure;
            sqlCommand.CommandTimeout = 600;

            sqlCommand.Parameters.Add("@TicketID", SqlDbType.VarChar).Value = ticketNumber;


            sqlCommand.Connection = new SqlConnection("Persist Security Info=False; User ID=OBIRead; Password=OB1R3@d; Initial Catalog=OBI; Server=obidb-wc-p01");

            SqlDataAdapter dataAdapter = new SqlDataAdapter(sqlCommand);
            DataSet dataSet = new DataSet();

            try
            {
                using (sqlCommand.Connection)
                {
                    sqlCommand.Connection.Open();

                    using (sqlCommand)
                    {
                        dataAdapter.Fill(dataSet, "dataSet");
                    }

                    sqlCommand.Connection.Close();


                    returnData.data.ticketId = dataSet.Tables[0].Rows[0]["TICKET_ID"].ToString();
                    returnData.data.problemSummary = dataSet.Tables[0].Rows[0]["PROBLEM_SUMMARY"].ToString();
                    returnData.data.regionName = dataSet.Tables[0].Rows[0]["REGION_NAME"].ToString();
                    returnData.data.supportAreaName = dataSet.Tables[0].Rows[0]["SUPPORT_AREA_NAME"].ToString();
                    returnData.data.queueName = dataSet.Tables[0].Rows[0]["QUEUE_Name"].ToString();
                    returnData.data.fullName = dataSet.Tables[0].Rows[0]["FULL_NAME"].ToString();
                    returnData.data.product = dataSet.Tables[0].Rows[0]["PRODUCT"].ToString();
                    returnData.data.crStatus = dataSet.Tables[0].Rows[0]["CR_STATUS"].ToString();
                    returnData.data.assignedDate = dataSet.Tables[0].Rows[0]["ASSIGNED_DATE"].ToString();
                    returnData.data.closedDate = dataSet.Tables[0].Rows[0]["CLOSED_DATE"].ToString();
                    returnData.data.originalTicket = dataSet.Tables[0].Rows[0]["ORIGINAL_TICKET"].ToString();
                    returnData.data.reworkTicket = dataSet.Tables[0].Rows[0]["REWORK_TICKET"].ToString();
                    returnData.data.nextAction = dataSet.Tables[0].Rows[0]["NEXT_ACTION"].ToString();
                    returnData.data.problemCode = dataSet.Tables[0].Rows[0]["PROBLEM_CODE"].ToString();
                    returnData.data.category = dataSet.Tables[0].Rows[0]["CATEGORY"].ToString();
                    returnData.data.subCategory = dataSet.Tables[0].Rows[0]["SUBCATEGORY"].ToString();
                    returnData.data.causeCode = dataSet.Tables[0].Rows[0]["CAUSE_CODE"].ToString();
                    returnData.data.causeDescription = dataSet.Tables[0].Rows[0]["CAUSE_DESCRIPTION"].ToString();
                    returnData.data.causeCategory = dataSet.Tables[0].Rows[0]["CAUSE_CATEGORY"].ToString();
                    returnData.data.causeSubcategory = dataSet.Tables[0].Rows[0]["CAUSE_SUBCATEGORY"].ToString();
                    returnData.data.solutionCode = dataSet.Tables[0].Rows[0]["SOLUTION_CODE"].ToString();
                    returnData.data.solutionDescription = dataSet.Tables[0].Rows[0]["SOLUTION_DESCRIPTION"].ToString();
                    returnData.data.solutionCategory = dataSet.Tables[0].Rows[0]["SOLUTION_CATEGORY"].ToString();
                    returnData.data.solutionSubcategory = dataSet.Tables[0].Rows[0]["SOLUTION_SUBCATEGORY"].ToString();
                    returnData.data.troubleStart = dataSet.Tables[0].Rows[0]["Trouble_Start"].ToString();
                    returnData.data.ttrStart = dataSet.Tables[0].Rows[0]["TTR_START"].ToString();
                    returnData.data.ttrStop = dataSet.Tables[0].Rows[0]["TTR_STOP"].ToString();
                    returnData.data.ttrSla = dataSet.Tables[0].Rows[0]["TTR_SLA"].ToString();
                    returnData.data.indicatorCity = dataSet.Tables[0].Rows[0]["INDICATOR_CITY"].ToString();
                    returnData.data.indicatorState = dataSet.Tables[0].Rows[0]["INDICATOR_STATE"].ToString();
                    returnData.data.contactName = dataSet.Tables[0].Rows[0]["CONTACT_NAME"].ToString();
                    returnData.data.contactPhone = dataSet.Tables[0].Rows[0]["CONTACT_PHONE"].ToString();
                    returnData.data.contactEmail = dataSet.Tables[0].Rows[0]["CONTACT_EMAIL"].ToString();
                    returnData.data.customerId = dataSet.Tables[0].Rows[0]["CUSTOMER_ID"].ToString();
                    returnData.data.accountNumber = dataSet.Tables[0].Rows[0]["ACCOUNT_NUMBER"].ToString();
                    returnData.data.node = dataSet.Tables[0].Rows[0]["NODE"].ToString();
                    returnData.data.systemId = dataSet.Tables[0].Rows[0]["SYSTEM_ID"].ToString();
                    returnData.data.networkElement = dataSet.Tables[0].Rows[0]["NETWORK_ELEMENT"].ToString();
                    returnData.data.sysNm = dataSet.Tables[0].Rows[0]["SYS_NM"].ToString();


                    returnData.success = true;
                }
            }
            catch (System.Exception error)
            {
                ErrorMessage errorMessage = new ErrorMessage();
                errorMessage.status = error.Source;
                errorMessage.message = error.Message;

                returnData.errors.Add(errorMessage);
                returnData.success = false;
            }

            return returnData;
        }

        public static ReturnData<List<CRTicketAttribute>> GetCRTicketAttributes(String ticketNumber)
        {
            ReturnData<List<CRTicketAttribute>> returnData = new ReturnData<List<CRTicketAttribute>>();
            returnData.data = new List<CRTicketAttribute>();

            SqlCommand sqlCommand = new SqlCommand("dbo.rxrCRTktDetailDataEl");
            sqlCommand.CommandType = CommandType.StoredProcedure;
            sqlCommand.CommandTimeout = 600;

            sqlCommand.Parameters.Add("@TicketID", SqlDbType.VarChar).Value = ticketNumber;

            sqlCommand.Connection = new SqlConnection("Persist Security Info=False; User ID=OBIRead; Password=OB1R3@d; Initial Catalog=OBI; Server=obidb-wc-p01");

            SqlDataAdapter dataAdapter = new SqlDataAdapter(sqlCommand);
            DataSet dataSet = new DataSet();

            try
            {
                using (sqlCommand.Connection)
                {
                    sqlCommand.Connection.Open();

                    using (sqlCommand)
                    {
                        dataAdapter.Fill(dataSet, "dataSet");
                    }

                    sqlCommand.Connection.Close();

                    for (int indexRow = 0; indexRow <= dataSet.Tables[0].Rows.Count - 1; indexRow++)
                    {
                        CRTicketAttribute cRTicketAttribute = new CRTicketAttribute();

                        cRTicketAttribute.ticketId = dataSet.Tables[0].Rows[indexRow]["TICKET_ID"].ToString();
                        cRTicketAttribute.attributeLabel = dataSet.Tables[0].Rows[indexRow]["ATTR_LABEL"].ToString();
                        cRTicketAttribute.attributeValue = dataSet.Tables[0].Rows[indexRow]["VALUE_CHAR"].ToString();

                        returnData.data.Add(cRTicketAttribute);
                    }

                    returnData.success = true;
                }
            }
            catch (System.Exception error)
            {
                ErrorMessage errorMessage = new ErrorMessage();
                errorMessage.status = error.Source;
                errorMessage.message = error.Message;

                returnData.errors.Add(errorMessage);
                returnData.success = false;
            }

            return returnData;
        }

        public static ReturnData<List<CRTicketWorkLog>> GetCRTicketWorkLog(String ticketNumber, String source)
        {
            ReturnData<List<CRTicketWorkLog>> returnData = new ReturnData<List<CRTicketWorkLog>>();
            returnData.data = new List<CRTicketWorkLog>();

            SqlCommand sqlCommand = new SqlCommand("dbo.rxrCRTktDetailWorklog");
            sqlCommand.CommandType = CommandType.StoredProcedure;
            sqlCommand.CommandTimeout = 600;

            sqlCommand.Parameters.Add("@TicketID", SqlDbType.VarChar).Value = ticketNumber;
            sqlCommand.Parameters.Add("@Source", SqlDbType.VarChar).Value = source;

            sqlCommand.Connection = new SqlConnection("Persist Security Info=False; User ID=OBIRead; Password=OB1R3@d; Initial Catalog=OBI; Server=obidb-wc-p01");

            SqlDataAdapter dataAdapter = new SqlDataAdapter(sqlCommand);
            DataSet dataSet = new DataSet();

            try
            {
                using (sqlCommand.Connection)
                {
                    sqlCommand.Connection.Open();

                    using (sqlCommand)
                    {
                        dataAdapter.Fill(dataSet, "dataSet");
                    }

                    sqlCommand.Connection.Close();

                    for (int indexRow = 0; indexRow <= dataSet.Tables[0].Rows.Count - 1; indexRow++)
                    {
                        CRTicketWorkLog cRTicketWorkLog = new CRTicketWorkLog();

                        cRTicketWorkLog.ticketId = dataSet.Tables[0].Rows[indexRow]["TICKET_ID"].ToString();
                        cRTicketWorkLog.createDate = dataSet.Tables[0].Rows[indexRow]["CREATE_DATE"].ToString();
                        cRTicketWorkLog.submitter = dataSet.Tables[0].Rows[indexRow]["SUBMITTER"].ToString();
                        cRTicketWorkLog.source = dataSet.Tables[0].Rows[indexRow]["SOURCE"].ToString();
                        cRTicketWorkLog.subject = dataSet.Tables[0].Rows[indexRow]["SUBJECT"].ToString();
                        cRTicketWorkLog.details = dataSet.Tables[0].Rows[indexRow]["DETAILS"].ToString();

                        returnData.data.Add(cRTicketWorkLog);
                    }

                    returnData.success = true;
                }
            }
            catch (System.Exception error)
            {
                ErrorMessage errorMessage = new ErrorMessage();
                errorMessage.status = error.Source;
                errorMessage.message = error.Message;

                returnData.errors.Add(errorMessage);
                returnData.success = false;
            }

            return returnData;
        }

        public static ReturnData<SITicketGeneralDetails> GetSITicketGeneralDetails(String ticketNumber)
        {

            ReturnData<SITicketGeneralDetails> returnData = new ReturnData<SITicketGeneralDetails>();
            returnData.data = new SITicketGeneralDetails();

            SqlCommand sqlCommand = new SqlCommand("dbo.rxrSITktDetailGen");
            sqlCommand.CommandType = CommandType.StoredProcedure;
            sqlCommand.CommandTimeout = 600;

            sqlCommand.Parameters.Add("@TicketID", SqlDbType.VarChar).Value = ticketNumber;


            sqlCommand.Connection = new SqlConnection("Persist Security Info=False; User ID=OBIRead; Password=OB1R3@d; Initial Catalog=OBI; Server=obidb-wc-p01");

            SqlDataAdapter dataAdapter = new SqlDataAdapter(sqlCommand);
            DataSet dataSet = new DataSet();

            try
            {
                using (sqlCommand.Connection)
                {
                    sqlCommand.Connection.Open();

                    using (sqlCommand)
                    {
                        dataAdapter.Fill(dataSet, "dataSet");
                    }

                    sqlCommand.Connection.Close();


                    returnData.data.ticketId = dataSet.Tables[0].Rows[0]["TICKET_ID"].ToString();
                    returnData.data.incidentSummary = dataSet.Tables[0].Rows[0]["INCIDENT_SUMMARY"].ToString().Trim();
                    returnData.data.supportAreaName = dataSet.Tables[0].Rows[0]["SUPPORT_AREA_NAME"].ToString();
                    returnData.data.submittingName = dataSet.Tables[0].Rows[0]["SUBMITTING_NAME"].ToString();
                    returnData.data.queueName = dataSet.Tables[0].Rows[0]["QUEUE_NAME"].ToString();
                    returnData.data.fullName = dataSet.Tables[0].Rows[0]["FULL_NAME"].ToString();
                    returnData.data.siSeverity = dataSet.Tables[0].Rows[0]["SI_SEVERITY"].ToString();
                    returnData.data.severity = dataSet.Tables[0].Rows[0]["SEVERITY"].ToString();
                    returnData.data.siStatus = dataSet.Tables[0].Rows[0]["SI_STATUS"].ToString();
                    returnData.data.siPriority = dataSet.Tables[0].Rows[0]["SI_PRIORITY"].ToString();
                    returnData.data.priority = dataSet.Tables[0].Rows[0]["PRIORITY"].ToString();
                    returnData.data.product = dataSet.Tables[0].Rows[0]["PRODUCT"].ToString();
                    returnData.data.createDate = dataSet.Tables[0].Rows[0]["CREATE_DATE"].ToString();
                    returnData.data.assignedDate = dataSet.Tables[0].Rows[0]["ASSIGNED_DATE"].ToString();
                    returnData.data.telephonyAffected = dataSet.Tables[0].Rows[0]["TELEPHONY_AFFECTED"].ToString();
                    returnData.data.videoAffected = dataSet.Tables[0].Rows[0]["VIDEO_AFFECTED"].ToString();
                    returnData.data.hsdAffected = dataSet.Tables[0].Rows[0]["HSD_AFFECTED"].ToString();
                    returnData.data.totalCustomersAffected = dataSet.Tables[0].Rows[0]["TOTAL_CUST_AFFECTED"].ToString();
                    returnData.data.nextAction = dataSet.Tables[0].Rows[0]["NEXT_ACTION"].ToString();
                    returnData.data.problemCode = dataSet.Tables[0].Rows[0]["PROBLEM_CODE"].ToString();
                    returnData.data.source = dataSet.Tables[0].Rows[0]["SOURCE"].ToString();
                    returnData.data.category = dataSet.Tables[0].Rows[0]["CATEGORY"].ToString();
                    returnData.data.subCategory = dataSet.Tables[0].Rows[0]["SUBCATEGORY"].ToString();
                    returnData.data.causeCode = dataSet.Tables[0].Rows[0]["CAUSE_CODE"].ToString();
                    returnData.data.causeDescription = dataSet.Tables[0].Rows[0]["CAUSE_DESCRIPTION"].ToString();
                    returnData.data.causeCategory = dataSet.Tables[0].Rows[0]["CAUSE_CATEGORY"].ToString();
                    returnData.data.causeSubCategory = dataSet.Tables[0].Rows[0]["CAUSE_SUBCATEGORY"].ToString();
                    returnData.data.solutionCode = dataSet.Tables[0].Rows[0]["SOLUTION_CODE"].ToString();
                    returnData.data.solutionDescription = dataSet.Tables[0].Rows[0]["SOLUTION_DESCRIPTION"].ToString();
                    returnData.data.solutionCategory = dataSet.Tables[0].Rows[0]["SOLUTION_CATEGORY"].ToString();
                    returnData.data.solutionSubCategory = dataSet.Tables[0].Rows[0]["SOLUTION_SUBCATEGORY"].ToString();
                    returnData.data.externalAssignee = dataSet.Tables[0].Rows[0]["EXTERNAL_ASSIGNEE"].ToString();
                    returnData.data.externalReference = dataSet.Tables[0].Rows[0]["EXTERNAL_REFERENCE"].ToString();
                    returnData.data.alarmDate = dataSet.Tables[0].Rows[0]["ALARM_DATE"].ToString();
                    returnData.data.ttrStart = dataSet.Tables[0].Rows[0]["TTR_START"].ToString();
                    returnData.data.ttrStop = dataSet.Tables[0].Rows[0]["TTR_STOP"].ToString();
                    returnData.data.indicatorCity = dataSet.Tables[0].Rows[0]["INDICATOR_CITY"].ToString();
                    returnData.data.indicatorState = dataSet.Tables[0].Rows[0]["INDICATOR_STATE"].ToString();
                    returnData.data.networkElement = dataSet.Tables[0].Rows[0]["NETWORK_ELEMENT"].ToString();
                    returnData.data.correlationType = dataSet.Tables[0].Rows[0]["CORRELATION_TYPE"].ToString();
                    returnData.data.serviceCondition = dataSet.Tables[0].Rows[0]["SERVICE_CONDITION"].ToString();
                    returnData.data.careImpact = dataSet.Tables[0].Rows[0]["CARE_IMPACT"].ToString();
                    returnData.data.estimatedServiceRestore = dataSet.Tables[0].Rows[0]["ESTIMATED_SERVICE_RESTORE"].ToString();
                    returnData.data.siLastModifiedBy = dataSet.Tables[0].Rows[0]["SI_LAST_MODIFIED_BY"].ToString();
                    returnData.data.lastModifiedBy = dataSet.Tables[0].Rows[0]["LAST_MODIFIED_BY"].ToString();
                    returnData.data.modifiedDate = dataSet.Tables[0].Rows[0]["MODIFIED_DATE"].ToString();
                    returnData.data.siResolvedBy = dataSet.Tables[0].Rows[0]["SI_RESOLVED_BY"].ToString();
                    returnData.data.resolvedBy = dataSet.Tables[0].Rows[0]["RESOLVED_BY"].ToString();
                    returnData.data.contactDepartment = dataSet.Tables[0].Rows[0]["CONTACT_DEPARTMENT"].ToString();
                    returnData.data.contactPhone = dataSet.Tables[0].Rows[0]["CONTACT_PHONE"].ToString();
                    returnData.data.regionName = dataSet.Tables[0].Rows[0]["REGION_NAME"].ToString();
                    returnData.data.duration = dataSet.Tables[0].Rows[0]["DURATION"].ToString();


                    returnData.success = true;
                }
            }
            catch (System.Exception error)
            {
                ErrorMessage errorMessage = new ErrorMessage();
                errorMessage.status = error.Source;
                errorMessage.message = error.Message;

                returnData.errors.Add(errorMessage);
                returnData.success = false;
            }

            return returnData;
        }

        public static ReturnData<List<SITicketAttribute>> GetSITicketAttributes(String ticketNumber)
        {
            ReturnData<List<SITicketAttribute>> returnData = new ReturnData<List<SITicketAttribute>>();
            returnData.data = new List<SITicketAttribute>();

            SqlCommand sqlCommand = new SqlCommand("dbo.rxrSITktDetailDataEl");
            sqlCommand.CommandType = CommandType.StoredProcedure;
            sqlCommand.CommandTimeout = 600;

            sqlCommand.Parameters.Add("@TicketID", SqlDbType.VarChar).Value = ticketNumber;

            sqlCommand.Connection = new SqlConnection("Persist Security Info=False; User ID=OBIRead; Password=OB1R3@d; Initial Catalog=OBI; Server=obidb-wc-p01");

            SqlDataAdapter dataAdapter = new SqlDataAdapter(sqlCommand);
            DataSet dataSet = new DataSet();

            try
            {
                using (sqlCommand.Connection)
                {
                    sqlCommand.Connection.Open();

                    using (sqlCommand)
                    {
                        dataAdapter.Fill(dataSet, "dataSet");
                    }

                    sqlCommand.Connection.Close();

                    for (int indexRow = 0; indexRow <= dataSet.Tables[0].Rows.Count - 1; indexRow++)
                    {
                        SITicketAttribute sITicketAttribute = new SITicketAttribute();

                        sITicketAttribute.ticketId = dataSet.Tables[0].Rows[indexRow]["TICKET_ID"].ToString();
                        sITicketAttribute.attributeLabel = dataSet.Tables[0].Rows[indexRow]["ATTR_LABEL"].ToString();
                        sITicketAttribute.attributeValue = dataSet.Tables[0].Rows[indexRow]["VALUE_CHAR"].ToString();

                        returnData.data.Add(sITicketAttribute);
                    }

                    returnData.success = true;
                }
            }
            catch (System.Exception error)
            {
                ErrorMessage errorMessage = new ErrorMessage();
                errorMessage.status = error.Source;
                errorMessage.message = error.Message;

                returnData.errors.Add(errorMessage);
                returnData.success = false;
            }

            return returnData;
        }

        public static ReturnData<List<SITicketCustomerTicket>> GetSITicketCustomerTickets(String ticketNumber)
        {
            ReturnData<List<SITicketCustomerTicket>> returnData = new ReturnData<List<SITicketCustomerTicket>>();
            returnData.data = new List<SITicketCustomerTicket>();

            SqlCommand sqlCommand = new SqlCommand("dbo.rxrSITktDetailCust");
            sqlCommand.CommandType = CommandType.StoredProcedure;

            sqlCommand.Parameters.Add("@TicketID", SqlDbType.VarChar).Value = ticketNumber;

            sqlCommand.Connection = new SqlConnection("Persist Security Info=False; User ID=SALPUser; Password=OBI@dm1n; Initial Catalog=OBI; Server=obidb-wc-p01");

            SqlDataAdapter dataAdapter = new SqlDataAdapter(sqlCommand);
            DataSet dataSet = new DataSet();

            try
            {
                using (sqlCommand.Connection)
                {
                    sqlCommand.Connection.Open();

                    using (sqlCommand)
                    {
                        dataAdapter.Fill(dataSet, "dataSet");
                    }

                    sqlCommand.Connection.Close();

                    for (int indexRow = 0; indexRow <= dataSet.Tables[0].Rows.Count - 1; indexRow++)
                    {
                        SITicketCustomerTicket sITicketCustomerTicket = new SITicketCustomerTicket();

                        sITicketCustomerTicket.customerTicketId = dataSet.Tables[0].Rows[indexRow]["CUST_TICKET_ID"].ToString();
                        sITicketCustomerTicket.problemSummary = dataSet.Tables[0].Rows[indexRow]["PROBLEM_SUMMARY"].ToString();
                        sITicketCustomerTicket.createDate = dataSet.Tables[0].Rows[indexRow]["CREATE_DATE"].ToString();
                        sITicketCustomerTicket.contactName = dataSet.Tables[0].Rows[indexRow]["CONTACT_NAME"].ToString();
                        sITicketCustomerTicket.status = dataSet.Tables[0].Rows[indexRow]["STATUS"].ToString();
                        sITicketCustomerTicket.node = dataSet.Tables[0].Rows[indexRow]["NODE"].ToString();

                        returnData.data.Add(sITicketCustomerTicket);
                    }


                    returnData.success = true;
                }
            }
            catch (System.Exception error)
            {
                ErrorMessage errorMessage = new ErrorMessage();
                errorMessage.status = error.Source;
                errorMessage.message = error.Message;

                returnData.errors.Add(errorMessage);
                returnData.success = false;
            }

            return returnData;
        }


        public static ReturnData<List<SITicketAffectedElement>> GetSITicketAffectedElements(String ticketNumber)
        {
            ReturnData<List<SITicketAffectedElement>> returnData = new ReturnData<List<SITicketAffectedElement>>();
            returnData.data = new List<SITicketAffectedElement>();

            SqlCommand sqlCommand = new SqlCommand("dbo.rxrSITktDetailAffEle");
            sqlCommand.CommandType = CommandType.StoredProcedure;
            sqlCommand.CommandTimeout = 600;

            sqlCommand.Parameters.Add("@TicketID", SqlDbType.VarChar).Value = ticketNumber;

            sqlCommand.Connection = new SqlConnection("Persist Security Info=False; User ID=OBIRead; Password=OB1R3@d; Initial Catalog=OBI; Server=obidb-wc-p01");

            SqlDataAdapter dataAdapter = new SqlDataAdapter(sqlCommand);
            DataSet dataSet = new DataSet();

            try
            {
                using (sqlCommand.Connection)
                {
                    sqlCommand.Connection.Open();

                    using (sqlCommand)
                    {
                        dataAdapter.Fill(dataSet, "dataSet");
                    }

                    sqlCommand.Connection.Close();

                    for (int indexRow = 0; indexRow <= dataSet.Tables[0].Rows.Count - 1; indexRow++)
                    {
                        SITicketAffectedElement sITicketAffectedElement = new SITicketAffectedElement();

                        sITicketAffectedElement.sourceTicket = dataSet.Tables[0].Rows[indexRow]["SOURCE_TICKET"].ToString();
                        sITicketAffectedElement.status = dataSet.Tables[0].Rows[indexRow]["STATUS"].ToString();
                        sITicketAffectedElement.entryId = dataSet.Tables[0].Rows[indexRow]["ENTRY_ID"].ToString();
                        sITicketAffectedElement.serviceAffected = dataSet.Tables[0].Rows[indexRow]["SERVICE_AFFECT"].ToString();
                        sITicketAffectedElement.elementName = dataSet.Tables[0].Rows[indexRow]["ELEMENT_NAME"].ToString();
                        sITicketAffectedElement.elementType = dataSet.Tables[0].Rows[indexRow]["ELEMENT_TYPE"].ToString();
                        sITicketAffectedElement.systemName = dataSet.Tables[0].Rows[indexRow]["SYSTEM_NAME"].ToString();
                        sITicketAffectedElement.telephonyAffected = dataSet.Tables[0].Rows[indexRow]["TELEPHONY_AFF_SUBS"].ToString();
                        sITicketAffectedElement.videoAffected = dataSet.Tables[0].Rows[indexRow]["VIDEO_AFF_SUBS"].ToString();
                        sITicketAffectedElement.hSDAffected = dataSet.Tables[0].Rows[indexRow]["HSD_AFF_SUBS"].ToString();
                        sITicketAffectedElement.totalAffected = dataSet.Tables[0].Rows[indexRow]["TOTAL_AFF_SUBS"].ToString();
                        sITicketAffectedElement.aeStatus = dataSet.Tables[0].Rows[indexRow]["AE_STATUS"].ToString();
                        sITicketAffectedElement.actualStart = dataSet.Tables[0].Rows[indexRow]["ACTUAL_START"].ToString();
                        sITicketAffectedElement.actualEnd = dataSet.Tables[0].Rows[indexRow]["ACTUAL_END"].ToString();


                        returnData.data.Add(sITicketAffectedElement);
                    }

                    returnData.success = true;
                }
            }
            catch (System.Exception error)
            {
                ErrorMessage errorMessage = new ErrorMessage();
                errorMessage.status = error.Source;
                errorMessage.message = error.Message;

                returnData.errors.Add(errorMessage);
                returnData.success = false;
            }

            return returnData;
        }

        public static ReturnData<List<SITicketWorkLog>> GetSITicketWorkLog(String ticketNumber, String source)
        {
            ReturnData<List<SITicketWorkLog>> returnData = new ReturnData<List<SITicketWorkLog>>();
            returnData.data = new List<SITicketWorkLog>();

            SqlCommand sqlCommand = new SqlCommand("dbo.rxrSITktDetailWorklog");
            sqlCommand.CommandType = CommandType.StoredProcedure;
            sqlCommand.CommandTimeout = 600;

            sqlCommand.Parameters.Add("@TicketID", SqlDbType.VarChar).Value = ticketNumber;
            sqlCommand.Parameters.Add("@Source", SqlDbType.VarChar).Value = source;

            sqlCommand.Connection = new SqlConnection("Persist Security Info=False; User ID=OBIRead; Password=OB1R3@d; Initial Catalog=OBI; Server=obidb-wc-p01");

            SqlDataAdapter dataAdapter = new SqlDataAdapter(sqlCommand);
            DataSet dataSet = new DataSet();

            try
            {
                using (sqlCommand.Connection)
                {
                    sqlCommand.Connection.Open();

                    using (sqlCommand)
                    {
                        dataAdapter.Fill(dataSet, "dataSet");
                    }

                    sqlCommand.Connection.Close();

                    for (int indexRow = 0; indexRow <= dataSet.Tables[0].Rows.Count - 1; indexRow++)
                    {
                        SITicketWorkLog sITicketWorkLog = new SITicketWorkLog();

                        sITicketWorkLog.ticketId = dataSet.Tables[0].Rows[indexRow]["TICKET_ID"].ToString();
                        sITicketWorkLog.createDate = dataSet.Tables[0].Rows[indexRow]["CREATE_DATE"].ToString();
                        sITicketWorkLog.submitter = dataSet.Tables[0].Rows[indexRow]["SUBMITTER"].ToString();
                        sITicketWorkLog.source = dataSet.Tables[0].Rows[indexRow]["SOURCE"].ToString();
                        sITicketWorkLog.subject = dataSet.Tables[0].Rows[indexRow]["SUBJECT"].ToString();
                        sITicketWorkLog.details = dataSet.Tables[0].Rows[indexRow]["DETAILS"].ToString();

                        returnData.data.Add(sITicketWorkLog);
                    }

                    returnData.success = true;
                }
            }
            catch (System.Exception error)
            {
                ErrorMessage errorMessage = new ErrorMessage();
                errorMessage.status = error.Source;
                errorMessage.message = error.Message;

                returnData.errors.Add(errorMessage);
                returnData.success = false;
            }

            return returnData;
        }

        public static ReturnData<OutageElement> GetOETicketGeneralDetails(String ticketNumber)
        {

            ReturnData<OutageElement> returnData = new ReturnData<OutageElement>();
            returnData.data = new OutageElement();

            SqlCommand sqlCommand = new SqlCommand("dbo.rxrOEDetailAffEle");
            sqlCommand.CommandType = CommandType.StoredProcedure;
            sqlCommand.CommandTimeout = 600;

            sqlCommand.Parameters.Add("@TicketID", SqlDbType.VarChar).Value = ticketNumber;


            sqlCommand.Connection = new SqlConnection("Persist Security Info=False; User ID=OBIRead; Password=OB1R3@d; Initial Catalog=OBI; Server=obidb-wc-p01");

            SqlDataAdapter dataAdapter = new SqlDataAdapter(sqlCommand);
            DataSet dataSet = new DataSet();

            try
            {
                using (sqlCommand.Connection)
                {
                    sqlCommand.Connection.Open();

                    using (sqlCommand)
                    {
                        dataAdapter.Fill(dataSet, "dataSet");
                    }

                    sqlCommand.Connection.Close();


                    returnData.data.ticketId = dataSet.Tables[0].Rows[0]["TICKET_ID"].ToString();
                    returnData.data.entryId = dataSet.Tables[0].Rows[0]["ENTRY_ID"].ToString();
                    returnData.data.serviceAffected = dataSet.Tables[0].Rows[0]["SERVICE_AFFECT"].ToString();
                    returnData.data.elementName = dataSet.Tables[0].Rows[0]["ELEMENT_NAME"].ToString();
                    returnData.data.telephonyAffected = dataSet.Tables[0].Rows[0]["TELEPHONY_AFF_SUBS"].ToString();
                    returnData.data.hsdAffected = dataSet.Tables[0].Rows[0]["HSD_AFF_SUBS"].ToString();
                    returnData.data.videoAffected = dataSet.Tables[0].Rows[0]["VIDEO_AFF_SUBS"].ToString();
                    returnData.data.totalAffected = dataSet.Tables[0].Rows[0]["TOTAL_AFF_SUBS"].ToString();
                    returnData.data.aeStatus = dataSet.Tables[0].Rows[0]["AE_STATUS"].ToString();
                    returnData.data.elementType = dataSet.Tables[0].Rows[0]["ELEMENT_TYPE"].ToString();
                    returnData.data.plannedFlag = dataSet.Tables[0].Rows[0]["PLANNED_FLAG"].ToString();
                    returnData.data.plannedStart = dataSet.Tables[0].Rows[0]["PLANNED_START"].ToString();
                    returnData.data.plannedEnd = dataSet.Tables[0].Rows[0]["PLANNED_END"].ToString();
                    returnData.data.duration = dataSet.Tables[0].Rows[0]["DURATION"].ToString();
                    returnData.data.source = dataSet.Tables[0].Rows[0]["SOURCE"].ToString();
                    returnData.data.itemFocus = dataSet.Tables[0].Rows[0]["ITEM_FOCUS"].ToString();
                    returnData.data.regionId = dataSet.Tables[0].Rows[0]["REGION_ID"].ToString();
                    returnData.data.regionName = dataSet.Tables[0].Rows[0]["REGION_NAME"].ToString();
                    returnData.data.systemId = dataSet.Tables[0].Rows[0]["SYSTEM_ID"].ToString();
                    returnData.data.systemName = dataSet.Tables[0].Rows[0]["SYSTEM_NAME"].ToString();
                    returnData.data.indicatorState = dataSet.Tables[0].Rows[0]["INDICATOR_STATE"].ToString();
                    returnData.data.location = dataSet.Tables[0].Rows[0]["LOCATION"].ToString();
                    returnData.data.product = dataSet.Tables[0].Rows[0]["PRODUCT"].ToString();
                    returnData.data.commercialUse = dataSet.Tables[0].Rows[0]["COMMERCIAL_USE"].ToString();
                    returnData.data.messageInternal = dataSet.Tables[0].Rows[0]["MESSAGE_INTERNAL"].ToString();
                    returnData.data.linkProblemCode = dataSet.Tables[0].Rows[0]["LINK_PROBLEM_CODE"].ToString();
                    returnData.data.problemDescription = dataSet.Tables[0].Rows[0]["PROBLEM_DESCRIPTION"].ToString();
                    returnData.data.category = dataSet.Tables[0].Rows[0]["CATEGORY"].ToString();
                    returnData.data.subcategory = dataSet.Tables[0].Rows[0]["SUBCATEGORY"].ToString();
                    returnData.data.causeCode = dataSet.Tables[0].Rows[0]["CAUSE_CODE"].ToString();
                    returnData.data.causeDescription = dataSet.Tables[0].Rows[0]["CAUSE_DESCRIPTION"].ToString();
                    returnData.data.causeCategory = dataSet.Tables[0].Rows[0]["CAUSE_CATEGORY"].ToString();
                    returnData.data.causeSubcategory = dataSet.Tables[0].Rows[0]["CAUSE_SUBCATEGORY"].ToString();
                    returnData.data.solutionCode = dataSet.Tables[0].Rows[0]["SOLUTION_CODE"].ToString();
                    returnData.data.solutionDescription = dataSet.Tables[0].Rows[0]["SOLUTION_DESCRIPTION"].ToString();
                    returnData.data.solutionCategory = dataSet.Tables[0].Rows[0]["SOLUTION_CATEGORY"].ToString();
                    returnData.data.solutionSubcategory = dataSet.Tables[0].Rows[0]["SOLUTION_SUBCATEGORY"].ToString();
                    returnData.data.originatingTicket = dataSet.Tables[0].Rows[0]["ORIGINATING_TICKET"].ToString();
                    returnData.data.submitter = dataSet.Tables[0].Rows[0]["SUBMITTER"].ToString();
                    returnData.data.lastModifiedBy = dataSet.Tables[0].Rows[0]["LAST_MODIFIED_BY"].ToString();
                    returnData.data.actualStart = dataSet.Tables[0].Rows[0]["ACTUAL_START"].ToString();
                    returnData.data.actualEnd = dataSet.Tables[0].Rows[0]["ACTUAL_END"].ToString();
                    returnData.data.etr = dataSet.Tables[0].Rows[0]["ETR"].ToString();
                    returnData.data.createDate = dataSet.Tables[0].Rows[0]["CREATE_DATE"].ToString();
                    returnData.data.modifiedDate = dataSet.Tables[0].Rows[0]["MODIFIED_DATE"].ToString();

                    returnData.success = true;
                }
            }
            catch (System.Exception error)
            {
                ErrorMessage errorMessage = new ErrorMessage();
                errorMessage.status = error.Source;
                errorMessage.message = error.Message;

                returnData.errors.Add(errorMessage);
                returnData.success = false;
            }

            return returnData;
        }

        public static ReturnData<OETicketAffectedElement> GetOETicketAffectedElements(String ticketNumber)
        {
            ReturnData<OETicketAffectedElement> returnData = new ReturnData<OETicketAffectedElement>();
            returnData.data = new OETicketAffectedElement();

            SqlCommand sqlCommand = new SqlCommand("dbo.rxrOEDetailAffEle");
            sqlCommand.CommandType = CommandType.StoredProcedure;
            sqlCommand.CommandTimeout = 600;

            sqlCommand.Parameters.Add("@TicketID", SqlDbType.VarChar).Value = ticketNumber;


            sqlCommand.Connection = new SqlConnection("Persist Security Info=False; User ID=OBIRead; Password=OB1R3@d; Initial Catalog=OBI; Server=obidb-wc-p01");

            SqlDataAdapter dataAdapter = new SqlDataAdapter(sqlCommand);
            DataSet dataSet = new DataSet();

            try
            {
                using (sqlCommand.Connection)
                {
                    sqlCommand.Connection.Open();

                    using (sqlCommand)
                    {
                        dataAdapter.Fill(dataSet, "dataSet");
                    }

                    sqlCommand.Connection.Close();


                    returnData.data.ticketId = dataSet.Tables[0].Rows[0]["TICKET_ID"].ToString();
                    returnData.data.entryId = dataSet.Tables[0].Rows[0]["ENTRY_ID"].ToString();
                    returnData.data.serviceAffected = dataSet.Tables[0].Rows[0]["SERVICE_AFFECT"].ToString();
                    returnData.data.elementName = dataSet.Tables[0].Rows[0]["ELEMENT_NAME"].ToString();
                    returnData.data.telephonyAffected = dataSet.Tables[0].Rows[0]["TELEPHONY_AFF_SUBS"].ToString();
                    returnData.data.hsdAffected = dataSet.Tables[0].Rows[0]["HSD_AFF_SUBS"].ToString();
                    returnData.data.videoAffected = dataSet.Tables[0].Rows[0]["VIDEO_AFF_SUBS"].ToString();
                    returnData.data.totalAffected = dataSet.Tables[0].Rows[0]["TOTAL_AFF_SUBS"].ToString();
                    returnData.data.aeStatus = dataSet.Tables[0].Rows[0]["AE_STATUS"].ToString();
                    returnData.data.elementType = dataSet.Tables[0].Rows[0]["ELEMENT_TYPE"].ToString();
                    if (dataSet.Tables[0].Rows[0]["PLANNED_FLAG"].ToString() == "1")
                    {
                        returnData.data.plannedFlag = "Planned";
                    }
                    else
                    {
                        returnData.data.plannedFlag = "Unplanned";
                    }
                    //returnData.data.plannedFlag = dataSet.Tables[0].Rows[0]["PLANNED_FLAG"].ToString();
                    returnData.data.systemName = dataSet.Tables[0].Rows[0]["SYSTEM_NAME"].ToString();
                    returnData.data.messageInterval = dataSet.Tables[0].Rows[0]["MESSAGE_INTERNAL"].ToString();
                    returnData.data.causeCode = dataSet.Tables[0].Rows[0]["CAUSE_CODE"].ToString();
                    returnData.data.causeDescription = dataSet.Tables[0].Rows[0]["CAUSE_DESCRIPTION"].ToString();
                    returnData.data.solutionCode = dataSet.Tables[0].Rows[0]["SOLUTION_CODE"].ToString();
                    returnData.data.solutionDescription = dataSet.Tables[0].Rows[0]["SOLUTION_DESCRIPTION"].ToString();
                    returnData.data.originatingTicket = dataSet.Tables[0].Rows[0]["ORIGINATING_TICKET"].ToString();
                    returnData.data.submitter = dataSet.Tables[0].Rows[0]["SUBMITTER"].ToString();
                    returnData.data.lastModifiedBy = dataSet.Tables[0].Rows[0]["LAST_MODIFIED_BY"].ToString();
                    returnData.data.actualStart = dataSet.Tables[0].Rows[0]["ACTUAL_START"].ToString();
                    returnData.data.actualEnd = dataSet.Tables[0].Rows[0]["ACTUAL_END"].ToString();


                    returnData.success = true;
                }
            }
            catch (System.Exception error)
            {
                ErrorMessage errorMessage = new ErrorMessage();
                errorMessage.status = error.Source;
                errorMessage.message = error.Message;

                returnData.errors.Add(errorMessage);
                returnData.success = false;
            }

            return returnData;
        }

        public static ReturnData<List<OETicketAttribute>> GetOETicketAttributes(String ticketNumber)
        {
            ReturnData<List<OETicketAttribute>> returnData = new ReturnData<List<OETicketAttribute>>();
            returnData.data = new List<OETicketAttribute>();

            SqlCommand sqlCommand = new SqlCommand("dbo.rxrOEDetailDataEl");
            sqlCommand.CommandType = CommandType.StoredProcedure;
            sqlCommand.CommandTimeout = 600;

            sqlCommand.Parameters.Add("@TicketID", SqlDbType.VarChar).Value = ticketNumber;

            sqlCommand.Connection = new SqlConnection("Persist Security Info=False; User ID=OBIRead; Password=OB1R3@d; Initial Catalog=OBI; Server=obidb-wc-p01");

            SqlDataAdapter dataAdapter = new SqlDataAdapter(sqlCommand);
            DataSet dataSet = new DataSet();

            try
            {
                using (sqlCommand.Connection)
                {
                    sqlCommand.Connection.Open();

                    using (sqlCommand)
                    {
                        dataAdapter.Fill(dataSet, "dataSet");
                    }

                    sqlCommand.Connection.Close();

                    for (int indexRow = 0; indexRow <= dataSet.Tables[0].Rows.Count - 1; indexRow++)
                    {
                        OETicketAttribute oETicketAttribute = new OETicketAttribute();

                        oETicketAttribute.sourceTicket = dataSet.Tables[0].Rows[indexRow]["SOURCE_TICKET"].ToString();
                        oETicketAttribute.attributeLabel = dataSet.Tables[0].Rows[indexRow]["ATTR_LABEL"].ToString();
                        oETicketAttribute.attributeValue = dataSet.Tables[0].Rows[indexRow]["VALUE_CHAR"].ToString();

                        returnData.data.Add(oETicketAttribute);
                    }

                    returnData.success = true;
                }
            }
            catch (System.Exception error)
            {
                ErrorMessage errorMessage = new ErrorMessage();
                errorMessage.status = error.Source;
                errorMessage.message = error.Message;

                returnData.errors.Add(errorMessage);
                returnData.success = false;
            }

            return returnData;
        }


        public static ReturnData<List<OETicketWorkLog>> GetOETicketWorkLog(String ticketNumber, String source)
        {
            ReturnData<List<OETicketWorkLog>> returnData = new ReturnData<List<OETicketWorkLog>>();
            returnData.data = new List<OETicketWorkLog>();

            SqlCommand sqlCommand = new SqlCommand("dbo.rxrOEDetailWorklog");
            sqlCommand.CommandType = CommandType.StoredProcedure;
            sqlCommand.CommandTimeout = 600;

            sqlCommand.Parameters.Add("@TicketID", SqlDbType.VarChar).Value = ticketNumber;
            sqlCommand.Parameters.Add("@Source", SqlDbType.VarChar).Value = source;

            sqlCommand.Connection = new SqlConnection("Persist Security Info=False; User ID=OBIRead; Password=OB1R3@d; Initial Catalog=OBI; Server=obidb-wc-p01");

            SqlDataAdapter dataAdapter = new SqlDataAdapter(sqlCommand);
            DataSet dataSet = new DataSet();

            try
            {
                using (sqlCommand.Connection)
                {
                    sqlCommand.Connection.Open();

                    using (sqlCommand)
                    {
                        dataAdapter.Fill(dataSet, "dataSet");
                    }

                    sqlCommand.Connection.Close();

                    for (int indexRow = 0; indexRow <= dataSet.Tables[0].Rows.Count - 1; indexRow++)
                    {
                        OETicketWorkLog oETicketWorkLog = new OETicketWorkLog();

                        oETicketWorkLog.sourceTicket = dataSet.Tables[0].Rows[indexRow]["SOURCE_TICKET"].ToString();
                        oETicketWorkLog.createDate = dataSet.Tables[0].Rows[indexRow]["CREATE_DATE"].ToString();
                        oETicketWorkLog.submitter = dataSet.Tables[0].Rows[indexRow]["SUBMITTER"].ToString();
                        oETicketWorkLog.source = dataSet.Tables[0].Rows[indexRow]["SOURCE"].ToString();
                        oETicketWorkLog.subject = dataSet.Tables[0].Rows[indexRow]["SUBJECT"].ToString();
                        oETicketWorkLog.details = dataSet.Tables[0].Rows[indexRow]["DETAILS"].ToString();

                        returnData.data.Add(oETicketWorkLog);
                    }

                    returnData.success = true;
                }
            }
            catch (System.Exception error)
            {
                ErrorMessage errorMessage = new ErrorMessage();
                errorMessage.status = error.Source;
                errorMessage.message = error.Message;

                returnData.errors.Add(errorMessage);
                returnData.success = false;
            }

            return returnData;
        }



        public static List<CMTreeMapRoot> GetCMTreeView(String name, String month, String year)
        {
            List<CMTreeMapRoot> rootData = new List<CMTreeMapRoot>();

            SqlCommand sqlCommand = new SqlCommand("dbo.Get_CM_TreeView");
            sqlCommand.CommandType = CommandType.StoredProcedure;
            sqlCommand.CommandTimeout = 600;


            sqlCommand.Parameters.Add("@name", SqlDbType.VarChar).Value = name;
            sqlCommand.Parameters.Add("@maint_month", SqlDbType.VarChar).Value = month;
            sqlCommand.Parameters.Add("@main_year", SqlDbType.VarChar).Value = year;

            sqlCommand.Connection = new SqlConnection("Persist Security Info=False; User ID=OBIRead; Password=OB1R3@d; Initial Catalog=OBI; Server=obidb-wc-p01");

            SqlDataAdapter dataAdapter = new SqlDataAdapter(sqlCommand);
            DataSet dataSet = new DataSet();

            try
            {
                using (sqlCommand.Connection)
                {
                    sqlCommand.Connection.Open();

                    using (sqlCommand)
                    {
                        dataAdapter.Fill(dataSet, "dataSet");
                    }

                    sqlCommand.Connection.Close();

                    CMTreeMapRoot treeMapRoot = new CMTreeMapRoot();

                    treeMapRoot.text = dataSet.Tables[0].Rows[0]["UserName"].ToString();
                    treeMapRoot.supervisor = dataSet.Tables[0].Rows[0]["Supervisor"].ToString();
                    treeMapRoot.value = Convert.ToInt32(dataSet.Tables[0].Rows[0]["SUM_TOTAL_COUNT"].ToString());
                    treeMapRoot.failureRate = Convert.ToInt32(dataSet.Tables[0].Rows[0]["Overall_Failure_Rate"].ToString());

                    treeMapRoot.children = new List<CMTreeMapNode>();

                    for (int indexRow = 0; indexRow <= dataSet.Tables[0].Rows.Count - 1; indexRow++)
                    {
                        CMTreeMapNode node = new CMTreeMapNode();
                        node.children = new List<CMTreeMapAttribute>();

                        node.text = dataSet.Tables[0].Rows[indexRow]["Direct_Report"].ToString();
                        node.supervisor = dataSet.Tables[0].Rows[indexRow]["Supervisor"].ToString();
                        node.value = Convert.ToInt32(dataSet.Tables[0].Rows[indexRow]["Total_Count"].ToString());
                        node.failureRate = Convert.ToInt32(dataSet.Tables[0].Rows[indexRow]["Incident_Pct"].ToString());

                        if (dataSet.Tables[0].Rows[indexRow]["Direct_Report"].ToString().Length == 0)
                        {
                            node.expanded = true;
                        }

                        CMTreeMapAttribute attribute = new CMTreeMapAttribute();
                        Int32 totalCount = Convert.ToInt32(dataSet.Tables[0].Rows[indexRow]["Total_Count"].ToString());
                        Int32 incidentCount = Convert.ToInt32(dataSet.Tables[0].Rows[indexRow]["Incident_Count"].ToString());
                        Int32 successCount = totalCount - incidentCount;

                        attribute = new CMTreeMapAttribute();
                        attribute.userName = dataSet.Tables[0].Rows[indexRow]["Direct_Report"].ToString();
                        attribute.text = "Success Count - " + successCount;
                        attribute.value = successCount;

                        node.children.Add(attribute);

                        attribute = new CMTreeMapAttribute();
                        attribute.userName = dataSet.Tables[0].Rows[indexRow]["Direct_Report"].ToString();
                        attribute.text = "Failed Count - " + incidentCount;
                        attribute.value = incidentCount;

                        node.children.Add(attribute);

                        treeMapRoot.children.Add(node);
                    }

                    rootData.Add(treeMapRoot);
                }
            }
            catch (System.Exception error)
            {
                ErrorMessage errorMessage = new ErrorMessage();
                errorMessage.status = error.Source;
                errorMessage.message = error.Message;
            }

            return rootData;
        }


        public static ReturnData<List<CMDetailByHeirarchy>> GetCMDetailByHierarchy(String userName, String month, String year, String reportLevel)
        {
            ReturnData<List<CMDetailByHeirarchy>> returnData = new ReturnData<List<CMDetailByHeirarchy>>();
            returnData.data = new List<CMDetailByHeirarchy>();

            SqlCommand sqlCommand = new SqlCommand("dbo.Get_CM_Detail_By_Hierarchy");
            sqlCommand.CommandType = CommandType.StoredProcedure;
            sqlCommand.CommandTimeout = 600;

            sqlCommand.Parameters.Add("@name", SqlDbType.VarChar).Value = userName;
            sqlCommand.Parameters.Add("@maint_month", SqlDbType.VarChar).Value = month;
            sqlCommand.Parameters.Add("@main_year", SqlDbType.VarChar).Value = year;
            sqlCommand.Parameters.Add("@rptLevel", SqlDbType.VarChar).Value = reportLevel;

            sqlCommand.Connection = new SqlConnection("Persist Security Info=False; User ID=OBIRead; Password=OB1R3@d; Initial Catalog=OBI; Server=obidb-wc-p01");

            SqlDataAdapter dataAdapter = new SqlDataAdapter(sqlCommand);
            DataSet dataSet = new DataSet();

            try
            {
                using (sqlCommand.Connection)
                {
                    sqlCommand.Connection.Open();

                    using (sqlCommand)
                    {
                        dataAdapter.Fill(dataSet, "dataSet");
                    }

                    sqlCommand.Connection.Close();

                    for (int indexRow = 0; indexRow <= dataSet.Tables[0].Rows.Count - 1; indexRow++)
                    {
                        CMDetailByHeirarchy cMDetailByHeirarchy = new CMDetailByHeirarchy();

                        cMDetailByHeirarchy.maintenanceMonth = dataSet.Tables[0].Rows[indexRow]["MAINT_Month"].ToString();
                        cMDetailByHeirarchy.maintenanceYear = dataSet.Tables[0].Rows[indexRow]["MAINT_Year"].ToString();
                        cMDetailByHeirarchy.heiarchyUser = dataSet.Tables[0].Rows[indexRow]["Hierarchy_User"].ToString();
                        cMDetailByHeirarchy.directReport = dataSet.Tables[0].Rows[indexRow]["Direct_Report"].ToString();
                        cMDetailByHeirarchy.maintenanceImplementor = dataSet.Tables[0].Rows[indexRow]["Maint_Implementor"].ToString();
                        cMDetailByHeirarchy.sm = dataSet.Tables[0].Rows[indexRow]["SM"].ToString();
                        cMDetailByHeirarchy.smStatus = dataSet.Tables[0].Rows[indexRow]["SM Status"].ToString();
                        cMDetailByHeirarchy.smMaintenanceType = dataSet.Tables[0].Rows[indexRow]["SM Maint Type"].ToString();
                        cMDetailByHeirarchy.smAuthor = dataSet.Tables[0].Rows[indexRow]["SM Author"].ToString();
                        cMDetailByHeirarchy.smParent = dataSet.Tables[0].Rows[indexRow]["SM Parent"].ToString();
                        cMDetailByHeirarchy.closedBy = dataSet.Tables[0].Rows[indexRow]["CLOSED_BY"].ToString();
                        cMDetailByHeirarchy.ticketImplementer = dataSet.Tables[0].Rows[indexRow]["TKT_IMPLEMENTER"].ToString();
                        cMDetailByHeirarchy.ticketId = dataSet.Tables[0].Rows[indexRow]["TKT_ID"].ToString();
                        cMDetailByHeirarchy.ticketStart = dataSet.Tables[0].Rows[indexRow]["TKT_START"].ToString();
                        cMDetailByHeirarchy.ticketEnd = dataSet.Tables[0].Rows[indexRow]["TKT_END"].ToString();
                        cMDetailByHeirarchy.smCreateDate = dataSet.Tables[0].Rows[indexRow]["SM_CREATE_DATE"].ToString();
                        cMDetailByHeirarchy.smTimeframeStart = dataSet.Tables[0].Rows[indexRow]["SM_TIMEFRAME_START"].ToString();
                        cMDetailByHeirarchy.smTimeframeEnd = dataSet.Tables[0].Rows[indexRow]["SM_TIMEFRAME_END"].ToString();
                        cMDetailByHeirarchy.smClosedDate = dataSet.Tables[0].Rows[indexRow]["SM_CLOSED_DATE"].ToString();
                        cMDetailByHeirarchy.smMaintenanceCategory = dataSet.Tables[0].Rows[indexRow]["SM Maint Category"].ToString();
                        cMDetailByHeirarchy.smMaintenanceDetail = dataSet.Tables[0].Rows[indexRow]["SM Maint Detail"].ToString();
                        cMDetailByHeirarchy.smRegion = dataSet.Tables[0].Rows[indexRow]["SM Region"].ToString();
                        cMDetailByHeirarchy.smResolutionDescription = dataSet.Tables[0].Rows[indexRow]["SM Resolution Description"].ToString();
                        cMDetailByHeirarchy.resolutionDescription = dataSet.Tables[0].Rows[indexRow]["Resolution_Description"].ToString();
                        cMDetailByHeirarchy.resultCategory = dataSet.Tables[0].Rows[indexRow]["RESULT_CATEGORY"].ToString();
                        cMDetailByHeirarchy.smResolutionDetail = dataSet.Tables[0].Rows[indexRow]["SM Resolution Detail"].ToString();
                        cMDetailByHeirarchy.cm = dataSet.Tables[0].Rows[indexRow]["CM"].ToString();
                        cMDetailByHeirarchy.cmMaintenanceType = dataSet.Tables[0].Rows[indexRow]["CM Maint Type"].ToString();
                        cMDetailByHeirarchy.cmCategory = dataSet.Tables[0].Rows[indexRow]["CM Category"].ToString();
                        cMDetailByHeirarchy.cmSubcategory = dataSet.Tables[0].Rows[indexRow]["CM Subcategory"].ToString();
                        cMDetailByHeirarchy.cmProcedureName = dataSet.Tables[0].Rows[indexRow]["CM Procedure Name"].ToString();
                        cMDetailByHeirarchy.procedureId = dataSet.Tables[0].Rows[indexRow]["PROCEDURE_ID"].ToString();
                        cMDetailByHeirarchy.cmSummary = dataSet.Tables[0].Rows[indexRow]["CM Summary"].ToString();
                        cMDetailByHeirarchy.cmDetail = dataSet.Tables[0].Rows[indexRow]["CM Detail"].ToString();
                        cMDetailByHeirarchy.cmRisk = dataSet.Tables[0].Rows[indexRow]["CM Risk"].ToString();
                        cMDetailByHeirarchy.cmImpact = dataSet.Tables[0].Rows[indexRow]["CM Impact"].ToString();
                        cMDetailByHeirarchy.cmCreateDate = dataSet.Tables[0].Rows[indexRow]["CM_CREATE_DATE"].ToString();
                        cMDetailByHeirarchy.cmPlannedStart = dataSet.Tables[0].Rows[indexRow]["CM_PLANNED_START"].ToString();
                        cMDetailByHeirarchy.cmPlannedEnd = dataSet.Tables[0].Rows[indexRow]["CM_PLANNED_END"].ToString();
                        cMDetailByHeirarchy.cmStatus = dataSet.Tables[0].Rows[indexRow]["CM Status"].ToString();
                        cMDetailByHeirarchy.cmSubmitter = dataSet.Tables[0].Rows[indexRow]["CM Submitter"].ToString();
                        cMDetailByHeirarchy.cmSubmitterName = dataSet.Tables[0].Rows[indexRow]["CM Submitter Name"].ToString();
                        cMDetailByHeirarchy.cmRegion = dataSet.Tables[0].Rows[indexRow]["CM Region"].ToString();
                        cMDetailByHeirarchy.orgUnitDescription = dataSet.Tables[0].Rows[indexRow]["ORG_UNIT_DESCR"].ToString();
                        cMDetailByHeirarchy.orgHeirarchyLevel1Description = dataSet.Tables[0].Rows[indexRow]["ORG_HIERARCHY_LEVEL1_DESCR"].ToString();
                        cMDetailByHeirarchy.orgHeirarchyLevel2Description = dataSet.Tables[0].Rows[indexRow]["ORG_HIERARCHY_LEVEL2_DESCR"].ToString();
                        cMDetailByHeirarchy.sapLead = dataSet.Tables[0].Rows[indexRow]["SAP_Lead"].ToString();
                        cMDetailByHeirarchy.sapDir = dataSet.Tables[0].Rows[indexRow]["SAP_DIR"].ToString();
                        cMDetailByHeirarchy.sapVp = dataSet.Tables[0].Rows[indexRow]["SAP_VP"].ToString();
                        cMDetailByHeirarchy.sapVpTop = dataSet.Tables[0].Rows[indexRow]["SAP_VP_Top"].ToString();
                        cMDetailByHeirarchy.siTicket = dataSet.Tables[0].Rows[indexRow]["SI_TICKET"].ToString();
                        cMDetailByHeirarchy.relatedTicket = dataSet.Tables[0].Rows[indexRow]["RELATED_TICKET"].ToString();
                        cMDetailByHeirarchy.siCreateDate = dataSet.Tables[0].Rows[indexRow]["SI_CREATE_DATE"].ToString();
                        cMDetailByHeirarchy.siActualStart = dataSet.Tables[0].Rows[indexRow]["SI_ACTUAL_START"].ToString();
                        cMDetailByHeirarchy.siActualEnd = dataSet.Tables[0].Rows[indexRow]["SI_ACTUAL_END"].ToString();
                        cMDetailByHeirarchy.ticketImplementerNew = dataSet.Tables[0].Rows[indexRow]["Ticket_Implementer_New"].ToString();
                        cMDetailByHeirarchy.cancelledBeforeMaintenance = dataSet.Tables[0].Rows[indexRow]["Cancelled_Before_Maint"].ToString();
                        cMDetailByHeirarchy.cancelledBeforeMaintenanceSm = dataSet.Tables[0].Rows[indexRow]["Cancelled_Before_Maint_SM"].ToString();
                        cMDetailByHeirarchy.sucessCategory = dataSet.Tables[0].Rows[indexRow]["SUCCESS_CATEGORY"].ToString();
                        cMDetailByHeirarchy.supportAreaName = dataSet.Tables[0].Rows[indexRow]["SUPPORT_AREA_NAME"].ToString();
                        cMDetailByHeirarchy.queueName = dataSet.Tables[0].Rows[indexRow]["QUEUE_NAME"].ToString();
                        cMDetailByHeirarchy.siSeverity = dataSet.Tables[0].Rows[indexRow]["SI_SEVERITY"].ToString();
                        cMDetailByHeirarchy.leadOrgUnitDescription = dataSet.Tables[0].Rows[indexRow]["Lead_ORG_UNIT_DESCR"].ToString();
                        cMDetailByHeirarchy.dirOrgUnitDescription = dataSet.Tables[0].Rows[indexRow]["DIR_ORG_UNIT_DESCR"].ToString();
                        cMDetailByHeirarchy.vpOrgUnitDescription = dataSet.Tables[0].Rows[indexRow]["VP_ORG_UNIT_DESCR"].ToString();
                        cMDetailByHeirarchy.leadOrgHierarchyLevel1Description = dataSet.Tables[0].Rows[indexRow]["Lead_ORG_HIERARCHY_LEVEL1_DESCR"].ToString();
                        cMDetailByHeirarchy.dirOrgHierarchyLevel1Description = dataSet.Tables[0].Rows[indexRow]["DIR_ORG_HIERARCHY_LEVEL1_DESCR"].ToString();
                        cMDetailByHeirarchy.vpOrgHierarchyLevel1Description = dataSet.Tables[0].Rows[indexRow]["VP_ORG_HIERARCHY_LEVEL1_DESCR"].ToString();
                        cMDetailByHeirarchy.vpOrgHierarchyLevel2Description = dataSet.Tables[0].Rows[indexRow]["VP_ORG_HIERARCHY_LEVEL2_DESCR"].ToString();
                        cMDetailByHeirarchy.ts = dataSet.Tables[0].Rows[indexRow]["TS"].ToString();

                        returnData.data.Add(cMDetailByHeirarchy);
                    }

                    returnData.success = true;
                }
            }
            catch (System.Exception error)
            {
                ErrorMessage errorMessage = new ErrorMessage();
                errorMessage.status = error.Source;
                errorMessage.message = error.Message;

                returnData.errors.Add(errorMessage);
                returnData.success = false;
            }

            return returnData;
        }

        public static ReturnData<List<SAPUser>> GetSAPUser(String name, String level, String unit)
        {
            ReturnData<List<SAPUser>> returnData = new ReturnData<List<SAPUser>>();
            returnData.data = new List<SAPUser>();

            SqlCommand sqlCommand = new SqlCommand("dbo.sapLookupList");
            sqlCommand.CommandType = CommandType.StoredProcedure;
            sqlCommand.CommandTimeout = 600;

            sqlCommand.Parameters.Add("@Lookup", SqlDbType.VarChar).Value = name;
            sqlCommand.Parameters.Add("@Level", SqlDbType.VarChar).Value = level;
            sqlCommand.Parameters.Add("@Unit", SqlDbType.VarChar).Value = unit;

            sqlCommand.Connection = new SqlConnection("Persist Security Info=False; User ID=OBIRead; Password=OB1R3@d; Initial Catalog=Central; Server=obidb-wc-p01");

            SqlDataAdapter dataAdapter = new SqlDataAdapter(sqlCommand);
            DataSet dataSet = new DataSet();

            try
            {
                using (sqlCommand.Connection)
                {
                    sqlCommand.Connection.Open();

                    using (sqlCommand)
                    {
                        dataAdapter.Fill(dataSet, "dataSet");
                    }

                    sqlCommand.Connection.Close();

                    for (int indexRow = 0; indexRow <= dataSet.Tables[0].Rows.Count - 1; indexRow++)
                    {
                        SAPUser SAPUser = new SAPUser();

                        SAPUser.displayName = dataSet.Tables[0].Rows[indexRow]["Display Name"].ToString();
                        SAPUser.name = dataSet.Tables[0].Rows[indexRow]["Name"].ToString();
                        SAPUser.hierarchyLevel1 = dataSet.Tables[0].Rows[indexRow]["Hierarchy Level 1"].ToString();
                        SAPUser.department = dataSet.Tables[0].Rows[indexRow]["Department"].ToString();
                        SAPUser.orgUnit = dataSet.Tables[0].Rows[indexRow]["Org Unit"].ToString();
                        SAPUser.jobDescription = dataSet.Tables[0].Rows[indexRow]["Job Descrption"].ToString();
                        SAPUser.emailAddress = dataSet.Tables[0].Rows[indexRow]["Email Address"].ToString();
                        SAPUser.workPhone = dataSet.Tables[0].Rows[indexRow]["Work Phone"].ToString();
                        SAPUser.loginId = dataSet.Tables[0].Rows[indexRow]["Login ID"].ToString();
                        SAPUser.supervisor = dataSet.Tables[0].Rows[indexRow]["Supervisor"].ToString();

                        returnData.data.Add(SAPUser);
                    }

                    returnData.success = true;
                }
            }
            catch (System.Exception error)
            {
                ErrorMessage errorMessage = new ErrorMessage();
                errorMessage.status = error.Source;
                errorMessage.message = error.Message;

                returnData.errors.Add(errorMessage);
                returnData.success = false;
            }

            return returnData;
        }

        public static ReturnData<List<SAPSubordinate>> GetSAPSubordinates(String name)
        {
            ReturnData<List<SAPSubordinate>> returnData = new ReturnData<List<SAPSubordinate>>();
            returnData.data = new List<SAPSubordinate>();

            SqlCommand sqlCommand = new SqlCommand("dbo.User_Subordinates");
            sqlCommand.CommandType = CommandType.StoredProcedure;
            sqlCommand.CommandTimeout = 600;

            sqlCommand.Parameters.Add("@SLT", SqlDbType.VarChar).Value = name;

            sqlCommand.Connection = new SqlConnection("Persist Security Info=False; User ID=OBIRead; Password=OB1R3@d; Initial Catalog=Central; Server=obidb-wc-p01");

            SqlDataAdapter dataAdapter = new SqlDataAdapter(sqlCommand);
            DataSet dataSet = new DataSet();

            try
            {
                using (sqlCommand.Connection)
                {
                    sqlCommand.Connection.Open();

                    using (sqlCommand)
                    {
                        dataAdapter.Fill(dataSet, "dataSet");
                    }

                    sqlCommand.Connection.Close();

                    for (int indexRow = 0; indexRow <= dataSet.Tables[0].Rows.Count - 1; indexRow++)
                    {
                        SAPSubordinate SAPSubordinate = new SAPSubordinate();

                        SAPSubordinate.employeeId = dataSet.Tables[0].Rows[indexRow]["EMPLID"].ToString();
                        SAPSubordinate.name = dataSet.Tables[0].Rows[indexRow]["Name"].ToString();
                        SAPSubordinate.loginId = dataSet.Tables[0].Rows[indexRow]["AD_LOGIN_ID"].ToString();
                        SAPSubordinate.jobLevelCode = dataSet.Tables[0].Rows[indexRow]["JOB_LEVEL_CODE"].ToString();
                        SAPSubordinate.employeeStatusDescription = dataSet.Tables[0].Rows[indexRow]["EMPL_STATUS_DESCR"].ToString();
                        SAPSubordinate.supervisorName = dataSet.Tables[0].Rows[indexRow]["SUPERVISOR_NAME"].ToString();
                        SAPSubordinate.employeeStatus = dataSet.Tables[0].Rows[indexRow]["EMPL_STATUS"].ToString();
                        SAPSubordinate.workEmail = dataSet.Tables[0].Rows[indexRow]["AD_WORK_EMAIL"].ToString();
                        SAPSubordinate.path = dataSet.Tables[0].Rows[indexRow]["PATH"].ToString();
                        SAPSubordinate.direct = dataSet.Tables[0].Rows[indexRow]["Direct"].ToString();

                        returnData.data.Add(SAPSubordinate);
                    }

                    returnData.success = true;
                }
            }
            catch (System.Exception error)
            {
                ErrorMessage errorMessage = new ErrorMessage();
                errorMessage.status = error.Source;
                errorMessage.message = error.Message;

                returnData.errors.Add(errorMessage);
                returnData.success = false;
            }

            return returnData;
        }

        public static ReturnData<List<ActiveTicket>> GetActiveTickets(String severity)
        {
            ReturnData<List<ActiveTicket>> returnData = new ReturnData<List<ActiveTicket>>();
            returnData.data = new List<ActiveTicket>();

            SqlCommand sqlCommand = new SqlCommand("dbo.Get_Active_SA_SI_Tickets");
            sqlCommand.CommandType = CommandType.StoredProcedure;
            sqlCommand.CommandTimeout = 600;

            sqlCommand.Parameters.Add("@sev", SqlDbType.VarChar).Value = severity;

            sqlCommand.Connection = new SqlConnection("Persist Security Info=False; User ID=OBIRead; Password=OB1R3@d; Initial Catalog=OBI; Server=obidb-wc-p01");

            SqlDataAdapter dataAdapter = new SqlDataAdapter(sqlCommand);
            DataSet dataSet = new DataSet();

            try
            {
                using (sqlCommand.Connection)
                {
                    sqlCommand.Connection.Open();

                    using (sqlCommand)
                    {
                        dataAdapter.Fill(dataSet, "dataSet");
                    }

                    sqlCommand.Connection.Close();

                    for (int indexRow = 0; indexRow <= dataSet.Tables[0].Rows.Count - 1; indexRow++)
                    {
                        ActiveTicket ActiveTicket = new ActiveTicket();

                        ActiveTicket.ticketId = dataSet.Tables[0].Rows[indexRow]["TICKET_ID"].ToString();
                        ActiveTicket.incidentSummary = dataSet.Tables[0].Rows[indexRow]["INCIDENT_SUMMARY"].ToString();
                        ActiveTicket.nextAction = dataSet.Tables[0].Rows[indexRow]["NEXT_ACTION"].ToString();
                        ActiveTicket.serviceCondition = dataSet.Tables[0].Rows[indexRow]["SERVICE_CONDITION"].ToString();
                        ActiveTicket.severity = dataSet.Tables[0].Rows[indexRow]["SEVERITY"].ToString();
                        ActiveTicket.createDateMt = DateTime.Parse(dataSet.Tables[0].Rows[indexRow]["CREATE_DATE_MT"].ToString()).ToString();
                        ActiveTicket.supportAreaName = dataSet.Tables[0].Rows[indexRow]["SUPPORT_AREA_NAME"].ToString();
                        ActiveTicket.regionName = dataSet.Tables[0].Rows[indexRow]["REGION_NAME"].ToString();
                        ActiveTicket.queueName = dataSet.Tables[0].Rows[indexRow]["QUEUE_NAME"].ToString();

                        returnData.data.Add(ActiveTicket);
                    }

                    returnData.success = true;
                }
            }
            catch (System.Exception error)
            {
                ErrorMessage errorMessage = new ErrorMessage();
                errorMessage.status = error.Source;
                errorMessage.message = error.Message;

                returnData.errors.Add(errorMessage);
                returnData.success = false;
            }

            return returnData;
        }

        public static ReturnData<List<SIEventFunnel>> GetSIEventTunnel(String ticketId)
        {

            ReturnData<List<SIEventFunnel>> returnData = new ReturnData<List<SIEventFunnel>>();
            returnData.data = new List<SIEventFunnel>();

            SqlCommand sqlCommand = new SqlCommand("dbo.Get_CallFunnel_by_TicketID");
            sqlCommand.CommandType = CommandType.StoredProcedure;
            sqlCommand.CommandTimeout = 600;

            sqlCommand.Parameters.Add("@TicketID", SqlDbType.VarChar).Value = ticketId;

            sqlCommand.Connection = new SqlConnection("Persist Security Info=False; User ID=OBIRead; Password=OB1R3@d; Initial Catalog=OBI; Server=obidb-wc-p01");

            SqlDataAdapter dataAdapter = new SqlDataAdapter(sqlCommand);
            DataSet dataSet = new DataSet();

            try
            {
                using (sqlCommand.Connection)
                {
                    sqlCommand.Connection.Open();

                    using (sqlCommand)
                    {
                        dataAdapter.Fill(dataSet, "dataSet");
                    }

                    sqlCommand.Connection.Close();

                    for (int indexRow = 0; indexRow <= dataSet.Tables[0].Rows.Count - 1; indexRow++)
                    {
                        SIEventFunnel sIEventTunnel = new SIEventFunnel();

                        sIEventTunnel.ticketId = dataSet.Tables[0].Rows[indexRow]["TICKET_ID"].ToString();
                        sIEventTunnel.timeFrame = dataSet.Tables[0].Rows[indexRow]["Time_Frame"].ToString();
                        sIEventTunnel.agentOffered = dataSet.Tables[0].Rows[indexRow]["Agent_Offered"].ToString();
                        sIEventTunnel.deflected = dataSet.Tables[0].Rows[indexRow]["Deflected"].ToString();
                        sIEventTunnel.fiveWeekBaseline = dataSet.Tables[0].Rows[indexRow]["Five_Week_Baseline"].ToString();

                        returnData.data.Add(sIEventTunnel);
                    }

                    returnData.success = true;
                }
            }
            catch (System.Exception error)
            {
                ErrorMessage errorMessage = new ErrorMessage();
                errorMessage.status = error.Source;
                errorMessage.message = error.Message;

                returnData.errors.Add(errorMessage);
                returnData.success = false;
            }

            return returnData;
        }
    }  
}