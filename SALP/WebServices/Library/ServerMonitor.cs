﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;

using SALP.WebServices.DataTypes;


namespace SALP.WebServices.Library
{
    public class ServerMonitor
    {
        public static ReturnData<List<ServerMonitorItem>> GetServerJobs(String serverName)
        {

            ReturnData<List<ServerMonitorItem>> returnData = new ReturnData<List<ServerMonitorItem>>();
            returnData.data = new List<ServerMonitorItem>();

            SqlCommand sqlCommand = new SqlCommand();
            sqlCommand.CommandType = CommandType.Text;
            sqlCommand.CommandText = "SELECT * FROM iJobs.dbo.AgentLongRunningJobs WITH(NOLOCK)";

            String connectionString = "Persist Security Info=False; User ID=OBIRead; Password=OB1R3@d; Initial Catalog=iJobs; Server=" + serverName;

            sqlCommand.Connection = new SqlConnection(connectionString);

            SqlDataAdapter dataAdapter = new SqlDataAdapter(sqlCommand);
            DataSet dataSet = new DataSet();

            try
            {
                using (sqlCommand.Connection)
                {
                    sqlCommand.Connection.Open();

                    using (sqlCommand)
                    {
                        dataAdapter.Fill(dataSet, "dataSet");
                    }

                    sqlCommand.Connection.Close();

                    for (int indexRow = 0; indexRow <= dataSet.Tables[0].Rows.Count - 1; indexRow++)
                    {
                        ServerMonitorItem serverMonitorItem = new ServerMonitorItem();

                        serverMonitorItem.name = dataSet.Tables[0].Rows[indexRow]["name"].ToString();
                        serverMonitorItem.startDate = dataSet.Tables[0].Rows[indexRow]["StartDate"].ToString();
                        serverMonitorItem.hours = dataSet.Tables[0].Rows[indexRow]["Hours"].ToString();

                        returnData.data.Add(serverMonitorItem);
                    }

                    returnData.success = true;
                }
            }
            catch (System.Exception error)
            {
                ErrorMessage errorMessage = new ErrorMessage();
                errorMessage.status = error.Source;
                errorMessage.message = error.Message;

                returnData.errors.Add(errorMessage);
                returnData.success = false;
            }

            return returnData;
        }


        public static ReturnData<List<CRTicketStatus>> GetCRTicketStatus()
        {

            ReturnData<List<CRTicketStatus>> returnData = new ReturnData<List<CRTicketStatus>>();
            returnData.data = new List<CRTicketStatus>();

            SqlCommand sqlCommand = new SqlCommand("OBI.dbo.ServMon_CRTkt_Chart");
            sqlCommand.CommandType = CommandType.StoredProcedure;

            sqlCommand.Connection = new SqlConnection("Persist Security Info=False; User ID=OBIRead; Password=OB1R3@d; Initial Catalog=OBI; Server=obidb-wc-p01");

            SqlDataAdapter dataAdapter = new SqlDataAdapter(sqlCommand);
            DataSet dataSet = new DataSet();

            try
            {
                using (sqlCommand.Connection)
                {
                    sqlCommand.Connection.Open();

                    using (sqlCommand)
                    {
                        dataAdapter.Fill(dataSet, "dataSet");
                    }

                    sqlCommand.Connection.Close();

                    for (int indexRow = 0; indexRow <= dataSet.Tables[0].Rows.Count - 1; indexRow++)
                    {
                        CRTicketStatus cRTicketStatus = new CRTicketStatus();

                        cRTicketStatus.date = dataSet.Tables[0].Rows[indexRow]["r_date"].ToString();
                        cRTicketStatus.currentWeek = dataSet.Tables[0].Rows[indexRow]["CurrentWeek"].ToString();
                        cRTicketStatus.previousWeek = dataSet.Tables[0].Rows[indexRow]["PreviousWeek"].ToString();

                        returnData.data.Add(cRTicketStatus);
                    }

                    returnData.success = true;
                }
            }
            catch (System.Exception error)
            {
                ErrorMessage errorMessage = new ErrorMessage();
                errorMessage.status = error.Source;
                errorMessage.message = error.Message;

                returnData.errors.Add(errorMessage);
                returnData.success = false;
            }

            return returnData;
        }


        public static ReturnData<List<URLMonitorItem>> GetURLMonitor()
        {

            ReturnData<List<URLMonitorItem>> returnData = new ReturnData<List<URLMonitorItem>>();
            returnData.data = new List<URLMonitorItem>();

            SqlCommand sqlCommand = new SqlCommand("OBI.dbo.ServMonChartData_U");
            sqlCommand.CommandType = CommandType.StoredProcedure;

            sqlCommand.Parameters.Add("@Period", SqlDbType.VarChar).Value = 60;
            sqlCommand.Parameters.Add("@Type", SqlDbType.VarChar).Value = "Min";

            sqlCommand.Connection = new SqlConnection("Persist Security Info=False; User ID=OBIRead; Password=OB1R3@d; Initial Catalog=OBI; Server=obidb-wc-p02");

            SqlDataAdapter dataAdapter = new SqlDataAdapter(sqlCommand);
            DataSet dataSet = new DataSet();

            try
            {
                using (sqlCommand.Connection)
                {
                    sqlCommand.Connection.Open();

                    using (sqlCommand)
                    {
                        dataAdapter.Fill(dataSet, "dataSet");
                    }

                    sqlCommand.Connection.Close();

                    for (int indexRow = 0; indexRow <= dataSet.Tables[0].Rows.Count - 1; indexRow++)
                    {
                        URLMonitorItem URLMonitorItem = new URLMonitorItem();

                        int bnocTools = int.Parse(dataSet.Tables[0].Rows[indexRow]["bnoctools_CCSOutageLog"].ToString()) * 100;
                        int etsTools = int.Parse(dataSet.Tables[0].Rows[indexRow]["etstools_OutageLog"].ToString()) * 100;
                        int rcor = int.Parse(dataSet.Tables[0].Rows[indexRow]["obiweb-po-01p_rcor"].ToString()) * 100;

                        URLMonitorItem.date = dataSet.Tables[0].Rows[indexRow]["ldate"].ToString();
                        URLMonitorItem.bnocTools = bnocTools.ToString();
                        URLMonitorItem.etsTools = etsTools.ToString();
                        URLMonitorItem.rcor = rcor.ToString();

                        returnData.data.Add(URLMonitorItem);
                    }

                    returnData.success = true;
                }
            }
            catch (System.Exception error)
            {
                ErrorMessage errorMessage = new ErrorMessage();
                errorMessage.status = error.Source;
                errorMessage.message = error.Message;

                returnData.errors.Add(errorMessage);
                returnData.success = false;
            }

            return returnData;
        }


        public static ReturnData<List<ErrorStatus>> GetSubscriptionErrorStatus()
        {

            ReturnData<List<ErrorStatus>> returnData = new ReturnData<List<ErrorStatus>>();
            returnData.data = new List<ErrorStatus>();

            SqlCommand sqlCommand = new SqlCommand("OBI.dbo.ServMon_SSRS_Sub");
            sqlCommand.CommandType = CommandType.StoredProcedure;


            sqlCommand.Connection = new SqlConnection("Persist Security Info=False; User ID=OBIRead; Password=OB1R3@d; Initial Catalog=OBI; Server=obidb-wc-p01");

            SqlDataAdapter dataAdapter = new SqlDataAdapter(sqlCommand);
            DataSet dataSet = new DataSet();

            try
            {
                using (sqlCommand.Connection)
                {
                    sqlCommand.Connection.Open();

                    using (sqlCommand)
                    {
                        dataAdapter.Fill(dataSet, "dataSet");
                    }

                    sqlCommand.Connection.Close();

                    for (int indexRow = 0; indexRow <= dataSet.Tables[0].Rows.Count - 1; indexRow++)
                    {
                        ErrorStatus errorStatus = new ErrorStatus();
                        Decimal successRate = Decimal.Parse(dataSet.Tables[0].Rows[indexRow]["Success_Rate"].ToString()) * 100;

                        errorStatus.date = dataSet.Tables[0].Rows[indexRow]["r_date"].ToString();
                        errorStatus.successRate = successRate.ToString("0.0");

                        returnData.data.Add(errorStatus);
                    }

                    returnData.success = true;
                }
            }
            catch (System.Exception error)
            {
                ErrorMessage errorMessage = new ErrorMessage();
                errorMessage.status = error.Source;
                errorMessage.message = error.Message;

                returnData.errors.Add(errorMessage);
                returnData.success = false;
            }

            return returnData;
        }

        public static ReturnData<List<TempDBLogItem>> GetTempDBLogStatus()
        {

            ReturnData<List<TempDBLogItem>> returnData = new ReturnData<List<TempDBLogItem>>();
            returnData.data = new List<TempDBLogItem>();

            SqlCommand sqlCommand = new SqlCommand("OBI.dbo.ServMonChartLogData");
            sqlCommand.CommandType = CommandType.StoredProcedure;

            sqlCommand.Parameters.Add("@Period", SqlDbType.VarChar).Value = 60;
            sqlCommand.Parameters.Add("@Type", SqlDbType.VarChar).Value = "Min";

            sqlCommand.Connection = new SqlConnection("Persist Security Info=False; User ID=OBIRead; Password=OB1R3@d; Initial Catalog=OBI; Server=obidb-wc-p02");

            SqlDataAdapter dataAdapter = new SqlDataAdapter(sqlCommand);
            DataSet dataSet = new DataSet();

            try
            {
                using (sqlCommand.Connection)
                {
                    sqlCommand.Connection.Open();

                    using (sqlCommand)
                    {
                        dataAdapter.Fill(dataSet, "dataSet");
                    }

                    sqlCommand.Connection.Close();

                    for (int indexRow = 0; indexRow <= dataSet.Tables[0].Rows.Count - 1; indexRow++)
                    {
                        TempDBLogItem tempDBLogItem = new TempDBLogItem();
                        Decimal tempDBLogPct = Decimal.Parse(dataSet.Tables[0].Rows[indexRow]["TempDBLogPct"].ToString()) * 100;
                        Decimal tempDBLogStatus = Decimal.Parse(dataSet.Tables[0].Rows[indexRow]["TempDBLogStatus"].ToString()) * 100;

                        tempDBLogItem.date = dataSet.Tables[0].Rows[indexRow]["ldate"].ToString();
                        tempDBLogItem.tempDBLogPct = tempDBLogPct.ToString("0.0");
                        tempDBLogItem.tempDBLogStatus = tempDBLogStatus.ToString("0.0");

                        returnData.data.Add(tempDBLogItem);
                    }

                    returnData.success = true;
                }
            }
            catch (System.Exception error)
            {
                ErrorMessage errorMessage = new ErrorMessage();
                errorMessage.status = error.Source;
                errorMessage.message = error.Message;

                returnData.errors.Add(errorMessage);
                returnData.success = false;
            }

            return returnData;
        }

        public static ReturnData<List<ApplicationEventMonitorItem>> GetApplicationEventMonitor()
        {

            ReturnData<List<ApplicationEventMonitorItem>> returnData = new ReturnData<List<ApplicationEventMonitorItem>>();
            returnData.data = new List<ApplicationEventMonitorItem>();

            SqlCommand sqlCommand = new SqlCommand("OBI.dbo.ServMonChartData_A");
            sqlCommand.CommandType = CommandType.StoredProcedure;

            sqlCommand.Parameters.Add("@Period", SqlDbType.VarChar).Value = 60;
            sqlCommand.Parameters.Add("@Type", SqlDbType.VarChar).Value = "Min";

            sqlCommand.Connection = new SqlConnection("Persist Security Info=False; User ID=OBIRead; Password=OB1R3@d; Initial Catalog=OBI; Server=obidb-wc-p02");

            SqlDataAdapter dataAdapter = new SqlDataAdapter(sqlCommand);
            DataSet dataSet = new DataSet();

            try
            {
                using (sqlCommand.Connection)
                {
                    sqlCommand.Connection.Open();

                    using (sqlCommand)
                    {
                        dataAdapter.Fill(dataSet, "dataSet");
                    }

                    sqlCommand.Connection.Close();

                    for (int indexRow = 0; indexRow <= dataSet.Tables[0].Rows.Count - 1; indexRow++)
                    {
                        ApplicationEventMonitorItem applicationEventMonitorItem = new ApplicationEventMonitorItem();
                        Decimal oBIDR_po_P01 = Decimal.Parse(dataSet.Tables[0].Rows[indexRow]["OBIDR-po-P01"].ToString()) * 100;
                        Decimal oBIPROC_WC_P01 = Decimal.Parse(dataSet.Tables[0].Rows[indexRow]["OBIPROC-WC-P01"].ToString()) * 100;
                        Decimal oBIPROC_WC_P01_APP = Decimal.Parse(dataSet.Tables[0].Rows[indexRow]["OBIPROC-WC-P01_App"].ToString()) * 100;
                        Decimal oBIWEB_WC_P01 = Decimal.Parse(dataSet.Tables[0].Rows[indexRow]["OBIWEB-WC-P01"].ToString()) * 100;

                        applicationEventMonitorItem.date = dataSet.Tables[0].Rows[indexRow]["ldate"].ToString();
                        applicationEventMonitorItem.oBIDR_po_P01 = oBIDR_po_P01.ToString("0.0");
                        applicationEventMonitorItem.oBIPROC_WC_P01 = oBIPROC_WC_P01.ToString("0.0");
                        applicationEventMonitorItem.oBIPROC_WC_P01_APP = oBIPROC_WC_P01_APP.ToString("0.0");
                        applicationEventMonitorItem.oBIWEB_WC_P01 = oBIWEB_WC_P01.ToString("0.0");

                        returnData.data.Add(applicationEventMonitorItem);
                    }

                    returnData.success = true;
                }
            }
            catch (System.Exception error)
            {
                ErrorMessage errorMessage = new ErrorMessage();
                errorMessage.status = error.Source;
                errorMessage.message = error.Message;

                returnData.errors.Add(errorMessage);
                returnData.success = false;
            }

            return returnData;
        }


        public static ReturnData<List<CRTicketsItem>> GetCRTickets()
        {

            ReturnData<List<CRTicketsItem>> returnData = new ReturnData<List<CRTicketsItem>>();
            returnData.data = new List<CRTicketsItem>();

            SqlCommand sqlCommand = new SqlCommand("dbo.CR_TICKET_2HR");
            sqlCommand.CommandType = CommandType.StoredProcedure;
            sqlCommand.CommandTimeout = 600;


            sqlCommand.Connection = new SqlConnection("Persist Security Info=False; User Id=MonMan; Password=M0nM@n; Initial Catalog=EPSSA_Monitoring; Server=OBIVDB-WC-04P");

            SqlDataAdapter dataAdapter = new SqlDataAdapter(sqlCommand);
            DataSet dataSet = new DataSet();

            try
            {
                using (sqlCommand.Connection)
                {
                    sqlCommand.Connection.Open();

                    using (sqlCommand)
                    {
                        dataAdapter.Fill(dataSet, "dataSet");
                    }

                    sqlCommand.Connection.Close();

                    for (int indexRow = 0; indexRow <= dataSet.Tables[0].Rows.Count - 1; indexRow++)
                    {
                        CRTicketsItem cRTicketsItem = new CRTicketsItem();

                        cRTicketsItem.date = dataSet.Tables[0].Rows[indexRow]["Time_Seg"].ToString();
                        cRTicketsItem.cRCount = dataSet.Tables[0].Rows[indexRow]["CR_Count"].ToString();
                        cRTicketsItem.baseline = dataSet.Tables[0].Rows[indexRow]["Baseline"].ToString();

                        returnData.data.Add(cRTicketsItem);
                    }

                    returnData.success = true;
                }
            }
            catch (System.Exception error)
            {
                ErrorMessage errorMessage = new ErrorMessage();
                errorMessage.status = error.Source;
                errorMessage.message = error.Message;

                returnData.errors.Add(errorMessage);
                returnData.success = false;
            }

            return returnData;
        }


        public static ReturnData<List<IVRCDRCall>> GetIVRCDRCalls()
        {

            ReturnData<List<IVRCDRCall>> returnData = new ReturnData<List<IVRCDRCall>>();
            returnData.data = new List<IVRCDRCall>();

            SqlCommand sqlCommand = new SqlCommand("dbo.IVR_CDR_Calls_2HR");
            sqlCommand.CommandType = CommandType.StoredProcedure;
            sqlCommand.CommandTimeout = 600;


            sqlCommand.Connection = new SqlConnection("Persist Security Info=False; User Id=MonMan; Password=M0nM@n; Initial Catalog=EPSSA_Monitoring; Server=OBIVDB-WC-04P");

            SqlDataAdapter dataAdapter = new SqlDataAdapter(sqlCommand);
            DataSet dataSet = new DataSet();

            try
            {
                using (sqlCommand.Connection)
                {
                    sqlCommand.Connection.Open();

                    using (sqlCommand)
                    {
                        dataAdapter.Fill(dataSet, "dataSet");
                    }

                    sqlCommand.Connection.Close();

                    for (int indexRow = 0; indexRow <= dataSet.Tables[0].Rows.Count - 1; indexRow++)
                    {
                        IVRCDRCall iVRCDRCall = new IVRCDRCall();

                        iVRCDRCall.date = dataSet.Tables[0].Rows[indexRow]["Time_Seg"].ToString();
                        iVRCDRCall.callCount = dataSet.Tables[0].Rows[indexRow]["Call_Count"].ToString();
                        iVRCDRCall.baseline = dataSet.Tables[0].Rows[indexRow]["Baseline"].ToString();

                        returnData.data.Add(iVRCDRCall);
                    }

                    returnData.success = true;
                }
            }
            catch (System.Exception error)
            {
                ErrorMessage errorMessage = new ErrorMessage();
                errorMessage.status = error.Source;
                errorMessage.message = error.Message;

                returnData.errors.Add(errorMessage);
                returnData.success = false;
            }

            return returnData;
        }


        public static ReturnData<List<CRTicketDisplayItem>> GetCRTicketDelayDisplay()
        {

            ReturnData<List<CRTicketDisplayItem>> returnData = new ReturnData<List<CRTicketDisplayItem>>();
            returnData.data = new List<CRTicketDisplayItem>();

            SqlCommand sqlCommand = new SqlCommand();
            sqlCommand.CommandType = CommandType.Text;


            sqlCommand.CommandText = "SELECT Central.dbo.GET_TZTIME(Central.dbo.GET_DATEfEPOCH(DT), 'MT') AS Remedy,";
            sqlCommand.CommandText += "Central.dbo.GET_TZTIME(Central.dbo.GET_GMTTIME(GETDATE(), 'ET'), 'MT') AS TimeOfRefresh,";
            sqlCommand.CommandText += "Central.dbo.GET_TZTIME(Central.dbo.GET_DATEfEPOCH((SELECT MAX(CREATE_DATE) FROM Tickets.dbo.CR_TICKET_60_tmp with(nolock))),'MT') AS XNOCOBI,";
            sqlCommand.CommandText += "CASE WHEN(SELECT MAX(CREATE_DATE) ";
            sqlCommand.CommandText += "FROM Tickets.dbo.CR_TICKET_60_tmp with(nolock)) < DT THEN 'RED' ELSE 'BLACK' END AS dtLag ";
            sqlCommand.CommandText += "FROM OPENQUERY(rxr_rtq, 'SELECT MAX(CREATE_DATE) AS DT FROM ARADMIN.CR_TICKET')";


            sqlCommand.CommandTimeout = 600;


            String connectionString = "Persist Security Info=False; User ID=OBIRead; Password=OB1R3@d; Initial Catalog=iJobs; Server=OBIDB-WC-P01";

            sqlCommand.Connection = new SqlConnection(connectionString);

            SqlDataAdapter dataAdapter = new SqlDataAdapter(sqlCommand);
            DataSet dataSet = new DataSet();

            try
            {
                using (sqlCommand.Connection)
                {
                    sqlCommand.Connection.Open();

                    using (sqlCommand)
                    {
                        dataAdapter.Fill(dataSet, "dataSet");
                    }

                    sqlCommand.Connection.Close();

                    CRTicketDisplayItem cRTicketDisplayItem01 = new CRTicketDisplayItem();
                    cRTicketDisplayItem01.name = "TTSPRD_RTQ";
                    cRTicketDisplayItem01.value = dataSet.Tables[0].Rows[0]["Remedy"].ToString();

                    returnData.data.Add(cRTicketDisplayItem01);

                    CRTicketDisplayItem cRTicketDisplayItem02 = new CRTicketDisplayItem();
                    cRTicketDisplayItem02.name = "XNOCOBI";
                    cRTicketDisplayItem02.value = dataSet.Tables[0].Rows[0]["XNOCOBI"].ToString();

                    returnData.data.Add(cRTicketDisplayItem02);

                    CRTicketDisplayItem cRTicketDisplayItem03 = new CRTicketDisplayItem();
                    cRTicketDisplayItem03.name = "Current time";
                    cRTicketDisplayItem03.value = dataSet.Tables[0].Rows[0]["TimeOfRefresh"].ToString();

                    returnData.data.Add(cRTicketDisplayItem03);

                    CRTicketDisplayItem cRTicketDisplayItem04 = new CRTicketDisplayItem();
                    cRTicketDisplayItem04.name = "dtLag";
                    cRTicketDisplayItem04.value = dataSet.Tables[0].Rows[0]["dtLag"].ToString();

                    returnData.data.Add(cRTicketDisplayItem04);

                    returnData.success = true;
                }
            }
            catch (System.Exception error)
            {
                ErrorMessage errorMessage = new ErrorMessage();
                errorMessage.status = error.Source;
                errorMessage.message = error.Message;

                returnData.errors.Add(errorMessage);
                returnData.success = false;
            }

            return returnData;
        }


        public static ReturnData<List<ServerProcessesItem>> GetServerProcesses(String serverName)
        {

            ReturnData<List<ServerProcessesItem>> returnData = new ReturnData<List<ServerProcessesItem>>();
            returnData.data = new List<ServerProcessesItem>();

            SqlCommand sqlCommand = new SqlCommand();
            sqlCommand.CommandType = CommandType.Text;
            sqlCommand.CommandTimeout = 600;

            sqlCommand.CommandText = "SELECT * from WhatIsGoingOn";


            sqlCommand.Connection = new SqlConnection("Persist Security Info=False; User ID=ProcMon; Password=xnocOB1;  Initial Catalog=Central; Server=" + serverName);

            SqlDataAdapter dataAdapter = new SqlDataAdapter(sqlCommand);
            DataSet dataSet = new DataSet();

            try
            {
                using (sqlCommand.Connection)
                {
                    sqlCommand.Connection.Open();

                    using (sqlCommand)
                    {
                        dataAdapter.Fill(dataSet, "dataSet");
                    }

                    sqlCommand.Connection.Close();

                    for (int indexRow = 0; indexRow <= dataSet.Tables[0].Rows.Count - 1; indexRow++)
                    {
                        ServerProcessesItem serverProcessesItem = new ServerProcessesItem();

                        serverProcessesItem.statementText = dataSet.Tables[0].Rows[indexRow]["statement_text"].ToString();
                        serverProcessesItem.databaseName = dataSet.Tables[0].Rows[indexRow]["DatabaseName"].ToString();
                        serverProcessesItem.cpuTime = dataSet.Tables[0].Rows[indexRow]["CPU_Time"].ToString();
                        serverProcessesItem.runningMinutes = dataSet.Tables[0].Rows[indexRow]["RunningMinutes"].ToString();
                        serverProcessesItem.runningFrom = dataSet.Tables[0].Rows[indexRow]["RunningFrom"].ToString();
                        serverProcessesItem.runningBy = dataSet.Tables[0].Rows[indexRow]["RunningBy"].ToString();
                        serverProcessesItem.sessionId = dataSet.Tables[0].Rows[indexRow]["SessionID"].ToString();
                        serverProcessesItem.blockedBy = dataSet.Tables[0].Rows[indexRow]["BlockedBy"].ToString();
                        serverProcessesItem.reads = dataSet.Tables[0].Rows[indexRow]["reads"].ToString();
                        serverProcessesItem.writes = dataSet.Tables[0].Rows[indexRow]["writes"].ToString();
                        serverProcessesItem.programName = dataSet.Tables[0].Rows[indexRow]["program_name"].ToString();
                        serverProcessesItem.loginName = dataSet.Tables[0].Rows[indexRow]["login_name"].ToString();
                        serverProcessesItem.status = dataSet.Tables[0].Rows[indexRow]["status"].ToString();
                        serverProcessesItem.waitTime = dataSet.Tables[0].Rows[indexRow]["waittime"].ToString();
                        serverProcessesItem.lastWaitType = dataSet.Tables[0].Rows[indexRow]["lastwaittype"].ToString();
                        serverProcessesItem.cmd = dataSet.Tables[0].Rows[indexRow]["cmd"].ToString();
                        serverProcessesItem.lastRequestStartTime = dataSet.Tables[0].Rows[indexRow]["last_request_start_time"].ToString();
                        serverProcessesItem.logicalReads = dataSet.Tables[0].Rows[indexRow]["logical_reads"].ToString();

                        returnData.data.Add(serverProcessesItem);
                    }

                    returnData.success = true;
                }
            }
            catch (System.Exception error)
            {
                ErrorMessage errorMessage = new ErrorMessage();
                errorMessage.status = error.Source;
                errorMessage.message = error.Message;

                returnData.errors.Add(errorMessage);
                returnData.success = false;
            }

            return returnData;
        }

        public static ReturnData<String> KillProcess(String serverName, String sessionId, Boolean statusOnly)
        {
            ReturnData<String> returnData = new ReturnData<String>();

            SqlCommand sqlCommand = new SqlCommand();
            sqlCommand.CommandType = CommandType.Text;
            sqlCommand.CommandTimeout = 600;

            sqlCommand.CommandText = "Kill " + sessionId;

            if (statusOnly)
            {
                sqlCommand.CommandText += " WITH STATUSONLY";
            }



            sqlCommand.Connection = new SqlConnection("Persist Security Info=False; User ID=ProcMon; Password=xnocOB1; Initial Catalog=Central; Server=" + serverName);

            SqlDataAdapter dataAdapter = new SqlDataAdapter(sqlCommand);
            DataSet dataSet = new DataSet();

            try
            {
                using (sqlCommand.Connection)
                {
                    sqlCommand.Connection.Open();
                    sqlCommand.ExecuteNonQuery();
                }

                returnData.success = true;
            }
            catch (System.Exception error)
            {
                ErrorMessage errorMessage = new ErrorMessage();
                errorMessage.status = error.Source;
                errorMessage.message = error.Message;

                returnData.errors.Add(errorMessage);
                returnData.success = false;
            }

            return returnData;
        }


        public static ReturnData<List<Server>> GetServersList(Boolean hasMonitoredProcesses, Boolean hasLongRunningJobs)
        {
            ReturnData<List<Server>> returnData = new ReturnData<List<Server>>();

            try
            {
                returnData.data = GetAllServers(hasMonitoredProcesses, hasLongRunningJobs);
                returnData.success = true;
            }
            catch (System.Exception error)
            {
                ErrorMessage errorMessage = new ErrorMessage();
                errorMessage.status = error.Source;
                errorMessage.message = error.Message;

                returnData.errors.Add(errorMessage);
                returnData.success = false;
            }

            return returnData;
        }

        public static ReturnData<String> AddServer(String serverName, Boolean hasMonitoredProcesses, Boolean hasLongRunningJobs)
        {
            ReturnData<String> returnData = new ReturnData<String>();

            SqlCommand sqlCommand = new SqlCommand();
            sqlCommand.CommandType = CommandType.Text;
            sqlCommand.CommandTimeout = 600;

            sqlCommand.CommandText = "INSERT INTO Servers(serverName, hasMonitoredProcesses, hasLongRunningJobs) ";
            sqlCommand.CommandText += "VALUES ('" + serverName + "','" + hasMonitoredProcesses + "','" + hasLongRunningJobs + "')";


            sqlCommand.Connection = new SqlConnection("Persist Security Info=False; User ID=salpuser; Password=5@lpuser; Initial Catalog=SALP; Server=epssadb-po-01p");

            SqlDataAdapter dataAdapter = new SqlDataAdapter(sqlCommand);
            DataSet dataSet = new DataSet();

            try
            {
                using (sqlCommand.Connection)
                {
                    sqlCommand.Connection.Open();
                    sqlCommand.ExecuteNonQuery();
                }

                returnData.success = true;
            }
            catch (System.Exception error)
            {
                ErrorMessage errorMessage = new ErrorMessage();
                errorMessage.status = error.Source;
                errorMessage.message = error.Message;

                returnData.errors.Add(errorMessage);
                returnData.success = false;
            }

            return returnData;
        }

        public static ReturnData<String> EditServer(String id, String serverName, Boolean hasMonitoredProcesses, Boolean hasLongRunningJobs)
        {
            ReturnData<String> returnData = new ReturnData<String>();

            SqlCommand sqlCommand = new SqlCommand();
            sqlCommand.CommandType = CommandType.Text;
            sqlCommand.CommandTimeout = 600;

            sqlCommand.CommandText = "UPDATE Servers ";
            sqlCommand.CommandText += "SET ServerName='" + serverName + "', hasMonitoredProcesses = '" + hasMonitoredProcesses + "', hasLongRunningJobs= '" + hasLongRunningJobs + "' ";
            sqlCommand.CommandText += "WHERE id = '" + id + "'";


            sqlCommand.Connection = new SqlConnection("Persist Security Info=False; User ID=salpuser; Password=5@lpuser; Initial Catalog=SALP; Server=epssadb-po-01p");

            SqlDataAdapter dataAdapter = new SqlDataAdapter(sqlCommand);
            DataSet dataSet = new DataSet();

            try
            {
                using (sqlCommand.Connection)
                {
                    sqlCommand.Connection.Open();
                    sqlCommand.ExecuteNonQuery();
                }

                returnData.success = true;
            }
            catch (System.Exception error)
            {
                ErrorMessage errorMessage = new ErrorMessage();
                errorMessage.status = error.Source;
                errorMessage.message = error.Message;

                returnData.errors.Add(errorMessage);
                returnData.success = false;
            }

            return returnData;
        }


        public static ReturnData<String> DeleteServer(String id)
        {
            ReturnData<String> returnData = new ReturnData<String>();

            SqlCommand sqlCommand = new SqlCommand();
            sqlCommand.CommandType = CommandType.Text;
            sqlCommand.CommandTimeout = 600;

            sqlCommand.CommandText = "DELETE FROM Servers ";
            sqlCommand.CommandText += "WHERE id = '" + id + "'";


            sqlCommand.Connection = new SqlConnection("Persist Security Info=False; User ID=salpuser; Password=5@lpuser; Initial Catalog=SALP; Server=epssadb-po-01p");

            SqlDataAdapter dataAdapter = new SqlDataAdapter(sqlCommand);
            DataSet dataSet = new DataSet();

            try
            {
                using (sqlCommand.Connection)
                {
                    sqlCommand.Connection.Open();
                    sqlCommand.ExecuteNonQuery();
                }

                returnData.success = true;
            }
            catch (System.Exception error)
            {
                ErrorMessage errorMessage = new ErrorMessage();
                errorMessage.status = error.Source;
                errorMessage.message = error.Message;

                returnData.errors.Add(errorMessage);
                returnData.success = false;
            }

            return returnData;
        }



        public static ReturnData<List<ServerCPUUsage>> GetServerCPUUsage(String serverName)
        {

            ReturnData<List<ServerCPUUsage>> returnData = new ReturnData<List<ServerCPUUsage>>();
            returnData.data = new List<ServerCPUUsage>();

            SqlCommand sqlCommand = new SqlCommand();
            sqlCommand.CommandType = CommandType.Text;
            sqlCommand.CommandTimeout = 600;

            sqlCommand.CommandText = "SELECT [Timestamp], record_time, record.value('(./Record/@id)[1]', 'int') AS record_id, ";
            sqlCommand.CommandText += "record.value('(./Record/SchedulerMonitorEvent/SystemHealth/ProcessUtilization)[1]', 'int') / 100.0 AS [CPU] ";
            sqlCommand.CommandText += "FROM (SELECT [timestamp], ";
            sqlCommand.CommandText += "dateadd (ms, r.[timestamp] - sys.ms_ticks, getdate()) as record_time, ";
            sqlCommand.CommandText += "CONVERT(XML, record) AS [record] ";
            sqlCommand.CommandText += "FROM sys.dm_os_ring_buffers r WITH (NOLOCK) ";
            sqlCommand.CommandText += "cross join sys.dm_os_sys_info sys ";
            sqlCommand.CommandText += "WHERE ring_buffer_type = N'RING_BUFFER_SCHEDULER_MONITOR' ";
            sqlCommand.CommandText += "AND record LIKE N'%<SystemHealth>%' ";
            sqlCommand.CommandText += ") AS x";

            sqlCommand.Connection = new SqlConnection("Persist Security Info=False; User ID=ProcMon; Password=xnocOB1;  Initial Catalog=Central; Server=" + serverName);

            SqlDataAdapter dataAdapter = new SqlDataAdapter(sqlCommand);
            DataSet dataSet = new DataSet();

            try
            {
                using (sqlCommand.Connection)
                {
                    sqlCommand.Connection.Open();

                    using (sqlCommand)
                    {
                        dataAdapter.Fill(dataSet, "dataSet");
                    }

                    sqlCommand.Connection.Close();

                    for (int indexRow = 0; indexRow <= dataSet.Tables[0].Rows.Count - 1; indexRow++)
                    {
                        ServerCPUUsage serverCPUUsage = new ServerCPUUsage();

                        serverCPUUsage.timestamp = dataSet.Tables[0].Rows[indexRow]["Timestamp"].ToString();
                        serverCPUUsage.recordTime = dataSet.Tables[0].Rows[indexRow]["record_time"].ToString();
                        serverCPUUsage.cpu = (Convert.ToDecimal(dataSet.Tables[0].Rows[indexRow]["CPU"].ToString()) * 100).ToString("##");

                        returnData.data.Add(serverCPUUsage);
                    }

                    returnData.success = true;
                }
            }
            catch (System.Exception error)
            {
                ErrorMessage errorMessage = new ErrorMessage();
                errorMessage.status = error.Source;
                errorMessage.message = error.Message;

                returnData.errors.Add(errorMessage);
                returnData.success = false;
            }

            return returnData;
        }



        private static String GetCPUUsage(String serverName)
        {
            String result = "";

            SqlCommand sqlCommand = new SqlCommand();
            sqlCommand.CommandType = CommandType.Text;
            sqlCommand.CommandTimeout = 600;

            sqlCommand.CommandText = "SELECT TOP (1) [CPU] / 100.0 AS [CPU_usage] FROM (SELECT record.value('(./Record/@id)[1]', 'int') AS record_id , record.value('(./Record/SchedulerMonitorEvent/SystemHealth/ProcessUtilization)[1]', 'int') AS [CPU] ";
            sqlCommand.CommandText += "FROM (SELECT [timestamp], CONVERT(XML, record) AS [record] FROM sys.dm_os_ring_buffers WITH (NOLOCK) WHERE ring_buffer_type = N'RING_BUFFER_SCHEDULER_MONITOR' AND record LIKE N'%<SystemHealth>%') AS x ";
            sqlCommand.CommandText += ") AS y ORDER BY record_id DESC";

            sqlCommand.Connection = new SqlConnection("Persist Security Info=False; User ID=ProcMon; Password=xnocOB1; Server=" + serverName);

            SqlDataAdapter dataAdapter = new SqlDataAdapter(sqlCommand);
            DataSet dataSet = new DataSet();

            try
            {
                using (sqlCommand.Connection)
                {
                    sqlCommand.Connection.Open();

                    result = (Convert.ToDecimal(sqlCommand.ExecuteScalar()) * 100).ToString("##.##");
                }
            }
            catch (System.Exception error)
            {
                ErrorMessage errorMessage = new ErrorMessage();
                errorMessage.status = error.Source;
                errorMessage.message = error.Message;

            }

            return result;
        }


        private static String GetMemoryUsage(String serverName)
        {
            String result = "";

            SqlCommand sqlCommand = new SqlCommand();
            sqlCommand.CommandType = CommandType.Text;
            sqlCommand.CommandTimeout = 600;

            sqlCommand.CommandText = "Select * from OpenQuery([" + serverName;
            sqlCommand.CommandText += "], 'SELECT    1.0 - ( available_physical_memory_kb / ( total_physical_memory_kb * 1.0 ) ) memory_usage FROM master.sys.dm_os_sys_memory')";


            sqlCommand.Connection = new SqlConnection("Persist Security Info=False; User ID=salpuser; Password=5@lpuser; Initial Catalog=SALP; Server=epssadb-po-01p");

            SqlDataAdapter dataAdapter = new SqlDataAdapter(sqlCommand);
            DataSet dataSet = new DataSet();

            try
            {
                using (sqlCommand.Connection)
                {
                    sqlCommand.Connection.Open();

                    result = (Convert.ToDecimal(sqlCommand.ExecuteScalar()) * 100).ToString("##.##");
                }
            }
            catch (System.Exception error)
            {
                ErrorMessage errorMessage = new ErrorMessage();
                errorMessage.status = error.Source;
                errorMessage.message = error.Message;

            }

            return result;
        }


        private static List<Server> GetAllServers(Boolean hasMonitoredProcesses = false, Boolean hasLongRunningJobs = false)
        {
            List<Server> servers = new List<Server>();

            SqlCommand sqlCommand = new SqlCommand();
            sqlCommand.CommandType = CommandType.Text;
            sqlCommand.CommandText = "SELECT * FROM SALP.dbo.Servers ";


            if (hasMonitoredProcesses)
            {
                sqlCommand.CommandText += "WHERE hasMonitoredProcesses = 'True'";
            }

            if (hasLongRunningJobs)
            {
                sqlCommand.CommandText += "WHERE hasLongRunningJobs = 'True'";
            }

            sqlCommand.CommandText += "ORDER BY serverName";

            String connectionString = "Persist Security Info=False; User ID=salpuser; Password=5@lpuser; Initial Catalog=SALP; Server=epssadb-po-01p";

            sqlCommand.Connection = new SqlConnection(connectionString);

            SqlDataAdapter dataAdapter = new SqlDataAdapter(sqlCommand);
            DataSet dataSet = new DataSet();

            try
            {
                using (sqlCommand.Connection)
                {
                    sqlCommand.Connection.Open();

                    using (sqlCommand)
                    {
                        dataAdapter.Fill(dataSet, "dataSet");
                    }

                    sqlCommand.Connection.Close();

                    for (int indexRow = 0; indexRow <= dataSet.Tables[0].Rows.Count - 1; indexRow++)
                    {
                        Server server = new Server();

                        server.id = dataSet.Tables[0].Rows[indexRow]["id"].ToString();
                        server.serverName = dataSet.Tables[0].Rows[indexRow]["serverName"].ToString().Trim();
                        server.hasMonitoredProcesses = dataSet.Tables[0].Rows[indexRow]["hasMonitoredProcesses"].ToString();
                        server.hasLongRunningJobs = dataSet.Tables[0].Rows[indexRow]["hasLongRunningJobs"].ToString();

                        server.memoryUsage = GetMemoryUsage(server.serverName);
                        server.cpuUsage = GetCPUUsage(server.serverName);

                        servers.Add(server);
                    }
                }
            }
            catch (System.Exception error)
            {
                ErrorMessage errorMessage = new ErrorMessage();
                errorMessage.status = error.Source;
                errorMessage.message = error.Message;

            }

            return servers;

        }
    }
}