﻿using System;
using System.Runtime.Serialization;

namespace SALP.WebServices.DataTypes
{
    public class CRTicketDisplayItem
    {
        String _name;
        String _value;


        public CRTicketDisplayItem()
        {
            // Base Constructor
        }

        [DataMember]
        public String name
        {
            get { return this._name; }
            set { this._name = value; }
        }

        [DataMember]
        public String value
        {
            get { return this._value; }
            set { this._value = value; }
        }
    }
}