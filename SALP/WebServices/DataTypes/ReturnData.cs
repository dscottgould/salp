﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Collections;
using SALP.WebServices.DataTypes;

namespace SALP.WebServices.DataTypes
{
    [DataContract]
    public class ReturnData<customType>
    {
        private Boolean _success = false;
        private customType _data;
        private List<ErrorMessage> _errors = new List<ErrorMessage>();


        public ReturnData()
        {
            // Base Constructor
        }

        public ReturnData(ref customType data)
        {
            this._data = data;
        }

        [DataMember]
        public Boolean success
        {
            get { return this._success; }
            set { this._success = value; }
        }

        [DataMember]
        public customType data
        {
            get { return this._data; }
            set { this._data = value; }
        }

        [DataMember]
        public List<ErrorMessage> errors
        {
            get { return this._errors; }
            set { this._errors = value; }
        }
    }
}