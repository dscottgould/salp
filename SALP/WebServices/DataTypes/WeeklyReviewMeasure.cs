﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace SALP.WebServices.DataTypes
{
    public class WeeklyReviewMeasure
    {
        String _weekStarting;
        String _change;
        String _detect;
        String _engage;
        String _frequency;
        String _hwSw;
        String _mtbf;
        String _restore;
        String _toEngage;
        String _toRestore;

        public WeeklyReviewMeasure()
        {
            // Base Constructor
        }

        [DataMember]
        public String weekStarting
        {
            get { return this._weekStarting; }
            set { this._weekStarting = value; }
        }

        [DataMember]
        public String change
        {
            get { return this._change; }
            set { this._change = value; }
        }

        [DataMember]
        public String detect
        {
            get { return this._detect; }
            set { this._detect = value; }
        }

        [DataMember]
        public String engage
        {
            get { return this._engage; }
            set { this._engage = value; }
        }

        [DataMember]
        public String frequency
        {
            get { return this._frequency; }
            set { this._frequency = value; }
        }

        [DataMember]
        public String hwSw
        {
            get { return this._hwSw; }
            set { this._hwSw = value; }
        }

        [DataMember]
        public String mtbf
        {
            get { return this._mtbf; }
            set { this._mtbf = value; }
        }

        [DataMember]
        public String restore
        {
            get { return this._restore; }
            set { this._restore = value; }
        }

        [DataMember]
        public String toEngage
        {
            get { return this._toEngage; }
            set { this._toEngage = value; }
        }

        [DataMember]
        public String toRestore
        {
            get { return this._toRestore; }
            set { this._toRestore = value; }
        }
    }
}