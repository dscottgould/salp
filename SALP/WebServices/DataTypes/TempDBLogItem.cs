﻿using System;
using System.Runtime.Serialization;

namespace SALP.WebServices.DataTypes
{
    public class TempDBLogItem
    {
        String _date;
        String _tempDBLogPct;
        String _tempDBLogStatus;


        public TempDBLogItem()
        {
            // Base Constructor
        }

        [DataMember]
        public String date
        {
            get { return this._date; }
            set { this._date = value; }
        }

        [DataMember]
        public String tempDBLogPct
        {
            get { return this._tempDBLogPct; }
            set { this._tempDBLogPct = value; }
        }

        [DataMember]
        public String tempDBLogStatus
        {
            get { return this._tempDBLogStatus; }
            set { this._tempDBLogStatus = value; }
        }
    }
}