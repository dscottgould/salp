﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;


namespace SALP.WebServices.DataTypes
{
    public class FilterList
    {
        private string _filterName;
        private string _userName;
        private List<FilterListItem> _filterElement;


        public FilterList()
        {
            // Base Constructor
        }

        [DataMember]
        public String filterName
        {
            get { return this._filterName; }
            set { this._filterName = value; }
        }

        public String userName
        {
            get { return this._userName; }
            set { this._userName = value; }
        }

        public List<FilterListItem> FilterElement
        {
            get { return this._filterElement; }
            set { this._filterElement = value; }
        }


    }
}