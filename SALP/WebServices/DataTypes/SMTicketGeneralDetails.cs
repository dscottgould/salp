﻿using System;
using System.Runtime.Serialization;

namespace SALP.WebServices.DataTypes
{
    [DataContract]

    public class SMTicketGeneralDetails
    {
        private String _ticketId;
        private String _parentTicketId;
        private String _projectName;
        private String _regionName;
        private String _ticketType;
        private String _product;
        private String _smStatus;
        private String _createDate;
        private String _nextAction;
        private String _maintenanceType;
        private String _maintenanceDescription;
        private String _maintenaceCategory;
        private String _maintenanceDetail;
        private String _smFrequency;
        private String _videoAffected;
        private String _hsdAffected;
        private String _telephonyAffected;
        private String _totalCustomersAffected;
        private String _communitiesAffected;
        private String _actualStart;
        private String _actualEnd;
        private String _smDuration;
        private String _resolutionDescription;
        private String _indicatorCity;
        private String _indicatorState;
        private String _localManagementApprover;
        private String _localManagementPhone;
        private String _supportAreaName;
        private String _fullName;
        private String _apCreateDate;
        private String _plannedStart;
        private String _plannedEnd;

        public SMTicketGeneralDetails()
        {
            // Base Constructor
        }

        [DataMember]
        public String ticketId
        {
            get { return this._ticketId; }
            set { this._ticketId = value; }
        }

        [DataMember]
        public String parentTicketId
        {
            get { return this._parentTicketId; }
            set { this._parentTicketId = value; }
        }

        [DataMember]
        public String projectName
        {
            get { return this._projectName; }
            set { this._projectName = value; }
        }

        [DataMember]
        public String ticketType
        {
            get { return this._ticketType; }
            set { this._ticketType = value; }
        }

        [DataMember]
        public String regionName
        {
            get { return this._regionName; }
            set { this._regionName = value; }
        }

        [DataMember]
        public String product
        {
            get { return this._product; }
            set { this._product = value; }
        }

        [DataMember]
        public String smStatus
        {
            get { return this._smStatus; }
            set { this._smStatus = value; }
        }

        [DataMember]
        public String createDate
        {
            get { return this._createDate; }
            set { this._createDate = value; }
        }

        [DataMember]
        public String nextAction
        {
            get { return this._nextAction; }
            set { this._nextAction = value; }
        }

        [DataMember]
        public String maintenanceType
        {
            get { return this._maintenanceType; }
            set { this._maintenanceType = value; }
        }

        [DataMember]
        public String maintenanceDescription
        {
            get { return this._maintenanceDescription; }
            set { this._maintenanceDescription = value; }
        }

        [DataMember]
        public String maintenaceCategory
        {
            get { return this._maintenaceCategory; }
            set { this._maintenaceCategory = value; }
        }

        [DataMember]
        public String maintenanceDetail
        {
            get { return this._maintenanceDetail; }
            set { this._maintenanceDetail = value; }
        }

        [DataMember]
        public String smFrequency
        {
            get { return this._smFrequency; }
            set { this._smFrequency = value; }
        }

        [DataMember]
        public String videoAffected
        {
            get { return this._videoAffected; }
            set { this._videoAffected = value; }
        }

        [DataMember]
        public String hsdAffected
        {
            get { return this._hsdAffected; }
            set { this._hsdAffected = value; }
        }

        [DataMember]
        public String telephonyAffected
        {
            get { return this._telephonyAffected; }
            set { this._telephonyAffected = value; }
        }

        [DataMember]
        public String totalCustomersAffected
        {
            get { return this._totalCustomersAffected; }
            set { this._totalCustomersAffected = value; }
        }

        [DataMember]
        public String communitiesAffected
        {
            get { return this._communitiesAffected; }
            set { this._communitiesAffected = value; }
        }

        [DataMember]
        public String actualStart
        {
            get { return this._actualStart; }
            set { this._actualStart = value; }
        }

        [DataMember]
        public String actualEnd
        {
            get { return this._actualEnd; }
            set { this._actualEnd = value; }
        }

        [DataMember]
        public String smDuration
        {
            get { return this._smDuration; }
            set { this._smDuration = value; }
        }

        [DataMember]
        public String resolutionDescription
        {
            get { return this._resolutionDescription; }
            set { this._resolutionDescription = value; }
        }

        [DataMember]
        public String indicatorCity
        {
            get { return this._indicatorCity; }
            set { this._indicatorCity = value; }
        }

        [DataMember]
        public String indicatorState
        {
            get { return this._indicatorState; }
            set { this._indicatorState = value; }
        }

        [DataMember]
        public String localManagementApprover
        {
            get { return this._localManagementApprover; }
            set { this._localManagementApprover = value; }
        }

        [DataMember]
        public String localManagementPhone
        {
            get { return this._localManagementPhone; }
            set { this._localManagementPhone = value; }
        }

        [DataMember]
        public String supportAreaName
        {
            get { return this._supportAreaName; }
            set { this._supportAreaName = value; }
        }

        [DataMember]
        public String fullName
        {
            get { return this._fullName; }
            set { this._fullName = value; }
        }

        [DataMember]
        public String apCreateDate
        {
            get { return this._apCreateDate; }
            set { this._apCreateDate = value; }
        }

        [DataMember]
        public String plannedStart
        {
            get { return this._plannedStart; }
            set { this._plannedStart = value; }
        }

        [DataMember]
        public String plannedEnd
        {
            get { return this._plannedEnd; }
            set { this._plannedEnd = value; }
        }
    }
}