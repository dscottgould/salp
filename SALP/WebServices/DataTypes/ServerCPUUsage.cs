﻿using System;
using System.Runtime.Serialization;

namespace SALP.WebServices.DataTypes
{
    public class ServerCPUUsage
    {
        private String _timestamp;
        private String _recordTime;
        private String _cpu;

        public ServerCPUUsage()
        {
            // Base Constructor
        }

        [DataMember]
        public String timestamp
        {
            get { return this._timestamp; }
            set { this._timestamp = value; }
        }

        [DataMember]
        public String recordTime
        {
            get { return this._recordTime; }
            set { this._recordTime = value; }
        }

        [DataMember]
        public String cpu
        {
            get { return this._cpu; }
            set { this._cpu = value; }
        }
    }
}