﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace SALP.WebServices.DataTypes
{
    [DataContract]

    public class StackedBinsBase
    {
        private List<StackedBinsList> _StackedBins;
        public StackedBinsBase()
        {
            // Base Constructor
        }

        [DataMember]
        public List<StackedBinsList> StackedBins
        {
            get { return this._StackedBins; }
            set { this._StackedBins = value; }
        }
    }
}