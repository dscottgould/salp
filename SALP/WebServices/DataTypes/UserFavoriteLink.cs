﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace SALP.WebServices.DataTypes
{
    public class UserFavoriteLink
    {
        String _linkId;
        String _description;
        String _link;
        String _group;
        String _display;

        public UserFavoriteLink()
        {
            // Base Constructor
        }


        [DataMember]
        public String linkId
        {
            get { return this._linkId; }
            set { this._linkId = value; }
        }

        [DataMember]
        public String description
        {
            get { return this._description; }
            set { this._description = value; }
        }

        [DataMember]
        public String link
        {
            get { return this._link; }
            set { this._link = value; }
        }

        [DataMember]
        public String group
        {
            get { return this._group; }
            set { this._group = value; }
        }

        [DataMember]
        public String display
        {
            get { return this._display; }
            set { this._display = value; }
        }
    }
}