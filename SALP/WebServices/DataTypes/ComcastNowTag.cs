﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace SALP.WebServices.DataTypes
{
    public class ComcastNowTag
    {
        private String _id;
        private String _name;
        private String _url;

        public ComcastNowTag()
        {
            // Base Constructor
        }

        [DataMember]
        public String id
        {
            get { return this._id; }
            set { this._id = value; }
        }

        [DataMember]
        public String name
        {
            get { return this._name; }
            set { this._name = value; }
        }

        [DataMember]
        public String url
        {
            get { return this._url; }
            set { this._url = value; }
        }

    }
}