﻿using System;
using System.Runtime.Serialization;

namespace SALP.WebServices.DataTypes
{
    [DataContract]

    public class MaintenanceCausedSev1MonthOverMonth
    {
        private String _maintenanceMonth;
        private String _failCountPreviousYear;
        private String _failCountCurrentYear;
        private String _failPctPreviousYear;
        private String _failPctCurrentYear;
        private String _totalCountPreviousYear;
        private String _totalCountCurrentYear;

        public MaintenanceCausedSev1MonthOverMonth()
        {
            // Base Constructor
        }


        [DataMember]
        public String maintenanceMonth
        {
            get { return this._maintenanceMonth; }
            set { this._maintenanceMonth = value; }
        }

        [DataMember]
        public String failCountPreviousYear
        {
            get { return this._failCountPreviousYear; }
            set { this._failCountPreviousYear = value; }
        }

        [DataMember]
        public String failCountCurrentYear
        {
            get { return this._failCountCurrentYear; }
            set { this._failCountCurrentYear = value; }
        }

        [DataMember]
        public String failPctPreviousYear
        {
            get { return this._failPctPreviousYear; }
            set { this._failPctPreviousYear = value; }
        }


        [DataMember]
        public String failPctCurrentYear
        {
            get { return this._failPctCurrentYear; }
            set { this._failPctCurrentYear = value; }
        }


        [DataMember]
        public String totalCountPreviousYear
        {
            get { return this._totalCountPreviousYear; }
            set { this._totalCountPreviousYear = value; }
        }

        [DataMember]
        public String totalCountCurrentYear
        {
            get { return this._totalCountCurrentYear; }
            set { this._totalCountCurrentYear = value; }
        }
    }
}