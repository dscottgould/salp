﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace SALP.WebServices.DataTypes
{
    [DataContract]
    public class CMTreeMapNode
    {
        private String _text;
        private String _supervisor;
        private Int32 _value;
        private Int32 _failureRate;
        private Boolean _expanded = false;
        private Boolean _isChildNode = true;
        private List<CMTreeMapAttribute> _children;

        public CMTreeMapNode()
        {
            // Base Constructor
        }


        [DataMember]
        public String text
        {
            get { return this._text; }
            set { this._text = value; }
        }

        [DataMember]
        public String supervisor
        {
            get { return this._supervisor; }
            set { this._supervisor = value; }
        }

        [DataMember]
        public Int32 value
        {
            get { return this._value; }
            set { this._value = value; }
        }

        [DataMember]
        public Int32 failureRate
        {
            get { return this._failureRate; }
            set { this._failureRate = value; }
        }

        [DataMember]
        public Boolean expanded
        {
            get { return this._expanded; }
            set { this._expanded = value; }
        }

        [DataMember]
        public Boolean isChildNode
        {
            get { return this._isChildNode; }
            set { this._isChildNode = value; }
        }

        [DataMember]
        public List<CMTreeMapAttribute> children
        {
            get { return this._children; }
            set { this._children = value; }
        }
    }
}