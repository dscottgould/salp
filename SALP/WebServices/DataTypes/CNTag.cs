﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace SALP.WebServices.DataTypes
{
    public class CNTag
    {
        private String _name;
        private String _url;

        public CNTag()
        {
            // Base Constructo
        }

        [DataMember]
        public String name
        {
            get { return this._name; }
            set { this._name = value; }
        }

        [DataMember]
        public String url
        {
            get { return this._url; }
            set { this._url = value; }
        }
    }
}