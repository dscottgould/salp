﻿using System;
using System.Runtime.Serialization;

namespace SALP.WebServices.DataTypes
{
    [DataContract]

    public class OETicketAttribute
    {
        private String _sourceTicket;
        private String _attributeLabel;
        private String _attributeValue;

        public OETicketAttribute()
        {
            // Base Constructor
        }

        [DataMember]
        public String sourceTicket
        {
            get { return this._sourceTicket; }
            set { this._sourceTicket = value; }
        }

        [DataMember]
        public String attributeLabel
        {
            get { return this._attributeLabel; }
            set { this._attributeLabel = value; }
        }

        [DataMember]
        public String attributeValue
        {
            get { return this._attributeValue; }
            set { this._attributeValue = value; }
        }
    }
}