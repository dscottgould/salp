﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace SALP.WebServices.DataTypes
{
    [DataContract]
    public class Tickets
    {
        Ticket _oldTicket = new Ticket();
        Ticket _newTicket = new Ticket();

        public Tickets()
        {
            // Base Constructor
        }

        [DataMember]
        public Ticket oldTicket
        {
            get { return this._oldTicket; }
            set { this._oldTicket = value; }
        }

        [DataMember]
        public Ticket newTicket
        {
            get { return this._newTicket; }
            set { this._newTicket = value; }
        }
    }
}