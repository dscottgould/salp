﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace SALP.WebServices.DataTypes
{
    public class ErrorMessage
    {
        private String _status;
        private String _message;

        public ErrorMessage()
        {
            // Base Constructor
        }

        [DataMember]
        public String status
        {
            get { return this._status; }
            set { this._status = value; }
        }

        [DataMember]
        public String message
        {
            get { return this._message; }
            set { this._message = value; }
        }

    }
}