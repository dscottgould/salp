﻿using System;
using System.Runtime.Serialization;


namespace SALP.WebServices.DataTypes
{
    public class ServerProcessesItem
    {
        String _statementText;
        String _databaseName;
        String _cpuTime;
        String _runningMinutes;
        String _runningFrom;
        String _runningBy;
        String _sessionId;
        String _blockedBy;
        String _reads;
        String _writes;
        String _programName;
        String _loginName;
        String _status;
        String _waitTime;
        String _lastWaitType;
        String _cmd;
        String _lastRequestStartTime;
        String _logicalReads;


        public ServerProcessesItem()
        {
            // Base Constructor
        }

        [DataMember]
        public String statementText
        {
            get { return this._statementText; }
            set { this._statementText = value; }
        }

        [DataMember]
        public String databaseName
        {
            get { return this._databaseName; }
            set { this._databaseName = value; }
        }

        [DataMember]
        public String cpuTime
        {
            get { return this._cpuTime; }
            set { this._cpuTime = value; }
        }

        [DataMember]
        public String runningMinutes
        {
            get { return this._runningMinutes; }
            set { this._runningMinutes = value; }
        }

        [DataMember]
        public String runningFrom
        {
            get { return this._runningFrom; }
            set { this._runningFrom = value; }
        }

        [DataMember]
        public String runningBy
        {
            get { return this._runningBy; }
            set { this._runningBy = value; }
        }

        [DataMember]
        public String sessionId
        {
            get { return this._sessionId; }
            set { this._sessionId = value; }
        }

        [DataMember]
        public String blockedBy
        {
            get { return this._blockedBy; }
            set { this._blockedBy = value; }
        }

        [DataMember]
        public String reads
        {
            get { return this._reads; }
            set { this._reads = value; }
        }

        [DataMember]
        public String writes
        {
            get { return this._writes; }
            set { this._writes = value; }
        }

        [DataMember]
        public String programName
        {
            get { return this._programName; }
            set { this._programName = value; }
        }

        [DataMember]
        public String loginName
        {
            get { return this._loginName; }
            set { this._loginName = value; }
        }

        [DataMember]
        public String status
        {
            get { return this._status; }
            set { this._status = value; }
        }

        [DataMember]
        public String waitTime
        {
            get { return this._waitTime; }
            set { this._waitTime = value; }
        }

        [DataMember]
        public String lastWaitType
        {
            get { return this._lastWaitType; }
            set { this._lastWaitType = value; }
        }

        [DataMember]
        public String cmd
        {
            get { return this._cmd; }
            set { this._cmd = value; }
        }

        [DataMember]
        public String lastRequestStartTime
        {
            get { return this._lastRequestStartTime; }
            set { this._lastRequestStartTime = value; }
        }

        [DataMember]
        public String logicalReads
        {
            get { return this._logicalReads; }
            set { this._logicalReads = value; }
        }
    }
}