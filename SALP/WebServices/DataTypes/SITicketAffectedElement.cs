﻿using System;
using System.Runtime.Serialization;

namespace SALP.WebServices.DataTypes
{
    public class SITicketAffectedElement
    {
        private String _sourceTicket;
        private String _status;
        private String _entryId;
        private String _serviceAffected;
        private String _elementName;
        private String _elementType;
        private String _systemName;
        private String _telephonyAffected;
        private String _videoAffected;
        private String _hSDAffected;
        private String _totalAffected;
        private String _aeStatus;
        private String _actualStart;
        private String _actualEnd;

        public SITicketAffectedElement()
        {
            // Base Constructor
        }

        [DataMember]
        public String sourceTicket
        {
            get { return this._sourceTicket; }
            set { this._sourceTicket = value; }
        }

        [DataMember]
        public String status
        {
            get { return this._status; }
            set { this._status = value; }
        }

        [DataMember]
        public String entryId
        {
            get { return this._entryId; }
            set { this._entryId = value; }
        }

        [DataMember]
        public String serviceAffected
        {
            get { return this._serviceAffected; }
            set { this._serviceAffected = value; }
        }

        [DataMember]
        public String elementName
        {
            get { return this._elementName; }
            set { this._elementName = value; }
        }

        [DataMember]
        public String elementType
        {
            get { return this._elementType; }
            set { this._elementType = value; }
        }

        [DataMember]
        public String systemName
        {
            get { return this._systemName; }
            set { this._systemName = value; }
        }

        [DataMember]
        public String telephonyAffected
        {
            get { return this._telephonyAffected; }
            set { this._telephonyAffected = value; }
        }

        [DataMember]
        public String videoAffected
        {
            get { return this._videoAffected; }
            set { this._videoAffected = value; }
        }

        [DataMember]
        public String hSDAffected
        {
            get { return this._hSDAffected; }
            set { this._hSDAffected = value; }
        }

        [DataMember]
        public String totalAffected
        {
            get { return this._totalAffected; }
            set { this._totalAffected = value; }
        }

        [DataMember]
        public String aeStatus
        {
            get { return this._aeStatus; }
            set { this._aeStatus = value; }
        }

        [DataMember]
        public String actualStart
        {
            get { return this._actualStart; }
            set { this._actualStart = value; }
        }

        [DataMember]
        public String actualEnd
        {
            get { return this._actualEnd; }
            set { this._actualEnd = value; }
        }
    }
}