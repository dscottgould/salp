﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace SALP.WebServices.DataTypes
{
    public class SITicketTimeline
    {
        String _ticketNumber;
        String _name;
        String _start;
        String _duration;
        String _durationNeg;

        public SITicketTimeline()
        {
            // Base Constructor
        }

        [DataMember]
        public String ticketNumber
        {
            get { return this._ticketNumber; }
            set { this._ticketNumber = value; }
        }

        [DataMember]
        public String name
        {
            get { return this._name; }
            set { this._name = value; }
        }

        [DataMember]
        public String start
        {
            get { return this._start; }
            set { this._start = value; }
        }

        [DataMember]
        public String duration
        {
            get { return this._duration; }
            set { this._duration = value; }
        }

        [DataMember]
        public String durationNeg
        {
            get { return this._durationNeg; }
            set { this._durationNeg = value; }
        }
    }
}