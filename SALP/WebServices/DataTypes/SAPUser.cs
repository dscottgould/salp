﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace SALP.WebServices.DataTypes
{
    [DataContract]

    public class SAPUser
    {
        private String _displayName;
        private String _name;
        private String _hierarchyLevel1;
        private String _department;
        private String _orgUnit;
        private String _jobDescription;
        private String _emailAddress;
        private String _workPhone;
        private String _loginId;
        private String _supervisor;

        public SAPUser()
        {
            // Base Constructor
        }

        [DataMember]
        public String displayName
        {
            get { return this._displayName; }
            set { this._displayName = value; }
        }

        [DataMember]
        public String name
        {
            get { return this._name; }
            set { this._name = value; }
        }

        [DataMember]
        public String hierarchyLevel1
        {
            get { return this._hierarchyLevel1; }
            set { this._hierarchyLevel1 = value; }
        }

        [DataMember]
        public String department
        {
            get { return this._department; }
            set { this._department = value; }
        }

        [DataMember]
        public String orgUnit
        {
            get { return this._orgUnit; }
            set { this._orgUnit = value; }
        }

        [DataMember]
        public String jobDescription
        {
            get { return this._jobDescription; }
            set { this._jobDescription = value; }
        }

        [DataMember]
        public String emailAddress
        {
            get { return this._emailAddress; }
            set { this._emailAddress = value; }
        }

        [DataMember]
        public String workPhone
        {
            get { return this._workPhone; }
            set { this._workPhone = value; }
        }

        [DataMember]
        public String loginId
        {
            get { return this._loginId; }
            set { this._loginId = value; }
        }

        [DataMember]
        public String supervisor
        {
            get { return this._supervisor; }
            set { this._supervisor = value; }
        }
    }
}