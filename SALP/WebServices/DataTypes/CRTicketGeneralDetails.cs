﻿using System;
using System.Runtime.Serialization;

namespace SALP.WebServices.DataTypes
{
    [DataContract]

    public class CRTicketGeneralDetails
    {
        private String _ticketId;
        private String _problemSummary;
        private String _regionName;
        private String _supportAreaName;
        private String _queueName;
        private String _fullName;
        private String _product;
        private String _crStatus;
        private String _assignedDate;
        private String _closedDate;
        private String _originalTicket;
        private String _reworkTicket;
        private String _nextAction;
        private String _problemCode;
        private String _category;
        private String _subCategory;
        private String _causeCode;
        private String _causeDescription;
        private String _causeCategory;
        private String _causeSubcategory;
        private String _solutionCode;
        private String _solutionDescription;
        private String _solutionCategory;
        private String _solutionSubcategory;
        private String _troubleStart;
        private String _ttrStart;
        private String _ttrStop;
        private String _ttrSla;
        private String _indicatorCity;
        private String _indicatorState;
        private String _contactName;
        private String _contactPhone;
        private String _contactEmail;
        private String _customerId;
        private String _accountNumber;
        private String _node;
        private String _systemId;
        private String _networkElement;
        private String _sysNm;


        public CRTicketGeneralDetails()
        {
            // Base Constructor
        }

        [DataMember]
        public String ticketId
        {
            get { return this._ticketId; }
            set { this._ticketId = value; }
        }

        [DataMember]
        public String problemSummary
        {
            get { return this._problemSummary; }
            set { this._problemSummary = value; }
        }

        [DataMember]
        public String regionName
        {
            get { return this._regionName; }
            set { this._regionName = value; }
        }

        [DataMember]
        public String supportAreaName
        {
            get { return this._supportAreaName; }
            set { this._supportAreaName = value; }
        }

        [DataMember]
        public String queueName
        {
            get { return this._queueName; }
            set { this._queueName = value; }
        }

        [DataMember]
        public String fullName
        {
            get { return this._fullName; }
            set { this._fullName = value; }
        }

        [DataMember]
        public String product
        {
            get { return this._product; }
            set { this._product = value; }
        }

        [DataMember]
        public String crStatus
        {
            get { return this._crStatus; }
            set { this._crStatus = value; }
        }

        [DataMember]
        public String assignedDate
        {
            get { return this._assignedDate; }
            set { this._assignedDate = value; }
        }

        [DataMember]
        public String closedDate
        {
            get { return this._closedDate; }
            set { this._closedDate = value; }
        }

        [DataMember]
        public String originalTicket
        {
            get { return this._originalTicket; }
            set { this._originalTicket = value; }
        }

        [DataMember]
        public String reworkTicket
        {
            get { return this._reworkTicket; }
            set { this._reworkTicket = value; }
        }

        [DataMember]
        public String nextAction
        {
            get { return this._nextAction; }
            set { this._nextAction = value; }
        }

        [DataMember]
        public String problemCode
        {
            get { return this._problemCode; }
            set { this._problemCode = value; }
        }

        [DataMember]
        public String category
        {
            get { return this._category; }
            set { this._category = value; }
        }

        [DataMember]
        public String subCategory
        {
            get { return this._subCategory; }
            set { this._subCategory = value; }
        }

        [DataMember]
        public String causeCode
        {
            get { return this._causeCode; }
            set { this._causeCode = value; }
        }

        [DataMember]
        public String causeDescription
        {
            get { return this._causeDescription; }
            set { this._causeDescription = value; }
        }

        [DataMember]
        public String causeCategory
        {
            get { return this._causeCategory; }
            set { this._causeCategory = value; }
        }

        [DataMember]
        public String causeSubcategory
        {
            get { return this._causeSubcategory; }
            set { this._causeSubcategory = value; }
        }

        [DataMember]
        public String solutionCode
        {
            get { return this._solutionCode; }
            set { this._solutionCode = value; }
        }

        [DataMember]
        public String solutionDescription
        {
            get { return this._solutionDescription; }
            set { this._solutionDescription = value; }
        }

        [DataMember]
        public String solutionCategory
        {
            get { return this._solutionCategory; }
            set { this._solutionCategory = value; }
        }

        [DataMember]
        public String solutionSubcategory
        {
            get { return this._solutionSubcategory; }
            set { this._solutionSubcategory = value; }
        }

        [DataMember]
        public String troubleStart
        {
            get { return this._troubleStart; }
            set { this._troubleStart = value; }
        }

        [DataMember]
        public String ttrStart
        {
            get { return this._ttrStart; }
            set { this._ttrStart = value; }
        }

        [DataMember]
        public String ttrStop
        {
            get { return this._ttrStop; }
            set { this._ttrStop = value; }
        }

        [DataMember]
        public String ttrSla
        {
            get { return this._ttrSla; }
            set { this._ttrSla = value; }
        }

        [DataMember]
        public String indicatorCity
        {
            get { return this._indicatorCity; }
            set { this._indicatorCity = value; }
        }

        [DataMember]
        public String indicatorState
        {
            get { return this._indicatorState; }
            set { this._indicatorState = value; }
        }

        [DataMember]
        public String contactName
        {
            get { return this._contactName; }
            set { this._contactName = value; }
        }

        [DataMember]
        public String contactPhone
        {
            get { return this._contactPhone; }
            set { this._contactPhone = value; }
        }

        [DataMember]
        public String contactEmail
        {
            get { return this._contactEmail; }
            set { this._contactEmail = value; }
        }

        [DataMember]
        public String customerId
        {
            get { return this._customerId; }
            set { this._customerId = value; }
        }

        [DataMember]
        public String accountNumber
        {
            get { return this._accountNumber; }
            set { this._accountNumber = value; }
        }

        [DataMember]
        public String node
        {
            get { return this._node; }
            set { this._node = value; }
        }

        [DataMember]
        public String systemId
        {
            get { return this._systemId; }
            set { this._systemId = value; }
        }

        [DataMember]
        public String networkElement
        {
            get { return this._networkElement; }
            set { this._networkElement = value; }
        }

        [DataMember]
        public String sysNm
        {
            get { return this._sysNm; }
            set { this._sysNm = value; }
        }
    }
}