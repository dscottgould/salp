﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace SALP.WebServices.DataTypes
{
    public class CustomerTicket
    {
        String _customerTicketId;
        String _problemSummary;
        String _createDate;
        String _contactName;
        String _status;
        String _node;
           

        public CustomerTicket()
        {
            // Base Constructor
        }

        [DataMember]
        public String customerTicketId
        {
            get { return this._customerTicketId; }
            set { this._customerTicketId = value; }
        }

        [DataMember]
        public String problemSummary
        {
            get { return this._problemSummary; }
            set { this._problemSummary = value; }
        }

        [DataMember]
        public String createDate
        {
            get { return this._createDate; }
            set { this._createDate = value; }
        }

        [DataMember]
        public String contactName
        {
            get { return this._contactName; }
            set { this._contactName = value; }
        }

        [DataMember]
        public String status
        {
            get { return this._status; }
            set { this._status = value; }
        }

        [DataMember]
        public String node
        {
            get { return this._node; }
            set { this._node = value; }
        }
    }
}