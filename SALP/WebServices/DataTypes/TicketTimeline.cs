﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace SALP.WebServices.DataTypes
{
    public class TicketTimeline
    {
        String _ticketId;
        String _alarmStartDate;
        String _workingStartDate;
        String _resolvedDate;
        String _actualStartDate;
        String _actualEndDate;


        public TicketTimeline()
        {
            // Base Constructor
        }


        [DataMember]
        public String ticketId
        {
            get { return this._ticketId; }
            set { this._ticketId = value; }
        }

        [DataMember]
        public String alarmStartDate
        {
            get { return this._alarmStartDate; }
            set { this._alarmStartDate = value; }
        }

        [DataMember]
        public String workingStartDate
        {
            get { return this._workingStartDate; }
            set { this._workingStartDate = value; }
        }

        [DataMember]
        public String resolvedDate
        {
            get { return this._resolvedDate; }
            set { this._resolvedDate = value; }
        }

        [DataMember]
        public String actualStartDate
        {
            get { return this._actualStartDate; }
            set { this._actualStartDate = value; }
        }

        [DataMember]
        public String actualEndDate
        {
            get { return this._actualEndDate; }
            set { this._actualEndDate = value; }
        }
    }
}