﻿using System;
using System.Runtime.Serialization;

namespace SALP.WebServices.DataTypes
{
    [DataContract]

    public class SITicketCustomerTicket
    {
        private String _customerTicketId;
        private String _problemSummary;
        private String _createDate;
        private String _contactName;
        private String _status;
        private String _node;

        public SITicketCustomerTicket()
        {
            // Base Constructor
        }

        [DataMember]
        public String customerTicketId
        {
            get { return this._customerTicketId; }
            set { this._customerTicketId = value; }
        }

        [DataMember]
        public String problemSummary
        {
            get { return this._problemSummary; }
            set { this._problemSummary = value; }
        }

        [DataMember]
        public String createDate
        {
            get { return this._createDate; }
            set { this._createDate = value; }
        }

        [DataMember]
        public String contactName
        {
            get { return this._contactName; }
            set { this._contactName = value; }
        }

        [DataMember]
        public String status
        {
            get { return this._status; }
            set { this._status = value; }
        }

        [DataMember]
        public String node
        {
            get { return this._node; }
            set { this._node = value; }
        }
    }
}