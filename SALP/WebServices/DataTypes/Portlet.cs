﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace SALP.WebServices.DataTypes
{
    [DataContract]
    public class Portlet
    {
        String _id;
        String _displayName;
        String _objectClass;
        String _category;

        public Portlet()
        {
            // Base Constructor
        }

        [DataMember]
        public String id
        {
            get { return this._id; }
            set { this._id = value; }
        }

        [DataMember]
        public String displayName
        {
            get { return this._displayName; }
            set { this._displayName = value; }
        }

        [DataMember]
        public String objectClass
        {
            get { return this._objectClass; }
            set { this._objectClass = value; }
        }

        [DataMember]
        public String category
        {
            get { return this._category; }
            set { this._category = value; }
        }

    }
}