﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace SALP.WebServices.DataTypes
{
    [DataContract]
    public class Stocks
    {
        String _symbol;
        String _last;
        String _change;

        public Stocks()
        {
            // Base Constructor
        }

        [DataMember]
        public String symbol
        {
            get { return this._symbol; }
            set { this._symbol = value; }
        }

        [DataMember]
        public String last
        {
            get { return this._last; }
            set { this._last = value; }
        }

        [DataMember]
        public String change
        {
            get { return this._change; }
            set { this._change = value; }
        }

    }
}