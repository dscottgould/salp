﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace SALP.WebServices.DataTypes
{
    public class UserFilter
    {
        private string _userFilter;

        public UserFilter()
        {
            // Base Constructor
        }

        [DataMember]
        public string userFilter
        {
            get { return this._userFilter; }
            set { this._userFilter = value; }
        }
    }
}