﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;


namespace SALP.WebServices.DataTypes
{
    public class SAPSubordinate
    {
        private String _employeeId;
        private String _name;
        private String _loginId;
        private String _jobLevelCode;
        private String _employeeStatusDescription;
        private String _supervisorName;
        private String _employeeStatus;
        private String _workEmail;
        private String _path;
        private String _direct;

        public SAPSubordinate()
        {
            // Base Constructor
        }

        [DataMember]
        public String employeeId
        {
            get { return this._employeeId; }
            set { this._employeeId = value; }
        }

        [DataMember]
        public String name
        {
            get { return this._name; }
            set { this._name = value; }
        }

        [DataMember]
        public String loginId
        {
            get { return this._loginId; }
            set { this._loginId = value; }
        }

        [DataMember]
        public String jobLevelCode
        {
            get { return this._jobLevelCode; }
            set { this._jobLevelCode = value; }
        }

        [DataMember]
        public String employeeStatusDescription
        {
            get { return this._employeeStatusDescription; }
            set { this._employeeStatusDescription = value; }
        }

        [DataMember]
        public String supervisorName
        {
            get { return this._supervisorName; }
            set { this._supervisorName = value; }
        }

        [DataMember]
        public String employeeStatus
        {
            get { return this._employeeStatus; }
            set { this._employeeStatus = value; }
        }

        [DataMember]
        public String workEmail
        {
            get { return this._workEmail; }
            set { this._workEmail = value; }
        }

        [DataMember]
        public String path
        {
            get { return this._path; }
            set { this._path = value; }
        }

        [DataMember]
        public String direct
        {
            get { return this._direct; }
            set { this._direct = value; }
        }
    }
}