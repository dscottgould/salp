﻿using System;
using System.Runtime.Serialization;


namespace SALP.WebServices.DataTypes
{
    public class ApplicationEventMonitorItem
    {
        String _date;
        String _oBIDR_po_P01;
        String _oBIPROC_WC_P01;
        String _oBIPROC_WC_P01_APP;
        String _oBIWEB_WC_P01;

        public ApplicationEventMonitorItem()
        {
            // Base Constructor
        }

        [DataMember]
        public String date
        {
            get { return this._date; }
            set { this._date = value; }
        }

        [DataMember]
        public String oBIDR_po_P01
        {
            get { return this._oBIDR_po_P01; }
            set { this._oBIDR_po_P01 = value; }
        }

        [DataMember]
        public String oBIPROC_WC_P01
        {
            get { return this._oBIPROC_WC_P01; }
            set { this._oBIPROC_WC_P01 = value; }
        }

        [DataMember]
        public String oBIPROC_WC_P01_APP
        {
            get { return this._oBIPROC_WC_P01_APP; }
            set { this._oBIPROC_WC_P01_APP = value; }
        }

        [DataMember]
        public String oBIWEB_WC_P01
        {
            get { return this._oBIWEB_WC_P01; }
            set { this._oBIWEB_WC_P01 = value; }
        }
    }
}