﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;


namespace SALP.WebServices.DataTypes
{
    [DataContract]
    public class SIEventFunnel
    {
        private String _ticketId;
        private String _timeFrame;
        private String _agentOffered;
        private String _deflected;
        private String _fiveWeekBaseline;

        public SIEventFunnel()
        {
            // Base Constructor
        }

        [DataMember]
        public String ticketId
        {
            get { return this._ticketId; }
            set { this._ticketId = value; }
        }

        [DataMember]
        public String timeFrame
        {
            get { return this._timeFrame; }
            set { this._timeFrame = value; }
        }

        [DataMember]
        public String agentOffered
        {
            get { return this._agentOffered; }
            set { this._agentOffered = value; }
        }

        [DataMember]
        public String deflected
        {
            get { return this._deflected; }
            set { this._deflected = value; }
        }

        [DataMember]
        public String fiveWeekBaseline
        {
            get { return this._fiveWeekBaseline; }
            set { this._fiveWeekBaseline = value; }
        }
    }
}