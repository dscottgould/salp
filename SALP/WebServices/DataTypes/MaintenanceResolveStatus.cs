﻿using System;
using System.Runtime.Serialization;

namespace SALP.WebServices.DataTypes
{
    [DataContract]

    public class MaintenanceResolveStatus
    {
        private String _month;
        private String _year;
        private String _comcastDivision;
        private String _vpDepartment;
        private String _vpName;
        private String _dirGroup;
        private String _dirName;
        private String _failCount;
        private String _failPct;
        private String _causedIncidentCount;
        private String _incidentPct;
        private String _incidentCount;
        private String _sev1Count;
        private String _sev1Pct;
        private String _sev1TicketCount;
        private String _successCount;
        private String _successPct;
        private String _totalCount;

        public MaintenanceResolveStatus()
        {
            // Base Constructor
        }


        [DataMember]
        public String month
        {
            get { return this._month; }
            set { this._month = value; }
        }

        [DataMember]
        public String year
        {
            get { return this._year; }
            set { this._year = value; }
        }

        [DataMember]
        public String comcastDivision
        {
            get { return this._comcastDivision; }
            set { this._comcastDivision = value; }
        }

        [DataMember]
        public String vpDepartment
        {
            get { return this._vpDepartment; }
            set { this._vpDepartment = value; }
        }


        [DataMember]
        public String vpName
        {
            get { return this._vpName; }
            set { this._vpName = value; }
        }

        [DataMember]
        public String dirGroup
        {
            get { return this._dirGroup; }
            set { this._dirGroup = value; }
        }

        [DataMember]
        public String dirName
        {
            get { return this._dirName; }
            set { this._dirName = value; }
        }

        [DataMember]
        public String failCount
        {
            get { return this._failCount; }
            set { this._failCount = value; }
        }

        [DataMember]
        public String failPct
        {
            get { return this._failPct; }
            set { this._failPct = value; }
        }

        [DataMember]
        public String causedIncidentCount
        {
            get { return this._causedIncidentCount; }
            set { this._causedIncidentCount = value; }
        }

        [DataMember]
        public String incidentPct
        {
            get { return this._incidentPct; }
            set { this._incidentPct = value; }
        }

        [DataMember]
        public String incidentCount
        {
            get { return this._incidentCount; }
            set { this._incidentCount = value; }
        }

        [DataMember]
        public String sev1Count
        {
            get { return this._sev1Count; }
            set { this._sev1Count = value; }
        }


        [DataMember]
        public String sev1Pct
        {
            get { return this._sev1Pct; }
            set { this._sev1Pct = value; }
        }

        [DataMember]
        public String sev1TicketCount
        {
            get { return this._sev1TicketCount; }
            set { this._sev1TicketCount = value; }
        }


        [DataMember]
        public String successCount
        {
            get { return this._successCount; }
            set { this._successCount = value; }
        }

        [DataMember]
        public String successPct
        {
            get { return this._successPct; }
            set { this._successPct = value; }
        }

        [DataMember]
        public String totalCount
        {
            get { return this._totalCount; }
            set { this._totalCount = value; }
        }
    }
}