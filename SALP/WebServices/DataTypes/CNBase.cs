﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace SALP.WebServices.DataTypes
{
    public class CNBase
    {
        private String _articleCount;
        private List<CNArticle> _articles = new List<CNArticle>();

        public CNBase()
        {
            // Base Constructor
        }

        [DataMember]
        public String articleCount
        {
            get { return this._articleCount; }
            set { this._articleCount = value; }
        }

        [DataMember]
        public List<CNArticle> articles
        {
            get { return this._articles; }
            set { this._articles = value; }
        }
    }
}