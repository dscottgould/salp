﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace SALP.WebServices.DataTypes
{
    public class TicketDetailsAttributes
    {
        String _contactInfoDepartment;
        String _contactInfoPhone;
        String _indicatorState;
        String _managementBridge;
        String _detectedBy;
        String _outageDescription;
        String _customerExperience;
        String _indicatorCity;
        String _technicalBridge;


        public TicketDetailsAttributes()
        {
            // Base Constructor
        }

        [DataMember]
        public String contactInfoDepartment
        {
            get { return this._contactInfoDepartment; }
            set { this._contactInfoDepartment = value; }
        }

        [DataMember]
        public String contactInfoPhone
        {
            get { return this._contactInfoPhone; }
            set { this._contactInfoPhone = value; }
        }

        [DataMember]
        public String indicatorState
        {
            get { return this._indicatorState; }
            set { this._indicatorState = value; }
        }

        [DataMember]
        public String managementBridge
        {
            get { return this._managementBridge; }
            set { this._managementBridge = value; }
        }

        [DataMember]
        public String detectedBy
        {
            get { return this._detectedBy; }
            set { this._detectedBy = value; }
        }

        [DataMember]
        public String outageDescription
        {
            get { return this._outageDescription; }
            set { this._outageDescription = value; }
        }

        [DataMember]
        public String customerExperience
        {
            get { return this._customerExperience; }
            set { this._customerExperience = value; }
        }

        [DataMember]
        public String indicatorCity
        {
            get { return this._indicatorCity; }
            set { this._indicatorCity = value; }
        }

        [DataMember]
        public String technicalBridge
        {
            get { return this._technicalBridge; }
            set { this._technicalBridge = value; }
        }
    }
}