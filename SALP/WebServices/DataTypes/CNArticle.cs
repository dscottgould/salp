﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace SALP.WebServices.DataTypes
{
    public class CNArticle
    {
        private String _title;
        private String _url;
        private String _publishedDate;
        private List<CNTag> _tags = new List<CNTag>();

        public CNArticle()
        {
            // Base Constructor
        }

        [DataMember]
        public String title
        {
            get { return this._title; }
            set { this._title = value; }
        }

        [DataMember]
        public String url
        {
            get { return this._url; }
            set { this._url = value; }
        }

        [DataMember]
        public String publishedDate
        {
            get { return this._publishedDate; }
            set { this._publishedDate = value; }
        }

        [DataMember]
        public List<CNTag> tags
        {
            get { return this._tags; }
            set { this._tags = value; }
        }
    }
}