﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace SALP.WebServices.DataTypes
{
    public class HaystackSegmentRecord
    {
        private string _name;
        private string _value;


        public HaystackSegmentRecord()
        {
               
        }


        [DataMember]
        public string name
        {
            get { return this._name ; }
            set { this._name = value; }
        }

        [DataMember]
        public string value
        {
            get { return this._value; }
            set { this._value = value; }
        }

    }
}