﻿using System;
using System.Runtime.Serialization;


namespace SALP.WebServices.DataTypes
{
    public class CRTicketsItem
    {
        String _date;
        String _cRCount;
        String _baseline;


        public CRTicketsItem()
        {
            // Base Constructor
        }

        [DataMember]
        public String date
        {
            get { return this._date; }
            set { this._date = value; }
        }

        [DataMember]
        public String cRCount
        {
            get { return this._cRCount; }
            set { this._cRCount = value; }
        }

        [DataMember]
        public String baseline
        {
            get { return this._baseline; }
            set { this._baseline = value; }
        }
    }
}