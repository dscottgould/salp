﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace SALP.WebServices.DataTypes
{
    public class TicketDetailsAffected
    {
        String _entryId;
        String _serviceAffected;
        String _elementName;
        String _elementType;
        String _systemName;
        String _telephonyAffectedSubscriptions;
        String _videoAffectedSubscriptions;
        String _hsdAffectedSubscriptions;
        String _totalAffectedSubscriptions;
        String _aeStatus;
        String _actualStart;
        String _actualEnd;

        public TicketDetailsAffected()
        {
            // Base Constructor
        }

        [DataMember]
        public String entryId
        {
            get { return this._entryId; }
            set { this._entryId = value; }
        }

        [DataMember]
        public String serviceAffected
        {
            get { return this._serviceAffected; }
            set { this._serviceAffected = value; }
        }

        [DataMember]
        public String elementName
        {
            get { return this._elementName; }
            set { this._elementName = value; }
        }

        [DataMember]
        public String elementType
        {
            get { return this._elementType; }
            set { this._elementType = value; }
        }

        [DataMember]
        public String systemName
        {
            get { return this._systemName; }
            set { this._systemName = value; }
        }

        [DataMember]
        public String telephonyAffectedSubscriptions
        {
            get { return this._telephonyAffectedSubscriptions; }
            set { this._telephonyAffectedSubscriptions = value; }
        }

        [DataMember]
        public String videoAffectedSubscriptions
        {
            get { return this._videoAffectedSubscriptions; }
            set { this._videoAffectedSubscriptions = value; }
        }

        [DataMember]
        public String hsdAffectedSubscriptions
        {
            get { return this._hsdAffectedSubscriptions; }
            set { this._hsdAffectedSubscriptions = value; }
        }

        [DataMember]
        public String totalAffectedSubscriptions
        {
            get { return this._totalAffectedSubscriptions; }
            set { this._totalAffectedSubscriptions = value; }
        }

        [DataMember]
        public String aeStatus
        {
            get { return this._aeStatus; }
            set { this._aeStatus = value; }
        }

        [DataMember]
        public String actualStart
        {
            get { return this._actualStart; }
            set { this._actualStart = value; }
        }

        [DataMember]
        public String actualEnd
        {
            get { return this._actualEnd; }
            set { this._actualEnd = value; }
        }


    }
}