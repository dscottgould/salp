﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace SALP.WebServices.DataTypes
{
    [DataContract]

    public class HaystackDivisions
    {
        private String _type;
        private String _all;
        private String _not;
        private String _undefined;
        private List<string> _value;


        public HaystackDivisions()
        {
            //Base Constructor
        }

        [DataMember]
        public String type
        {
            get { return this._type; }
            set { this._type = value; }
        }

        [DataMember]
        public String all
        {
            get { return this._all; }
            set { this._all = value; }
        }

        [DataMember]
        public String not
        {
            get { return this._not; }
            set { this._not = value; }
        }

        [DataMember]
        public String undefined
        {
            get { return this._undefined; }
            set { this._undefined = value; }
        }

        [DataMember]
        public List<string> value
        {
            get { return this._value; }
            set { this._value = value; }
        }
    }
}