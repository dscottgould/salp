﻿using System;
using System.Runtime.Serialization;

namespace SALP.WebServices.DataTypes
{
    [DataContract]

    public class CMTicketAffectedElement
    {
        private String _ticketId;
        private String _entryId;
        private String _serviceAffected;
        private String _elementName;
        private String _videoAffected;
        private String _hsdAffected;
        private String _telephonyAffected;
        private String _totalAffected;
        private String _status;
        private String _actualStart;
        private String _actualEnd;


        public CMTicketAffectedElement()
        {
            // Base Constructor
        }

        [DataMember]
        public String ticketId
        {
            get { return this._ticketId; }
            set { this._ticketId = value; }
        }

        [DataMember]
        public String entryId
        {
            get { return this._entryId; }
            set { this._entryId = value; }
        }

        [DataMember]
        public String serviceAffected
        {
            get { return this._serviceAffected; }
            set { this._serviceAffected = value; }
        }

        [DataMember]
        public String elementName
        {
            get { return this._elementName; }
            set { this._elementName = value; }
        }

        [DataMember]
        public String videoAffected
        {
            get { return this._videoAffected; }
            set { this._videoAffected = value; }
        }

        [DataMember]
        public String hsdAffected
        {
            get { return this._hsdAffected; }
            set { this._hsdAffected = value; }
        }

        [DataMember]
        public String telephonyAffected
        {
            get { return this._telephonyAffected; }
            set { this._telephonyAffected = value; }
        }

        [DataMember]
        public String totalAffected
        {
            get { return this._totalAffected; }
            set { this._totalAffected = value; }
        }

        [DataMember]
        public String status
        {
            get { return this._status; }
            set { this._status = value; }
        }

        [DataMember]
        public String actualStart
        {
            get { return this._actualStart; }
            set { this._actualStart = value; }
        }

        [DataMember]
        public String actualEnd
        {
            get { return this._actualEnd; }
            set { this._actualEnd = value; }
        }
    }
}