﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace SALP.WebServices.DataTypes
{
    [DataContract]
    public class NetworkLensIncident
    {
        String _ticketId;
        String _incidentWeek;
        String _networkLens;
        String _lensGroup;
        String _rootCauseGroup;
        String _actualStartDate;
        String _alarmStartDate;
        String _workingDate;
        String _resolvedDate;
        String _causeCategory;
        String _causeDescription;
        String _solutionCategory;
        String _solutionDescription;
        String _problemCategory;
        String _problemSummary;
        String _fixemIncidentSummary;
        String _detectedBy;
        String _source;
        String _editLensGroup;
        String _editRootCauseGroup;
        String _editIncidentSummary;
        String _editIncidentWeek;

        public NetworkLensIncident()
        {
            // Base Constructor
        }


        [DataMember]
        public String ticketId
        {
            get { return this._ticketId; }
            set { this._ticketId = value; }
        }


        [DataMember]
        public String incidentWeek
        {
            get { return this._incidentWeek; }
            set { this._incidentWeek = value; }
        }

        [DataMember]
        public String networkLens
        {
            get { return this._networkLens; }
            set { this._networkLens = value; }
        }

        [DataMember]
        public String lensGroup
        {
            get { return this._lensGroup; }
            set { this._lensGroup = value; }
        }


        [DataMember]
        public String rootCauseGroup
        {
            get { return this._rootCauseGroup; }
            set { this._rootCauseGroup = value; }
        }

        [DataMember]
        public String actualStartDate
        {
            get { return this._actualStartDate; }
            set { this._actualStartDate = value; }
        }

        [DataMember]
        public String alarmStartDate
        {
            get { return this._alarmStartDate; }
            set { this._alarmStartDate = value; }
        }

        [DataMember]
        public String workingDate
        {
            get { return this._workingDate; }
            set { this._workingDate = value; }
        }

        [DataMember]
        public String resolvedDate
        {
            get { return this._resolvedDate; }
            set { this._resolvedDate = value; }
        }

        [DataMember]
        public String causeCategory
        {
            get { return this._causeCategory; }
            set { this._causeCategory = value; }
        }

        [DataMember]
        public String causeDescription
        {
            get { return this._causeDescription; }
            set { this._causeDescription = value; }
        }

        [DataMember]
        public String solutionCategory
        {
            get { return this._solutionCategory; }
            set { this._solutionCategory = value; }
        }

        [DataMember]
        public String solutionDescription
        {
            get { return this._solutionDescription; }
            set { this._solutionDescription = value; }
        }

        [DataMember]
        public String problemCategory
        {
            get { return this._problemCategory; }
            set { this._problemCategory = value; }
        }

        [DataMember]
        public String problemSummary
        {
            get { return this._problemSummary; }
            set { this._problemSummary = value; }
        }

        [DataMember]
        public String fixemIncidentSummary
        {
            get { return this._fixemIncidentSummary; }
            set { this._fixemIncidentSummary = value; }
        }

        [DataMember]
        public String detectedBy
        {
            get { return this._detectedBy; }
            set { this._detectedBy = value; }
        }

        [DataMember]
        public String source
        {
            get { return this._source; }
            set { this._source = value; }
        }

        [DataMember]
        public String editLensGroup
        {
            get { return this._editLensGroup; }
            set { this._editLensGroup = value; }
        }

        [DataMember]
        public String editRootCauseGroup
        {
            get { return this._editRootCauseGroup; }
            set { this._editRootCauseGroup = value; }
        }

        [DataMember]
        public String editIncidentSummary
        {
            get { return this._editIncidentSummary; }
            set { this._editIncidentSummary = value; }
        }

        [DataMember]
        public String editIncidentWeek
        {
            get { return this._editIncidentWeek; }
            set { this._editIncidentWeek = value; }
        }
    }
}