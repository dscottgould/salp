﻿using System;
using System.Runtime.Serialization;

namespace SALP.WebServices.DataTypes
{
    [DataContract]

    public class CMTicketContactDetail
    {
        private String _ticketId;
        private String _createDate;
        private String _contactName;
        private String _emailAddress;
        private String _primaryPhone;
        private String _contactRole;

        public CMTicketContactDetail()
        {
            // Base Constructor
        }


        [DataMember]
        public String ticketId
        {
            get { return this._ticketId; }
            set { this._ticketId = value; }
        }

        [DataMember]
        public String createDate
        {
            get { return this._createDate; }
            set { this._createDate = value; }
        }

        [DataMember]
        public String contactName
        {
            get { return this._contactName; }
            set { this._contactName = value; }
        }

        [DataMember]
        public String emailAddress
        {
            get { return this._emailAddress; }
            set { this._emailAddress = value; }
        }

        [DataMember]
        public String primaryPhone
        {
            get { return this._primaryPhone; }
            set { this._primaryPhone = value; }
        }

        [DataMember]
        public String contactRole
        {
            get { return this._contactRole; }
            set { this._contactRole = value; }
        }
    }
}