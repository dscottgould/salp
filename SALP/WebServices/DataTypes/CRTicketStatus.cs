﻿using System;
using System.Runtime.Serialization;

namespace SALP.WebServices.DataTypes
{
    public class CRTicketStatus
    {
        String _date;
        String _currentWeek;
        String _previousWeek;


        public CRTicketStatus()
        {
            // Base Constructor
        }

        [DataMember]
        public String date
        {
            get { return this._date; }
            set { this._date = value; }
        }

        [DataMember]
        public String currentWeek
        {
            get { return this._currentWeek; }
            set { this._currentWeek = value; }
        }

        [DataMember]
        public String previousWeek
        {
            get { return this._previousWeek; }
            set { this._previousWeek = value; }
        }
    }
}