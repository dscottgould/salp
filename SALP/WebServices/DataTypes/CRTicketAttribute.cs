﻿using System;
using System.Runtime.Serialization;

namespace SALP.WebServices.DataTypes
{
    [DataContract]

    public class CRTicketAttribute
    {
        private String _ticketId;
        private String _attributeLabel;
        private String _attributeValue;

        public CRTicketAttribute()
        {
            // Base Constructor
        }

        [DataMember]
        public String ticketId
        {
            get { return this._ticketId; }
            set { this._ticketId = value; }
        }

        [DataMember]
        public String attributeLabel
        {
            get { return this._attributeLabel; }
            set { this._attributeLabel = value; }
        }

        [DataMember]
        public String attributeValue
        {
            get { return this._attributeValue; }
            set { this._attributeValue = value; }
        }
    }
}