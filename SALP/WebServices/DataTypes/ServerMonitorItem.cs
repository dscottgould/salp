﻿using System;
using System.Runtime.Serialization;


namespace SALP.WebServices.DataTypes
{
    public class ServerMonitorItem
    {
        String _name;
        String _startDate;
        String _hours;

        public ServerMonitorItem()
        {
            // Base Constructor
        }

        [DataMember]
        public String name
        {
            get { return this._name; }
            set { this._name = value; }
        }

        [DataMember]
        public String startDate
        {
            get { return this._startDate; }
            set { this._startDate = value; }
        }

        [DataMember]
        public String hours
        {
            get { return this._hours; }
            set { this._hours = value; }
        }
    }
}