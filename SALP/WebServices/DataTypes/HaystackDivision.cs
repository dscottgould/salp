﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace SALP.WebServices.DataTypes
{
    public class HaystackDivision
    {
        private String _division;

        public HaystackDivision()
        {
            // Base Constructor
        }

        [DataMember]
        public String division
        {
            get { return this._division; }
            set { this._division = value; }
        }
    }
}