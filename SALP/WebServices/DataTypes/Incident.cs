﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace SALP.WebServices.DataTypes
{
    public class Incident
    {
        String _ttsId;
        String _networkLens;
        String _lensGroup;
        String _rootCauseGroup;
        String _maintMismatch;
        String _ttsChangeRelated;
        String _fixEmChangeRelated;
        String _ttActualStart;
        String _defectTimeIssue;
        String _detect;
        String _ttAlarmStart;
        String _engageTimeIssue;
        String _engage;
        String _ttsWorking;
        String _resolvedTimeIssue;
        String _resolve;
        String _ttActualEnd;
        String _ttResolved;
        String _ttCauseDescription;
        String _ttSolutionDescription;
        String _ttProblemSummary;
        String _incidentSummary;
        String _siUserImpactCount;


        public Incident()
        {
            // Base Constructor
        }

        [DataMember]
        public String ttsId
        {
            get { return this._ttsId; }
            set { this._ttsId = value; }
        }

        [DataMember]
        public String networkLens
        {
            get { return this._networkLens; }
            set { this._networkLens = value; }
        }

        [DataMember]
        public String lensGroup
        {
            get { return this._lensGroup; }
            set { this._lensGroup = value; }
        }

        [DataMember]
        public String rootCauseGroup
        {
            get { return this._rootCauseGroup; }
            set { this._rootCauseGroup = value; }
        }

        [DataMember]
        public String maintMismatch
        {
            get { return this._maintMismatch; }
            set { this._maintMismatch = value; }
        }

        [DataMember]
        public String ttsChangeRelated
        {
            get { return this._ttsChangeRelated; }
            set { this._ttsChangeRelated = value; }
        }

        [DataMember]
        public String fixEmChangeRelated
        {
            get { return this._fixEmChangeRelated; }
            set { this._fixEmChangeRelated = value; }
        }

        [DataMember]
        public String ttActualStart
        {
            get { return this._ttActualStart; }
            set { this._ttActualStart = value; }
        }

        [DataMember]
        public String defectTimeIssue
        {
            get { return this._defectTimeIssue; }
            set { this._defectTimeIssue = value; }
        }

        [DataMember]
        public String detect
        {
            get { return this._detect; }
            set { this._detect = value; }
        }

        [DataMember]
        public String ttAlarmStart
        {
            get { return this._ttAlarmStart; }
            set { this._ttAlarmStart = value; }
        }

        [DataMember]
        public String engageTimeIssue
        {
            get { return this._engageTimeIssue; }
            set { this._engageTimeIssue = value; }
        }

        [DataMember]
        public String engage
        {
            get { return this._engage; }
            set { this._engage = value; }
        }

        [DataMember]
        public String ttsWorking
        {
            get { return this._ttsWorking; }
            set { this._ttsWorking = value; }
        }

        [DataMember]
        public String resolvedTimeIssue
        {
            get { return this._resolvedTimeIssue; }
            set { this._resolvedTimeIssue = value; }
        }

        [DataMember]
        public String resolve
        {
            get { return this._resolve; }
            set { this._resolve = value; }
        }

        [DataMember]
        public String ttActualEnd
        {
            get { return this._ttActualEnd; }
            set { this._ttActualEnd = value; }
        }

        [DataMember]
        public String ttResolved
        {
            get { return this._ttResolved; }
            set { this._ttResolved = value; }
        }

        [DataMember]
        public String ttCauseDescription
        {
            get { return this._ttCauseDescription; }
            set { this._ttCauseDescription = value; }
        }

        [DataMember]
        public String ttSolutionDescription
        {
            get { return this._ttSolutionDescription; }
            set { this._ttSolutionDescription = value; }
        }

        [DataMember]
        public String ttProblemSummary
        {
            get { return this._ttProblemSummary; }
            set { this._ttProblemSummary = value; }
        }

        [DataMember]
        public String incidentSummary
        {
            get { return this._incidentSummary; }
            set { this._incidentSummary = value; }
        }

        [DataMember]
        public String siUserImpactCount
        {
            get { return this._siUserImpactCount; }
            set { this._siUserImpactCount = value; }
        }
    }
}