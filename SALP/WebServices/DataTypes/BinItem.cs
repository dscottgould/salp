﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace SALP.WebServices.DataTypes
{
    [DataContract]

    public class BinItem
    {
        private String _eventCount;
        private String _startDate;
        private String _endDate;

        public BinItem()
        {
            // Base Constructor
        }


        [DataMember]
        public String eventCount
        {
            get { return this._eventCount; }
            set { this._eventCount = value; }
        }

        [DataMember]
        public String startDate
        {
            get { return this._startDate; }
            set { this._startDate = value; }
        }

        [DataMember]
        public String endDate
        {
            get { return this._endDate; }
            set { this._endDate = value; }
        }
    }
}