﻿using System;
using System.Runtime.Serialization;


namespace SALP.WebServices.DataTypes
{
    public class ErrorStatus
    {
        String _date;
        String _successRate;

        public ErrorStatus()
        {
            // Base Constructor
        }

        [DataMember]
        public String date
        {
            get { return this._date; }
            set { this._date = value; }
        }

        [DataMember]
        public String successRate
        {
            get { return this._successRate; }
            set { this._successRate = value; }
        }
    }
}