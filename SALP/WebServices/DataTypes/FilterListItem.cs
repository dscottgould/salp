﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace SALP.WebServices.DataTypes
{
    public class FilterListItem
    {
        private string _type;
        private Boolean _all;
        private Boolean _not;
        private Boolean _undefined;
        private List<string> _value;

        public FilterListItem()
        {
            // Base Constructor
        }

        [DataMember]
        public String type
        {
            get { return this._type; }
            set { this._type = value; }
        }

        public Boolean all
        {
            get { return this._all; }
            set { this._all = value; }
        }

        public Boolean not
        {
            get { return this._not; }
            set { this._not = value; }
        }

        public Boolean undefined
        {
            get { return this._undefined; }
            set { this._undefined = value; }
        }

        public List<string> value
        {
            get { return this._value; }
            set { this._value = value; }
        }
    }
}