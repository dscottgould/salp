﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace SALP.WebServices.DataTypes
{
    public class BrouhaRecord
    {
        private string _id;
        private string _title;
        private string _severity;
        private string _createDate;

        public BrouhaRecord()
        {
            // Base Constructor
        }

        [DataMember]
        public String id
        {
            get { return this._id; }
            set { this._id = value; }
        }

        [DataMember]
        public String title
        {
            get { return this._title; }
            set { this._title = value; }
        }

        [DataMember]
        public String severity
        {
            get { return this._severity; }
            set { this._severity = value; }
        }

        [DataMember]
        public String createDate
        {
            get { return this._createDate; }
            set { this._createDate = value; }
        }
    }
}