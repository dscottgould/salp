﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace SALP.WebServices.DataTypes
{
    [DataContract]

    public class CMDetailByHeirarchy
    {
        private String _maintenanceMonth;
        private String _maintenanceYear;
        private String _heiarchyUser;
        private String _directReport;
        private String _maintenanceImplementor;
        private String _sm;
        private String _smStatus;
        private String _smMaintenanceType;
        private String _smAuthor;
        private String _smParent;
        private String _closedBy;
        private String _ticketImplementer;
        private String _ticketId;
        private String _ticketStart;
        private String _ticketEnd;
        private String _smCreateDate;
        private String _smTimeframeStart;
        private String _smTimeframeEnd;
        private String _smClosedDate;
        private String _smMaintenanceCategory;
        private String _smMaintenanceDescription;
        private String _smMaintenanceDetail;
        private String _smRegion;
        private String _smResolutionDescription;
        private String _resolutionDescription;
        private String _resultCategory;
        private String _smResolutionDetail;
        private String _cm;
        private String _cmMaintenanceType;
        private String _cmCategory;
        private String _cmSubcategory;
        private String _cmProcedureName;
        private String _procedureId;
        private String _cmSummary;
        private String _cmDetail;
        private String _cmRisk;
        private String _cmImpact;
        private String _cmCreateDate;
        private String _cmPlannedStart;
        private String _cmPlannedEnd;
        private String _cmStatus;
        private String _cmSubmitter;
        private String _cmSubmitterName;
        private String _cmRegion;
        private String _orgUnitDescription;
        private String _orgHeirarchyLevel1Description;
        private String _orgHeirarchyLevel2Description;
        private String _sapLead;
        private String _sapDir;
        private String _sapVp;
        private String _sapVpTop;
        private String _siTicket;
        private String _relatedTicket;
        private String _siCreateDate;
        private String _siActualStart;
        private String _siActualEnd;
        private String _ticketImplementerNew;
        private String _cancelledBeforeMaintenance;
        private String _cancelledBeforeMaintenanceSm;
        private String _sucessCategory;
        private String _supportAreaName;
        private String _queueName;
        private String _siSeverity;
        private String _leadOrgUnitDescription;
        private String _dirOrgUnitDescription;
        private String _vpOrgUnitDescription;
        private String _leadOrgHierarchyLevel1Description;
        private String _dirOrgHierarchyLevel1Description;
        private String _vpOrgHierarchyLevel1Description;
        private String _vpOrgHierarchyLevel2Description;
        private String _ts;




        public CMDetailByHeirarchy()
        {
            // Base Constructor
        }

        [DataMember]
        public String maintenanceMonth
        {
            get { return this._maintenanceMonth; }
            set { this._maintenanceMonth = value; }
        }

        [DataMember]
        public String maintenanceYear
        {
            get { return this._maintenanceYear; }
            set { this._maintenanceYear = value; }
        }

        [DataMember]
        public String heiarchyUser
        {
            get { return this._heiarchyUser; }
            set { this._heiarchyUser = value; }
        }

        [DataMember]
        public String directReport
        {
            get { return this._directReport; }
            set { this._directReport = value; }
        }

        [DataMember]
        public String maintenanceImplementor
        {
            get { return this._maintenanceImplementor; }
            set { this._maintenanceImplementor = value; }
        }

        [DataMember]
        public String sm
        {
            get { return this._sm; }
            set { this._sm = value; }
        }

        [DataMember]
        public String smStatus
        {
            get { return this._smStatus; }
            set { this._smStatus = value; }
        }

        [DataMember]
        public String smMaintenanceType
        {
            get { return this._smMaintenanceType; }
            set { this._smMaintenanceType = value; }
        }

        [DataMember]
        public String smAuthor
        {
            get { return this._smAuthor; }
            set { this._smAuthor = value; }
        }

        [DataMember]
        public String smParent
        {
            get { return this._smParent; }
            set { this._smParent = value; }
        }

        [DataMember]
        public String closedBy
        {
            get { return this._closedBy; }
            set { this._closedBy = value; }
        }

        [DataMember]
        public String ticketImplementer
        {
            get { return this._ticketImplementer; }
            set { this._ticketImplementer = value; }
        }

        [DataMember]
        public String ticketId
        {
            get { return this._ticketId; }
            set { this._ticketId = value; }
        }

        [DataMember]
        public String ticketStart
        {
            get { return this._ticketStart; }
            set { this._ticketStart = value; }
        }

        [DataMember]
        public String ticketEnd
        {
            get { return this._ticketEnd; }
            set { this._ticketEnd = value; }
        }

        [DataMember]
        public String smCreateDate
        {
            get { return this._smCreateDate; }
            set { this._smCreateDate = value; }
        }

        [DataMember]
        public String smTimeframeStart
        {
            get { return this._smTimeframeStart; }
            set { this._smTimeframeStart = value; }
        }

        [DataMember]
        public String smTimeframeEnd
        {
            get { return this._smTimeframeEnd; }
            set { this._smTimeframeEnd = value; }
        }

        [DataMember]
        public String smClosedDate
        {
            get { return this._smClosedDate; }
            set { this._smClosedDate = value; }
        }

        [DataMember]
        public String smMaintenanceCategory
        {
            get { return this._smMaintenanceCategory; }
            set { this._smMaintenanceCategory = value; }
        }

        [DataMember]
        public String smMaintenanceDescription
        {
            get { return this._smMaintenanceDescription; }
            set { this._smMaintenanceDescription = value; }
        }

        [DataMember]
        public String smMaintenanceDetail
        {
            get { return this._smMaintenanceDetail; }
            set { this._smMaintenanceDetail = value; }
        }

        [DataMember]
        public String smRegion
        {
            get { return this._smRegion; }
            set { this._smRegion = value; }
        }

        [DataMember]
        public String smResolutionDescription
        {
            get { return this._smResolutionDescription; }
            set { this._smResolutionDescription = value; }
        }

        [DataMember]
        public String resolutionDescription
        {
            get { return this._resolutionDescription; }
            set { this._resolutionDescription = value; }
        }

        [DataMember]
        public String resultCategory
        {
            get { return this._resultCategory; }
            set { this._resultCategory = value; }
        }

        [DataMember]
        public String smResolutionDetail
        {
            get { return this._smResolutionDetail; }
            set { this._smResolutionDetail = value; }
        }

        [DataMember]
        public String cm
        {
            get { return this._cm; }
            set { this._cm = value; }
        }

        [DataMember]
        public String cmMaintenanceType
        {
            get { return this._cmMaintenanceType; }
            set { this._cmMaintenanceType = value; }
        }

        [DataMember]
        public String cmCategory
        {
            get { return this._cmCategory; }
            set { this._cmCategory = value; }
        }

        [DataMember]
        public String cmSubcategory
        {
            get { return this._cmSubcategory; }
            set { this._cmSubcategory = value; }
        }

        [DataMember]
        public String cmProcedureName
        {
            get { return this._cmProcedureName; }
            set { this._cmProcedureName = value; }
        }

        [DataMember]
        public String procedureId
        {
            get { return this._procedureId; }
            set { this._procedureId = value; }
        }

        [DataMember]
        public String cmSummary
        {
            get { return this._cmSummary; }
            set { this._cmSummary = value; }
        }

        [DataMember]
        public String cmDetail
        {
            get { return this._cmDetail; }
            set { this._cmDetail = value; }
        }

        [DataMember]
        public String cmRisk
        {
            get { return this._cmRisk; }
            set { this._cmRisk = value; }
        }

        [DataMember]
        public String cmImpact
        {
            get { return this._cmImpact; }
            set { this._cmImpact = value; }
        }

        [DataMember]
        public String cmCreateDate
        {
            get { return this._cmCreateDate; }
            set { this._cmCreateDate = value; }
        }

        [DataMember]
        public String cmPlannedStart
        {
            get { return this._cmPlannedStart; }
            set { this._cmPlannedStart = value; }
        }

        [DataMember]
        public String cmPlannedEnd
        {
            get { return this._cmPlannedEnd; }
            set { this._cmPlannedEnd = value; }
        }

        [DataMember]
        public String cmStatus
        {
            get { return this._cmStatus; }
            set { this._cmStatus = value; }
        }

        [DataMember]
        public String cmSubmitter
        {
            get { return this._cmSubmitter; }
            set { this._cmSubmitter = value; }
        }

        [DataMember]
        public String cmSubmitterName
        {
            get { return this._cmSubmitterName; }
            set { this._cmSubmitterName = value; }
        }

        [DataMember]
        public String cmRegion
        {
            get { return this._cmRegion; }
            set { this._cmRegion = value; }
        }

        [DataMember]
        public String orgUnitDescription
        {
            get { return this._orgUnitDescription; }
            set { this._orgUnitDescription = value; }
        }

        [DataMember]
        public String orgHeirarchyLevel1Description
        {
            get { return this._orgHeirarchyLevel1Description; }
            set { this._orgHeirarchyLevel1Description = value; }
        }

        [DataMember]
        public String orgHeirarchyLevel2Description
        {
            get { return this._orgHeirarchyLevel2Description; }
            set { this._orgHeirarchyLevel2Description = value; }
        }

        [DataMember]
        public String sapLead
        {
            get { return this._sapLead; }
            set { this._sapLead = value; }
        }

        [DataMember]
        public String sapDir
        {
            get { return this._sapDir; }
            set { this._sapDir = value; }
        }

        [DataMember]
        public String sapVp
        {
            get { return this._sapVp; }
            set { this._sapVp = value; }
        }

        [DataMember]
        public String sapVpTop
        {
            get { return this._sapVpTop; }
            set { this._sapVpTop = value; }
        }

        [DataMember]
        public String siTicket
        {
            get { return this._siTicket; }
            set { this._siTicket = value; }
        }

        [DataMember]
        public String relatedTicket
        {
            get { return this._relatedTicket; }
            set { this._relatedTicket = value; }
        }

        [DataMember]
        public String siCreateDate
        {
            get { return this._siCreateDate; }
            set { this._siCreateDate = value; }
        }

        [DataMember]
        public String siActualStart
        {
            get { return this._siActualStart; }
            set { this._siActualStart = value; }
        }

        [DataMember]
        public String siActualEnd
        {
            get { return this._siActualEnd; }
            set { this._siActualEnd = value; }
        }

        [DataMember]
        public String ticketImplementerNew
        {
            get { return this._ticketImplementerNew; }
            set { this._ticketImplementerNew = value; }
        }

        [DataMember]
        public String cancelledBeforeMaintenance
        {
            get { return this._cancelledBeforeMaintenance; }
            set { this._cancelledBeforeMaintenance = value; }
        }

        [DataMember]
        public String cancelledBeforeMaintenanceSm
        {
            get { return this._cancelledBeforeMaintenanceSm; }
            set { this._cancelledBeforeMaintenanceSm = value; }
        }

        [DataMember]
        public String sucessCategory
        {
            get { return this._sucessCategory; }
            set { this._sucessCategory = value; }
        }

        [DataMember]
        public String supportAreaName
        {
            get { return this._supportAreaName; }
            set { this._supportAreaName = value; }
        }

        [DataMember]
        public String queueName
        {
            get { return this._queueName; }
            set { this._queueName = value; }
        }

        [DataMember]
        public String siSeverity
        {
            get { return this._siSeverity; }
            set { this._siSeverity = value; }
        }

        [DataMember]
        public String leadOrgUnitDescription
        {
            get { return this._leadOrgUnitDescription; }
            set { this._leadOrgUnitDescription = value; }
        }

        [DataMember]
        public String dirOrgUnitDescription
        {
            get { return this._dirOrgUnitDescription; }
            set { this._dirOrgUnitDescription = value; }
        }

        [DataMember]
        public String vpOrgUnitDescription
        {
            get { return this._vpOrgUnitDescription; }
            set { this._vpOrgUnitDescription = value; }
        }

        [DataMember]
        public String leadOrgHierarchyLevel1Description
        {
            get { return this._leadOrgHierarchyLevel1Description; }
            set { this._leadOrgHierarchyLevel1Description = value; }
        }

        [DataMember]
        public String dirOrgHierarchyLevel1Description
        {
            get { return this._dirOrgHierarchyLevel1Description; }
            set { this._dirOrgHierarchyLevel1Description = value; }
        }

        [DataMember]
        public String vpOrgHierarchyLevel1Description
        {
            get { return this._vpOrgHierarchyLevel1Description; }
            set { this._vpOrgHierarchyLevel1Description = value; }
        }

        [DataMember]
        public String vpOrgHierarchyLevel2Description
        {
            get { return this._vpOrgHierarchyLevel2Description; }
            set { this._vpOrgHierarchyLevel2Description = value; }
        }

        [DataMember]
        public String ts
        {
            get { return this._ts; }
            set { this._ts = value; }
        }
    }
}