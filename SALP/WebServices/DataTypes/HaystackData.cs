﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace SALP.WebServices.DataTypes
{
    public class HaystackData
    {
        private List<HaystackRecord> _bins;

        public HaystackData()
        {
            // Constructor
        }

        [DataMember]
        public List<HaystackRecord> bins
        {
            get { return this._bins; }
            set { this._bins = value; }
        }
    }
}