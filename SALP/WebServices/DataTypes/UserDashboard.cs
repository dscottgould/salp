﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace SALP.WebServices.DataTypes
{
    [DataContract]
    public class UserDashboard
    {
        String _dashboardId;
        String _dashboardName;
        String _cookieValue;

        public UserDashboard()
        {
            // Base Constructor
        }

        [DataMember]
        public String dashboardId
        {
            get { return this._dashboardId; }
            set { this._dashboardId = value; }
        }

        [DataMember]
        public String dashboardName
        {
            get { return this._dashboardName; }
            set { this._dashboardName = value; }
        }

        [DataMember]
        public String cookieValue
        {
            get { return this._cookieValue; }
            set { this._cookieValue = value; }
        }

    }
}