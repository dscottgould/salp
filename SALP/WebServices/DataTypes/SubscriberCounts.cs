﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SALP.WebServices.DataTypes
{
    public class SubscriberCounts
    {
        private string _productService;
        private string _countDate;
        private string _subCount;
        private string _comp;
        

        public SubscriberCounts()
        {
            // Constructor
        }

        public String productService
        {
            get { return this._productService; }
            set { this._productService = value; }
        }

        public String countDate
        {
            get { return this._countDate; }
            set { this._countDate = value; }
        }

        public String subCount
        {
            get { return this._subCount; }
            set { this._subCount = value; }
        }

        public String comp
        {
            get { return this._comp; }
            set { this._comp = value; }
        }
    }
}