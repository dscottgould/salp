﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace SALP.WebServices.DataTypes
{
    [DataContract]

    public class StackedBinsList
    {
        private String _name;
        private List<BinItem> _bins;

        public StackedBinsList()
        {
            // Base Constructor
        }

        [DataMember]
        public String name
        {
            get { return this._name; }
            set { this._name = value; }
        }

        [DataMember]
        public List<BinItem> bins
        {
            get { return this._bins; }
            set { this._bins = value; }
        }
    }
}