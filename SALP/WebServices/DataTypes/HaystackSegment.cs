﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace SALP.WebServices.DataTypes
{
    public class HaystackSegment
    {
        private string _type;
        private string _all;
        private string _undefined;
        private List<string> _value;

        public HaystackSegment()
        {
            // constructor
        }


        [DataMember]
        public string type
        {
            get { return this._type; }
            set { this._type = value; }
        }
        [DataMember]
        public string all
        {
            get { return this._all; }
            set { this._all = value; }
        }
        [DataMember]
        public string undefined
        {
            get { return this._undefined; }
            set { this._undefined = value; }
        }

        [DataMember]
        public List<string> value
        {
            get { return this._value; }
            set { this._value = value; }
        }

    }
}