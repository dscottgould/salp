﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace SALP.WebServices.DataTypes
{
    public class HaystackRecord
    {
        private String[] _eventCountBaselines;
        private string _eventCount;
        private string _startDate;
        private string _endDate;

        public HaystackRecord()
        {
            //Base Constructor
        }

        [DataMember]
        public String[] eventCountBaselines
        {
            get { return this._eventCountBaselines; }
            set { this._eventCountBaselines = value; }
        }

        [DataMember]
        public String eventCount
        {
            get { return this._eventCount; }
            set { this._eventCount = value; }
        }

        [DataMember]
        public String startDate
        {
            get { return this._startDate; }
            set { this._startDate = value; }
        }

        [DataMember]
        public String endDate
        {
            get { return this._endDate; }
            set { this._endDate = value; }
        }

    }


}