﻿using System;
using System.Runtime.Serialization;


namespace SALP.WebServices.DataTypes
{
    public class URLMonitorItem
    {
        String _date;
        String _bnocTools;
        String _etsTools;
        String _rcor;

        public URLMonitorItem()
        {
            // Base Constructor
        }

        [DataMember]
        public String date
        {
            get { return this._date; }
            set { this._date = value; }
        }

        [DataMember]
        public String bnocTools
        {
            get { return this._bnocTools; }
            set { this._bnocTools = value; }
        }

        [DataMember]
        public String etsTools
        {
            get { return this._etsTools; }
            set { this._etsTools = value; }
        }

        [DataMember]
        public String rcor
        {
            get { return this._rcor; }
            set { this._rcor = value; }
        }
    }
}