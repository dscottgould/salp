﻿using System;
using System.Runtime.Serialization;

namespace SALP.WebServices.DataTypes
{
    [DataContract]

    public class OETicketAffectedElement
    {
        private String _ticketId;
        private String _entryId;
        private String _serviceAffected;
        private String _elementName;
        private String _telephonyAffected;
        private String _hsdAffected;
        private String _videoAffected;
        private String _totalAffected;
        private String _aeStatus;
        private String _elementType;
        private String _plannedFlag;
        private String _systemName;
        private String _messageInterval;
        private String _causeCode;
        private String _causeDescription;
        private String _solutionCode;
        private String _solutionDescription;
        private String _originatingTicket;
        private String _submitter;
        private String _lastModifiedBy;
        private String _actualStart;
        private String _actualEnd;

        public OETicketAffectedElement()
        {
            // Base Constutor
        }

        [DataMember]
        public String ticketId
        {
            get { return this._ticketId; }
            set { this._ticketId = value; }
        }

        [DataMember]
        public String entryId
        {
            get { return this._entryId; }
            set { this._entryId = value; }
        }

        [DataMember]
        public String serviceAffected
        {
            get { return this._serviceAffected; }
            set { this._serviceAffected = value; }
        }

        [DataMember]
        public String elementName
        {
            get { return this._elementName; }
            set { this._elementName = value; }
        }

        [DataMember]
        public String telephonyAffected
        {
            get { return this._telephonyAffected; }
            set { this._telephonyAffected = value; }
        }

        [DataMember]
        public String hsdAffected
        {
            get { return this._hsdAffected; }
            set { this._hsdAffected = value; }
        }

        [DataMember]
        public String videoAffected
        {
            get { return this._videoAffected; }
            set { this._videoAffected = value; }
        }

        [DataMember]
        public String totalAffected
        {
            get { return this._totalAffected; }
            set { this._totalAffected = value; }
        }

        [DataMember]
        public String aeStatus
        {
            get { return this._aeStatus; }
            set { this._aeStatus = value; }
        }

        [DataMember]
        public String elementType
        {
            get { return this._elementType; }
            set { this._elementType = value; }
        }

        [DataMember]
        public String plannedFlag
        {
            get { return this._plannedFlag; }
            set { this._plannedFlag = value; }
        }

        [DataMember]
        public String systemName
        {
            get { return this._systemName; }
            set { this._systemName = value; }
        }

        [DataMember]
        public String messageInterval
        {
            get { return this._messageInterval; }
            set { this._messageInterval = value; }
        }

        [DataMember]
        public String causeCode
        {
            get { return this._causeCode; }
            set { this._causeCode = value; }
        }

        [DataMember]
        public String causeDescription
        {
            get { return this._causeDescription; }
            set { this._causeDescription = value; }
        }

        [DataMember]
        public String solutionCode
        {
            get { return this._solutionCode; }
            set { this._solutionCode = value; }
        }

        [DataMember]
        public String solutionDescription
        {
            get { return this._solutionDescription; }
            set { this._solutionDescription = value; }
        }

        [DataMember]
        public String originatingTicket
        {
            get { return this._originatingTicket; }
            set { this._originatingTicket = value; }
        }

        [DataMember]
        public String submitter
        {
            get { return this._submitter; }
            set { this._submitter = value; }
        }

        [DataMember]
        public String lastModifiedBy
        {
            get { return this._lastModifiedBy; }
            set { this._lastModifiedBy = value; }
        }

        [DataMember]
        public String actualStart
        {
            get { return this._actualStart; }
            set { this._actualStart = value; }
        }

        [DataMember]
        public String actualEnd
        {
            get { return this._actualEnd; }
            set { this._actualEnd = value; }
        }
    }
}