﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;


namespace SALP.WebServices.DataTypes
{
    [DataContract]
    public class HaystackBreakDownCall
    {
        private String _eventDate;
        private List<String> _fields;
        private List<String> _fieldNames;
        private String _data1;
        private String _data2;
        private String _data3;
        private String _data4;
        private String _data5;
        private String _data6;
        private String _data7;
        private String _data8;
        private String _data9;
        private String _data10;
        private String _data11;

        public HaystackBreakDownCall()
        {
            // Base Constructor
        }

        [DataMember]
        public String eventDate
        {
            get { return this._eventDate; }
            set { this._eventDate = value; }
        }

        [DataMember]
        public List<String> fields
        {
            get { return this._fields; }
            set { this._fields = value; }
        }

        [DataMember]
        public List<String> fieldNames
        {
            get { return this._fieldNames; }
            set { this._fieldNames = value; }
        }

        [DataMember]
        public String data1
        {
            get { return this._data1; }
            set { this._data1 = value; }
        }

        [DataMember]
        public String data2
        {
            get { return this._data2; }
            set { this._data2 = value; }
        }

        [DataMember]
        public String data3
        {
            get { return this._data3; }
            set { this._data3 = value; }
        }

        [DataMember]
        public String data4
        {
            get { return this._data4; }
            set { this._data4 = value; }
        }

        [DataMember]
        public String data5
        {
            get { return this._data5; }
            set { this._data5 = value; }
        }

        [DataMember]
        public String data6
        {
            get { return this._data6; }
            set { this._data6 = value; }
        }

        [DataMember]
        public String data7
        {
            get { return this._data7; }
            set { this._data7 = value; }
        }

        [DataMember]
        public String data8
        {
            get { return this._data8; }
            set { this._data8 = value; }
        }

        [DataMember]
        public String data9
        {
            get { return this._data9; }
            set { this._data9 = value; }
        }

        [DataMember]
        public String data10
        {
            get { return this._data10; }
            set { this._data10 = value; }
        }

        [DataMember]
        public String data11
        {
            get { return this._data11; }
            set { this._data11 = value; }
        }
    }
}