﻿using System;
using System.Runtime.Serialization;


namespace SALP.WebServices.DataTypes
{
    [DataContract]

    public class Sev1CausedByMaintDetail
    {
        private String _maintTicketId;
        private String _cmSummary;
        private String _resolutionDescription;
        private String _ttsId;
        private String _rcaTitle;
        private String _actualStart;
        private String _actualEnd;
        private String _causeDescription;
        private String _solutionDescription;
        private String _problemDescription;
        private String _queueName;
        private String _selfInflicted;
        private String _execSummary;
        private String _customerImpact;
        private String _careImpact;
        private String _platform;
        private String _rootCause;
        private String _responsibleParty;
        private String _detect;
        private String _redundacy;
        private String _responsiblePerson;


        public Sev1CausedByMaintDetail()
        {
            // Base Constructor
        }

        [DataMember]
        public String maintTicketId
        {
            get { return this._maintTicketId; }
            set { this._maintTicketId = value; }
        }

        [DataMember]
        public String cmSummary
        {
            get { return this._cmSummary; }
            set { this._cmSummary = value; }
        }

        [DataMember]
        public String resolutionDescription
        {
            get { return this._resolutionDescription; }
            set { this._resolutionDescription = value; }
        }

        [DataMember]
        public String ttsId
        {
            get { return this._ttsId; }
            set { this._ttsId = value; }
        }

        [DataMember]
        public String rcaTitle
        {
            get { return this._rcaTitle; }
            set { this._rcaTitle = value; }
        }

        [DataMember]
        public String actualStart
        {
            get { return this._actualStart; }
            set { this._actualStart = value; }
        }

        [DataMember]
        public String actualEnd
        {
            get { return this._actualEnd; }
            set { this._actualEnd = value; }
        }

        [DataMember]
        public String causeDescription
        {
            get { return this._causeDescription; }
            set { this._causeDescription = value; }
        }

        [DataMember]
        public String solutionDescription
        {
            get { return this._solutionDescription; }
            set { this._solutionDescription = value; }
        }

        [DataMember]
        public String problemDescription
        {
            get { return this._problemDescription; }
            set { this._problemDescription = value; }
        }

        [DataMember]
        public String queueName
        {
            get { return this._queueName; }
            set { this._queueName = value; }
        }

        [DataMember]
        public String selfInflicted
        {
            get { return this._selfInflicted; }
            set { this._selfInflicted = value; }
        }

        [DataMember]
        public String execSummary
        {
            get { return this._execSummary; }
            set { this._execSummary = value; }
        }

        [DataMember]
        public String customerImpact
        {
            get { return this._customerImpact; }
            set { this._customerImpact = value; }
        }

        [DataMember]
        public String careImpact
        {
            get { return this._careImpact; }
            set { this._careImpact = value; }
        }

        [DataMember]
        public String platform
        {
            get { return this._platform; }
            set { this._platform = value; }
        }

        [DataMember]
        public String rootCause
        {
            get { return this._rootCause; }
            set { this._rootCause = value; }
        }

        [DataMember]
        public String responsibleParty
        {
            get { return this._responsibleParty; }
            set { this._responsibleParty = value; }
        }

        [DataMember]
        public String detect
        {
            get { return this._detect; }
            set { this._detect = value; }
        }

        [DataMember]
        public String redundacy
        {
            get { return this._redundacy; }
            set { this._redundacy = value; }
        }

        [DataMember]
        public String responsiblePerson
        {
            get { return this._responsiblePerson; }
            set { this._responsiblePerson = value; }
        }
    }
}