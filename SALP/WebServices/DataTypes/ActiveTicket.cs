﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace SALP.WebServices.DataTypes
{
    [DataContract]

    public class ActiveTicket
    {
        private String _ticketId;
        private String _incidentSummary;
        private String _nextAction;
        private String _serviceCondition;
        private String _severity;
        private String _createDateMt;
        private String _supportAreaName;
        private String _regionName;
        private String _queueName;


        public ActiveTicket()
        {
            // Base Constructor
        }

        [DataMember]
        public String ticketId
        {
            get { return this._ticketId; }
            set { this._ticketId = value; }
        }

        [DataMember]
        public String incidentSummary
        {
            get { return this._incidentSummary; }
            set { this._incidentSummary = value; }
        }

        [DataMember]
        public String nextAction
        {
            get { return this._nextAction; }
            set { this._nextAction = value; }
        }

        [DataMember]
        public String serviceCondition
        {
            get { return this._serviceCondition; }
            set { this._serviceCondition = value; }
        }

        [DataMember]
        public String severity
        {
            get { return this._severity; }
            set { this._severity = value; }
        }

        [DataMember]
        public String createDateMt
        {
            get { return this._createDateMt; }
            set { this._createDateMt = value; }
        }

        [DataMember]
        public String supportAreaName
        {
            get { return this._supportAreaName; }
            set { this._supportAreaName = value; }
        }

        [DataMember]
        public String regionName
        {
            get { return this._regionName; }
            set { this._regionName = value; }
        }

        [DataMember]
        public String queueName
        {
            get { return this._queueName; }
            set { this._queueName = value; }
        }
    }
}