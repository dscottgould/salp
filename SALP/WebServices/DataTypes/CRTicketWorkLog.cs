﻿using System;
using System.Runtime.Serialization;

namespace SALP.WebServices.DataTypes
{
    [DataContract]

    public class CRTicketWorkLog
    {
        private String _ticketId;
        private String _createDate;
        private String _submitter;
        private String _source;
        private String _subject;
        private String _details;

        public CRTicketWorkLog()
        {

        }

        [DataMember]
        public String ticketId
        {
            get { return this._ticketId; }
            set { this._ticketId = value; }
        }

        [DataMember]
        public String createDate
        {
            get { return this._createDate; }
            set { this._createDate = value; }
        }

        [DataMember]
        public String submitter
        {
            get { return this._submitter; }
            set { this._submitter = value; }
        }

        [DataMember]
        public String source
        {
            get { return this._source; }
            set { this._source = value; }
        }

        [DataMember]
        public String subject
        {
            get { return this._subject; }
            set { this._subject = value; }
        }

        [DataMember]
        public String details
        {
            get { return this._details; }
            set { this._details = value; }
        }
    }
}