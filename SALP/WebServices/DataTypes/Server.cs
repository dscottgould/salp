﻿using System;
using System.Runtime.Serialization;


namespace SALP.WebServices.DataTypes
{
    public class Server
    {
        String _id;
        String _serverName;
        String _memoryUsage;
        String _cpuUsage;
        String _hasMonitoredProcesses;
        String _hasLongRunningJobs;

        public Server()
        {
            // Base Constructor
        }


        [DataMember]
        public String id
        {
            get { return this._id; }
            set { this._id = value; }
        }

        [DataMember]
        public String serverName
        {
            get { return this._serverName; }
            set { this._serverName = value; }
        }


        [DataMember]
        public String memoryUsage
        {
            get { return this._memoryUsage; }
            set { this._memoryUsage = value; }
        }


        [DataMember]
        public String cpuUsage
        {
            get { return this._cpuUsage; }
            set { this._cpuUsage = value; }
        }
        [DataMember]
        public String hasMonitoredProcesses
        {
            get { return this._hasMonitoredProcesses; }
            set { this._hasMonitoredProcesses = value; }
        }

        [DataMember]
        public String hasLongRunningJobs
        {
            get { return this._hasLongRunningJobs; }
            set { this._hasLongRunningJobs = value; }
        }
    }
}