﻿using System;
using System.Runtime.Serialization;

namespace SALP.WebServices.DataTypes
{
    public class FCCThresholdBreachedItem
    {
        String _ticketId;
        String _monitor;
        String _threshhold;
        String _thStatus;
        String _monitorDiscovery;
        String _ces;
        String _email;
        String _rcorStatus;
        String _rcorDate;
        String _workedBy;

        public FCCThresholdBreachedItem()
        {
            // Base Constructor
        }


        [DataMember]
        public String ticketId
        {
            get { return this._ticketId; }
            set { this._ticketId = value; }
        }


        [DataMember]
        public String monitor
        {
            get { return this._monitor; }
            set { this._monitor = value; }
        }


        [DataMember]
        public String threshhold
        {
            get { return this._threshhold; }
            set { this._threshhold = value; }
        }


        [DataMember]
        public String thStatus
        {
            get { return this._thStatus; }
            set { this._thStatus = value; }
        }


        [DataMember]
        public String monitorDiscovery
        {
            get { return this._monitorDiscovery; }
            set { this._monitorDiscovery = value; }
        }


        [DataMember]
        public String ces
        {
            get { return this._ces; }
            set { this._ces = value; }
        }

        [DataMember]
        public String email
        {
            get { return this._email; }
            set { this._email = value; }
        }


        [DataMember]
        public String rcorStatus
        {
            get { return this._rcorStatus; }
            set { this._rcorStatus = value; }
        }


        [DataMember]
        public String rcorDate
        {
            get { return this._rcorDate; }
            set { this._rcorDate = value; }
        }


        [DataMember]
        public String workedBy
        {
            get { return this._workedBy; }
            set { this._workedBy = value; }
        }
    }
}