﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace SALP.WebServices.DataTypes
{
    public class WorkLogTicket
    {
        String _ticketNumber;
        String _createDate;
        String _submitter;
        String _source;
        String _subject;
        String _details;

        public WorkLogTicket()
        {
            // Base Constructor
        }



        [DataMember]
        public String ticketNumber
        {
            get { return this._ticketNumber; }
            set { this._ticketNumber = value; }
        }

        [DataMember]
        public String createDate
        {
            get { return this._createDate; }
            set { this._createDate = value; }
        }

        [DataMember]
        public String submitter
        {
            get { return this._submitter; }
            set { this._submitter = value; }
        }

        [DataMember]
        public String source
        {
            get { return this._source; }
            set { this._source = value; }
        }

        [DataMember]
        public String subject
        {
            get { return this._subject; }
            set { this._subject = value; }
        }

        [DataMember]
        public String details
        {
            get { return this._details; }
            set { this._details = value; }
        }
    }
}