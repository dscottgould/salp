﻿using System;
using System.Runtime.Serialization;


namespace SALP.WebServices.DataTypes
{
    public class IVRCDRCall
    {
        String _date;
        String _callCount;
        String _baseline;

        public IVRCDRCall()
        {
            // Base Constuctor
        }

        [DataMember]
        public String date
        {
            get { return this._date; }
            set { this._date = value; }
        }

        [DataMember]
        public String callCount
        {
            get { return this._callCount; }
            set { this._callCount = value; }
        }

        [DataMember]
        public String baseline
        {
            get { return this._baseline; }
            set { this._baseline = value; }
        }

    }
}