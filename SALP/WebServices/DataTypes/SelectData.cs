﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace SALP.WebServices.DataTypes
{
    [DataContract]
    public class SelectData
    {
        public String _displayName;
        public String _className;

        public SelectData()
        {
            // Base Constructor
        }

        [DataMember]
        public String displayName
        {
            get { return this._displayName; }
            set { this._displayName = value; }
        }

        [DataMember]
        public String className
        {
            get { return this._className; }
            set { this._className = value; }
        }

    }
}