﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace SALP.WebServices.DataTypes
{
    public class HaystackFilters
    {
        private List<string> _UserFilter;

        public HaystackFilters()
        {
            // Base Constructor
        }

        [DataMember]
        public List<string> UserFilter
        {
            get { return this._UserFilter; }
            set { this._UserFilter = value; }
        }

    }
}