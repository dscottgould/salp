﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace SALP.WebServices.DataTypes
{
    public class ConflictTicket
    {
        String _submitterName;
        String _supervisorName;
        String _bypassed;
        String _notBypassed;
        String _total;
        String _percentNotBypassed;


        public ConflictTicket()
        {
            // Base Constuctor
        }

        [DataMember]
        public String submitterName
        {
            get { return this._submitterName; }
            set { this._submitterName = value; }
        }


        [DataMember]
        public String supervisorName
        {
            get { return this._supervisorName; }
            set { this._supervisorName = value; }
        }

        [DataMember]
        public String bypassed
        {
            get { return this._bypassed; }
            set { this._bypassed = value; }
        }


        [DataMember]
        public String notBypassed
        {
            get { return this._notBypassed; }
            set { this._notBypassed = value; }
        }


        [DataMember]
        public String total
        {
            get { return this._total; }
            set { this._total = value; }
        }

        [DataMember]
        public String percentNotBypassed
        {
            get { return this._percentNotBypassed; }
            set { this._percentNotBypassed = value; }
        }
    }
}