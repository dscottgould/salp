﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Runtime.Serialization;

namespace SALP.WebServices.DataTypes
{
    public class HSDRecord
    {
        private string _date;
        private string _apps;
        private string _connect;
        private string _cpe;
        private string _degradedConectivity;
        private string _generalInquiry;
        private string _homeNetwork;
        private string _noConectivity;
        private string _callCount;


        public HSDRecord()
        {
            //Base Constructor
        }

        public HSDRecord(DataRow record)
        {
            //this._date = record["Product_Service"].ToString();
            //this._apps = record["Create_Date"].ToString();
            //this._cpe = record["Trouble_Type"].ToString();
            //this._degradedConectivity = record["Ticket_Count"].ToString();
            //this._generalInquiry = record["Color"].ToString();
            //this._homeNetwork = record["Sort_Order"].ToString();
            //this._noConectivity = record["Call_Count"].ToString();     
        }


        [DataMember]
        public String date
        {
            get { return this._date; }
            set { this._date = value; }
        }

        [DataMember]
        public String apps
        {
            get { return this._apps; }
            set { this._apps = value; }
        }

        [DataMember]
        public String connect
        {
            get { return this._connect; }
            set { this._connect = value; }
        }

        [DataMember]
        public String cpe
        {
            get { return this._cpe; }
            set { this._cpe = value; }
        }

        [DataMember]
        public String degradedConectivity
        {
            get { return this._degradedConectivity; }
            set { this._degradedConectivity = value; }
        }

        [DataMember]
        public String generalInquiry
        {
            get { return this._generalInquiry; }
            set { this._generalInquiry = value; }
        }

        [DataMember]
        public String homeNetwork
        {
            get { return this._homeNetwork; }
            set { this._homeNetwork = value; }
        }

        [DataMember]
        public String noConectivity
        {
            get { return this._noConectivity; }
            set { this._noConectivity = value; }
        }

        [DataMember]
        public String callCount
        {
            get { return this._callCount; }
            set { this._callCount = value; }
        }
    }
}