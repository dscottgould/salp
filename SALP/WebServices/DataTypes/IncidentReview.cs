﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace SALP.WebServices.DataTypes
{
    public class IncidentReview
    {
        String _label;
        String _mtbf;
        String _frequency;
        String _wowFrequency;
        String _changeRelated;
        String _detect;
        String _engage;
        String _resolve;
        String _wowResolve;
        String _config;
        String _hwSw;
        String _asDesign;
        String _executionDefect;
        String _impact;

        public IncidentReview()
        {
            // Base Constructor
        }

        [DataMember]
        public String label
        {
            get { return this._label; }
            set { this._label = value; }
        }

        [DataMember]
        public String mtbf
        {
            get { return this._mtbf; }
            set { this._mtbf = value; }
        }

        [DataMember]
        public String frequency
        {
            get { return this._frequency; }
            set { this._frequency = value; }
        }

        [DataMember]
        public String wowFrequency
        {
            get { return this._wowFrequency; }
            set { this._wowFrequency = value; }
        }

        [DataMember]
        public String changeRelated
        {
            get { return this._changeRelated; }
            set { this._changeRelated = value; }
        }

        [DataMember]
        public String detect
        {
            get { return this._detect; }
            set { this._detect = value; }
        }

        [DataMember]
        public String engage
        {
            get { return this._engage; }
            set { this._engage = value; }
        }

        [DataMember]
        public String resolve
        {
            get { return this._resolve; }
            set { this._resolve = value; }
        }

        [DataMember]
        public String wowResolve
        {
            get { return this._wowResolve; }
            set { this._wowResolve = value; }
        }

        [DataMember]
        public String config
        {
            get { return this._config; }
            set { this._config = value; }
        }

        [DataMember]
        public String hwSw
        {
            get { return this._hwSw; }
            set { this._hwSw = value; }
        }

        [DataMember]
        public String asDesign
        {
            get { return this._asDesign; }
            set { this._asDesign = value; }
        }

        [DataMember]
        public String executionDefect
        {
            get { return this._executionDefect; }
            set { this._executionDefect = value; }
        }

        [DataMember]
        public String impact
        {
            get { return this._impact; }
            set { this._impact = value; }
        }
    }
}