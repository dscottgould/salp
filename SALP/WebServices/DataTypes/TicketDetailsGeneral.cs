﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace SALP.WebServices.DataTypes
{
    public class TicketDetailsGeneral
    {
        String _ticketId;
        String _incidentSummary;
        String _supportAreaName;
        String _submittingName;
        String _queueName;
        String _fullName;
        String _siSeverity;
        String _severity;
        String _siStatus;
        String _siPriority;
        String _priority;
        String _product;
        String _createDate;
        String _assignedDate;
        String _telephonyAffected;
        String _videoAffected;
        String _hsdAffected;
        String _totalCustomersAffected;
        String _nextAction;
        String _problemCode;
        String _source;
        String _category;
        String _subCategory;
        String _causeCode;
        String _causeDescription;
        String _causeCategory;
        String _causeSubCategory;
        String _solutionCode;
        String _solutionDescription;
        String _solutionCategory;
        String _solutionSubCategory;
        String _externalAssignee;
        String _externalReference;
        String _alarmDate;
        String _ttrStart;
        String _ttrStop;
        String _indicatorCity;
        String _indicatorState;
        String _networkElement;
        String _correlationType;
        String _serviceCondition;
        String _careImpact;
        String _estimatedServiceRestore;
        String _siLastModifiedBy;
        String _lastModifiedBy;
        String _modifiedDate;
        String _siResolvedBy;
        String _resolvedBy;
        String _contactDepartment;
        String _contactPhone;
        String _regionName;
        String _duration;



        public TicketDetailsGeneral()
        {
            // Base Constructor
        }

        [DataMember]
        public String ticketId
        {
            get { return this._ticketId; }
            set { this._ticketId = value; }
        }

        [DataMember]
        public String incidentSummary
        {
            get { return this._incidentSummary; }
            set { this._incidentSummary = value; }
        }

        [DataMember]
        public String supportAreaName
        {
            get { return this._supportAreaName; }
            set { this._supportAreaName = value; }
        }

        [DataMember]
        public String submittingName
        {
            get { return this._submittingName; }
            set { this._submittingName = value; }
        }

        [DataMember]
        public String queueName
        {
            get { return this._queueName; }
            set { this._queueName = value; }
        }


        [DataMember]
        public String fullName
        {
            get { return this._fullName; }
            set { this._fullName = value; }
        }

        [DataMember]
        public String siSeverity
        {
            get { return this._siSeverity; }
            set { this._siSeverity = value; }
        }

        [DataMember]
        public String severity
        {
            get { return this._severity; }
            set { this._severity = value; }
        }

        [DataMember]
        public String siStatus
        {
            get { return this._siStatus; }
            set { this._siStatus = value; }
        }

        [DataMember]
        public String siPriority
        {
            get { return this._siPriority; }
            set { this._siPriority = value; }
        }

        [DataMember]
        public String priority
        {
            get { return this._priority; }
            set { this._priority = value; }
        }

        [DataMember]
        public String product
        {
            get { return this._product; }
            set { this._product = value; }
        }


        [DataMember]
        public String createDate
        {
            get { return this._createDate; }
            set { this._createDate = value; }
        }

        [DataMember]
        public String assignedDate
        {
            get { return this._assignedDate; }
            set { this._assignedDate = value; }
        }

        [DataMember]
        public String telephonyAffected
        {
            get { return this._telephonyAffected; }
            set { this._telephonyAffected = value; }
        }

        [DataMember]
        public String videoAffected
        {
            get { return this._videoAffected; }
            set { this._videoAffected = value; }
        }


        [DataMember]
        public String hsdAffected
        {
            get { return this._hsdAffected; }
            set { this._hsdAffected = value; }
        }

        [DataMember]
        public String totalCustomersAffected
        {
            get { return this._totalCustomersAffected; }
            set { this._totalCustomersAffected = value; }
        }

        [DataMember]
        public String nextAction
        {
            get { return this._nextAction; }
            set { this._nextAction = value; }
        }

        [DataMember]
        public String problemCode
        {
            get { return this._problemCode; }
            set { this._problemCode = value; }
        }

        [DataMember]
        public String source
        {
            get { return this._source; }
            set { this._source = value; }
        }


        [DataMember]
        public String category
        {
            get { return this._category; }
            set { this._category = value; }
        }

        [DataMember]
        public String subCategory
        {
            get { return this._subCategory; }
            set { this._subCategory = value; }
        }

        [DataMember]
        public String causeCode
        {
            get { return this._causeCode; }
            set { this._causeCode = value; }
        }

        [DataMember]
        public String causeDescription
        {
            get { return this._causeDescription; }
            set { this._causeDescription = value; }
        }

        [DataMember]
        public String causeCategory
        {
            get { return this._causeCategory; }
            set { this._causeCategory = value; }
        }

        [DataMember]
        public String causeSubCategory
        {
            get { return this._causeSubCategory; }
            set { this._causeSubCategory = value; }
        }

        [DataMember]
        public String solutionCode
        {
            get { return this._solutionCode; }
            set { this._solutionCode = value; }
        }

        [DataMember]
        public String solutionDescription
        {
            get { return this._solutionDescription; }
            set { this._solutionDescription = value; }
        }

        [DataMember]
        public String solutionCategory
        {
            get { return this._solutionCategory; }
            set { this._solutionCategory = value; }
        }

        [DataMember]
        public String solutionSubCategory
        {
            get { return this._solutionSubCategory; }
            set { this._solutionSubCategory = value; }
        }

        [DataMember]
        public String externalAssignee
        {
            get { return this._externalAssignee; }
            set { this._externalAssignee = value; }
        }


        [DataMember]
        public String externalReference
        {
            get { return this._externalReference; }
            set { this._externalReference = value; }
        }


        [DataMember]
        public String alarmDate
        {
            get { return this._alarmDate; }
            set { this._alarmDate = value; }
        }


        [DataMember]
        public String ttrStart
        {
            get { return this._ttrStart; }
            set { this._ttrStart = value; }
        }

        [DataMember]
        public String ttrStop
        {
            get { return this._ttrStop; }
            set { this._ttrStop = value; }
        }

        [DataMember]
        public String indicatorCity
        {
            get { return this._indicatorCity; }
            set { this._indicatorCity = value; }
        }

        [DataMember]
        public String indicatorState
        {
            get { return this._indicatorState; }
            set { this._indicatorState = value; }
        }

        [DataMember]
        public String networkElement
        {
            get { return this._networkElement; }
            set { this._networkElement = value; }
        }

        [DataMember]
        public String correlationType
        {
            get { return this._correlationType; }
            set { this._correlationType = value; }
        }

        [DataMember]
        public String serviceCondition
        {
            get { return this._serviceCondition; }
            set { this._serviceCondition = value; }
        }

        [DataMember]
        public String careImpact
        {
            get { return this._careImpact; }
            set { this._careImpact = value; }
        }

        [DataMember]
        public String estimatedServiceRestore
        {
            get { return this._estimatedServiceRestore; }
            set { this._estimatedServiceRestore = value; }
        }

        [DataMember]
        public String siLastModifiedBy
        {
            get { return this._siLastModifiedBy; }
            set { this._siLastModifiedBy = value; }
        }

        [DataMember]
        public String lastModifiedBy
        {
            get { return this._lastModifiedBy; }
            set { this._lastModifiedBy = value; }
        }

        [DataMember]
        public String modifiedDate
        {
            get { return this._modifiedDate; }
            set { this._modifiedDate = value; }
        }

        [DataMember]
        public String siResolvedBy
        {
            get { return this._siResolvedBy; }
            set { this._siResolvedBy = value; }
        }

        [DataMember]
        public String resolvedBy
        {
            get { return this._resolvedBy; }
            set { this._resolvedBy = value; }
        }

        [DataMember]
        public String contactDepartment
        {
            get { return this._contactDepartment; }
            set { this._contactDepartment = value; }
        }


        [DataMember]
        public String contactPhone
        {
            get { return this._contactPhone; }
            set { this._contactPhone = value; }
        }

        [DataMember]
        public String regionName
        {
            get { return this._regionName; }
            set { this._regionName = value; }
        }

        [DataMember]
        public String duration
        {
            get { return this._duration; }
            set { this._duration = value; }
        }
    }
}