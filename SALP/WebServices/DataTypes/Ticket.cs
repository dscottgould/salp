﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace SALP.WebServices.DataTypes
{
    [DataContract]
    public class Ticket
    {
        String _ticketId;
        String _calcTTR;
        String _smRelatedTicket;
        String _adjDurationMinutes;
        String _causeDescription;
        String _solutionDescription;
        String _smId;
        String _severity;
        String _alarmStartDate;
        String _alarmStartTime;
        String _workingStartDate;
        String _workingStartTime;
        String _resolvedDate;
        String _resolvedTime;
        String _actualStartDate;
        String _actualStartTime;
        String _actualEndDate;
        String _actualEndTime;
        String _changeReason;
        String _modifiedDate;


        public Ticket()
        {
            // Base Constructor
        }


        [DataMember]
        public String ticketId
        {
            get { return this._ticketId; }
            set { this._ticketId = value; }
        }

        [DataMember]
        public String calcTTR
        {
            get { return this._calcTTR; }
            set { this._calcTTR = value; }
        }

        [DataMember]
        public String smRelatedTicket
        {
            get { return this._smRelatedTicket; }
            set { this._smRelatedTicket = value; }
        }

        [DataMember]
        public String adjDurationMinutes
        {
            get { return this._adjDurationMinutes; }
            set { this._adjDurationMinutes = value; }
        }

        [DataMember]
        public String causeDescription
        {
            get { return this._causeDescription; }
            set { this._causeDescription = value; }
        }

        [DataMember]
        public String solutionDescription
        {
            get { return this._solutionDescription; }
            set { this._solutionDescription = value; }
        }

        [DataMember]
        public String smId
        {
            get { return this._smId; }
            set { this._smId = value; }
        }

        [DataMember]
        public String severity
        {
            get { return this._severity; }
            set { this._severity = value; }
        }

        [DataMember]
        public String alarmStartDate
        {
            get { return this._alarmStartDate; }
            set { this._alarmStartDate = value; }
        }

        [DataMember]
        public String alarmStartTime
        {
            get { return this._alarmStartTime; }
            set { this._alarmStartTime = value; }
        }

        [DataMember]
        public String workingStartDate
        {
            get { return this._workingStartDate; }
            set { this._workingStartDate = value; }
        }

        [DataMember]
        public String workingStartTime
        {
            get { return this._workingStartTime; }
            set { this._workingStartTime = value; }
        }

        [DataMember]
        public String resolvedDate
        {
            get { return this._resolvedDate; }
            set { this._resolvedDate = value; }
        }

        [DataMember]
        public String resolvedTime
        {
            get { return this._resolvedTime; }
            set { this._resolvedTime = value; }
        }

        [DataMember]
        public String actualStartDate
        {
            get { return this._actualStartDate; }
            set { this._actualStartDate = value; }
        }

        [DataMember]
        public String actualStartTime
        {
            get { return this._actualStartTime; }
            set { this._actualStartTime = value; }
        }

        [DataMember]
        public String actualEndTime
        {
            get { return this._actualEndTime; }
            set { this._actualEndTime = value; }
        }

        [DataMember]
        public String actualEndDate
        {
            get { return this._actualEndDate; }
            set { this._actualEndDate = value; }
        }

        [DataMember]
        public String changeReason
        {
            get { return this._changeReason; }
            set { this._changeReason = value; }
        }

        [DataMember]
        public String modifiedDate
        {
            get { return this._modifiedDate; }
            set { this._modifiedDate = value; }
        }
    }
}