﻿using System;
using System.Runtime.Serialization;

namespace SALP.WebServices.DataTypes
{
    [DataContract]

    public class OutageElement
    {
        private String _ticketId;
        private String _entryId;
        private String _serviceAffected;
        private String _elementName;
        private String _telephonyAffected;
        private String _hsdAffected;
        private String _videoAffected;
        private String _totalAffected;
        private String _aeStatus;
        private String _elementType;
        private String _plannedFlag;
        private String _plannedStart;
        private String _plannedEnd;
        private String _duration;
        private String _source;
        private String _itemFocus;
        private String _regionId;
        private String _regionName;
        private String _systemId;
        private String _systemName;
        private String _indicatorState;
        private String _location;
        private String _product;
        private String _commercialUse;
        private String _messageInternal;
        private String _linkProblemCode;
        private String _problemDescription;
        private String _category;
        private String _subcategory;
        private String _causeCode;
        private String _causeDescription;
        private String _causeCategory;
        private String _causeSubcategory;
        private String _solutionCode;
        private String _solutionDescription;
        private String _solutionCategory;
        private String _solutionSubcategory;
        private String _originatingTicket;
        private String _submitter;
        private String _lastModifiedBy;
        private String _actualStart;
        private String _actualEnd;
        private String _etr;
        private String _createDate;
        private String _modifiedDate;


        public OutageElement()
        {
            // Base Constructor
        }

        [DataMember]
        public String ticketId
        {
            get { return this._ticketId; }
            set { this._ticketId = value; }
        }

        [DataMember]
        public String entryId
        {
            get { return this._entryId; }
            set { this._entryId = value; }
        }

        [DataMember]
        public String serviceAffected
        {
            get { return this._serviceAffected; }
            set { this._serviceAffected = value; }
        }

        [DataMember]
        public String elementName
        {
            get { return this._elementName; }
            set { this._elementName = value; }
        }

        [DataMember]
        public String telephonyAffected
        {
            get { return this._telephonyAffected; }
            set { this._telephonyAffected = value; }
        }

        [DataMember]
        public String hsdAffected
        {
            get { return this._hsdAffected; }
            set { this._hsdAffected = value; }
        }

        [DataMember]
        public String videoAffected
        {
            get { return this._videoAffected; }
            set { this._videoAffected = value; }
        }

        [DataMember]
        public String totalAffected
        {
            get { return this._totalAffected; }
            set { this._totalAffected = value; }
        }

        [DataMember]
        public String aeStatus
        {
            get { return this._aeStatus; }
            set { this._aeStatus = value; }
        }

        [DataMember]
        public String elementType
        {
            get { return this._elementType; }
            set { this._elementType = value; }
        }

        [DataMember]
        public String plannedFlag
        {
            get { return this._plannedFlag; }
            set { this._plannedFlag = value; }
        }

        [DataMember]
        public String plannedStart
        {
            get { return this._plannedStart; }
            set { this._plannedStart = value; }
        }

        [DataMember]
        public String plannedEnd
        {
            get { return this._plannedEnd; }
            set { this._plannedEnd = value; }
        }

        [DataMember]
        public String duration
        {
            get { return this._duration; }
            set { this._duration = value; }
        }

        [DataMember]
        public String source
        {
            get { return this._source; }
            set { this._source = value; }
        }

        [DataMember]
        public String itemFocus
        {
            get { return this._itemFocus; }
            set { this._itemFocus = value; }
        }

        [DataMember]
        public String regionId
        {
            get { return this._regionId; }
            set { this._regionId = value; }
        }

        [DataMember]
        public String regionName
        {
            get { return this._regionName; }
            set { this._regionName = value; }
        }

        [DataMember]
        public String systemId
        {
            get { return this._systemId; }
            set { this._systemId = value; }
        }

        [DataMember]
        public String systemName
        {
            get { return this._systemName; }
            set { this._systemName = value; }
        }

        [DataMember]
        public String indicatorState
        {
            get { return this._indicatorState; }
            set { this._indicatorState = value; }
        }

        [DataMember]
        public String location
        {
            get { return this._location; }
            set { this._location = value; }
        }

        [DataMember]
        public String product
        {
            get { return this._product; }
            set { this._product = value; }
        }

        [DataMember]
        public String commercialUse
        {
            get { return this._commercialUse; }
            set { this._commercialUse = value; }
        }

        [DataMember]
        public String messageInternal
        {
            get { return this._messageInternal; }
            set { this._messageInternal = value; }
        }

        [DataMember]
        public String linkProblemCode
        {
            get { return this._linkProblemCode; }
            set { this._linkProblemCode = value; }
        }

        [DataMember]
        public String problemDescription
        {
            get { return this._problemDescription; }
            set { this._problemDescription = value; }
        }

        [DataMember]
        public String category
        {
            get { return this._category; }
            set { this._category = value; }
        }

        [DataMember]
        public String subcategory
        {
            get { return this._subcategory; }
            set { this._subcategory = value; }
        }

        [DataMember]
        public String causeCode
        {
            get { return this._causeCode; }
            set { this._causeCode = value; }
        }

        [DataMember]
        public String causeDescription
        {
            get { return this._causeDescription; }
            set { this._causeDescription = value; }
        }

        [DataMember]
        public String causeCategory
        {
            get { return this._causeCategory; }
            set { this._causeCategory = value; }
        }

        [DataMember]
        public String causeSubcategory
        {
            get { return this._causeSubcategory; }
            set { this._causeSubcategory = value; }
        }

        [DataMember]
        public String solutionCode
        {
            get { return this._solutionCode; }
            set { this._solutionCode = value; }
        }

        [DataMember]
        public String solutionDescription
        {
            get { return this._solutionDescription; }
            set { this._solutionDescription = value; }
        }

        [DataMember]
        public String solutionCategory
        {
            get { return this._solutionCategory; }
            set { this._solutionCategory = value; }
        }

        [DataMember]
        public String solutionSubcategory
        {
            get { return this._solutionSubcategory; }
            set { this._solutionSubcategory = value; }
        }

        [DataMember]
        public String originatingTicket
        {
            get { return this._originatingTicket; }
            set { this._originatingTicket = value; }
        }

        [DataMember]
        public String submitter
        {
            get { return this._submitter; }
            set { this._submitter = value; }
        }


        [DataMember]
        public String lastModifiedBy
        {
            get { return this._lastModifiedBy; }
            set { this._lastModifiedBy = value; }
        }

        [DataMember]
        public String actualStart
        {
            get { return this._actualStart; }
            set { this._actualStart = value; }
        }

        [DataMember]
        public String actualEnd
        {
            get { return this._actualEnd; }
            set { this._actualEnd = value; }
        }

        [DataMember]
        public String etr
        {
            get { return this._etr; }
            set { this._etr = value; }
        }

        [DataMember]
        public String createDate
        {
            get { return this._createDate; }
            set { this._createDate = value; }
        }

        [DataMember]
        public String modifiedDate
        {
            get { return this._modifiedDate; }
            set { this._modifiedDate = value; }
        }
    }
}