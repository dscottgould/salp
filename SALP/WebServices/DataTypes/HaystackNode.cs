﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace SALP.WebServices.DataTypes
{
    public class HaystackNode
    {
        private String _node;

        public HaystackNode()
        {
            // Base Constructor
        }

        [DataMember]
        public String node
        {
            get { return this._node; }
            set { this._node = value; }
        }
    }
}