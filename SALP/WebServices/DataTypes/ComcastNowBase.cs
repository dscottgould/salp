﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace SALP.WebServices.DataTypes
{
    public class ComcastNowBase
    {
        private String _last;
        private String _itemCount;
        private String _totalArticleCount;
        private String _totalTopicCount;
        private List<ComcastNowItem> _items;

        public ComcastNowBase()
        {
            /// Base Constructor
        }

        [DataMember]
        public String last
        {
            get { return this._last; }
            set { this._last = value; }
        }

        [DataMember]
        public String itemCount
        {
            get { return this._itemCount; }
            set { this._itemCount = value; }
        }

        [DataMember]
        public String totalArticleCount
        {
            get { return this._totalArticleCount; }
            set { this._totalArticleCount = value; }
        }

        [DataMember]
        public String totalTopicCount
        {
            get { return this._totalTopicCount; }
            set { this._totalTopicCount = value; }
        }


        [DataMember]
        public List<ComcastNowItem> items
        {
            get { return this._items; }
            set { this._items = value; }
        }
    }
}