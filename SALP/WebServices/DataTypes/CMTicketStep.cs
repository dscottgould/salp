﻿using System;
using System.Runtime.Serialization;

namespace SALP.WebServices.DataTypes
{
    [DataContract]
    public class CMTicketStep
    {
        private String _ticketId;
        private String _stepDescription;
        private String _stepLabel;
        private String _stepSequence;
        private String _procedureId;
        private String _plannedStart;
        private String _plannedEnd;
        private String _actualStart;
        private String _actualEnd;
        private String _estimatedDuration;
        private String _actualDuration;
        

        public CMTicketStep()
        {
            // Base Constructor
        }

        [DataMember]
        public String ticketId
        {
            get { return this._ticketId; }
            set { this._ticketId = value; }
        }

        [DataMember]
        public String stepDescription
        {
            get { return this._stepDescription; }
            set { this._stepDescription = value; }
        }

        [DataMember]
        public String stepLabel
        {
            get { return this._stepLabel; }
            set { this._stepLabel = value; }
        }

        [DataMember]
        public String stepSequence
        {
            get { return this._stepSequence; }
            set { this._stepSequence = value; }
        }

        [DataMember]
        public String procedureId
        {
            get { return this._procedureId; }
            set { this._procedureId = value; }
        }

        [DataMember]
        public String plannedStart
        {
            get { return this._plannedStart; }
            set { this._plannedStart = value; }
        }

        [DataMember]
        public String plannedEnd
        {
            get { return this._plannedEnd; }
            set { this._plannedEnd = value; }
        }

        [DataMember]
        public String actualStart
        {
            get { return this._actualStart; }
            set { this._actualStart = value; }
        }

        [DataMember]
        public String actualEnd
        {
            get { return this._actualEnd; }
            set { this._actualEnd = value; }
        }

        [DataMember]
        public String estimatedDuration
        {
            get { return this._estimatedDuration; }
            set { this._estimatedDuration = value; }
        }


        [DataMember]
        public String actualDuration
        {
            get { return this._actualDuration; }
            set { this._actualDuration = value; }
        }
    }
}