﻿using System;
using System.Runtime.Serialization;

namespace SALP.WebServices.DataTypes
{
    [DataContract]
    public class SRTicketGeneralDetails
    {
        private String _ticketId;
        private String _statusCode;
        private String _status;
        private String _priorityCode;
        private String _priority;
        private String _submitter;
        private String _assignedDate;
        private String _createDate;
        private String _assignedDivision;
        private String _supportAreaName;
        private String _assignedQName;
        private String _fullName;
        private String _problemCode;
        private String _problemDescription;
        private String _problemCategory;
        private String _problemSubcategory;
        private String _solutionCode;
        private String _solutionDescription;
        private String _solutionCategory;
        private String _solutionSubcategory;
        private String _requestForName;
        private String _requestForPhone;
        private String _requestForLocation;
        private String _requestForEmail;
        private String _workLocationStreet;
        private String _workLocationCity;
        private String _contactName;
        private String _contactPhone;
        private String _contactEmail;

        public SRTicketGeneralDetails()
        {
            // Base Constructor
        }

        [DataMember]
        public String ticketId
        {
            get { return this._ticketId; }
            set { this._ticketId = value; }
        }

        [DataMember]
        public String statusCode
        {
            get { return this._statusCode; }
            set { this._statusCode = value; }
        }

        [DataMember]
        public String status
        {
            get { return this._status; }
            set { this._status = value; }
        }

        [DataMember]
        public String priorityCode
        {
            get { return this._priorityCode; }
            set { this._priorityCode = value; }
        }

        [DataMember]
        public String priority
        {
            get { return this._priority; }
            set { this._priority = value; }
        }

        [DataMember]
        public String submitter
        {
            get { return this._submitter; }
            set { this._submitter = value; }
        }

        [DataMember]
        public String assignedDate
        {
            get { return this._assignedDate; }
            set { this._assignedDate = value; }
        }

        [DataMember]
        public String createDate
        {
            get { return this._createDate; }
            set { this._createDate = value; }
        }

        [DataMember]
        public String assignedDivision
        {
            get { return this._assignedDivision; }
            set { this._assignedDivision = value; }
        }

        [DataMember]
        public String supportAreaName
        {
            get { return this._supportAreaName; }
            set { this._supportAreaName = value; }
        }

        [DataMember]
        public String assignedQName
        {
            get { return this._assignedQName; }
            set { this._assignedQName = value; }
        }

        [DataMember]
        public String fullName
        {
            get { return this._fullName; }
            set { this._fullName = value; }
        }

        [DataMember]
        public String problemCode
        {
            get { return this._problemCode; }
            set { this._problemCode = value; }
        }

        [DataMember]
        public String problemDescription
        {
            get { return this._problemDescription; }
            set { this._problemDescription = value; }
        }

        [DataMember]
        public String problemCategory
        {
            get { return this._problemCategory; }
            set { this._problemCategory = value; }
        }

        [DataMember]
        public String problemSubcategory
        {
            get { return this._problemSubcategory; }
            set { this._problemSubcategory = value; }
        }

        [DataMember]
        public String solutionCode
        {
            get { return this._solutionCode; }
            set { this._solutionCode = value; }
        }

        [DataMember]
        public String solutionDescription
        {
            get { return this._solutionDescription; }
            set { this._solutionDescription = value; }
        }

        [DataMember]
        public String solutionCategory
        {
            get { return this._solutionCategory; }
            set { this._solutionCategory = value; }
        }

        [DataMember]
        public String solutionSubcategory
        {
            get { return this._solutionSubcategory; }
            set { this._solutionSubcategory = value; }
        }

        [DataMember]
        public String requestForName
        {
            get { return this._requestForName; }
            set { this._requestForName = value; }
        }

        [DataMember]
        public String requestForPhone
        {
            get { return this._requestForPhone; }
            set { this._requestForPhone = value; }
        }

        [DataMember]
        public String requestForLocation
        {
            get { return this._requestForLocation; }
            set { this._requestForLocation = value; }
        }

        [DataMember]
        public String requestForEmail
        {
            get { return this._requestForEmail; }
            set { this._requestForEmail = value; }
        }

        [DataMember]
        public String workLocationStreet
        {
            get { return this._workLocationStreet; }
            set { this._workLocationStreet = value; }
        }

        [DataMember]
        public String workLocationCity
        {
            get { return this._workLocationCity; }
            set { this._workLocationCity = value; }
        }

        [DataMember]
        public String contactName
        {
            get { return this._contactName; }
            set { this._contactName = value; }
        }

        [DataMember]
        public String contactPhone
        {
            get { return this._contactPhone; }
            set { this._contactPhone = value; }
        }
        
        [DataMember]
        public String contactEmail
        {
            get { return this._contactEmail; }
            set { this._contactEmail = value; }
        }
    }
}