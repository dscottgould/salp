﻿using System;
using System.Runtime.Serialization;

namespace SALP.WebServices.DataTypes
{
    [DataContract]

    public class CMTicketGeneralDetails
    {
        private String _ticketId;
        private String _changeSummary;
        private String _regionName;
        private String _supportAreaName;
        private String _queueName;
        private String _fullName;
        private String _status;
        private String _ndStatus;
        private String _product;
        private String _assignedDate;
        private String _videoAffected;
        private String _hsdAffected;
        private String _telephonyAffected;
        private String _totalCustomersAffected;
        private String _nextAction;
        private String _maintenanceType;
        private String _projectName;
        private String _changeCategory;
        private String _changeSubcategory;
        private String _impactLevel;
        private String _indicatorCity;
        private String _changeDetail;
        private String _plannedStart;
        private String _plannedEnd;

        public CMTicketGeneralDetails()
        {
            // Base Constructor
        }

        [DataMember]
        public String ticketId
        {
            get { return this._ticketId; }
            set { this._ticketId = value; }
        }

        [DataMember]
        public String changeSummary
        {
            get { return this._changeSummary; }
            set { this._changeSummary = value; }
        }

        [DataMember]
        public String regionName
        {
            get { return this._regionName; }
            set { this._regionName = value; }
        }

        [DataMember]
        public String supportAreaName
        {
            get { return this._supportAreaName; }
            set { this._supportAreaName = value; }
        }

        [DataMember]
        public String queueName
        {
            get { return this._queueName; }
            set { this._queueName = value; }
        }

        [DataMember]
        public String fullName
        {
            get { return this._fullName; }
            set { this._fullName = value; }
        }

        [DataMember]
        public String status
        {
            get { return this._status; }
            set { this._status = value; }
        }

        [DataMember]
        public String ndStatus
        {
            get { return this._ndStatus; }
            set { this._ndStatus = value; }
        }

        [DataMember]
        public String product
        {
            get { return this._product; }
            set { this._product = value; }
        }

        [DataMember]
        public String assignedDate
        {
            get { return this._assignedDate; }
            set { this._assignedDate = value; }
        }

        [DataMember]
        public String videoAffected
        {
            get { return this._videoAffected; }
            set { this._videoAffected = value; }
        }

        [DataMember]
        public String hsdAffected
        {
            get { return this._hsdAffected; }
            set { this._hsdAffected = value; }
        }

        [DataMember]
        public String telephonyAffected
        {
            get { return this._telephonyAffected; }
            set { this._telephonyAffected = value; }
        }

        [DataMember]
        public String totalCustomersAffected
        {
            get { return this._totalCustomersAffected; }
            set { this._totalCustomersAffected = value; }
        }

        [DataMember]
        public String nextAction
        {
            get { return this._nextAction; }
            set { this._nextAction = value; }
        }

        [DataMember]
        public String maintenanceType
        {
            get { return this._maintenanceType; }
            set { this._maintenanceType = value; }
        }

        [DataMember]
        public String projectName
        {
            get { return this._projectName; }
            set { this._projectName = value; }
        }

        [DataMember]
        public String changeCategory
        {
            get { return this._changeCategory; }
            set { this._changeCategory = value; }
        }

        [DataMember]
        public String changeSubcategory
        {
            get { return this._changeSubcategory; }
            set { this._changeSubcategory = value; }
        }

        [DataMember]
        public String impactLevel
        {
            get { return this._impactLevel; }
            set { this._impactLevel = value; }
        }

        [DataMember]
        public String indicatorCity
        {
            get { return this._indicatorCity; }
            set { this._indicatorCity = value; }
        }

        [DataMember]
        public String changeDetail
        {
            get { return this._changeDetail; }
            set { this._changeDetail = value; }
        }

        [DataMember]
        public String plannedStart
        {
            get { return this._plannedStart; }
            set { this._plannedStart = value; }
        }

        [DataMember]
        public String plannedEnd
        {
            get { return this._plannedEnd; }
            set { this._plannedEnd = value; }
        }
    }
}