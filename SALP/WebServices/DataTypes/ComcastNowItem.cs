﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace SALP.WebServices.DataTypes
{
    public class ComcastNowItem
    {
        private String _id;
        private String _title;
        private String _comments;
        private String _favorites;
        private String _url;
        private String _publishedDate;
        private String _dateTimeAdded;
        private String _favorite;
        private String _moduleCount;
        private String _favoriteType;
        private List<ComcastNowTag> _tags;

        public ComcastNowItem()
        {
            // Base Constructor
        }

        [DataMember]
        public String id
        {
            get { return this._id; }
            set { this._id = value; }
        }

        [DataMember]
        public String title
        {
            get { return this._title; }
            set { this._title = value; }
        }

        [DataMember]
        public String comments
        {
            get { return this._comments; }
            set { this._comments = value; }
        }

        [DataMember]
        public String favorites
        {
            get { return this._favorites; }
            set { this._favorites = value; }
        }

        [DataMember]
        public String url
        {
            get { return this._url; }
            set { this._url = value; }
        }


        [DataMember]
        public String publishedDate
        {
            get { return this._publishedDate; }
            set { this._publishedDate = value; }
        }

        [DataMember]
        public String dateTimeAdded
        {
            get { return this._dateTimeAdded; }
            set { this._dateTimeAdded = value; }
        }

        [DataMember]
        public String favorite
        {
            get { return this._favorite; }
            set { this._favorite = value; }
        }

        [DataMember]
        public String moduleCount
        {
            get { return this._moduleCount; }
            set { this._moduleCount = value; }
        }

        [DataMember]
        public String favoriteType
        {
            get { return this._favoriteType; }
            set { this._favoriteType = value; }
        }

        [DataMember]
        public List<ComcastNowTag> tags
        {
            get { return this._tags; }
            set { this._tags = value; }
        }
    }
}