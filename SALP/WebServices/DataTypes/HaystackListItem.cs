﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace SALP.WebServices.DataTypes
{
    public class HaystackItem
    {
        private string _baseline;
        private string _baseline1;
        private string _baseline2;
        private string _baseline3;
        private string _baseline4;
        private string _baseline5;
        private string _count;
        private string _startDate;
        private string _endDate;

        public HaystackItem()
        {
            //Base Constructor
        }

        [DataMember]
        public String baseline
        {
            get { return this._baseline; }
            set { this._baseline = value; }
        }

        [DataMember]
        public String baseline1
        {
            get { return this._baseline1; }
            set { this._baseline1 = value; }
        }

        [DataMember]
        public String baseline2
        {
            get { return this._baseline2; }
            set { this._baseline2 = value; }
        }

        [DataMember]
        public String baseline3
        {
            get { return this._baseline3; }
            set { this._baseline3 = value; }
        }

        [DataMember]
        public String baseline4
        {
            get { return this._baseline4; }
            set { this._baseline4 = value; }
        }

        [DataMember]
        public String baseline5
        {
            get { return this._baseline5; }
            set { this._baseline5 = value; }
        }

        [DataMember]
        public String count
        {
            get { return this._count; }
            set { this._count = value; }
        }

        [DataMember]
        public String startDate
        {
            get { return this._startDate; }
            set { this._startDate = value; }
        }

        [DataMember]
        public String endDate
        {
            get { return this._endDate; }
            set { this._endDate = value; }
        }

    }
}