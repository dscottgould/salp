﻿using System;
using System.Runtime.Serialization;

namespace SALP.WebServices.DataTypes
{
    [DataContract]

    public class CMTreeMapAttribute
    {
        private String _userName;
        private String _text;
        private Int32 _value;
        private Boolean _isAttributeNode = true;

        public CMTreeMapAttribute()
        {
            // Base Constructor
        }

        [DataMember]
        public String userName
        {
            get { return this._userName; }
            set { this._userName = value; }
        }

        [DataMember]
        public String text
        {
            get { return this._text; }
            set { this._text = value; }
        }

        [DataMember]
        public Int32 value
        {
            get { return this._value; }
            set { this._value = value; }
        }

        [DataMember]
        public Boolean isAttributeNode
        {
            get { return this._isAttributeNode; }
            set { this._isAttributeNode = value; }
        }
    }
}