﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace SALP.WebServices.DataTypes
{
    public class WorkLogNotification
    {
        String _logId;
        String _message;


        public WorkLogNotification()
        {
            // Base Constructor
        }

        [DataMember]
        public String logId
        {
            get { return this._logId; }
            set { this._logId = value; }
        }

        [DataMember]
        public String message
        {
            get { return this._message; }
            set { this._message = value; }
        }
    }
}