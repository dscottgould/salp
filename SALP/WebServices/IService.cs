﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.ServiceModel.Web;

using SALP.WebServices.DataTypes;

namespace SALP.WebServices
{
    [ServiceContract(SessionMode = SessionMode.Allowed)]
    public interface IService
    {
        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        ReturnData<String> GetDashboardByID(String dashboardId);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        ReturnData<List<UserDashboard>> GetUserDashboards(String accountName);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        ReturnData<UserDashboard> GetUserCookie(String accountName);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        ReturnData<String> SaveUserCookie(String accountName, String dashboardName, String cookie);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        ReturnData<String> RenameDashboard(String dashboardId, String dashboardName);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        ReturnData<String> DeleteDashboard(String accountName, String dashboardId);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        ReturnData<String> SetDefaultDashboard(String accountName, String dashboardId);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        ReturnData<Stocks> GetComcastStock();

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        ReturnData<List<CNArticle>> GetComcastNow();

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        ReturnData<List<BrouhaRecord>> GetBrouhaData();

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        ReturnData<List<HSDRecord>> GetHSDMetrics();

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        ReturnData<List<SubscriberCounts>> GetSubscriberCounts();

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        ReturnData<List<HaystackItem>> GetHaystackCalls(string username = null, string password = null, string segment = null, string filter = null, string fromDate = null, string toDate = null);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        ReturnData<List<HaystackItem>> GetHaystackTickets(string username = null, string password = null, string segment = null, string filter = null, string fromDate = null, string toDate = null);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        ReturnData<List<HaystackItem>> GetHaystackTruckRolls(string username = null, string password = null, string segment = null, string filter = null, string fromDate = null, string toDate = null);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        ReturnData<List<HaystackSegmentRecord>> GetHaystackSegments();

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        ReturnData<List<HaystackDivision>> GetHaystackDivisions(string username);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        ReturnData<List<HaystackNode>> GetHaystackNodes(string username);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        ReturnData<List<UserFilter>> GetUserFilterList(string username, string password);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        ReturnData<List<Portlet>> GetPorletsList();

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        ReturnData<String> SaveUserPortlets(String accountName, String porletsList);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        ReturnData<String> GetUserPortlets(String accountName);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        ReturnData<List<SelectData>> GetSelectData(String accountName, int categoryId);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        ReturnData<List<Ticket>> GetTickets();


        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        ReturnData<Tickets> GetTicket(String ticketNumber);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        ReturnData<String> UpdateTicket(String actualEndDate, String actualEndTime, String actualStartDate, String actualStartTime,
            String adjDurationMinutes, String alarmStartDate, String alarmStartTime, String causeDescription, String changeReason, String modifiedDate,
            String resolvedDate, String resolvedTime, String severity, String smId, String solutionDescription, String ticketId,
            String workingStartDate, String workingStartTime);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        ReturnData<NetworkLensIncident> GetIncidentTicket(String ticketNumber);


        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        ReturnData<String> UpdateIncidentTicket(String ticketId, String editIncidentWeek, String editLensGroup, String editRootCauseGroup, String editIncidentSummary);


        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        ReturnData<List<Incident>> GetIncidentTickets(String week = null);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        ReturnData<TicketDetailsGeneral> GetTicketDetailsGeneral(String ticketNumber);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        ReturnData<TicketDetailsAttributes> GetTicketDetailsAttributes(String ticketNumber);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        ReturnData<List<CustomerTicket>> GetCustomerTickets(String ticketNumber);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        ReturnData<List<TicketDetailsAffected>> GetTicketDetailsAffected(String ticketNumber);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        ReturnData<List<WorkLogTicket>> GetWorkLogTickets(String ticketNumber, String source);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        ReturnData<TicketTimeline> GetTicketTimeline(String ticketNumber);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        ReturnData<String> AddUserFavoriteLink(String accountName, String description, String link, String group);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        ReturnData<List<UserFavoriteLink>> GetUserFavoriteLinks(String accountName);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        ReturnData<String> EditUserFavoriteLink(String linkId, String accountName, String description, String link, String group);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        ReturnData<String> DeleteUserFavoriteLink(String linkId);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        ReturnData<List<WorkLogNotification>> GetWorkLogNotifications(String ticketNumber, String eventType);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        ReturnData<List<SITicketTimeline>> GetSITicketTimeline(String ticketNumber);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        ReturnData<List<ConflictTicket>> GetConflictTickets(String months);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        ReturnData<List<UserFavoriteLink>> GetAllUserFavoriteLinks(String accountName);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        ReturnData<String> SaveUserFavoriteLinks(String accountName, String selectionList);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        ReturnData<List<IncidentReview>> GetNetworkOperationsIncidentReview();

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        ReturnData<List<WeeklyReviewMeasure>> GetWeeklyReviewMeasures(String lens, String lensGroup);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        ReturnData<List<ServerMonitorItem>> GetServerJobs(String serverName);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        ReturnData<List<CRTicketStatus>> GetCRTicketStatus();

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        ReturnData<List<URLMonitorItem>> GetURLMonitor();

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        ReturnData<List<ErrorStatus>> GetSubscriptionErrorStatus();

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        ReturnData<List<TempDBLogItem>> GetTempDBLogStatus();

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        ReturnData<List<ApplicationEventMonitorItem>> GetApplicationEventMonitor();

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        ReturnData<List<CRTicketsItem>> GetCRTickets();

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        ReturnData<List<IVRCDRCall>> GetIVRCDRCalls();

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        ReturnData<List<CRTicketDisplayItem>> GetCRTicketDelayDisplay();

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        ReturnData<List<FCCThresholdBreachedItem>> GetFCCThresholdBreachedData();

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        ReturnData<List<ServerProcessesItem>> GetServerProcesses(String serverName);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        ReturnData<String> KillProcess(String serverName, String sessionId, Boolean statusOnly);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        ReturnData<List<Server>> GetServersList(Boolean hasMonitoredProcesses, Boolean hasLongRunningJobs);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        ReturnData<String> AddServer(String serverName, Boolean hasMonitoredProcesses, Boolean hasLongRunningJobs);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        ReturnData<String> EditServer(String id, String serverName, Boolean hasMonitoredProcesses, Boolean hasLongRunningJobs);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        ReturnData<String> DeleteServer(String id);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        ReturnData<List<ServerCPUUsage>> GetServerCPUUsage(String serverName);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        ReturnData<List<MaintenanceResolveStatus>> GetMaintanceResoveStatus();

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        ReturnData<List<MaintenanceCausedSev1MonthOverMonth>> GetMaintanceCausedSev1MonthOverMonth();


        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        ReturnData<List<MaintenanceResolveStatus>> GetMaintanceCausedSuccessFailByMonth(String month, String year);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        ReturnData<List<Sev1CausedByMaintDetail>> GetSev1CausedByMaintDetails(String directorName, String month, String year, String reportLevel);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        ReturnData<SMTicketGeneralDetails> GetSMTicketGeneralDetails(String ticketNumber);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        ReturnData<List<SMTicketAffectedElement>> GetSMTicketAffectedElements(String ticketNumber);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        ReturnData<List<WorkLogTicket>> GetSMTicketWorkLog(String ticketNumber, String source);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        ReturnData<SRTicketGeneralDetails> GetSRTicketGeneralDetails(String ticketNumber);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        ReturnData<List<SRTicketAttribute>> GetSRTicketAttributes(String ticketNumber);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        ReturnData<List<WorkLogTicket>> GetSRTicketWorkLog(String ticketNumber, String source);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        ReturnData<CMTicketGeneralDetails> GetCMTicketGeneralDetails(String ticketNumber);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        ReturnData<List<CMTicketContactDetail>> GetCMContactDetails(String ticketNumber);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        ReturnData<List<CMTicketAttribute>> GetCMTicketAttributes(String ticketNumber);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        ReturnData<List<CMTicketStep>> GetCMTicketSteps(String ticketNumber);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        ReturnData<List<CMTicketAffectedElement>> GetCMTicketAffectedElements(String ticketNumber);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        ReturnData<List<CMTicketWorkLog>> GetCMTicketWorkLog(String ticketNumber, String source);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        ReturnData<CRTicketGeneralDetails> GetCRTicketGeneralDetails(String ticketNumber);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        ReturnData<List<CRTicketAttribute>> GetCRTicketAttributes(String ticketNumber);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        ReturnData<List<CRTicketWorkLog>> GetCRTicketWorkLog(String ticketNumber, String source);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        ReturnData<SITicketGeneralDetails> GetSITicketGeneralDetails(String ticketNumber);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        ReturnData<List<SITicketAttribute>> GetSITicketAttributes(String ticketNumber);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        ReturnData<List<SITicketCustomerTicket>> GetSITicketCustomerTickets(String ticketNumber);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        ReturnData<List<SITicketAffectedElement>> GetSITicketAffectedElements(String ticketNumber);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        ReturnData<List<SITicketWorkLog>> GetSITicketWorkLog(String ticketNumber, String source);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        ReturnData<OutageElement> GetOETicketGeneralDetails(String ticketNumber);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        ReturnData<OETicketAffectedElement> GetOETicketAffectedElements(String ticketNumber);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        ReturnData<List<OETicketAttribute>> GetOETicketAttributes(String ticketNumber);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        ReturnData<List<OETicketWorkLog>> GetOETicketWorkLog(String ticketNumber, String source);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        List<CMTreeMapRoot> GetCMTreeView(String name, String month, String year);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        ReturnData<List<CMDetailByHeirarchy>> GetCMDetailByHierarchy(String userName, String month, String year, String reportLevel);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        ReturnData<List<SAPUser>> GetSAPUser(String name, String level, String unit);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        ReturnData<List<SAPSubordinate>> GetSAPSubordinates(String name);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        ReturnData<List<ActiveTicket>> GetActiveTickets(String severity);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        ReturnData<String> SaveHaystackFilter(String accountName, String userName, String password, String filterName, String division, String nodes);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        ReturnData<List<HaystackBreakDownCall>> GetHaystackBinsByBreakdownCalls(String breakdown, String segment, String filter, String username, String password, Boolean showOthers, String fromDate, String toDate);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        ReturnData<List<HaystackBreakDownCall>> GetHaystackBinsByBreakdownTickets(String breakdown, String segment, String filter, String username, String password, Boolean showOthers, String fromDate, String toDate);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        ReturnData<List<HaystackBreakDownCall>> GetHaystackBinsByBreakdownTruckRolls(String breakdown, String segment, String filter, String username, String password, Boolean showOthers, String fromDate, String toDate);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        ReturnData<List<SIEventFunnel>> GetSIEventTunnel(String ticketId);








        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        string TestMe();

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        string Test(int value);
    }


}
