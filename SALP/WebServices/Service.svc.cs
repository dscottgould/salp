﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Web;
using System.IO;
using System.Text;
using System.Net;
using System.Xml;

using System.Runtime.Serialization.Json;

using SALP.WebServices.DataTypes;
using SALP.WebServices.Library;

namespace SALP.WebServices
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    [ServiceBehavior(IncludeExceptionDetailInFaults = true, Name = "SALP.WebServices", Namespace = "http://comcast.com/Dashboard")]
    public class Service : IService
    {
        public ReturnData<String> GetDashboardByID(String dashboardId)
        {
            ReturnData<String> returnData = Comcast.GetDashboardByID(dashboardId);

            return returnData;
        }

        public ReturnData<List<UserDashboard>> GetUserDashboards(String accountName)
        {
            ReturnData<List<UserDashboard>> returnData = Comcast.GetUserDashboards(accountName);

            return returnData;
        }


        public ReturnData<UserDashboard> GetUserCookie(String accountName)
        {
            ReturnData<UserDashboard> returnData = Comcast.GetUserCookie(accountName);

            return returnData;
        }

        public ReturnData<String> SaveUserCookie(String accountName, String dashboardName, String cookie)
        {
            ReturnData<String> returnData = Comcast.SaveUserCookie(accountName, dashboardName, cookie);

            return returnData;
        }

        public ReturnData<String> RenameDashboard(String dashboardId, String dashboardName)
        {
            ReturnData<String> returnData = Comcast.RenameDashboard(dashboardId, dashboardName);

            return returnData;
        }


        public ReturnData<String> DeleteDashboard(String accountName, String dashboardId)
        {
            ReturnData<String> returnData = Comcast.DeleteDashboard(accountName, dashboardId);

            return returnData;
        }

        public ReturnData<String> SetDefaultDashboard(String accountName, String dashboardId)
        {
            ReturnData<String> returnData = Comcast.SetDefaultDashboard(accountName, dashboardId);

            return returnData;
        }

        public ReturnData<Stocks> GetComcastStock()
        {
            ReturnData<Stocks> returnData = Comcast.GetComcastStock();

            return returnData;
        }

        public ReturnData<List<CNArticle>> GetComcastNow()
        {
            ReturnData<List<CNArticle>> returnData = Comcast.GetComcastNow();

            return returnData;
        }

        public ReturnData<List<BrouhaRecord>> GetBrouhaData()
        {
            ReturnData<List<BrouhaRecord>> returnData = Brouha.GetBrouhaData();

            return returnData;
        }

        public ReturnData<List<HSDRecord>> GetHSDMetrics()
        {
            ReturnData<List<HSDRecord>> returnData = Comcast.GetHSDMetrics();

            return returnData;
        }

        public ReturnData<List<SubscriberCounts>> GetSubscriberCounts()
        {
            ReturnData<List<SubscriberCounts>> returnData = Comcast.GetSubscriberCounts();

            return returnData;
        }



        public ReturnData<List<HaystackItem>> GetHaystackCalls(string username, string password, string segment, string filter, string fromDate, string toDate)
        {
            ReturnData<List<HaystackItem>> returnData;

            CookieCollection cookies = Haystack.GetHaystackCookie(username, password);

            returnData = Haystack.GetHaystackCalls(cookies, username, password, segment, filter, fromDate, toDate);

            return returnData;
        }


        public ReturnData<List<HaystackItem>> GetHaystackTickets(string username, string password, string segment, string filter, string fromDate, string toDate)
        {
            ReturnData<List<HaystackItem>> returnData;

            CookieCollection cookies = Haystack.GetHaystackCookie(username, password);

            returnData = Haystack.GetHaystackTickets(cookies, username, password, segment, filter, fromDate, toDate);

            return returnData;
        }

        public ReturnData<List<HaystackItem>> GetHaystackTruckRolls(string username, string password, string segment, string filter, string fromDate, string toDate)
        {
            ReturnData<List<HaystackItem>> returnData;

            CookieCollection cookies = Haystack.GetHaystackCookie(username, password);

            returnData = Haystack.GetHaystackTruckRolls(cookies, username, password, segment, filter, fromDate, toDate);

            return returnData;
        }

        public ReturnData<List<HaystackSegmentRecord>> GetHaystackSegments()
        {
            ReturnData<List<HaystackSegmentRecord>> returnData;

            CookieCollection cookies = Haystack.GetHaystackCookie(null, null);

            returnData = Haystack.GetHaystackSegments(cookies);

            return returnData;
        }


        public ReturnData<List<UserFilter>> GetUserFilterList(String username, string password)
        {
            ReturnData<List<UserFilter>> returnData;

            CookieCollection cookies = Haystack.GetHaystackCookie(username, password);

            returnData = Haystack.GetUserFilterList(cookies, username);

            return returnData;
        }

        public ReturnData<List<HaystackDivision>> GetHaystackDivisions(String username)
        {
            ReturnData<List<HaystackDivision>> returnData;

            CookieCollection cookies = Haystack.GetHaystackCookie(null, null);

            returnData = Haystack.GetDivisionsList(cookies, username);

            return returnData;
        }

        public ReturnData<List<HaystackNode>> GetHaystackNodes(String username)
        {
            ReturnData<List<HaystackNode>> returnData;

            CookieCollection cookies = Haystack.GetHaystackCookie(null, null);

            returnData = Haystack.GetHaystackNodes(cookies, username);

            return returnData;
        }

        public ReturnData<List<Portlet>> GetPorletsList()
        {
            ReturnData<List<Portlet>> returnData = Comcast.GetPorletsList();

            return returnData;
        }

        public ReturnData<String> SaveUserPortlets(String accountName, String porletsList)
        {
            ReturnData<String> returnData = Comcast.SaveUserPortlets(accountName, porletsList);

            return returnData;
        }

        public ReturnData<String> GetUserPortlets(String accountName)
        {
            ReturnData<String> returnData = Comcast.GetUserPortlets(accountName);

            return returnData;
        }

        public ReturnData<List<SelectData>> GetSelectData(String accountName, int categoryId)
        {
            ReturnData<List<SelectData>> returnData = Comcast.GetSelectData(accountName, categoryId);

            return returnData;
        }


        public ReturnData<List<Ticket>> GetTickets()
        {
            ReturnData<List<Ticket>> returnData = Comcast.GetTickets();

            return returnData;
        }

        public ReturnData<Tickets> GetTicket(String ticketNumber)
        {
            ReturnData<Tickets> returnData = Comcast.GetTicket(ticketNumber);

            return returnData;
        }

        public ReturnData<String> UpdateTicket(String actualEndDate, String actualEndTime, String actualStartDate, String actualStartTime,
            String adjDurationMinutes, String alarmStartDate, String alarmStartTime, String causeDescription, String changeReason, String modifiedDate,
            String resolvedDate, String resolvedTime, String severity, String smId, String solutionDescription, String ticketId,
            String workingStartDate, String workingStartTime)
        {
            ReturnData<String> returnData = Comcast.UpdateTicket(actualEndDate, actualEndTime, actualStartDate, actualStartTime, adjDurationMinutes,
                alarmStartDate, alarmStartTime, causeDescription, changeReason, modifiedDate, resolvedDate, resolvedTime, severity, smId, solutionDescription,
                ticketId, workingStartDate, workingStartTime);

            return returnData;
        }

        public ReturnData<NetworkLensIncident> GetIncidentTicket(String ticketNumber)
        {
            ReturnData<NetworkLensIncident> returnData = Comcast.GetIncidentTicket(ticketNumber);

            return returnData;
        }

        public ReturnData<String> UpdateIncidentTicket(String ticketId, String editIncidentWeek, String editLensGroup, String editRootCauseGroup, String editIncidentSummary)
        {
            ReturnData<String> returnData = Comcast.UpdateIncidentTicket(ticketId, editIncidentWeek, editLensGroup, editRootCauseGroup, editIncidentSummary);

            return returnData;
        }

        public ReturnData<List<Incident>> GetIncidentTickets(String week)
        {
            ReturnData<List<Incident>> returnData = Comcast.GetIncidentTickets(week);

            return returnData;
        }

        public ReturnData<TicketDetailsGeneral> GetTicketDetailsGeneral(String ticketNumber)
        {
            ReturnData<TicketDetailsGeneral> returnData = Comcast.GetTicketDetailsGeneral(ticketNumber);

            return returnData;
        }

        public ReturnData<TicketDetailsAttributes> GetTicketDetailsAttributes(String ticketNumber)
        {
            ReturnData<TicketDetailsAttributes> returnData = Comcast.GetTicketDetailsAttributes(ticketNumber);

            return returnData;
        }

        public ReturnData<List<CustomerTicket>> GetCustomerTickets(String ticketNumber)
        {
            ReturnData<List<CustomerTicket>> returnData = Comcast.GetCustomerTickets(ticketNumber);

            return returnData;
        }

        public ReturnData<List<TicketDetailsAffected>> GetTicketDetailsAffected(String ticketNumber)
        {
            ReturnData<List<TicketDetailsAffected>> returnData = Comcast.GetTicketDetailsAffected(ticketNumber);

            return returnData;
        }

        public ReturnData<List<WorkLogTicket>> GetWorkLogTickets(String ticketNumber, String source)
        {
            ReturnData<List<WorkLogTicket>> returnData = Comcast.GetWorkLogTickets(ticketNumber, source);

            return returnData;
        }

        public ReturnData<TicketTimeline> GetTicketTimeline(String ticketNumber)
        {
            ReturnData<TicketTimeline> returnData = Comcast.GetTicketTimeline(ticketNumber);

            return returnData;
        }

        public ReturnData<String> AddUserFavoriteLink(String accountName, String description, String link, String group)
        {
            ReturnData<String> returnData = Comcast.AddUserFavoriteLink(accountName, description, link, group);

            return returnData;
        }

        public ReturnData<List<UserFavoriteLink>> GetUserFavoriteLinks(String accountName)
        {
            ReturnData<List<UserFavoriteLink>> returnData = Comcast.GetUserFavoriteLinks(accountName);

            return returnData;
        }

        public ReturnData<String> EditUserFavoriteLink(String linkId, String accountName, String description, String link, String group)
        {
            ReturnData<String> returnData = Comcast.EditUserFavoriteLink(linkId, accountName, description, link, group);

            return returnData;
        }

        public ReturnData<String> DeleteUserFavoriteLink(String linkId)
        {
            ReturnData<String> returnData = Comcast.DeleteUserFavoriteLink(linkId);

            return returnData;
        }

        public ReturnData<List<WorkLogNotification>> GetWorkLogNotifications(String ticketNumber, String eventType)
        {
            ReturnData<List<WorkLogNotification>> returnData = Comcast.GetWorkLogNotifications(ticketNumber, eventType);

            return returnData;
        }

        public ReturnData<List<SITicketTimeline>> GetSITicketTimeline(String ticketNumber)
        {
            ReturnData<List<SITicketTimeline>> returnData = Comcast.GetSITicketTimeline(ticketNumber);

            return returnData;
        }

        public ReturnData<List<ConflictTicket>> GetConflictTickets(String months)
        {
            ReturnData<List<ConflictTicket>> returnData = Comcast.GetConflictTickets(months);

            return returnData;
        }

        public ReturnData<List<UserFavoriteLink>> GetAllUserFavoriteLinks(String accountName)
        {
            ReturnData<List<UserFavoriteLink>> returnData = Comcast.GetAllUserFavoriteLinks(accountName);

            return returnData;
        }

        public ReturnData<String> SaveUserFavoriteLinks(String accountName, String selectionList)
        {
            ReturnData<String> returnData = Comcast.SaveUserFavoriteLinks(accountName, selectionList);

            return returnData;
        }

        public ReturnData<List<IncidentReview>> GetNetworkOperationsIncidentReview()
        {
            ReturnData<List<IncidentReview>> returnData = Comcast.GetNetworkOperationsIncidentReview();

            return returnData;
        }

        public ReturnData<List<WeeklyReviewMeasure>> GetWeeklyReviewMeasures(String lens, String lensGroup)
        {
            ReturnData<List<WeeklyReviewMeasure>> returnData = Comcast.GetWeeklyReviewMeasures(lens, lensGroup);

            return returnData;
        }

        public ReturnData<List<ServerMonitorItem>> GetServerJobs(String serverName)
        {
            ReturnData<List<ServerMonitorItem>> returnData = ServerMonitor.GetServerJobs(serverName);

            return returnData;
        }

        public ReturnData<List<CRTicketStatus>> GetCRTicketStatus()
        {
            ReturnData<List<CRTicketStatus>> returnData = ServerMonitor.GetCRTicketStatus();

            return returnData;
        }

        public ReturnData<List<URLMonitorItem>> GetURLMonitor()
        {
            ReturnData<List<URLMonitorItem>> returnData = ServerMonitor.GetURLMonitor();

            return returnData;
        }

        public ReturnData<List<ErrorStatus>> GetSubscriptionErrorStatus()
        {
            ReturnData<List<ErrorStatus>> returnData = ServerMonitor.GetSubscriptionErrorStatus();

            return returnData;
        }

        public ReturnData<List<TempDBLogItem>> GetTempDBLogStatus()
        {
            ReturnData<List<TempDBLogItem>> returnData = ServerMonitor.GetTempDBLogStatus();

            return returnData;
        }

        public ReturnData<List<ApplicationEventMonitorItem>> GetApplicationEventMonitor()
        {
            ReturnData<List<ApplicationEventMonitorItem>> returnData = ServerMonitor.GetApplicationEventMonitor();

            return returnData;
        }

        public ReturnData<List<CRTicketsItem>> GetCRTickets()
        {
            ReturnData<List<CRTicketsItem>> returnData = ServerMonitor.GetCRTickets();

            return returnData;
        }

        public ReturnData<List<IVRCDRCall>> GetIVRCDRCalls()
        {
            ReturnData<List<IVRCDRCall>> returnData = ServerMonitor.GetIVRCDRCalls();

            return returnData;
        }

        public ReturnData<List<CRTicketDisplayItem>> GetCRTicketDelayDisplay()
        {
            ReturnData<List<CRTicketDisplayItem>> returnData = ServerMonitor.GetCRTicketDelayDisplay();

            return returnData;
        }

        public ReturnData<List<FCCThresholdBreachedItem>> GetFCCThresholdBreachedData()
        {
            ReturnData<List<FCCThresholdBreachedItem>> returnData = Comcast.GetFCCThresholdBreachedData();

            return returnData;
        }

        public ReturnData<List<ServerProcessesItem>> GetServerProcesses(String serverName)
        {
            ReturnData<List<ServerProcessesItem>> returnData = ServerMonitor.GetServerProcesses(serverName);

            return returnData;
        }

        public ReturnData<String> KillProcess(String serverName, String sessionId, Boolean statusOnly)
        {
            ReturnData<String> returnData = ServerMonitor.KillProcess(serverName, sessionId, statusOnly);

            return returnData;
        }

        public ReturnData<List<Server>> GetServersList(Boolean hasMonitoredProcesses, Boolean hasLongRunningJobs)
        {
            ReturnData<List<Server>> returnData = ServerMonitor.GetServersList(hasMonitoredProcesses, hasLongRunningJobs);

            return returnData;
        }

        public ReturnData<String> AddServer(String serverName, Boolean hasMonitoredProcesses, Boolean hasLongRunningJobs)
        {
            ReturnData<String> returnData = ServerMonitor.AddServer(serverName, hasMonitoredProcesses, hasLongRunningJobs);

            return returnData;
        }

        public ReturnData<String> EditServer(String id, String serverName, Boolean hasMonitoredProcesses, Boolean hasLongRunningJobs)
        {
            ReturnData<String> returnData = ServerMonitor.EditServer(id, serverName, hasMonitoredProcesses, hasLongRunningJobs);

            return returnData;
        }

        public ReturnData<String> DeleteServer(String id)
        {
            ReturnData<String> returnData = ServerMonitor.DeleteServer(id);

            return returnData;
        }

        public ReturnData<List<ServerCPUUsage>> GetServerCPUUsage(String serverName)
        {
            ReturnData<List<ServerCPUUsage>> returnData = ServerMonitor.GetServerCPUUsage(serverName);

            return returnData;
        }

        public ReturnData<List<MaintenanceResolveStatus>> GetMaintanceResoveStatus()
        {
            ReturnData<List<MaintenanceResolveStatus>> returnData = Comcast.GetMaintanceResoveStatus();

            return returnData;
        }

        public ReturnData<List<MaintenanceCausedSev1MonthOverMonth>> GetMaintanceCausedSev1MonthOverMonth()
        {
            ReturnData<List<MaintenanceCausedSev1MonthOverMonth>> returnData = Comcast.GetMaintanceCausedSev1MonthOverMonth();

            return returnData;
        }


        public ReturnData<List<MaintenanceResolveStatus>> GetMaintanceCausedSuccessFailByMonth(String month, String year)
        {
            ReturnData<List<MaintenanceResolveStatus>> returnData = Comcast.GetMaintanceCausedSuccessFailByMonth(month, year);

            return returnData;
        }

        public ReturnData<List<Sev1CausedByMaintDetail>> GetSev1CausedByMaintDetails(String directorName, String month, String year, String reportLevel)
        {
            ReturnData<List<Sev1CausedByMaintDetail>> returnData = Comcast.GetSev1CausedByMaintDetails(directorName, month, year, reportLevel);

            return returnData;
        }

        public ReturnData<SMTicketGeneralDetails> GetSMTicketGeneralDetails(String ticketNumber)
        {
            ReturnData<SMTicketGeneralDetails> returnData = Comcast.GetSMTicketGeneralDetails(ticketNumber);

            return returnData;
        }

        public ReturnData<List<SMTicketAffectedElement>> GetSMTicketAffectedElements(String ticketNumber)
        {
            ReturnData<List<SMTicketAffectedElement>> returnData = Comcast.GetSMTicketAffectedElements(ticketNumber);

            return returnData;
        }

        public ReturnData<List<WorkLogTicket>> GetSMTicketWorkLog(String ticketNumber, String source)
        {
            ReturnData<List<WorkLogTicket>> returnData = Comcast.GetSMTicketWorkLog(ticketNumber, source);

            return returnData;
        }

        public ReturnData<SRTicketGeneralDetails> GetSRTicketGeneralDetails(String ticketNumber)
        {
            ReturnData<SRTicketGeneralDetails> returnData = Comcast.GetSRTicketGeneralDetails(ticketNumber);

            return returnData;
        }

        public ReturnData<List<SRTicketAttribute>> GetSRTicketAttributes(String ticketNumber)
        {
            ReturnData<List<SRTicketAttribute>> returnData = Comcast.GetSRTicketAttributes(ticketNumber);

            return returnData;
        }

        public ReturnData<List<WorkLogTicket>> GetSRTicketWorkLog(String ticketNumber, String source)
        {
            ReturnData<List<WorkLogTicket>> returnData = Comcast.GetSRTicketWorkLog(ticketNumber, source);

            return returnData;
        }

        public ReturnData<CMTicketGeneralDetails> GetCMTicketGeneralDetails(String ticketNumber)
        {
            ReturnData<CMTicketGeneralDetails> returnData = Comcast.GetCMTicketGeneralDetails(ticketNumber);

            return returnData;
        }

        public ReturnData<List<CMTicketContactDetail>> GetCMContactDetails(String ticketNumber)
        {
            ReturnData<List<CMTicketContactDetail>> returnData = Comcast.GetCMContactDetails(ticketNumber);

            return returnData;
        }

        public ReturnData<List<CMTicketAttribute>> GetCMTicketAttributes(String ticketNumber)
        {
            ReturnData<List<CMTicketAttribute>> returnData = Comcast.GetCMTicketAttributes(ticketNumber);

            return returnData;
        }

        public ReturnData<List<CMTicketStep>> GetCMTicketSteps(String ticketNumber)
        {
            ReturnData<List<CMTicketStep>> returnData = Comcast.GetCMTicketSteps(ticketNumber);

            return returnData;
        }

        public ReturnData<List<CMTicketAffectedElement>> GetCMTicketAffectedElements(String ticketNumber)
        {
            ReturnData<List<CMTicketAffectedElement>> returnData = Comcast.GetCMTicketAffectedElements(ticketNumber);

            return returnData;
        }

        public ReturnData<List<CMTicketWorkLog>> GetCMTicketWorkLog(String ticketNumber, String source)
        {
            ReturnData<List<CMTicketWorkLog>> returnData = Comcast.GetCMTicketWorkLog(ticketNumber, source);

            return returnData;
        }

        public ReturnData<CRTicketGeneralDetails> GetCRTicketGeneralDetails(String ticketNumber)
        {
            ReturnData<CRTicketGeneralDetails> returnData = Comcast.GetCRTicketGeneralDetails(ticketNumber);

            return returnData;
        }

        public ReturnData<List<CRTicketAttribute>> GetCRTicketAttributes(String ticketNumber)
        {
            ReturnData<List<CRTicketAttribute>> returnData = Comcast.GetCRTicketAttributes(ticketNumber);

            return returnData;
        }

        public ReturnData<List<CRTicketWorkLog>> GetCRTicketWorkLog(String ticketNumber, String source)
        {
            ReturnData<List<CRTicketWorkLog>> returnData = Comcast.GetCRTicketWorkLog(ticketNumber, source);

            return returnData;
        }

        public ReturnData<SITicketGeneralDetails> GetSITicketGeneralDetails(String ticketNumber)
        {
            ReturnData<SITicketGeneralDetails> returnData = Comcast.GetSITicketGeneralDetails(ticketNumber);

            return returnData;
        }

        public ReturnData<List<SITicketAttribute>> GetSITicketAttributes(String ticketNumber)
        {
            ReturnData<List<SITicketAttribute>> returnData = Comcast.GetSITicketAttributes(ticketNumber);

            return returnData;
        }

        public ReturnData<List<SITicketCustomerTicket>> GetSITicketCustomerTickets(String ticketNumber)
        {
            ReturnData<List<SITicketCustomerTicket>> returnData = Comcast.GetSITicketCustomerTickets(ticketNumber);

            return returnData;
        }

        public ReturnData<List<SITicketAffectedElement>> GetSITicketAffectedElements(String ticketNumber)
        {
            ReturnData<List<SITicketAffectedElement>> returnData = Comcast.GetSITicketAffectedElements(ticketNumber);

            return returnData;
        }

        public ReturnData<List<SITicketWorkLog>> GetSITicketWorkLog(String ticketNumber, String source)
        {
            ReturnData<List<SITicketWorkLog>> returnData = Comcast.GetSITicketWorkLog(ticketNumber, source);

            return returnData;
        }

        public ReturnData<OutageElement> GetOETicketGeneralDetails(String ticketNumber)
        {
            ReturnData<OutageElement> returnData = Comcast.GetOETicketGeneralDetails(ticketNumber);

            return returnData;
        }

        public ReturnData<OETicketAffectedElement> GetOETicketAffectedElements(String ticketNumber)
        {
            ReturnData<OETicketAffectedElement> returnData = Comcast.GetOETicketAffectedElements(ticketNumber);

            return returnData;
        }

        public ReturnData<List<OETicketAttribute>> GetOETicketAttributes(String ticketNumber)
        {
            ReturnData<List<OETicketAttribute>> returnData = Comcast.GetOETicketAttributes(ticketNumber);

            return returnData;
        }

        public ReturnData<List<OETicketWorkLog>> GetOETicketWorkLog(String ticketNumber, String source)
        {
            ReturnData<List<OETicketWorkLog>> returnData = Comcast.GetOETicketWorkLog(ticketNumber, source);

            return returnData;
        }


        public List<CMTreeMapRoot> GetCMTreeView(String name, String month, String year)
        {
            List<CMTreeMapRoot> returnData = Comcast.GetCMTreeView(name, month, year);

            return returnData;
        }


        public ReturnData<List<CMDetailByHeirarchy>> GetCMDetailByHierarchy(String userName, String month, String year, String reportLevel)
        {
            ReturnData<List<CMDetailByHeirarchy>> returnData = Comcast.GetCMDetailByHierarchy(userName, month, year, reportLevel);

            return returnData;
        }

        public ReturnData<List<SAPUser>> GetSAPUser(String name, String level, String unit)
        {
            ReturnData<List<SAPUser>> returnData = Comcast.GetSAPUser(name, level, unit);

            return returnData;
        }

        public ReturnData<List<SAPSubordinate>> GetSAPSubordinates(String name)
        {
            ReturnData<List<SAPSubordinate>> returnData = Comcast.GetSAPSubordinates(name);

            return returnData;
        }

        public ReturnData<List<ActiveTicket>> GetActiveTickets(String severity)
        {
            ReturnData<List<ActiveTicket>> returnData = Comcast.GetActiveTickets(severity);

            return returnData;
        }

        public ReturnData<String> SaveHaystackFilter(String accountName, String userName, String password, String filterName, String division, String nodes)
        {
            ReturnData<String> returnData;

            CookieCollection cookies = Haystack.GetHaystackCookie(userName, password);

            returnData = Haystack.SaveHaystackFilter(cookies, accountName, filterName, division, nodes);

            return returnData;
        }

        public ReturnData<List<HaystackBreakDownCall>> GetHaystackBinsByBreakdownCalls(String breakdown, String segment, String filter, String username, String password, Boolean showOthers, String fromDate, String toDate)
        {
            ReturnData<List<HaystackBreakDownCall>> returnData;

            CookieCollection cookies = Haystack.GetHaystackCookie(username, password);

            returnData  = Haystack.GetHaystackBinsByBreakdownCalls(cookies, breakdown, segment, filter, username, password, showOthers, fromDate, toDate);

            return returnData;
        }

        public ReturnData<List<HaystackBreakDownCall>> GetHaystackBinsByBreakdownTickets(String breakdown, String segment, String filter, String username, String password, Boolean showOthers, String fromDate, String toDate)
        {
            ReturnData<List<HaystackBreakDownCall>> returnData;

            CookieCollection cookies = Haystack.GetHaystackCookie(username, password);

            returnData = Haystack.GetHaystackBinsByBreakdownTickets(cookies, breakdown, segment, filter, username, password, showOthers, fromDate, toDate);

            return returnData;
        }

        public ReturnData<List<HaystackBreakDownCall>> GetHaystackBinsByBreakdownTruckRolls(String breakdown, String segment, String filter, String username, String password, Boolean showOthers, String fromDate, String toDate)
        {
            ReturnData<List<HaystackBreakDownCall>> returnData;

            CookieCollection cookies = Haystack.GetHaystackCookie(username, password);

            returnData = Haystack.GetHaystackBinsByBreakdownTruckRolls(cookies, breakdown, segment, filter, username, password, showOthers, fromDate, toDate);

            return returnData;
        }


        public ReturnData<List<SIEventFunnel>> GetSIEventTunnel(String ticketId)
        {
            ReturnData<List<SIEventFunnel>> returnData = Comcast.GetSIEventTunnel(ticketId);

            return returnData;
        }


        public string TestMe()
        {
            return System.Security.Principal.WindowsIdentity.GetCurrent().Name.ToString();
        }



        public string Test(int value)
        {

            return string.Format("You entered: {0}", value);
        }
    }
}


    
