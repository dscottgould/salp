﻿//
//
//	Data.js
//
//


// Set Namespaces
SALP = new Object();
SALP.data = new Object();

//
// Data Stores
//


//SALP.data.selectChart08 = Ext.create('Ext.data.Store', 
//{
//    fields: ['displayName', 'className'],
//    data : [
//			{"displayName":"Comcast Now", "className":"SALP.portlets.ComcastNowPortlet"}
//    ]
//});
//



SALP.data.selectPage = Ext.create('Ext.data.Store', 
{
    fields: ['displayName', 'value', 'id'],
    data : [
			{"displayName":"Page 01", "value": 0, id: 0}
    ]
});



SALP.data.selectSegment = new Object();
SALP.data.selectSegment.model =  Ext.define('selectSegment', 
{
	extend: 'Ext.data.Model',
	fields: [
		{name: 'name', type: 'string'},
		{name: 'value', type: 'string'}
]});

SALP.data.selectSegment.store = Ext.create('Ext.data.Store', 
	{
	autoLoad: true,
	model: SALP.data.selectSegment.model,
	proxy: 
	{
		type: 'ajax',
		url: 'WebServices/Service.svc/GetHaystackSegments',
		pageParam: undefined,
   	startParam: undefined,
   	limitParam: undefined,
		reader: 
		{
			type: 'json',
  		root: 'data'
		}
	}
});

SALP.data.selectFilter = new Object();
SALP.data.selectFilter.model =  Ext.define('selectSegment', 
{
	extend: 'Ext.data.Model',
	fields: [
		{name: 'userFilter', type: 'string'}
]});

SALP.data.selectFilter.store = Ext.create('Ext.data.Store', 
	{
	autoLoad: true,
	model: SALP.data.selectFilter.model,
	proxy: 
	{
		type: 'ajax',
		url: 'WebServices/Service.svc/GetUserFilterList',
		pageParam: undefined,
   	startParam: undefined,
   	limitParam: undefined,
		reader: 
		{
			type: 'json',
  		root: 'data'
		}
	}
});


SALP.data.ComcastNow = new Object();

SALP.data.ComcastNow.model =  Ext.define('ComcastNow', 
{
	extend: 'Ext.data.Model',
	fields: [
		{name: 'title', type: 'string'},
		{name: 'url', type: 'string'},
		{name: 'tags', type: 'auto'},
		{name: 'publishedDate', type: 'date'}]  
});

SALP.data.ComcastNow.store = Ext.create('Ext.data.Store', 
{
	autoLoad: true,
	model:SALP.data.ComcastNow.model,
	proxy: 
	{
		type: 'ajax',
		url: 'WebServices/Service.svc/GetComcastNow',
		pageParam: undefined,
		startParam: undefined,
		limitParam: undefined,
		reader: 
		{
			type: 'json',
			root: 'data'
		}
	}
});	

SALP.data.Brouha = new Object();

SALP.data.Brouha.model =  Ext.define('Brouha', 
{
	extend: 'Ext.data.Model',
	fields: [
		{name: 'id', type: 'string'},
		{name: 'title', type: 'string'},
		{name: 'severity', type: 'string'},
		{name: 'createDate', type: 'date'}
]});

SALP.data.Brouha.store = Ext.create('Ext.data.Store', 
{
	autoLoad: true,
	model: SALP.data.Brouha.model,
	proxy: 
	{
  	type: 'ajax',
   	url: 'WebServices/Service.svc/GetBrouhaData',
   	pageParam: undefined,
		startParam: undefined,
		limitParam: undefined,
   	reader: 
   	{
    	type: 'json',
      root: 'data'
   	}
	}
});


SALP.data.Dashboards = new Object();

SALP.data.Dashboards.model =  Ext.define('Dashboard', 
{
	extend: 'Ext.data.Model',
	fields: [
		{name: 'dashboardId', type: 'string'},
		{name: 'dashboardName', type: 'string'}
		//{name: 'cookieValue', type: 'string'}
]});

SALP.data.Dashboards.store = Ext.create('Ext.data.Store', 
{
	//autoLoad: true,
	model: SALP.data.Dashboards.model,
	proxy: 
	{
  	type: 'ajax',
   	url: 'WebServices/Service.svc/GetUserDashboards',
//   	extraParams:
//   	{
//   		accountName: SALP.user.accountName
//   	},
   	pageParam: undefined,
		startParam: undefined,
		limitParam: undefined,
   	reader: 
   	{
    	type: 'json',
      root: 'data'
   	}
	}
});

SALP.data.Portlets = new Object();

SALP.data.Portlets.model =  Ext.define('Portlet', 
{
	extend: 'Ext.data.Model',
	fields: [
		{name: 'id', type: 'string'},
		{name: 'displayName', type: 'string'},
		{name: 'objectClass', type: 'string'},
		{name: 'category', type: 'string'}
]});

SALP.data.Portlets.store = Ext.create('Ext.data.Store', 
{
	autoLoad: true,
	model: SALP.data.Portlets.model,
	proxy: 
	{
  	type: 'ajax',
   	url: 'WebServices/Service.svc/GetPorletsList',
//   	extraParams:
//   	{
//   		accountName: SALP.user.accountName
//   	},
   	pageParam: undefined,
		startParam: undefined,
		limitParam: undefined,
   	reader: 
   	{
    	type: 'json',
      root: 'data'
   	}
	}
});


SALP.data.OldTicketModel = Ext.define('OldTicket', 
{
	extend: 'Ext.data.Model',
  fields: [
  	{name: 'ticketId',  type: 'string'},
    {name: 'calTTR',   type: 'string'},
    {name: 'smRelatedTicket', type: 'string'},
    {name: 'causeDescription', type: 'string'},
    {name: 'solutionDescription', type: 'string'},
    {name: 'severity',  type: 'string'},
    
    {name: 'alarmStartDate',  type: 'string'},
    {name: 'workingStartDate',  type: 'string'},
    {name: 'resolvedDate',  type: 'string'},
    {name: 'actualStartDate',  type: 'string'},
    {name: 'actualEndDate',  type: 'string'},
    
    {name: 'modifiedDate',  type: 'string'}
  ]
});



SALP.data.NewTicketModel = Ext.define('NewTicket', 
{
	extend: 'Ext.data.Model',
  fields: [
  	{name: 'ticketId',  type: 'string'},
    {name: 'adjDurationMinutes',   type: 'string'},
    {name: 'causeDescription', type: 'string'},
    {name: 'solutionDescription', type: 'string'},
    {name: 'smId',  type: 'string'},
    {name: 'severity',  type: 'string'},
    
    {name: 'alarmStartDate',  type: 'date'},
    {name: 'alarmStartTime',  type: 'date'},
    
    {name: 'workingStartDate',  type: 'date'},
    {name: 'workingStartTime',  type: 'date'},
    
    {name: 'resolvedDate',  type: 'date'},
    {name: 'resolvedTime',  type: 'date'},
    
    {name: 'actualStartDate',  type: 'date'},
    {name: 'actualStartTime',  type: 'date'},
    
    {name: 'actualEndDate',  type: 'date'},
    {name: 'actualEndTime',  type: 'date'},
    
    {name: 'changeReason',  type: 'string'},
    {name: 'modifiedDate',  type: 'string'}
  ]
});



SALP.data.CauseDecriptions = Ext.create('Ext.data.Store', 
{
    fields: ['displayName', 'submitValue'],
    data : [
        {"displayName":"Failed/Degraded Hardware", "submitValue":"Failed/Degraded Hardware"},
        {"displayName":"Hardware", "submitValue":"Hardware"},
        {"displayName":"Configuration Error", "submitValue":"Configuration Error"},
        {"displayName":"Facility Issue", "submitValue":"Facility Issue"},
        {"displayName":"Software Bug", "submitValue":"Software Bug"}
    ]
});


SALP.data.Severity = Ext.create('Ext.data.Store', 
{
    fields: ['displayName', 'submitValue'],
    data : [
        {"displayName":"SEV 1", "submitValue":"SEV 1"},
        {"displayName":"SEV 2", "submitValue":"SEV 2"},
        {"displayName":"SEV 3", "submitValue":"SEV 3"},
        {"displayName":"Informational", "submitValue":"Informational"}
    ]
});

SALP.data.IncidentTicketAModel = Ext.define('IncidentTicketA', 
{
	extend: 'Ext.data.Model',
  fields: [
  	{name: 'ticketId',  type: 'string'},
  	{name: 'incidentWeek', type: 'date'},
    {name: 'networkLens', type: 'string'},
    {name: 'lensGroup', type: 'string'},
    {name: 'rootCauseGroup', type: 'string'},
    {name: 'actualStartDate',  type: 'string'},
    {name: 'alarmStartDate',  type: 'string'},
    {name: 'workingDate',  type: 'string'},
    {name: 'resolvedDate',  type: 'string'},
    {name: 'causeCategory',  type: 'string'},
    {name: 'causeDescription',  type: 'string'},
    {name: 'solutionCategory',  type: 'string'},
    {name: 'solutionDescription',  type: 'string'},
    {name: 'problemCategory',  type: 'string'},
    {name: 'problemSummary',  type: 'string'},
    {name: 'fixemIncidentSummary',  type: 'string'},
    {name: 'detectedBy',  type: 'string'},
    {name: 'source',  type: 'string'}
  ]
});

SALP.data.IncidentTicketBModel = Ext.define('IncidentTicketB', 
{
	extend: 'Ext.data.Model',
  fields: [
  	{name: 'ticketId',  type: 'string'},
    {name: 'editLensGroup',  type: 'string'},
    {name: 'editRootCauseGroup',  type: 'string'},
    {name: 'editIncidentSummary',  type: 'string'},
    {name: 'editIncidentWeek',   type: 'date'}
  ]
});

SALP.data.LensGroup = Ext.create('Ext.data.Store', 
{
    fields: ['displayName', 'submitValue'],
    data : [
        {"displayName":"Facilities/Transport/Power", "submitValue":"Facilities/Transport/Power"},
        {"displayName":"IP(Switching/Routing)", "submitValue":"IP(Switching/Routing)"},
        {"displayName":"Fiber Cuts", "submitValue":"Fiber Cuts"}
    ]
});

SALP.data.RootCause = Ext.create('Ext.data.Store', 
{
    fields: ['displayName', 'submitValue'],
    data : [
        {"displayName":"HW/SW", "submitValue":"HW/SW"},
        {"displayName":"Config", "submitValue":"Config"},
        {"displayName":"Execution Defect", "submitValue":"Execution Defect"},
        {"displayName":"As Design", "submitValue":"As Design"},
    ]
});


SALP.data.Incidents = new Object();

SALP.data.Incidents.model =  Ext.define('Incident', 
{
	extend: 'Ext.data.Model',
	fields: [
		{name: 'ttsId', type: 'string'},
		{name: 'networkLens', type: 'string'},
		{name: 'lensGroup', type: 'string'},
		{name: 'rootCauseGroup', type: 'string'},
		{name: 'maintMismatch', type: 'string'},
		{name: 'ttsChangeRelated', type: 'string'},
		{name: 'fixEmChangeRelated', type: 'string'},
		{name: 'ttActualStart', type: 'string'},
		{name: 'defectTimeIssue', type: 'string'},
		{name: 'detect', type: 'string'},
		{name: 'ttAlarmStart', type: 'string'},
		{name: 'engageTimeIssue', type: 'string'},
		{name: 'engage', type: 'string'},
		{name: 'ttsWorking', type: 'string'},
		{name: 'resolvedTimeIssue', type: 'string'},
		{name: 'resolve', type: 'string'},
		{name: 'ttActualEnd', type: 'string'},
		{name: 'ttCauseDescription', type: 'string'},
		{name: 'ttSolutionDescription', type: 'string'},
		{name: 'ttProblemSummary', type: 'string'},
		{name: 'incidentSummary', type: 'string'},
		{name: 'siUserImpactCount', type: 'string'}
]});


SALP.data.IncidentTickets = Ext.create('Ext.data.Store', 
{
    fields: ['displayName', 'submitValue'],
    data : [
        {"displayName":"This Week", "submitValue":"this"},
        {"displayName":"Last Week", "submitValue":"last"},
        {"displayName":"2 Weeks Ago", "submitValue":"2weeks"}
    ]
});


SALP.data.TicketDetails = new Object();

SALP.data.TicketDetails.modelA =  Ext.define('TicketDetailsA', 
{
	extend: 'Ext.data.Model',
	fields: [
		{name: 'ticketId', type: 'string'},
		{name: 'incidentSummary', type: 'string'},
		{name: 'supportAreaName', type: 'string'},
		{name: 'submittingName', type: 'string'},
		{name: 'queueName', type: 'string'},
		{name: 'fullName', type: 'string'},
		{name: 'siSeverity', type: 'string'},
		{name: 'siStatus', type: 'string'},
		{name: 'siPriority', type: 'string'},
		{name: 'priority', type: 'string'},
		{name: 'product', type: 'string'},
		{name: 'createDate', type: 'string'},
		{name: 'assignedDate', type: 'string'},
		{name: 'telephonyAffected', type: 'string'},
		{name: 'videoAffected', type: 'string'},
		{name: 'hsdAffected', type: 'string'},
		{name: 'totalCustomersAffected', type: 'string'},
		{name: 'nextAction', type: 'string'},
		{name: 'problemCode', type: 'string'},
		{name: 'source', type: 'string'},
		{name: 'category', type: 'string'},
		{name: 'subCategory', type: 'string'},
		{name: 'causeCode', type: 'string'},
		{name: 'causeDescription', type: 'string'},
		{name: 'causeCategory', type: 'string'},
		{name: 'causeSubCategory', type: 'string'},
		{name: 'solutionCode', type: 'string'},
		{name: 'solutionDescription', type: 'string'},
		{name: 'solutionCategory', type: 'string'},
		{name: 'solutionSubCategory', type: 'string'},
		{name: 'externalAssignee', type: 'string'},
		{name: 'externalReference', type: 'string'},
		{name: 'alarmDate', type: 'string'},
		{name: 'ttrStart', type: 'string'},
		{name: 'ttrStop', type: 'string'},
		{name: 'indicatorCity', type: 'string'},
		{name: 'indicatorState', type: 'string'},
		{name: 'networkElement', type: 'string'},
		{name: 'correlationType', type: 'string'},
		{name: 'serviceCondition', type: 'string'},
		{name: 'careImpact', type: 'string'},
		{name: 'estimatedServiceRestore', type: 'string'},
		{name: 'siLastModifiedBy', type: 'string'},
		{name: 'lastModifiedBy', type: 'string'},
		{name: 'modifiedDate', type: 'string'},
		{name: 'siResolvedBy', type: 'string'},
		{name: 'resolvedBy', type: 'string'},
		{name: 'contactDepartment', type: 'string'},
		{name: 'contactPhone', type: 'string'},
		{name: 'regionName', type: 'string'},
		{name: 'duration', type: 'string'},
		{name: 'detectedBy', type: 'string'},
		{name: 'cicGroupId', type: 'string'}
]});


SALP.data.TicketDetails.modelB =  Ext.define('TicketDetailsB', 
{
	extend: 'Ext.data.Model',
	fields: [
		{name: 'contactInfoDepartment', type: 'string'},
		{name: 'contactInfoPhone', type: 'string'},
		{name: 'indicatorState', type: 'string'},
		{name: 'managementBridge', type: 'string'},
		{name: 'detectedBy', type: 'string'},
		{name: 'outageDescription', type: 'string'},
		{name: 'customerExperience', type: 'string'},
		{name: 'outageDescription', type: 'string'},
		{name: 'technicalBridge', type: 'string'},
		{name: 'indicatorCity', type: 'string'}
]});



SALP.data.CustomerTickets = new Object();

SALP.data.CustomerTickets.model =  Ext.define('CustomerTickets', 
{
	extend: 'Ext.data.Model',
	fields: [
		{name: 'customerTicketId', type: 'string'},
		{name: 'problemSummary', type: 'string'},
		{name: 'createDate', type: 'string'},
		{name: 'contactName', type: 'string'},
		{name: 'status', type: 'string'},
		{name: 'node', type: 'string'}
]});


SALP.data.DetailsAffected = new Object();

SALP.data.DetailsAffected.model =  Ext.define('DetailsAffected', 
{
	extend: 'Ext.data.Model',
	fields: [
		{name: 'entryId', type: 'string'},
		{name: 'serviceAffected', type: 'string'},
		{name: 'elementName', type: 'string'},
		{name: 'elementType', type: 'string'},
		{name: 'systemName', type: 'string'},
		{name: 'telephonyAffectedSubscriptions', type: 'string'},
		{name: 'videoAffectedSubscriptions', type: 'string'},
		{name: 'hsdAffectedSubscriptions', type: 'string'},
		{name: 'totalAffectedSubscriptions', type: 'string'},
		{name: 'aeStatus', type: 'string'},
		{name: 'actualStart', type: 'string'},
		{name: 'actualEnd', type: 'string'}
]});



SALP.data.WorkLog = new Object()

SALP.data.WorkLog.model =  Ext.define('WorkLog', 
{
	extend: 'Ext.data.Model',
	fields: [
		{name: 'ticketNumber', type: 'string'},
		{name: 'createDate', type: 'string'},
		{name: 'submitter', type: 'string'},
		{name: 'source', type: 'string'},
		{name: 'subject', type: 'string'},
		{name: 'details', type: 'string'}
]});


SALP.data.WorkLog.select = Ext.create('Ext.data.Store', 
{
    fields: ['displayName', 'submitValue'],
    data : [
        {"displayName":"None", "submitValue":"none"},
        {"displayName":"All Sources", "submitValue":"%"},
        {"displayName":"ARCHER", "submitValue":"ARCHER%"},
        {"displayName":"Assignement", "submitValue":"Assignement%"},
        {"displayName":"Auspice", "submitValue":"Auspice%"},
        {"displayName":"Categorization", "submitValue":"Categorization%"},
        {"displayName":"Cause", "submitValue":"Cause%"},
        {"displayName":"Dispatch Order", "submitValue":"Dispatch Order%"},
        {"displayName":"External", "submitValue":"External%"},
        {"displayName":"Manual", "submitValue":"Manual%"},
        {"displayName":"Network Tech", "submitValue":"Network Tech%"},
        {"displayName":"Notification", "submitValue":"Notification%"},
        {"displayName":"Priority", "submitValue":"Priority%"},
        {"displayName":"RMA", "submitValue":"RMA%"},
        {"displayName":"RX-Correlation Summary", "submitValue":"RX-Correlation Summary%"},
        {"displayName":"RX-Resolution", "submitValue":"RX-Resolution%"},
        {"displayName":"satort6656", "submitValue":"satort6656%"},
        {"displayName":"Severity", "submitValue":"Severity%"},
        {"displayName":"Solution", "submitValue":"Solution%"},
        {"displayName":"Status", "submitValue":"Status%"},
        {"displayName":"Ticket Link", "submitValue":"Ticket Link%"},
        {"displayName":"Ticket Monitored", "submitValue":"Ticket Monitored%"},
        {"displayName":"TICKET UNLIMITED", "submitValue":"TICKET UNLIMITED%"}
    ]
});



SALP.data.chart = new Object();
SALP.data.chart.siTicketTimline = new Object();

SALP.data.chart.siTicketTimline.store = Ext.create('Ext.data.Store', 
{
  fields: ['name', 'start', 'end'],
  data: [
//   			{"name": "Alarm Time", "start": 4, "end": 8},
//	   		{"name": "Alarm Time2", "start": 5, "end": 6},
  
    		{"name": "Time To Detect", "start": new Date('10/19/2016 8:56:38 AM'), "end": new Date('10/19/2016 8:57:00 AM')},
       	{"name": "Time To Engage", "start": new Date('10/19/2016 8:57:00 AM'), "end": new Date('10/19/2017 9:10:00 AM')}
      
  ]
});


SALP.data.FavoriteLinks = new Object()

SALP.data.FavoriteLinks.model =  Ext.define('FavoriteLinks', 
{
	extend: 'Ext.data.Model',
	fields: [
		{name: 'linkId', type: 'string'},
		{name: 'description', type: 'string'},
		{name: 'link', type: 'string'},
		{name: 'group', type: 'string'},
		{name: 'display', type: 'string'}
]});

SALP.data.FavoriteLinks.store = Ext.create('Ext.data.Store', 
{
	//autoLoad: true,
	model: SALP.data.FavoriteLinks.model,
	proxy: 
	{
  	type: 'ajax',
   	url: 'WebServices/Service.svc/GetUserFavoriteLinks',
//   	extraParams:
//   	{
//	  	accountName: SALP.user.accountName
//   	},
   	pageParam: undefined,
		startParam: undefined,
		limitParam: undefined,
   	reader: 
   	{
    	type: 'json',
      root: 'data'
   	}
	}
});

SALP.data.AllFavoriteLinks = new Object();

SALP.data.AllFavoriteLinks.store = Ext.create('Ext.data.Store', 
{
	//autoLoad: true,
	model: SALP.data.FavoriteLinks.model,
	proxy: 
	{
  	type: 'ajax',
   	url: 'WebServices/Service.svc/GetAllUserFavoriteLinks',
//   	extraParams:
//   	{
//	  	accountName: SALP.user.accountName
//   	},
   	pageParam: undefined,
		startParam: undefined,
		limitParam: undefined,
   	reader: 
   	{
    	type: 'json',
      root: 'data'
   	}
	}
});



SALP.data.SITimeline = new Object();

SALP.data.SITimeline.model =  Ext.define('SITimeline', 
{
	extend: 'Ext.data.Model',
	fields: [
		{name: 'name', type: 'string'},
		{name: 'start', type: 'string'},
		{name: 'duration', type: 'string'},
		{name: 'durationNeg', type: 'string'}
]});


SALP.data.ConflictTicket = new Object();

SALP.data.ConflictTicket.model =  Ext.define('SITimeline', 
{
	extend: 'Ext.data.Model',
	fields: [
		{name: 'submitter', type: 'string'},
		{name: 'submitterName', type: 'string'},
		{name: 'bypassed', type: 'string'},
		{name: 'notBypassed', type: 'string'},
		{name: 'total', type: 'string'},
		{name: 'percentNotBypassed', type: 'string'}
]});




SALP.data.ConflictTicket.select = Ext.create('Ext.data.Store', 
{
    fields: ['displayName', 'submitValue'],
    data : [
        {"displayName":"January", "submitValue":"January"},
        {"displayName":"February", "submitValue":"February"},
        {"displayName":"March", "submitValue":"March"},
        {"displayName":"April", "submitValue":"April"},
        {"displayName":"May", "submitValue":"May"},
        {"displayName":"June", "submitValue":"June"},
        {"displayName":"July", "submitValue":"July"},
        {"displayName":"August", "submitValue":"August"},
        {"displayName":"September", "submitValue":"September"},
        {"displayName":"October", "submitValue":"October"},
        {"displayName":"November", "submitValue":"November"},
        {"displayName":"December", "submitValue":"December"}
    ]
});



SALP.data.IncidentReview = new Object();

SALP.data.IncidentReview.model = Ext.define('IncidentReview', 
{
	extend: 'Ext.data.Model',
	fields: [
		{name: 'label', type: 'string'},
		{name: 'mtbf', type: 'string'},
		{name: 'frequency', type: 'string'},
		{name: 'wowFrequency', type: 'int'},
		{name: 'changeRelated', type: 'string'},
		{name: 'detect', type: 'string'},
		{name: 'engage', type: 'string'},
		{name: 'resolve', type: 'string'},
		{name: 'wowResolve', type: 'int'},
		{name: 'config', type: 'string'},
		{name: 'hwSw', type: 'string'},
		{name: 'asDesign', type: 'string'},
		{name: 'executionDefect', type: 'string'},
		{name: 'impact', type: 'string'}
]});


SALP.data.WeeklyReviewMeasure = new Object();

SALP.data.WeeklyReviewMeasure.model = Ext.define('WeeklyReviewMeasure', 
{
	extend: 'Ext.data.Model',
	fields: [
		{name: 'weekStarting', type: 'date'},
		{name: 'change', type: 'int'},
		{name: 'detect', type: 'int'},
		{name: 'engage', type: 'int'},
		{name: 'frequency', type: 'int'},
		{name: 'hwSw', type: 'int'},
		{name: 'mtbf', type: 'int'},
		{name: 'restore', type: 'int'},
		{name: 'toEngage', type: 'int'},
		{name: 'toRestore', type: 'int'}
]});



SALP.data.ServerMonitor = new Object();

SALP.data.ServerMonitor.model = Ext.define('ServerMonitor',  
{
	extend: 'Ext.data.Model',
	fields: [
		{name: 'name', type: 'string'},
		{name: 'startDate', type: 'date'},
		{name: 'hours', type: 'int'}
]});


SALP.data.CRTicketStatus = new Object();

SALP.data.CRTicketStatus.model = Ext.define('CRTicketStatus',  
{
	extend: 'Ext.data.Model',
	fields: [
		{name: 'date', type: 'date'},
		{name: 'currentWeek', type: 'int'},
		{name: 'previousWeek', type: 'int'}
]});



SALP.data.URLMonitor = new Object();

SALP.data.URLMonitor.model = Ext.define('URLMonitor',  
{
	extend: 'Ext.data.Model',
	fields: [
		{name: 'date', type: 'date'},
		{name: 'bnocTools', type: 'int'},
		{name: 'etsTools', type: 'int'},
		{name: 'rcor', type: 'int'}
]});



SALP.data.SubscriptionErrorStatus = new Object();

SALP.data.SubscriptionErrorStatus.model = Ext.define('SubscriptionErrorStatus',  
{
	extend: 'Ext.data.Model',
	fields: [
		{name: 'date', type: 'date'},
		{name: 'successRate', type: 'int'}
]});



SALP.data.TempDBLogStatus = new Object();

SALP.data.TempDBLogStatus.model = Ext.define('TempDBLogStatus',  
{
	extend: 'Ext.data.Model',
	fields: [
		{name: 'date', type: 'date'},
		{name: 'tempDBLogPct', type: 'int'},
		{name: 'tempDBLogStatus', type: 'int'}
]});


SALP.data.ApplicationEventMonitor = new Object();

SALP.data.ApplicationEventMonitor.model = Ext.define('ApplicationEventMonitor',  
{
	extend: 'Ext.data.Model',
	fields: [
		{name: 'date', type: 'date'},
		{name: 'oBIDR_po_P01', type: 'int'},
		{name: 'oBIPROC_WC_P01', type: 'int'},
		{name: 'oBIPROC_WC_P01_APP', type: 'int'},
		{name: 'oBIWEB_WC_P01', type: 'int'}
]});


SALP.data.CRTickets = new Object();

SALP.data.CRTickets.model = Ext.define('CRTickets',  
{
	extend: 'Ext.data.Model',
	fields: [
		{name: 'date', type: 'date'},
		{name: 'cRCount', type: 'int'},
		{name: 'baseline', type: 'int'}
]});


SALP.data.IVRCDRCalls = new Object();

SALP.data.IVRCDRCalls.model = Ext.define('IVRCDRCalls',  
{
	extend: 'Ext.data.Model',
	fields: [
		{name: 'date', type: 'date'},
		{name: 'callCount', type: 'int'},
		{name: 'baseline', type: 'int'}
]});



SALP.data.CRTicketDelayDisplay = new Object();

SALP.data.CRTicketDelayDisplay.model = Ext.define('CRTicketDelayDisplay',  
{
	extend: 'Ext.data.Model',
	fields: [
		{name: 'name', type: 'string'},
		{name: 'value', type: 'string'}
]});



SALP.data.FCCThresholdBreached = new Object();

SALP.data.FCCThresholdBreached.model = Ext.define('CRTicketDelayDisplay',  
{
	extend: 'Ext.data.Model',
	fields: [
		{name: 'ticketId', type: 'string'},
		{name: 'monitor', type: 'string'},
		{name: 'threshhold', type: 'string'},
		{name: 'thStatus', type: 'string'},
		{name: 'monitorDiscovery', type: 'string'},
		{name: 'ces', type: 'string'},
		{name: 'email', type: 'string'},
		{name: 'rcorStatus', type: 'string'},
		{name: 'workedBy', type: 'string'}
]});


SALP.data.CurrentServerProcesses = new Object();

SALP.data.CurrentServerProcesses.model = Ext.define('CurrentServerProcesses',  
{
	extend: 'Ext.data.Model',
	fields: [
		{name: 'statementText', type: 'string'},
		{name: 'databaseName', type: 'string'},
		{name: 'cpuTime', type: 'string'},
		{name: 'runningMinutes', type: 'int'},
		{name: 'runningFrom', type: 'string'},
		{name: 'runningBy', type: 'string'},
		{name: 'sessionId', type: 'int'},
		{name: 'blockedBy', type: 'int'},
		{name: 'reads', type: 'int'},
		{name: 'writes', type: 'int'},
		{name: 'programName', type: 'string'},
		{name: 'loginName', type: 'string'},
		{name: 'status', type: 'string'},
		{name: 'waitTime', type: 'int'},
		{name: 'lastWaitType', type: 'string'},
		{name: 'cmd', type: 'string'},
		{name: 'lastRequestStartTime', type: 'date'},
		{name: 'logicalReads', type: 'int'}
]});



SALP.data.Servers = new Object();

SALP.data.Servers.model = Ext.define('Servers',  
{
	extend: 'Ext.data.Model',
	fields: [
		{name: 'id', type: 'string'},
		{name: 'serverName', type: 'string'},
		{name: 'memoryUsage', type: 'string'},
		{name: 'cpuUsage', type: 'string'},
		{name: 'hasMonitoredProcesses', type: 'boolean'},
		{name: 'hasLongRunningJobs', type: 'boolean'}
]});


SALP.data.ServersProcesses = new Object();

SALP.data.ServersProcesses.store = Ext.create('Ext.data.Store', 
{
	autoLoad: true,
	model: SALP.data.Servers.model,
	proxy: 
	{
  	type: 'ajax',
   	url: 'WebServices/Service.svc/GetServersList',
   	extraParams:
   	{
	  	hasMonitoredProcesses: 'True'
   	},
   	pageParam: undefined,
		startParam: undefined,
		limitParam: undefined,
   	reader: 
   	{
    	type: 'json',
      root: 'data'
   	}
	}
});

SALP.data.ServersJobs = new Object();

SALP.data.ServersJobs.store = Ext.create('Ext.data.Store', 
{
	autoLoad: true,
	model: SALP.data.Servers.model,
	proxy: 
	{
  	type: 'ajax',
   	url: 'WebServices/Service.svc/GetServersList',
   	extraParams:
   	{
	  	hasLongRunningJobs: 'True'
   	},
   	pageParam: undefined,
		startParam: undefined,
		limitParam: undefined,
   	reader: 
   	{
    	type: 'json',
      root: 'data'
   	}
	}
});


SALP.data.Servers.CPUUsage = new Object();

SALP.data.Servers.CPUUsage.model = Ext.define('CPUUsage',  
{
	extend: 'Ext.data.Model',
	fields: [
		{name: 'timestamp', type: 'string'},
		{name: 'recordTime', type: 'string'},
		{name: 'cpu', type: 'string'}
]});


SALP.data.MaintenanceResolveStatus = new Object();

SALP.data.MaintenanceResolveStatus.model = Ext.define('MaintenanceResolveStatus',  
{
	extend: 'Ext.data.Model',
	fields: [
		{name: 'month', type: 'string'},
		{name: 'year', type: 'string'},
		{name: 'comcastDivision', type: 'string'},
		{name: 'vpDepartment', type: 'string'},
		{name: 'vpName', type: 'string'},
		{name: 'dirGroup', type: 'string'},
		{name: 'dirName', type: 'string'},
		{name: 'failCount', type: 'int'},
		{name: 'failPct', type: 'string'},
		{name: 'causedIncidentCount', type: 'int'},
		{name: 'incidentPct', type: 'string'},
		{name: 'incidentCount', type: 'int'},
		{name: 'sev1Count', type: 'int'},
		{name: 'sev1Pct', type: 'string'},
		{name: 'sev1TicketCount', type: 'int'},
		{name: 'successCount', type: 'int'},
		{name: 'successPct', type: 'int'},
		{name: 'totalCount', type: 'int'}
]});



SALP.data.MaintenanceCausedSev1MonthOverMonth = new Object();

SALP.data.MaintenanceCausedSev1MonthOverMonth.model = Ext.define('maintenanceCausedSev1MonthOverMonth',  
{
	extend: 'Ext.data.Model',
	fields: [
		{name: 'maintenanceMonth', type: 'string'},
		{name: 'failCountPreviousYear', type: 'string'},
		{name: 'failCountCurrentYear', type: 'string'},
		{name: 'failPctPreviousYear', type: 'string'},
		{name: 'failPctCurrentYear', type: 'string'},
		{name: 'totalCountPreviousYear', type: 'string'},
		{name: 'totalCountCurrentYear', type: 'string'}
]});


SALP.data.Sev1CausedByMaintDetails = new Object();

SALP.data.Sev1CausedByMaintDetails.model = Ext.define('sev1CausedByMaintDetails',  
{
	extend: 'Ext.data.Model',
	fields: [
		{name: 'maintTicketId', type: 'string'},
		{name: 'cmSummary', type: 'string'},
		{name: 'resolutionDescription', type: 'string'},
		{name: 'ttsId', type: 'string'},
		{name: 'rcaTitle', type: 'string'},
		{name: 'actualStart', type: 'string'},
		{name: 'actualEnd', type: 'string'},
		{name: 'causeDescription', type: 'string'},
		{name: 'solutionDescription', type: 'string'},
		{name: 'problemDescription', type: 'string'},
		{name: 'queueName', type: 'string'},
		{name: 'selfInflicted', type: 'string'},
		{name: 'execSummary', type: 'string'},
		{name: 'customerImpact', type: 'string'},
		{name: 'careImpact', type: 'string'},
		{name: 'platform', type: 'string'},
		{name: 'rootCause', type: 'string'},
		{name: 'responsibleParty', type: 'string'},
		{name: 'detect', type: 'string'},
		{name: 'redundacy', type: 'string'},
		{name: 'responsiblePerson', type: 'string'}
]});


SALP.data.SMTickets = new Object();
SALP.data.SMTickets.GeneralDetails = new Object();

SALP.data.SMTickets.GeneralDetails.model = Ext.define('SMTicketGeneralDetails', 
{
	extend: 'Ext.data.Model',
	fields: [
		{name: 'ticketId', type: 'string'},
		{name: 'parentTicketId', type: 'string'},
		{name: 'projectName', type: 'string'},
		{name: 'regionName', type: 'string'},
		{name: 'ticketType', type: 'string'},
		{name: 'product', type: 'string'},
		{name: 'smStatus', type: 'string'},
		{name: 'createDate', type: 'string'},
		{name: 'nextAction', type: 'string'},
		{name: 'maintenanceType', type: 'string'},
		{name: 'maintenanceDescription', type: 'string'},
		{name: 'maintenaceCategory', type: 'string'},
		{name: 'maintenanceDetail', type: 'string'},
		{name: 'smFrequency', type: 'string'},
		{name: 'videoAffected', type: 'string'},
		{name: 'hsdAffected', type: 'string'},
		{name: 'telephonyAffected', type: 'string'},
		{name: 'totalCustomersAffected', type: 'string'},
		{name: 'communitiesAffected', type: 'string'},
		{name: 'actualStart', type: 'string'},
		{name: 'actualEnd', type: 'string'},
		{name: 'smDuration', type: 'string'},
		{name: 'resolutionDescription', type: 'string'},
		{name: 'indicatorCity', type: 'string'},
		{name: 'indicatorState', type: 'string'},
		{name: 'localManagementApprover', type: 'string'},
		{name: 'localManagementPhone', type: 'string'},
		{name: 'supportAreaName', type: 'string'},
		{name: 'fullName', type: 'string'},
		{name: 'apCreateDate', type: 'string'},
		{name: 'plannedStart', type: 'string'},
		{name: 'plannedEnd', type: 'string'}
]});

SALP.data.SMTickets.AffectedElements = new Object();

SALP.data.SMTickets.AffectedElements.model = Ext.define('SMTicketAffectedElements', 
{
	extend: 'Ext.data.Model',
	fields: [
		{name: 'ticketId', type: 'string'},
		{name: 'entryId', type: 'string'},
		{name: 'serviceAffected', type: 'string'},
		{name: 'elementName', type: 'string'},
		{name: 'actualStart', type: 'string'},
		{name: 'actualEnd', type: 'string'}
]});



Ext.define('SRTicketGeneralDetails', 
{
	extend: 'Ext.data.Model',
	fields: [
		{name: 'ticketId', type: 'string'},
		{name: 'statusCode', type: 'string'},
		{name: 'status', type: 'string'},
		{name: 'priorityCode', type: 'string'},
		{name: 'priority', type: 'string'},
		{name: 'submitter', type: 'string'},
		{name: 'assignedDate', type: 'string'},
		{name: 'createDate', type: 'string'},
		{name: 'assignedDivision', type: 'string'},
		{name: 'supportAreaName', type: 'string'},
		{name: 'assignedQName', type: 'string'},
		{name: 'fullName', type: 'string'},
		{name: 'problemCode', type: 'string'},
		{name: 'problemDescription', type: 'string'},
		{name: 'problemCategory', type: 'string'},
		{name: 'problemSubcategory', type: 'string'},
		{name: 'solutionCode', type: 'string'},
		{name: 'solutionDescription', type: 'string'},
		{name: 'solutionCategory', type: 'string'},
		{name: 'solutionSubcategory', type: 'string'},
		{name: 'requestForName', type: 'string'},
		{name: 'requestForPhone', type: 'string'},
		{name: 'requestForLocation', type: 'string'},
		{name: 'requestForEmail', type: 'string'},
		{name: 'workLocationStreet', type: 'string'},
		{name: 'workLocationCity', type: 'string'},
		{name: 'contactName', type: 'string'},
		{name: 'contactPhone', type: 'string'},
		{name: 'contactEmail', type: 'string'}
]});

Ext.define('SRTicketAttributes', 
{
	extend: 'Ext.data.Model',
	fields: [
		{name: 'ticketId', type: 'string'},
		{name: 'attributeLabel', type: 'string'},
		{name: 'attributeValue', type: 'string'},
		{name: 'attributeDisplay', type: 'string'}
]});


Ext.define('CMTicketGeneralDetails', 
{
	extend: 'Ext.data.Model',
	fields: [
		{name: 'ticketId', type: 'string'},
		{name: 'changeSummary', type: 'string'},
		{name: 'regionName', type: 'string'},
		{name: 'supportAreaName', type: 'string'},
		{name: 'queueName', type: 'string'},
		{name: 'fullName', type: 'string'},
		{name: 'status', type: 'string'},
		{name: 'ndStatus', type: 'string'},
		{name: 'product', type: 'string'},
		{name: 'assignedDate', type: 'string'},
		{name: 'videoAffected', type: 'string'},
		{name: 'hsdAffected', type: 'string'},
		{name: 'telephonyAffected', type: 'string'},
		{name: 'totalCustomersAffected', type: 'string'},
		{name: 'nextAction', type: 'string'},
		{name: 'maintenanceType', type: 'string'},
		{name: 'projectName', type: 'string'},
		{name: 'changeCategory', type: 'string'},
		{name: 'changeSubcategory', type: 'string'},
		{name: 'impactLevel', type: 'string'},
		{name: 'changeDetail', type: 'string'},
		{name: 'plannedStart', type: 'string'},
		{name: 'plannedEnd', type: 'string'}
]});


Ext.define('CMContactDetails', 
{
	extend: 'Ext.data.Model',
	fields: [
		{name: 'ticketId', type: 'string'},
		{name: 'createDate', type: 'string'},
		{name: 'contactName', type: 'string'},
		{name: 'emailAddress', type: 'string'},
		{name: 'primaryPhone', type: 'string'},
		{name: 'contactRole', type: 'string'}
]});

Ext.define('CMTicketAttributes', 
{
	extend: 'Ext.data.Model',
	fields: [
		{name: 'ticketId', type: 'string'},
		{name: 'attributeLabel', type: 'string'},
		{name: 'attributeValue', type: 'string'}
]});


Ext.define('CMTicketSteps', 
{
	extend: 'Ext.data.Model',
	fields: [
		{name: 'ticketId', type: 'string'},
		{name: 'stepDescription', type: 'string'},
		{name: 'stepLabel', type: 'string'},
		{name: 'stepSequence', type: 'string'},
		{name: 'procedureId', type: 'string'},
		{name: 'plannedStart', type: 'string'},
		{name: 'plannedEnd', type: 'string'},
		{name: 'actualStart', type: 'string'},
		{name: 'actualEnd', type: 'string'},
		{name: 'estimatedDuration', type: 'string'},
		{name: 'actualDuration', type: 'string'}
]});

Ext.define('CMTicketAffectedElements', 
{
	extend: 'Ext.data.Model',
	fields: [
		{name: 'ticketId', type: 'string'},
		{name: 'entryId', type: 'string'},
		{name: 'serviceAffected', type: 'string'},
		{name: 'elementName', type: 'string'},
		{name: 'videoAffected', type: 'string'},
		{name: 'hsdAffected', type: 'string'},
		{name: 'telephonyAffected', type: 'string'},
		{name: 'totalAffected', type: 'string'},
		{name: 'status', type: 'string'},
		{name: 'actualStart', type: 'string'},
		{name: 'actualEnd', type: 'string'}
]});


Ext.define('CMTicketWorkLog', 
{
	extend: 'Ext.data.Model',
	fields: [
		{name: 'entryId', type: 'string'},
		{name: 'createDate', type: 'string'},
		{name: 'submitter', type: 'string'},
		{name: 'source', type: 'string'},
		{name: 'subject', type: 'string'},
		{name: 'details', type: 'string'}
]});


Ext.define('CRTicketGeneralDetails', 
{
	extend: 'Ext.data.Model',
	fields: [
		{name: 'ticketId', type: 'string'},
		{name: 'problemSummary', type: 'string'},
		{name: 'regionName', type: 'string'},
		{name: 'supportAreaName', type: 'string'},
		{name: 'queueName', type: 'string'},
		{name: 'fullName', type: 'string'},
		{name: 'product', type: 'string'},
		{name: 'crStatus', type: 'string'},
		{name: 'assignedDate', type: 'string'},
		{name: 'closedDate', type: 'string'},
		{name: 'originalTicket', type: 'string'},
		{name: 'reworkTicket', type: 'string'},
		{name: 'nextAction', type: 'string'},
		{name: 'problemCode', type: 'string'},
		{name: 'category', type: 'string'},
		{name: 'subCategory', type: 'string'},
		{name: 'causeCode', type: 'string'},
		{name: 'causeDescription', type: 'string'},
		{name: 'causeCategory', type: 'string'},
		{name: 'causeSubcategory', type: 'string'},
		{name: 'solutionCode', type: 'string'},
		{name: 'solutionDescription', type: 'string'},
		{name: 'solutionCategory', type: 'string'},
		{name: 'solutionSubcategory', type: 'string'},
		{name: 'troubleStart', type: 'string'},
		{name: 'ttrStart', type: 'string'},
		{name: 'ttrStop', type: 'string'},
		{name: 'ttrSla', type: 'string'},
		{name: 'indicatorCity', type: 'string'},
		{name: 'indicatorState', type: 'string'},
		{name: 'contactName', type: 'string'},
		{name: 'contactPhone', type: 'string'},
		{name: 'contactEmail', type: 'string'},
		{name: 'customerId', type: 'string'},
		{name: 'accountNumber', type: 'string'},
		{name: 'node', type: 'string'},
		{name: 'systemId', type: 'string'},
		{name: 'networkElement', type: 'string'},
		{name: 'sysNm', type: 'string'}
]});

Ext.define('CRTicketAttributes', 
{
	extend: 'Ext.data.Model',
	fields: [
		{name: 'ticketId', type: 'string'},
		{name: 'attributeLabel', type: 'string'},
		{name: 'attributeValue', type: 'string'}
]});


Ext.define('SITicketGeneralDetails', 
{
	extend: 'Ext.data.Model',
	fields: [
		{name: 'ticketId', type: 'string'},
		{name: 'incidentSummary', type: 'string'},
		{name: 'supportAreaName', type: 'string'},
		{name: 'submittingName', type: 'string'},
		{name: 'queueName', type: 'string'},
		{name: 'fullName', type: 'string'},
		{name: 'siSeverity', type: 'string'},
		{name: 'siStatus', type: 'string'},
		{name: 'siPriority', type: 'string'},
		{name: 'priority', type: 'string'},
		{name: 'product', type: 'string'},
		{name: 'createDate', type: 'string'},
		{name: 'assignedDate', type: 'string'},
		{name: 'telephonyAffected', type: 'string'},
		{name: 'videoAffected', type: 'string'},
		{name: 'hsdAffected', type: 'string'},
		{name: 'totalCustomersAffected', type: 'string'},
		{name: 'nextAction', type: 'string'},
		{name: 'problemCode', type: 'string'},
		{name: 'source', type: 'string'},
		{name: 'category', type: 'string'},
		{name: 'subCategory', type: 'string'},
		{name: 'causeCode', type: 'string'},
		{name: 'causeDescription', type: 'string'},
		{name: 'causeCategory', type: 'string'},
		{name: 'causeSubCategory', type: 'string'},
		{name: 'solutionCode', type: 'string'},
		{name: 'solutionDescription', type: 'string'},
		{name: 'solutionCategory', type: 'string'},
		{name: 'solutionSubCategory', type: 'string'},
		{name: 'externalAssignee', type: 'string'},
		{name: 'externalReference', type: 'string'},
		{name: 'alarmDate', type: 'string'},
		{name: 'ttrStart', type: 'string'},
		{name: 'ttrStop', type: 'string'},
		{name: 'indicatorCity', type: 'string'},
		{name: 'indicatorState', type: 'string'},
		{name: 'networkElement', type: 'string'},
		{name: 'correlationType', type: 'string'},
		{name: 'serviceCondition', type: 'string'},
		{name: 'careImpact', type: 'string'},
		{name: 'estimatedServiceRestore', type: 'string'},
		{name: 'siLastModifiedBy', type: 'string'},
		{name: 'lastModifiedBy', type: 'string'},
		{name: 'modifiedDate', type: 'string'},
		{name: 'siResolvedBy', type: 'string'},
		{name: 'resolvedBy', type: 'string'},
		{name: 'contactDepartment', type: 'string'},
		{name: 'contactPhone', type: 'string'},
		{name: 'regionName', type: 'string'},
		{name: 'duration', type: 'string'},
		{name: 'detectedBy', type: 'string'},
		{name: 'cicGroupId', type: 'string'}
]});


Ext.define('SITicketAttributes', 
{
	extend: 'Ext.data.Model',
	fields: [
		{name: 'ticketId', type: 'string'},
		{name: 'attributeLabel', type: 'string'},
		{name: 'attributeValue', type: 'string'}
]});


Ext.define('SITicketCustomerTicket', 
{
	extend: 'Ext.data.Model',
	fields: [
		{name: 'customerTicketId', type: 'string'},
		{name: 'problemSummary', type: 'string'},
		{name: 'createDate', type: 'string'},
		{name: 'contactName', type: 'string'},
		{name: 'status', type: 'string'},
		{name: 'node', type: 'string'}
]});

Ext.define('SITicketAffectedElements', 
{
	extend: 'Ext.data.Model',
	fields: [
		{name: 'sourceTicket', type: 'string'},
		{name: 'status', type: 'string'},
		{name: 'entryId', type: 'string'},
		{name: 'serviceAffected', type: 'string'},
		{name: 'elementName', type: 'string'},
		{name: 'elementType', type: 'string'},
		{name: 'systemName', type: 'string'},
		{name: 'telephonyAffected', type: 'string'},
		{name: 'videoAffected', type: 'string'},
		{name: 'hSDAffected', type: 'string'},
		{name: 'totalAffected', type: 'string'},
		{name: 'aeStatus', type: 'string'},
		{name: 'actualStart', type: 'string'},
		{name: 'actualEnd', type: 'string'}
]});

Ext.define('SITicketWorkLog', 
{
	extend: 'Ext.data.Model',
	fields: [
		{name: 'entryId', type: 'string'},
		{name: 'createDate', type: 'string'},
		{name: 'submitter', type: 'string'},
		{name: 'source', type: 'string'},
		{name: 'subject', type: 'string'},
		{name: 'details', type: 'string'}
]});



Ext.define('OETicketGeneralDetails', 
{
	extend: 'Ext.data.Model',
	fields: [
		{name: 'ticketId', type: 'string'},
		{name: 'entryId', type: 'string'},
		{name: 'serviceAffected', type: 'string'},
		{name: 'elementName', type: 'string'},
		{name: 'telephonyAffected', type: 'string'},
		{name: 'hsdAffected', type: 'string'},
		{name: 'videoAffected', type: 'string'},
		{name: 'totalAffected', type: 'string'},
		{name: 'aeStatus', type: 'string'},
		{name: 'elementType', type: 'string'},
		{name: 'plannedFlag', type: 'string'},
		{name: 'plannedStart', type: 'string'},
		{name: 'plannedEnd', type: 'string'},
		{name: 'duration', type: 'string'},
		{name: 'source', type: 'string'},
		{name: 'itemFocus', type: 'string'},
		{name: 'regionId', type: 'string'},
		{name: 'regionName', type: 'string'},
		{name: 'systemId', type: 'string'},
		{name: 'systemName', type: 'string'},
		{name: 'indicatorState', type: 'string'},
		{name: 'location', type: 'string'},
		{name: 'product', type: 'string'},
		{name: 'commercialUse', type: 'string'},
		{name: 'messageInternal', type: 'string'},
		{name: 'linkProblemCode', type: 'string'},
		{name: 'problemDescription', type: 'string'},
		{name: 'category', type: 'string'},
		{name: 'subcategory', type: 'string'},
		{name: 'causeCode', type: 'string'},
		{name: 'causeDescription', type: 'string'},
		{name: 'causeCategory', type: 'string'},
		{name: 'causeSubcategory', type: 'string'},
		{name: 'solutionCode', type: 'string'},
		{name: 'solutionDescription', type: 'string'},
		{name: 'solutionCategory', type: 'string'},
		{name: 'solutionSubcategory', type: 'string'},
		{name: 'originatingTicket', type: 'string'},
		{name: 'submitter', type: 'string'},
		{name: 'lastModifiedBy', type: 'string'},
		{name: 'actualStart', type: 'string'},
		{name: 'actualEnd', type: 'string'},
		{name: 'etr', type: 'string'},
		{name: 'createDate', type: 'string'},
		{name: 'modifiedDate', type: 'string'},
]});

Ext.define('OETicketAffectedElements', 
{
	extend: 'Ext.data.Model',
	fields: [
		{name: 'aeTicketId', type: 'string'},
		{name: 'aeEntryId', type: 'string'},
		{name: 'aeServiceAffected', type: 'string'},
		{name: 'aeElementName', type: 'string'},
		{name: 'aeTelephonyAffected', type: 'string'},
		{name: 'aeVideoAffected', type: 'string'},
		{name: 'aeHSDAffected', type: 'string'},
		{name: 'aeTotalAffected', type: 'string'},
		{name: 'aeStatus', type: 'string'},
		{name: 'aeElementType', type: 'string'},
		{name: 'aePlannedFlag', type: 'string'},
		{name: 'aeSystemName', type: 'string'},
		{name: 'aeMessageInternal', type: 'string'},
		{name: 'aeCauseCode', type: 'string'},
		{name: 'aeCauseDescription', type: 'string'},
		{name: 'aeSolutionCode', type: 'string'},
		{name: 'aeSolutionDescription', type: 'string'},
		{name: 'aeOriginatingTicket', type: 'string'},
		{name: 'aeSubmitter', type: 'string'},
		{name: 'aeLastModifiedBy', type: 'string'},
		{name: 'aeActualStart', type: 'string'},
		{name: 'aeActualEnd', type: 'string'},
]});


Ext.define('OETicketWorkLog', 
{
	extend: 'Ext.data.Model',
	fields: [
		{name: 'sourceTicket', type: 'string'},
		{name: 'createDate', type: 'string'},
		{name: 'submitter', type: 'string'},
		{name: 'source', type: 'string'},
		{name: 'subject', type: 'string'},
		{name: 'details', type: 'string'}
]});

Ext.define('OETicketAttributes', 
{
	extend: 'Ext.data.Model',
	fields: [
		{name: 'sourceTicket', type: 'string'},
		{name: 'attributeLabel', type: 'string'},
		{name: 'attributeValue', type: 'string'}
]});


SALP.data.AllMaintenanceSuccessFailure = Ext.create('Ext.data.Store', 
{
    fields: ['displayName', 'submitValue'],
    data : [
        {"displayName":"Comcast Division", "submitValue":"comcastDivision"},
        {"displayName":"VP Department", "submitValue":"vpDepartment"},
        {"displayName":"VP Name", "submitValue":"vpName"},
        {"displayName":"Dir Group", "submitValue":"dirGroup"},
        {"displayName":"Dir Name", "submitValue":"dirName"}
    ]
});




Ext.define('OrgTreeMapModel', 
{
	extend: 'Ext.data.Model',
	fields: [
		{name: 'text', type: 'string'},
		{name: 'supervisor', type: 'string'},
		{name: 'value', type: 'int'},
		{name: 'failureRate', type: 'int'},
		{name: 'expanded', type: 'boolean'},
		{
    	name: 'change',
      calculate: function () 
      {	
      	return (-5 + Math.random() * 10).toFixed(2); 
      }
    }
]});


Ext.create('Ext.data.Store', 
{
	storeId: 'selectDay',
  fields: ['displayName'],
  data : [
  	{"displayName":"2015"},
  	{"displayName":"2016"},
  	{"displayName":"2017"}
  ]
});


Ext.create('Ext.data.Store', 
{
	storeId: 'selectMonth',
  fields: ['displayName', 'submitValue'],
  data : [
  	{"displayName":"January", "submitValue":"1"},
   	{"displayName":"Feburary", "submitValue":"2"},
    {"displayName":"March", "submitValue":"3"},
    {"displayName":"April", "submitValue":"4"},
    {"displayName":"May", "submitValue":"5"},
    {"displayName":"June", "submitValue":"6"},
    {"displayName":"July", "submitValue":"7"},
    {"displayName":"August", "submitValue":"8"},
    {"displayName":"September", "submitValue":"9"},
    {"displayName":"October", "submitValue":"10"},
    {"displayName":"November", "submitValue":"11"},
    {"displayName":"December", "submitValue":"12"},
  ]
});



Ext.define('CMDetailByHierarchy', 
{
	extend: 'Ext.data.Model',
	fields: [
		{name: 'maintenanceMonth', type: 'string'},
		{name: 'maintenanceYear', type: 'string'},
		{name: 'heiarchyUser', type: 'string'},
		{name: 'directReport', type: 'string'},
		{name: 'maintenanceImplementor', type: 'string'},
		{name: 'sm', type: 'string'},
		{name: 'smStatus', type: 'string'},
		{name: 'smMaintenanceType', type: 'string'},
		{name: 'smAuthor', type: 'string'},
		{name: 'smParent', type: 'string'},
		{name: 'closedBy', type: 'string'},
		{name: 'ticketImplementer', type: 'string'},
		{name: 'ticketId', type: 'string'},
		{name: 'ticketStart', type: 'string'},
		{name: 'ticketEnd', type: 'string'},
		{name: 'smCreateDate', type: 'string'},
		{name: 'smTimeframeStart', type: 'string'},
		{name: 'smTimeframeEnd', type: 'string'},
		{name: 'smClosedDate', type: 'string'},
		{name: 'smMaintenanceCategory', type: 'string'},
		{name: 'smMaintenanceDetail', type: 'string'},
		{name: 'smRegion', type: 'string'},
		{name: 'smResolutionDescription', type: 'string'},
		{name: 'resolutionDescription', type: 'string'},
		{name: 'resultCategory', type: 'string'},
		{name: 'smResolutionDetail', type: 'string'},
		{name: 'cm', type: 'string'},
		{name: 'cmMaintenanceType', type: 'string'},
		{name: 'cmCategory', type: 'string'},
		{name: 'cmSubcategory', type: 'string'},
		{name: 'cmProcedureName', type: 'string'},
		{name: 'procedureId', type: 'string'},
		{name: 'cmSummary', type: 'string'},
		{name: 'cmDetail', type: 'string'},
		{name: 'cmRisk', type: 'string'},
		{name: 'cmImpact', type: 'string'},
		{name: 'cmCreateDate', type: 'string'},
		{name: 'cmPlannedStart', type: 'string'},
		{name: 'cmPlannedEnd', type: 'string'},
		{name: 'cmStatus', type: 'string'},
		{name: 'cmSubmitter', type: 'string'},
		{name: 'cmSubmitterName', type: 'string'},
		{name: 'cmRegion', type: 'string'},
		{name: 'orgUnitDescription', type: 'string'},
		{name: 'orgHeirarchyLevel1Description', type: 'string'},
		{name: 'orgHeirarchyLevel2Description', type: 'string'},
		{name: 'sapLead', type: 'string'},
		{name: 'sapDir', type: 'string'},
		{name: 'sapVp', type: 'string'},
		{name: 'sapVpTop', type: 'string'},
		{name: 'siTicket', type: 'string'},
		{name: 'relatedTicket', type: 'string'},
		{name: 'siCreateDate', type: 'string'},
		{name: 'siActualStart', type: 'string'},
		{name: 'siActualEnd', type: 'string'},
		{name: 'ticketImplementerNew', type: 'string'},
		{name: 'cancelledBeforeMaintenance', type: 'string'},
		{name: 'cancelledBeforeMaintenanceSm', type: 'string'},
		{name: 'sucessCategory', type: 'string'},
		{name: 'supportAreaName', type: 'string'},
		{name: 'queueName', type: 'string'},
		{name: 'siSeverity', type: 'string'},
		{name: 'leadOrgUnitDescription', type: 'string'},
		{name: 'dirOrgUnitDescription', type: 'string'},
		{name: 'vpOrgUnitDescription', type: 'string'},
		{name: 'leadOrgHierarchyLevel1Description', type: 'string'},
		{name: 'dirOrgHierarchyLevel1Description', type: 'string'},
		{name: 'vpOrgHierarchyLevel1Description', type: 'string'},
		{name: 'vpOrgHierarchyLevel2Description', type: 'string'},
		{name: 'ts', type: 'string'},
]});


Ext.define('SAPUser', 
{
	extend: 'Ext.data.Model',
	fields: [
		{name: 'displayName', type: 'string'},
		{name: 'name', type: 'string'},
		{name: 'hierarchyLevel1', type: 'string'},
		{name: 'department', type: 'string'},
		{name: 'orgUnit', type: 'string'},
		{name: 'jobDescription', type: 'string'},
		{name: 'emailAddress', type: 'string'},
		{name: 'workPhone', type: 'string'},
		{name: 'loginId', type: 'string'},
		{name: 'supervisor', type: 'string'}
]});


Ext.define('SAPSubordinate', 
{
	extend: 'Ext.data.Model',
	fields: [
		{name: 'employeeId', type: 'string'},
		{name: 'name', type: 'string'},
		{name: 'loginId', type: 'string'},
		{name: 'jobLevelCode', type: 'string'},
		{name: 'employeeStatusDescription', type: 'string'},
		{name: 'supervisorName', type: 'string'},
		{name: 'employeeStatus', type: 'string'},
		{name: 'workEmail', type: 'string'},
		{name: 'path', type: 'string'},
		{name: 'direct', type: 'string'}
]});

Ext.define('OpenTickets', 
{
	extend: 'Ext.data.Model',
	fields: [
		{name: 'ticketId', type: 'string'},
		{name: 'incidentSummary', type: 'string'},
		{name: 'nextAction', type: 'string'},
		{name: 'serviceCondition', type: 'string'},
		{name: 'severity', type: 'string'},
		{name: 'createDateMt', type: 'date'},
		{name: 'supportAreaName', type: 'string'},
		{name: 'regionName', type: 'string'},
		{name: 'queueName', type: 'string'}
]});


Ext.define('HaystackDivisions', 
{
	extend: 'Ext.data.Model',
	fields: [
		{name: 'division', type: 'string'},
]});

Ext.create('Ext.data.Store', 
{
	storeId: 'selectDivision',
	autoLoad: false,
	model: 'HaystackDivisions',
	proxy: 
	{
  	type: 'ajax',
   	url: 'WebServices/Service.svc/GetHaystackDivisions',
   	extraParams:
   	{
   		//username: SALP.user.accountName
   	},
   	pageParam: undefined,
		startParam: undefined,
		limitParam: undefined,
   	reader: 
   	{
    	type: 'json',
      root: 'data'
   	}
	}
});


Ext.create('Ext.data.Store', 
{
	storeId: 'selectBreakdown',
  fields: ['displayName', 'submitValue'],
  data : [
  	{"displayName":"<b>Network Elements</b>", "submitValue":""},
  	{"displayName":"Division", "submitValue":"level1"},
		{"displayName":"Region", "submitValue":"level2"},
		{"displayName":"System", "submitValue":"level3"},
		{"displayName":"Headend" , "submitValue":"level8"},
		{"displayName":"CMTS Model", "submitValue":"levelBModel"},
		{"displayName":"CMTS", "submitValue":"level9"},
		{"displayName":"Node", "submitValue":"level10"},
		{"displayName":"Video Controller", "submitValue":"level4"},
		{"displayName":"Switch", "submitValue":"level6"},
		{"displayName": "<b>Devices</b>", "submitValue":""},
		{"displayName":"MODEM Manufacturer", "submitValue":"deviceDataMake"},
		{"displayName":"MODEM Model", "submitValue":"deviceDataModel"},
		{"displayName":"MODEM Boot", "submitValue":"deviceDataBoot"},
		{"displayName":"STB Manufacturer", "submitValue":"deviceVideoMake"},
		{"displayName":"STB Model", "submitValue":"deviceVideoModel"},
		{"displayName":"STB EPG", "submitValue":"deviceVideoEpg"},
		{"displayName":"STB Firmware", "submitValue":"deviceVideoFirmware"},
		{"displayName":"EMTA Manufacturer", "submitValue":"deviceVoiceMake"},
		{"displayName":"EMTA Model", "submitValue":"deviceVoiceModel"},
		{"displayName":"EMTA Boot", "submitValue":"deviceVoiceBoot"},
		{"displayName":"<b>Event Attributes</b>", "submitValue":""},
		{"displayName":"Call Type", "submitValue":"callTerminationReason"},
		{"displayName":"Ticket Problem Category", "submitValue":"problemCodeCat"},
		{"displayName":"Ticket Problem Sub Category", "submitValue":"problemCodeSubcat"},
		{"displayName":"Ticket Problem Code", "submitValue":"problemCode"},
		{"displayName":"Ticket Solution Category", "submitValue":"solutionCodeCat"},
		{"displayName":"Ticket Solution Sub Category", "submitValue":"solutionCodeSubcat"},
		{"displayName":"Ticket Solution Code", "submitValue":"solutionCode"},
		{"displayName":"Truck Rolls Resolution Code", "submitValue":"truckRollResolution"},
		{"displayName":"LoB", "submitValue":"lob"},
		{"displayName":"<b>Account Attributes</b>", "submitValue":""},
		{"displayName":"Adv Servces", "submitValue":"advancedServices"},
		{"displayName":"Account Type", "submitValue":"accountType"}
  ]
});

Ext.define('BreakdownCalls', 
{
	extend: 'Ext.data.Model',
	fields: [
		{name: 'eventDate', type: 'date'},
		{name: 'data1', type: 'int'},
		{name: 'data2', type: 'int'},
		{name: 'data3', type: 'int'},
		{name: 'data4', type: 'int'},
		{name: 'data5', type: 'int'},
		{name: 'data6', type: 'int'},
		{name: 'data7', type: 'int'},
		{name: 'data8', type: 'int'},
		{name: 'data9', type: 'int'},
		{name: 'data10', type: 'int'},
		{name: 'data11', type: 'int'}
]});


Ext.define('SIEventFunnel', 
{
	extend: 'Ext.data.Model',
	fields: [
		{name: 'timeFrame', type: 'date'},
		{name: 'ticketId', type: 'string'},
		{name: 'agentOffered', type: 'int'},
		{name: 'deflected', type: 'int'},
		{name: 'fiveWeekBaseline', type: 'int'}
]});



SALP.data.Test = Ext.create('Ext.data.Store', 
{
fields:['weekStarting', 'change','detect'],
	data:[
		{'weekStarting': '2016-12-12', 'change': 10, 'detect': 7},
		{'weekStarting': '2016-12-19', 'change': 5, 'detect': 5}
	]
	
});