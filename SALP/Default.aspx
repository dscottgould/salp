﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="SALP.Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=Edge" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0" />
    <meta http-equiv="cache-control" content="max-age=0" />
    <meta http-equiv="cache-control" content="no-cache" />
    <meta http-equiv="expires" content="0" />
    <meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
    <meta http-equiv="pragma" content="no-cache" />

    <title>Service Assurance Launchpad</title>
    
    <link rel="icon" type="image/png" href="/img/favicon.png" />

		<link rel="stylesheet" href="css/theme-classic-all.css" type="text/css" />
		<link rel="stylesheet" href="css/charts-all.css" type="text/css"  />
		<link rel="stylesheet" href="css/d3-all.css" type="text/css"  />

    <link rel="stylesheet" href="css/SALP.css" type="text/css"  />
    <link rel="stylesheet" href="css/help.css" type="text/css" />
    

    <script type="text/javascript" src="lib/ext-all.js"></script>
    <script type="text/javascript" src="lib/charts.js"></script>
    <script type="text/javascript" src="lib/exporter.js"></script>
    <script type="text/javascript" src="lib/d3.js"></script>

    <script type="text/javascript" src="data/Data.js"></script>
    <script type="text/javascript" src="src/Utility.js"></script>
    <script type="text/javascript" src="src/SALP.js"></script>
</head>
<body>
</body>
</html>
