﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.DirectoryServices;
using System.Collections;
using System.Web.Script.Serialization;

namespace SALP
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            // Set Session Object
            Session["Comcast"] = "Comcast";

            this.SetUser();
        }

        private void SetUser()
        {
            //string principal = this.Context.User.Identity.Name.Split('\\')[1];

            string principal = System.Web.HttpContext.Current.Request.LogonUserIdentity.Name.Split('\\')[1];
            string filter = string.Format("(&(ObjectClass={0})(sAMAccountName={1}))", "person", principal);

            string[] properties = new string[] { "fullname" };

            string defaultNamingContext;
            using (DirectoryEntry rootDSE = new DirectoryEntry("LDAP://RootDSE"))
            {
                defaultNamingContext = rootDSE.Properties["defaultNamingContext"].Value.ToString();
            }

            //DirectoryEntry : represents a node or object in the Active Directory hierarchy, 
            //DirectorySearcher : performs queries against Active Directory Domain Services. 
            DirectoryEntry adRoot = new DirectoryEntry("LDAP://" + defaultNamingContext); //, null, null, AuthenticationTypes.Secure);
            DirectorySearcher searcher = new DirectorySearcher(adRoot);
            searcher.SearchScope = SearchScope.Subtree;
            searcher.ReferralChasing = ReferralChasingOption.All;
            searcher.PropertiesToLoad.AddRange(properties);
            searcher.Filter = filter;
            SearchResult sResult = searcher.FindOne();
            DirectoryEntry directoryEntry = sResult.GetDirectoryEntry();


            //Finally, once information is retrieved in the DirectoryEntry object, we can get the details of each property defined.

            string displayName = directoryEntry.Properties["displayName"][0].ToString();
            string firstName = directoryEntry.Properties["givenName"][0].ToString();
            string lastName = directoryEntry.Properties["sn"][0].ToString();

            string accountName = directoryEntry.Properties["sAMAccountName"][0].ToString();

            var domainName = System.Environment.UserDomainName;
            var test2 = System.Net.NetworkInformation.IPGlobalProperties.GetIPGlobalProperties().DomainName;

            //string email = directoryEntry.Properties["mail"][0].ToString();
            //this.txtDepartment.Text = directoryEntry.Properties["department"][0].ToString();

            SALP.DataTypes.User user = new SALP.DataTypes.User();
            user.accountName = accountName;
            user.displayName = displayName;
            user.domainName = domainName;

            ArrayList groups = this.GetGroups();

            if(groups.IndexOf("CABLE\\CHQ - SALPAuth-FCC") > 0)
            {
                user.hasFCCAccess = true;
            }

            if (groups.IndexOf("CABLE\\CHQ - SALPAuth-Billing") > 0)
            {
                user.hasBillingAccess = true;
            }

            if (groups.IndexOf("CABLE\\CHQ - SALPAuth-BLR") > 0)
            {
                user.hasBLRAccess = true;
            }

            if (groups.IndexOf("CABLE\\CHQ-SALP_TTS_Bandaid") > 0)
            {
                user.hasMaintenanceAccess = true;
            }

            if (groups.IndexOf("CABLE\\CHQCDC-NETOPortal-SrvAdms") > 0)
            {
                user.hasXNOCOBIAccess = true;
            }


            //user.groups = this.GetGroups();

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            String cookieString = serializer.Serialize(user);

            // Create New Cookie
            HttpCookie cookie = new HttpCookie("SALP-User");

            cookie.Value = cookieString;

            // Save Cookie
            Response.Cookies.Add(cookie);
        }

    
        public ArrayList GetGroups()
        {
            ArrayList groups = new ArrayList();

            foreach (System.Security.Principal.IdentityReference group in System.Web.HttpContext.Current.Request.LogonUserIdentity.Groups)
            {
                try
                {
                    groups.Add(group.Translate(typeof(System.Security.Principal.NTAccount)).ToString());
                }
                catch
                {
                    groups.Add(group.ToString());
                }
            }
            return groups;
        }
    }
}