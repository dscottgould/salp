﻿//
//
//   Org TreeMap
//
//

Ext.define('SALP.d3.OrganizationTreeMap', 
{
	alias: 'widget.d3.OrganizationTreeMap',
	extend: 'Ext.d3.hierarchy.TreeMap',
	
	store: Ext.create('Ext.data.TreeStore', 
  {
  	//autoLoad: true,
  	model: 'OrgTreeMapModel',
  	proxy: 
  	{
    	type: 'ajax',
     	url: 'WebServices/Service.svc/GetCMTreeView',
     	pageParam: undefined,
  		startParam: undefined,
  		limitParam: undefined,
     	reader: 
     	{
      	type: 'json',
        //root: 'data'
     	}
  	}
  }),

	rootVisible: false,
	//selectEventName: null,
	//expandEventName: null,
		
            
	colorAxis: 
	{
  	scale: 
  	{
    	type: 'category20c'
    },
    field: 'text',
    processor: function (axis, scale, node, field) 
    {
    	var color = 'rgba(255,255,255, 1)';
    	
    	if(node.data.text.search('Success') >= 0)
    	{
    		color = 'rgba(126,255,119, 1)';
    	}
    	else if(node.data.text.search('Fail') >= 0)
    	{
    		color = 'rgba(255, 112, 112, 1)';
    	}
    	else
    	{
    		var failureRate = node.data.failureRate;
    		
    		if(failureRate == 0)
    		{
    			color = 'rgba(126,255,119, 1)';
    		}
    		
    		if((failureRate <= 5) && (failureRate > 0))
    		{
    			color = 'rgba(195, 255, 175, 1)';
    		}
    		
    		if((failureRate > 5) && (failureRate <= 12))
    		{
    			color = 'rgba(255, 178, 147, 1)';
    		}
    		
    		if((failureRate > 12) && (failureRate <= 20))
    		{
    			color = 'rgba(255, 137, 143, 1)';
    		}
    		
    		if(failureRate > 20)
    		{
    			color = 'rgba(255, 112, 112, 1)';
    		}
    	}
    	
    	return color
    }
  },
			
  
  tooltip: 
  {
  	cls: 'tip',
    style: 'background-color: #e67e22;border-color: #747474;border-radius: 8px;',
    trackMouse: true,
    renderer: function(component, tooltip, node, element, event)
    {
 			var tpl, html;
 			
 			if(node.data.isParentNode)
 			{
 				tpl = this.lookupTpl('parentTpl');
 			}
 			
 			if(node.data.isChildNode)
 			{
 				tpl = this.lookupTpl('childTpl');
 			}
 			
 			if(node.data.isAttributeNode)
 			{
 				tpl = this.lookupTpl('attributeTpl');
 			}

      //component.setSelection(node);

      html = tpl.apply(node);
      tooltip.setHtml(html);
    }
 	},
 	
	parentTpl: [
    '<div class="tip-title">{data.text}</div>',
    '<tpl for="childNodes">',
    '<div><span class="tip-attribute">{data.text} - {data.value} Total</span></div>',
    '<tpl if="xindex &gt; 10">...{% break; %}</tpl>',
    '</tpl>'
	],

	childTpl: [
    '<div class="tip-title">{data.text}</div>',
    '<tpl for="childNodes">',
    '<div><span class="tip-attribute">{data.text}</span></div>',
    '<tpl if="xindex &gt; 10">...{% break; %}</tpl>',
    '</tpl>'
	],
	
  attributeTpl: [
  	'<div class="tip-attribute">{data.text}</div>',
//    '<div>{data.value}</div>'
  ], 
  
});