﻿//
//
//  FCC Threshold Breached Grid
//
//

Ext.define('SALP.grid.FCCThresholdBreachedGrid', 
{
	alias: 'widget.grid.FCCThresholdBreachedGrid',
	extend: 'Ext.grid.Panel',
	
	stripeRows: true,
  columnLines: true, 

  emptyText: '<span style="color:red">No recent thresholds breached or tickets not worked.</span>',
	
	initComponent: function()
  {
		this.store = this.buildStore();
		
		this.columns = this.buildColumns();
		
		this.listeners = this.buildListeners();
		
		this.task = new Ext.util.DelayedTask(function()
		{
			if (this.store)
			{
				this.store.reload();
			}
  	
  		Ext.defer(function() 
      {
      	this.setLoading(false);
      }, 2000, this);
  	
  		this.task.delay(60000);
  		
		}, this);
		
		this.task.delay(60000);

		//this.task.delay(60000);
		
    this.callParent(arguments);
  },
  
  
  
  buildStore: function()
  {
  	return Ext.create('Ext.data.Store', 
    {
    	autoLoad: true,
    	model: SALP.data.FCCThresholdBreached.model,
    	proxy: 
    	{
      	type: 'ajax',
       	url: 'WebServices/Service.svc/GetFCCThresholdBreachedData',
       	extraParams:
       	{
					//server: 'OBIDB-WC-P01'
       	},
       	pageParam: undefined,
    		startParam: undefined,
    		limitParam: undefined,
       	reader: 
       	{
        	type: 'json',
          root: 'data'
       	}
    	}
    });
  },
  
  
  buildColumns: function()
  {
  	return [
  	{
  		text: 'Ticket',
      sortable : true,
      dataIndex: 'ticketId',
      width: 80,
      renderer: function(value, metaData, record, rowIndex, colIndex, store, view)
      {
      	if(record.data.rcorStatus == 'NOT WORKED')
      	{
      		metaData.style = "color:red;";
      	}

      	return value
      }
  	},
  	{
  		text: 'Monitor',
      sortable : true,
      dataIndex: 'monitor',
      width: 50,
      renderer: function(value, metaData, record, rowIndex, colIndex, store, view)
      {
      	if(record.data.rcorStatus == 'NOT WORKED')
      	{
      		metaData.style = "color:red;";
      	}

      	return value
      }
  	},
  	{
  		text: 'Threshold',
      sortable : true,
      dataIndex: 'threshhold',
      width: 130,
      renderer: function(value, metaData, record, rowIndex, colIndex, store, view)
      {
      	if(record.data.rcorStatus == 'NOT WORKED')
      	{
      		metaData.style = "color:red;";
      	}

      	return value
      }
  	},
  	{
  		text: 'Description',
      sortable : true,
      dataIndex: 'thStatus',
      width: 90,
      renderer: function(value, metaData, record, rowIndex, colIndex, store, view)
      {
      	if(record.data.rcorStatus == 'NOT WORKED')
      	{
      		metaData.style = "color:red;";
      	}

      	return value
      }
  	},
  	{
  		text: 'Monitor Discovery',
      sortable : true,
      dataIndex: 'monitorDiscovery',
      width: 130,
      renderer: function(value, metaData, record, rowIndex, colIndex, store, view)
      {
      	if(record.data.rcorStatus == 'NOT WORKED')
      	{
      		metaData.style = "color:red;";
      	}

      	return value
      }
  	},
  	{
  		text: 'CES',
      sortable : true,
      dataIndex: 'ces',
      width: 50,
      renderer: function(value, metaData, record, rowIndex, colIndex, store, view)
      {
      	if(record.data.rcorStatus == 'NOT WORKED')
      	{
      		metaData.style = "color:red;";
      	}

      	return value
      }
  	},
  	{
  		text: 'Email',
      sortable : true,
      dataIndex: 'email',
      width: 50,
      renderer: function(value, metaData, record, rowIndex, colIndex, store, view)
      {
      	if(record.data.rcorStatus == 'NOT WORKED')
      	{
      		metaData.style = "color:red;";
      	}

      	return value
      }
  	},
  	{
  		text: 'RCOR Status',
      sortable : true,
      dataIndex: 'rcorStatus',
      width: 130,
      renderer: function(value, metaData, record, rowIndex, colIndex, store, view)
      {
      	if(record.data.rcorStatus == 'NOT WORKED')
      	{
      		metaData.style = "color:red;";
      	}

      	return value
      }
  	},
  	{
  		text: 'RCOR Date',
      sortable : true,
      dataIndex: 'rcorDate',
      width: 130,
      renderer: function(value, metaData, record, rowIndex, colIndex, store, view)
      {
      	if(record.data.rcorStatus == 'NOT WORKED')
      	{
      		metaData.style = "color:red;";
      	}

      	return value
      }
  	},
  	{
  		text: 'Worked By',
      sortable : true,
      dataIndex: 'workedBy',
      width: 100,
      renderer: function(value, metaData, record, rowIndex, colIndex, store, view)
      {
      	if(record.data.rcorStatus == 'NOT WORKED')
      	{
      		metaData.style = "color:red;";
      	}

      	return value
      }
  	}]
 	},
 	
 	buildListeners: function()
  {
  	return {
  		rowdblclick: function(grid, record, object, rowIndex)
      {
	    	var ticketId = record.data.ticketId
	    	
	    	SALP.getTicketDetails(ticketId);
	    	
	    	//SALP.openSite('http://sams.cable.comcast.com/cgi-bin/arnie.cgi?OPT_M=SI_DETAIL&TICKET_ID=' + ticketId);
     	},
      scope: this
    }
  }
});