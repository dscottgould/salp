﻿//
//
//  Customer Tickets Grid
//
//

Ext.define('SALP.grid.CustomerTicketsGrid', 
{
	alias: 'widget.grid.CustomerTicketsGrid',
	extend: 'Ext.grid.Panel',
	
	stripeRows: true,
  columnLines: true, 

  
	
	initComponent: function()
  {
		this.store = this.buildStore();
		
		this.columns = this.buildColumns();
		
		
    this.callParent(arguments);
  },
  
  
  
  buildStore: function()
  {
  	return Ext.create('Ext.data.Store', 
    {
    	autoLoad: true,
    	model: SALP.data.Incidents.model,
    	proxy: 
    	{
      	type: 'ajax',
       	url: 'WebServices/Service.svc/GetCustomerTickets',
       	extraParams:
       	{
					ticketNumber: this.ticketNumber
       	},
       	pageParam: undefined,
    		startParam: undefined,
    		limitParam: undefined,
       	reader: 
       	{
        	type: 'json',
          root: 'data'
       	}
    	},
    	listeners: 
    	{
				'load': function(store, records, successfull, operation, options)
				{
					if(records.length <= 1) this.setHeight(0);
				},
				scope: this
			}
    });
  },
  
  buildColumns: function()
  {
  	return [
  	{
  		text: 'Customer Ticket Id',
      sortable : true,
      dataIndex: 'customerTicketId',
      width: 86
  	},
  	{
  		text: 'Summary',
      sortable : true,
      dataIndex: 'problemSummary',
      width: 254
  	},
  	{
  		text: 'Create Date',
      sortable : true,
      dataIndex: 'createDate',
      width: 125
  	},
  	{
  		text: 'Contact',
      sortable : true,
      dataIndex: 'contactName',
      width: 170
  	},
  	{
  		text: 'Status',
      sortable : true,
      dataIndex: 'status',
      width: 49
  	},
  	{
  		text: 'Node',
      sortable : true,
      dataIndex: 'node',
      width: 69 
  	}]
  }
});