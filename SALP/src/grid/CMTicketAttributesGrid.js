﻿//
//
//  CM Ticket Attributes Grid
//
//

Ext.define('SALP.grid.CMTicketAttributesGrid', 
{
	alias: 'widget.grid.CMTicketAttributesGrid',
	extend: 'Ext.grid.Panel',
	
	stripeRows: true,
  columnLines: true, 
	hideHeaders: true,
  
	
	initComponent: function()
  {
		this.store = this.buildStore();
		
		this.columns = this.buildColumns();
		
    this.callParent(arguments);
  },
  
  
  
  buildStore: function()
  {
  	return Ext.create('Ext.data.Store', 
    {
    	autoLoad: true,
    	model: 'CMTicketAttributes',
    	proxy: 
    	{
      	type: 'ajax',
       	url: 'WebServices/Service.svc/GetCMTicketAttributes',
       	extraParams:
       	{
					ticketNumber: this.ticketNumber
       	},
       	pageParam: undefined,
    		startParam: undefined,
    		limitParam: undefined,
       	reader: 
       	{
        	type: 'json',
          root: 'data'
       	}
    	}
//    	listeners: 
//    	{
//				'load': function(store, records, successfull, operation, options)
//				{
//					this.setHeight(30 + records.length * 20);
//				},
//				scope: this
//			}
    });
  },
  
  
  buildColumns: function()
  {
  	return [
  	{
  		text: '',
      dataIndex: 'attributeLabel',
      flex: 1,
      renderer: function(value, metaData, record, rowIdx, colIdx, store) 
      {
        return '<b>' + value + '</b>';
    	}
  	},
  	{
  		text: '',
      dataIndex: 'attributeValue',
      flex: 1,
      renderer: function(value, metaData, record, rowIdx, colIdx, store) 
      {
        metaData.tdAttr = 'data-qtip="' + value + '"';
        return value;
    	}
  	}]
  }
});