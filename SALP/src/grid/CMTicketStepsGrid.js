﻿//
//
//  CM Ticket Steps Grid
//
//

Ext.define('SALP.grid.CMTicketStepsGrid', 
{
	alias: 'widget.grid.CMTicketStepsGrid',
	extend: 'Ext.grid.Panel',
	
	stripeRows: true,
  columnLines: true, 
  
	
	initComponent: function()
  {
		this.store = this.buildStore();
		
		this.columns = this.buildColumns();
		
    this.callParent(arguments);
  },
  
  
  
  buildStore: function()
  {
  	return Ext.create('Ext.data.Store', 
    {
    	autoLoad: true,
    	model: 'CMTicketSteps',
    	proxy: 
    	{
      	type: 'ajax',
       	url: 'WebServices/Service.svc/GetCMTicketSteps',
       	extraParams:
       	{
					ticketNumber: this.ticketNumber
       	},
       	pageParam: undefined,
    		startParam: undefined,
    		limitParam: undefined,
       	reader: 
       	{
        	type: 'json',
          root: 'data'
       	}
    	}
//    	listeners: 
//    	{
//				'load': function(store, records, successfull, operation, options)
//				{
//					this.setHeight(30 + records.length * 20);
//				},
//				scope: this
//			}
    });
  },
  
  
  buildColumns: function()
  {
  	return [
  	{
  		text: 'Step Label',
      dataIndex: 'stepLabel',
      flex: 1
  	},
  	{
  		text: 'Step Description',
      dataIndex: 'stepDescription',
      flex: 1,
      renderer: function(value, metaData, record, rowIdx, colIdx, store) 
      {
        metaData.tdAttr = 'data-qtip="' + value + '"';
        return value;
    	}
  	},
  	{
  		text: 'Sequence',
      dataIndex: 'stepSequence',
      flex: 1
  	},
  	{
  		text: 'Procedure ID',
      dataIndex: 'procedureId',
      flex: 1
  	},
  	{
  		text: 'Actual Start',
      dataIndex: 'actualStart',
      flex: 1,
      renderer: function(value, metaData, record, rowIdx, colIdx, store) 
      {
        metaData.tdAttr = 'data-qtip="' + value + '"';
        return value;
    	}
  	},
  	{
  		text: 'Actual End',
      dataIndex: 'actualEnd',
      flex: 1,
      renderer: function(value, metaData, record, rowIdx, colIdx, store) 
      {
        metaData.tdAttr = 'data-qtip="' + value + '"';
        return value;
    	}
  	}]
  }
});