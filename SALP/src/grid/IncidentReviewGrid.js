﻿//
//
//  Incident Review Grid
//
//

Ext.define('SALP.grid.IncidentReviewGrid', 
{
	itemId: 'IncidentReviewGrid',
	alias: 'widget.grid.IncidentReviewGrid',
	extend: 'Ext.grid.Panel',
	
	
	stripeRows: true,
  columnLines: true, 
  
  
  initComponent: function()
  {
		this.store = this.buildStore();
		
		this.columns = this.buildColumns();
		
    this.callParent(arguments);
  },
  
   
  buildStore: function()
  {
  	return Ext.create('Ext.data.Store', 
    {
    	autoLoad: true,
    	model: SALP.data.IncidentReview.model,
    	proxy: 
    	{
      	type: 'ajax',
       	url: 'WebServices/Service.svc/GetNetworkOperationsIncidentReview',
    //   	extraParams:
    //   	{
    //	  	accountName: SALP.user.accountName
    //   	},
       	pageParam: undefined,
    		startParam: undefined,
    		limitParam: undefined,
       	reader: 
       	{
        	type: 'json',
          root: 'data'
       	}
    	}
    })
  },
  
  
  buildColumns: function()
  {
  	return [
  	{
  		text: '<b>Network Lens</b>',
  		dataIndex: 'label',
  		sortable : false,
  		menuDisabled: true,
  		width: 170,
 			renderer: function(value, metaData, record, rowIndex, colIndex, store, view)
      {
      	if(value == "CRAN & Backbone")
      	{
      		value = '<b>' + value + '</b>'
      	}
      	else
      	{
      		value = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + value
      	}
      	
      	var t=1;
      	
      	return value
      }
  	},
  	{
  		text: '<b>Incidents</b>',
			menuDisabled: true,
			sortable : false,
			columns: [
			{
				text: 'MTBF<br/>Days',
				dataIndex: 'mtbf',
				menuDisabled: true,
				sortable : false,
				width: 40
			},
			{
				text: 'Freq',
				dataIndex: 'frequency',
				menuDisabled: true,
				sortable : false,
				width: 40
			},
			{
				text: 'WoW<br/>Freq',
				dataIndex: 'wowFrequency',
				menuDisabled: true,
				sortable : false,
				width: 40,
  			renderer: function(value, metaData, record, rowIndex, colIndex, store, view)
        {
        	var returnValue = '<img src="',
        			value = parseInt(value);
        	
        	if(value < 0)
        	{
        		returnValue += 'img/IndicatorGreen.png" />' 
        	}
        	
        	if(value == 0)
        	{
        		returnValue += 'img/IndicatorYellow.png" />' 
        	}
        	
        	if(value > 0)
        	{
        		returnValue += 'img/IndicatorRed.png" />' 
        	}
        	
        	return returnValue
        }
			},
			{
				text: 'Change<br/>Related',
				dataIndex: 'changeRelated',
				menuDisabled: true,
				sortable : false,
				width: 55
			}]
  	},
  	{
  		text: '<b>Avg. Restoration (min)</b>',
			menuDisabled: true,
			sortable : false,
			columns:[
			{
				text: 'Detect',
				dataIndex: 'detect',
				menuDisabled: true,
				sortable : false,
				width: 50
			},
			{
				text: 'Engage',
				dataIndex: 'engage',
				menuDisabled: true,
				sortable : false,
				width: 50
			},
			{
				text: 'Restore',
				dataIndex: 'resolve',
				menuDisabled: true,
				sortable : false,
				width: 60
			},
			{
				text: 'WoW<br/>Restore',
				dataIndex: 'wowResolve',
				menuDisabled: true,
				sortable : false,
				width: 60,
				renderer: function(value, metaData, record, rowIndex, colIndex, store, view)
        {
        	var returnValue = '<img src="',
        			value = parseInt(value);
        	
        	if(value < 0)
        	{
        		returnValue += 'img/IndicatorGreen.png" />' 
        	}
        	
        	if(value == 0)
        	{
        		returnValue += 'img/IndicatorYellow.png" />' 
        	}
        	
        	if(value > 0)
        	{
        		returnValue += 'img/IndicatorRed.png" />' 
        	}
        	
        	return returnValue
        }
			}]
  	},
  	{
  		text: '<b>Root Cause</b>',
			menuDisabled: true,
			sortable : false,
			columns:[
			{
				text: 'Config',
				dataIndex: 'config',
				menuDisabled: true,
				sortable : false,
				width: 50
			},
			{
				text: '<center>HW/<br/>SW</center>',
				dataIndex: 'hwSw',
				menuDisabled: true,
				sortable : false,
				width: 50
			},
			{
				text: '<center>As<br/>Design</center>',
				dataIndex: 'asDesign',
				menuDisabled: true,
				sortable : false,
				width: 50
			},
			{
				text: 'Execution<br/>Defect',
				dataIndex: 'executionDefect',
				menuDisabled: true,
				sortable : false,
				width: 65
			}]
  	},
  	{
  		text: '<b>Impact</b>',
			menuDisabled: true,
			sortable : false,
			columns:[
			{
				text: '<center>Incr Call<br/>%</center>',
				dataIndex: 'impact',
				menuDisabled: true,
				sortable : false,
				width: 60
			}]
		}]
  }
});