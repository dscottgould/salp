﻿//
//
//  Conflict Bypass Grid
//
//

Ext.define('SALP.grid.ConflictBypassGrid', 
{
	alias: 'widget.grid.ConflictBypassGrid',
	extend: 'Ext.grid.Panel',
	
	stripeRows: true,
  columnLines: true, 
  
  
//  viewConfig : 
//  {
//  	enableTextSelection: true
//  },

	
	initComponent: function()
  {
  	this.header = this.buildHeader();
  			
		this.store = this.buildStore();
		
		this.columns = this.buildColumns();
		
		
    this.callParent(arguments);
  },
  
 	buildHeader: function()
  {
  	return   {
    	itemPosition: 1, // after title before collapse tool
    	items: [
    	{
				xtype: 'combobox',
				store: SALP.data.ConflictTicket.select,
				multiSelect: true,
				editable: false,
				forceSelection: true,
				queryMode: 'local',
				displayField: 'displayName',
				valueField: 'submitValue',
    		listeners : 
    		{
    			select : function(comboBox, selection, object) 
    			{
    				var selectedMonths = [];
    				
    				for(var index = 0; index <= selection.length - 1; index ++)
    				{
    					selectedMonths.push(selection[index].data.submitValue);
    				}
    				
  					this.store.load
        		({
        			params:
        			{
								months: selectedMonths
        			}
        		});
      		},
      		afterrender: function(comboBox, object)
      		{
      			comboBox.select(comboBox.getStore().collect(comboBox.valueField));
      		},
      		scope: this
      	}
    	}]
  	}
  },
  
  buildStore: function()
  {
  	return Ext.create('Ext.data.Store', 
    {
    	autoLoad: true,
    	model: SALP.data.ConflictTicket.model,
    	proxy: 
    	{
      	type: 'ajax',
       	url: 'WebServices/Service.svc/GetConflictTickets',
       	timeout: 100000, 
       	extraParams:
       	{
       		months: 'January, February, March, April, May, June, July, August, September, October, November, December'
       	},
       	pageParam: undefined,
    		startParam: undefined,
    		limitParam: undefined,
       	reader: 
       	{
        	type: 'json',
          root: 'data'
       	}
    	}
    });
  },
  buildColumns: function()
  {
  	return [
  	{
  		text: 'Submitter Name',
      flex: 1,
      sortable : true,
      dataIndex: 'submitterName',
      renderer: function(value, metaData, record, rowIdx, colIdx, store) 
      {
        metaData.tdAttr = 'data-qtip="' + value + '"';
        return value;
    	}
  	},
  	{
  		text: 'Supervisor Name',
      flex: 1,
      sortable : true,
      dataIndex: 'supervisorName',
      renderer: function(value, metaData, record, rowIdx, colIdx, store) 
      {
        metaData.tdAttr = 'data-qtip="' + value + '"';
        return value;
    	}
  	},
  	{
  		text: 'Bypassed',
      flex: .5,
      sortable : true,
      dataIndex: 'bypassed'
  	},
  	{
  		text: 'Not Bypassed',
      flex: .5,
      sortable : true,
      dataIndex: 'notBypassed'
  	},
  	{
  		text: 'Total',
      flex: .5,
      sortable : true,
      dataIndex: 'total'
  	},
  	{
  		text: '% Not Bypassed',
      flex: .6,
      sortable : true,
      dataIndex: 'percentNotBypassed',
      renderer: function(value, metaData, record, rowIndex, colIndex, store, view)
      {
      	var valueFloat = parseFloat(value);
      	
      	if(valueFloat < 44.5)
      	{
      		metaData.style = "background-color:#BD1100;";
      	}
      	
      	if((valueFloat <= 50.0) && (valueFloat > 44.5))
      	{
      		metaData.style = "background-color:#D23A21;";
      	}
      	
      	if((valueFloat <= 56.30) && (valueFloat > 50.0))
      	{
      		metaData.style = "background-color:#E6583E;";
      	}
      	
      	if((valueFloat <= 61.50) && (valueFloat > 56.30))
      	{
      		metaData.style = "background-color:#F7705B;";
      	}
      	
      	if((valueFloat <= 67.00) && (valueFloat > 61.50))
      	{
      		metaData.style = "background-color:#FE8E7E;";
      	}
      	
      	if((valueFloat <= 75.00) && (valueFloat > 67.00))
      	{
      		metaData.style = "background-color:#B1DE7F;";
      	}
      	
      	if((valueFloat < 77.50) && (valueFloat >= 75.00))
      	{
      		metaData.style = "background-color:#90CB68;";
      	}
      	
      	if((valueFloat < 85.50) && (valueFloat >= 77.50))
      	{
      		metaData.style = "background-color:#75B65D;";
      	}
      	
      	if((valueFloat < 90.50) && (valueFloat >= 85.50))
      	{
      		metaData.style = "background-color:#5EA049;";
      	}
      	
      	if((valueFloat <= 100.00) && (valueFloat >= 90.50))
      	{
      		metaData.style = "background-color:#4A8C1C;";
      	}
      	
      	return value + '%';
      }
  	}]
  }
});