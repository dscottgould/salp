﻿//
//
//  Server Processes Basic Grid
//
//

Ext.define('SALP.grid.ServerProcessesBasicGrid', 
{
	alias: 'widget.grid.ServerProcessesBasicGrid',
	extend: 'Ext.grid.Panel',
	
	stripeRows: true,
  columnLines: true, 

	emptyText: 'No Server Processes Found.', 
	
	initComponent: function()
  {
		this.store = this.buildStore();
		
		this.columns = this.buildColumns();
		
		this.listeners = this.buildListeners();
		
		this.task = new Ext.util.DelayedTask(function()
		{
			if (this.store)
			{
				this.store.reload();
			}
  	
  		Ext.defer(function() 
      {
      	this.setLoading(false);
      }, 2000, this);
  	
  		this.task.delay(60000);
  		
		}, this);
		
		
		this.task.delay(60000);
		
    this.callParent(arguments);
  },
  
  
  
  buildStore: function()
  {
  	return Ext.create('Ext.data.Store', 
    {
    	autoLoad: true,
    	model: SALP.data.CurrentServerProcesses.model,
    	proxy: 
    	{
      	type: 'ajax',
       	url: 'WebServices/Service.svc/GetServerProcesses',
       	extraParams:
       	{
					serverName: this.server.trim()
       	},
       	pageParam: undefined,
    		startParam: undefined,
    		limitParam: undefined,
       	reader: 
       	{
        	type: 'json',
          root: 'data'
       	}
    	},
    	listeners: [
    	{
    		beforeload: function()
    		{
    			this.removeAll();
    		}
    	}]
    });
  },
  
  
  buildColumns: function()
  {
  	return [
  	{
  		text: 'Session ID',
      sortable : true,
      dataIndex: 'sessionId',
      width: 65
  	},
  	{
  		text: 'Database Name',
      sortable : true,
      dataIndex: 'databaseName',
      width: 90
  	},
  	{
  		text: 'Running Minutes',
      sortable : true,
      dataIndex: 'runningMinutes',
      width: 95
  	},
  	{
  		text: 'Blocked By',
      sortable : true,
      dataIndex: 'blockedBy',
      width: 65,
      renderer: function(value, metaData, record, rowIndex, colIndex, store, view)
      {
      	var blockedBy = record.data.blockedBy;
      	
      	if(blockedBy > 0)
      	{
      		metaData.style = "background-color:#FC8F8F;";
      	}
      	
      	return value;
      }
  	},
  	{
  		text: 'Program Name',
      sortable : true,
      dataIndex: 'programName',
      flex: 1,
      renderer: function(value, metaData, record, rowIndex, colIndex, store, view)
      {
      	metaData.tdAttr = 'data-qtip="' + value + '"';
      	
      	return value;
      }
  	}]
 	},
 	
 	buildListeners: function()
  {
  	return {
  		rowdblclick: function(grid, record, object, rowIndex)
      {
      	if(Ext.getCmp('ServerCurrentProcessesWindow'))
      	{
      		SALP.view.ServerCurrentProcessesWindow.close();
      	}

    		SALP.view.ServerCurrentProcessesWindow = Ext.create('SALP.view.ServerCurrentProcessesWindow',
    		{
    			server: this.server
    		}).show();

     	},
      scope: this
    }
  }
});