﻿//
//
//  Ticket Delay Display Grid
//
//

Ext.define('SALP.grid.TicketDelayDisplayGrid', 
{
	alias: 'widget.grid.TicketDelayDisplayGrid',
	extend: 'Ext.grid.Panel',
	
	stripeRows: true,
  columnLines: true, 

  
	
	initComponent: function()
  {
		this.store = this.buildStore();
		
		this.columns = this.buildColumns();
		
    this.callParent(arguments);
  },
  
  
  
  buildStore: function()
  {
  	return Ext.create('Ext.data.Store', 
    {
    	autoLoad: true,
    	model: SALP.data.CRTicketDelayDisplay.model,
    	proxy: 
    	{
      	type: 'ajax',
       	url: 'WebServices/Service.svc/GetCRTicketDelayDisplay',
       	timeout: 60000, 
       	extraParams:
       	{
					//server: 'OBIDB-WC-P01'
       	},
       	pageParam: undefined,
    		startParam: undefined,
    		limitParam: undefined,
       	reader: 
       	{
        	type: 'json',
          root: 'data'
       	}
    	}
    });
  },
  
  
  buildColumns: function()
  {
  	return [
  	{
  		text: 'CR Ticket Lag',
      sortable : true,
      dataIndex: 'name',
      flex: 1,
      renderer: function(value, metaData, record, rowIndex, colIndex, store, view)
      {
      	if((rowIndex == 1) && (store.getAt(3).data.value == "RED"))
      	{
      		metaData.style = "color:red;";
      	}
      	
      	if(rowIndex == 3)
      	{
      		value = ""
      	}

      	return value
      }
  	},
  	{
  		text: 'Max Time',
      sortable : true,
      dataIndex: 'value',
      flex: .7,
      renderer: function(value, metaData, record, rowIndex, colIndex, store, view)
      {
      	if((rowIndex == 1) && (store.getAt(3).data.value == "RED"))
      	{
      		metaData.style = "color:red;";
      	}
      	
      	if(rowIndex == 3)
      	{
      		value = ""
      	}

      	return value
      }
  	}]
 	}
});