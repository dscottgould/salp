﻿//
//
// Maintenance Caused Sev1 Month Over Month Grid
//
//

Ext.define('SALP.grid.MaintenanceCausedSev1MonthOverMonthGrid', 
{
	alias: 'widget.grid.MaintenanceCausedSev1MonthOverMonthGrid',
	extend: 'Ext.grid.Panel',
	
	cls: 'MaintenanceCausedSev1MonthOverMonthGrid',
	stripeRows: true,
  columnLines: true, 
  scrollable: true,


	emptyText: 'No Records Found.', 
	
	initComponent: function()
  {
		this.store = this.buildStore();
		
		this.columns = this.buildColumns();
		
		this.listeners = this.buildListeners();
		
    this.callParent(arguments);
  },
  
    
  buildStore: function()
  {
  	return Ext.create('Ext.data.Store', 
    {
    	autoLoad: true,
    	model: SALP.data.MaintenanceCausedSev1MonthOverMonth.model,
    	proxy: 
    	{
      	type: 'ajax',
       	url: 'WebServices/Service.svc/GetMaintanceCausedSev1MonthOverMonth',
//       	extraParams:
//       	{
//    	  	hasMonitoredProcesses: 'True'
//       	},
       	pageParam: undefined,
    		startParam: undefined,
    		limitParam: undefined,
       	reader: 
       	{
        	type: 'json',
          root: 'data'
       	},
       	timeout: 1200000
    	}
    });
  },
  
  buildColumns: function()
  {
  	return [
  	{
  		text: 'Month',
      sortable : true,
      dataIndex: 'maintenanceMonth',
      width: 50,
      renderer: function(value, metaData, record, rowIndex, colIndex, store, view)
      {
      	if (SALP.grid.column0 == true)
      	{
      		metaData.style = "background-color:#FFFFFF;";
      		SALP.grid.column0 = false;
      	}
      	else
      	{
      		metaData.style = "background-color:#BCBCBC;";
      		SALP.grid.column0 = true;
      	}
      	
      	return value
      }   
  	},
  	{
  		text: 'Fail Count',
  		columns:[
  		{
  			text: '<center>Previous Year</center>',
        sortable : true,
        dataIndex: 'failCountPreviousYear',
        width: 100,
        renderer: function(value, metaData, record, rowIndex, colIndex, store, view)
        {   
        	var year = new Date().getFullYear() - 1;
        	
        	metaData.tdAttr = 'data-qtip="Double Click For ' + year + ' Details"';
        	   	
        	if (SALP.grid.column1 == true)
        	{
        		metaData.style = "background-color:#FFB293;";
        		SALP.grid.column1 = false;
        	}
        	else
        	{
        		metaData.style = "background-color:#FF7070;";
        		SALP.grid.column1 = true;
        	}
        	
        	value = Ext.util.Format.number(value, '0,000');
        	
        	return value
        }
  		}]
  	},
  	{
  		text: 'Fail Count',
  		columns:[
  		{
    		text: '<center>Current Year</center>',
        sortable : true,
        dataIndex: 'failCountCurrentYear',
        width: 100,
        renderer: function(value, metaData, record, rowIndex, colIndex, store, view)
        {      	
        	var year = new Date().getFullYear();
        	
        	metaData.tdAttr = 'data-qtip="Double Click For ' + year + ' Details"';
        	
        	if (SALP.grid.column2 == true)
        	{
        		metaData.style = "background-color:#C3FFAF;";
        		SALP.grid.column2 = false;
        	}
        	else
        	{
        		metaData.style = "background-color:#7EFF77;";
        		SALP.grid.column2 = true;
        	}
        	
        	value = Ext.util.Format.number(value, '0,000');
        	
        	return value
        }
  		}]
  	},
  	{
  		text: 'Fail %',
  		columns:[
  		{
    		text: '<center>Previous Year</center>',
        sortable : true,
        dataIndex: 'failPctPreviousYear',
        width: 100,
        renderer: function(value, metaData, record, rowIndex, colIndex, store, view)
        {
        	var year = new Date().getFullYear() - 1;
        	
        	metaData.tdAttr = 'data-qtip="Double Click For ' + year + ' Details"';
        	      	
        	if (SALP.grid.column3 == true)
        	{
        		metaData.style = "background-color:#FFB293;";
        		SALP.grid.column3 = false;
        	}
        	else
        	{
        		metaData.style = "background-color:#FF7070;";
        		SALP.grid.column3 = true;
        	}
        	
        	if(value)
        	{
        		
        		return value + "%";
        	}
        	else
      		{
      			return value
      		}
        }
  		}]
  	},
  	{
  		text: 'Fail %',
  		columns:[
  		{
  			text: '<center>Current Year</center>',
        sortable : true,
        dataIndex: 'failPctCurrentYear',
        width: 100,
        renderer: function(value, metaData, record, rowIndex, colIndex, store, view)
        {      	
        	var year = new Date().getFullYear();
        	
        	metaData.tdAttr = 'data-qtip="Double Click For ' + year + ' Details"';
        	
        	if (SALP.grid.column4 == true)
        	{
        		metaData.style = "background-color:#C3FFAF;";
        		SALP.grid.column4 = false;
        	}
        	else
        	{
        		metaData.style = "background-color:#7EFF77;";
        		SALP.grid.column4 = true;
        	}
        	
        	if(value)
        	{
        		return value + "%";
        	}
        	else
      		{
      			return value
      		}
        }
  		}]
  	},
  	{
  		text: 'Total Count',
  		columns:[
  		{
    		text: '<center>Previous Year</center>',
        sortable : true,
        dataIndex: 'totalCountPreviousYear',
        width: 100,
        renderer: function(value, metaData, record, rowIndex, colIndex, store, view)
        {      	
        	var year = new Date().getFullYear() - 1;
        	
        	metaData.tdAttr = 'data-qtip="Double Click For ' + year + ' Details"';
        	
        	if (SALP.grid.column5 == true)
        	{
        		metaData.style = "background-color:#FFB293;";
        		SALP.grid.column5 = false;
        	}
        	else
        	{
        		metaData.style = "background-color:#FF7070;";
        		SALP.grid.column5 = true;
        	}
        	
        	value = Ext.util.Format.number(value, '0,000');
        	
        	return value
        }
  		}]
  	},
  	{
  		text: 'Total Count',
  		columns:[
  		{
    		text: '<center>Current Year</center>',
        sortable : true,
        dataIndex: 'totalCountCurrentYear',
        width: 100,
        renderer: function(value, metaData, record, rowIndex, colIndex, store, view)
        {      	
        	var year = new Date().getFullYear();
        	
        	metaData.tdAttr = 'data-qtip="Double Click For ' + year + ' Details"';
        	
        	if (SALP.grid.column6 == true)
        	{
        		metaData.style = "background-color:#C3FFAF;";
        		SALP.grid.column6 = false;
        	}
        	else
        	{
        		metaData.style = "background-color:#7EFF77;";
        		SALP.grid.column6 = true;
        	}
        	
        	value = Ext.util.Format.number(value, '0,000');
        	
        	return value
        }
  		}]
  	}]
  },
  
  buildListeners: function()
  {
  	return {
     	'celldblclick': function(view, td, columnIndex, record, tr, rowIndex, event) 
     	{
     		var year;
     		
     		switch(columnIndex)
     		{
     			case 0:
     			case 2:
     			case 4:
     			case 6:
     				year = new Date().getFullYear()
     				break;
     				
     			case 1:
     			case 3:
     			case 5:
     				year = new Date().getFullYear() - 1
     				break;
     		}
     		
			  Ext.create('SALP.view.AllMaintenanceSuccessFailureMonthOverMonthWindow',
				{
					month: record.data.maintenanceMonth,
					year: year,
					constrain: true,
					//constrainTo: Ext.getCmp('app-tabPanel').getEl()
				}).show()
     	},
      scope: this
    }
  }
});