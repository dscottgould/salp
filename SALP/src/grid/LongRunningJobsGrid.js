﻿//
//
//  LongRunningJobsGrid.js
//
//

Ext.define('SALP.grid.LongRunningJobsGrid', 
{
	alias: 'widget.grid.LongRunningJobsGrid',
	extend: 'Ext.grid.Panel',
	
	stripeRows: true,
  columnLines: true, 

	emptyText: 'No Long Running Jobs Found.',  
	
	initComponent: function()
  {
		this.store = this.buildStore();
		
		this.columns = this.buildColumns();
		
		this.task = new Ext.util.DelayedTask(function()
		{
  		this.setLoading('Loading...');
  		
  		if(this.store)
  		{
  			this.store.reload();
  		}
  	
  		Ext.defer(function() 
      {
      	this.setLoading(false);
      }, 2000, this);
  	
  		this.task.delay(900000);
  		
		}, this);
		

		this.task.delay(900000);
		
    this.callParent(arguments);
  },
  
  
  
  buildStore: function()
  {
  	return Ext.create('Ext.data.Store', 
    {
    	autoLoad: true,
    	model: SALP.data.WorkLog.model,
    	proxy: 
    	{
      	type: 'ajax',
       	url: 'WebServices/Service.svc/GetServerJobs',
       	extraParams:
       	{
					serverName: this.server
       	},
       	pageParam: undefined,
    		startParam: undefined,
    		limitParam: undefined,
       	reader: 
       	{
        	type: 'json',
          root: 'data'
       	}
    	},
    	listeners: [
    	{
    		beforeload: function()
    		{
    			this.removeAll();
    		}
    	}]
    });
  },
  
  
  buildColumns: function()
  {
  	return [
  	{
  		text: 'Name',
      sortable : true,
      dataIndex: 'name',
      flex: 1
  	},
  	{
  		text: 'Start Date',
      sortable : true,
      dataIndex: 'startDate',
      flex: .7
  	},
  	{
  		text: 'Hours',
      sortable : true,
      dataIndex: 'hours',
      width: 50
  	}]
 	}
});