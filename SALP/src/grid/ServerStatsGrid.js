﻿//
//
//  Server StatsGrid
//
//

Ext.define('SALP.grid.ServerStatsGrid', 
{
	alias: 'widget.grid.ServerStatsGrid',
	extend: 'Ext.grid.Panel',
	
	stripeRows: true,
  columnLines: true, 

	emptyText: 'No Servers Found.', 
	
	initComponent: function()
  {
		this.store = this.buildStore();
		
		this.columns = this.buildColumns();
		
		this.listeners = this.buildListeners();

		this.task = new Ext.util.DelayedTask(function()
		{
			if (this.store)
			{
				this.store.reload();
			}
  	
  		Ext.defer(function() 
      {
      	this.setLoading(false);
      }, 2000, this);
  	
  		this.task.delay(60000);
  		
		}, this);
		
		
		this.task.delay(60000);
		
    this.callParent(arguments);
  },
  
    
  buildStore: function()
  {
  	return Ext.create('Ext.data.Store', 
    {
    	autoLoad: true,
    	model: SALP.data.Servers.model,
    	proxy: 
    	{
      	type: 'ajax',
       	url: 'WebServices/Service.svc/GetServersList',
//       	extraParams:
//       	{
//    	  	hasMonitoredProcesses: 'True'
//       	},
       	pageParam: undefined,
    		startParam: undefined,
    		limitParam: undefined,
       	reader: 
       	{
        	type: 'json',
          root: 'data'
       	}
    	}
    });
  },
  
  buildColumns: function()
  {
  	return [
  	{
  		text: 'Server Name',
      sortable : true,
      dataIndex: 'serverName',
      flex: 1
  	},
  	{
  		text: 'CPU',
      sortable : true,
      dataIndex: 'cpuUsage',
      width: 55,
      renderer: function(value, metaData, record, rowIndex, colIndex, store, view)
      {
				metaData.style = "background-color:green;";

				if(value > 80)
      	{
      		metaData.style = "background-color:yellow;";
      	}

      	if(value > 90)
      	{
      		metaData.style = "background-color:red;";
      	}
      	
      	if (value)
      	{
      		value =  value + '%'
      	}
      	else
      	{
      			value =  '0%'
      	}
      	
      	return value;
      }
  	},
  	{
  		text: 'Memory',
      sortable : true,
      dataIndex: 'memoryUsage',
      width: 65,
      renderer: function(value, metaData, record, rowIndex, colIndex, store, view)
      {
      	metaData.style = "background-color:green;";

				if(value > 85)
      	{
      		metaData.style = "background-color:yellow;";
      	}

      	if(value > 90)
      	{
      		metaData.style = "background-color:red;";
      	}
      	
      	
      	if (value)
      	{
      		value =  value + '%'
      	}
      	else
      	{
      			value =  '0%'
      	}
      	
      	return value;
      }
  	}]
  },
  
  buildListeners: function()
  {
  	return {
     	'celldblclick': function(view, td, columnIndex, record, tr, rowIndex, event) 
     	{
     		if(columnIndex == 0)
     		{
     			Ext.create('SALP.view.ServerCurrentProcessesWindow',
					{
						server: record.data.serverName
					}).show()
     		}
     		
				if(columnIndex == 1)
				{
					Ext.create('SALP.view.ServerCPUUsageWindow',
					{
						server: record.data.serverName
					}).show()
				}
				
				if(columnIndex == 2)
				{
					
				}
     	},
      scope: this
    }
  }
});