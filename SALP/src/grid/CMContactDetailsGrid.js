﻿//
//
//  CM Contact Detials Grid
//
//

Ext.define('SALP.grid.CMContactDetailsGrid', 
{
	alias: 'widget.grid.CMContactDetailsGrid',
	extend: 'Ext.grid.Panel',
	
	stripeRows: true,
  columnLines: true, 
	//hideHeaders: true,
  
	
	initComponent: function()
  {
		this.store = this.buildStore();
		
		this.columns = this.buildColumns();
		
    this.callParent(arguments);
  },
  
  
  
  buildStore: function()
  {
  	return Ext.create('Ext.data.Store', 
    {
    	autoLoad: true,
    	model: 'CMContactDetails',
    	proxy: 
    	{
      	type: 'ajax',
       	url: 'WebServices/Service.svc/GetCMContactDetails',
       	extraParams:
       	{
					ticketNumber: this.ticketNumber
       	},
       	pageParam: undefined,
    		startParam: undefined,
    		limitParam: undefined,
       	reader: 
       	{
        	type: 'json',
          root: 'data'
       	}
    	}
//    	listeners: 
//    	{
//				'load': function(store, records, successfull, operation, options)
//				{
//					this.setHeight(30 + records.length * 20);
//				},
//				scope: this
//			}
    });
  },
  
  
  buildColumns: function()
  {
  	return [
  	{
  		text: 'Create Date',
      sortable : true,
      dataIndex: 'createDate',
      flex: 1,
      renderer: function(value, metaData, record, rowIdx, colIdx, store) 
      {
        metaData.tdAttr = 'data-qtip="' + value + '"';
        return value;
    	}
  	},
  	{
  		text: 'Contact Name',
      sortable : true,
      dataIndex: 'contactName',
      flex: 1,
      renderer: function(value, metaData, record, rowIdx, colIdx, store) 
      {
        metaData.tdAttr = 'data-qtip="' + value + '"';
        return value;
    	}
  	},
  	{
  		text: 'Email Address',
      sortable : true,
      dataIndex: 'emailAddress',
      flex: 1,
      renderer: function(value, metaData, record, rowIdx, colIdx, store) 
      {
        metaData.tdAttr = 'data-qtip="' + value + '"';
        return value;
    	}
  	},
  	{
  		text: 'Primary Phone',
      sortable : true,
      dataIndex: 'primaryPhone',
      flex: 1
  	},
  	{
  		text: 'Contact Role',
      sortable : true,
      dataIndex: 'contactRole',
      flex: 1,
      renderer: function(value, metaData, record, rowIdx, colIdx, store) 
      {
        metaData.tdAttr = 'data-qtip="' + value + '"';
        return value;
    	}
  	}]
  }
});