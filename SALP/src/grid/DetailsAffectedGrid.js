﻿//
//
//  Incident Tickets Grid
//
//

Ext.define('SALP.grid.DetailsAffectedGrid', 
{
	alias: 'widget.grid.DetailsAffectedGrid',
	extend: 'Ext.grid.Panel',
	
	stripeRows: true,
  columnLines: true, 

  
	
	initComponent: function()
  {
		this.store = this.buildStore();
		
		this.columns = this.buildColumns();
		
    this.callParent(arguments);
  },
  
  
  
  buildStore: function()
  {
  	return Ext.create('Ext.data.Store', 
    {
    	autoLoad: true,
    	model: SALP.data.DetailsAffected.model,
    	proxy: 
    	{
      	type: 'ajax',
       	url: 'WebServices/Service.svc/GetTicketDetailsAffected',
       	extraParams:
       	{
					ticketNumber: this.ticketNumber
       	},
       	pageParam: undefined,
    		startParam: undefined,
    		limitParam: undefined,
       	reader: 
       	{
        	type: 'json',
          root: 'data'
       	}
    	},
    	listeners: 
    	{
				'load': function(store, records, successfull, operation, options)
				{
					this.setHeight(30 + records.length * 20);
				},
				scope: this
			}
    });
  },
  
  
  buildColumns: function()
  {
  	return [
  	{
  		text: 'Entry Id',
      sortable : true,
      dataIndex: 'entryId',
      width: 84
  	},
  	{
  		text: 'Affected',
      sortable : true,
      dataIndex: 'serviceAffected',
      width: 53
  	},
  	{
  		text: 'Element',
      sortable : true,
      dataIndex: 'elementName',
      width: 58
  	},
  	{
  		text: 'Video',
      sortable : true,
      dataIndex: 'videoAffectedSubscriptions',
      width: 46
  	},
  	{
  		text: 'HSD',
      sortable : true,
      dataIndex: 'hsdAffectedSubscriptions',
      width: 40
  	},
  	{  	
  		text: 'Telephony',
      sortable : true,
      dataIndex: 'telephonyAffectedSubscriptions',
      width: 66
  	},
  	{
  		text: 'Total',
      sortable : true,
      dataIndex: 'totalAffectedSubscriptions',
      width: 44
  	},
  	{
  		text: 'Status',
      sortable : true,
      dataIndex: 'aeStatus',
      width: 59
  	},
  	{
  		text: 'Actual Start',
      flex: 1,
      sortable : true,
      dataIndex: 'actualStart'
  	},
  	{
  		text: 'Actual End',
      flex: 1,
      sortable : true,
      dataIndex: 'actualEnd'
  	}]
  }
});