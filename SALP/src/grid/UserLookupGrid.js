﻿//
//
//  User Lookup Grid
//
//

Ext.define('SALP.grid.UserLookupGrid', 
{
	alias: 'widget.grid.UserLookupGrid',
	extend: 'Ext.grid.Panel',
	
	stripeRows: true,
  columnLines: true, 
	autoScroll: true,
  scrollable: true,
  
  emptyText: 'No Records Found.', 
	
	selModel: 
	{
    type: 'spreadsheet',
    columnSelect: true  
	},
  
  plugins: [
	{
  	ptype: 'gridexporter'
  },
  {
  	ptype: 'clipboard'
  }],
  
	
	initComponent: function()
  {
		this.store = this.buildStore();
		
		this.columns = this.buildColumns();
		
		this.listeners = this.buildListeners();
		
    this.callParent(arguments);
  },
  
  
  
  buildStore: function()
  {
  	return Ext.create('Ext.data.Store', 
    {
    	autoLoad: true,
    	model: 'SAPUser',
    	proxy: 
    	{
      	type: 'ajax',
       	url: 'WebServices/Service.svc/GetSAPUser',
       	extraParams:
       	{
					name: '*'
       	},
       	pageParam: undefined,
    		startParam: undefined,
    		limitParam: undefined,
       	reader: 
       	{
        	type: 'json',
          root: 'data'
       	}
    	}
//    	listeners: 
//    	{
//				'load': function(store, records, successfull, operation, options)
//				{
//					this.setHeight(30 + records.length * 20);
//				},
//				scope: this
//			}
    });
  },
  
  
  buildColumns: function()
  {
  	return [
  	{
  		text: 'Display Name',
      dataIndex: 'displayName',
      flex: 1,
      renderer: function(value, metaData, record, rowIdx, colIdx, store) 
      {
        metaData.tdAttr = 'data-qtip="' + value + '"';
        return value;
    	}
  	},
  	{
  		text: 'Name',
      dataIndex: 'name',
      flex: 1,
      renderer: function(value, metaData, record, rowIndex, colIndex, store, view)
      {
      	metaData.tdAttr = 'data-qtip="Double-Click to see Subordinates"';
      	
      	return value;
      }
  	},
  	{
  		text: 'Hierarchy Level 1',
      dataIndex: 'hierarchyLevel1',
      flex: 1
  	},
  	{
  		text: 'Department',
      dataIndex: 'department',
      flex: 1,
      renderer: function(value, metaData, record, rowIdx, colIdx, store) 
      {
        metaData.tdAttr = 'data-qtip="' + value + '"';
        return value;
    	}
  	},
  	{
  		text: 'Org Unit',
      dataIndex: 'orgUnit',
      flex: 1,
      renderer: function(value, metaData, record, rowIdx, colIdx, store) 
      {
        metaData.tdAttr = 'data-qtip="' + value + '"';
        return value;
    	}
  	},
  	{
  		text: 'Job Description',
      dataIndex: 'jobDescription',
      flex: 1,
      renderer: function(value, metaData, record, rowIdx, colIdx, store) 
      {
        metaData.tdAttr = 'data-qtip="' + value + '"';
        return value;
    	}
  	},
  	{
  		text: 'Email Address',
      dataIndex: 'emailAddress',
      flex: 1,
      renderer: function(value, metaData, record, rowIdx, colIdx, store) 
      {
        metaData.tdAttr = 'data-qtip="' + value + '"';
        return value;
    	}
  	},
  	{
  		text: 'Work Phone',
      dataIndex: 'workPhone',
      flex: 1,
      renderer: function(value, metaData, record, rowIdx, colIdx, store) 
      {
        metaData.tdAttr = 'data-qtip="' + value + '"';
        return value;
    	}
  	},
  	{
  		text: 'Login ID',
      dataIndex: 'loginId',
      flex: 1,
      renderer: function(value, metaData, record, rowIdx, colIdx, store) 
      {
        metaData.tdAttr = 'data-qtip="' + value + '"';
        return value;
    	}
  	},
  	{
  		text: 'Supervisor',
      dataIndex: 'supervisor',
      flex: 1,
      renderer: function(value, metaData, record, rowIndex, colIndex, store, view)
      {
      	metaData.tdAttr = 'data-qtip="Double-Click to see Subordinates"';
      	
      	return value;
      }
  	}]
  },
  
  buildListeners: function()
  {
  	return {
  		'celldblclick': function(grid, td, cellIndex, record, tr, rowIndex, event, options)
  		{
  			var name = record.data.name,
  					supervisor = record.data.supervisor;
  			
  			if(cellIndex == 2)
  			{
    			Ext.create('SALP.view.SubordinatesSearchWindow',
    			{
    				searchName: name
    				
    			}).show();
  			}
  			
 				if(cellIndex == 10)
  			{
    			Ext.create('SALP.view.SubordinatesSearchWindow',
    			{
    				searchName: supervisor
    				
    			}).show();
  			}
  			
  			var t=1;
  		}, 
  		scope: this
  	}
  }
});