﻿//
//
//  CM Detail By Hierarchy Grid
//
//

Ext.define('SALP.grid.CMDetailByHierarchyGrid', 
{
	alias: 'widget.grid.CMDetailByHierarchyGrid',
	extend: 'Ext.grid.Panel',
	
	stripeRows: true,
  columnLines: true, 
	//hideHeaders: true,
  
 	selModel: 
	{
    type: 'spreadsheet',
    columnSelect: true  
	},

	plugins: [
	{
  	ptype: 'gridexporter'
  },
  {
  	ptype: 'clipboard'
  }],
	
	initComponent: function()
  {
		this.store = this.buildStore();
		
		this.columns = this.buildColumns();
		
		this.listeners = this.buildListeners();
		
    this.callParent(arguments);
  },
  
  
  
  buildStore: function()
  {
  	return Ext.create('Ext.data.Store', 
    {
    	autoLoad: true,
    	model: 'CMDetailByHierarchy',
    	proxy: 
    	{
      	type: 'ajax',
       	url: 'WebServices/Service.svc/GetCMDetailByHierarchy',
       	timeout: 60000,
       	extraParams:
       	{
					userName: this.userName,
					month: this.month,
					year: this.year, 
					reportLevel: this.reportLevel
       	},
       	pageParam: undefined,
    		startParam: undefined,
    		limitParam: undefined,
       	reader: 
       	{
        	type: 'json',
          root: 'data'
       	}
    	}
//    	listeners: 
//    	{
//				'load': function(store, records, successfull, operation, options)
//				{
//					this.setHeight(30 + records.length * 20);
//				},
//				scope: this
//			}
    });
  },
  
  
  buildColumns: function()
  {
  	return [
  	{
  		text: 'Maintenance Month',
  		tooltip: 'Maintenance Month',
      sortable : true,
      dataIndex: 'maintenanceMonth',
      width: 100,
      renderer: function(value, metaData, record, rowIdx, colIdx, store) 
      {
        metaData.tdAttr = 'data-qtip="' + value + '"';
        return value;
    	}
  	},
  	{
  		text: 'Maintenance Year',
  		tooltip: 'Maintenance Year',
      sortable : true,
      dataIndex: 'maintenanceYear',
      width: 100,
      renderer: function(value, metaData, record, rowIdx, colIdx, store) 
      {
        metaData.tdAttr = 'data-qtip="' + value + '"';
        return value;
    	}
  	},
  	{
  		text: 'Heiarchy User',
  		tooltip: 'Heiarchy User',
      sortable : true,
      dataIndex: 'heiarchyUser',
      width: 100,
      renderer: function(value, metaData, record, rowIdx, colIdx, store) 
      {
        metaData.tdAttr = 'data-qtip="' + value + '"';
        return value;
    	}
  	},
  	{
  		text: 'Direct Report',
  		tooltip: 'Direct Report',
      sortable : true,
      dataIndex: 'directReport',
      width: 100,
      renderer: function(value, metaData, record, rowIdx, colIdx, store) 
      {
        metaData.tdAttr = 'data-qtip="' + value + '"';
        return value;
    	}
  	},
  	{
  		text: 'Maintenance Implementor',
  		tooltip: 'Maintenance Implementor',
      sortable : true,
      dataIndex: 'maintenanceImplementor',
      width: 100,
      renderer: function(value, metaData, record, rowIdx, colIdx, store) 
      {
        metaData.tdAttr = 'data-qtip="' + value + '"';
        return value;
    	}
  	},
  	{
  		text: 'SM',
  		tooltip: 'SM',
      sortable : true,
      dataIndex: 'sm',
      width: 100,
      renderer: function(value, metaData, record, rowIndex, colIndex, store, view)
      {
      	metaData.tdAttr = 'data-qtip="Double-Click to see Details"';
      	
      	return value;
      }
  	},
  	{
  		text: 'SM Status',
  		tooltip: 'SM Status',
      sortable : true,
      dataIndex: 'smStatus',
      width: 100,
      renderer: function(value, metaData, record, rowIdx, colIdx, store) 
      {
        metaData.tdAttr = 'data-qtip="' + value + '"';
        return value;
    	}
  	},
  	{
  		text: 'SM Maintenance Type',
  		tooltip: 'SM Maintenance Type',
      sortable : true,
      dataIndex: 'smMaintenanceType',
      width: 100,
      renderer: function(value, metaData, record, rowIdx, colIdx, store) 
      {
        metaData.tdAttr = 'data-qtip="' + value + '"';
        return value;
    	}
  	},
  	{
  		text: 'SM Author',
  		tooltip: 'SM Author',
      sortable : true,
      dataIndex: 'smAuthor',
      width: 100,
      renderer: function(value, metaData, record, rowIdx, colIdx, store) 
      {
        metaData.tdAttr = 'data-qtip="' + value + '"';
        return value;
    	}
  	},
  	{
  		text: 'SM Parent',
  		tooltip: 'SM Parent',
      sortable : true,
      dataIndex: 'smParent',
      width: 100,
      renderer: function(value, metaData, record, rowIndex, colIndex, store, view)
      {
      	metaData.tdAttr = 'data-qtip="Double-Click to see Details"';
      	
      	return value;
      }
  	},
  	{
  		text: 'Closed By',
  		tooltip: 'Closed By',
      sortable : true,
      dataIndex: 'closedBy',
      width: 100,
      renderer: function(value, metaData, record, rowIdx, colIdx, store) 
      {
        metaData.tdAttr = 'data-qtip="' + value + '"';
        return value;
    	}
  	},
  	{
  		text: 'Ticket Implementer',
  		tooltip: 'Ticket Implementer',
      sortable : true,
      dataIndex: 'ticketImplementer',
      width: 100,
      renderer: function(value, metaData, record, rowIdx, colIdx, store) 
      {
        metaData.tdAttr = 'data-qtip="' + value + '"';
        return value;
    	}
  	},
  	{
  		text: 'Ticket Id',
  		tooltip: 'Ticket Id',
      sortable : true,
      dataIndex: 'ticketId',
      width: 100,
      renderer: function(value, metaData, record, rowIdx, colIdx, store) 
      {
        metaData.tdAttr = 'data-qtip="' + value + '"';
        return value;
    	}
  	},
  	{
  		text: 'Ticket Start',
  		tooltip: 'Ticket Start',
      sortable : true,
      dataIndex: 'ticketStart',
      width: 100,
      renderer: function(value, metaData, record, rowIdx, colIdx, store) 
      {
        metaData.tdAttr = 'data-qtip="' + value + '"';
        return value;
    	}
  	},
  	{
  		text: 'Ticket End',
  		tooltip: 'Ticket End',
      sortable : true,
      dataIndex: 'ticketEnd',
      width: 100,
      renderer: function(value, metaData, record, rowIdx, colIdx, store) 
      {
        metaData.tdAttr = 'data-qtip="' + value + '"';
        return value;
    	}
  	},
  	{
  		text: 'SM Create Date',
  		tooltip: 'SM Create Date',
      sortable : true,
      dataIndex: 'smCreateDate',
      width: 100,
      renderer: function(value, metaData, record, rowIdx, colIdx, store) 
      {
        metaData.tdAttr = 'data-qtip="' + value + '"';
        return value;
    	}
  	},
  	{
  		text: 'SM Timeframe Start',
  		tooltip: 'SM Timeframe Start',
      sortable : true,
      dataIndex: 'smTimeframeStart',
      width: 100,
      renderer: function(value, metaData, record, rowIdx, colIdx, store) 
      {
        metaData.tdAttr = 'data-qtip="' + value + '"';
        return value;
    	}
  	},
  	{
  		text: 'SM Timeframe End',
  		tooltip: 'SM Timeframe End',
      sortable : true,
      dataIndex: 'smTimeframeEnd',
      width: 100,
      renderer: function(value, metaData, record, rowIdx, colIdx, store) 
      {
        metaData.tdAttr = 'data-qtip="' + value + '"';
        return value;
    	}
  	},
  	{
  		text: 'SM Timeframe Start',
  		tooltip: 'SM Closed Date',
      sortable : true,
      dataIndex: 'smClosedDate',
      width: 100,
      renderer: function(value, metaData, record, rowIdx, colIdx, store) 
      {
        metaData.tdAttr = 'data-qtip="' + value + '"';
        return value;
    	}
  	},
  	{
  		text: 'SM Maintenance Category',
  		tooltip: 'SM Maintenance Category',
      sortable : true,
      dataIndex: 'smMaintenanceCategory',
      width: 100,
      renderer: function(value, metaData, record, rowIdx, colIdx, store) 
      {
        metaData.tdAttr = 'data-qtip="' + value + '"';
        return value;
    	}
  	},
  	{
  		text: 'SM Maintenance Detail',
  		tooltip: 'SM Maintenance Detail',
      sortable : true,
      dataIndex: 'smMaintenanceDetail',
      width: 100,
      renderer: function(value, metaData, record, rowIdx, colIdx, store) 
      {
        metaData.tdAttr = 'data-qtip="' + value + '"';
        return value;
    	}
  	},
  	{
  		text: 'SM Region',
  		tooltip: 'SM Region',
      sortable : true,
      dataIndex: 'smRegion',
      width: 100,
      renderer: function(value, metaData, record, rowIdx, colIdx, store) 
      {
        metaData.tdAttr = 'data-qtip="' + value + '"';
        return value;
    	}
  	},
  	{
  		text: 'SM Resolution Description',
  		tooltip: 'SM Resolution Description',
      sortable : true,
      dataIndex: 'smResolutionDescription',
      width: 100,
      renderer: function(value, metaData, record, rowIdx, colIdx, store) 
      {
        metaData.tdAttr = 'data-qtip="' + value + '"';
        return value;
    	}
  	},
  	{
  		text: 'Resolution Description',
  		tooltip: 'Resolution Description',
      sortable : true,
      dataIndex: 'resolutionDescription',
      width: 100,
      renderer: function(value, metaData, record, rowIdx, colIdx, store) 
      {
        metaData.tdAttr = 'data-qtip="' + value + '"';
        return value;
    	}
  	},
  	{
  		text: 'Result Category',
  		tooltip: 'Result Category',
      sortable : true,
      dataIndex: 'resultCategory',
      width: 100,
      renderer: function(value, metaData, record, rowIdx, colIdx, store) 
      {
        metaData.tdAttr = 'data-qtip="' + value + '"';
        return value;
    	}
  	},
  	{
  		text: 'Resolution Description',
  		tooltip: 'SM Resolution Detail',
      sortable : true,
      dataIndex: 'smResolutionDetail',
      width: 100,
      renderer: function(value, metaData, record, rowIdx, colIdx, store) 
      {
        metaData.tdAttr = 'data-qtip="' + value + '"';
        return value;
    	}
  	},
  	{
  		text: 'CM',
  		tooltip: 'CM',
      sortable : true,
      dataIndex: 'cm',
      width: 100,
      renderer: function(value, metaData, record, rowIndex, colIndex, store, view)
      {
      	metaData.tdAttr = 'data-qtip="Double-Click to see Details"';
      	
      	return value;
      }
  	},
  	{
  		text: 'CM Maintenance Type',
  		tooltip: 'CM Maintenance Type',
      sortable : true,
      dataIndex: 'cmMaintenanceType',
      width: 100,
      renderer: function(value, metaData, record, rowIdx, colIdx, store) 
      {
        metaData.tdAttr = 'data-qtip="' + value + '"';
        return value;
    	}
  	},
  	{
  		text: 'CM Category',
  		tooltip: 'CM Category',
      sortable : true,
      dataIndex: 'cmCategory',
      width: 100,
      renderer: function(value, metaData, record, rowIdx, colIdx, store) 
      {
        metaData.tdAttr = 'data-qtip="' + value + '"';
        return value;
    	}
  	},
  	{
  		text: 'CM Subcategory',
  		tooltip: 'CM Subcategory',
      sortable : true,
      dataIndex: 'cmSubcategory',
      width: 100,
      renderer: function(value, metaData, record, rowIdx, colIdx, store) 
      {
        metaData.tdAttr = 'data-qtip="' + value + '"';
        return value;
    	}
  	},
  	{
  		text: 'CM Procedure Name',
  		tooltip: 'CM Procedure Name',
      sortable : true,
      dataIndex: 'cmProcedureName',
      width: 100,
      renderer: function(value, metaData, record, rowIdx, colIdx, store) 
      {
        metaData.tdAttr = 'data-qtip="' + value + '"';
        return value;
    	}
  	},
  	{
  		text: 'Procedure ID',
  		tooltip: 'Procedure ID',
      sortable : true,
      dataIndex: 'procedureId',
      width: 100,
      renderer: function(value, metaData, record, rowIdx, colIdx, store) 
      {
        metaData.tdAttr = 'data-qtip="' + value + '"';
        return value;
    	}
  	},
  	{
  		text: 'CM Summary',
  		tooltip: 'CM Summary',
      sortable : true,
      dataIndex: 'cmSummary',
      width: 100,
      renderer: function(value, metaData, record, rowIdx, colIdx, store) 
      {
        metaData.tdAttr = 'data-qtip="' + value + '"';
        return value;
    	}
  	},
  	{
  		text: 'CM Detail',
  		tooltip: 'CM Detail',
      sortable : true,
      dataIndex: 'cmDetail',
      width: 100,
      renderer: function(value, metaData, record, rowIdx, colIdx, store) 
      {
        metaData.tdAttr = 'data-qtip="' + value + '"';
        return value;
    	}
  	},
  	{
  		text: 'CM Risk',
  		tooltip: 'CM Risk',
      sortable : true,
      dataIndex: 'cmRisk',
      width: 100,
      renderer: function(value, metaData, record, rowIdx, colIdx, store) 
      {
        metaData.tdAttr = 'data-qtip="' + value + '"';
        return value;
    	}
  	},
  	{
  		text: 'CM Impact',
  		tooltip: 'CM Impact',
      sortable : true,
      dataIndex: 'cmImpact',
      width: 100,
      renderer: function(value, metaData, record, rowIdx, colIdx, store) 
      {
        metaData.tdAttr = 'data-qtip="' + value + '"';
        return value;
    	}
  	},
  	{
  		text: 'CM Create Date',
  		tooltip: 'CM Create Date',
      sortable : true,
      dataIndex: 'cmCreateDate',
      width: 100,
      renderer: function(value, metaData, record, rowIdx, colIdx, store) 
      {
        metaData.tdAttr = 'data-qtip="' + value + '"';
        return value;
    	}
  	},
  	{
  		text: 'CM Planned Start',
  		tooltip: 'CM Planned Start',
      sortable : true,
      dataIndex: 'cmPlannedStart',
      width: 100,
      renderer: function(value, metaData, record, rowIdx, colIdx, store) 
      {
        metaData.tdAttr = 'data-qtip="' + value + '"';
        return value;
    	}
  	},
  	{
  		text: 'CM Planned End',
  		tooltip: 'CM Planned End',
      sortable : true,
      dataIndex: 'cmPlannedEnd',
      width: 100,
      renderer: function(value, metaData, record, rowIdx, colIdx, store) 
      {
        metaData.tdAttr = 'data-qtip="' + value + '"';
        return value;
    	}
    },
    {
  		text: 'CM Status',
  		tooltip: 'CM Status',
      sortable : true,
      dataIndex: 'cmStatus',
      width: 100,
      renderer: function(value, metaData, record, rowIdx, colIdx, store) 
      {
        metaData.tdAttr = 'data-qtip="' + value + '"';
        return value;
    	}
  	},	
  	{
  		text: 'CM Submitter',
  		tooltip: 'CM Submitter',
      sortable : true,
      dataIndex: 'cmSubmitter',
      width: 100,
      renderer: function(value, metaData, record, rowIdx, colIdx, store) 
      {
        metaData.tdAttr = 'data-qtip="' + value + '"';
        return value;
    	}
    },
    {
  		text: 'CM Submitter Name',
  		tooltip: 'CM Submitter Name',
      sortable : true,
      dataIndex: 'cmSubmitterName',
      width: 100,
      renderer: function(value, metaData, record, rowIdx, colIdx, store) 
      {
        metaData.tdAttr = 'data-qtip="' + value + '"';
        return value;
    	}
  	},
  	{
  		text: 'CM Region',
  		tooltip: 'CM Region',
      sortable : true,
      dataIndex: 'cmRegion',
      width: 100,
      renderer: function(value, metaData, record, rowIdx, colIdx, store) 
      {
        metaData.tdAttr = 'data-qtip="' + value + '"';
        return value;
    	}
    },
    {
  		text: 'Organizational Unit Description',
  		tooltip: 'Organizational Unit Description',
      sortable : true,
      dataIndex: 'orgUnitDescription',
      width: 100,
      renderer: function(value, metaData, record, rowIdx, colIdx, store) 
      {
        metaData.tdAttr = 'data-qtip="' + value + '"';
        return value;
    	}
  	},
  	{
  		text: 'Organizational Heirarchy Level 1 Description',
  		tooltip: 'Organizational Heirarchy Level 1 Description',
      sortable : true,
      dataIndex: 'orgHeirarchyLevel1Description',
      width: 100,
      renderer: function(value, metaData, record, rowIdx, colIdx, store) 
      {
        metaData.tdAttr = 'data-qtip="' + value + '"';
        return value;
    	}
    },
    {
  		text: 'Organizational Heirarchy Level 2 Description',
  		tooltip: 'Organizational Heirarchy Level 2 Description',
      sortable : true,
      dataIndex: 'orgHeirarchyLevel2Description',
      width: 100,
      renderer: function(value, metaData, record, rowIdx, colIdx, store) 
      {
        metaData.tdAttr = 'data-qtip="' + value + '"';
        return value;
    	}
  	},
  	{
  		text: 'SAP Lead',
  		tooltip: 'SAP Lead',
      sortable : true,
      dataIndex: 'sapLead',
      width: 100,
      renderer: function(value, metaData, record, rowIdx, colIdx, store) 
      {
        metaData.tdAttr = 'data-qtip="' + value + '"';
        return value;
    	}
    },
   	{
  		text: 'SAP Lead',
  		tooltip: 'SAP Lead',
      sortable : true,
      dataIndex: 'sapLead',
      width: 100,
      renderer: function(value, metaData, record, rowIdx, colIdx, store) 
      {
        metaData.tdAttr = 'data-qtip="' + value + '"';
        return value;
    	}
    },
    {
  		text: 'SAP VP',
  		tooltip: 'SAP VP',
      sortable : true,
      dataIndex: 'sapVp',
      width: 100,
      renderer: function(value, metaData, record, rowIdx, colIdx, store) 
      {
        metaData.tdAttr = 'data-qtip="' + value + '"';
        return value;
    	}
  	},
  	{
  		text: 'SAP VP Top',
  		tooltip: 'SAP VP Top',
      sortable : true,
      dataIndex: 'sapVpTop',
      width: 100,
      renderer: function(value, metaData, record, rowIdx, colIdx, store) 
      {
        metaData.tdAttr = 'data-qtip="' + value + '"';
        return value;
    	}
    },
    {
  		text: 'SI Ticket',
  		tooltip: 'SI Ticket',
      sortable : true,
      dataIndex: 'siTicket',
      width: 100,
      renderer: function(value, metaData, record, rowIndex, colIndex, store, view)
      {
      	metaData.tdAttr = 'data-qtip="Double-Click to see Details"';
      	
      	return value;
      }
  	},
  	{
  		text: 'Related Ticket',
  		tooltip: 'Related Ticket',
      sortable : true,
      dataIndex: 'relatedTicket',
      width: 100,
      renderer: function(value, metaData, record, rowIndex, colIndex, store, view)
      {
      	metaData.tdAttr = 'data-qtip="Double-Click to see Details"';
      	
      	return value;
      }
    },
    {
  		text: 'SI Create Date',
  		tooltip: 'SI Create Date',
      sortable : true,
      dataIndex: 'siCreateDate',
      width: 100,
      renderer: function(value, metaData, record, rowIdx, colIdx, store) 
      {
        metaData.tdAttr = 'data-qtip="' + value + '"';
        return value;
    	}
  	},
  	{
  		text: 'SI Actual Start',
  		tooltip: 'SI Actual Start',
      sortable : true,
      dataIndex: 'siActualStart',
      width: 100,
      renderer: function(value, metaData, record, rowIdx, colIdx, store) 
      {
        metaData.tdAttr = 'data-qtip="' + value + '"';
        return value;
    	}
    },
    {
  		text: 'SI Actual End',
  		tooltip: 'SI Actual End',
      sortable : true,
      dataIndex: 'siActualEnd',
      width: 100,
      renderer: function(value, metaData, record, rowIdx, colIdx, store) 
      {
        metaData.tdAttr = 'data-qtip="' + value + '"';
        return value;
    	}
  	},
  	{
  		text: 'Ticket Implementer New',
  		tooltip: 'Ticket Implementer New',
      sortable : true,
      dataIndex: 'ticketImplementerNew',
      width: 100,
      renderer: function(value, metaData, record, rowIdx, colIdx, store) 
      {
        metaData.tdAttr = 'data-qtip="' + value + '"';
        return value;
    	}
    },
    {
  		text: 'Cancelled Before Maintenance',
  		tooltip: 'Cancelled Before Maintenance',
      sortable : true,
      dataIndex: 'cancelledBeforeMaintenance',
      width: 100,
      renderer: function(value, metaData, record, rowIndex, colIndex, store, view)
      {
      	metaData.tdAttr = 'data-qtip="Double-Click to see Details"';
      	
      	return value;
      }
  	},
  	{
  		text: 'Cancelled Before Maintenance SM',
  		tooltip: 'Cancelled Before Maintenance SM',
      sortable : true,
      dataIndex: 'cancelledBeforeMaintenanceSm',
      width: 100,
      renderer: function(value, metaData, record, rowIdx, colIdx, store) 
      {
        metaData.tdAttr = 'data-qtip="' + value + '"';
        return value;
    	}
    },
    {
  		text: 'Sucess Category',
  		tooltip: 'Sucess Category',
      sortable : true,
      dataIndex: 'sucessCategory',
      width: 100,
      renderer: function(value, metaData, record, rowIdx, colIdx, store) 
      {
        metaData.tdAttr = 'data-qtip="' + value + '"';
        return value;
    	}
  	},
  	{
  		text: 'Support Area Name',
  		tooltip: 'Support Area Name',
      sortable : true,
      dataIndex: 'supportAreaName',
      width: 100,
      renderer: function(value, metaData, record, rowIdx, colIdx, store) 
      {
        metaData.tdAttr = 'data-qtip="' + value + '"';
        return value;
    	}
    },
    {
  		text: 'Queue Name',
  		tooltip: 'Queue Name',
      sortable : true,
      dataIndex: 'queueName',
      width: 100,
      renderer: function(value, metaData, record, rowIdx, colIdx, store) 
      {
        metaData.tdAttr = 'data-qtip="' + value + '"';
        return value;
    	}
  	},
  	{
  		text: 'SI Severity',
  		tooltip: 'SI Severity',
      sortable : true,
      dataIndex: 'siSeverity',
      width: 100,
      renderer: function(value, metaData, record, rowIdx, colIdx, store) 
      {
        metaData.tdAttr = 'data-qtip="' + value + '"';
        return value;
    	}
    },
  	{
  		text: 'Lead Organizational Unit Description',
  		tooltip: 'Lead Organizational Unit Description',
      sortable : true,
      dataIndex: 'leadOrgUnitDescription',
      width: 100,
      renderer: function(value, metaData, record, rowIdx, colIdx, store) 
      {
        metaData.tdAttr = 'data-qtip="' + value + '"';
        return value;
    	}
  	},
  	{
  		text: 'SI Severity',
  		tooltip: 'Dir Organizational Unit Description',
      sortable : true,
      dataIndex: 'dirOrgUnitDescription',
      width: 100,
      renderer: function(value, metaData, record, rowIdx, colIdx, store) 
      {
        metaData.tdAttr = 'data-qtip="' + value + '"';
        return value;
    	}
    },
    {
  		text: 'VP Organizational Unit Description',
  		tooltip: 'VP Organizational Unit Description',
      sortable : true,
      dataIndex: 'vpOrgUnitDescription',
      width: 100,
      renderer: function(value, metaData, record, rowIdx, colIdx, store) 
      {
        metaData.tdAttr = 'data-qtip="' + value + '"';
        return value;
    	}
  	},
  	{
  		text: 'Lead Organizational Hierarchy Level 1 Description',
  		tooltip: 'Lead Organizational Hierarchy Level 1 Description',
      sortable : true,
      dataIndex: 'leadOrgHierarchyLevel1Description',
      width: 100,
      renderer: function(value, metaData, record, rowIdx, colIdx, store) 
      {
        metaData.tdAttr = 'data-qtip="' + value + '"';
        return value;
    	}
    },
    {
  		text: 'Dir Organizational Hierarchy Level 1 Description',
  		tooltip: 'Dir Organizational Hierarchy Level 1 Description',
      sortable : true,
      dataIndex: 'dirOrgHierarchyLevel1Description',
      width: 100,
      renderer: function(value, metaData, record, rowIdx, colIdx, store) 
      {
        metaData.tdAttr = 'data-qtip="' + value + '"';
        return value;
    	}
  	},
  	{
  		text: 'VP Organizational Hierarchy Level 1 Description',
  		tooltip: 'VP Organizational Hierarchy Level 1 Description',
      sortable : true,
      dataIndex: 'vpOrgHierarchyLevel1Description',
      width: 100,
      renderer: function(value, metaData, record, rowIdx, colIdx, store) 
      {
        metaData.tdAttr = 'data-qtip="' + value + '"';
        return value;
    	}
    },
    {
  		text: 'VP Organizational Hierarchy Level 2 Description',
  		tooltip: 'VP Organizational Hierarchy Level 2 Description',
      sortable : true,
      dataIndex: 'vpOrgHierarchyLevel2Description',
      width: 100,
      renderer: function(value, metaData, record, rowIdx, colIdx, store) 
      {
        metaData.tdAttr = 'data-qtip="' + value + '"';
        return value;
    	}
  	},
  	{
  		text: 'TS',
  		tooltip: 'TS',
      sortable : true,
      dataIndex: 'ts',
      width: 100,
      renderer: function(value, metaData, record, rowIdx, colIdx, store) 
      {
        metaData.tdAttr = 'data-qtip="' + value + '"';
        return value;
    	}
    }]
  },
  
  buildListeners: function()
  {
  	return {
     	'celldblclick': function(view, td, columnIndex, record, tr, rowIndex, event) 
     	{
  			if(columnIndex == 6)
  			{
  				Ext.create('SALP.view.MultiTicketWindow', 
  				{
  					ticketNumber: record.data.sm
  				}).show();
  			} 		
  			
  			if(columnIndex == 10)
  			{
  				Ext.create('SALP.view.MultiTicketWindow', 
  				{
  					ticketNumber: record.data.smParent
  				}).show();
  			}
  			
  			if(columnIndex == 13)
  			{
  				Ext.create('SALP.view.MultiTicketWindow', 
  				{
  					ticketNumber: record.data.ticketId
  				}).show();
  			}
  			
  			if(columnIndex == 27)
  			{
  				Ext.create('SALP.view.MultiTicketWindow', 
  				{
  					ticketNumber: record.data.cm
  				}).show();
  			}
  			
  			if(columnIndex == 51)
  			{
  				Ext.create('SALP.view.MultiTicketWindow', 
  				{
  					ticketNumber: record.data.siTicket
  				}).show();
  			}
  			
  			if(columnIndex == 52)
  			{
  				Ext.create('SALP.view.MultiTicketWindow', 
  				{
  					ticketNumber: record.data.relatedTicket
  				}).show();
  			}
  			
  			if(columnIndex == 58)
  			{
  				Ext.create('SALP.view.MultiTicketWindow', 
  				{
  					ticketNumber: record.data.cancelledBeforeMaintenanceSm
  				}).show();
  			}
     	}
    }
  }
});