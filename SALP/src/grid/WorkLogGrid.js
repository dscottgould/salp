﻿//
//
//  Work Log Grid
//
//

Ext.define('SALP.grid.WorkLogGrid', 
{
	alias: 'widget.grid.WorkLogGrid',
	extend: 'Ext.grid.Panel',
	
	stripeRows: true,
  columnLines: true, 

  
	
	initComponent: function()
  {
		this.store = this.buildStore();
		
		this.header = this.buildHeader();
		
		this.columns = this.buildColumns();
		
		this.listeners = this.buildListeners();
		
    this.callParent(arguments);
  },
  
  
  
  buildStore: function()
  {
  	return Ext.create('Ext.data.Store', 
    {
    	autoLoad: true,
    	model: SALP.data.WorkLog.model,
    	proxy: 
    	{
      	type: 'ajax',
       	url: 'WebServices/Service.svc/GetWorkLogTickets',
       	extraParams:
       	{
					ticketNumber: this.ticketNumber,
					source: 'none'
       	},
       	pageParam: undefined,
    		startParam: undefined,
    		limitParam: undefined,
       	reader: 
       	{
        	type: 'json',
          root: 'data'
       	}
    	}
    });
  },
  
  buildHeader: function()
  {
  	return   {
    	itemPosition: 1, // after title before collapse tool
    	items: [
    	{
				xtype: 'combobox',
				store: SALP.data.WorkLog.select,
				queryMode: 'local',
				displayField: 'displayName',
				valueField: 'submitValue',
				value: 'none',
    		listeners : 
    		{
    			select : function(comboBox, dataModel, object) 
    			{
  					this.store.load
        		({
        			params:
        			{
        				ticketNumber: this.ticketNumber,
								source: dataModel.data.submitValue
        			}
        		});
      		},
      		scope: this
      	}
    	}]
  	}
  },
  
  buildColumns: function()
  {
  	return [
  	{
      dataIndex: 'ticketNumber',
      hidden: true
  	},
  	{
  		text: 'Create Date',
      sortable : true,
      dataIndex: 'createDate',
      flex: 1
  	},
  	{
  		text: 'Submitter',
      sortable : true,
      dataIndex: 'submitter',
      flex: 1
  	},
  	{
  		text: 'Source',
      sortable : true,
      dataIndex: 'source',
      flex: 1
  	},
  	{
  		text: 'Subject',
      sortable : true,
      dataIndex: 'subject',
      flex: 1
  	},
  	{
  		text: 'Details',
      sortable : true,
      dataIndex: 'details',
      flex: 1,
      renderer: function(value, metaData, record, rowIndex, colIndex, store, view)
      {
      	
      	metaData.tdAttr = 'data-qtip="Double Click for Detail Window"';
      	
      	return value;
      }
  	}]
 	},
 	  
  buildListeners: function()
  {
  	return {
//  		rowdblclick: function(grid, record, object, rowIndex)
//      {
//	    	SALP.getIncidentTicket(record.data.ttsId)
//     	},
     	'celldblclick': function(view, td, cellIndex, record, tr, rowIndex, event) 
     	{
     		var ticketId = record.data.ttsId;
     		
				if (cellIndex == 5)
				{
					SALP.getWorkLogDetails(record)
				}
     	},
      scope: this
    }
  }
});