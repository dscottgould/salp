﻿//
//
//  Incident Tickets Grid
//
//

Ext.define('SALP.grid.IncidentTicketsGrid', 
{
	id: 'IncidentTicketsGrid',
	alias: 'widget.grid.IncidentTicketsGrid',
	extend: 'Ext.grid.Panel',
	
	store: SALP.data.Incidents.store,
	
	stripeRows: true,
  columnLines: true, 
  
  
//  viewConfig : 
//  {
//  	enableTextSelection: true
//  },
//  

	selModel: 
	{
    type: 'spreadsheet',
    columnSelect: true  
	},

	
	plugins: [
	{
  	ptype: 'gridexporter'
  },
  {
  	ptype: 'clipboard'
  }],
	
	initComponent: function()
  {
		this.header = this.buildHeader()
		
		this.store = this.buildStore();
		
		this.columns = this.buildColumns();
		
		this.listeners = this.buildListeners();
		
		this.task = new Ext.util.DelayedTask(function()
		{
  		this.setLoading('Loading...');
  		
  		this.store.reload();
  	
  		Ext.defer(function() 
      {
      	this.setLoading(false);
      }, 2000, this);
  	
  		this.task.delay(900000);
  		
		}, this);
		

		this.task.delay(900000);
		
    this.callParent(arguments);
  },
  
  buildHeader: function()
  {
  	return   {
    	itemPosition: 1, // after title before collapse tool
    	items: [
    	{
        xtype: 'button',
        text: 'Download',
       	listeners: 
				{
    			click: function() 
     			{
						this.saveDocumentAs(
						{
    					type: 'xlsx',
    					title: 'Incident Tickets',
    					fileName: 'IncidentTickets.xlsx'
						});
      		},
      		scope: this
				}
      },
      {
      	xtype: 'container',
      	width: 10
      },
    	{
        xtype: 'button',
        text: 'Refresh',
       	listeners: 
				{
    			click: function() 
     			{
						this.store.reload();
      		},
      		scope: this
				}
      },
      {
      	xtype: 'container',
      	width: 10
      },
    	{
				xtype: 'combobox',
				store: SALP.data.IncidentTickets,
				queryMode: 'local',
				displayField: 'displayName',
				valueField: 'submitValue',
				value: 'this',
    		listeners : 
    		{
    			select : function(comboBox, dataModel, object) 
    			{
  					this.store.load
        		({
        			params:
        			{
								week: dataModel.data.submitValue
        			}
        		});
      		},
      		scope: this
      	}
    	}]
  	}
  },
  
  
  buildStore: function()
  {
  	return Ext.create('Ext.data.Store', 
    {
    	autoLoad: true,
    	model: SALP.data.Incidents.model,
    	proxy: 
    	{
      	type: 'ajax',
       	url: 'WebServices/Service.svc/GetIncidentTickets',
       	extraParams:
       	{
       		//week: 'last'
       		week: 'this'
       	},
       	pageParam: undefined,
    		startParam: undefined,
    		limitParam: undefined,
       	reader: 
       	{
        	type: 'json',
          root: 'data'
       	}
    	}
    });
  },
  
  
  buildColumns: function()
  {
  	return [
  	{
  		text: 'Ticket Id',
      flex: 1,
      sortable : true,
      dataIndex: 'ttsId'
  	},
  	{
  		text: 'Network Lens',
      flex: 1,
      sortable : true,
      dataIndex: 'networkLens'
  	},
  	{
  		text: 'Lens Group',
      flex: 1,
      sortable : true,
      dataIndex: 'lensGroup',
      renderer: function(value, metaData, record, rowIndex, colIndex, store, view)
      {
      	if(value == null || value.length == 0)
      	{
      		metaData.style = "background-color:#FC8F8F;";
      	}
      	
      	return value;
      }
  	},
  	{
  		text: 'Root Cause Group',
      flex: 1,
      sortable : true,
      dataIndex: 'rootCauseGroup',
      renderer: function(value, metaData, record, rowIndex, colIndex, store, view)
      {
      	if(value == null || value.length == 0)
      	{
      		metaData.style = "background-color:#FC8F8F;";
      	}
      	
      	return value;
      }
  	},
  	{
  		text: 'Maintance Mismatch',
      flex: 1,
      sortable : true,
      dataIndex: 'maintMismatch'
  	},
  	{
  		text: 'Change Related',
      flex: 1,
      sortable : true,
      dataIndex: 'ttsChangeRelated',
      renderer: function(value, metaData, record, rowIndex, colIndex, store, view)
      {
      	var maintMismatch = record.data.maintMismatch
      	
      	if(maintMismatch == 'X')
      	{
      		metaData.style = "background-color:#FC8F8F;";
      	}
      	
      	return value;
      }
  	},
  	{
  		text: 'FixEm Change Related',
      flex: 1,
      sortable : true,
      dataIndex: 'fixEmChangeRelated',
      renderer: function(value, metaData, record, rowIndex, colIndex, store, view)
      {
      	var maintMismatch = record.data.maintMismatch
      	
      	if(maintMismatch == 'X')
      	{
      		metaData.style = "background-color:#FC8F8F;";
      	}
      	
      	return value;
      }
  	},
  	{
  		text: 'Actual Start',
      flex: 1,
      sortable : true,
      dataIndex: 'ttActualStart'
  	},
  	{
  		text: 'Defect Time Issue',
      flex: 1,
      sortable : true,
      dataIndex: 'defectTimeIssue'
  	},
  	{
  		text: 'Minutes To Detect',
      flex: 1,
      sortable : true,
      dataIndex: 'detect',
      renderer: function(value, metaData, record, rowIndex, colIndex, store, view)
      {
      	var defectTimeIssue = record.data.defectTimeIssue
      	
      	if(defectTimeIssue == 'X')
      	{
      		metaData.style = "background-color:#FC8F8F;";
      	}
      	
      	return value;
      }
  	},
  	{
  		text: 'Alarm Start',
      flex: 1,
      sortable : true,
      dataIndex: 'ttAlarmStart'
  	},
  	{
  		text: 'Engage Time Issue',
      flex: 1,
      sortable : true,
      dataIndex: 'engageTimeIssue'
  	},
  	{
  		text: 'Minutes To Engage',
      flex: 1,
      sortable : true,
      dataIndex: 'engage',
      renderer: function(value, metaData, record, rowIndex, colIndex, store, view)
      {
      	var engageTimeIssue = record.data.engageTimeIssue
      	
      	if(engageTimeIssue == 'X')
      	{
      		metaData.style = "background-color:#FC8F8F;";
      	}
      	
      	return value;
      }
  	},
  	{
  		text: 'Working',
      flex: 1,
      sortable : true,
      dataIndex: 'ttsWorking'
  	},
  	{
  		text: 'Resolved Time Issue',
      flex: 1,
      sortable : true,
      dataIndex: 'resolvedTimeIssue'
  	},
  	{
  		text: 'Minutes To Resolve',
      flex: 1,
      sortable : true,
      dataIndex: 'resolve',
      renderer: function(value, metaData, record, rowIndex, colIndex, store, view)
      {
      	var resolvedTimeIssue = record.data.resolvedTimeIssue
      	
      	if(resolvedTimeIssue == 'X')
      	{
      		metaData.style = "background-color:#FC8F8F;";
      	}
      	
      	return value;
      }
  	},
  	{
  		text: 'Actual End',
      flex: 1,
      sortable : true,
      dataIndex: 'ttActualEnd'
  	},
  	{
  		text: 'Cause Description',
      flex: 1,
      sortable : true,
      dataIndex: 'ttCauseDescription'
  	},
  	{
  		text: 'Solution Description',
      flex: 1,
      sortable : true,
      dataIndex: 'ttSolutionDescription'
  	},
  	{
  		text: 'Problem Summary',
      flex: 1,
      sortable : true,
      dataIndex: 'ttProblemSummary'
  	},
  	{
  		text: 'Incident Summary',
      flex: 1,
      sortable : true,
      dataIndex: 'incidentSummary',
      renderer: function(value, metaData, record, rowIndex, colIndex, store, view)
      {
      	if(value.length > 100)
      	{
      		metaData.style = "background-color:#FC8F8F;";
      	}
      	
      	metaData.tdAttr = 'data-qtip="' + value + '"';
      	
      	return value;
      }
  	},
  	{
  		text: 'SI Subcount',
      flex: 1,
      sortable : true,
      dataIndex: 'siUserImpactCount'
  	}]
  },
  
  buildListeners: function()
  {
  	return {
//  		rowdblclick: function(grid, record, object, rowIndex)
//      {
//	    	SALP.getIncidentTicket(record.data.ttsId)
//     	},
     	'celldblclick': function(view, td, columnIndex, record, tr, rowIndex, event) 
     	{
     		var ticketId = record.data.ttsId;
     		
     		if(columnIndex >= 5 && columnIndex <= 17)
     		{
     			// Band-Aid Ticket
     			SALP.getTicket(ticketId);
     		}
     		else if(columnIndex == 1)
     		{
     			// Service Incident Ticket Detail Window
     			SALP.getTicketDetails(ticketId);
     		}
     		else
     		{
     			// Incident Ticket
     			SALP.getIncidentTicket(ticketId);
     		}
     	},
      scope: this
    }
  }
});