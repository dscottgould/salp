﻿//
//
//  Maintenance Resolve Status Grid
//
//

Ext.define('SALP.grid.MaintenanceResolveStatusGrid', 
{
	alias: 'widget.grid.MaintenanceResolveStatusGrid',
	extend: 'Ext.grid.Panel',
	
	stripeRows: true,
  columnLines: true, 
  scrollable: true,

	emptyText: 'No Records Found.', 
	
	selModel: 
	{
    type: 'spreadsheet',
    columnSelect: true  
	},

	plugins: 'clipboard',
	
	initComponent: function()
  {
		this.store = this.buildStore();
		
		this.columns = this.buildColumns();
		
		this.listeners = this.buildListeners();
		
    this.callParent(arguments);
  },
  
    
  buildStore: function()
  {
  	return Ext.create('Ext.data.Store', 
    {
    	autoLoad: true,
    	model: SALP.data.MaintenanceResolveStatus.model,
    	proxy: 
    	{
      	type: 'ajax',
       	url: 'WebServices/Service.svc/GetMaintanceResoveStatus',
//       	extraParams:
//       	{
//    	  	hasMonitoredProcesses: 'True'
//       	},
       	pageParam: undefined,
    		startParam: undefined,
    		limitParam: undefined,
       	reader: 
       	{
        	type: 'json',
          root: 'data'
       	},
       	timeout: 1200000
    	}
    });
  },
  
  buildColumns: function()
  {
  	return [
  	{
  		text: 'Maintenance Month',
      sortable : true,
      dataIndex: 'month',
      width: 50
  	},
  	{
  		text: 'Maintenance Year',
      sortable : true,
      dataIndex: 'year',
      width: 50
  	}, 
  	{
  		text: 'Comcast Division',
      sortable : true,
      dataIndex: 'comcastDivision',
      width: 150
  	}, 
  	{
  		text: 'VP Department',
      sortable : true,
      dataIndex: 'vpDepartment',
      width: 150
  	}, 
  	{
  		text: 'VP Name',
      sortable : true,
      dataIndex: 'vpName',
      width: 150
  	}, 
  	{
  		text: 'Dir Group',
      sortable : true,
      dataIndex: 'dirGroup',
      width: 150
  	}, 
  	{
  		text: 'Dir Name',
      sortable : true,
      dataIndex: 'dirName',
      width: 150
  	},
  	{
  		text: 'Fail Count',
      sortable : true,
      dataIndex: 'failCount',
      width: 70
  	},  
  	{
  		text: 'Fail %',
      sortable : true,
      dataIndex: 'failPct',
      width: 70,
      renderer: function(value, metaData, record, rowIndex, colIndex, store, view)
      {      	
      	var returnValue;
      	
      	if(value)
      	{
      		return value + "%";
      	}
      	else
    		{
    			return value
    		}
      }
  	},
  	{
  		text: 'Sev1 Count',
      sortable : true,
      dataIndex: 'sev1Count',
      width: 50,
      renderer: function(value, metaData, record, rowIndex, colIndex, store, view)
      {
      	metaData.tdAttr = 'data-qtip="Double-Click to see Sev1 Caused By Incident"';
      	
      	return value;
      }
  	},
  	{
  		text: 'Sev1 %',
      sortable : true,
      dataIndex: 'sev1Pct',
      width: 50,
      renderer: function(value, metaData, record, rowIndex, colIndex, store, view)
      {      	
      	var returnValue;
      	
      	metaData.tdAttr = 'data-qtip="Double-Click to see Sev1 Caused By Incident"';
      	
      	if(value)
      	{
      		return value + "%";
      	}
      	else
    		{
    			return value
    		}
      }
  	},
  	{
  		text: 'Sev1 Ticket Count',
      sortable : true,
      dataIndex: 'sev1TicketCount',
      width: 70,
      renderer: function(value, metaData, record, rowIndex, colIndex, store, view)
      {
      	metaData.tdAttr = 'data-qtip="Double-Click to see Sev1 Caused By Incident"';
      	
      	return value;
      }
  	},
  	{
  		text: 'Success Count',
      sortable : true,
      dataIndex: 'successCount',
      width: 70
  	},
  	{
  		text: 'Success %',
      sortable : true,
      dataIndex: 'successPct',
      width: 70,
      renderer: function(value, metaData, record, rowIndex, colIndex, store, view)
      {      	
      	var returnValue;
      	
      	if(value)
      	{
      		return value + "%";
      	}
      	else
    		{
    			return value
    		}
      }
  	}, 
  	{
  		text: 'Total Count',
      sortable : true,
      dataIndex: 'totalCount',
      width: 70
  	}]
  },
  buildListeners: function()
  {
  	return {
     	'celldblclick': function(view, td, columnIndex, record, tr, rowIndex, event) 
     	{
     		if((columnIndex == 10) || (columnIndex == 11) || (columnIndex == 12))
     		{
     			if(record.data.sev1Count > 0)
     			{
     				 Ext.create('SALP.view.Sev1CausedByMaintDetailsWindow',
     				 {
     				 	directorName: record.data.dirName,
     				 	month: record.data.month,
     				 	year: record.data.year
     				 }).show();
     			}
     		}
     		else
     		{
     			SALP.openSite('https://tableau.comcast.com/#/site/InfrastructureServices/views/ChangeSuccessMetrics/SuccessFailRate?:iid=1');
     		}
     	},
      scope: this
    }
  }
});