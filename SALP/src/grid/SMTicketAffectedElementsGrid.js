﻿//
//
//  SM Ticket Affected Elements Grid
//
//

Ext.define('SALP.grid.SMTicketAffectedElementsGrid', 
{
	alias: 'widget.grid.SMTicketAffectedElementsGrid',
	extend: 'Ext.grid.Panel',
	
	stripeRows: true,
  columnLines: true, 

  selModel: 
	{
    type: 'spreadsheet',
    columnSelect: true  
	},
	
	plugins: 'clipboard',
  
	
	initComponent: function()
  {
		this.store = this.buildStore();
		
		this.columns = this.buildColumns();
		
		this.listeners = this.buildListeners();
		
    this.callParent(arguments);
  },
  
  
  
  buildStore: function()
  {
  	return Ext.create('Ext.data.Store', 
    {
    	autoLoad: true,
    	model: 'SMTicketAffectedElements',
    	proxy: 
    	{
      	type: 'ajax',
       	url: 'WebServices/Service.svc/GetSMTicketAffectedElements',
       	extraParams:
       	{
					ticketNumber: this.ticketNumber
       	},
       	pageParam: undefined,
    		startParam: undefined,
    		limitParam: undefined,
       	reader: 
       	{
        	type: 'json',
          root: 'data'
       	}
    	}
//    	listeners: 
//    	{
//				'load': function(store, records, successfull, operation, options)
//				{
//					this.setHeight(30 + records.length * 20);
//				},
//				scope: this
//			}
    });
  },
  
  
  buildColumns: function()
  {
  	return [
  	{
  		text: 'Entry Id',
      dataIndex: 'entryId',
      flex: 1
  	},
  	{
  		text: 'Element',
      dataIndex: 'element',
      flex: 1
  	},
  	{
  		text: 'Service Affected',
      dataIndex: 'serviceAffected',
      flex: 1
  	},
  	{
  		text: 'Start Date',
      dataIndex: 'actualStart',
      flex: 1
  	},
  	{
  		text: 'End Date',
      dataIndex: 'actualEnd',
      flex: 1
  	}]
  },
  buildListeners: function()
  {
  	return {
//  		rowdblclick: function(grid, record, object, rowIndex)
//      {
//	    	SALP.getIncidentTicket(record.data.ttsId)
//     	},
     	'celldblclick': function(view, td, cellIndex, record, tr, rowIndex, event) 
     	{
     		var entryId = record.data.entryId;
     		
				if (cellIndex == 1)
				{
        	Ext.create('SALP.view.MultiTicketWindow',
        	{
        		ticketNumber: entryId
        	}).show();
				}
     	},
      scope: this
    }
  }
});