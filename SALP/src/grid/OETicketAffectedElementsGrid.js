﻿//
//
//  OE Affected Elements Grid
//
//

Ext.define('SALP.grid.OETicketAffectedElementsGrid', 
{
	alias: 'widget.grid.OETicketAffectedElementsGrid',
	extend: 'Ext.grid.Panel',
	
	stripeRows: true,
  columnLines: true, 
  scrollable: true,
	
	initComponent: function()
  {
		this.store = this.buildStore();
		
		this.columns = this.buildColumns();
		
    this.callParent(arguments);
  },
  
  
  
  buildStore: function()
  {
  	return Ext.create('Ext.data.Store', 
    {
    	autoLoad: true,
    	model: 'OETicketAffectedElements',
    	proxy: 
    	{
      	type: 'ajax',
       	url: 'WebServices/Service.svc/GetOETicketAffectedElements',
       	extraParams:
       	{
					ticketNumber: this.ticketNumber
       	},
       	pageParam: undefined,
    		startParam: undefined,
    		limitParam: undefined,
       	reader: 
       	{
        	type: 'json',
          root: 'data'
       	}
    	}
//    	listeners: 
//    	{
//				'load': function(store, records, successfull, operation, options)
//				{
//					this.setHeight(30 + records.length * 20);
//				},
//				scope: this
//			}
    });
  },
  
  
  buildColumns: function()
  {
  	return [
  	{
  		text: 'Entry ID',
      dataIndex: 'entryId',
      width: 100,
      renderer: function(value, metaData, record, rowIdx, colIdx, store) 
      {
        metaData.tdAttr = 'data-qtip="' + value + '"';
        return value;
    	}
  	},
  	{
  		text: 'Affect',
      dataIndex: 'serviceAffected',
      width: 50,
      renderer: function(value, metaData, record, rowIdx, colIdx, store) 
      {
        metaData.tdAttr = 'data-qtip="' + value + '"';
        return value;
    	}
  	},
  	{
  		text: 'Element',
      dataIndex: 'elementName',
      width: 50,
      renderer: function(value, metaData, record, rowIdx, colIdx, store) 
      {
        metaData.tdAttr = 'data-qtip="' + value + '"';
        return value;
    	}
  	},
  	{
  		text: 'Video',
      dataIndex: 'videoAffected',
      flex: 1
  	},
  	{
  		text: 'HSD',
      dataIndex: 'hsdAffected',
      flex: 1
  	},
  	{
  		text: 'Telephony',
      dataIndex: 'telephonyAffected',
      flex: 1
  	},
  	{
  		text: 'Total',
      dataIndex: 'totalAffected',
      flex: 1
  	},
  	{
  		text: 'Status',
      dataIndex: 'aeStatus',
      width: 100,
      renderer: function(value, metaData, record, rowIdx, colIdx, store) 
      {
        metaData.tdAttr = 'data-qtip="' + value + '"';
        return value;
    	}
  	},
  	{
  		text: 'Actual Start',
      dataIndex: 'actualStart',
      width: 100,
      renderer: function(value, metaData, record, rowIdx, colIdx, store) 
      {
        metaData.tdAttr = 'data-qtip="' + value + '"';
        return value;
    	}
  	},
  	{
  		text: 'Actual End',
      dataIndex: 'actualEnd',
      width: 100,
      renderer: function(value, metaData, record, rowIdx, colIdx, store) 
      {
        metaData.tdAttr = 'data-qtip="' + value + '"';
        return value;
    	}
  	},
  	{
  		text: 'Element Type',
      dataIndex: 'elementType',
      width: 100,
      renderer: function(value, metaData, record, rowIdx, colIdx, store) 
      {
        metaData.tdAttr = 'data-qtip="' + value + '"';
        return value;
    	}
  	},
  	{
  		text: 'Planned?',
      dataIndex: 'actualEnd',
      width: 100,
      renderer: function(value, metaData, record, rowIdx, colIdx, store) 
      {
      	if(value == 1)
      	{
      		value = 'Planned'
      	}
      	else
      	{
      		value = 'Unplanned'      	
      	}
      	
        metaData.tdAttr = 'data-qtip="' + value + '"';
        return value;
    	}
  	},
  	{
  		text: 'System',
      dataIndex: 'systemName',
      width: 100,
      renderer: function(value, metaData, record, rowIdx, colIdx, store) 
      {
        metaData.tdAttr = 'data-qtip="' + value + '"';
        return value;
    	}
  	},
  	{
  		text: 'Cause Code',
      dataIndex: 'causeCode',
      width: 100,
      renderer: function(value, metaData, record, rowIdx, colIdx, store) 
      {
        metaData.tdAttr = 'data-qtip="' + value + '"';
        return value;
    	}
  	},
  	{
  		text: 'Cause Description',
      dataIndex: 'causeDescription',
      width: 100,
      renderer: function(value, metaData, record, rowIdx, colIdx, store) 
      {
        metaData.tdAttr = 'data-qtip="' + value + '"';
        return value;
    	}
  	},
  	{
  		text: 'Solution Code',
      dataIndex: 'solutionCode',
      width: 100,
      renderer: function(value, metaData, record, rowIdx, colIdx, store) 
      {
        metaData.tdAttr = 'data-qtip="' + value + '"';
        return value;
    	}
  	},
  	{
  		text: 'Solution Description',
      dataIndex: 'solutionDescription',
      width: 100,
      renderer: function(value, metaData, record, rowIdx, colIdx, store) 
      {
        metaData.tdAttr = 'data-qtip="' + value + '"';
        return value;
    	}
  	},
  	{
  		text: 'Submitter',
      dataIndex: 'submitter',
      width: 100,
      renderer: function(value, metaData, record, rowIdx, colIdx, store) 
      {
        metaData.tdAttr = 'data-qtip="' + value + '"';
        return value;
    	}
  	}]
  }
});