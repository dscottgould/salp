﻿//
//
//  Favorite Links Grid
//
//

Ext.define('SALP.grid.FavoriteLinksGrid', 
{
	itemId: 'FavoriteLinksGrid',
	alias: 'widget.grid.FavoriteLinksGrid',
	extend: 'Ext.grid.Panel',
	
	
	stripeRows: true,
  columnLines: true, 
  
  
  initComponent: function()
  {
  	this.tbar = this.buildToolbar();
  	
		this.store = SALP.data.FavoriteLinks.store;
		
		this.columns = this.buildColumns();
		
		this.listeners = this.buildListeners();
		
    this.callParent(arguments);
  },
  
  
  buildToolbar: function()
  {
  	return [
  	'->',
  	{
  		text: 'Manage',
  		iconCls: 'linkIcon',
  		tooltip: 'Manage Links',
  		handler: function(button, event)
  		{
  			this.manageLinks();
  		},
  		scope: this
  	},
  	{
  		text: 'Add',
  		iconCls: 'addLink',
  		tooltip: 'Add Link',
  		handler: function(button, event)
  		{
  			this.addLink();
  		},
  		scope: this
  	},
  	{
  		text: 'Edit',
  		iconCls: 'editLink',
  		tooltip: 'Edit Link',
  		handler: function(button, event)
  		{
  			this.editLink();
  		},
  		scope: this
  	},
  	{
  		text: 'Delete',
  		iconCls: 'deleteLink',
  		tooltip: 'Delete Link',
  		handler: function(button, event)
  		{
  			this.deleteLink();
  		},
  		scope: this
  	}]
  },
   
  buildStore: function()
  {
  	return Ext.create('Ext.data.Store', 
    {
    	storeId: 'FavoriteLinksStore',
    	autoLoad: true,
    	model: SALP.data.FavoriteLinks.model ,
    	proxy: 
    	{
      	type: 'ajax',
       	url: 'WebServices/Service.svc/GetUserFavoriteLinks',
       	extraParams:
       	{
   		  	accountName: SALP.user.accountName
       	},
       	pageParam: undefined,
    		startParam: undefined,
    		limitParam: undefined,
       	reader: 
       	{
        	type: 'json',
          root: 'data'
       	}
    	}
    });
  },
  
  
  buildColumns: function()
  {
  	return [
  	{
  		dataIndex: 'linkId',
  		hidden: true
  	},
  	{
  		text: 'Group',
      flex: .5,
      sortable : true,
      dataIndex: 'group'
  	},
  	{
  		text: 'Description',
      flex: 1,
      sortable : true,
      dataIndex: 'description'
  	},
  	{
      dataIndex: 'link',
      hidden: true
  	}]
  },
  
  buildListeners: function()
  {
  	return {
  		rowdblclick: function(grid, record, object, rowIndex)
      {
	    	SALP.openSite(record.data.link);
     	},
      scope: this
    }
  },
  
  
  manageLinks: function()
  {
  	SALP.manageLinks();
  },

  
  addLink: function()
  {
  	SALP.view.AddEditFavoriteLinkWindow = Ext.create('SALP.view.AddEditFavoriteLinkWindow',
  	{
  		mode: 'Add'
  	}).show();	
  },
  
  editLink: function()
  {
  	var selection = this.selection;
  	
  	if(selection)
  	{
  		SALP.view.AddEditFavoriteLinkWindow = Ext.create('SALP.view.AddEditFavoriteLinkWindow',
  		{
  			mode: 'Edit',
  			record: selection
  		}).show();
  	}
  },
  deleteLink: function()
  {
  	var selection = this.selection;
  	
  	if(selection)
  	{
  		SALP.view.DeleteFavoriteLinkWindow = Ext.create('SALP.view.DeleteFavoriteLinkWindow',
  		{
  			record: selection
  		}).show();
  	}
  }
});