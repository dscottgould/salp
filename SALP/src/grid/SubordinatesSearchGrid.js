﻿//
//
//  User Lookup Grid
//
//

Ext.define('SALP.grid.SubordinatesSearchGrid', 
{
	alias: 'widget.grid.SubordinatesSearchGrid',
	extend: 'Ext.grid.Panel',
	
	stripeRows: true,
  columnLines: true, 
	//autoScroll: true,
  //scrollable: true,
  
  emptyText: 'No Records Found.', 
	
	selModel: 
	{
    type: 'spreadsheet',
    columnSelect: true  
	},
  
  plugins: [
	{
  	ptype: 'gridexporter'
  },
  {
  	ptype: 'clipboard'
  }],
  
	
	initComponent: function()
  {
		this.store = this.buildStore();
		
		this.columns = this.buildColumns();
		
		this.listeners = this.buildListeners();
		
    this.callParent(arguments);
  },
  
  
  
  buildStore: function()
  {
  	return Ext.create('Ext.data.Store', 
    {
    	autoLoad: true,
    	model: 'SAPSubordinate',
    	proxy: 
    	{
      	type: 'ajax',
       	url: 'WebServices/Service.svc/GetSAPSubordinates',
       	extraParams:
       	{
					name: this.searchName
       	},
       	pageParam: undefined,
    		startParam: undefined,
    		limitParam: undefined,
       	reader: 
       	{
        	type: 'json',
          root: 'data'
       	},
       	timeout: 1200000
    	}
//    	listeners: 
//    	{
//				'load': function(store, records, successfull, operation, options)
//				{
//					this.setHeight(30 + records.length * 20);
//				},
//				scope: this
//			}
    });
  },
  
  
  buildColumns: function()
  {
  	return [
//  	{
//  		text: 'Employee ID',
//      dataIndex: 'employeeId',
//      width: 100,
//      renderer: function(value, metaData, record, rowIdx, colIdx, store) 
//      {
//        metaData.tdAttr = 'data-qtip="' + value + '"';
//        return value;
//    	}
//  	},
  	{
  		text: 'Name',
      dataIndex: 'name',
      width: 100,
      renderer: function(value, metaData, record, rowIndex, colIndex, store, view)
      {
      	metaData.tdAttr = 'data-qtip="Double-Click to see Subordinates"';
      	
      	return value;
      }
  	},
  	{
  		text: 'LoginId',
      dataIndex: 'loginId',
      width: 100,
      renderer: function(value, metaData, record, rowIdx, colIdx, store) 
      {
        metaData.tdAttr = 'data-qtip="' + value + '"';
        return value;
    	}
  	},
  	{
  		text: 'Job Level Code',
      dataIndex: 'jobLevelCode',
      width: 100,
      renderer: function(value, metaData, record, rowIdx, colIdx, store) 
      {
        metaData.tdAttr = 'data-qtip="' + value + '"';
        return value;
    	}
  	},
  	{
  		text: 'Employee Status Description',
      dataIndex: 'employeeStatusDescription',
      width: 100,
      renderer: function(value, metaData, record, rowIdx, colIdx, store) 
      {
        metaData.tdAttr = 'data-qtip="' + value + '"';
        return value;
    	}
  	},
  	{
  		text: 'Supervisor Name',
      dataIndex: 'supervisorName',
      width: 100,
      renderer: function(value, metaData, record, rowIndex, colIndex, store, view)
      {
      	metaData.tdAttr = 'data-qtip="Double-Click to see Subordinates"';
      	
      	return value;
      }
  	},
  	{
  		text: 'Employee Status',
      dataIndex: 'employeeStatus',
      width: 100,
      renderer: function(value, metaData, record, rowIdx, colIdx, store) 
      {
        metaData.tdAttr = 'data-qtip="' + value + '"';
        return value;
    	}
  	},
  	{
  		text: 'Work Email',
      dataIndex: 'workEmail',
      width: 100,
      renderer: function(value, metaData, record, rowIdx, colIdx, store) 
      {
        metaData.tdAttr = 'data-qtip="' + value + '"';
        return value;
    	}
  	},
  	{
  		text: 'Path',
      dataIndex: 'path',
      width: 100,
      renderer: function(value, metaData, record, rowIdx, colIdx, store) 
      {
        metaData.tdAttr = 'data-qtip="' + value + '"';
        return value;
    	}
  	},
  	{
  		text: 'Direct',
      dataIndex: 'direct',
      width: 100,
      renderer: function(value, metaData, record, rowIdx, colIdx, store) 
      {
        metaData.tdAttr = 'data-qtip="' + value + '"';
        return value;
    	}
  	}]
  },
  
  buildListeners: function()
  {
  	return {
  		'celldblclick': function(grid, td, cellIndex, record, tr, rowIndex, event, options)
  		{	
  			if(cellIndex == 1)
  			{
  				this.up().setTitle("SAP Subordinates Lookup - " + record.data.name);
  				
  				this.getStore().load(
  				{
  					params:
  					{
  						name: record.data.name
  					}
  				})
  			}
  			
  			if(cellIndex == 5)
  			{
  				this.up().setTitle("SAP Subordinates Lookup - " + record.data.supervisorName);
  				
  				this.getStore().load(
  				{
  					params:
  					{
  						name: record.data.supervisorName
  					}
  				})
  			}
  			
  			
  			var t=1;
  		}, 
  		scope: this
  	}
  }
});