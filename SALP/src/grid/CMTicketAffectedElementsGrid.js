﻿//
//
//  CM Affected Elements Grid
//
//

Ext.define('SALP.grid.CMTicketAffectedElementsGrid', 
{
	alias: 'widget.grid.CMTicketAffectedElementsGrid',
	extend: 'Ext.grid.Panel',
	
	stripeRows: true,
  columnLines: true, 
  
	selModel: 
	{
    type: 'spreadsheet',
    columnSelect: true  
	},
	
	plugins: 'clipboard',
	
	
	initComponent: function()
  {
		this.store = this.buildStore();
		
		this.columns = this.buildColumns();
		
		this.listeners = this.buildListeners();
		
    this.callParent(arguments);
  },
  
  
  
  buildStore: function()
  {
  	return Ext.create('Ext.data.Store', 
    {
    	autoLoad: true,
    	model: 'CMTicketAffectedElements',
    	proxy: 
    	{
      	type: 'ajax',
       	url: 'WebServices/Service.svc/GetCMTicketAffectedElements',
       	extraParams:
       	{
					ticketNumber: this.ticketNumber
       	},
       	pageParam: undefined,
    		startParam: undefined,
    		limitParam: undefined,
       	reader: 
       	{
        	type: 'json',
          root: 'data'
       	}
    	}
//    	listeners: 
//    	{
//				'load': function(store, records, successfull, operation, options)
//				{
//					this.setHeight(30 + records.length * 20);
//				},
//				scope: this
//			}
    });
  },
  
  
  buildColumns: function()
  {
  	return [
  	{
  		text: 'Entry ID',
      dataIndex: 'entryId',
      flex: 1,
      renderer: function(value, metaData, record, rowIdx, colIdx, store) 
      {
        metaData.tdAttr = 'data-qtip="' + value + '"';
        return value;
    	}
  	},
  	{
  		text: 'Affect',
      dataIndex: 'serviceAffected',
      flex: 1,
      renderer: function(value, metaData, record, rowIdx, colIdx, store) 
      {
        metaData.tdAttr = 'data-qtip="' + value + '"';
        return value;
    	}
  	},
  	{
  		text: 'Element',
      dataIndex: 'elementName',
      flex: 1,
      renderer: function(value, metaData, record, rowIdx, colIdx, store) 
      {
        metaData.tdAttr = 'data-qtip="' + value + '"';
        return value;
    	}
  	},
  	{
  		text: 'Video',
      dataIndex: 'videoAffected',
      flex: 1
  	},
  	{
  		text: 'HSD',
      dataIndex: 'hsdAffected',
      flex: 1
  	},
  	{
  		text: 'Telephony',
      dataIndex: 'telephonyAffected',
      flex: 1
  	},
  	{
  		text: 'Total',
      dataIndex: 'totalAffected',
      flex: 1
  	},
  	{
  		text: 'Status',
      dataIndex: 'status',
      flex: 1,
      renderer: function(value, metaData, record, rowIdx, colIdx, store) 
      {
        metaData.tdAttr = 'data-qtip="' + value + '"';
        return value;
    	}
  	},
  	{
  		text: 'Actual Start',
      dataIndex: 'actualStart',
      flex: 1,
      renderer: function(value, metaData, record, rowIdx, colIdx, store) 
      {
        metaData.tdAttr = 'data-qtip="' + value + '"';
        return value;
    	}
  	},
  	{
  		text: 'Actual End',
      dataIndex: 'actualEnd',
      flex: 1,
      renderer: function(value, metaData, record, rowIdx, colIdx, store) 
      {
        metaData.tdAttr = 'data-qtip="' + value + '"';
        return value;
    	}
  	}]
  },
  buildListeners: function()
  {
  	return {
//  		rowdblclick: function(grid, record, object, rowIndex)
//      {
//	    	SALP.getIncidentTicket(record.data.ttsId)
//     	},
     	'celldblclick': function(view, td, cellIndex, record, tr, rowIndex, event) 
     	{
     		var entryId = record.data.entryId;
     		
				if (cellIndex == 1)
				{
        	Ext.create('SALP.view.MultiTicketWindow',
        	{
        		ticketNumber: entryId
        	}).show();
				}
     	},
      scope: this
    }
  }
});