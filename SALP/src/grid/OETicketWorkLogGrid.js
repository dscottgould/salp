﻿//
//
// 	OE Ticket Worklog Grid
//
//

Ext.define('SALP.grid.OETicketWorklogGrid', 
{
	alias: 'widget.grid.OETicketWorklogGrid',
	extend: 'Ext.grid.Panel',
	
	stripeRows: true,
  columnLines: true, 

  
	
	initComponent: function()
  {
		this.store = this.buildStore();
		
		this.header = this.buildHeader();
		
		this.columns = this.buildColumns();
		
		this.listeners = this.buildListeners();
		
    this.callParent(arguments);
  },
  
  
  
  buildStore: function()
  {
  	return Ext.create('Ext.data.Store', 
    {
    	autoLoad: true,
    	model: 'OETicketWorkLog',
    	proxy: 
    	{
      	type: 'ajax',
       	url: 'WebServices/Service.svc/GetOETicketWorkLog',
       	extraParams:
       	{
					ticketNumber: this.ticketNumber,
					source: 'none'
       	},
       	pageParam: undefined,
    		startParam: undefined,
    		limitParam: undefined,
       	reader: 
       	{
        	type: 'json',
          root: 'data'
       	}
    	}
    });
  },
  
  buildHeader: function()
  {
  	return   {
    	itemPosition: 1, // after title before collapse tool
    	items: [
    	{
				xtype: 'combobox',
				store: SALP.data.WorkLog.select,
				queryMode: 'local',
				displayField: 'displayName',
				valueField: 'submitValue',
				value: 'none',
    		listeners : 
    		{
    			select : function(comboBox, dataModel, object) 
    			{
  					this.store.load
        		({
        			params:
        			{
        				ticketNumber: this.ticketNumber,
								source: dataModel.data.submitValue
        			}
        		});
      		},
      		scope: this
      	}
    	}]
  	}
  },
  
  buildColumns: function()
  {
  	return [
  	{
      text: 'Source Ticket',
      sortable : true,
      dataIndex: 'sourceTicket',
      flex: 1
  	},
  	{
  		text: 'Create Date',
      sortable : true,
      dataIndex: 'createDate',
      flex: 1,
      renderer: function(value, metaData, record, rowIdx, colIdx, store) 
      {
        metaData.tdAttr = 'data-qtip="' + value + '"';
        return value;
    	}
  	},
  	{
  		text: 'Submitter',
      sortable : true,
      dataIndex: 'submitter',
      flex: 1,
      renderer: function(value, metaData, record, rowIdx, colIdx, store) 
      {
        metaData.tdAttr = 'data-qtip="' + value + '"';
        return value;
    	}
  	},
  	{
  		text: 'Source',
      sortable : true,
      dataIndex: 'source',
      flex: 1,
      renderer: function(value, metaData, record, rowIdx, colIdx, store) 
      {
        metaData.tdAttr = 'data-qtip="' + value + '"';
        return value;
    	}
  	},
  	{
  		text: 'Subject',
      sortable : true,
      dataIndex: 'subject',
      flex: 1,
      renderer: function(value, metaData, record, rowIdx, colIdx, store) 
      {
        metaData.tdAttr = 'data-qtip="' + value + '"';
        return value;
    	}
  	},
  	{
  		text: 'Details',
      sortable : true,
      dataIndex: 'details',
      flex: 1,
      renderer: function(value, metaData, record, rowIndex, colIndex, store, view)
      {
      	
      	metaData.tdAttr = 'data-qtip="Double Click for Detail Window"';
      	
      	return value;
      }
  	}]
 	},
 	buildListeners: function()
  {
  	return {
//  		rowdblclick: function(grid, record, object, rowIndex)
//      {
//	    	SALP.getIncidentTicket(record.data.ttsId)
//     	},
     	'celldblclick': function(view, td, cellIndex, record, tr, rowIndex, event) 
     	{
     		var ticketId = record.data.ticketId;
     		
				if (cellIndex == 5)
				{
					this.getWorkLogDetails(record)
				}
     	},
      scope: this
    }
  },
  getWorkLogDetails: function(record)
  {
  	if (record.data.source != 'Notification')
  	{
  		SALP.view.workLogDetailsWindow = Ext.create('SALP.view.WorkLogDetailsWindow', 
  		{
  			details: record.data.details
  		}).show();
  	}
  	else
  	{
  		SALP.view.workLogNotificationsWindow = Ext.create('SALP.view.WorklogNotificationsWindow',
  		{
  			ticketNumber: record.data.ticketId,
  			subject: record.data.subject
  		}).show();
  	}
  }
});