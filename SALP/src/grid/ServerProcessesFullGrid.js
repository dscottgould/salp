﻿//
//
//  Server Processes Basic Grid
//
//

Ext.define('SALP.grid.ServerProcessesFullGrid', 
{
	alias: 'widget.grid.ServerProcessesFullGrid',
	extend: 'Ext.grid.Panel',
	
	stripeRows: true,
  columnLines: true, 

//	selModel: 
//	{
//    type: 'spreadsheet',
//    columnSelect: true  
//	},
	
	initComponent: function()
  {
		this.store = this.buildStore();
		
		this.columns = this.buildColumns();
		
		this.listeners = this.buildListeners();
		
		
		this.task = new Ext.util.DelayedTask(function()
		{
			if (this.store)
			{
				this.store.reload();
			}
  	
  		Ext.defer(function() 
      {
      	this.setLoading(false);
      }, 2000, this);
  	
  		this.task.delay(60000);
  		
		}, this);
		
		
		this.task.delay(60000);
		
    this.callParent(arguments);
  },
  
  
  
  buildStore: function()
  {
  	return Ext.create('Ext.data.Store', 
    {
    	storeId: 'CurrentServerProcesses',
    	autoLoad: true,
    	model: SALP.data.CurrentServerProcesses.model,
    	proxy: 
    	{
      	type: 'ajax',
       	url: 'WebServices/Service.svc/GetServerProcesses',
       	extraParams:
       	{
					serverName: this.server
       	},
       	pageParam: undefined,
    		startParam: undefined,
    		limitParam: undefined,
       	reader: 
       	{
        	type: 'json',
          root: 'data'
       	}
    	}
    });
  },
  
  
  buildColumns: function()
  {
  	return [
  	{
  		xtype: 'actioncolumn',
  		width: 25,
  		items: [
  		{
  			icon: '/img/cog_stop.png',
  			tooltip: 'Kill Process',
  			handler: function(grid, rowIndex, colIndex) 
  			{
  				var record = grid.getStore().getAt(rowIndex);
  				
  				Ext.create('SALP.view.KillProcessDialog',
  				{
  					server: this.up().up().server,
  					record: record,
  					store: grid.getStore()
  				}).show();
  			}
  		}]
  	},
  	{
  		text: 'Session ID',
      sortable : true,
      dataIndex: 'sessionId',
      flex: 1
  	},
  	{
  		text: 'Blocked By',
      sortable : true,
      dataIndex: 'blockedBy',
      flex: 1,
      renderer: function(value, metaData, record, rowIndex, colIndex, store, view)
      {
      	var blockedBy = record.data.blockedBy;
      	
      	if(blockedBy > 0)
      	{
      		metaData.style = "background-color:#FC8F8F;";
      	}
      	
      	return value;
      }
  	},
  	{
  		text: 'Database Name',
      sortable : true,
      dataIndex: 'databaseName',
      flex: 1
  	},
  	{
  		text: 'Running Minutes',
      sortable : true,
      dataIndex: 'runningMinutes',
      flex: 1
  	},
  	{
  		text: 'Running From',
      sortable : true,
      dataIndex: 'runningFrom',
      flex: 1,
      renderer: function(value, metaData, record, rowIndex, colIndex, store, view)
      {
      	metaData.tdAttr = 'data-qtip="' + value + '"';
      	
      	return value;
      }
  	},
  	{
  		text: 'Running By',
      sortable : true,
      dataIndex: 'runningBy',
      flex: 1,
      renderer: function(value, metaData, record, rowIndex, colIndex, store, view)
      {
      	metaData.tdAttr = 'data-qtip="' + value + '"';
      	
      	return value;
      }
  	},
  	{
  		text: 'Program Name',
      sortable : true,
      dataIndex: 'programName',
      flex: 1,
      renderer: function(value, metaData, record, rowIndex, colIndex, store, view)
      {
      	metaData.tdAttr = 'data-qtip="' + value + '"';
      	
      	return value;
      }
  	},
  	{
  		text: 'Command',
      sortable : true,
      dataIndex: 'cmd',
      flex: 1
  	},
  	{
  		text: 'Login Name',
      sortable : true,
      dataIndex: 'loginName',
      flex: 1,
      renderer: function(value, metaData, record, rowIndex, colIndex, store, view)
      {
      	metaData.tdAttr = 'data-qtip="' + value + '"';
      	
      	return value;
      }
  	},
  	{
  		text: 'Status',
      sortable : true,
      dataIndex: 'status',
      flex: 1
  	},
  	{
  		text: 'Reads',
      sortable : true,
      dataIndex: 'reads',
      flex: 1
  	},
  	{
  		text: 'Writes',
      sortable : true,
      dataIndex: 'writes',
      flex: 1
  	},
  	{
  		text: 'Logical Reads',
      sortable : true,
      dataIndex: 'logicalReads',
      flex: 1
  	},
  	{
  		text: 'Wait Time',
      sortable : true,
      dataIndex: 'waitTime',
      flex: 1
  	},
  	{
  		text: 'Last Wait Type',
      sortable : true,
      dataIndex: 'lastWaitType',
      flex: 1,
      renderer: function(value, metaData, record, rowIndex, colIndex, store, view)
      {
      	metaData.tdAttr = 'data-qtip="' + value + '"';
      	
      	return value;
      }
  	},
  	{
  		text: 'Last Request Start Time',
      sortable : true,
      dataIndex: 'lastRequestStartTime',
      flex: 1,
      renderer: function(value, metaData, record, rowIndex, colIndex, store, view)
      {
      	metaData.tdAttr = 'data-qtip="' + value + '"';
      	
      	value = Ext.Date.format(value, 'm/d/Y H:i');  
      	
      	return value;
      }
  	}]
 	},
 	
 	buildListeners: function()
  {
  	return {
  		celldblclick: function(grid, td, cellIndex, record, tr, rowIndex, event, eOpts)
  		{
  			if((cellIndex == 2) && (record.data.blockedBy > 0))
  			{
  				var blockedBy = record.data.blockedBy,
  						blockedByRecord = Ext.getStore('CurrentServerProcesses').findRecord('sessionId', blockedBy);
  				
  				if(blockedByRecord)
  				{
    				Ext.create('SALP.view.ServerProcessesDetailsWindow',
        		{
        			server: this.server,
        			record: blockedByRecord
        		}).show();
  				}		

  				var t=1;
  			}
  			else
  			{
  				Ext.create('SALP.view.ServerProcessesDetailsWindow',
      		{
      			server: this.server,
      			record: record
      		}).show();
  			}
  		},
  		
//  		rowdblclick: function(grid, record, object, rowIndex)
//      {
//      	if(Ext.getCmp('ServerProcessesDetailsWindow'))
//      	{
//      		SALP.view.ServerProcessesDetailsWindow.toFront();
//      	}
//      	else
//      	{
//      		SALP.view.ServerProcessesDetailsWindow = Ext.create('SALP.view.ServerProcessesDetailsWindow',
//      		{
//      			server: this.server,
//      			record: record
//      		}).show();
//      	}
//     	},
      scope: this
    }
  }
});