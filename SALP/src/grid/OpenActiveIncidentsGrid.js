﻿//
//
//  Open/Active Incidents Grid
//
//

Ext.define('SALP.grid.OpenActiveIncidentsGrid', 
{
	alias: 'widget.grid.OpenActiveIncidentsGrid',
	extend: 'Ext.grid.Panel',
	
	stripeRows: true,
  columnLines: true, 
	//hideHeaders: true,
  
	
	initComponent: function()
  {
		this.task = new Ext.util.DelayedTask(function()
		{
  		this.setLoading('Loading...');
  		
  		if(this.store)
  		{
  			this.store.reload();
  		}
  	
  		Ext.defer(function() 
      {
      	this.setLoading(false);
      }, 2000, this);
  	
  		this.task.delay(900000);
  		
		}, this);
		

		this.task.delay(900000);
  	
		this.store = this.buildStore();
		
		this.columns = this.buildColumns();
		
    this.callParent(arguments);
  },
  
  
  
  buildStore: function()
  {
  	var searchString = Ext.util.Cookies.get('IncidentsSearch');
  	
  	if(!searchString)
  	{
  		searchString = 5
  	}
  	
  	return Ext.create('Ext.data.Store', 
    {
    	autoLoad: true,
    	model: 'OpenTickets',
    	proxy: 
    	{
      	type: 'ajax',
       	url: 'WebServices/Service.svc/GetActiveTickets',
       	extraParams:
       	{
					severity: searchString
       	},
       	pageParam: undefined,
    		startParam: undefined,
    		limitParam: undefined,
       	reader: 
       	{
        	type: 'json',
          root: 'data'
       	}
    	}
    });
  },

  
  buildColumns: function()
  {
  	return [
  	{
  		text: 'Ticket ID',
      sortable : true,
      dataIndex: 'ticketId',
      width: 65,
      renderer : function(value)
      {
      	var t=1;
      	
      	return "<a href='javascript:void(0)' onclick='SALP.openTicket(\"" + value + "\");'>" + value + "</a>";
      }
  	},
  	{
  		text: 'Incident Summary',
      sortable : true,
      dataIndex: 'incidentSummary',
      width: 170,
      renderer: function(value, metaData, record, rowIdx, colIdx, store) 
      {
        metaData.tdAttr = 'data-qtip="' + this.buildTooltip(record) + '"';
        return value;
    	}
  	},
  	{
  		text: 'Severity',
      sortable : true,
      dataIndex: 'severity',
      width: 40,
      renderer: function(value, metaData, record, rowIdx, colIdx, store) 
      {
        metaData.tdAttr = 'data-qtip="' + this.buildTooltip(record) + '"';
        return value;
    	}
  	},
  	{
  		text: 'Create Date (MT)',
      sortable : true,
      dataIndex: 'createDateMt',
      width: 100,
      renderer: function(value, metaData, record, rowIdx, colIdx, store) 
      {
      	value = Ext.Date.format(value, 'm/d/Y H:i');;
      	
      	//value = new Date(value)
      	
        metaData.tdAttr = 'data-qtip="' + this.buildTooltip(record) + '"';
        return value;
    	}
  	}]
  },
  
  
  buildTooltip: function(record)
  {
 		var tipText = '<b>Ticket ID:</b> ' + record.data.ticketId + '<br/>';
      	tipText += '<b>Support Area Name:</b> ' + record.data.supportAreaName + '<br/>';
      	tipText += '<b>Severity:</b> ' + record.data.severity + '<br/>';
      	tipText += '<b>Service Condition:</b> ' + record.data.serviceCondition + '<br/>';
      	tipText += '<b>Region Name:</b> ' + record.data.regionName + '<br/>';
      	tipText += '<b>Queue Name:</b> ' + record.data.queueName + '<br/>';
      	tipText += '<b>Next Action:</b> ' + record.data.nextAction + '</br>';
      	tipText += '<b>Incident Summary:</b> ' + record.data.incidentSummary + '<br/>';
      	tipText += '<b>Create Date MT:</b> ' + Ext.Date.format(record.data.createDateMt, 'm/d/Y H:i') + '<br/>'
      	
      	
  		return tipText;
  }
});