﻿//
//
//  Sev1 Caused By Maint Details Grid
//
//

Ext.define('SALP.grid.Sev1CausedByMaintDetailsGrid', 
{
	alias: 'widget.grid.Sev1CausedByMaintDetailsGrid',
	extend: 'Ext.grid.Panel',
	
	stripeRows: true,
  columnLines: true, 
  scrollable: true,

	emptyText: 'No Records Found.', 
	
	selModel: 
	{
    type: 'spreadsheet',
    columnSelect: true  
	},

	plugins: [
	{
  	ptype: 'gridexporter'
  },
  {
  	ptype: 'clipboard'
  }],
	
	initComponent: function()
  {
  	this.header = this.buildHeader();
  	
		this.store = this.buildStore();
		
		this.columns = this.buildColumns();
		
		this.listeners = this.buildListeners();
		
    this.callParent(arguments);
  },
  
  buildHeader: function()
  {
  	return   {
    	itemPosition: 1, // after title before collapse tool
    	items: [
    	{
        xtype: 'button',
        text: 'Download',
       	listeners: 
				{
    			click: function() 
     			{
						this.saveDocumentAs(
						{
    					type: 'xlsx',
    					title: 'Maintance Ticket Details',
    					fileName: 'Sev1CausedByMaintDetails.xlsx'
						});
      		},
      		scope: this
				}
    	}]
  	}
  },
    
  buildStore: function()
  {
  	return Ext.create('Ext.data.Store', 
    {
    	autoLoad: true,
    	model: SALP.data.Sev1CausedByMaintDetails.model,
    	proxy: 
    	{
      	type: 'ajax',
       	url: 'WebServices/Service.svc/GetSev1CausedByMaintDetails',
       	extraParams:
       	{
    	  	directorName: this.directorName,
    	  	month: this.month,
  				year: this.year,
  				reportLevel: this.reportLevel
       	},
       	pageParam: undefined,
    		startParam: undefined,
    		limitParam: undefined,
       	reader: 
       	{
        	type: 'json',
          root: 'data'
       	},
       	timeout: 1200000
    	}
    });
  },
  
  buildColumns: function()
  {
  	return [
  	{
  		text: 'Maintenance Ticket Id',
      sortable : true,
      dataIndex: 'maintTicketId',
      width: 150,
      renderer: function(value, metaData, record, rowIndex, colIndex, store, view)
      {
      	metaData.tdAttr = 'data-qtip="Double-Click for Ticket Details"';
      	
      	return value;
      }
  	},
  	{
  		text: 'CM Summary',
      sortable : true,
      dataIndex: 'cmSummary',
      width: 150,
      renderer: function(value, metaData, record, rowIndex, colIndex, store, view)
      {
      	metaData.tdAttr = 'data-qtip="' + value + '"';
      	
      	return value;
      }
  	},
  	{
  		text: 'Resolution Description',
      sortable : true,
      dataIndex: 'resolutionDescription',
      width: 150,
      renderer: function(value, metaData, record, rowIndex, colIndex, store, view)
      {
      	metaData.tdAttr = 'data-qtip="' + value + '"';
      	
      	return value;
      }
  	},
  	{
  		text: 'TTS ID',
      sortable : true,
      dataIndex: 'ttsId',
      width: 150,
      renderer: function(value, metaData, record, rowIndex, colIndex, store, view)
      {
      	metaData.tdAttr = 'data-qtip="Double-Click for Ticket Details"';
      	
      	return value;
      }
  	},
  	{
  		text: 'Title',
      sortable : true,
      dataIndex: 'rcaTitle',
      width: 150
  	},
  	{
  		text: 'Actual Start',
      sortable : true,
      dataIndex: 'actualStart',
      width: 150
  	},
  	{
  		text: 'Actual End',
      sortable : true,
      dataIndex: 'actualEnd',
      width: 150
  	},
  	{
  		text: 'Cause Description',
      sortable : true,
      dataIndex: 'causeDescription',
      width: 150
  	},
  	{
  		text: 'Solution Description',
      sortable : true,
      dataIndex: 'solutionDescription',
      width: 150
  	},
  	{
  		text: 'Problem Description',
      sortable : true,
      dataIndex: 'problemDescription',
      width: 150
  	},
  	{
  		text: 'Queue Name',
      sortable : true,
      dataIndex: 'queueName',
      width: 150
  	},
  	{
  		text: 'Self Inflicted',
      sortable : true,
      dataIndex: 'selfInflicted',
      width: 150
  	},
  	{
  		text: 'Exec Summary',
      sortable : true,
      dataIndex: 'execSummary',
      width: 150,
      renderer: function(value, metaData, record, rowIndex, colIndex, store, view)
      {
      	metaData.tdAttr = 'data-qtip="' + value + '"';
      	
      	return value;
      }
  	},
  	{
  		text: 'Customer Impact',
      sortable : true,
      dataIndex: 'customerImpact',
      width: 150,
      renderer: function(value, metaData, record, rowIndex, colIndex, store, view)
      {
      	metaData.tdAttr = 'data-qtip="' + value + '"';
      	
      	return value;
      }
  	},
  	{
  		text: 'Care Impact',
      sortable : true,
      dataIndex: 'careImpact',
      width: 150,
      renderer: function(value, metaData, record, rowIndex, colIndex, store, view)
      {
      	metaData.tdAttr = 'data-qtip="' + value + '"';
      	
      	return value;
      }
  	},
  	{
  		text: 'Platform',
      sortable : true,
      dataIndex: 'platform',
      width: 150
  	},
  	{
  		text: 'Root Cause',
      sortable : true,
      dataIndex: 'rootCause',
      width: 150
  	},
  	{
  		text: 'Responsible Party',
      sortable : true,
      dataIndex: 'responsibleParty',
      width: 150
  	},
  	{
  		text: 'Root Cause',
      sortable : true,
      dataIndex: 'rootCause',
      width: 150
  	},
  	{
  		text: 'Detect',
      sortable : true,
      dataIndex: 'detect',
      width: 150
  	},
  	{
  		text: 'Redundacy',
      sortable : true,
      dataIndex: 'redundacy',
      width: 150
  	},
  	{
  		text: 'Responsible Person',
      sortable : true,
      dataIndex: 'responsiblePerson',
      width: 150
  	}]
  },
  
  buildListeners: function()
  {
  	return {
     	'celldblclick': function(view, td, columnIndex, record, tr, rowIndex, event) 
     	{
     		if(columnIndex == 1)
     		{
     			var ticketNumber = record.data.maintTicketId
     			
     			if(ticketNumber)
        	{
          	Ext.create('SALP.view.MultiTicketWindow',
          	{
          		ticketNumber: ticketNumber
          	}).show();
        	}
     		}
     		
     		if(columnIndex == 4)
     		{
     			var ticketNumber = record.data.ttsId
     			
     			if(ticketNumber)
        	{
          	Ext.create('SALP.view.MultiTicketWindow',
          	{
          		ticketNumber: ticketNumber
          	}).show();
        	}
     		}
     		
     		
//     		else if (columnIndex == 2)
//     		{
//     			SALP.getTicketDetails(record.data.id);
//     		}
//     		else
//     		{
//     			SALP.openSite('https://tableau.comcast.com/#/site/InfrastructureServices/views/ChangeSuccessMetrics/SuccessFailRate?:iid=1');
//     		}
     	},
      scope: this
    }
  }
});