﻿//
//
//  SI Ticket Customers Tickets Grid
//
//

Ext.define('SALP.grid.SITicketCustomerTicketsGrid', 
{
	alias: 'widget.grid.SITicketCustomerTicketsGrid',
	extend: 'Ext.grid.Panel',
	
	stripeRows: true,
  columnLines: true, 

  emptyText: 'No Records Found',
	
	initComponent: function()
  {
		this.store = this.buildStore();
		
		this.columns = this.buildColumns();
		
		
    this.callParent(arguments);
  },
  
  
  
  buildStore: function()
  {
  	return Ext.create('Ext.data.Store', 
    {
    	autoLoad: true,
    	model: 'SITicketCustomerTicket',
    	proxy: 
    	{
      	type: 'ajax',
       	url: 'WebServices/Service.svc/GetSITicketCustomerTickets',
       	extraParams:
       	{
					ticketNumber: this.ticketNumber
       	},
       	pageParam: undefined,
    		startParam: undefined,
    		limitParam: undefined,
       	reader: 
       	{
        	type: 'json',
          root: 'data'
       	}
    	}
//    	listeners: 
//    	{
//				'load': function(store, records, successfull, operation, options)
//				{
//					if(records.length <= 1) this.setHeight(0);
//				},
//				scope: this
//			}
    });
  },
  
  buildColumns: function()
  {
  	return [
  	{
  		text: 'Customer Ticket Id',
      sortable : true,
      dataIndex: 'customerTicketId',
      flex: 1
  	},
  	{
  		text: 'Summary',
      sortable : true,
      dataIndex: 'problemSummary',
      flex: 1,
      renderer: function(value, metaData, record, rowIdx, colIdx, store) 
      {
        metaData.tdAttr = 'data-qtip="' + value + '"';
        return value;
    	}
  	},
  	{
  		text: 'Create Date',
      sortable : true,
      dataIndex: 'createDate',
      flex: 1,
      renderer: function(value, metaData, record, rowIdx, colIdx, store) 
      {
        metaData.tdAttr = 'data-qtip="' + value + '"';
        return value;
    	}
  	},
  	{
  		text: 'Contact',
      sortable : true,
      dataIndex: 'contactName',
      flex: 1,
      renderer: function(value, metaData, record, rowIdx, colIdx, store) 
      {
        metaData.tdAttr = 'data-qtip="' + value + '"';
        return value;
    	}
  	},
  	{
  		text: 'Status',
      sortable : true,
      dataIndex: 'status',
      flex: 1
  	},
  	{
  		text: 'Node',
      sortable : true,
      dataIndex: 'node',
      flex: 1
  	}]
  }
});