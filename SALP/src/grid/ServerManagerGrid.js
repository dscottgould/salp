﻿//
//
//  ServerManagerGrid.js
//
//

Ext.define('SALP.grid.ServerManagerGrid', 
{
	alias: 'widget.grid.ServerManagerGrid',
	extend: 'Ext.grid.Panel',
	
	stripeRows: true,
  columnLines: true, 

  
	
	initComponent: function()
  {
  	this.tbar = this.buildToolbar();
  	
		this.store = this.buildStore();
		
		this.columns = this.buildColumns();
		
		
    this.callParent(arguments);
  },
  
  buildToolbar: function()
  {
  	return [
  	'->',
  	{
  		text: 'Add',
  		iconCls: 'serverAdd',
  		tooltip: 'Add Server',
  		handler: function(button, event)
  		{
  			this.addServer();
  		},
  		scope: this
  	},
  	{
  		text: 'Edit',
  		iconCls: 'serverEdit',
  		tooltip: 'Edit Server',
  		handler: function(button, event)
  		{
  			this.editServer();
  		},
  		scope: this
  	},
  	{
  		text: 'Delete',
  		iconCls: 'serverDelete',
  		tooltip: 'Delete Server',
  		handler: function(button, event)
  		{
  			this.deleteServer();
  		},
  		scope: this
  	}]
  },
  
  buildStore: function()
  {
  	return Ext.create('Ext.data.Store', 
    {
    	autoLoad: true,
    	model: SALP.data.Servers.model,
    	proxy: 
    	{
      	type: 'ajax',
       	url: 'WebServices/Service.svc/GetServersList',
       	extraParams:
       	{
					//serverName: this.server
       	},
       	pageParam: undefined,
    		startParam: undefined,
    		limitParam: undefined,
       	reader: 
       	{
        	type: 'json',
          root: 'data'
       	}
    	}
    });
  },
  
  
  buildColumns: function()
  {
  	return [
  	{
  		text: 'ID',
      dataIndex: 'id',
      hidden: true
  	},
  	{
  		text: 'Server Name',
      sortable : true,
      dataIndex: 'serverName',
      flex: 1
  	},
  	{
  		text: 'Monitored Processses',
      sortable : true,
      dataIndex: 'hasMonitoredProcesses',
      flex: 1
  	},
  	{
  		text: 'Long Running Jobs',
      sortable : true,
      dataIndex: 'hasLongRunningJobs',
      flex: 1
  	}]
 	},
 	
 	addServer: function()
  {
  	Ext.create('SALP.view.AddEditServersWindow',
  	{
  		mode: 'Add',
  		grid: this
  	}).show();	
  },
  
  editServer: function()
  {
  	var selection = this.selection;
  	
  	if(selection)
  	{
  		Ext.create('SALP.view.AddEditServersWindow',
  		{
  			mode: 'Edit',
  			record: selection,
  			grid: this
  		}).show();
  	}
  },
  deleteServer: function()
  {
  	var selection = this.selection;
  	
  	if(selection)
  	{
  		Ext.create('SALP.view.DeleteServerDialog',
  		{
  			record: selection,
  			grid: this
  		}).show();
  	}
  }
});