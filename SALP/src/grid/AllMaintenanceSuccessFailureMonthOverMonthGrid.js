﻿//
//
//  All Maintenance Success Failure Month Over Month Grid
//
//

Ext.define('SALP.grid.AllMaintenanceSuccessFailureMonthOverMonthGrid', 
{
	alias: 'widget.grid.AllMaintenanceSuccessFailureMonthOverMonthGrid',
	extend: 'Ext.grid.Panel',
	
	stripeRows: true,
  columnLines: true, 
  stateful: false,
	scrollable: true,

	emptyText: 'No Records Found.', 
	
	selModel: 
	{
    type: 'spreadsheet',
    columnSelect: true  
	},

	plugins: [
	{
  	ptype: 'gridexporter'
  },
  {
  	ptype: 'clipboard'
  }],
	
	initComponent: function()
  {
  	this.header = this.buildHeader();
  	
		this.store = this.buildStore();
		
		this.columns = this.buildColumns();
		
		this.listeners = this.buildListeners();
		
    this.callParent(arguments);
  },
  
  buildHeader: function()
  {
  	return   {
    	itemPosition: 1, // after title before collapse tool
    	items: [
    	{
    		xtype: 'container',
    		html: 'Filter Results - '
    	}, 
    	{
				xtype: 'combobox',
				store: SALP.data.AllMaintenanceSuccessFailure,
				queryMode: 'local',
				displayField: 'displayName',
				valueField: 'submitValue',
    		listeners : 
    		{
    			select : function(comboBox, dataModel, object) 
    			{
						var t=1;
      		},
      		scope: this
      	}
    	},
    	{
    		xtype: 'container',
    		html: '- By - '
    	}, 
    	{
    		xtype: 'textfield',
    		width: 100,
    		listeners:  
    		{
        	specialkey: function (object, event) 
        	{    
          	if (event.getKey() == event.ENTER) 
          	{
            	this.getFiltered();
            }
          },
          scope: this
        }
    	},
    	{
    		xtype: 'button',
    		text: 'Apply',
       	listeners: 
  			{
    			click: function() 
     			{
						this.getFiltered();
      		},
      		scope: this
  			}
  		},
  		{
    		xtype: 'button',
    		text: 'Clear',
       	listeners: 
  			{
    			click: function() 
     			{
     				this.getHeader().items.items[2].setValue('');
  					this.getHeader().items.items[4].setValue('');
  					
  					this.getStore().clearFilter();
      		},
      		scope: this
  			}
  		},
  		{
  			xtype: 'container',
  			width: 50
  		}, 
    	{
        xtype: 'button',
        text: 'Download',
       	listeners: 
				{
    			click: function() 
     			{
						this.saveDocumentAs(
						{
    					type: 'xlsx',
    					title: 'All Maintenance Success Failure Month Over Month',
    					fileName: 'AllMaintenance.xlsx'
						});
      		},
      		scope: this
				}
    	}]
  	}
  },
  
  buildStore: function()
  {
  	return Ext.create('Ext.data.Store', 
    {
    	autoLoad: true,
    	model: 'MaintenanceResolveStatus',
    	proxy: 
    	{
      	type: 'ajax',
       	url: 'WebServices/Service.svc/GetMaintanceCausedSuccessFailByMonth',
       	extraParams:
       	{
    	  	month: this.month,
    	  	year: this.year
       	},
       	pageParam: undefined,
    		startParam: undefined,
    		limitParam: undefined,
       	reader: 
       	{
        	type: 'json',
          root: 'data'
       	}
    	}
    });
  },
  
    
  buildColumns: function()
  {
  	return [
  	{
  		text: 'Month',
      sortable : true,
      dataIndex: 'month',
      width: 50
  	},
  	{
  		text: 'Year',
      sortable : true,
      dataIndex: 'year',
      width: 50
  	},
  	{
  		text: 'Comcast Division',
      sortable : true,
      dataIndex: 'comcastDivision',
      width: 150
  	},
  	{
  		text: 'VP Department',
      sortable : true,
      dataIndex: 'vpDepartment',
      width: 150
  	},
  	{
  		text: 'VP Name',
      sortable : true,
      dataIndex: 'vpName',
      width: 150
  	},
  	{
  		text: 'Dir Group',
      sortable : true,
      dataIndex: 'dirGroup',
      width: 150
  	},
  	{
  		text: 'Dir Name',
      sortable : true,
      dataIndex: 'dirName',
      width: 150
  	},
  	{
  		text: 'Fail Count',
  		tooltip: 'Fail Count',
      sortable : true,
      dataIndex: 'failCount',
      width: 50
  	},
  	{
  		text: 'Fail %',
  		tooltip: 'Fail %',
      sortable : true,
      dataIndex: 'failPct',
      width: 50,
      renderer: function(value, metaData, record, rowIndex, colIndex, store, view)
      {      	
      	var returnValue;
      	
      	if(value)
      	{
      		return value + "%";
      	}
      	else
    		{
    			return value
    		}
      }
  	},
  	{
  		text: 'Caused Incident Count',
  		tooltip: 'Caused Incident Count',
      sortable : true,
      dataIndex: 'causedIncidentCount',
      width: 50
  	},
  	{
  		text: 'Incident %',
  		tooltip: 'Incident %',
      sortable : true,
      dataIndex: 'incidentPct',
      width: 50,
      renderer: function(value, metaData, record, rowIndex, colIndex, store, view)
      {      	
      	var returnValue;
      	
      	if(value)
      	{
      		return value + "%";
      	}
      	else
    		{
    			return value
    		}
      }
  	},
  	{
  		text: 'Incident Count',
  		tooltip: 'Incident Count',
      sortable : true,
      dataIndex: 'incidentCount',
      width: 50
  	},
  	{
  		text: 'Cause Sev1 Count',
  		tooltip: 'Cause Sev1 Count',
      sortable : true,
      dataIndex: 'sev1Count',
      width: 50,
      renderer: function(value, metaData, record, rowIndex, colIndex, store, view)
      {
      	metaData.tdAttr = 'data-qtip="Double-Click to see Sev1 Caused By Incident"';
      	
      	return value;
      }
  	},
  	{
  		text: 'Sev1 %',
  		tooltip: 'Sev1 %',
      sortable : true,
      dataIndex: 'sev1Pct',
      width: 50,
      renderer: function(value, metaData, record, rowIndex, colIndex, store, view)
      {      	
      	var returnValue;
      	
      	metaData.tdAttr = 'data-qtip="Double-Click to see Sev1 Caused By Incident"';
      	
      	if(value)
      	{
      		return value + "%";
      	}
      	else
    		{
    			return value
    		}
      }
  	},
  	{
  		text: 'Sev1 Distinct Count',
  		tooltip: 'Sev1 Distinct Count',
      sortable : true,
      dataIndex: 'sev1TicketCount',
      width: 50,
      renderer: function(value, metaData, record, rowIndex, colIndex, store, view)
      {
      	metaData.tdAttr = 'data-qtip="Double-Click to see Sev1 Caused By Incident"';
      	
      	return value;
      }
  	},
  	{
  		text: 'Success Count',
  		tooltip: 'Success Count',
      sortable : true,
      dataIndex: 'successCount',
      width: 50
  	},
  	{
  		text: 'Success %',
  		tooltip: 'Success %',
      sortable : true,
      dataIndex: 'successPct',
      width: 50,
      renderer: function(value, metaData, record, rowIndex, colIndex, store, view)
      {      	
      	var returnValue;
      	
      	if(value)
      	{
      		return value + "%";
      	}
      	else
    		{
    			return value
    		}
      }
  	},
  	{
  		text: 'Total Count',
  		tooltip: 'Total Count',
      sortable : true,
      dataIndex: 'totalCount',
      width: 50
  	}]
  },
  
  buildListeners: function()
  {
  	return {
//  		'afterrender': function(grid, options)
//  		{
//  			this.view.getEl().on('scroll', function(event, target)
//  			{ 
//  				if(target.scrollTop > 1)
//  				{
//        		this.setScrollX(0);
//        	}
//        }, this);
//
//  		},
     	'celldblclick': function(view, td, columnIndex, record, tr, rowIndex, event) 
     	{
     		if(columnIndex == 3)
     		{
     			var comcastDivision = record.data.comcastDivision,
     			   	columnSelect = this.getHeader().items.items[2],
   						filterText =  this.getHeader().items.items[4];
   				
   				columnSelect.setValue('comcastDivision');
   				filterText.setValue(comcastDivision);
   				
   				this.getFiltered();    			
     		}
     		
     		if(columnIndex == 4)
     		{
     			var vpDepartment = record.data.vpDepartment,
     			   	columnSelect = this.getHeader().items.items[2],
   						filterText =  this.getHeader().items.items[4];
   				
   				columnSelect.setValue('vpDepartment');
   				filterText.setValue(vpDepartment);
   				
   				this.getFiltered();    			
     		}
     		
   			if(columnIndex == 5)
   			{
   				var vpName = record.data.vpName, 
   						columnSelect = this.getHeader().items.items[2],
   						filterText =  this.getHeader().items.items[4];
   				
   				columnSelect.setValue('vpName');
   				filterText.setValue(vpName);
   				
   				this.getFiltered();
   			}
   			
   			if(columnIndex == 6)
   			{
   				var dirGroup = record.data.dirGroup, 
   						columnSelect = this.getHeader().items.items[2],
   						filterText =  this.getHeader().items.items[4];
   				
   				columnSelect.setValue('dirGroup');
   				filterText.setValue(dirGroup);
   				
   				this.getFiltered();
   			}
   			
   			if(columnIndex == 7)
   			{
   				var dirName = record.data.dirName, 
   						columnSelect = this.getHeader().items.items[2],
   						filterText =  this.getHeader().items.items[4];
   				
   				columnSelect.setValue('dirName');
   				filterText.setValue(dirName);
   				
   				this.getFiltered();
   			}
   			
   			
     		if((columnIndex == 8) || (columnIndex == 9))
 				{
					Ext.create('SALP.view.Sev1CausedByMaintDetailsWindow',
				 	{
				 		directorName: record.data.dirName,
				 		month: record.data.month,
				 		year: record.data.year,
				 		reportLevel: 'fail'
				 	}).show();
     		}
     		
     		if((columnIndex == 10) || (columnIndex == 11) || (columnIndex == 12))
 				{
					Ext.create('SALP.view.Sev1CausedByMaintDetailsWindow',
				 	{
				 		directorName: record.data.dirName,
				 		month: record.data.month,
				 		year: record.data.year,
				 		reportLevel: 'Incidents'
				 	}).show();
     		}
     		
     		if((columnIndex == 13) || (columnIndex == 14) || (columnIndex == 15))
 				{
					Ext.create('SALP.view.Sev1CausedByMaintDetailsWindow',
				 	{
				 		directorName: record.data.dirName,
				 		month: record.data.month,
				 		year: record.data.year,
				 		reportLevel: 'Sev1'
				 	}).show();
     		}
     		
     		if((columnIndex == 16) || (columnIndex == 17))
 				{
					Ext.create('SALP.view.Sev1CausedByMaintDetailsWindow',
				 	{
				 		directorName: record.data.dirName,
				 		month: record.data.month,
				 		year: record.data.year,
				 		reportLevel: 'Success'
				 	}).show();
     		}
     		
     		if(columnIndex == 18)
 				{
					Ext.create('SALP.view.Sev1CausedByMaintDetailsWindow',
				 	{
				 		directorName: record.data.dirName,
				 		month: record.data.month,
				 		year: record.data.year,
				 		reportLevel: 'All'
				 	}).show();
     		}
     	},
      scope: this
    }
  },
  
  getFiltered: function()
  {
		var selectedColumn = this.getHeader().items.items[2].getValue(),
		filterText = this.getHeader().items.items[4].getValue();
		
		this.getStore().filter(selectedColumn, filterText);
  },
  
  onScrollStart: function()
  {
  	var t=1;
  }
});