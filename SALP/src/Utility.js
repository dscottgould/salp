﻿//
//
//  Utility.js
//
//


//
// Application Namespace
//
Ext.ns('SALP');


//
//  Utility Functions
//


SALP.getNextAvailible = function()
{
	var next = new Object(),
			test = new Object(),
			hasFound = false,
			countColumns = SALP.countColumns * 2 - 1
			
	next =
	{
		slot: 0,
		column: 0,
		page: 0
	}	
			
	if (!hasFound)
	{
		for(var pageIndex = 0; pageIndex <= SALP.countPages - 1; pageIndex ++)
		{
			if (!hasFound)
			{
				for(var columnIndex = 0; columnIndex <= countColumns; columnIndex ++)
				{
					if (!hasFound)
					{
						for(var slotIndex = 0; slotIndex <= 2; slotIndex ++)
						{
							try
							{
								test = Ext.getCmp('app-tabPanel').items.items[pageIndex].items.items[columnIndex].items.items[slotIndex];
							}
							catch(error)
							{
								break;
							}
							
				
							var t=1;
							
							next.slot ++;
							
							if(next.slot >= 13) next.slot = 1;
				
							if (test == undefined)
							{
								//next.slot = slotIndex;
								next.column = columnIndex + 1;
								next.page = pageIndex + 1;
								hasFound = true
								
								break;
							}
						}
					}
					else
					{
						break;		
					}
				}
			}
			else
			{
				columnIndex = 0;
				slotIndex = 0;
				break;		
			}
		}
	}

	if(!hasFound)
	{
		Ext.Msg.alert('Status', 'All Columns Are Full.');
	}

	return next
}


SALP.getNextAvailibleByPage = function(currentPage)
{
	var next = new Object(),
			test = new Object(),
			hasFound = false,
			countColumns = SALP.countColumns * 2 - 1
			
	next =
	{
		slot: 0,
		column: 0,
		page: 0
	}	
			

	for(var columnIndex = 0; columnIndex <= countColumns; columnIndex ++)
	{
		if (!hasFound)
		{
			for(var slotIndex = 0; slotIndex <= 3; slotIndex ++)
			{
				try
				{
					test = Ext.getCmp('app-tabPanel').items.items[currentPage].items.items[columnIndex].items.items[slotIndex];
				}
				catch(error)
				{
					break;
				}
				
	
				var t=1;
				
				next.slot ++;
				
				if(next.slot >= 13) next.slot = 1;
	
				if (test == undefined)
				{
					next.slot = slotIndex + 1;
					next.column = columnIndex + 1;
					next.page = currentPage + 1;
					hasFound = true
					
					break;
				}
			}
		}
		else
		{
			break;		
		}
	}


	if(!hasFound)
	{
		Ext.Msg.alert('Status', 'All Columns Are Full.');
	}

	return next
}

SALP.getURLParams = function()
{
	var params = window.location.href.split("?")[1],
			returnValue = new String();
	
	if(params)
	{
	    var paramName = params.split("=")[0],
					paramValue = params.split("=")[1];
	}

	if (paramName == 'dashboard')
	{
		returnValue = paramValue;
	}
			
	return returnValue
}


SALP.getCookie = function(dashboardId)
{
	var rawCookie = new Object();

  if(dashboardId.length == 0)
  {
  	Ext.Ajax.request(
  	{
  		url: '/WebServices/Service.svc/GetUserCookie',
  		method: 'GET',
  		params:  
    	{ 
      	  accountName: SALP.user.accountName
    	}, 
  		success: function(response, opts) 
  		{
  			var returnData = Ext.decode(response.responseText);
  			
  			if(returnData.data.cookieValue.length > 0) rawCookie = Ext.decode(returnData.data.cookieValue);
  			
  			rawCookie.dashboardId = returnData.data.dashboardId;
				rawCookie.dashboardName = returnData.data.dashboardName;

				SALP.rawCookie = rawCookie;
			
				SALP.loadSlots();
  		},
  		failure: function(response, opts) 
  		{
  			var t=1;
  		}
  	});
  }
  else
  {
  	Ext.Ajax.request(
  	{
  		url: '/WebServices/Service.svc/GetDashboardByID',
  		method: 'GET',
  		params:  
    	{ 
      	  dashboardId: dashboardId
    	}, 
  		success: function(response, opts) 
  		{
				rawCookie = Ext.decode(response.responseText).data;
			
				if(rawCookie)	
				{
					SALP.rawCookie = Ext.decode(rawCookie);
					SALP.dashboardId = dashboardId;
				}
				else
				{
					SALP.rawCookie = "notFound";
				}

				SALP.loadSlots();
  		},
  		failure: function(response, opts) 
  		{
  			var t=1;
  		}	
  	});
  }

	return rawCookie
}


SALP.clearCurrentTab = function()
{
	var activeTab = Ext.getCmp('app-tabPanel').getActiveTab(),
		  activeTabIndex;
		  
	if(activeTab) 
	{
		activeTabIndex = Ext.getCmp('app-tabPanel').items.findIndex('id', activeTab.id);
	}
	else
	{
		activeTabIndex = 0;
	}
	

	Ext.getCmp('app-tabPanel').items.items[activeTabIndex].items.items[0].remove(1);
	Ext.getCmp('app-tabPanel').items.items[activeTabIndex].items.items[0].remove(1);
	Ext.getCmp('app-tabPanel').items.items[activeTabIndex].items.items[0].remove(1);
		
	Ext.getCmp('app-tabPanel').items.items[activeTabIndex].items.items[2].remove(1);
	Ext.getCmp('app-tabPanel').items.items[activeTabIndex].items.items[2].remove(1);
	Ext.getCmp('app-tabPanel').items.items[activeTabIndex].items.items[2].remove(1);	

	Ext.getCmp('app-tabPanel').items.items[activeTabIndex].items.items[4].remove(1);
	Ext.getCmp('app-tabPanel').items.items[activeTabIndex].items.items[4].remove(1);
	Ext.getCmp('app-tabPanel').items.items[activeTabIndex].items.items[4].remove(1);	
	
	Ext.getCmp('app-tabPanel').items.items[activeTabIndex].items.items[6].remove(1);
	Ext.getCmp('app-tabPanel').items.items[activeTabIndex].items.items[6].remove(1);
	Ext.getCmp('app-tabPanel').items.items[activeTabIndex].items.items[6].remove(1);

}

SALP.clearLayout = function()
{
	Ext.getCmp('app-tabPanel').removeAll();
	
	SALP.rawCookie.pages = new Array();
	SALP.rawCookie.slots = new Array();
	
	SALP.rawCookie.countPages = 1;
	SALP.rawCookie.countPortlets = 0;
}



SALP.loadSlots = function()
{
	var rawCookie = SALP.rawCookie,
			dashboardName = '<center>' + rawCookie.dashboardName + '</center>'
	
	
	// Check For Cookie
	if((rawCookie == undefined) || (rawCookie.pages == undefined))
	{
		
		// Create New Cookie
		rawCookie = SALP.createCookie();

		// Setup Portlet Defaults
		rawCookie = SALP.setDefaults(rawCookie);
  		
  	var page = 
  	{
  		pageIndex: 0,
  		title: 'Page 01',
  		columnWidths: [0.25, 0.25, 0.25, 0.25]
  	};
  	
  	rawCookie.pages.push(page);
	}
	  	
  Ext.getCmp('idPageSelect').getStore().removeAll();
	
	SALP.rawCookie = rawCookie;
	
	// Set Dashboard Title
	Ext.getCmp('dashboard-title').setHtml(rawCookie.dashboardName);
	
	// Load Pages
	if(rawCookie != 'notFound')
	{
  	for (var index =0;index <= rawCookie.pages.length - 1; index ++)
  	{
  		var currentPage = rawCookie.pages[index];
  		
				
  			SALP.addPage(currentPage);
  			SALP.countPages ++;

  	}
  	
  	// Set Column Widths
  	for(var pageIndex = 0; pageIndex <= SALP.rawCookie.pages.length - 1; pageIndex ++)
  	{
  		var columnAdjustment = parseFloat(rawCookie.pages[pageIndex].columnWidths[0]) + parseFloat(rawCookie.pages[pageIndex].columnWidths[1]) + parseFloat(rawCookie.pages[pageIndex].columnWidths[2]) + parseFloat(rawCookie.pages[pageIndex].columnWidths[3]) - 1
  		
  		if(Ext.getCmp('app-tabPanel').items.items[pageIndex].items.items.length != 4)
  		{
  			Ext.getCmp('app-tabPanel').items.items[pageIndex].items.items[0].columnWidth = parseFloat(rawCookie.pages[pageIndex].columnWidths[0]);
  			Ext.getCmp('app-tabPanel').items.items[pageIndex].items.items[2].columnWidth = parseFloat(rawCookie.pages[pageIndex].columnWidths[1]);
  			Ext.getCmp('app-tabPanel').items.items[pageIndex].items.items[4].columnWidth = parseFloat(rawCookie.pages[pageIndex].columnWidths[2]);
  			Ext.getCmp('app-tabPanel').items.items[pageIndex].items.items[6].columnWidth = parseFloat(rawCookie.pages[pageIndex].columnWidths[3]) - columnAdjustment;
  		}
  		else
  		{
  			Ext.getCmp('app-tabPanel').items.items[pageIndex].items.items[0].columnWidth = parseFloat(rawCookie.pages[pageIndex].columnWidths[0]);
  			Ext.getCmp('app-tabPanel').items.items[pageIndex].items.items[1].columnWidth = parseFloat(rawCookie.pages[pageIndex].columnWidths[1]);
  			Ext.getCmp('app-tabPanel').items.items[pageIndex].items.items[2].columnWidth = parseFloat(rawCookie.pages[pageIndex].columnWidths[2]);
  			Ext.getCmp('app-tabPanel').items.items[pageIndex].items.items[3].columnWidth = parseFloat(rawCookie.pages[pageIndex].columnWidths[3]) - columnAdjustment;
  		}
  	}


  	
  	// Load Slots
  	for (var index = 0; index <= rawCookie.slots.length - 1; index ++)
  	{
  		var currentSlot = rawCookie.slots[index],
  				portlet;
  				
  		// Set Dimensions
  		currentSlot.height = rawCookie.slots[index].height;
  		currentSlot.width = rawCookie.slots[index].width;
  		
  		// Set Filters
  		currentSlot.username = rawCookie.slots[index].username;
  		currentSlot.password = rawCookie.slots[index].password;
  		currentSlot.segment = rawCookie.slots[index].segment;
  		currentSlot.filter = rawCookie.slots[index].filter;
  		currentSlot.breakdown = rawCookie.slots[index].breakdown;
  		currentSlot.showOthers = rawCookie.slots[index].showOthers;
  		
  		portlet = SALP.addPorlet(currentSlot);
  		
  		if((rawCookie.slots[index].class.indexOf('Haystack') > 0)  || (rawCookie.slots[index].class.indexOf('HaystackBreakdown') > 0))
  		{
				var username = rawCookie.slots[index].username,
						password = rawCookie.slots[index].password,
						segment = rawCookie.slots[index].segment,
						filter = rawCookie.slots[index].filter,
						breakdown = rawCookie.slots[index].breakdown,
						showOthers = rawCookie.slots[index].showOthers,
						title;
  				
  			switch(rawCookie.slots[index].class)
 				{
 					case 'SALP.portlets.HaystackCallsPortlet':
 						title = 'Haystack Calls- '
 						break;
 				
 					case 'SALP.portlets.HaystackTicketsPortlet':
 						title = 'Haystack Tickets- '
 						break;
 			
 					case 'SALP.portlets.HaystackTruckRollsPortlet':
 						title = 'Haystack Truck Rolls- '
 						break;
 						
 					case 'SALP.portlets.HaystackBreakdownCallsPortlet':
 						title = 'Haystack Breakdown - Calls';
 						break;
 					
 					case 'SALP.portlets.HaystackBreakdownTicketsPortlet':
 						title = 'Haystack Breakdown - Tickets';
 						break;
 						
 					case 'SALP.portlets.HaystackBreakdownTruckRollsPortlet':
 						title = 'Haystack Breakdown - Truck Rolls';
 						break;
 				}
  				
  			if(rawCookie.slots[index].class.indexOf('HaystackBreakdown') > 0)	
  			{
					var chart = portlet.items.items[0].items.items[0],
							toolbar = portlet.items.items[0].dockedItems.items[0]
					
					chart.filters.breakdown = breakdown;
					chart.filters.segment = segment;
					chart.filters.filter = filter;
					chart.filters.username = username;
					chart.filters.password = password;
					chart.filters.showOthers = showOthers;
					
					toolbar.items.items[1].setValue(showOthers);
					toolbar.items.items[3].setValue(breakdown);
					
					portlet.items.items[0].loadStore();
					
//					Ext.defer(function() 
//		      {
//	    	  	portlet.items.items[0].loadStore();
//	        }, 5000);
  			}
  			else
  			{
    			portlet.items.first().store.load
        	({
        		params:
        		{
        			username: username,
        			password: password,
        			segment: segment,
        			filter: filter
        		}
        	});
  			}	
      	
      	if ((segment) || (filter))
        {
        	if (segment) title += ' - ' + rawCookie.slots[index].segment;
        	if (filter) title +=  ' : ' + rawCookie.slots[index].filter;
        	
        	portlet.setTitle(title);
       	}
  		}
  	}
  }
}

SALP.addPorlet = function(currentSlot)
{
	if(currentSlot.class.indexOf('Haystack') > 0)
	{
		var portlet = new Ext.create('Ext.dashboard.Panel',
    {
    	tools: SALP.getFilterTools(currentSlot),
    	//header: SALP.buildToolHeader(),
    	items: new Ext.create(currentSlot.class),
    	height: currentSlot.height,
    	width: currentSlot.width,
    	props: 
    	{
    		currentSlot: currentSlot.slot,
    		username: currentSlot.username,
    		password: currentSlot.password,
    		segment: currentSlot.segment,
    		filter: currentSlot.filter,
    		breakdown: currentSlot.breakdown,
    		showOthers: currentSlot.showOthers
    	},
    	listeners: 
    	{
    		'close': Ext.bind(SALP.onPortletClose, this)
    	}
    });  
    
    portlet.setTitle(portlet.items.first().props.title);
	}
	else if (currentSlot.class == 'SALP.portlets.OpenActiveIncidentsPortlet')
	{
		var portlet = Ext.create('Ext.dashboard.Panel',
    {
    	tools: SALP.getOpenIncidentsTools(currentSlot),
    	//header: SALP.buildToolHeader(),
    	items: Ext.create(currentSlot.class),
    	height: currentSlot.height,
    	width: currentSlot.width,
    	props: 
    	{
    		currentSlot: currentSlot.slot,
    		username: currentSlot.username,
    		password: currentSlot.password,
    		segment: currentSlot.segment,
    		filter: currentSlot.filter
    	},
    	listeners: 
    	{
    		'close': Ext.bind(SALP.onPortletClose, this)
    	}
    });  
    
    portlet.setTitle(portlet.items.first().props.title);
	}
	else if (currentSlot.class == 'SALP.portlets.CMTreeViewChartPortlet')
	{
		var portlet = Ext.create('Ext.dashboard.Panel',
    {
    	tools: SALP.getD3Tools(currentSlot),
    	//header: SALP.buildToolHeader(),
    	items: Ext.create(currentSlot.class),
    	height: currentSlot.height,
    	width: currentSlot.width,
    	props: 
    	{
    		currentSlot: currentSlot.slot,
    		username: currentSlot.username,
    		password: currentSlot.password,
    		segment: currentSlot.segment,
    		filter: currentSlot.filter
    	},
    	listeners: 
    	{
    		'close': Ext.bind(SALP.onPortletClose, this)
    	}
    });  
    
    portlet.setTitle(portlet.items.first().props.title);
	}
	else if((currentSlot.class == 'SALP.portlets.CurrentServerProcessesPortlet') || (currentSlot.class == 'SALP.portlets.LongRunningJobsPortlet') || (currentSlot.class == 'SALP.portlets.ServerCPUUsagePortlet'))
	{
		if(currentSlot.server)
		{
  		var portlet = Ext.create('Ext.dashboard.Panel',
      {
      	tools: SALP.getServerTools(),
      	items: Ext.create(currentSlot.class,
      	{
      		props:
      		{
      			server: currentSlot.server
      		}
      	}),
      	props: 
    		{
    			currentSlot: currentSlot.slot,
    			username: currentSlot.username,
    			password: currentSlot.password,
    			segment: currentSlot.segment,
    			filter: currentSlot.filter
    		},
      	height: currentSlot.height,
      	width: currentSlot.width,
      	listeners: 
      	{
      		'close': Ext.bind(SALP.onPortletClose, this)
      	}
      });
      
      if (currentSlot.class == 'SALP.portlets.CurrentServerProcessesPortlet') 
      {
      	portlet.setTitle('Server Current Processes - ' + currentSlot.server);
      }
      else if(currentSlot.class == 'SALP.portlets.LongRunningJobsPortlet')
     	{
     		portlet.setTitle('Long Running Jobs - ' + currentSlot.server);
     	}
     	else if(currentSlot.class == 'SALP.portlets.ServerCPUUsagePortlet')
     	{
     			portlet.setTitle('Server CPU Usage - ' + currentSlot.server);
     	}
     	
		}
		else
		{
			var portlet = Ext.create('Ext.dashboard.Panel',
      {
      	tools: SALP.getServerTools(),
      	items: Ext.create(currentSlot.class),
      	height: currentSlot.height,
      	width: currentSlot.width,
      	props: 
    		{
    			currentSlot: currentSlot.slot,
    			username: currentSlot.username,
    			password: currentSlot.password,
    			segment: currentSlot.segment,
    			filter: currentSlot.filter
    		},
      	listeners: 
      	{
      		'close': Ext.bind(SALP.onPortletClose, this)
      	}
      });		
      
      portlet.setTitle(portlet.items.items[0].props.title);
		}
	}
	else
	{
  	var portlet = Ext.create('Ext.dashboard.Panel',
    {
    	tools: SALP.getTools(),
    	items: Ext.create(currentSlot.class),
    	height: currentSlot.height,
    	width: currentSlot.width,
    	props: 
    	{
    		currentSlot: currentSlot.slot
    	},
    	listeners: 
    	{
    		'close': Ext.bind(SALP.onPortletClose, this)
    	}
    });
    
    portlet.setTitle(portlet.items.first().props.title);
	}

	portlet.setHeight(currentSlot.height);
  portlet.setWidth(currentSlot.width);
	
	portlet.items.first().props.class = currentSlot.class;
	portlet.items.first().props.slot = currentSlot.slot;
	portlet.items.first().props.column = currentSlot.column;
	portlet.items.first().props.page = currentSlot.page;
//	portlet.items.first().props.username = currentSlot.username;
//	portlet.items.first().props.password = currentSlot.password;
//	portlet.items.first().props.segment = currentSlot.segment;
//	portlet.items.first().props.filter = currentSlot.filter;
	
	
	
	try
	{
		Ext.getCmp('app-tabPanel').items.items[currentSlot.page - 1].items.items[currentSlot.column - 1].add(portlet);
		
		SALP.countPortlets ++;
	}
	catch(error)
	{
		// Do Nothing
	}
	
	return portlet
}

SALP.onPortletClose = function()
{
	var t=1;
}



SALP.saveSlots = function(dashboardName)
{
	if(dashboardName == undefined)
	{
		dashboardName = 'Default'
	}
	

	rawCookie = SALP.createCookie();
	
	
	// Save Pages
	for(var pageIndex = 0; pageIndex <= Ext.getCmp('app-tabPanel').items.items.length - 1; pageIndex ++)
	{
		if (Ext.getCmp('app-tabPanel').items.items[pageIndex])
		{
			var pageItem = Ext.getCmp('app-tabPanel').items.items[pageIndex];
			
			var page = 
			{
				pageIndex: pageItem.pageIndex,
				title: pageItem.title,
				columnWidths: [0.25, 0.25, 0.25, 0.25]
			};
		
			rawCookie.pages.push(page);
		}
	}
	
	// Save Column Widths
	for(var pageIndex = 0; pageIndex <= Ext.getCmp('app-tabPanel').items.items.length - 1; pageIndex ++)
	{
		rawCookie.pages[pageIndex].columnWidths = new Array();
		
		if(Ext.getCmp('app-tabPanel').items.items[pageIndex].items.length == 7)
		{
			for (var columnIndex = 0; columnIndex < 7; columnIndex += 2)
			{
				if(Ext.getCmp('app-tabPanel').items.items[pageIndex].items.items[columnIndex].columnWidth)
				{
					var currentWidth = Ext.getCmp('app-tabPanel').items.items[pageIndex].items.items[columnIndex].columnWidth.toFixed(2);
		
					if(currentWidth != undefined)
					{
						rawCookie.pages[pageIndex].columnWidths.push(currentWidth);
					}
				}
			}
		}
		else
		{
			for (var columnIndex = 0; columnIndex < 4; columnIndex ++)
			{
				if(Ext.getCmp('app-tabPanel').items.items[pageIndex].items.items[columnIndex].columnWidth)
				{
					var currentWidth = Ext.getCmp('app-tabPanel').items.items[pageIndex].items.items[columnIndex].columnWidth.toFixed(2);
		
					if(currentWidth != undefined)
					{
						rawCookie.pages[pageIndex].columnWidths.push(currentWidth);
					}
				}	
			}
		}				
						

	}
	
	// Save Slots
	for(var pageIndex = 0; pageIndex <= Ext.getCmp('app-tabPanel').items.items.length - 1; pageIndex ++)
	{
		if (pageIndex == 0)
		{
			for(var columnIndex = 0; columnIndex <= 7; columnIndex += 2)
			{
				for(var slotIndex = 0; slotIndex <= 3; slotIndex ++)
				{
					try
					{
						var portlet = Ext.getCmp('app-tabPanel').items.items[pageIndex].items.items[columnIndex].items.items[slotIndex].items.first(),
								props = Ext.getCmp('app-tabPanel').items.items[pageIndex].items.items[columnIndex].items.items[slotIndex].props
				
						var portletItem = 
						{
							slot: slotIndex + 1,
							column: columnIndex + 1,
							page: pageIndex + 1,
							class: portlet.$className,
							segment: props.segment,
							filter: props.filter,
							username: props.username,
							password: props.password,
							width: portlet.up().getWidth(),
							height: portlet.up().getHeight(),
							title: portlet.ownerCt.getTitle()
						}
					
						if(portlet.props.server)
						{
							portletItem.server = portlet.props.server.trim();
						}
						
						switch(portlet.$className)
						{
							case 'SALP.portlets.HaystackBreakdownCallsPortlet':
							case 'SALP.portlets.HaystackBreakdownTicketsPortlet':
							case 'SALP.portlets.HaystackBreakdownTruckRollsPortlet':
								portletItem.showOthers = portlet.dockedItems.items[0].items.items[1].getValue()
								portletItem.breakdown = portlet.dockedItems.items[0].items.items[3].getValue()
							break;
						}
					
						rawCookie.slots.push(portletItem);
					}
					catch(error)
					{
						// Do Nothing
					}
				}
			}
		}
		else 
		{
			var columnCount = 1;
			
			for(var columnIndex = 0; columnIndex <= 7; columnIndex += 2)
			{	
				for(var slotIndex = 0; slotIndex <= 3; slotIndex ++)
				{
					try
					{
						var portlet = Ext.getCmp('app-tabPanel').items.items[pageIndex].items.items[columnIndex].items.items[slotIndex].items.first(),
								props = Ext.getCmp('app-tabPanel').items.items[pageIndex].items.items[columnIndex].items.items[slotIndex].props
				
						var portletItem = 
						{
							slot: slotIndex + 1,
							column: columnCount,
							page: pageIndex + 1,
							class: portlet.$className,
							segment: props.segment,
							filter: props.filter,
							username: props.username,
							password: props.password,
							width: portlet.up().getWidth(),
							height: portlet.up().getHeight(),
							title: portlet.ownerCt.getTitle(),
							server: portlet.items.items[0].server
						}
						
						switch(portlet.$className)
						{
							case 'SALP.portlets.HaystackBreakdownCallsPortlet':
							case 'SALP.portlets.HaystackBreakdownTicketsPortlet':
							case 'SALP.portlets.HaystackBreakdownTruckRollsPortlet':
								var filters = Ext.getCmp('app-tabPanel').items.items[pageIndex].items.items[columnIndex].items.items[slotIndex].items.items[0].items.items[0].filters
							
								portletItem.segment = filters.segment;
								//portletItem.filter = filters.filter;
								//portletItem.username = filters.username;
								//portletItem.password = filters.password;
								portletItem.showOthers = portlet.dockedItems.items[0].items.items[1].getValue();
								portletItem.breakdown = portlet.dockedItems.items[0].items.items[3].getValue();
							break;
						}
					
					
						rawCookie.slots.push(portletItem);
					}
					catch(error)
					{
						// Do Nothing
					}
				}	
				
				columnCount ++;
			}
		}
		
		columnIndex = 0;
		slotIndex = 0;
	}
	
	//rawCookie.countPortlets = rawCookie.slots.length;
	rawCookie.countPages = rawCookie.pages.length;
	
	rawCookie.dashboardName = dashboardName;
	
	SALP.rawCookie = rawCookie;
	
	SALP.saveCookie(Ext.encode(rawCookie));
}


SALP.saveCookie = function(cookie)
{
	  var dashboardName = Ext.decode(cookie).dashboardName;
	  
	  Ext.Ajax.request(
    {
    	url: '/WebServices/Service.svc/SaveUserCookie',
    	method: 'GET',
    	params:  
      { 
          accountName: SALP.user.accountName,
          dashboardName: dashboardName,
          cookie: cookie
      }, 
    	success: function(response, opts) 
    	{
				SALP.dashboardId = Ext.decode(response.responseText).data;
				
				Ext.getCmp('DashboardList').getStore().reload();
    	},
    	failure: function(response, opts) 
    	{
    		var t=1;
    	}
    });
}


SALP.setDefaults = function(rawCookie) 
{
	var portlet = 
	{
		slot: 1,
		column: 1,
		page: 1,
		class: 'SALP.portlets.HaystackCallsPortlet',
		segment: new String(),
		filter: new String(),
		username: new String(),
		password: new String
	}
	
	rawCookie.slots.push(portlet);
	
	var portlet = 
	{
		slot: 2,
		column: 1,
		page: 1,
		class: 'SALP.portlets.CoreConfigRolling12MonthPortlet',
		segment: new String(),
		filter: new String(),
		username: new String(),
		password: new String
	}
	
	rawCookie.slots.push(portlet);
	
	var portlet = 
	{
		slot: 3,
		column: 1,
		page: 1,
		class: 'SALP.portlets.DailyCentra3941tPortlet',
		segment: new String(),
		filter: new String(),
		username: new String(),
		password: new String
	}
	
	rawCookie.slots.push(portlet);
	
	var portlet = 
	{
		slot: 4,
		column: 3,
		page: 1,
		class: 'SALP.portlets.HSDTicketsPortlet',
		segment: new String(),
		filter: new String(),
		username: new String(),
		password: new String
	}
	
	rawCookie.slots.push(portlet);
	
	var portlet = 
	{
		slot: 5,
		column: 3,
		page: 1,
		class: 'SALP.portlets.SubscriberCountsPortlet',
		segment: new String(),
		filter: new String(),
		username: new String(),
		password: new String
	}
	
	rawCookie.slots.push(portlet);
	
	var portlet = 
	{
		slot: 6,
		column: 3,
		page: 1,
		class: 'SALP.portlets.OBIPortlet',
		segment: new String(),
		filter: new String(),
		username: new String(),
		password: new String
	}
	
	rawCookie.slots.push(portlet);
	
	var portlet = 
	{
		slot: 7,
		column: 5,
		page: 1,
		class: 'SALP.portlets.BrouhaPortlet',
		segment: new String(),
		filter: new String(),
		username: new String(),
		password: new String
	}
	
	rawCookie.slots.push(portlet);
	
	var portlet = 
	{
		slot: 8,
		column: 5,
		page: 1,
		class: 'SALP.portlets.SMNPortlet',
		segment: new String(),
		filter: new String(),
		username: new String(),
		password: new String
	}
	
	rawCookie.slots.push(portlet);
	
	var portlet = 
	{
		slot: 9,
		column: 5,
		page: 1,
		class: 'SALP.portlets.CSSNPortlet',
		segment: new String(),
		filter: new String(),
		username: new String(),
		password: new String
	}
	
	rawCookie.slots.push(portlet);
	
	var portlet = 
	{
		slot: 10,
		column: 7,
		page: 1,
		class: 'SALP.portlets.FCCFinalReports12MonthsPortlet',
		segment: new String(),
		filter: new String(),
		username: new String(),
		password: new String
	}
	
	rawCookie.slots.push(portlet);
	
	var portlet = 
	{
		slot: 11,
		column: 7,
		page: 1,
		class: 'SALP.portlets.FCCDailyVolumePortlet',
		segment: new String(),
		filter: new String(),
		username: new String(),
		password: new String
	}
	
	rawCookie.slots.push(portlet);
	

	return rawCookie
}


SALP.createCookie = function()
{
	rawCookie = new Object();
	
	rawCookie =
	{
		dashboardId: new String(),
		dashboardName: 'Default',
		countPages: new Number(),
		countColumns: new Number(),
		countPorlets: new Number(),
		slots: new Array(),
		pages: new Array()
	}
	
	return rawCookie
}



SALP.openSite = function(site)
{	
	var tab = window.open(site, '_blank')
	
	if(tab) tab.focus();
}

SALP.addChart = function(selectedChart)
{
	if(Ext.getCmp(selectedChart).getValue())
	{
  	if (Ext.getCmp('app-tabPanel').items.items.length == 0)
  	{
  		Ext.Msg.alert('Status', 'Add Tab Panel before Adding Portlet.');
  	}
  	else
  	{
  		var activeTab = Ext.getCmp('app-tabPanel').getActiveTab();
  		
  		if (activeTab)
  		{
  			var activeTabIndex = Ext.getCmp('app-tabPanel').items.findIndex('id', activeTab.id);
  		}
  		else
  		{
  			var activeTabIndex = 0;
  		}
  		
  		var chartSelection = Ext.getCmp(selectedChart).getValue(),
  			  nextAvailible = SALP.getNextAvailibleByPage(activeTabIndex),
  				portlet =
  				{
  					slot: nextAvailible.slot,
  					column: nextAvailible.column,
  					page: nextAvailible.page,
  					class: chartSelection
  				};
  				
  				
			SALP.addPorlet(portlet);	
  	}
  }
}

SALP.addPage = function(page)
{
	var tabPanel = Ext.getCmp('app-tabPanel'),
			newRecord = 
			{
				id: page.pageIndex,
				displayName: page.title,
				value: page.pageIndex
			}
	
	// Force Dashboard Creation
	var newTab = Ext.create('Ext.dashboard.Dashboard',
      {
      	title: page.title,
      	pageIndex: page.pageIndex,
      	parts:
  			{
  				default:
  				{
  					viewTemplate:
  					{
  						// Empty Container
  						xtype: 'container'
  					}
  				}
  			},
    		defaultContent: [
        {
        	type: 'default',
          columnIndex: 0
        }, 
        {
          type: 'default',
          columnIndex: 1
        },
        {
          type: 'default',
          columnIndex: 2
        }, 
        {
          type: 'default',
          columnIndex: 3
        }]
      });
	
	
	// Insert Store Record
	Ext.getCmp('idPageSelect').getStore().insert(page.pageIndex, newRecord)
	
	// Update Store
	Ext.getCmp('idPageSelect').getStore().loadData(Ext.getCmp('idPageSelect').getStore().getRange(), false);
	
	// Add Tab Page
	tabPanel.add(newTab);
}



SALP.addTab = function(button, event, object)
{
	var tabName = Ext.getCmp('tabNameField').getValue(),
			tabPanel = Ext.getCmp('app-tabPanel'),
			numberOfTabPages = Ext.getCmp('idPageSelect').getStore().getCount(),
			newTab, newRecord;
	
	// Set Default Name
	if(tabName.length == 0)
	{
		tabName = 'New Tab'
	}
	
	
	// Force Dashboard Creation
	var newTab = Ext.create('Ext.dashboard.Dashboard',
      {
      	title: tabName,
      	pageIndex: numberOfTabPages,
      	parts:
  			{
  				default:
  				{
  					viewTemplate:
  					{
  						// Empty Container
  						xtype: 'container'
  					}
  				}
  			},
    		defaultContent: [
        {
        	type: 'default',
          columnIndex: 0
        }, 
        {
          type: 'default',
          columnIndex: 1
        },
        {
          type: 'default',
          columnIndex: 2
        }, 
        {
          type: 'default',
          columnIndex: 3
        }]
      });
	

	// Create Selection Item
	newRecord =
	{
		displayName: tabName,
		value: tabName
	}
	
	// Insert Store Record
	Ext.getCmp('idPageSelect').getStore().insert(numberOfTabPages + 1, newRecord)
	
	// Update Store
	Ext.getCmp('idPageSelect').getStore().loadData(Ext.getCmp('idPageSelect').getStore().getRange(), false);
	
	// Add New Page
	tabPanel.add(newTab);
	
	tabPanel.setActiveTab(0);
	
	// Update Page Count
	SALP.countPages ++;
	SALP.countColumns + 4;
};


SALP.removeTab = function(button, event, object)
{
	var record = Ext.getCmp('idPageSelect').findRecord(Ext.getCmp('idPageSelect').valueField, Ext.getCmp('idPageSelect').getValue()),
			index = Ext.getCmp('idPageSelect').store.indexOf(record),
			tabPage = Ext.getCmp('app-tabPanel').items.items[index];
		
		
		Ext.getCmp('app-tabPanel').remove(tabPage);
		
		Ext.getCmp('idPageSelect').getStore().remove(record);
		
		// Update Store
		Ext.getCmp('idPageSelect').getStore().loadData(Ext.getCmp('idPageSelect').getStore().getRange(), false);
		
		SALP.countPages --;
	//}
}



SALP.setTabName = function(button, event, object)
{
	var record = Ext.getCmp('idPageSelect').findRecord(Ext.getCmp('idPageSelect').valueField, Ext.getCmp('idPageSelect').getValue()),
			index = Ext.getCmp('idPageSelect').store.indexOf(record),
			newTabName = Ext.getCmp('tabNameField').getValue();
	
	Ext.getCmp('app-tabPanel').items.items[index].setTitle(newTabName);
	
	record.data.displayName = newTabName;
	
	Ext.getCmp('idPageSelect').getStore().loadData(Ext.getCmp('idPageSelect').getStore().getRange(), false);
}


SALP.getTools = function()
{
    return [
    {
  		xtype: 'tool',
  		tooltip: 'Print Chart',
      type: 'print',
  		handler: function(event, toolEl, panel)
  		{
      	var chart = panel.up().items.items[0].items.items[0];
      	
      	if (chart.preview)
      	{
      		chart.preview();
      	}
      }
  	},
  	{
  		xtype: 'tool',
  		tooltip: 'Save Chart',
      type: 'save',
  		handler: function(event, toolEl, panel)
  		{
      	var chart = panel.up().items.items[0].items.items[0],
      			title = chart.up().up().getTitle(),
      			filename = title.replace(/ /g, '_'),
      			toDate = new Date();
      			
      	filename = filename.replace(/:/g,'-');
      	filename = filename.replace('__','');
      	filename = filename.replace(/\//g, '-');

      	toDate = Ext.Date.format(toDate, 'Y-m-d_Hi');
      	filename = filename + '_' + toDate;
      	
      	if(chart.download)
      	{
      		chart.download(
      		{
      			filename: filename
      		});
      	}
      }
  	},
    {
      xtype: 'tool',
      type: 'refresh',
      handler: function(e, target, header, tool)
      {
        var portlet = header.ownerCt,
        		currentClass = portlet.items.items[0].$className,
        		store = portlet.items.items[0].items.items[0].store;
        
        
        if(currentClass != 'SALP.portlets.FavoriteLinksTreePortlet')
        {
        	if(store)
        	{
        		//portlet.setLoading('Loading...');
        		
        		store.reload();
        		
        		Ext.defer(function() 
	          {
              portlet.setLoading(false);
            }, 2000);
        	}
        	else
        	{
        		portlet.setLoading('Loading...');
        		
        		portlet.items.items[0].updateLayout();
        		
         		Ext.defer(function() 
	          {
              portlet.setLoading(false);
            }, 2000);
        	}
        }
      }
    }]
}

SALP.getOpenIncidentsTools = function()
{
  return [
  {
		xtype: 'tool',
    type: 'search',
    tooltip: 'Filter Severity',
   	handler: SALP.openIncidentsFilterWindow
	},
  {
    xtype: 'tool',
    type: 'refresh',
    handler: function(e, target, header, tool)
    {
      var portlet = header.ownerCt,
      		currentClass = portlet.items.items[0].$className,
      		store = portlet.items.items[0].items.items[0].store;
      
      
      if(currentClass != 'SALP.portlets.FavoriteLinksTreePortlet')
      {
      	if(store)
      	{
      		//portlet.setLoading('Loading...');
      		
      		store.reload();
      		
      		Ext.defer(function() 
          {
            portlet.setLoading(false);
          }, 2000);
      	}
      	else
      	{
      		portlet.setLoading('Loading...');
      		
      		portlet.items.items[0].updateLayout();
      		
       		Ext.defer(function() 
          {
            portlet.setLoading(false);
          }, 2000);
      	}
      }
    }
  }]
}

SALP.getD3Tools = function()
{
	return [
	{
		xtype: 'tool',
    type: 'search',
    tooltip: 'Open Chart',
   	handler: SALP.openD3FilterDialog
	}]
}

SALP.getFilterTools = function()
{
	return [
	{
		xtype: 'tool',
		tooltip: 'Print Chart',
    type: 'print',
		handler: function(event, toolEl, panel)
		{
    	var chart = panel.up().items.items[0].items.items[0];
    	
    	chart.preview();
    }
	},
	{
		xtype: 'tool',
		tooltip: 'Save Chart',
    type: 'save',
		handler: function(event, toolEl, panel)
		{
    	var chart = panel.up().items.items[0].items.items[0],
    			title = chart.up().up().getTitle(),
    			filename = title.replace(/ /g, '_'),
    			toDate = chart.up().up().props.toDate;
    			
    	filename = filename.replace(/:/g,'-');
    	filename = filename.replace('__','');
    			
			if(!toDate)
			{
				toDate = new Date();
			}
			else
			{
				toDate = new Date(toDate);		
			}

    	toDate = Ext.Date.format(toDate, 'Y-m-d_Hi');
    	filename = filename + '_' + toDate;
    	
    	chart.download(
    	{
    		filename: filename
    	});
    }
	},
	{
		xtype: 'tool',
    type: 'refresh',
    tooltip: 'Refresh Chart',
    handler: Ext.bind(SALP.refreshHaystackStore, this)
	},
//	{
//		xtype: 'tool',
//		tooltip: 'Haystack Filters',
//    type: 'gear',
//		handler: function(event, toolEl, panel)
//		{
//    	if(Ext.getCmp('haystackFiltersWindow'))
//    	{
//    		Ext.getCmp('haystackFiltersWindow').toFront;
//    	}
//    	else
//    	{
//    		Ext.create('SALP.view.HaystackFiltersWindow').show();
//    	}
//    }
//	},
	{
		xtype: 'tool',
    type: 'search',
    tooltip: 'Chart Filters',
   	handler: Ext.bind(SALP.openParamWindow)
	}]
}


SALP.refreshHaystackStore = function(event, target, header, tool)
{
	var portlet = header.ownerCt;
            
  portlet.setLoading('Loading...');
  
  if(portlet.title.indexOf("Breakdown") > 0)
  {
  	portlet.items.items[0].items.items[0].store.reload();
  }
  else
  {
 		portlet.items.first().store.reload();
  }
      
  Ext.defer(function() 
  {
  	portlet.setLoading(false);
	}, 2000);
}

SALP.openParamWindow = function(event, target, header, tool)
{
	SALP.view.paramWindow = Ext.create('SALP.view.ParamWindow',
	{
		props:
		{
			portlet: header.ownerCt
		}
	}).show();
	
}

SALP.openD3FilterDialog = function()
{
	//Ext.create('SALP.view.D3FilterDialog').show();
	
	Ext.create('SALP.view.TreeMapWindow').show();
}

SALP.openAddTabWindow = function()
{
	SALP.view.addTabWindow = Ext.create('SALP.view.AddTabWindow').show();
	
	var t=1;
}

SALP.openRenameTabWindow = function()
{
	SALP.view.RenameTabWindow = Ext.create('SALP.view.RenameTabWindow').show();
	
	var t=1;
}


SALP.setHaystackFilter = function()
{
	var username = Ext.getCmp('userNameField').value,
      userpassword = Ext.getCmp('userPasswordField').value,
      selectedSegment = Ext.getCmp('segmentField').value,
      selectedFilter = Ext.getCmp('filterField').value,
      portlet = this.props.portlet,
      currentSlot = this.props.portlet.props.currentSlot,
      title = portlet.getTitle(),
      fromDate = null,
      toDate = null,
      rawCookie,
      chart = portlet.items.items[0].items.items[0];
	

	
	if(Ext.getCmp('SelectDateRangePanel'))
	{
		fromDate = Ext.getCmp('SelectDateRangePanel').getDates().fromDate;
    toDate = Ext.getCmp('SelectDateRangePanel').getDates().toDate;
	}
								
 	switch(portlet.items.items[0].$className)
	{
		case 'SALP.portlets.HaystackCallsPortlet':
			title = 'Haystack Calls';
			break;
			
		case 'SALP.portlets.HaystackTicketsPortlet':
			title = 'Haystack Tickets';
			break;
		
		case 'SALP.portlets.HaystackTruckRollsPortlet':
			title = 'Haystack Truck Rolls';
			break;
			
		case 'SALP.portlets.HaystackBreakdownCallsPortlet':
			title = 'Haystack Breakdown - Calls';
			break;
			
		case 'SALP.portlets.HaystackBreakdownTicketsPortlet':
			title = 'Haystack Breakdown - Tickets';
			break;
			 				
		case 'SALP.portlets.HaystackBreakdownTruckRollsPortlet':
			title = 'Haystack Breakdown - Truck Rolls';
			break;
	}					

	if ((selectedSegment) || (selectedFilter))
  {
  	var chart = portlet.items.items[0].items.items[0];
  	
  	if (selectedSegment) title += '- ' + selectedSegment;
  	if (selectedFilter) title += ': ' + selectedFilter;
  	
  	portlet.setTitle(title);
 	}
 	else
 	{
 		switch(portlet.items.items[0].$className)
 		{
 			case 'SALP.portlets.HaystackCallsPortlet':
 				portlet.setTitle('Haystack Calls')
 				break;
 				
 			case 'SALP.portlets.HaystackTicketsPortlet':
 				portlet.setTitle('Haystack Tickets')
 				break;
 			
 			case 'SALP.portlets.HaystackTruckRollsPortlet':
 				portlet.setTitle('Haystack Truck Rolls')
 				break;
 				
 			case 'SALP.portlets.HaystackBreakdownCallsPortlet':
 				portlet.setTitle('Haystack Breakdown - Calls')
 				break;
 				
 			case 'SALP.portlets.HaystackBreakdownTicketsPortlet':
 				portlet.setTitle('Haystack Breakdown - Tickets')
 				break;
 				 				
 			case 'SALP.portlets.HaystackBreakdownTruckRollsPortlet':
 				portlet.setTitle('Haystack Breakdown - Truck Rolls')
 				break;
 		}
 	}
 	

 	
 	switch(portlet.items.items[0].$className)
	{
		case 'SALP.portlets.HaystackBreakdownCallsPortlet':
		case 'SALP.portlets.HaystackBreakdownTicketsPortlet':
		case 'SALP.portlets.HaystackBreakdownTruckRollsPortlet':
		
    	chart.filters.segment = selectedSegment;
     	chart.filters.filter = selectedFilter;
     	chart.filters.username = username;
     	chart.filters.password = userpassword;
     	
		
		
//			if(portlet.items.items[0].items.items[0].store.getRange().length > 0)
//			{
//				portlet.setLoading(true);
//		
//				Ext.Function.defer(function()
//				{
//  				var chart = portlet.items.items[0].items.items[0],
//  						fields = portlet.items.items[0].items.items[0].store.getRange()[0].data.fields,
//  						fieldNames = portlet.items.items[0].items.items[0].store.getRange()[0].data.fieldNames;
//  			
//  				chart.series[0].clearSprites();
//  			
//       	 	chart.axes[0].setFields(fields)
//    			chart.series[0].setYField(fields);
//    			     		
//    			chart.series[0].setTitle(fieldNames);
//    		
//    			chart.redraw(true);
//    		
//    			portlet.setLoading(false);
//  			}, 3000);
//			}

		
			break;
	}
 	
 	 	
 	portlet.props.username = username;
 	portlet.props.password = userpassword;
 	portlet.props.segment = selectedSegment;
 	portlet.props.filter = selectedFilter;
	
	if(portlet.title.indexOf("Breakdown") < 0)
  {
  	portlet.setLoading('Loading...');
  	
  	portlet.items.first().store.load
  	({
  		params:
  		{
  			username: username,
  			password: userpassword,
  			segment: selectedSegment,
  			filter: selectedFilter,
  			fromDate: fromDate,
  			toDate: toDate
  		}
  	});
  }
  else
  {
  	var store = portlet.items.items[0].items.items[0].getStore(),
  			chart = portlet.items.items[0].items.items[0],
  			showOthers = portlet.items.items[0].dockedItems.items[0].items.items[1],
  			breakdown = portlet.items.items[0].dockedItems.items[0].items.items[3].getValue();
  			
  			
  	chart.filters.breakdown = breakdown;		
  	chart.filters.segment = selectedSegment;
    chart.filters.filter = selectedFilter;
    chart.filters.username = username;
    chart.filters.password = userpassword;	
    chart.filters.showOthers = showOthers;
  	
  	portlet.items.items[0].loadStore();
  	
//  	store.load
//  	({
//  		params:
//  		{
//  			username: username,
//  			password: userpassword,
//  			segment: selectedSegment,
//  			filter: selectedFilter,
//				breakdown: breakdown
//  		}
//  	});
  }


	
	SALP.view.paramWindow.close();
													            
	Ext.defer(function() 
	{
		portlet.setLoading(false);
	}, 2000);
}

SALP.renderLink = function(site)
{
	var tab = window.open(site, '_blank');

	if(tab) tab.focus();
};


//SALP.updateStocks = function()
//{
//	//Ext.getCmp('StockTicker').setHtml("Loading....");
//	
//  Ext.Ajax.request(
//  {
//  	url: '/WebServices/Service.svc/GetComcastStock',
//  	success: function(response, opts) 
//  	{
//  		var stock = Ext.decode(response.responseText).data,
//  				message = '<b>Stock: ' + stock.symbol + ", last: " + stock.last + ", change: " + stock.change + '</b>';
//  		
//  		
//  		Ext.getCmp('StockTicker').setHtml(message);
//  	},
//  	failure: function(form, action) 
//  	{
//  		var t=1;
//  	}
//  });
//}


SALP.brouhaLink = function(value)
{
	
	var site = 'https://brouha.sys.comcast.net/incident/details/' + value;

	var tab = window.open(site, '_blank')

	if(tab) tab.focus();
}


SALP.deleteDashboard = function(selectedRecord)
{
	Ext.Ajax.request(
	{
		url: '/WebServices/Service.svc/DeleteDashboard',
		method: 'GET',
		params:  
  	{ 
    	  accountName: SALP.user.accountName,
    	  dashboardId: selectedRecord.dashboardId
  	}, 
		success: function(response, opts) 
		{
			Ext.getCmp('DashboardList').getStore().reload();
			
			var t=1;
		},
		failure: function(response, opts) 
		{
			var t=1;
		}
	});
}


SALP.setDashboardName = function(selectedRecord, dashboardName)
{
	if (dashboardName.length > 0)
	{
  	Ext.Ajax.request(
  	{
  		url: '/WebServices/Service.svc/RenameDashboard',
  		method: 'GET',
  		params:  
    	{ 
      	  dashboardId: selectedRecord.dashboardId,
      	  dashboardName: dashboardName
    	}, 
  		success: function(response, opts) 
  		{
  			Ext.getCmp('DashboardList').getStore().reload();
  			
  			var t=1;
  		},
  		failure: function(response, opts) 
  		{
  			var t=1;
  		}
  	});
	}
}

SALP.setDefaultDashboard = function(selectedRecord)
{
	if (selectedRecord.dashboardName.length > 0)
	{
  	Ext.Ajax.request(
  	{
  		url: '/WebServices/Service.svc/SetDefaultDashboard',
  		method: 'GET',
  		params:  
    	{ 
    		  accountName: SALP.user.accountName,
      	  dashboardId: selectedRecord.dashboardId

    	}, 
  		success: function(response, opts) 
  		{
  			Ext.getCmp('DashboardList').getStore().reload();
  			
  			var t=1;
  		},
  		failure: function(response, opts) 
  		{
  			var t=1;
  		}
  	});
	}
}


SALP.saveUserPortlets = function(porletsList)
{
	Ext.Ajax.request(
  {
  	url: '/WebServices/Service.svc/SaveUserPortlets',
  	method: 'GET',
  	params:  
    { 
        accountName: SALP.user.accountName,
        porletsList: porletsList
    }, 
  	success: function(response, opts) 
  	{
			var t=1;
			
			
			//Ext.Msg.alert('Status', 'Cookie Cleared.');
  	},
  	failure: function(response, opts) 
  	{
  		var t=1;
  	}
  });
  
  Ext.getCmp('selectChart01').getStore().reload();
  Ext.getCmp('selectChart02').getStore().reload();
  Ext.getCmp('selectChart03').getStore().reload();
  Ext.getCmp('selectChart08').getStore().reload();
  
  if(SALP.user.hasBillingAccess)
  {
  	Ext.getCmp('selectChart04').getStore().reload();
  }
  
  Ext.getCmp('selectChart05').getStore().reload();
  
  if(SALP.user.hasFCCAccess)
  {
  	Ext.getCmp('selectChart06').getStore().reload();
  }
  
  if(SALP.user.hasBLRAccess)
  {
  	Ext.getCmp('selectChart07').getStore().reload();
  }
  
  if(SALP.user.hasBLRAccess)
  {
  	Ext.getCmp('selectChart09').getStore().reload();
  }
  
  Ext.getCmp('selectChart10').getStore().reload();
}


SALP.loadUserPortletList = function()
{
	Ext.Ajax.request(
  {
  	url: '/WebServices/Service.svc/GetUserPortlets',
  	method: 'GET',
  	params:  
    { 
        accountName: SALP.user.accountName
    }, 
  	success: function(response, opts) 
  	{
			var userPorlets = Ext.decode(response.responseText).data.split(',');
			
			if(userPorlets.toString().length > 0)
			{
  			for(var index = 0; index <= userPorlets.length -1; index ++)
  			{
  				var selectionIndex = parseInt(userPorlets[index]),
  						selection = Ext.getCmp('Portlet-Selection').getStore().find('id', selectionIndex);
  				
  				Ext.getCmp('Portlet-Selection').getSelectionModel().select(selection, true)
  			}
  		}
  		else
			{
				Ext.getCmp('Portlet-Selection').getSelectionModel().selectAll();
			}
  	},
  	failure: function(response, opts) 
  	{
  		var t=1;
  	}
  });
}

SALP.getTicket = function(ticketNumber)
{
	Ext.Ajax.request(
  {
  	url: '/WebServices/Service.svc/GetTicket',
  	method: 'GET',
  	params:  
    { 
        ticketNumber: ticketNumber
    }, 
  	success: function(response, opts) 
  	{
			var ticketData = Ext.decode(response.responseText).data,
					ticketWindow = Ext.create('SALP.view.BandAidTicketWindow').show(),
				
					oldDataForm = ticketWindow.items.items[0].items.items[0],
					newDataForm = ticketWindow.items.items[0].items.items[1],
					oldTicket = Ext.create('OldTicket',
					{
						ticketId: ticketData.oldTicket.ticketId,
						calcTTR: ticketData.oldTicket.calcTTR,
						smRelatedTicket: ticketData.oldTicket.smRelatedTicket,
						causeDescription: ticketData.oldTicket.causeDescription,
						solutionDescription: ticketData.oldTicket.solutionDescription,
						severity: ticketData.oldTicket.severity,
						
						alarmStartDate: ticketData.oldTicket.alarmStartDate,
						alarmStartTime: ticketData.oldTicket.alarmStartTime,
						
						workingStartDate: ticketData.oldTicket.workingStartTime,
						workingStartTime: ticketData.oldTicket.workingStartTime,
						
						resolvedDate: ticketData.oldTicket.resolvedDate,
						resolvedTime: ticketData.oldTicket.resolvedTime,
						
						actualStartDate: ticketData.oldTicket.actualStartDate,
						actualStartTime: ticketData.oldTicket.actualStartTime,
						
						actualEndDate: ticketData.oldTicket.actualEndDate,
						actualEndTime: ticketData.oldTicket.actualEndTime,
						
						modifiedDate: ticketData.oldTicket.modifiedDate
					}),
					newTicket = Ext.create('NewTicket',
					{
						ticketId: ticketData.oldTicket.ticketId,
						adjDurationMinutes: ticketData.newTicket.adjDurationMinutes,
						causeDescription: ticketData.newTicket.causeDescription,
						solutionDescription: ticketData.newTicket.solutionDescription,
						smId: ticketData.newTicket.smId,
						severity: ticketData.newTicket.severity,
						
						alarmStartDate: ticketData.newTicket.alarmStartDate,
						alarmStartTime: ticketData.newTicket.alarmStartTime,
						
						workingStartDate: ticketData.newTicket.workingStartTime,
						workingStartTime: ticketData.newTicket.workingStartTime,
						
						resolvedDate: ticketData.newTicket.resolvedDate,
						resolvedTime: ticketData.newTicket.resolvedTime,
						
						actualStartDate: ticketData.newTicket.actualStartDate,
						actualStartTime: ticketData.newTicket.actualStartTime,
						
						actualEndDate: ticketData.newTicket.actualEndDate,
						actualEndTime: ticketData.newTicket.actualEndTime,
						
						changeReason: ticketData.newTicket.changeReason,
						modifiedDate: ticketData.newTicket.modifiedDate
					})

			oldDataForm.loadRecord(oldTicket);
			newDataForm.loadRecord(newTicket);
			
			ticketWindow.setTitle('BandAid Ticket #' +  ticketData.oldTicket.ticketId);

  	},
  	failure: function(response, opts) 
  	{
  		var t=1;
  	}
  });
}

SALP.setHaystackDate = function(fromDate, toDate)
{
	var activeTab = Ext.getCmp('app-tabPanel').getActiveTab(),
		  activeTabIndex;
		  
	if(activeTab) 
	{
		activeTabIndex = Ext.getCmp('app-tabPanel').items.findIndex('id', activeTab.id);
	}
	else
	{
		activeTabIndex = 0;
	} 
	
	for(var columnIndex = 0; columnIndex <= Ext.getCmp('app-tabPanel').items.items[activeTabIndex].items.items.length -1; columnIndex = columnIndex + 2)
	{
		for (var slotIndex = 0; slotIndex <= Ext.getCmp('app-tabPanel').items.items[activeTabIndex].items.items[columnIndex].items.items.length - 1; slotIndex ++)
		{
			var portlet = Ext.getCmp('app-tabPanel').items.items[activeTabIndex].items.items[columnIndex].items.items[slotIndex].items.first()
					
			if(portlet)
			{
				if(portlet.$className.indexOf('Haystack') > 0)
				{
					if(portlet.$className.indexOf('Breakdown') < 0)
					{
  					var chart = portlet.items.first(),
  							store = chart.getStore(),
  							segment = portlet.up().props.segment,
  							filter = portlet.up().props.filter,
  							userName = portlet.up().props.userName,
  							password = portlet.up().props.password;
  							
  					portlet.up().props.fromDate = fromDate;
  					portlet.up().props.toDate = toDate;		
  							
  					portlet.setLoading('Loading...');					
  							
  					store.load
  					({
  						params:
  						{
  							username: userName,
  							password: password,
  							segment: segment,
  							filter: filter,
  							fromDate: fromDate,
  							toDate: toDate
  						},
  						callback: function(records, operation, success) 
  						{
  							this.setLoading(false);
  							
         				var t=1;
      				},
      				scope: portlet
  					});		
					}
					else
					{
						var chart = portlet.items.items[0];
						
						chart.filters.fromDate = fromDate;
						chart.filters.toDate = toDate;
						
						portlet.loadStore();
					}
				}
			}		

			var t=1
		}
	}
}


SALP.getIncidentTicket = function(ticketNumber)
{
	Ext.Ajax.request(
  {
  	url: '/WebServices/Service.svc/GetIncidentTicket',
  	method: 'GET',
  	params:  
    { 
        ticketNumber: ticketNumber
    }, 
  	success: function(response, opts) 
  	{
			var ticketData = Ext.decode(response.responseText).data,
					ticketWindow = Ext.create('SALP.view.IncidentTicketWindow').show(),
				
					dataFormA = ticketWindow.items.items[0].items.items[0],
					dataFormB = ticketWindow.items.items[0].items.items[1],
					ticketA = Ext.create('IncidentTicketA',
					{
						ticketId: ticketData.ticketId,
						incidentWeek: ticketData.incidentWeek,
						networkLens: ticketData.networkLens,
						lensGroup: ticketData.lensGroup,
						rootCauseGroup: ticketData.rootCauseGroup,
						actualStartDate: ticketData.actualStartDate,
						alarmStartDate: ticketData.alarmStartDate,
						workingDate: ticketData.workingDate,
						resolvedDate: ticketData.resolvedDate,
						causeCategory: ticketData.causeCategory,
						causeDescription: ticketData.causeDescription,
						solutionCategory: ticketData.solutionCategory,
						solutionDescription: ticketData.solutionDescription,
						problemCategory: ticketData.problemCategory,
						problemSummary: ticketData.problemSummary,
						fixemIncidentSummary: ticketData.fixemIncidentSummary,
						detectedBy: ticketData.detectedBy,
						source: ticketData.source
					}),
					ticketB = Ext.create('IncidentTicketB',
					{
						ticketId: ticketData.ticketId,
						editLensGroup: ticketData.editLensGroup,
						editRootCauseGroup: ticketData.editRootCauseGroup,
						editIncidentSummary: ticketData.editIncidentSummary,
						editIncidentWeek: ticketData.incidentWeek
					});

			dataFormA.loadRecord(ticketA);
			dataFormB.loadRecord(ticketB);
			
			ticketWindow.setTitle('Incident Ticket #' +  ticketData.ticketId);

  	},
  	failure: function(response, opts) 
  	{
  		var t=1;
		}
  });
}


SALP.displayIncidentList = function()
{
	if(!Ext.getCmp('IncidentTicketWindow'))
	{
		SALP.view.incidentList = Ext.create('SALP.view.IncidentTicketsWindow').show();
	}
	else
	{
		SALP.view.incidentList.toFront()
	}
}


SALP.getTicketDetails = function(ticketNumber)
{
	Ext.Ajax.request(
  {
  	url: '/WebServices/Service.svc/GetTicketDetailsGeneral',
  	method: 'GET',
  	params:  
    { 
        ticketNumber: ticketNumber
    }, 
  	success: function(response, opts) 
  	{
			var ticketDetailsData = Ext.decode(response.responseText).data,
					ticketDetailsWindow = Ext.create('SALP.view.TicketDetailsWindow',
					{
						constrainTo: Ext.getCmp('app-tabPanel').getEl(),
						ticketNumber: ticketNumber
					}),
					dataFormA = ticketDetailsWindow.items.items[2],
					ticketDetailsA = Ext.create('TicketDetailsA',
					{
						ticketId: ticketDetailsData.ticketId,
						incidentSummary: ticketDetailsData.incidentSummary,
						supportAreaName: ticketDetailsData.supportAreaName,
						submittingName: ticketDetailsData.submittingName,
						queueName: ticketDetailsData.queueName,
						fullName: ticketDetailsData.fullName,
						siSeverity: ticketDetailsData.siSeverity,
						siStatus: ticketDetailsData.siStatus,
						siPriority: ticketDetailsData.siPriority,
						priority: ticketDetailsData.priority,
						product: ticketDetailsData.product,
						createDate: ticketDetailsData.createDate,
						assignedDate: ticketDetailsData.assignedDate,
						telephonyAffected: ticketDetailsData.telephonyAffected,
						videoAffected: ticketDetailsData.videoAffected,
						hsdAffected: ticketDetailsData.hsdAffected,
						totalCustomersAffected: ticketDetailsData.totalCustomersAffected,
						nextAction: ticketDetailsData.nextAction,
						problemCode: ticketDetailsData.problemCode,
						source: ticketDetailsData.source,
						category: ticketDetailsData.category,
						subCategory: ticketDetailsData.subCategory,
						causeCode: ticketDetailsData.causeCode,
						causeDescription: ticketDetailsData.causeDescription,
						causeCategory: ticketDetailsData.causeCategory,
						causeSubCategory: ticketDetailsData.causeSubCategory,
						solutionCode: ticketDetailsData.solutionCode,
						solutionDescription: ticketDetailsData.solutionDescription,
						solutionCategory: ticketDetailsData.solutionCategory,
						solutionSubCategory: ticketDetailsData.solutionSubCategory,
						externalAssignee: ticketDetailsData.externalAssignee,
						externalReference: ticketDetailsData.externalReference,
						alarmDate: ticketDetailsData.alarmDate,
						ttrStart: ticketDetailsData.ttrStart,
						ttrStop: ticketDetailsData.ttrStop,
						indicatorCity: ticketDetailsData.indicatorCity,
						indicatorState: ticketDetailsData.indicatorState,
						networkElement: ticketDetailsData.networkElement,
						correlationType: ticketDetailsData.correlationType,
						serviceCondition: ticketDetailsData.serviceCondition,
						careImpact: ticketDetailsData.careImpact,
						estimatedServiceRestore: ticketDetailsData.estimatedServiceRestore,
						siLastModifiedBy: ticketDetailsData.siLastModifiedBy,
						lastModifiedBy: ticketDetailsData.lastModifiedBy,
						modifiedDate: ticketDetailsData.modifiedDate,
						siResolvedBy: ticketDetailsData.siResolvedBy,
						resolvedBy: ticketDetailsData.resolvedBy,
						contactDepartment: ticketDetailsData.contactDepartment,
						contactPhone: ticketDetailsData.contactPhone,
						regionName: ticketDetailsData.regionName,
						duration: ticketDetailsData.duration,
						detectedBy: ticketDetailsData.source,
						cicGroupId: ticketDetailsData.contactDepartment
					});
					
					
			SALP.viewport.add(ticketDetailsWindow);
			ticketDetailsWindow.show();
				
			dataFormA.loadRecord(ticketDetailsA);			
			
			ticketDetailsWindow.setTitle('Details For Ticket #' +  ticketDetailsData.ticketId);
			
			  
      Ext.Ajax.request(
      {
      	url: '/WebServices/Service.svc/GetTicketDetailsAttributes',
      	method: 'GET',
      	params:  
        { 
            ticketNumber: ticketNumber
        }, 
      	success: function(response, opts) 
      	{
      		var ticketAttributesData = Ext.decode(response.responseText).data,
      				dataFormA = ticketDetailsWindow.items.items[2],
      				dataFormB = ticketDetailsWindow.items.items[3],
      				severityField = dataFormA.items.items[0].items.items[1],
      				ticketDetailsB = Ext.create('TicketDetailsB',
    					{
    						contactInfoDepartment: ticketAttributesData.contactInfoDepartment,
    						contactInfoPhone: ticketAttributesData.contactInfoPhone,
    						indicatorState: ticketAttributesData.indicatorState,
    						managementBridge: ticketAttributesData.managementBridge,
    						detectedBy: ticketAttributesData.detectedBy,
    						outageDescription: ticketAttributesData.outageDescription,
    						customerExperience: ticketAttributesData.customerExperience,
    						outageDescription: ticketAttributesData.outageDescription,
    						technicalBridge: ticketAttributesData.technicalBridge,
    						indicatorCity: ticketAttributesData.indicatorCity
    					});
      		
      		var t=1;
      		
      		dataFormB.loadRecord(ticketDetailsB);
      		
    			switch(severityField.value)
        	{
        		case 'Informational':
        			severityField.setStyle('background', '#00FF80');
        			severityField.setFieldStyle('color: #000000');
        			break;
        		
        		case 'Sev 1':
        			severityField.setStyle('background', '#FF0000');
        			severityField.setFieldStyle('color: #FFFFFF');
        			break;
        		
        		case 'Sev 2':
        			severityField.setStyle('background', '#FF3333');
        			severityField.setFieldStyle('color: #FFFFFF');
        			break;	
        			
        		case 'Sev 3':
        			severityField.setStyle('background', '#FF6666');
        			severityField.setFieldStyle('color: #000000');
        			break;
        			
        		case 'Sev 4':
        		  severityField.setStyle('background', '#FF9999');
        			severityField.setFieldStyle('color: #000000');
        			break;
        			
        		case 'Sev 5':
        		  severityField.setStyle('background', '#FFCCCC');
        			severityField.setFieldStyle('color: #000000');
        			break;
        	}
      		
      	},
      	failure: function(response, opts) 
      	{
      		var t=1;
      	}
      });
  	},
  	failure: function(response, opts) 
  	{
  		var t=1;
  	}
  });
}

SALP.getWorkLogDetails = function(record)
{
	if (record.data.source != 'Notification')
	{
		SALP.view.workLogDetailsWindow = Ext.create('SALP.view.WorkLogDetailsWindow', 
		{
			details: record.data.details
		}).show();
	}
	else
	{
		SALP.view.workLogNotificationsWindow = Ext.create('SALP.view.WorklogNotificationsWindow',
		{
			ticketNumber: record.data.ticketNumber,
			subject: record.data.subject
		}).show();
	}
};



SALP.reloadTreeStores = function()
{
	for(var pageIndex = 0; pageIndex <= Ext.getCmp('app-tabPanel').items.items.length -1; pageIndex ++)
	{
  	for(var columnIndex = 0; columnIndex <= Ext.getCmp('app-tabPanel').items.items[pageIndex].items.items.length -1; columnIndex = columnIndex + 2)
  	{
  		for (var slotIndex = 0; slotIndex <= Ext.getCmp('app-tabPanel').items.items[pageIndex].items.items[columnIndex].items.items.length - 1; slotIndex ++)
  		{
  			var portlet = Ext.getCmp('app-tabPanel').items.items[pageIndex].items.items[columnIndex].items.items[slotIndex].items.first();
  					
  			if(portlet)
  			{
  				if(portlet.$className.indexOf('FavoriteLinksTreePortlet') > 0)
  				{
  					portlet.setLoading('Loading...');		
  					
  					portlet.store.reload();
  				}
  			}		
  		}
  	}
  }
}



SALP.onAxisLabelRender = function (axis, label, layoutContext) 
{
    return layoutContext.renderer(label);
}


SALP.onSeriesTooltipRender = function (tooltip, record, item) 
{
    var fieldIndex = Ext.Array.indexOf(item.series.getYField(), item.field),
        name = item.series.getTitle()[fieldIndex];

    tooltip.setHtml(name + ' on ' + record.get('name') + ': ' + record.get(item.field));
}


SALP.getSITicket = function(portlet)
{
	var ticketNumber = portlet.items.items[1].items.items[0].value,
			chart = portlet.items.items[0]
	
	chart.store.load
	({
		params:
		{
			ticketNumber: ticketNumber
		},
		callback : function(records, options, success) 
		{
      if (success) 
      {
				chart.redraw();
      }
    }
	});

	var t=1;
}

SALP.loadAllUserFavoriteLinks = function()
{
	Ext.Ajax.request(
  {
  	url: '/WebServices/Service.svc/GetAllUserFavoriteLinks',
  	method: 'GET',
  	params:  
    { 
        accountName: SALP.user.accountName
    }, 
  	success: function(response, opts) 
  	{
			var results = Ext.decode(response.responseText); 
			
			SALP.data.AllFavoriteLinks.store.loadData(results.data);
			
			if (results.data.length)
			{
        for (var index = 0; index < results.data.length; index ++)
        {
          var record = SALP.data.AllFavoriteLinks.store.getById(results.data[index].id); 
          
          if (record.data.display == 'True')
          { 
          	Ext.getCmp('AllUserFavoritesGrid').getSelectionModel().select(record, true, false);
          }
        }
      }
  	},
  	failure: function(response, opts) 
  	{
  		var t=1;
  	}
  });
}


SALP.saveUserFavoriteLinks = function(selectionList)
{
	Ext.Ajax.request(
  {
  	url: '/WebServices/Service.svc/SaveUserFavoriteLinks',
  	method: 'GET',
  	params:  
    { 
        accountName: SALP.user.accountName,
        selectionList: selectionList
    }, 
  	success: function(response, opts) 
  	{
			var t=1;
			
			
			//Ext.Msg.alert('Status', 'Cookie Cleared.');
  	},
  	failure: function(response, opts) 
  	{
  		var t=1;
  	}
  });
	
	var t=1;
}

SALP.manageLinks = function()
{
	if (Ext.getCmp('ManageUserLinksWindow'))
	{
		SALP.view.ManageFavoriteLinksWindow.toFront();
	}
	else
	{
		SALP.view.ManageFavoriteLinksWindow = Ext.create('SALP.view.ManageUserLinksWindow').show();	
	}
}


SALP.getServerTools = function()
{
	return [
  {
		xtype: 'tool',
    type: 'refresh',
    tooltip: 'Refresh',
    handler: function()
    {
    	var portlet = this.up().up();
    	
    	portlet.items.items[0].items.items[0].getStore().reload();
    }
	},
	{
		xtype: 'tool',
    type: 'gear',
    tooltip: 'Select Server',
    handler: function()
    {
    	var portlet = this.up().up();
    	
    	Ext.create(SALP.view.SelectServerDialog,
    	{
    		caller: portlet
    	}).show();
    	
    	var t=1;
    }
	}]
}


SALP.openTicket = function(ticketNumber)
{
	Ext.create('SALP.view.MultiTicketWindow', 
	{
		ticketNumber: ticketNumber
	}).show();
}


SALP.openIncidentsFilterWindow = function(event, div, header, tool)
{
	var grid = header.up().items.items[0].items.items[0];
	
	if(!Ext.getCmp('OpenIncidentsFilterWindow'))
	{
  	Ext.create('SALP.view.OpenIncidentsFilterWindow',
  	{
  		grid: grid
  	}).show();
	}
	else
	{
		Ext.getCmp('OpenIncidentsFilterWindow').show();
	}
}



SALP.onContextMenu = function(event, target, object)
{
	event.preventDefault();

	Ext.create('SALP.nav.ContextMenu',
  {
  	x: event.getX(),
    y: event.getY()
  }).show();
	
}
