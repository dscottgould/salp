﻿//
//
//  Service Requst Ticket Form
//
//

Ext.define('SALP.form.ServiceRequestTicketForm', 
{
	alias: 'widget.form.ServiceRequestTicketForm',
	extend: 'Ext.form.Panel',
	
	height: '100%',
	layout: 'anchor',
	bodyPadding: '10 10 10 10',
  defaults: 
  {
  	anchor: '100%',
    labelWidth: 160
  },
  scrollable: true,
  
  initComponent: function()
  {
		this.items = this.buildItems();
    
    this.listeners = this.buildListeners();
    
    this.callParent(arguments);
  },
  
  buildItems: function()
  {
  	return [
  	{
  		xtype: 'container',
  		cls: 'TicketHeader',
  		html: '<center>Service Request Ticket Detail Report</center>'
  	},
  	{
  		xtype: 'container',
  		layout: 'hbox',
  		items: [
  		{
  			xtype: 'container',
  			flex: 1
  		},
  		{
  			xtype: 'displayfield',
				fieldCls: 'TicketHeader',
				name: 'ticketId'
  		},
  		{
  			xtype: 'container',
  			flex: 1
  		}]
  	},
  	{
  		xtype: 'container',
  		height: 20
  	},
  	{
  		xtype: 'container',
  		cls: 'TicketCategory',
  		html: '&nbsp;General Information'
  	},
  	{
  		xtype: 'container',
  		layout: 'hbox',
  		items: [
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>Problem Description</b>',
				name: 'problemDescription',
				labelWidth: 160,
				flex: 1
  		},
  		{
  			xtype: 'container',
  			width: 10
  		},
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>Status</b>',
				name: 'status',
				flex: 1
  		}]
  	},
  	{
  		xtype: 'container',
  		layout: 'hbox',
  		items: [
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>Submitter</b>',
				name: 'submitter',
				labelWidth: 160,
				flex: 1
  		},
  		{
  			xtype: 'container',
  			width: 10
  		},
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>Priority</b>',
				name: 'priority',
				flex: 1
  		}]
  	},
  	{
  		xtype: 'container',
  		layout: 'hbox',
  		items: [
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>Date Created</b>',
				name: 'createDate',
				flex: 1
  		},
  		{
  			xtype: 'container',
  			width: 10
  		},
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>Division</b>',
				name: 'assignedDivision',
				flex: 1
  		}]
  	},
  	{
  		xtype: 'container',
  		layout: 'hbox',
  		items: [
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>Assigned Date</b>',
				name: 'assignedDate',
				flex: 1
  		},
  		{
  			xtype: 'container',
  			width: 10
  		},
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>Support Area</b>',
				name: 'supportAreaName',
				flex: 1
  		}]
  	},
  	{
  		xtype: 'container',
  		layout: 'hbox',
  		items: [
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>Assigned Person</b>',
				name: 'fullName',
				labelWidth: 160,
				flex: 1
  		},
  		{
  			xtype: 'container',
  			width: 10
  		},
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>Queue</b>',
				name: 'assignedQName',
				flex: 1
  		}]
  	},
  	{
  		xtype: 'container',
  		cls: 'TicketCategory',
  		html: '&nbsp;Categorization'
  	},
  	{
  		xtype: 'container',
  		layout: 'hbox',
  		items: [
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>Problem Code</b>',
				name: 'problemCode',
				labelWidth: 140,
				flex: 1
  		},
  		{
  			xtype: 'container',
  			width: 10
  		},
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>Solution Code</b>',
				name: 'solutionCode',
				labelWidth: 90,
				flex: 1
  		}]
  	},
  	{
  		xtype: 'container',
  		layout: 'hbox',
  		items: [
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>Category</b>',
				name: 'problemCategory',
				labelWidth: 140,
				flex: 1
  		},
  		{
  			xtype: 'container',
  			width: 10
  		},
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>Description</b>',
				name: 'solutionDescription',
				labelWidth: 90,
				flex: 1
  		}]
  	},
  	{
  		xtype: 'container',
  		layout: 'hbox',
  		items: [
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>Subcategory</b>',
				name: 'problemSubcategory',
				labelWidth: 140,
				flex: 1
  		},
  		{
  			xtype: 'container',
  			width: 10
  		},
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>Category</b>',
				name: 'solutionCategory',
				labelWidth: 90,
				flex: 1
  		}]
  	},
  	{
  		xtype: 'container',
  		layout: 'hbox',
  		items: [
  		{
  			xtype: 'displayfield',
  			fieldLabel: '',
				name: '',
				labelWidth: 140,
				flex: 1
  		},
  		{
  			xtype: 'container',
  			width: 10
  		},
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>SubCategory</b>',
				name: 'solutionSubcategory',
				labelWidth: 90,
				flex: 1
  		}]
  	},
  	{
  		xtype: 'container',
  		cls: 'TicketCategory',
  		html: '&nbsp;Requestor Information'
  	},
  	{
  		xtype: 'container',
  		layout: 'hbox',
  		items: [
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>Name</b>',
				name: 'requestForName',
				labelWidth: 80,
				flex: 1
  		},
  		{
  			xtype: 'container',
  			width: 10
  		},
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>Phone</b>',
				name: 'requestForPhone',
				flex: 1
  		}]
  	},
  	{
  		xtype: 'container',
  		layout: 'hbox',
  		items: [
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>Email</b>',
				name: 'requestForEmail',
				labelWidth: 80,
				flex: 1
  		},
  		{
  			xtype: 'container',
  			width: 10
  		},
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>Location Code</b>',
				name: 'requestForLocation',
				flex: 1
  		}]
  	},
  	{
  		xtype: 'container',
  		layout: 'hbox',
  		items: [
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>Street</b>',
				name: 'workLocationStreet',
				labelWidth: 80,
				flex: 1
  		},
  		{
  			xtype: 'container',
  			width: 10
  		},
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>City</b>',
				name: 'workLocationCity',
				labelWidth: 100,
				flex: 1
  		}]
  	},
  	{
  		xtype: 'container',
  		cls: 'TicketCategory',
  		html: '&nbsp;Contact Information'
  	},
  	{
  		xtype: 'container',
  		layout: 'hbox',
  		items: [
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>Name</b>',
				name: 'contactName',
				labelWidth: 150,
				flex: 1
  		},
  		{
  			xtype: 'container',
  			width: 10
  		},
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>Phone</b>',
				name: 'contactPhone',
				flex: 1
  		}]
  	},
  	{
  		xtype: 'container',
  		layout: 'hbox',
  		items: [
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>Email</b>',
				name: 'contactEmail',
				labelWidth: 80,
				flex: 1
  		},
  		{
  			xtype: 'container',
  			width: 10
  		},
  		{
  			xtype: 'displayfield',
  			fieldLabel: '',
				name: '',
				labelWidth: 100,
				flex: 1
  		}]
  	},
  	{
  		xtype: 'container',
  		cls: 'TicketCategory',
  		html: '&nbsp;Attributes'
  	},
  	{
  		xtype: 'grid.SRTicketAttributesGrid',
  		ticketNumber: this.ticketNumber
  	},
  	{
  		xtype: 'container',
  		cls: 'TicketCategory',
  		html: '&nbsp;WorkLog'
  	},
  	{
  		xtype: 'grid.SRTicketWorklogGrid',
  		ticketNumber: this.ticketNumber
  	}]
  },
  

  buildListeners: function()
  {
  	return {
  		afterrender: function()
  		{
  			this.setLoading(true);
  			
  			this.ownerCt.setTitle('Service Request Ticket Details - ' + this.ticketNumber)
  			
  			Ext.Ajax.request(
        {
        	url: '/WebServices/Service.svc/GetSRTicketGeneralDetails',
        	method: 'GET',
        	params:  
          { 
              ticketNumber: this.ticketNumber
          }, 
          scope: this,
        	success: function(response, opts) 
        	{
						var data = Ext.decode(response.responseText).data,
						    ticket = Ext.create('SRTicketGeneralDetails',
    						{
									ticketId: data.ticketId,
									statusCode: data.statusCode,
									status: data.status,
									priorityCode: data.priorityCode,
									priority: data.priority,
									submitter: data.submitter,
									assignedDate: data.assignedDate,
									createDate: data.createDate,
									assignedDivision: data.assignedDivision,
									supportAreaName: data.supportAreaName,
									assignedQName: data.assignedQName,
									fullName: data.fullName,
									problemCode: data.problemCode,
									problemDescription: data.problemDescription,
									problemCategory: data.problemCategory,
									problemSubcategory: data.problemSubcategory,
									solutionCode: data.solutionCode,
									solutionDescription: data.solutionDescription,
									solutionCategory: data.solutionCategory,
									solutionSubcategory: data.solutionSubcategory,
									requestForName: data.requestForName,
									requestForPhone: data.requestForPhone,
									requestForLocation: data.requestForLocation,
									requestForEmail: data.requestForEmail,
									workLocationStreet: data.workLocationStreet,
									workLocationCity: data.workLocationCity,
									contactName: data.contactName,
									contactPhone: data.contactPhone,
									contactEmail: data.contactEmail
								});
					
						if(ticket.data.ticketId)
						{
							this.loadRecord(ticket);
						
							this.setLoading(false);
						}
						else
						{
							Ext.toast('Ticket not found.')
								
							this.up().close();
								
							Ext.create('SALP.view.TicketLookupDialog').show();
						}	
        	},
        	failure: function(response, opts) 
        	{
        		var t=1;
        	}
        });
  		},
  		scope: this
  	}
  }
});