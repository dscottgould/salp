﻿//
//
//   Ticket Details D Form
//
//

Ext.define('SALP.form.TicketDetailsDForm', 
{
	alias: 'widget.form.TicketDetailsDForm',
	extend: 'Ext.form.Panel',
	
	height: '100%',
	layout: 'anchor',
	bodyPadding: '10 10 10 10',
	//autoScroll: true,
	frame: false,
	border: false,
  defaults: 
  {
  	anchor: '100%',
    labelWidth: 170
  },
  
  initComponent: function()
  {
		this.items = this.buildItems();
    
    this.callParent(arguments);
  },
	
	
	buildItems: function()
	{
		return [
		{
			xtype: 'container',
			layout: 'hbox',
			items:[
			{
				xtype: 'displayfield',
				value: '<b>Work Log</b>',
				style: 
				{
        	background : '#efefef'
        },
        fieldStyle:
        {
        	'fontSize': '18px'
        },
				flex: 1
			}]
		}]
	}
	
});