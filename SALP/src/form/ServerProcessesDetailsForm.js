﻿//
//
//  Server Processes Details Form
//
//

Ext.define('SALP.form.ServerProcessesDetailsForm', 
{
	alias: 'widget.form.ServerProcessesDetailsForm',
	extend: 'Ext.form.Panel',
	
	height: '100%',
	layout: 'anchor',
	bodyPadding: '10 10 10 10',
	//bodyStyle:{"background-color":"#E0E0E0"}, 
	defaults: 
	{
  	anchor: '100%',
  	readOnly: true,
  	labelWidth: 100
	},
	
	 initComponent: function()
  {
  	this.listeners = this.buildListeners();
  	
		this.items = this.buildItems();
    
    this.callParent(arguments);
  },
	
	
	buildItems: function()
	{
		return [
		{
			xtype: 'container',
			layout: 'hbox',
			padding: '0 0 10 0',
			items: [
			{
				xtype: 'textfield',
				name: 'sessionId',
				fieldLabel: 'Sesssion Id',
				flex: 1
			},
			{
				xtype: 'container',
				width: 10
			},
			{
				xtype: 'textfield',
				name: 'loginName',
				fieldLabel: 'Login Name',
				flex: 1
			}]
		},
		{
			xtype: 'container',
			layout: 'hbox',
			padding: '0 0 10 0',
			items: [
			{
				xtype: 'textfield',
				name: 'runningFrom',
				fieldLabel: 'Running From',
				flex: 1
			},
			{
				xtype: 'container',
				width: 10
			},
			{
				xtype: 'textfield',
				name: 'runningBy',
				fieldLabel: 'Running By',
				flex: 1
			}]
		},
		{
			xtype: 'container',
			layout: 'hbox',
			padding: '0 0 10 0',
			items: [
			{
				xtype: 'textfield',
				name: 'databaseName',
				fieldLabel: 'Database',
				flex: 1
			},
			{
				xtype: 'container',
				width: 10
			},
			{
				xtype: 'textfield',
				name: 'cmd',
				fieldLabel: 'Command',
				flex: 1
			}]
		},
		{
			xtype: 'container',
			layout: 'hbox',
			padding: '0 0 10 0',
			items: [
			{
				xtype: 'textfield',
				name: 'status',
				fieldLabel: 'Status',
				flex: 1
			},
			{
				xtype: 'container',
				width: 10
			},
			{
				itemId: 'blockedByField',
				xtype: 'textfield',
				name: 'blockedBy',
				fieldLabel: 'Blocked By',
				flex: 1
			}]	
		},
		{
			xtype: 'textfield',
			name: 'programName',
			fieldLabel: 'Program Name'
		},
		{
			xtype: 'textareafield',
			name: 'statementText',
			fieldLabel: 'Statement Text',
			labelWidth: 100,
			height: 200
		},
		{
			xtype: 'container',
			layout: 'hbox',
			padding: '0 0 10 0',
			items: [
			{
				xtype: 'textfield',
				name: 'runningMinutes',
				fieldLabel: 'Running Minutes',
				flex: 1
			},
			{
				xtype: 'container',
				width: 10
			},
			{
				xtype: 'textfield',
				name: 'logicalReads',
				fieldLabel: 'Logical Reads',
				flex: 1
			}]
		},
		{
			xtype: 'container',
			layout: 'hbox',
			padding: '0 0 10 0',
			items: [
			{
				xtype: 'textfield',
				name: 'waitTime',
				fieldLabel: 'Wait Time',
				flex: 1
			},
			{
				xtype: 'container',
				width: 10
			},
			{
				xtype: 'textfield',
				name: 'reads',
				fieldLabel: 'Reads',
				flex: 1
			}]
		},
		{
			xtype: 'container',
			layout: 'hbox',
			padding: '0 0 10 0',
			items: [
			{
				xtype: 'textfield',
				name: 'lastWaitType',
				fieldLabel: 'Last Wait Type',
				flex: 1
			},
			{
				xtype: 'container',
				width: 10
			},
			{
				xtype: 'textfield',
				name: 'writes',
				fieldLabel: 'Writes',
				flex: 1
			}]
		},
		{
			xtype: 'displayfield',
			name: 'lastRequestStartTime',
			fieldLabel: 'Last Request Start Time',
			labelWidth: 150
		}]
	},
	
	buildListeners: function()
	{
		return {
			
			afterrender: function()
			{
				var blockedByField = this.items.items[3].items.items[2];
				
				if(this.record.data.blockedBy > 0)
				{
					blockedByField.setFieldStyle('background-color: #FC8F8F; background-image: none;');
				}
				
				blockedByField.getEl().on('dblclick', function()
				{
    			var blockedByRecord = Ext.getStore('CurrentServerProcesses').findRecord('sessionId', blockedByField.value);
    			
    			if(blockedByRecord)
    			{
      			Ext.create('SALP.view.ServerProcessesDetailsWindow',
        		{
        			server: this.server,
        			record: blockedByRecord
        		}).show();
    			}
  			},
  			this);

				
				this.loadRecord(this.record);
			}
		}			
	}
});