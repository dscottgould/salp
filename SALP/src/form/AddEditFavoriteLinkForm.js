﻿//
//
//  Add Edit Favorite Link Form
//
//

Ext.define('SALP.form.AddEditFavoriteLinkForm', 
{
	alias: 'widget.form.AddEditFavoriteLinkForm',
	extend: 'Ext.form.Panel',
	
	height: '100%',
	layout: 'anchor',
	bodyPadding: '10 10 10 10',
	bodyStyle:{"background-color":"#E0E0E0"}, 
	defaults: 
	{
  	anchor: '100%',
  	labelWidth: 75
	},
	
	 initComponent: function()
  {
  	var window = this.up();
  	
  	
  	if (window.mode == 'Add')
  	{
  		window.iconCls = 'addLink';
  		window.title = 'Add Favorite Link'
  	}
  	
  	if (window.mode == 'Edit')
  	{
  		window.iconCls = 'editLink';
  		window.title = 'Edit Favorite Link'
  	}
  	
  	this.listeners = this.buildListeners();
  	
		this.items = this.buildItems();
    
    this.callParent(arguments);
  },
	
	
	buildItems: function()
	{
		return [
		{
			xtype: 'hidden',
			name: 'linkId'
		},
		{
			xtype: 'hidden',
			name: 'accountName',
			value: SALP.user.accountName
		},
		{
			xtype: 'textfield',
			name: 'group',
			fieldLabel: 'Group',
			allowBlank: false
		},
		{
			xtype: 'textfield',
			name: 'description',
			fieldLabel: 'Description',
			allowBlank: false
		},
		{
			xtype: 'textfield',
			name: 'link',
			fieldLabel: 'Link',
			allowBlank: false
		}]
	},
	
	buildListeners: function()
	{
		return {
			
			afterrender: function()
			{
				if(this.up().mode == 'Edit')
				{
					this.loadRecord(this.up().record);
				}
			}
		}			
	}
});