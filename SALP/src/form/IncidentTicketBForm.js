﻿//
//
//  New Ticket Form
//
//

Ext.define('SALP.form.IncidentTicketBForm', 
{
	alias: 'widget.form.IncidentTicketBForm',
	extend: 'Ext.form.Panel',
	
	height: '100%',
	layout: 'anchor',
	bodyPadding: '10 10 10 10',
  defaults: 
  {
  	anchor: '100%',
    labelWidth: 160
  },
  
  initComponent: function()
  {
		this.items = this.buildItems();
    
    this.callParent(arguments);
  },
	
	
	buildItems: function()
	{
		return [
		{
			xtype: 'textfield',
			name: 'ticketId',
			fieldLabel: 'Ticket Number',
			hidden: true
		},
		{
			xtype: 'datefield',
			name: 'editIncidentWeek',
			fieldLabel: 'Incident Week',
			format: 'Y-m-d'
		},
		{
			xtype: 'combobox',
			name: 'editLensGroup',
			fieldLabel: 'Lens Group',
			store: SALP.data.LensGroup,
			queryMode: 'local',
			displayField: 'displayName',
			valueField: 'submitValue'
		},
		{
			xtype: 'combobox',
			name: 'editRootCauseGroup',
			fieldLabel: 'Root Cause Group',
			store: SALP.data.RootCause,
			queryMode: 'local',
			displayField: 'displayName',
			valueField: 'submitValue'
		},
		{
			xtype: 'textareafield',
			name: 'editIncidentSummary',
			fieldLabel: 'Incident Summary'
		}]
	}
});