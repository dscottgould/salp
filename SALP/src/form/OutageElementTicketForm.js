﻿//
//
//  Outage Element Ticket Form
//
//

Ext.define('SALP.form.OutageElementTicketForm', 
{
	alias: 'widget.form.OutageElementTicketForm',
	extend: 'Ext.form.Panel',
	
	height: '100%',
	layout: 'anchor',
	bodyPadding: '10 10 10 10',
  defaults: 
  {
  	anchor: '100%',
    labelWidth: 160
  },
  scrollable: true,
  
  initComponent: function()
  {
		this.items = this.buildItems();
    
    this.listeners = this.buildListeners();
    
    this.callParent(arguments);
  },
  
  buildItems: function()
  {
  	return [
  	{
  		xtype: 'container',
  		cls: 'TicketHeader',
  		html: '<center>Outage Element Ticket Detail Report</center>'
  	},
  	{
  		xtype: 'container',
  		layout: 'hbox',
  		items: [
  		{
  			xtype: 'container',
  			flex: 1
  		},
  		{
  			xtype: 'displayfield',
				fieldCls: 'TicketHeader'
				//name: 'ticketId'
  		},
  		{
  			xtype: 'container',
  			flex: 1
  		}]
  	},
  	{
  		xtype: 'container',
  		height: 20
  	},
  	{
  		xtype: 'container',
  		cls: 'TicketCategory',
  		html: '&nbsp;General Information'
  	},
  	{
  		xtype: 'container',
  		layout: 'hbox',
  		items: [
  		{
  			xtype: 'displayfield',
  			cls: 'ticketLink',
  			fieldLabel: '<b>Parent Ticket ID</b>',
				name: 'ticketId',
				labelWidth: 120,
				flex: 1	
  		},
  		{
  			xtype: 'container',
  			width: 10
  		},
  		{
  			xtype: 'displayfield',
  			cls: 'ticketLink',
  			fieldLabel: '<b>Originating Ticket</b>',
				name: 'originatingTicket',
				labelWidth: 115,
				flex: 1
  		}]
  	},
  	{
  		xtype: 'container',
  		layout: 'hbox',
  		items: [
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>Service Affected</b>',
				name: 'serviceAffected',
				labelWidth: 120,
				flex: 1
  		},
  		{
  			xtype: 'container',
  			width: 10
  		},
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>Status</b>',
				name: 'aeStatus',
				labelWidth: 110,
				flex: 1
  		}]
  	},
  	{
  		xtype: 'container',
  		layout: 'hbox',
  		items: [
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>Element Name</b>',
				name: 'elementName',
				labelWidth: 120,
				flex: 1
  		},
  		{
  			xtype: 'container',
  			width: 10
  		},
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>Video</b>',
				name: 'videoAffected',
				labelWidth: 110,
				flex: 1
  		}]
  	},
  	{
  		xtype: 'container',
  		layout: 'hbox',
  		items: [
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>Element Type</b>',
				name: 'elementType',
				labelWidth: 120,
				flex: 1
  		},
  		{
  			xtype: 'container',
  			width: 10
  		},
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>HSD</b>',
				name: 'hsdAffected',
				labelWidth: 110,
				flex: 1
  		}]
  	},
  	{
  		xtype: 'container',
  		layout: 'hbox',
  		items: [
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>Source</b>',
				name: 'source',
				labelWidth: 120,
				flex: 1
  		},
  		{
  			xtype: 'container',
  			width: 10
  		},
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>Telephony</b>',
				name: 'telephonyAffected',
				labelWidth: 110,
				flex: 1
  		}]
  	},
  	{
  		xtype: 'container',
  		layout: 'hbox',
  		items: [
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>Product</b>',
				name: 'product',
				labelWidth: 120,
				flex: 1
  		},
  		{
  			xtype: 'container',
  			width: 10
  		},
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>Total</b>',
				name: 'totalAffected',
				labelWidth: 110,
				flex: 1
  		}]
  	},
  	{
  		xtype: 'container',
  		layout: 'hbox',
  		items: [
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>Item Focus</b>',
				name: 'itemFocus',
				labelWidth: 120,
				flex: 1
  		},
  		{
  			xtype: 'container',
  			width: 10
  		},
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>Indicator State</b>',
				name: 'indicatorState',
				labelWidth: 110,
				flex: 1
  		}]
  	},
  	{
  		xtype: 'container',
  		layout: 'hbox',
  		items: [
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>System Name</b>',
				name: 'systemName',
				labelWidth: 120,
				flex: 1
  		},
  		{
  			xtype: 'container',
  			width: 10
  		},
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>Region Name</b>',
				name: 'regionName',
				labelWidth: 110,
				flex: 1
  		}]
  	},
  	{
  		xtype: 'container',
  		layout: 'hbox',
  		items: [
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>Commercial Use</b>',
				name: 'commercialUse',
				labelWidth: 120,
				flex: 1
  		},
  		{
  			xtype: 'container',
  			width: 10
  		},
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>Location</b>',
				name: 'location',
				labelWidth: 110,
				flex: 1
  		}]
  	},
  	{
  		xtype: 'container',
  		layout: 'hbox',
  		items: [
  		{
  			xtype: 'displayfield',
  			fieldLabel: '',
				name: '',
				labelWidth: 120,
				flex: 1
  		},
  		{
  			xtype: 'container',
  			width: 10
  		},
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>Message Internal</b>',
				name: 'messageInternal',
				labelWidth: 110,
				flex: 1
  		}]
  	},
  	{
  		xtype: 'container',
  		cls: 'TicketCategory',
  		html: '&nbsp;Categorization'
  	},
  	{
  		xtype: 'container',
  		layout: 'hbox',
  		items: [
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>Problem Description</b>',
				name: 'problemDescription',
				labelWidth: 140,
				flex: 1
  		}]
  	},
  	{
  		xtype: 'container',
  		layout: 'hbox',
  		items: [
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>Problem Subcategory</b>',
				name: 'subcategory',
				labelWidth: 140,
				flex: 1
  		},
  		{
  			xtype: 'container',
  			width: 10
  		},
 			{
  			xtype: 'displayfield',
  			fieldLabel: '<b>Problem Category</b>',
				name: 'category',
				labelWidth: 140,
				flex: 1
  		}] 
  	},
  	{
  		xtype: 'container',
  		layout: 'hbox',
  		items: [
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>Cause Description</b>',
				name: 'causeDescription',
				labelWidth: 140,
				flex: 1
  		},
  		{
  			xtype: 'container',
  			width: 10
  		},
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>Solution Description</b>',
				name: 'solutionDescription',
				labelWidth: 140,
				flex: 1
  		}]
  	},
  	{
  		xtype: 'container',
  		layout: 'hbox',
  		items: [
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>Cause Subcategory</b>',
				name: 'causeSubcategory',
				labelWidth: 140,
				flex: 1
  		},
  		{
  			xtype: 'container',
  			width: 10
  		},
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>Solution Subcategory</b>',
				name: 'solutionSubcategory',
				labelWidth: 140,
				flex: 1
  		}]
  	},
  	{
  		xtype: 'container',
  		layout: 'hbox',
  		items: [
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>Cause Category</b>',
				name: 'causeCategory',
				labelWidth: 140,
				flex: 1
  		},
  		{
  			xtype: 'container',
  			width: 10
  		},
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>Solution Category</b>',
				name: 'solutionCategory',
				labelWidth: 140,
				flex: 1
  		}]
  	},
  	{
  		xtype: 'container',
  		cls: 'TicketCategory',
  		html: '&nbsp;Details'
  	},
  	{
  		xtype: 'container',
  		layout: 'hbox',
  		items: [
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>Planned</b>',
				name: 'plannedFlag',
				labelWidth: 110,
				flex: 1
  		},
  		{
  			xtype: 'container',
  			width: 10
  		},
  		{
  			xtype: 'displayfield',
  			fieldLabel: '',
				name: '',
				flex: 1
  		}]
  	},
  	{
  		xtype: 'container',
  		layout: 'hbox',
  		items: [
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>Planned Start</b>',
				name: 'plannedStart',
				labelWidth: 110,
				flex: 1
  		},
  		{
  			xtype: 'container',
  			width: 10
  		},
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>Actual Start</b>',
				name: 'actualStart',
				flex: 1
  		}]
  	},
  	{
  		xtype: 'container',
  		layout: 'hbox',
  		items: [
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>Planned End</b>',
				name: 'plannedEnd',
				labelWidth: 110,
				flex: 1
  		},
  		{
  			xtype: 'container',
  			width: 10
  		},
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>Actual End</b>',
				name: 'actualEnd',
				flex: 1
  		}]
  	},
  	{
  		xtype: 'container',
  		layout: 'hbox',
  		items: [
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>ETR</b>',
				name: 'etr',
				labelWidth: 110,
				flex: 1
  		},
  		{
  			xtype: 'container',
  			width: 10
  		},
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>Duration</b>',
				name: 'duration',
				flex: 1
  		}]
  	},
  	{
  		xtype: 'container',
  		layout: 'hbox',
  		items: [
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>Submitter</b>',
				name: 'submitter',
				labelWidth: 110,
				flex: 1
  		},
  		{
  			xtype: 'container',
  			width: 10
  		},
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>Last Modified By</b>',
				name: 'lastModifiedBy',
				labelWidth: 110,
				flex: 1
  		}]
  	},
  	{
  		xtype: 'container',
  		layout: 'hbox',
  		items: [
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>Create Date</b>',
				name: 'createDate',
				labelWidth: 110,
				flex: 1
  		},
  		{
  			xtype: 'container',
  			width: 10
  		},
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>Modified Date</b>',
				name: 'modifiedDate',
				flex: 1
  		}]
  	},
  	{
  		xtype: 'container',
  		cls: 'TicketCategory',
  		html: '&nbsp;Attributes'
  	},
  	{
  		xtype: 'grid.OETicketAttributesGrid',
  		ticketNumber: this.ticketNumber
  	}]
  },
  

  buildListeners: function()
  {
  	return {
  		afterrender: function()
  		{
  			parentTicketFeild = this.items.items[4].items.items[0];
  			originTicketField = this.items.items[4].items.items[2]
  			
  			parentTicketFeild.getEl().on('dblclick', function()
				{
					this.getTicket(parentTicketFeild.getValue());
  			},
  			this);
  			
  			originTicketField.getEl().on('dblclick', function()
				{
					this.getTicket(originTicketField.getValue());
  			},
  			this);
  			
  			
  			this.setLoading(true);
  			
  			this.ownerCt.setTitle('Outage Element Ticket Details - ' + this.ticketNumber)
  			
  			this.items.items[1].items.items[1].setValue(this.ticketNumber);
  			
  			Ext.Ajax.request(
        {
        	url: '/WebServices/Service.svc/GetOETicketGeneralDetails',
        	method: 'GET',
        	params:  
          { 
              ticketNumber: this.ticketNumber
          }, 
          scope: this,
        	success: function(response, opts) 
        	{
						var data = Ext.decode(response.responseText).data,
						    ticket = Ext.create('OETicketGeneralDetails',
    						{
									ticketId: data.ticketId,
									entryId: data.entryId,
									serviceAffected: data.serviceAffected,
									elementName: data.elementName,
									telephonyAffected: data.telephonyAffected,
									hsdAffected: data.hsdAffected,
									videoAffected: data.videoAffected,
									totalAffected: data.totalAffected,
									aeStatus: data.aeStatus,
									elementType: data.elementType,
									plannedFlag: data.plannedFlag,
									plannedStart: data.plannedStart,
									plannedEnd: data.plannedEnd,
									duration: data.duration,
									source: data.source,
									itemFocus: data.itemFocus,
									regionId: data.regionId,
									regionName: data.regionName,
									systemId: data.systemId,
									systemName: data.systemName,
									indicatorState: data.indicatorState,
									location: data.location,
									causeCategory: data.causeCategory,
									product: data.product,
									commercialUse: data.commercialUse,
									messageInternal: data.messageInternal,
									linkProblemCode: data.linkProblemCode,
									problemDescription: data.problemDescription,
									category: data.category,
									subcategory: data.subcategory,
									causeCode: data.causeCode,
									causeDescription: data.causeDescription,
									causeCategory: data.causeCategory,
									causeSubcategory: data.causeSubcategory,
									solutionCode: data.solutionCode,
									solutionDescription: data.solutionDescription,
									solutionCategory: data.solutionCategory,
									solutionSubcategory: data.solutionSubcategory,
									originatingTicket: data.originatingTicket,
									submitter: data.submitter,
									lastModifiedBy: data.lastModifiedBy,
									actualStart: data.actualStart,
									actualEnd: data.actualEnd,
									etr: data.etr,
									createDate: data.createDate,
									modifiedDate: data.modifiedDate
								});
					
						if(ticket.data.ticketId)
						{
							this.loadRecord(ticket);
						
							this.setLoading(false);
						}
						else
						{
							Ext.toast('Ticket not found.')
								
							Ext.create('SALP.view.TicketLookupDialog').show();
							
							this.ownerCt.destroy();
						}	
        	},
        	failure: function(response, opts) 
        	{
        		var t=1;
        	}
        });
  		},
  		scope: this
  	}
  },
  getTicket: function(ticketId)
  {
	  Ext.create('SALP.view.MultiTicketWindow',
  	{
  		ticketNumber: ticketId
  	}).show();
  }
});