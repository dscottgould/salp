﻿//
//
//  OE Ticket Affected Elements Form
//
//

Ext.define('SALP.form.OETicketAffectedElementsForm', 
{
	alias: 'widget.form.OETicketAffectedElementsForm',
	extend: 'Ext.form.Panel',
	
	height: '100%',
	layout: 'anchor',
	bodyPadding: '10 10 10 10',
  defaults: 
  {
  	anchor: '100%',
    labelWidth: 20
  },
  scrollable: false,
  border: false,
  
  initComponent: function()
  {
		this.items = this.buildItems();
    
    this.listeners = this.buildListeners();
    
    this.callParent(arguments);
  },
  
  buildItems: function()
  {
  	return [
  	{
  		xtype: 'container',
  		layout: 'hbox',
  		defaults: 
      {
      	anchor: '100%',
        labelWidth: 70
      },
  		items: [
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>Ticket ID</b>',
				name: '',
				width: 100
  		},
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>Affect</b>',
				name: '',
				width: 75
  		},
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>Element</b>',
				name: '',
				width: 75
  		},
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>Video</b>',
				name: '',
				width: 50
  		},
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>HSD</b>',
				name: '',
				width: 50
  		},
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>Telephony</b>',
				name: '',
				width: 75
  		},
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>Total</b>',
				name: '',
				width: 50
  		}]
  	},
  	{
  		xtype: 'container',
  		layout: 'hbox',
  		defaults: 
      {
      	anchor: '100%',
        labelWidth: 70
      },
  		items: [
  		{
  			xtype: 'displayfield',
  			fieldLabel: '',
				name: 'aeTicketId',
				width: 100
  		},
  		{
  			xtype: 'displayfield',
  			fieldLabel: '',
				name: 'aeServiceAffected',
				width: 75
  		},
  		{
  			xtype: 'displayfield',
  			fieldLabel: '',
				name: 'aeElementName',
				width: 75
  		},
  		{
  			xtype: 'displayfield',
  			fieldLabel: '',
				name: 'aeVideoAffected',
				width: 50
  		},
  		{
  			xtype: 'displayfield',
  			fieldLabel: '',
				name: 'aeHSDAffected',
				width: 50
  		},
  		{
  			xtype: 'displayfield',
  			fieldLabel: '',
				name: 'aeTelephonyAffected',
				width: 75
  		},
  		{
  			xtype: 'displayfield',
  			fieldLabel: '',
				name: 'aeTotalAffected',
				width: 50
  		}]
  	},
  	{
  		xtype: 'container',
  		layout: 'hbox',
  		defaults: 
      {
      	anchor: '100%',
        labelWidth: 65
      },
  		items: [
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>Status</b>',
				name: '',
				flex: 1
  		},
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>Actual Start</b>',
  			labelWidth: 100,
				name: '',
				flex: 1
  		},
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>Actual End</b>',
  			labelWidth: 100,
				name: '',
				flex: 1
  		},
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>Element Type</b>',
  			labelWidth: 100,
				name: '',
				flex: 1
  		},
  		{
  			xtype: 'container',
  			width: 10
  		},
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>Planned?</b>',
				name: '',
				flex: 1
  		},
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>System</b>',
				name: '',
				flex: 1
  		},]
  	},
  	{
  		xtype: 'container',
  		layout: 'hbox',
  		defaults: 
      {
      	anchor: '100%',
        labelWidth: 65
      },
  		items: [
  		{
  			xtype: 'displayfield',
  			fieldLabel: '',
				name: 'aeStatus',
				flex: 1
  		},
  		{
  			xtype: 'displayfield',
  			fieldLabel: '',
				name: 'aeActualStart',
				flex: 1
  		},
  		{
  			xtype: 'displayfield',
  			fieldLabel: '',
				name: 'aeActualEnd',
				flex: 1
  		},
  		{
  			xtype: 'displayfield',
  			fieldLabel: '',
				name: 'aeElementType',
				flex: 1
  		},
  		{
  			xtype: 'container',
  			width: 10
  		},
  		{
  			xtype: 'displayfield',
  			fieldLabel: '',
				name: 'aePlannedFlag',
				flex: 1
  		},
  		{
  			xtype: 'displayfield',
  			fieldLabel: '',
				name: 'aeSystemName',
				flex: 1
  		}]
  	},
  	{
  		xtype: 'container',
  		layout: 'hbox',
  		defaults: 
      {
      	anchor: '100%',
        labelWidth: 65
      },
  		items: [
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>Cause Code</b>',
				name: '',
				labelWidth: 80
  		},
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>Cause Description</b>',
  			labelWidth: 120,
				name: ''
  		},
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>Solution Code</b>',
  			labelWidth: 120,
				name: ''
  		},
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>Solution Description</b>',
  			labelWidth: 140,
				name: ''
  		}]
  	},
  	{
  		xtype: 'container',
  		layout: 'hbox',
  		defaults: 
      {
      	anchor: '100%',
        labelWidth: 65
      },
  		items: [
  		{
  			xtype: 'displayfield',
  			fieldLabel: '',
				name: 'aeCauseCode',
				width: 100
  		},
  		{
  			xtype: 'displayfield',
  			fieldLabel: '',
				name: 'aeCauseDescription',
				width: 120
  		},
  		{
  			xtype: 'displayfield',
  			fieldLabel: '',
				name: 'aeSolutionCode',
				width: 120
  		},
  		{
  			xtype: 'displayfield',
  			fieldLabel: '',
				name: 'aeSolutionDescription',
				width: 100
  		}]
  	},
  	{
  		xtype: 'container',
  		layout: 'hbox',
  		defaults: 
      {
      	anchor: '100%',
        labelWidth: 65
      },
  		items: [
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>Submitter</b>',
				name: ''
  		}]
  	},
  	{
  		xtype: 'container',
  		layout: 'hbox',
  		defaults: 
      {
      	anchor: '100%',
        labelWidth: 65
      },
  		items: [
  		{
  			xtype: 'displayfield',
  			fieldLabel: '',
				name: 'aeSubmitter',
				flex: 1
  		}]
  	}]
  },
  

  buildListeners: function()
  {
  	return {
  		afterrender: function()
  		{
  			//this.setLoading(true);
  			
  			
  			Ext.Ajax.request(
        {
        	url: '/WebServices/Service.svc/GetOETicketAffectedElements',
        	method: 'GET',
        	params:  
          { 
              ticketNumber: this.ticketNumber
          }, 
          scope: this,
        	success: function(response, opts) 
        	{
						var data = Ext.decode(response.responseText).data,
						    record = Ext.create('OETicketAffectedElements',
    						{
									aeTicketId: data.ticketId,
									aeEntryId: data.entryId,
									aeServiceAffected: data.serviceAffected,
									aeElementName: data.elementName,
									aeTelephonyAffected: data.telephonyAffected,
									aeVideoAffected: data.videoAffected,
									aeHSDAffected: data.hsdAffected,
									aeTotalAffected: data.totalAffected,
									aeStatus: data.aeStatus,
									aeElementType: data.elementType,
									aePlannedFlag: data.plannedFlag,
									aeSystemName: data.systemName,
									aeMessageInternal: data.messageInternal,
									aeCauseCode: data.causeCode,
									aeCauseDescription: data.causeDescription,
									aeSolutionCode: data.solutionCode,
									aeSolutionDescription: data.solutionDescription,
									aeOriginatingTicket: data.originatingTicket,
									aeSubmitter: data.submitter,
									aeLastModifiedBy: data.lastModifiedBy,
									aeActualStart: data.actualStart,
									aeActualEnd: data.actualEnd
								});
					
						this.loadRecord(record);
        	},
        	failure: function(response, opts) 
        	{
        		var t=1;
        	}
        });
  		},
  		scope: this
  	}
  }
});