﻿//
//
//  Add Edit Servers Form
//
//

Ext.define('SALP.form.AddEditServersForm', 
{
	alias: 'widget.form.AddEditServersForm',
	extend: 'Ext.form.Panel',
	
	height: '100%',
	layout: 'anchor',
	bodyPadding: '10 10 10 10',
	bodyStyle:{"background-color":"#E0E0E0"}, 
	defaults: 
	{
  	anchor: '100%',
  	labelWidth: 150
	},
	
	 initComponent: function()
  {
  	var window = this.up();
  	
  	
  	if (window.mode == 'Add')
  	{
  		window.iconCls = 'serverAdd';
  		window.title = 'Add Server'
  	}
  	
  	if (window.mode == 'Edit')
  	{
  		window.iconCls = 'serverEdit';
  		window.title = 'Edit Server'
  	}
  	
  	this.listeners = this.buildListeners();
  	
		this.items = this.buildItems();
    
    this.callParent(arguments);
  },
	
	
	buildItems: function()
	{
		return [
		{
			xtype: 'hidden',
			name: 'id'
		},
		{
			xtype: 'textfield',
			name: 'serverName',
			fieldLabel: 'Server Name',
			allowBlank: false
		},
		{
			xtype: 'combo',
			name: 'hasMonitoredProcesses',
			fieldLabel: 'Monitored Processes',
			store: ['True','False'],
			allowBlank: false
		},
		{
			xtype: 'combo',
			name: 'hasLongRunningJobs',
			fieldLabel: 'Long Running Jobs',
			store: ['True','False'],
			allowBlank: false
		}]
	},
	
	buildListeners: function()
	{
		return {
			
			afterrender: function()
			{
				if(this.up().mode == 'Edit')
				{
					this.loadRecord(this.up().record);
				}
			}
		}			
	}
});