﻿//
//
//   Ticket Details A Form
//
//

Ext.define('SALP.form.TicketDetailsAForm', 
{
	alias: 'widget.form.TicketDetailsAForm',
	extend: 'Ext.form.Panel',
	
	height: '100%',
	layout: 'anchor',
	bodyPadding: '10 10 10 10',
	//autoScroll: true,
	frame: false,
	border: false,
  defaults: 
  {
  	anchor: '100%',
    labelWidth: 250
  },
  
  initComponent: function()
  {
		this.items = this.buildItems();
    
    this.callParent(arguments);
  },
	
	
	buildItems: function()
	{
		return [
		{
			xtype: 'container',
			layout: 'hbox',
			items:[
			{
				xtype: 'displayfield',
				value: '<b>General Information</b>',
				style: 
				{
        	background : '#efefef'
        },
        fieldStyle:
        {
        	'fontSize': '18px'
        },
				flex: 1
			},
			{
				//id: 'TicketDetailsSeverity',
				xtype: 'displayfield',
				name: 'siSeverity',
				style: 
				{
        	background : 'red',
          textAlign: 'center'
        },
        fieldStyle:
        {
       		color: 'white',
       		'font-weight': 'bold',
       		'fontSize': '18px'
        },
       	flex: .5,
			}]
		},
		{
			xtype: 'container',
			layout: 'hbox',
			items: [
			{
				xtype: 'displayfield',
				name: 'incidentSummary',
				fieldLabel: '&nbsp;&nbsp;<b>Problem Description</b>',
				labelWidth: 140,
				flex: 1
			},
			{
				xtype: 'displayfield',
				name: 'siStatus',
				fieldLabel: '<b>Status</b>',
				flex: 1
			}]
		},
		{
			xtype: 'container',
			layout: 'hbox',
			items: [
			{
				xtype: 'displayfield',
				name: 'region',
				fieldLabel: '&nbsp;&nbsp;<b>Region</b>',
				labelWidth: 140,
				flex: 1
			},
			{
				xtype: 'displayfield',
				name: 'siPriority',
				fieldLabel: '<b>Priority</b>',
				flex: 1
			}]
		},
		{
			xtype: 'container',
			layout: 'hbox',
			items: [
			{
				xtype: 'displayfield',
				name: 'supportAreaName',
				fieldLabel: '&nbsp;&nbsp;<b>Support Area</b>',
				labelWidth: 100,
				flex: 1
			},
			{
				xtype: 'displayfield',
				name: 'product',
				fieldLabel: '<b>Product</b>',
				flex: 1
			}]
		},
		{			
			xtype: 'container',
			layout: 'hbox',
			items: [
			{
				xtype: 'displayfield',
				name: 'queueName',
				fieldLabel: '&nbsp;&nbsp;<b>Queue</b>',
				labelWidth: 100,
				flex: 1
			},
			{
				xtype: 'displayfield',
				name: 'videoAffected',
				fieldLabel: '<b>Video</b>',
				flex: 1
			}]
		},
		{
			xtype: 'container',
			layout: 'hbox',
			items: [
			{
				xtype: 'displayfield',
				name: 'assignedDate',
				fieldLabel: '&nbsp;&nbsp;<b>Assigned Date</b>',
				labelWidth: 100,
				flex: 1
			},
			{
				xtype: 'displayfield',
				name: 'hsdAffected',
				fieldLabel: '<b>HSD</b>',
				flex: 1
			}]
		},
		{
			xtype: 'container',
			layout: 'hbox',
			items: [
			{
				xtype: 'displayfield',
				name: 'createDate',
				fieldLabel: '&nbsp;&nbsp;<b>Create Date</b>',
				labelWidth: 100,
				flex: 1
			},
			{
				xtype: 'displayfield',
				name: 'telephonyAffected',
				fieldLabel: '<b>Telephony</b>',
				flex: 1
			}]
		},
		{
			xtype: 'container',
			layout: 'hbox',
			items: [
			{
				xtype: 'displayfield',
				name: 'serviceCondition',
				fieldLabel: '&nbsp;&nbsp;<b>Service Condition</b>',
				labelWidth: 120,
				flex: 1
			},
			{
				xtype: 'displayfield',
				name: 'totalCustomersAffected',
				fieldLabel: '<b>Total</b>',
				flex: 1
			}]
		},
		{
			xtype: 'container',
			layout: 'hbox',
			items:[
			{
				xtype: 'displayfield',
				value: '<b>Categorization</b>',
				style: 
				{
        	background : '#efefef'
        },
        fieldStyle:
        {
        	'fontSize': '18px'
        },
				flex: 1
			}]
		},
		{
			xtype: 'container',
			layout: 'hbox',
			items: [
			{
				xtype: 'displayfield',
				name: 'category',
				fieldLabel: '&nbsp;&nbsp;<b>Problem Category</b>',
				labelWidth: 130,
				flex: 1
			},
			{
				xtype: 'displayfield',
				name: 'causeDescription',
				fieldLabel: '<b>Cause Description</b>',
				labelWidth: 130,
				flex: 1
			}]
		},
		{
			xtype: 'container',
			layout: 'hbox',
			items: [
			{
				xtype: 'displayfield',
				name: 'subCategory',
				fieldLabel: '&nbsp;&nbsp;<b>Problem Subcategory</b>',
				labelWidth: 150,
				flex: 1
			},
			{
				xtype: 'displayfield',
				name: 'causeCategory',
				fieldLabel: '<b>Cause Category</b>',
				flex: 1
			}]
		},
		{
			xtype: 'container',
			layout: 'hbox',
			items: [
			{
				xtype: 'displayfield',
				name: 'solutionDescription',
				fieldLabel: '&nbsp;&nbsp;<b>Solution Description</b>',
				labelWidth: 150,
				flex: 1
			}]
		},
		{
			xtype: 'container',
			layout: 'hbox',
			items: [
			{
				xtype: 'displayfield',
				name: 'solutionCategory',
				fieldLabel: '&nbsp;&nbsp;<b>Solution Category</b>',
				labelWidth: 150,
				flex: 1
			},
			{
				xtype: 'displayfield',
				name: 'externalAssignee',
				fieldLabel: '<b>External Assignee</b>',
				labelWidth: 150,
				flex: 1
			}]
		},
		{
			xtype: 'container',
			layout: 'hbox',
			items: [
			{
				xtype: 'displayfield',
				name: 'solutionSubCategory',
				fieldLabel: '&nbsp;&nbsp;<b>Solution Subcategory</b>',
				labelWidth: 150,
				flex: 1
			},
			{
				xtype: 'displayfield',
				name: 'externalReference',
				fieldLabel: '<b>External Reference</b>',
				labelWidth: 150,
				flex: 1
			}]
		},
		{
			xtype: 'container',
			layout: 'hbox',
			items:[
			{
				xtype: 'displayfield',
				value: '<b>Details</b>',
				style: 
				{
        	background : '#efefef'
        },
        fieldStyle:
        {
        	'fontSize': '18px'
        },
				flex: 1
			}]
		},
		{
			xtype: 'container',
			layout: 'hbox',
			items: [
			{
				xtype: 'displayfield',
				name: 'alarmDate',
				fieldLabel: '&nbsp;&nbsp;<b>Alarm Start</b>',
				labelWidth: 150,
				flex: 1
			},
			{
				xtype: 'displayfield',
				name: 'indicatorCity',
				fieldLabel: '<b>Indicator City</b>',
				flex: 1
			}]
		},
		{
			xtype: 'container',
			layout: 'hbox',
			items: [
			{
				xtype: 'displayfield',
				name: 'ttrStart',
				fieldLabel: '&nbsp;&nbsp;<b>TTR Start</b>',
				labelWidth: 150,
				flex: 1
			},
			{
				xtype: 'displayfield',
				name: 'indicatorState',
				fieldLabel: '<b>Indicator State</b>',
				flex: 1
			}]
		},
		{
			xtype: 'container',
			layout: 'hbox',
			items: [
			{
				xtype: 'displayfield',
				name: 'ttrStop',
				fieldLabel: '&nbsp;&nbsp;<b>TTR Stop</b>',
				labelWidth: 150,
				flex: 1
			},
			{
				xtype: 'displayfield',
				name: 'source',
				fieldLabel: '<b>Source</b>',
				flex: 1
			}]
		}]
	}
});