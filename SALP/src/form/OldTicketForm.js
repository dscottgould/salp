﻿//
//
//  Old Ticket Form
//
//

Ext.define('SALP.form.OldTicketForm', 
{
	alias: 'widget.form.oldticketform',
	extend: 'Ext.form.Panel',
	
	height: '100%',
	layout: 'anchor',
	bodyPadding: '10 10 10 10',
	bodyStyle:{"background-color":"#E0E0E0"}, 
	defaults: 
	{
  	anchor: '100%',
  	labelWidth: 160
	},
	
	 initComponent: function()
  {
		this.items = this.buildItems();
    
    this.callParent(arguments);
  },
	
	
	buildItems: function()
	{
		return [
	 	{
			xtype: 'textfield',
			name: 'ticketId',
			fieldLabel: 'Ticket Number',
			hidden: true
		},
		{
			xtype: 'textfield',
			name: 'smRelatedTicket',
			fieldLabel: 'SM Related Ticket',
			readOnly: true
		},
		{
			xtype: 'textfield',
			name: 'calcTTR',
			fieldLabel: 'Calc TTR',
			readOnly: true
		},
		{
			xtype: 'textfield',
			name: 'causeDescription',
			fieldLabel: 'Cause Description',
			readOnly: true
		},
		{
			xtype: 'textareafield',
			name: 'solutionDescription',
			fieldLabel: 'Solution Description',
			readOnly: true
		},
		{
			xtype: 'textfield',
			name: 'severity',
			fieldLabel: 'Severity',
			readOnly: true
		},
		{
			xtype: 'container',
			html: '<span style="color:red; float:right; font-weight:bold">NOTE: Times Displayed in Mountain Time.&nbsp;&nbsp;&nbsp;</span>'
		},
		{
			xtype: 'textfield',
			name: 'actualStartDate',
			fieldLabel: 'Actual Start',
			readOnly: true
		},
		{
			xtype: 'textfield',
			name: 'alarmStartDate',
			fieldLabel: 'Alarm Start',
			readOnly: true
		},
		{
			xtype: 'textfield',
			name: 'workingStartDate',
			fieldLabel: 'Working Start',
			readOnly: true
		},
		{
			xtype: 'textfield',
			name: 'resolvedDate',
			fieldLabel: 'Resolved Date',
			readOnly: true
		},

		{
			xtype: 'textfield',
			name: 'actualEndDate',
			fieldLabel: 'Actual End Date',
			readOnly: true
		},
		{
			xtype: 'container',
			height: 63,
		},
		{
			xtype: 'container',
			height: 25,
			html : '<hr />'
		},
		{
			xtype: 'displayfield',
			name: 'modifiedDate',
			fieldLabel: 'TTS Modified Date',
			readOnly: true
		}]
	}
});