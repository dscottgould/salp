﻿//
//
//  Change Management Ticket Form
//
//

Ext.define('SALP.form.ChangeManagementTicketForm', 
{
	alias: 'widget.form.ChangeManagementTicketForm',
	extend: 'Ext.form.Panel',
	
	height: '100%',
	layout: 'anchor',
	bodyPadding: '10 10 10 10',
  defaults: 
  {
  	anchor: '100%',
    labelWidth: 160
  },
  scrollable: true,
  
  initComponent: function()
  {
		this.items = this.buildItems();
    
    this.listeners = this.buildListeners();
    
    this.callParent(arguments);
  },
  
  buildItems: function()
  {
  	return [
  	{
  		xtype: 'container',
  		cls: 'TicketHeader',
  		html: '<center>Change Management Ticket Detail Report</center>'
  	},
  	{
  		xtype: 'container',
  		layout: 'hbox',
  		items: [
  		{
  			xtype: 'container',
  			flex: 1
  		},
  		{
  			xtype: 'displayfield',
				fieldCls: 'TicketHeader',
				name: 'ticketId'
  		},
  		{
  			xtype: 'container',
  			flex: 1
  		}]
  	},
  	{
  		xtype: 'container',
  		height: 20
  	},
  	{
  		xtype: 'container',
  		cls: 'TicketCategory',
  		html: '&nbsp;General Information'
  	},
  	{
  		xtype: 'container',
  		layout: 'hbox',
  		items: [
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>Change Summary</b>',
				name: 'changeSummary',
				labelWidth: 120,
				flex: 1
  		},
  		{
  			xtype: 'container',
  			width: 10
  		},
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>Status</b>',
				name: 'ndStatus',
				flex: 1
  		}]
  	},
  	{
  		xtype: 'container',
  		layout: 'hbox',
  		items: [
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>Region</b>',
				name: 'regionName',
				labelWidth: 100,
				flex: 1
  		},
  		{
  			xtype: 'container',
  			width: 10
  		},
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>Product</b>',
				name: 'product',
				flex: 1
  		}]
  	},
  	{
  		xtype: 'container',
  		layout: 'hbox',
  		items: [
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>Support Area</b>',
				name: 'supportAreaName',
				flex: 1
  		},
  		{
  			xtype: 'container',
  			width: 10
  		},
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>Video</b>',
				name: 'videoAffected',
				flex: 1
  		}]
  	},
  	{
  		xtype: 'container',
  		layout: 'hbox',
  		items: [
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>Queue</b>',
				name: 'queueName',
				flex: 1
  		},
  		{
  			xtype: 'container',
  			width: 10
  		},
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>HSD</b>',
				name: 'hsdAffected',
				flex: 1
  		}]
  	},
  	{
  		xtype: 'container',
  		layout: 'hbox',
  		items: [
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>Assignee</b>',
				name: 'fullName',
				labelWidth: 160,
				flex: 1
  		},
  		{
  			xtype: 'container',
  			width: 10
  		},
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>Telephony</b>',
				name: 'telephonyAffected',
				flex: 1
  		}]
  	},
  	{
  		xtype: 'container',
  		layout: 'hbox',
  		items: [
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>Assigned Date</b>',
				name: 'assignedDate',
				labelWidth: 140,
				flex: 1
  		},
  		{
  			xtype: 'container',
  			width: 10
  		},
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>Total</b>',
				name: 'totalCustomersAffected',
				labelWidth: 90,
				flex: 1
  		}]
  	},
  	{
  		xtype: 'container',
  		layout: 'hbox',
  		items: [
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>Next Action</b>',
				name: 'nextAction',
				labelWidth: 140,
				flex: 1
  		},
  		{
  			xtype: 'container',
  			width: 10
  		},
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>Maintenance Type</b>',
				name: 'maintenanceType',
				labelWidth: 120,
				flex: 1
  		}]
  	},
  	{
  		xtype: 'container',
  		layout: 'hbox',
  		items: [
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>Planned Start</b>',
				name: 'plannedStart',
				labelWidth: 110,
				flex: 1
  		},
  		{
  			xtype: 'container',
  			width: 10
  		},
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>Planned End</b>',
				name: 'plannedEnd',
				labelWidth: 90,
				flex: 1
  		}]
  	},
  	{
  		xtype: 'container',
  		layout: 'hbox',
  		items: [
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>Brief Description</b>',
				name: 'changeDetail',
				labelWidth: 110,
				flex: 1
  		}]
  	},
  	{
  		xtype: 'container',
  		cls: 'TicketCategory',
  		html: '&nbsp;Categorization'
  	},
  	{
  		xtype: 'container',
  		layout: 'hbox',
  		items: [
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>Project Name</b>',
				name: 'projectName',
				labelWidth: 110,
				flex: 1
  		},
  		{
  			xtype: 'container',
  			width: 10
  		},
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>Impact Level</b>',
				name: 'impactLevel',
				flex: 1
  		}]
  	},
  	{
  		xtype: 'container',
  		layout: 'hbox',
  		items: [
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>Change Category</b>',
				name: 'changeCategory',
				labelWidth: 140,
				flex: 1
  		},
  		{
  			xtype: 'container',
  			width: 10
  		},
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>Indicator City</b>',
				name: 'indicatorCity',
				flex: 1
  		}]
  	},
  	{
  		xtype: 'container',
  		layout: 'hbox',
  		items: [
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>Change Subcategory</b>',
				name: 'changeSubcategory',
				labelWidth: 140,
				flex: 1
  		}]
  	},
  	{
  		xtype: 'container',
  		cls: 'TicketCategory',
  		html: '&nbsp;Contact Details'
  	},
		{
			xtype: 'grid.CMContactDetailsGrid',
  		ticketNumber: this.ticketNumber
		},
  	{
  		xtype: 'container',
  		cls: 'TicketCategory',
  		html: '&nbsp;Attributes'
  	},
  	{
  		xtype: 'grid.CMTicketAttributesGrid',
  		ticketNumber: this.ticketNumber
  	},
  	{
  		xtype: 'container',
  		cls: 'TicketCategory',
  		html: '&nbsp;Steps'
  	},
  	{
  		xtype: 'grid.CMTicketStepsGrid',
  		ticketNumber: this.ticketNumber
  	},
  	{
  		xtype: 'container',
  		cls: 'TicketCategory',
  		html: '&nbsp;Affected Elements'
  	},
  	{
  		xtype: 'grid.CMTicketAffectedElementsGrid',
  		ticketNumber: this.ticketNumber
  	},
  	{
  		xtype: 'container',
  		cls: 'TicketCategory',
  		html: '&nbsp;WorkLog'
  	},
  	{
  		xtype: 'grid.CMTicketWorklogGrid',
  		ticketNumber: this.ticketNumber
  	}]
  },
  

  buildListeners: function()
  {
  	return {
  		afterrender: function()
  		{
  			this.setLoading(true);
  			
  			this.ownerCt.setTitle('Change Management Ticket Details - ' + this.ticketNumber)
  			
  			Ext.Ajax.request(
        {
        	url: '/WebServices/Service.svc/GetCMTicketGeneralDetails',
        	method: 'GET',
        	params:  
          { 
              ticketNumber: this.ticketNumber
          }, 
          scope: this,
        	success: function(response, opts) 
        	{
						var data = Ext.decode(response.responseText).data,
						    ticket = Ext.create('CMTicketGeneralDetails',
    						{
									ticketId: data.ticketId,
									changeSummary: data.changeSummary,
									regionName: data.regionName,
									supportAreaName: data.supportAreaName,
									queueName: data.queueName,
									fullName: data.fullName,
									status: data.status,
									ndStatus: data.ndStatus,
									product: data.product,
									assignedDate: data.assignedDate,
									videoAffected: data.videoAffected,
									hsdAffected: data.hsdAffected,
									telephonyAffected: data.telephonyAffected,
									totalCustomersAffected: data.totalCustomersAffected,
									nextAction: data.nextAction,
									maintenanceType: data.maintenanceType,
									projectName: data.projectName,
									changeCategory: data.changeCategory,
									changeSubcategory: data.changeSubcategory,
									impactLevel: data.impactLevel,
									changeDetail: data.changeDetail,
									plannedStart: data.plannedStart,
									plannedEnd: data.plannedEnd
								});
					
						if(ticket.data.ticketId)
						{
							this.loadRecord(ticket);
						
							this.setLoading(false);
						}
						else
						{
							Ext.toast('Ticket not found.')
								
							this.up().close();
								
							Ext.create('SALP.view.TicketLookupDialog').show();
						}	
        	},
        	failure: function(response, opts) 
        	{
        		var t=1;
        	}
        });
  		},
  		scope: this
  	}
  }
});