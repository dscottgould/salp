﻿//
//
//   Ticket Details B Form
//
//

Ext.define('SALP.form.TicketDetailsBForm', 
{
	alias: 'widget.form.TicketDetailsBForm',
	extend: 'Ext.form.Panel',
	
	height: '100%',
	layout: 'anchor',
	bodyPadding: '10 10 10 10',
	//autoScroll: true,
	frame: false,
	border: false,
  defaults: 
  {
  	anchor: '100%',
    labelWidth: 170
  },
  
  initComponent: function()
  {
		this.items = this.buildItems();
    
    this.callParent(arguments);
  },
	
	
	buildItems: function()
	{
		return [
		{
			xtype: 'container',
			layout: 'hbox',
			items:[
			{
				xtype: 'displayfield',
				value: '<b>Attributes</b>',
				style: 
				{
        	background : '#efefef'
        },
        fieldStyle:
        {
        	'fontSize': '18px'
        },
				flex: 1
			}]
		},
		{
			xtype: 'displayfield',
			name: 'contactInfoDepartment',
			fieldLabel: '<b>Contact Info Department</b>',
			labelWidth: 170
		},
		{
			xtype: 'displayfield',
			name: 'contactInfoPhone',
			fieldLabel: '<b>Contact Info Phone</b>',
			labelWidth: 170
		},
		{
			xtype: 'displayfield',
			name: 'indicatorState',
			fieldLabel: '<b>Indicator State</b>',
			labelWidth: 170
		},
		{
			xtype: 'displayfield',
			name: 'managementBridge',
			fieldLabel: '<b>Management Bridge</b>',
			labelWidth: 170
		},
		{
			xtype: 'displayfield',
			name: 'detectedBy',
			fieldLabel: '<b>Detected By</b>',
			labelWidth: 170
		},
		{
			xtype: 'displayfield',
			name: 'outageDescription',
			fieldLabel: '<b>Outage Description</b>',
			labelWidth: 170
		},
		{
			xtype: 'displayfield',
			name: 'customerExperience',
			fieldLabel: '<b>Customer Experience</b>',
			labelWidth: 170
		},
		{
			xtype: 'displayfield',
			name: 'indicatorCity',
			fieldLabel: '<b>Indicator City</b>',
			labelWidth: 170
		},
		{
			xtype: 'displayfield',
			name: 'technicalBridge',
			fieldLabel: '<b>Technical Bridge</b>',
			labelWidth: 170
		},
		{
			xtype: 'container',
			layout: 'hbox',
			items:[
			{
				xtype: 'displayfield',
				value: '<b>Outage</b>',
				style: 
				{
        	background : '#efefef'
        },
        fieldStyle:
        {
        	'fontSize': '18px'
        },
				flex: 1
			}]
		},
		{
			xtype: 'container',
			html : '<b>Customer Tickets Linked To This Ticket:</b>'
		}]
	}
});
		