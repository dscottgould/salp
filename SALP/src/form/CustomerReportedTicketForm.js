﻿//
//
//  Customer Reported Ticket Form
//
//

Ext.define('SALP.form.CustomerReportedTicketForm', 
{
	alias: 'widget.form.CustomerReportedTicketForm',
	extend: 'Ext.form.Panel',
	
	height: '100%',
	layout: 'anchor',
	bodyPadding: '10 10 10 10',
  defaults: 
  {
  	anchor: '100%',
    labelWidth: 160
  },
  scrollable: true,
  
  initComponent: function()
  {
		this.items = this.buildItems();
    
    this.listeners = this.buildListeners();
    
    this.callParent(arguments);
  },
  
  buildItems: function()
  {
  	return [
  	{
  		xtype: 'container',
  		cls: 'TicketHeader',
  		html: '<center>Customer Reported Ticket Detail Report</center>'
  	},
  	{
  		xtype: 'container',
  		layout: 'hbox',
  		items: [
  		{
  			xtype: 'container',
  			flex: 1
  		},
  		{
  			xtype: 'displayfield',
				fieldCls: 'TicketHeader',
				name: 'ticketId'
  		},
  		{
  			xtype: 'container',
  			flex: 1
  		}]
  	},
  	{
  		xtype: 'container',
  		height: 20
  	},
  	{
  		xtype: 'container',
  		cls: 'TicketCategory',
  		html: '&nbsp;General Information'
  	},
  	{
  		xtype: 'container',
  		layout: 'hbox',
  		items: [
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>Problem Description</b>',
				name: 'problemSummary',
				labelWidth: 140,
				flex: 1
  		},
  		{
  			xtype: 'container',
  			width: 10
  		},
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>Product</b>',
				name: 'product',
				flex: 1
  		}]
  	},
  	{
  		xtype: 'container',
  		layout: 'hbox',
  		items: [
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>Region</b>',
				name: 'regionName',
				labelWidth: 140,
				flex: 1
  		},
  		{
  			xtype: 'container',
  			width: 10
  		},
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>Original Ticket</b>',
				name: 'originalTicket',
				flex: 1
  		}]
  	},
  	{
  		xtype: 'container',
  		layout: 'hbox',
  		items: [
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>Support Area</b>',
				name: 'supportAreaName',
				labelWidth: 140,
				flex: 1
  		},
  		{
  			xtype: 'container',
  			width: 10
  		},
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>Rework Ticket</b>',
				name: 'reworkTicket',
				flex: 1
  		}]
  	},
  	{
  		xtype: 'container',
  		layout: 'hbox',
  		items: [
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>Queue</b>',
				name: 'queueName',
				flex: 1
  		},
  		{
  			xtype: 'container',
  			width: 10
  		},
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>Status</b>',
				name: 'crStatus',
				flex: 1
  		}]
  	},
  	{
  		xtype: 'container',
  		layout: 'hbox',
  		items: [
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>Assigned Date</b>',
				name: 'assignedDate',
				labelWidth: 110,
				flex: 1
  		},
  		{
  			xtype: 'container',
  			width: 10
  		},
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>Closed Date</b>',
				name: 'closedDate',
				flex: 1
  		}]
  	},
  	{
  		xtype: 'container',
  		cls: 'TicketCategory',
  		html: '&nbsp;Categorization'
  	},
  	{
  		xtype: 'container',
  		layout: 'hbox',
  		items: [
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>Problem Category</b>',
				name: 'category',
				labelWidth: 140,
				flex: 1
  		},
  		{
  			xtype: 'container',
  			width: 10
  		},
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>Cause Description</b>',
				name: 'causeDescription',
				labelWidth: 120,
				flex: 1
  		}]
  	},
  	{
  		xtype: 'container',
  		layout: 'hbox',
  		items: [
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>Problem Subcategory</b>',
				name: 'subCategory',
				labelWidth: 140,
				flex: 1
  		},
  		{
  			xtype: 'container',
  			width: 10
  		},
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>Cause Category</b>',
				name: 'causeCategory',
				labelWidth: 120,
				flex: 1
  		}]
  	},
  	{
  		xtype: 'container',
  		layout: 'hbox',
  		items: [
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>Solution Description</b>',
				name: 'solutionDescription',
				labelWidth: 140,
				flex: 1
  		},
  		{
  			xtype: 'container',
  			width: 10
  		},
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>Cause Subcategory</b>',
				name: 'causeSubcategory',
				labelWidth: 130,
				flex: 1
  		}]
  	},
  	{
  		xtype: 'container',
  		layout: 'hbox',
  		items: [
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>Solution Category</b>',
				name: 'solutionCategory',
				labelWidth: 140,
				flex: 1
  		},
  		{
  			xtype: 'container',
  			width: 10
  		},
  		{
  			xtype: 'displayfield',
  			fieldLabel: '',
				name: '',
				labelWidth: 90,
				flex: 1
  		}]
  	},
  	{
  		xtype: 'container',
  		layout: 'hbox',
  		items: [
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>Solution Subcategory</b>',
				name: 'solutionSubcategory',
				labelWidth: 140,
				flex: 1
  		},
  		{
  			xtype: 'container',
  			width: 10
  		},
  		{
  			xtype: 'displayfield',
  			fieldLabel: '',
				name: '',
				labelWidth: 90,
				flex: 1
  		}]
  	},
  	{
  		xtype: 'container',
  		cls: 'TicketCategory',
  		html: '&nbsp;Details'
  	},
  	{
  		xtype: 'container',
  		layout: 'hbox',
  		items: [
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>Trouble Start</b>',
				name: 'troubleStart',
				labelWidth: 100,
				flex: 1
  		},
  		{
  			xtype: 'container',
  			width: 10
  		},
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>TTR SLA</b>',
				name: 'ttrSla',
				flex: 1
  		}]
  	},
  	{
  		xtype: 'container',
  		layout: 'hbox',
  		items: [
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>TTR Start</b>',
				name: 'ttrStart',
				labelWidth: 100,
				flex: 1
  		},
  		{
  			xtype: 'container',
  			width: 10
  		},
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>Indicator City</b>',
				name: 'indicatorCity',
				flex: 1
  		}]
  	},
  	{
  		xtype: 'container',
  		layout: 'hbox',
  		items: [
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>TTR Stop</b>',
				name: 'ttrStop',
				labelWidth: 100,
				flex: 1
  		},
  		{
  			xtype: 'container',
  			width: 10
  		},
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>Indicator State</b>',
				name: 'indicatorState',
				labelWidth: 100,
				flex: 1
  		}]
  	},
  	{
  		xtype: 'container',
  		layout: 'hbox',
  		items: [
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>Contact Name</b>',
				name: 'contactName',
				labelWidth: 100,
				flex: 1
  		},
  		{
  			xtype: 'container',
  			width: 10
  		},
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>Account Number</b>',
				name: 'accountNumber',
				labelWidth: 100,
				flex: 1
  		}]
  	},
  	{
  		xtype: 'container',
  		layout: 'hbox',
  		items: [
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>Contact Phone</b>',
				name: 'contactPhone',
				labelWidth: 100,
				flex: 1
  		},
  		{
  			xtype: 'container',
  			width: 10
  		},
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>Node</b>',
				name: 'node',
				labelWidth: 100,
				flex: 1
  		}]
  	},
  	{
  		xtype: 'container',
  		layout: 'hbox',
  		items: [
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>Contact Email</b>',
				name: 'contactPhone',
				labelWidth: 100,
				flex: 1
  		},
  		{
  			xtype: 'container',
  			width: 10
  		},
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>System ID</b>',
				name: 'systemId',
				labelWidth: 100,
				flex: 1
  		}]
  	},
  	  	{
  		xtype: 'container',
  		layout: 'hbox',
  		items: [
  		{
  			xtype: 'displayfield',
  			fieldLabel: '',
				name: '',
				labelWidth: 80,
				flex: 1
  		},
  		{
  			xtype: 'container',
  			width: 10
  		},
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>System Name</b>',
				name: 'sysNm',
				labelWidth: 100,
				flex: 1
  		}]
  	},
  	{
  		xtype: 'container',
  		cls: 'TicketCategory',
  		html: '&nbsp;Attributes'
  	},
  	{
  		xtype: 'grid.CRTicketAttributesGrid',
  		ticketNumber: this.ticketNumber
  	},
  	{
  		xtype: 'container',
  		cls: 'TicketCategory',
  		html: '&nbsp;WorkLog'
  	},
  	{
  		xtype: 'grid.CRTicketWorklogGrid',
  		ticketNumber: this.ticketNumber
  	}]
  },
  

  buildListeners: function()
  {
  	return {
  		afterrender: function()
  		{
  			this.setLoading(true);
  			
  			this.ownerCt.setTitle('Customer Reported Ticket Details - ' + this.ticketNumber)
  			
  			Ext.Ajax.request(
        {
        	url: '/WebServices/Service.svc/GetCRTicketGeneralDetails',
        	method: 'GET',
        	params:  
          { 
              ticketNumber: this.ticketNumber
          }, 
          scope: this,
        	success: function(response, opts) 
        	{
						var data = Ext.decode(response.responseText).data,
						    ticket = Ext.create('CRTicketGeneralDetails',
    						{
									ticketId: data.ticketId,
									problemSummary: data.problemSummary,
									regionName: data.regionName,
									supportAreaName: data.supportAreaName,
									queueName: data.queueName,
									fullName: data.fullName,
									product: data.product,
									crStatus: data.crStatus,
									assignedDate: data.assignedDate,
									closedDate: data.closedDate,
									originalTicket: data.originalTicket,
									reworkTicket: data.reworkTicket,
									nextAction: data.nextAction,
									problemCode: data.problemCode,
									category: data.category,
									subCategory: data.subCategory,
									causeCode: data.causeCode,
									causeDescription: data.causeDescription,
									causeCategory: data.causeCategory,
									causeSubcategory: data.causeSubcategory,
									solutionCode: data.solutionCode,
									solutionDescription: data.solutionDescription,
									solutionCategory: data.solutionCategory,
									solutionSubcategory: data.solutionSubcategory,
									troubleStart: data.troubleStart,
									ttrStart: data.ttrStart,
									ttrStop: data.ttrStop,
									ttrSla: data.ttrSla,
									indicatorCity: data.indicatorCity,
									indicatorState: data.indicatorState,
									contactName: data.contactName,
									contactPhone: data.contactPhone,
									contactEmail: data.contactEmail,
									customerId: data.customerId,
									accountNumber: data.accountNumber,
									node: data.node,
									systemId: data.systemId,
									networkElement: data.networkElement,
									sysNm: data.sysNm
								});
					
						if(ticket.data.ticketId)
						{
							this.loadRecord(ticket);
						
							this.setLoading(false);
						}
						else
						{
							Ext.toast('Ticket not found.')
								
							this.up().close();
								
							Ext.create('SALP.view.TicketLookupDialog').show();
						}	
        	},
        	failure: function(response, opts) 
        	{
        		var t=1;
        	}
        });
  		},
  		scope: this
  	}
  }
});