﻿//
//
//  Scheduled Maintenance Ticket Form
//
//

Ext.define('SALP.form.ScheduledMaintenanceTicketForm', 
{
	alias: 'widget.form.ScheduledMaintenanceTicketForm',
	extend: 'Ext.form.Panel',
	
	height: '100%',
	layout: 'anchor',
	bodyPadding: '10 10 10 10',
  defaults: 
  {
  	anchor: '100%',
    labelWidth: 160
  },
  scrollable: true,
  
  initComponent: function()
  {
		this.items = this.buildItems();
    
    this.listeners = this.buildListeners();
    
    this.callParent(arguments);
  },
  
  buildItems: function()
  {
  	return [
  	{
  		xtype: 'container',
  		cls: 'TicketHeader',
  		html: '<center>Scheduled Maintenance Ticket Detail Report</center>'
  	},
  	{
  		xtype: 'container',
  		layout: 'hbox',
  		items: [
  		{
  			xtype: 'container',
  			flex: 1
  		},
  		{
  			xtype: 'displayfield',
				fieldCls: 'TicketHeader',
				name: 'ticketId'
  		},
  		{
  			xtype: 'container',
  			flex: 1
  		}]
  	},
  	{
  		xtype: 'container',
  		height: 20
  	},
  	{
  		xtype: 'container',
  		cls: 'TicketCategory',
  		html: '&nbsp;General Information'
  	},
  	{
  		xtype: 'container',
  		layout: 'hbox',
  		items: [
  		{
  			xtype: 'displayfield',
  			cls: 'ticketLink',
  			fieldLabel: '<b>Parent Ticket Id</b>',
				name: 'parentTicketId',
				labelWidth: 120,
				flex: 1
  		},
  		{
  			xtype: 'container',
  			width: 10
  		},
  		{
  			xtype: 'displayfield',
  			fieldLabel: '',
				name: '',
				flex: 1
  		}]
  	},
  	{
  		xtype: 'container',
  		layout: 'hbox',
  		items: [
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>Project Name</b>',
				name: 'projectName',
				flex: 1
  		},
  		{
  			xtype: 'container',
  			width: 10
  		},
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>Product</b>',
				name: 'product',
				flex: 1
  		}]
  	},
  	{
  		xtype: 'container',
  		layout: 'hbox',
  		items: [
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>Region</b>',
				name: 'regionName',
				flex: 1
  		},
  		{
  			xtype: 'container',
  			width: 10
  		},
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>SM Frequency</b>',
				name: 'smFrequency',
				flex: 1
  		}]
  	},
  	{
  		xtype: 'container',
  		layout: 'hbox',
  		items: [
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>Support Area</b>',
				name: 'supportAreaName',
				flex: 1
  		},
  		{
  			xtype: 'container',
  			width: 10
  		},
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>SM Duration</b>',
				name: 'smDuration',
				flex: 1
  		}]
  	},
  	{
  		xtype: 'container',
  		layout: 'hbox',
  		items: [
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>Ticket Type</b>',
				name: 'ticketType',
				flex: 1
  		},
  		{
  			xtype: 'container',
  			width: 10
  		},
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>Video</b>',
				name: 'videoAffected',
				flex: 1
  		}]
  	},
  	{
  		xtype: 'container',
  		layout: 'hbox',
  		items: [
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>Assignee</b>',
				name: 'fullName',
				flex: 1
  		},
  		{
  			xtype: 'container',
  			width: 10
  		},
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>HSD</b>',
				name: 'hsdAffected',
				flex: 1
  		}]
  	},
  	{
  		xtype: 'container',
  		layout: 'hbox',
  		items: [
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>Create Date</b>',
				name: 'createDate',
				flex: 1
  		},
  		{
  			xtype: 'container',
  			width: 10
  		},
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>Telephony</b>',
				name: 'telephonyAffected',
				flex: 1
  		}]
  	},
  	{
  		xtype: 'container',
  		layout: 'hbox',
  		items: [
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>Status</b>',
				name: 'smStatus',
				flex: 1
  		},
  		{
  			xtype: 'container',
  			width: 10
  		},
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>Total</b>',
				name: 'totalCustomersAffected',
				flex: 1
  		}]
  	},
  	{
  		xtype: 'container',
  		cls: 'TicketCategory',
  		html: '&nbsp;Categorization'
  	},
  	{
  		xtype: 'container',
  		layout: 'hbox',
  		items: [
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>Local Mgmt Approver</b>',
				name: 'localManagementApprover',
				labelWidth: 140,
				flex: 1
  		},
  		{
  			xtype: 'container',
  			width: 10
  		},
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>Maintenance Type</b>',
				name: 'maintenanceType',
				labelWidth: 155,
				flex: 1
  		}]
  	},
  	{
  		xtype: 'container',
  		layout: 'hbox',
  		items: [
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>Local Mgmt Phone</b>',
				name: 'localManagementPhone',
				labelWidth: 140,
				flex: 1
  		},
  		{
  			xtype: 'container',
  			width: 10
  		},
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>Maintenance Category</b>',
				name: 'maintenaceCategory',
				labelWidth: 155,
				flex: 1
  		}]
  	},
  	{
  		xtype: 'container',
  		layout: 'hbox',
  		items: [
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>Maintenance Detail</b>',
				name: 'maintenanceDetail',
				labelWidth: 140,
				flex: 1
  		},
  		{
  			xtype: 'container',
  			width: 10
  		},
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>Maintenance Description</b>',
				name: 'maintenanceDescription',
				labelWidth: 155,
				flex: 1
  		}]
  	},
  	{
  		xtype: 'container',
  		cls: 'TicketCategory',
  		html: '&nbsp;Details'
  	},
  	{
  		xtype: 'container',
  		layout: 'hbox',
  		items: [
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>Actual Start</b>',
				name: 'actualStart',
				labelWidth: 140,
				flex: 1
  		},
  		{
  			xtype: 'container',
  			width: 10
  		},
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>Planned Start</b>',
				name: 'plannedStart',
				labelWidth: 100,
				flex: 1
  		}]
  	},
  	{
  		xtype: 'container',
  		layout: 'hbox',
  		items: [
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>Actual End</b>',
				name: 'actualEnd',
				labelWidth: 140,
				flex: 1
  		},
  		{
  			xtype: 'container',
  			width: 10
  		},
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>Planned End</b>',
				name: 'plannedEnd',
				labelWidth: 100,
				flex: 1
  		}]
  	},
  	{
  		xtype: 'container',
  		layout: 'hbox',
  		items: [
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>Resolution Description</b>',
				name: 'resolutionDescription',
				labelWidth: 150,
				flex: 1
  		},
  		{
  			xtype: 'container',
  			width: 10
  		},
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>Indicator City</b>',
				name: 'indicatorCity',
				labelWidth: 100,
				flex: 1
  		}]
  	},
  	{
  		xtype: 'container',
  		layout: 'hbox',
  		items: [
  		{
  			xtype: 'container',
				flex: 1
  		},
  		{
  			xtype: 'container',
  			width: 10
  		},
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>Indicator State</b>',
				name: 'indicatorState',
				labelWidth: 100,
				flex: 1
  		}]
  	},
  	{
  		xtype: 'container',
  		cls: 'TicketCategory',
  		html: '&nbsp;Affected Elements'
  	},
  	{
  		xtype: 'grid.SMTicketAffectedElementsGrid',
  		ticketNumber: this.ticketNumber
  	},
  	{
  		xtype: 'container',
  		cls: 'TicketCategory',
  		html: '&nbsp;WorkLog'
  	},
  	{
  		xtype: 'grid.SMTicketWorklogGrid',
  		ticketNumber: this.ticketNumber
  	}]
  },
  

  buildListeners: function()
  {
  	return {
  		afterrender: function()
  		{
  			parentTicketFeild = this.items.items[4].items.items[0];
  			
  			parentTicketFeild.getEl().on('dblclick', function()
				{
					this.getTicket(parentTicketFeild.getValue());
  			},
  			this);
  			
  			this.setLoading(true);
  			
  			this.ownerCt.setTitle('Scheduled Maintenance Ticket Details - ' + this.ticketNumber)
  			
  			
  			Ext.Ajax.request(
        {
        	url: '/WebServices/Service.svc/GetSMTicketGeneralDetails',
        	method: 'GET',
        	params:  
          { 
              ticketNumber: this.ticketNumber
          }, 
          scope: this,
        	success: function(response, opts) 
        	{
						var data = Ext.decode(response.responseText).data,
						    ticket = Ext.create('SMTicketGeneralDetails',
    						{
									ticketId: data.ticketId,
									parentTicketId: data.parentTicketId,
									projectName: data.projectName,
									regionName: data.regionName,
									ticketType: data.ticketType,
									product: data.product,
									smStatus: data.smStatus,
									createDate: data.createDate,
									nextAction: data.nextAction,
									maintenanceType: data.maintenanceType,
									maintenanceDescription: data.maintenanceDescription,
									maintenaceCategory: data.maintenaceCategory,
									maintenanceDetail: data.maintenanceDetail,
									smFrequency: data.smFrequency,
									videoAffected: data.videoAffected,
									hsdAffected: data.hsdAffected,
									telephonyAffected: data.telephonyAffected,
									totalCustomersAffected: data.totalCustomersAffected,
									communitiesAffected: data.communitiesAffected,
									actualStart: data.actualStart,
									actualEnd: data.actualEnd,
									smDuration: data.smDuration,
									resolutionDescription: data.resolutionDescription,
									indicatorCity: data.indicatorCity,
									indicatorState: data.indicatorState,
									localManagementApprover: data.localManagementApprover,
									localManagementPhone: data.localManagementPhone,
									supportAreaName: data.supportAreaName,
									fullName: data.fullName,
									apCreateDate: data.apCreateDate,
									plannedStart: data.plannedStart,
									plannedEnd: data.plannedEnd
								});
								
						if(ticket.data.ticketId)
						{
							this.loadRecord(ticket);
						
							this.setLoading(false);
						}
						else
						{
							Ext.toast('Ticket not found.')
								
							this.up().close();
								
							Ext.create('SALP.view.TicketLookupDialog').show();
						}	
					
        	},
        	failure: function(response, opts) 
        	{
        		var t=1;
        	}
        });
  		},
  		scope: this
  	}
  },
  getTicket: function(ticketId)
  {
	  Ext.create('SALP.view.MultiTicketWindow',
  	{
  		ticketNumber: ticketId
  	}).show();
  }
});