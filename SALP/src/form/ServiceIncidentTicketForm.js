﻿//
//
//  Service Incident Ticket Form
//
//

Ext.define('SALP.form.ServiceIncidentTicketForm', 
{
	alias: 'widget.form.ServiceIncidentTicketForm',
	extend: 'Ext.form.Panel',
	
	height: '100%',
	layout: 'anchor',
	bodyPadding: '10 10 10 10',
  defaults: 
  {
  	anchor: '100%',
    labelWidth: 160
  },
  scrollable: true,
  
  initComponent: function()
  {
		this.items = this.buildItems();
    
    this.listeners = this.buildListeners();
    
    this.callParent(arguments);
  },
  
  buildItems: function()
  {
  	return [
  	{
  		xtype: 'container',
  		cls: 'TicketHeader',
  		html: '<center>Service Incident Ticket Detail Report</center>'
  	},
  	{
  		xtype: 'container',
  		layout: 'hbox',
  		items: [
  		{
  			xtype: 'container',
  			flex: 1
  		},
  		{
  			xtype: 'displayfield',
				fieldCls: 'TicketHeader',
				name: 'ticketId'
  		},
  		{
  			xtype: 'container',
  			flex: 1
  		}]
  	},
  	{
  		xtype: 'container',
  		height: 20
  	},
  	{
  		xtype: 'chart.SITicketTimelineChart',
  		height: 220
  	},
  	{
  		xtype: 'container',
  		cls: 'TicketCategory',
  		layout: 'hbox',
  		items: [
  		{
  			xtype: 'container',
  			cls: 'TicketCategory',
  			html: '&nbsp;General Information',
				flex: 1
  		},
  		{
  			xtype: 'container',
  			width: 10
  		},
  		{
  			xtype: 'displayfield'
  		}]
  	},
  	{
  		xtype: 'chart.SIEventFunnelChart',
  		ticketId: this.ticketNumber,
  		height: 220
  	},
  	{
  		xtype: 'container',
  		layout: 'hbox',
  		items: [
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>Problem Description</b>',
				name: 'incidentSummary',
				labelWidth: 140,
				flex: 1
  		},
  		{
  			xtype: 'container',
  			width: 10
  		},
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>Status</b>',
				name: 'siStatus',
				flex: 1
  		}]
  	},
  	{
  		xtype: 'container',
  		layout: 'hbox',
  		items: [
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>Region</b>',
				name: 'regionName',
				labelWidth: 140,
				flex: 1
  		},
  		{
  			xtype: 'container',
  			width: 10
  		},
  		{
  			xtype: 'displayfield',
  			fieldLabel: '',
				name: '',
				flex: 1
  		}]
  	},
  	{
  		xtype: 'container',
  		layout: 'hbox',
  		items: [
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>Support Area</b>',
				name: 'supportAreaName',
				flex: 1
  		},
  		{
  			xtype: 'container',
  			width: 10
  		},
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>Product</b>',
				name: 'product',
				flex: 1
  		}]
  	},
  	{
  		xtype: 'container',
  		layout: 'hbox',
  		items: [
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>Queue</b>',
				name: 'queueName',
				flex: 1
  		},
  		{
  			xtype: 'container',
  			width: 10
  		},
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>Video</b>',
				name: 'videoAffected',
				labelWidth: 110,
				flex: 1
  		}]
  	},
  	{
  		xtype: 'container',
  		layout: 'hbox',
  		items: [
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>Assigned Date</b>',
				name: 'assignedDate',
				labelWidth: 120,
				flex: 1
  		},
  		{
  			xtype: 'container',
  			width: 10
  		},
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>HSD</b>',
				name: 'hsdAffected',
				labelWidth: 110,
				flex: 1
  		}]
  	},
  	{
  		xtype: 'container',
  		layout: 'hbox',
  		items: [
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>Create Date</b>',
				name: 'createDate',
				labelWidth: 120,
				flex: 1
  		},
  		{
  			xtype: 'container',
  			width: 10
  		},
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>Telephony</b>',
				name: 'telephonyAffected',
				labelWidth: 110,
				flex: 1
  		}]
  	},
  	{
  		xtype: 'container',
  		layout: 'hbox',
  		items: [
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>Service Condition</b>',
				name: 'serviceCondition',
				labelWidth: 140,
				flex: 1
  		},
  		{
  			xtype: 'container',
  			width: 10
  		},
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>Total</b>',
				name: 'totalCustomersAffected',
				labelWidth: 110,
				flex: 1
  		}]
  	},
  	{
  		xtype: 'container',
  		cls: 'TicketCategory',
  		html: '&nbsp;Categorization'
  	},
  	{
  		xtype: 'container',
  		layout: 'hbox',
  		items: [
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>Problem Category</b>',
				name: 'category',
				labelWidth: 150,
				flex: 1
  		},
  		{
  			xtype: 'container',
  			width: 10
  		},
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>Cause Description</b>',
				name: 'causeDescription',
				labelWidth: 120,
				flex: 1
  		}]
  	},
  	{
  		xtype: 'container',
  		layout: 'hbox',
  		items: [
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>Problem Subcategory</b>',
				name: 'subCategory',
				labelWidth: 150,
				flex: 1
  		},
  		{
  			xtype: 'container',
  			width: 10
  		},
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>Cause Category</b>',
				name: 'causeCategory',
				flex: 1
  		}]
  	},
  	{
			xtype: 'container',
			layout: 'hbox',
			items: [
			{
				xtype: 'displayfield',
				name: 'solutionDescription',
				fieldLabel: '<b>Solution Description</b>',
				labelWidth: 150,
				flex: 1
			}]
		},
		{
  		xtype: 'container',
  		layout: 'hbox',
  		items: [
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>Solution Category</b>',
				name: 'solutionCategory',
				labelWidth: 140,
				flex: 1
  		},
  		{
  			xtype: 'container',
  			width: 10
  		},
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>External Assignee</b>',
				name: 'externalAssignee',
				labelWidth: 120,
				flex: 1
  		}]
  	},
  	{
  		xtype: 'container',
  		layout: 'hbox',
  		items: [
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>Solution Subcategory</b>',
				name: 'solutionSubCategory',
				labelWidth: 140,
				flex: 1
  		},
  		{
  			xtype: 'container',
  			width: 10
  		},
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>External Reference</b>',
				name: 'externalReference',
				labelWidth: 120,
				flex: 1
  		}]
  	},
  	{
  		xtype: 'container',
  		cls: 'TicketCategory',
  		html: '&nbsp;Details'
  	},
  	{
  		xtype: 'container',
  		layout: 'hbox',
  		items: [
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>Alarm Start</b>',
				name: 'alarmDate',
				labelWidth: 120,
				flex: 1
  		},
  		{
  			xtype: 'container',
  			width: 10
  		},
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>Indicator City</b>',
				name: 'indicatorCity',
				flex: 1
  		}]
  	},
  	{
  		xtype: 'container',
  		layout: 'hbox',
  		items: [
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>TTR Start</b>',
				name: 'ttrStart',
				labelWidth: 120,
				flex: 1
  		},
  		{
  			xtype: 'container',
  			width: 10
  		},
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>Indicator State</b>',
				name: 'indicatorState',
				flex: 1
  		}]
  	},
  	{
  		xtype: 'container',
  		layout: 'hbox',
  		items: [
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>TTR Stop</b>',
				name: 'ttrStop',
				labelWidth: 120,
				flex: 1
  		},
  		{
  			xtype: 'container',
  			width: 10
  		},
  		{
  			xtype: 'displayfield',
  			fieldLabel: '<b>Source</b>',
				name: 'source',
				flex: 1
  		}]
  	},
  	{
  		xtype: 'container',
  		cls: 'TicketCategory',
  		html: '&nbsp;Attributes'
  	},
  	{
  		xtype: 'grid.SITicketAttributesGrid',
  		ticketNumber: this.ticketNumber
  	},
  	{
  		xtype: 'container',
  		cls: 'TicketCategory',
  		html: '&nbsp;Outage'
  	},
  	{
  		xtype: 'grid.SITicketCustomerTicketsGrid',
  		ticketNumber: this.ticketNumber
  	},
  	{
  		xtype: 'container',
  		cls: 'TicketCategory',
  		html: '&nbsp;Affected Elements'
  	},
  	{
  		xtype: 'grid.SITicketAffectedElementsGrid',
  		ticketNumber: this.ticketNumber
  	},
  	{
  		xtype: 'container',
  		cls: 'TicketCategory',
  		html: '&nbsp;WorkLog'
  	},
  	{
  		xtype: 'grid.SITicketWorklogGrid',
  		ticketNumber: this.ticketNumber
  	}]
  },
  

  buildListeners: function()
  {
  	return {
  		afterrender: function()
  		{
  			this.setLoading(true);
  			
  			this.ownerCt.setTitle('Service Incident Ticket Details - ' + this.ticketNumber)
  			
  			Ext.Ajax.request(
        {
        	url: '/WebServices/Service.svc/GetSITicketGeneralDetails',
        	method: 'GET',
        	params:  
          { 
              ticketNumber: this.ticketNumber
          }, 
          scope: this,
        	success: function(response, opts) 
        	{
						var data = Ext.decode(response.responseText).data,
								chart = this.items.items[3],
								sevDisplayField = this.items.items[4].items.items[2],
						    ticket = Ext.create('SITicketGeneralDetails',
    						{
									ticketId: data.ticketId,
									incidentSummary: data.incidentSummary,
									supportAreaName: data.supportAreaName,
									submittingName: data.submittingName,
									queueName: data.queueName,
									fullName: data.fullName,
									siSeverity: data.siSeverity,
									siStatus: data.siStatus,
									siPriority: data.siPriority,
									priority: data.priority,
									product: data.product,
									createDate: data.createDate,
									assignedDate: data.assignedDate,
									videoAffected: data.videoAffected,
									hsdAffected: data.hsdAffected,
									telephonyAffected: data.telephonyAffected,
									totalCustomersAffected: data.totalCustomersAffected,
									nextAction: data.nextAction,
									problemCode: data.problemCode,
									source: data.source,
									category: data.category,
									subCategory: data.subCategory,
									causeCode: data.causeCode,
									causeDescription: data.causeDescription,
									causeCategory: data.causeCategory,
									causeSubCategory: data.causeSubCategory,
									solutionCode: data.solutionCode,
									solutionDescription: data.solutionDescription,
									solutionCategory: data.solutionCategory,
									solutionSubCategory: data.solutionSubCategory,
									externalAssignee: data.externalAssignee,
									externalReference: data.externalReference,
									alarmDate: data.alarmDate,
									ttrStart: data.ttrStart,
									ttrStop: data.ttrStop,
									indicatorCity: data.indicatorCity,
									indicatorState: data.indicatorState,
									networkElement: data.networkElement,
									correlationType: data.correlationType,
									serviceCondition: data.serviceCondition,
									careImpact: data.careImpact,
									estimatedServiceRestore: data.estimatedServiceRestore,
									siLastModifiedBy: data.siLastModifiedBy,
									lastModifiedBy: data.lastModifiedBy, 
									modifiedDate: data.modifiedDate,
									siResolvedBy: data.siResolvedBy, 
									resolvedBy: data.resolvedBy,
									contactDepartment: data.contactDepartment, 
									contactPhone: data.contactPhone,
									regionName: data.regionName,
									duration: data.duration,
									detectedBy: data.detectedBy,
									cicGroupId: data.cicGroupId
								});
					
						if(ticket.data.ticketId)
						{
  						this.loadRecord(ticket);
  											
  						sevDisplayField.setHtml('&nbsp;&nbsp;&nbsp;&nbsp;' + data.siSeverity + '&nbsp;&nbsp;&nbsp;&nbsp;');
  						
    					switch(data.siSeverity)
            	{
            		case 'Informational':
            			sevDisplayField.setStyle('background', '#00FF80');
            			sevDisplayField.setFieldStyle('color: #000000');
            			break;
            		
            		case 'Sev 1':
            			sevDisplayField.setStyle('background', '#FF0000');
            			sevDisplayField.setFieldStyle('color: #FFFFFF');
            			break;
            		
            		case 'Sev 2':
            			sevDisplayField.setStyle('background', '#FF3333');
            			sevDisplayField.setFieldStyle('color: #FFFFFF');
            			break;	
            			
            		case 'Sev 3':
            			sevDisplayField.setStyle('background', '#FF6666');
            			sevDisplayField.setFieldStyle('color: #000000');
            			break;
            			
            		case 'Sev 4':
            		  sevDisplayField.setStyle('background', '#FF9999');
            			sevDisplayField.setFieldStyle('color: #000000');
            			break;
            			
            		case 'Sev 5':
            		  sevDisplayField.setStyle('background', '#FFCCCC');
            			sevDisplayField.setFieldStyle('color: #000000');
            			break;
            	}

      				chart.store.load
            	({
            		params:
            		{
            			ticketNumber: this.ticketNumber
            		},
            		callback : function(records, options, success) 
            		{
                  if (success) 
                  {
            				chart.redraw();
                  }
                }
            	});
						}
						else
						{
								Ext.toast('Ticket not found.')
								
								this.up().close();
								
								Ext.create('SALP.view.TicketLookupDialog').show();
						}
					
						this.setLoading(false);
        	},
        	failure: function(response, opts) 
        	{
        		var t=1;
        	}
        });
  		},
  		scope: this
  	}
  }
});