﻿//
//
//  New Ticket Form
//
//

Ext.define('SALP.form.NewTicketForm', 
{
	alias: 'widget.form.newticketform',
	extend: 'Ext.form.Panel',
	
	height: '100%',
	layout: 'anchor',
	bodyPadding: '10 10 10 10',
  defaults: 
  {
  	anchor: '100%',
    labelWidth: 160
  },
  
  initComponent: function()
  {
		this.items = this.buildItems();
    
    this.callParent(arguments);
  },
	
	
	buildItems: function()
	{
		return [
		{
			xtype: 'textfield',
			name: 'ticketId',
			fieldLabel: 'Ticket Number',
			hidden: true
		},
		{
			xtype: 'textfield',
			name: 'smId',
			fieldLabel: 'SM ID'
		},
		{
			xtype: 'textfield',
			name: 'adjDurationMinutes',
			fieldLabel: 'Adjusted Duration (Minutes)'
		},
		{
			xtype: 'combobox',
			name: 'causeDescription',
			fieldLabel: 'Cause Description',
			store: SALP.data.CauseDecriptions,
			queryMode: 'local',
			displayField: 'displayName',
			valueField: 'submitValue'
		},
		{
			xtype: 'textareafield',
			name: 'solutionDescription',
			fieldLabel: 'Solution Description'
		},
		{
			xtype: 'combobox',
			name: 'severity',
			fieldLabel: 'Severity',
			store: SALP.data.Severity,
			queryMode: 'local',
			displayField: 'displayName',
			valueField: 'submitValue'
		},
		{
			xtype: 'container',
			html: '<span style="color:red; float:right; font-weight:bold">NOTE: All Times Should be entered in Mountain Time.</span>'
		},
		{
			xtype: 'container',
			layout: 'hbox',
			padding: '0 0 5 0',
			items: [
			{
				xtype: 'datefield',
				name: 'actualStartDate',
				fieldLabel: 'Actual Start Date/Time',
				labelWidth: 160,
				flex: 1
			},
			{
				xtype: 'timefield',
				name: 'actualStartTime',
				width: 90
			}]
		},
		{
			xtype: 'container',
			layout: 'hbox',
			padding: '0 0 5 0',
			items: [
			{
				xtype: 'datefield',
				name: 'alarmStartDate',
				fieldLabel: 'Alarm Start',
				labelWidth: 160,
				flex: 1
			},
			{
				xtype: 'timefield',
				name: 'alarmStartTime',
				width: 90
			}]
		},
		{
			xtype: 'container',
			layout: 'hbox',
			padding: '0 0 5 0',
			items: [
			{
				xtype: 'datefield',
				name: 'workingStartDate',
				fieldLabel: 'Working Start Date/Time',
				labelWidth: 160,
				flex: 1
			},
			{
				xtype: 'timefield',
				name: 'workingStartTime',
				width: 90
			}]
		},
		{
			xtype: 'container',
			layout: 'hbox',
			padding: '0 0 5 0',
			items: [
			{
				xtype: 'datefield',
				name: 'resolvedDate',
				fieldLabel: 'Resolved Date/Time',
				labelWidth: 160,
				flex: 1
			},
			{
				xtype: 'timefield',
				name: 'resolvedTime',
				width: 90
			}]
		},
		{
			xtype: 'container',
			layout: 'hbox',
			padding: '0 0 5 0',
			items: [
			{
				xtype: 'datefield',
				name: 'actualEndDate',
				fieldLabel: 'Actual End Date/Time',
				labelWidth: 160,
				flex: 1
			},
			{
				xtype: 'timefield',
				name: 'actualEndTime',
				width: 90
			}]
		},
		{
			xtype: 'textareafield',
			name: 'changeReason',
			fieldLabel: 'Change Reason',
			rows: 6
		},
		{
			xtype: 'container',
			height: 25,
			html : '<hr />'
		},
		{
			xtype: 'displayfield',
			name: 'modifiedDate',
			fieldLabel: 'Band-Aid Modified Date',
			readOnly: true
		}]
	}
});