﻿//
//
//  Incident Form
//
//

Ext.define('SALP.form.IncidentTicketAForm', 
{
	alias: 'widget.form.IncidentTicketAForm',
	extend: 'Ext.form.Panel',
	
	height: '100%',
	layout: 'anchor',
	bodyPadding: '10 10 10 10',
	bodyStyle:{"background-color":"#E0E0E0"}, 
  defaults: 
  {
  	anchor: '100%',
    labelWidth: 160,
   	readOnly: true
  },
  
  initComponent: function()
  {
		this.items = this.buildItems();
    
    this.callParent(arguments);
  },
  
	buildItems: function()
	{
		return [
		{
			xtype: 'datefield',
			name: 'incidentWeek',
			fieldLabel: 'Incident Week',
			format: 'Y-m-d'
		},
		{
			xtype: 'textfield',
			name: 'lensGroup',
			fieldLabel: 'Lens Group'
		},
		{
			xtype: 'textfield',
			name: 'rootCauseGroup',
			fieldLabel: 'Root Cause Group'
		},
		{
			xtype: 'textfield',
			name: 'networkLens',
			fieldLabel: 'Network Lens'
		},
		{
			xtype: 'textfield',
			name: 'causeCategory',
			fieldLabel: 'Cause Category'
		},
		{
			xtype: 'textfield',
			name: 'causeDescription',
			fieldLabel: 'Cause Description'
		},
		{
			xtype: 'textfield',
			name: 'solutionCategory',
			fieldLabel: 'Solution Category'
		},
		{
			xtype: 'textfield',
			name: 'solutionDescription',
			fieldLabel: 'Solution Description'
		},
		{
			xtype: 'textfield',
			name: 'problemCategory',
			fieldLabel: 'Problem Category'
		},
		{
			xtype: 'textfield',
			name: 'problemSummary',
			fieldLabel: 'Problem Summary',
		},
		{
			xtype: 'textareafield',
			name: 'fixemIncidentSummary',
			fieldLabel: 'Fixem Incident Summary',
			height: 170
		},
		{
			xtype: 'container',
			html: '<hr/>'
		},
		{
			xtype: 'textfield',
			name: 'actualStartDate',
			fieldLabel: 'Actual Start Date'
		},
		{
			xtype: 'textfield',
			name: 'alarmStartDate',
			fieldLabel: 'Alarm Start Date'
		},
		{
			xtype: 'textfield',
			name: 'workingDate',
			fieldLabel: 'Working Date'
		},
		{
			xtype: 'textfield',
			name: 'resolvedDate',
			fieldLabel: 'Resolved Date'
		}]
//		{
//			xtype: 'container',
//			html: '<hr/>'
//		},
//		{
//			xtype: 'displayfield',
//			name: 'detectedBy',
//			fieldLabel: 'Detected By'
//		},
//		{
//			xtype: 'displayfield',
//			name: 'source',
//			fieldLabel: 'Source'
//		},
//		{
//			xtype: 'container',
//			html: '<hr/>'
//		}]
	}
});