﻿//
//
//   Network Quality Network Lens Portlet
//
//



Ext.define('SALP.portlets.NetworkQualityNetworkLensPortlet', 
{
  extend: 'Ext.panel.Panel',
  
  layout: 'fit',
  height: 262,

  props:
  {
  	title: 'Network Quality - Network Lens'
  },
  
  
  initComponent: function()
  {
  	this.items = this.buildItems();
  	this.listeners = this.buildListeners();
  	
  	this.callParent(arguments);
  },
  
  
  buildItems: function()
  {
  	return [
  	{
  		xtype: 'image',
    	src: '/img/screencapture/NetworkQuality-Network_Lens.png'
  	}]
  },
  buildListeners: function()
  {
  	var listener =     	
  			{
  				element  : 'el',
					scope: this,
					click: function() 
					{
						SALP.openSite('http://tableau.cable.comcast.com/#/site/InfrastructureServices/views/NetworkQualityIncidents/NetworkQualityIncidents?:iid=1');
					}
  			}
  	
  	return listener
 	}
});