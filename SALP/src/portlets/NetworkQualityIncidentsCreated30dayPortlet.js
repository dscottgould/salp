﻿//
//
//  Network Quality Incidents Created 30 day
//
//



Ext.define('SALP.portlets.NetworkQualityIncidentsCreated30dayPortlet', 
{
  extend: 'Ext.panel.Panel',
  
  layout: 'fit',
  height: 262,

  props:
  {
  	title: 'Network Quality Incidents Created 30 Day'
  },
  
  
  initComponent: function()
  {
  	this.items = this.buildItems();
  	this.listeners = this.buildListeners();
  	
  	this.callParent(arguments);
  },
  
  
  buildItems: function()
  {
  	return [
  	{
  		xtype: 'image',
    	src: '/img/screencapture/Network_Quality_Incidents_Created_30_day.png'
  	}]
  },
  buildListeners: function()
  {
  	var listener =     	
  			{
  				element  : 'el',
					scope: this,
					click: function() 
					{
						SALP.openSite('http://tableau/#/site/InfrastructureServices/views/NetworkQualityIncidents/NetworkQualityIncidentsbyDaySALP?:iid=1');
					}
  			}
  	
  	return listener
 	}
});