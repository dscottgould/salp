﻿//
//
//   Core Weekly Summary
//
//



Ext.define('SALP.portlets.CoreWeeklySummaryPortlet', 
{

    extend: 'Ext.panel.Panel',
    alias: 'widget.coreweeklysummary',

    props:
    {
    	title: 'Core Incidents-Weekly Summary'
    },
    
    
    initComponent: function()
    {
    	Ext.apply(this,
			{
      	layout: 'fit',
        height: 262,
        //html: 'TEST',
        items:[
        {
        	xtype: 'image',
        	src: '/img/screencapture/Core_Weekly_Summary_sm.png'
        }],
				listeners: 
				{
					element  : 'el',
					scope: this,
					click: function() 
					{
						SALP.openSite('http://tableau.cable.comcast.com/#/site/InfrastructureServices/views/SI_CORE_Dash/IncidentsSummary?:iid=1');
					}
				}
			});
    	
    	
    	this.callParent(arguments);
    }
});