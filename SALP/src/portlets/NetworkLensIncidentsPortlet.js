﻿//
//
//   Network Lens Incidents Portlet
//
//



Ext.define('SALP.portlets.NetworkLensIncidentsPortlet', 
{
  extend: 'Ext.panel.Panel',
  
  layout: 'vbox',
  height: 262,
  bodyStyle: "background: #ffffff url(img/magnifying-glass.jpg) no-repeat center center !important",

  props:
  {
  	title: 'Network Quality - Incidents'
  },
  
  
  initComponent: function()
  {
  	this.items = this.buildItems();
  	
  	this.callParent(arguments);
  },
  
  
  buildItems: function()
  {
  	return [
  	{
  		xtype: 'container',
  		height: 80
  	},
		{
			id: 'IncidentNumberField',
			xtype: 'textfield',
			fieldLabel: 'Ticket #',
			labelWidth: 60,
			width: 175,
			padding: '10 10 10 10',
			listeners: 
     	{
      	specialkey: function(field, event)
      	{
        	if (event.getKey() == event.ENTER) 
        	{
						this.getTicket();
          }
        },
        scope: this
      }
			//value: 'SI017450930'
		},
		{
			xtype: 'container',
			layout: 'hbox',
			items: [
			{
				xtype: 'container',
				width: 25
			},
			{
				xtype: 'button',
        text: 'Show Incident List',
        listeners: 
				{
        	click: function(button, event, object) 
          {
          	SALP.displayIncidentList();
          },
          scope: this
        }
			},
			{
				xtype: 'container',
				width: 25
			},
			{
				xtype: 'button',
        text: 'Get Ticket',
        listeners: 
				{
        	click: function(button, event, object) 
          {
          	this.getTicket();
          },
          scope: this
        }
			}]
		}]
  },
  getTicket: function()
  {
  	var ticketNumber = Ext.getCmp('IncidentNumberField').getValue();
  	
  	if(ticketNumber.length > 0)
  	{
  		SALP.getIncidentTicket(ticketNumber);
  	}
  	else
		{
			Ext.toast('Please Enter Ticket Number.');
		}
  }	
});