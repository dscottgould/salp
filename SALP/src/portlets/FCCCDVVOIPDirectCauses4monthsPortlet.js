//
//
//   FCC CDV VOIP Direct Causes 4 months Portlet
//
//



Ext.define('SALP.portlets.FCCCDVVOIPDirectCauses4monthsPortlet', 
{
  extend: 'Ext.panel.Panel',

  layout: 'fit',
  height: 262,

  props:
  {
  	title: 'FCC Reporting CDV/VOIP Direct Causes Past 4 Months'
  },
  
  
  initComponent: function()
  {
  	this.items = this.buildItems();
  	this.listeners = this.buildListeners();  	
  	
  	this.callParent(arguments);
  },
  
  
  buildItems: function()
  {
  	return [
  	{
    	xtype: 'image',
      src: 'http://epssaweb-po-01p/dashboard/img/screencapture/FCC_CDV_VOIP_Direct_Causes_4_months_sm.png'
    }]
  },
  buildListeners: function()
  {
  	var listener =     	
  			{
  				element  : 'el',
					scope: this,
					click: function() 
					{
						SALP.openSite('http://obivrs-wc-01p/Reports/Pages/Report.aspx?ItemPath=%2fPortal%2fFCC+Potential+vs+Reported');
					}
  			}
  	
  	return listener
 	}
});