//
//
//   CR Tickets 30 Day SMC Portlet
//
//



Ext.define('SALP.portlets.CRTickets30DaySMCPortlet', 
{
  extend: 'Ext.panel.Panel',
  
  layout: 'fit',
  height: 262,

  props:
  {
  	title: 'CR Tickets 30 Day- SMC'
  },
  
  
  initComponent: function()
  {
  	this.items = this.buildItems();
    this.listeners = this.buildListeners();
  	
  	this.callParent(arguments);
  },
  
  buildItems: function()
  {
  	return [
  	{
    	xtype: 'image',
      src: 'http://epssaweb-po-01p/dashboard/img/screencapture/CR_Tickets_30_Day_SMC_sm.png'
    }]
  },
  buildListeners: function()
  {
  	var listener =     	
  			{
  				element  : 'el',
					scope: this,
					click: function() 
					{
						SALP.openSite('http://epssabi-wc-01p:9502/analytics/saw.dll?Answers&path=%2Fshared%2FCPE%2FAnalysis%2FTicket%20Count%20by%20Vendor%2FDaily%20Ticketed%20Device%20Count%20rolling%2030-SMC&nQUser=!OBIRead&nQPassword=!OB1R3@d');
					}
  			}
  	
  	return listener
 	}
});