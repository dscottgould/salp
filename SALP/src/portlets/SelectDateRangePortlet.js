﻿//
//
//   Select Date Range Portlet
//
//



Ext.define('SALP.portlets.SelectDateRangePortlet', 
{
  extend: 'Ext.panel.Panel',
  
  layout: 'fit',
  height: 262,

  props:
  {
  	title: 'Haystack - Select Date Range'
  },
  
  
  initComponent: function()
  {
  	this.items = this.buildItems();
  	
  	this.callParent(arguments);
  },
  
  
  buildItems: function()
  {
  	return [
  	{
  		xtype: 'view.selectdaterangepanel'
  	}]
  }
});