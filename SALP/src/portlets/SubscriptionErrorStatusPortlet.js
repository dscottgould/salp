﻿//
//
//   SubscriptionErrorStatusPortlet.js
//
//



Ext.define('SALP.portlets.SubscriptionErrorStatusPortlet', 
{
  extend: 'Ext.panel.Panel',
  
  layout: 'fit',
  height: 262,
  scrollable: true,

  props:
  {
  	title: 'SSRS Subscription Error Status'
  },
  
  
  initComponent: function()
  {
  	this.items = this.buildItems();
  	
  	this.callParent(arguments);
  },
  
  
  buildItems: function()
  {
  	return [
  	{
			xtype: 'chart.SubscriptionErrorStatusChart'
  	}]
  }
});