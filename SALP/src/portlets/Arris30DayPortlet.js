//
//
//   Arris 30Day Portlet
//
//



Ext.define('SALP.portlets.Arris30DayPortlet', 
{
  extend: 'Ext.panel.Panel',
  
  layout: 'fit',
  height: 262,

  props:
  {
  	title: 'CR 30 Day Tickets- Arris'
  },
  
  
  initComponent: function()
  {
  	this.items = this.buildItems();
  	this.listeners = this.buildListeners();
  	
  	this.callParent(arguments);
  },
  
  
  buildItems: function()
  {
  	return [
  	{
  		xtype: 'image',
    	src: '/img/screencapture/CR_Tickets_30_Day_Arris_sm.png'
  	}]
  },
  buildListeners: function()
  {
  	var listener =     	
  			{
  				element  : 'el',
					scope: this,
					click: function() 
					{
						SALP.openSite('http://epssabi-wc-01p:9502/analytics/saw.dll?Answers&path=%2Fshared%2FCPE%2FAnalysis%2FTicket%20Count%20by%20Vendor%2FDaily%20Ticketed%20Device%20Count%20rolling%2030-Arris&nQUser=!OBIRead&nQPassword=!OB1R3@d');
					}
  			}
  	
  	return listener
 	}
});