﻿//
//
//   Application & Enterprise Fiber Cuts Portlet
//
//



Ext.define('SALP.portlets.ApplicationEnterpriseFiberCutsPortlet', 
{
  extend: 'Ext.panel.Panel',
  
  layout: 'fit',
  height: 262,
  scrollable: true,

  props:
  {
  	title: 'Incident Review - Application & Enterprise - Fiber Cuts'
  },
  
  
  initComponent: function()
  {
  	this.items = this.buildItems();
  	
  	this.callParent(arguments);
  },
  
  
  buildItems: function()
  {
  	return [
  	{
  		xtype: 'chart.ApplicationEnterpriseFiberCutsChart'
  	}]
  }
});