﻿//
//
//   LongRunningJobsPortlet.js
//
//



Ext.define('SALP.portlets.LongRunningJobsPortlet', 
{
  extend: 'Ext.panel.Panel',
  
  layout: 'fit',
  height: 262,
  scrollable: true,

  props:
  {
  	title: 'Long Runnning Jobs: obidb-wc-p01',
  	server: 'obidb-wc-p01'  
  },
  
  
  initComponent: function()
  {
  	this.items = this.buildItems();
  	
  	this.callParent(arguments);
  },
  
  
  buildItems: function()
  {
  	return [
  	{
			xtype: 'grid.LongRunningJobsGrid',
			server: this.props.server 
  	}]
  }
});