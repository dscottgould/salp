﻿
//
// Haystack Calls Portlet
//
//


Ext.define('SALP.portlets.HaystackTicketsPortlet', 
{
    extend: 'Ext.panel.Panel',
    alias: 'widget.haystackticketsportlet',

    props:
    {
    	title: 'Haystack Tickets'
    },
    
    
    
    initComponent: function()
    {
			this.model =  Ext.define('HaystackTickets', 
			{
				extend: 'Ext.data.Model',
  			fields: [
  				{name: 'baseline', type: 'int'},
  				{name: 'baseline1', type: 'int'},
  				{name: 'baseline2', type: 'int'},
  				{name: 'baseline3', type: 'int'},
  				{name: 'baseline4', type: 'int'},
  				{name: 'baseline5', type: 'int'},
  				{name: 'count', type: 'int'},
  				{name: 'startDate', type: 'date'},
  				{name: 'endDate', type: 'date'}
			]});
    	
			this.store = Ext.create('Ext.data.Store', 
			{
				autoLoad: false,
  			model: this.model,
  			proxy: 
  			{
  				type: 'ajax',
    			url: 'WebServices/Service.svc/GetHaystackTickets',
    			pageParam: undefined,
   				startParam: undefined,
   				limitParam: undefined,
    			reader: 
    			{
    				type: 'json',
      			root: 'data'
    			}
  			}
			});
    	
    	
    	Ext.apply(this, 
    	{
				layout: 'fit',
        height: 262,
        items: 
        {
						xtype: 'chart',
            store: this.store,
            axes: [
            {
            	type: 'numeric',
              position: 'left',
              fields: ['count', 'baseline'],
              grid: true,
              label:
              {
              	renderer: Ext.util.Format.numberRenderer('0,0')
             	}
            },
            {
        			type: 'category',
        			position: 'bottom',
        			label: 
              {
              	rotate: 
              	{
                	degrees: 270
              	}
          		},
          		renderer: function(chart, date, object) 
          		{
          			var oneDay = 24*60*60*1000,
          			    firstDate = new Date(chart.config.chart.getStore().getAt(0).data.startDate),
										secondDate = new Date(chart.config.chart.getStore().getAt(chart.config.chart.getStore().getTotalCount() - 1).data.endDate),
										
										diffDays = Math.round(Math.abs((firstDate.getTime() - secondDate.getTime())/(oneDay))),
          			    format = Ext.util.Format.date(date,'h:i');
          			
          			if(diffDays >= 2)
          			{
          				format = Ext.util.Format.date(date, 'n/j');
          			}
          			
          			
          			return format;
          		}
    				}],
            series: [
            {
            	//type: 'column',
             	type: 'bar',
             	axis: 'left',
              xField: 'startDate',
              yField: 'count',
              tips: 
              {
              	trackMouse: true,
                width: 100,
                height: 20,
                renderer: function(toolTip, store, object) 
                {
                	toolTip.setTitle('Call Count: ' + store.data.count);
                }
              },
              label: 
              {
              	display: 'insideEnd',
                'text-anchor': 'middle',
                field: 'data1',
                renderer: Ext.util.Format.numberRenderer('0'),
                orientation: 'vertical',
                color: '#333'
              },
              renderer: function(sprite, record, attr, index, store)
              {
     						return Ext.apply(attr, 
     						{
        					fill: 'rgb(255, 179, 0)'
     						});
  						},
  						style:
  						{
                lineWidth: 5,
                maxBarWidth: 30,
                stroke: '#FFB300',
                opacity: 0.6
           		}
            },
            {
        			type: 'line',
          		axis: 'left',
          		xField: 'startDate',
          		yField: 'baseline',
          		style: 
          		{
            		'stroke-width': 4,
              	stroke: 'rgb(128, 128, 128)'
            	},
            	tips: 
          		{
          			trackMouse: true,
           	 		width: 120,
           	 		height: 20,
           	 		renderer: function(toolTip, store, object) 
           	 		{
           	 			var currrentValue = store.data.count;
           	 	
           	 			toolTip.setTitle('Baseline: '+ currrentValue);
           	 		}
         	 	}
       	 	}]
				},
				listeners: 
				{
					element  : 'el',
					scope: this,
					click: function() 
					{
						SALP.openSite('http://haystack');
					}
				}
			});
			    	
			this.store.load();    	
		
			
			this.task = new Ext.util.DelayedTask(function()
			{
    		this.setLoading('Loading...');
    		
    		this.store.reload();
    	
    		Ext.defer(function() 
        {
        	this.setLoading(false);
        }, 2000, this);
        
        this.down().redraw();
    	
    		this.task.delay(900000);
    		
			}, this);
		
			this.task.delay(900000);
			    	
    	this.callParent(arguments);
    }
});
    