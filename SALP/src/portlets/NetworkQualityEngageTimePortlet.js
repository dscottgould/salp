﻿//
//
//   Network Quality Engage Time Portlet
//
//



Ext.define('SALP.portlets.NetworkQualityEngageTimePortlet', 
{
  extend: 'Ext.panel.Panel',
  
  layout: 'fit',
  height: 262,

  props:
  {
  	title: 'Network Quality-Time to Engage'
  },
  
  
  initComponent: function()
  {
  	this.items = this.buildItems();
  	this.listeners = this.buildListeners();
  	
  	this.callParent(arguments);
  },
  
  
  buildItems: function()
  {
  	return [
  	{
  		xtype: 'image',
    	src: '/img/screencapture/NetworkQuality_Engage_Time.png'
  	}]
  },
  buildListeners: function()
  {
  	var listener =     	
  			{
  				element  : 'el',
					scope: this,
					click: function() 
					{
						SALP.openSite('http://tableau.cable.comcast.com/#/site/InfrastructureServices/views/NetworkQualityDashboard/NetworkKPIs?:iid=1');
					}
  			}
  	
  	return listener
 	}
});