﻿//
//
//   Maintenance Resolve Status
//
//



Ext.define('SALP.portlets.MaintenanceResolveStatusPortlet', 
{
  extend: 'Ext.panel.Panel',
  
  layout: 'fit',
  height: 262,

  props:
  {
  	title: 'Maintenance Resolve Status'
  },
  
  
  initComponent: function()
  {
  	this.items = this.buildItems();
  	
  	this.callParent(arguments);
  },
  
  
  buildItems: function()
  {
		return [
		{
			xtype: 'grid.MaintenanceResolveStatusGrid'
		}]
  }
});