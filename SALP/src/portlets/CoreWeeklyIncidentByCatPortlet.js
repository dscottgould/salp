﻿//
//
//   Core Weekly Incident By Cat Portlet
//
//



Ext.define('SALP.portlets.CoreWeeklyIncidentByCatPortlet', 
{
  extend: 'Ext.panel.Panel',
  
 	layout: 'fit',
  height: 262,

  props:
  {
  	title: 'Core Incidents-Weekly Incident by Category'
  },
  
  
  initComponent: function()
  {
  	this.items = this.buildItems();
  	this.listeners = this.buildListeners();
  	
  	this.callParent(arguments);
  },
  
  
  buildItems: function()
  {
  	return [
  	{
    	xtype: 'image',
      src: '/img/screencapture/Core_Weekly_Incident_By_Cat_sm.png'
    }]
  },
  buildListeners: function()
  {
  	var listener =     	
  			{
  				element  : 'el',
					scope: this,
					click: function() 
					{
						SALP.openSite('http://tableau.cable.comcast.com/#/site/InfrastructureServices/views/SI_CORE_Dash/IncidentsSummary?:iid=1');
					}
  			}
  	
  	return listener
 	}
});