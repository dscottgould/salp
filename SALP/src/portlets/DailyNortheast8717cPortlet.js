//
//
// Daily Northeast 8717c Portlet
//
//



Ext.define('SALP.portlets.DailyNortheast8717cPortlet', 
{

    extend: 'Ext.panel.Panel',
    
    props:
    {
    	title: 'Daily Northeast 8717c'
    },
    
    
    initComponent: function()
    {
    	Ext.apply(this,
			{
      	layout: 'fit',
        height: 262,
        //html: 'TEST',
        items:[
        {
        	xtype: 'image',
        	src: 'http://epssaweb-po-01p/dashboard/img/screencapture/daily_northeast_8717c.png'
        }],
				listeners: 
				{
					element  : 'el',
					scope: this,
					click: function() 
					{
						SALP.openSite('http://epssabi-wc-01p:9502/analytics/saw.dll?Answers&Path=%2Fshared%2FCPE%2FAnalysis%2FTicket%20Count%20By%20Problem%20Code%2FTC8717C%20Ticket%20Count%20By%20Overall%20Problem%20Code_Northeast%20Division');
					}
				}
			});
    	
    	
    	this.callParent(arguments);
    }
});