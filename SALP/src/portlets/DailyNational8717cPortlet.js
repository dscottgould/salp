//
//
//  Daily National 8717c Portlet.
//
//



Ext.define('SALP.portlets.DailyNational8717cPortlet', 
{

    extend: 'Ext.panel.Panel',
    
    props:
    {
    	title: 'Daily National 8717c'
    },
    
    
    initComponent: function()
    {
    	Ext.apply(this,
			{
      	layout: 'fit',
        height: 262,
        //html: 'TEST',
        items:[
        {
        	xtype: 'image',
        	src: 'http://epssaweb-po-01p/dashboard/img/screencapture/daily_national_8717c.png'
        }],
				listeners: 
				{
					element  : 'el',
					scope: this,
					click: function() 
					{
						SALP.openSite('http://epssabi-wc-01p:9502/analytics/saw.dll?Answers&Path=%2Fshared%2FCPE%2FAnalysis%2FTicket%20Count%20By%20Problem%20Code%2FDPC3941T%20Ticket%20Count%20By%20Overall%20Problem%20Code_West%20Division');
					}
				}
			});
    	
    	
    	this.callParent(arguments);
    }
});