﻿//
//
//   CRTicketStatusPortlet.js
//
//



Ext.define('SALP.portlets.CRTicketStatusPortlet', 
{
  extend: 'Ext.panel.Panel',
  
  layout: 'fit',
  height: 262,
  scrollable: true,

  props:
  {
  	title: 'CR Ticket Status'
  },
  
  
  initComponent: function()
  {
  	this.items = this.buildItems();
  	
  	this.callParent(arguments);
  },
  
  
  buildItems: function()
  {
  	return [
  	{
			xtype: 'chart.CRTicketStatusChart'
  	}]
  }
});