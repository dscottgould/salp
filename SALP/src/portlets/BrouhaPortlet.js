//
//
//   Brouha Portlet
//
//



Ext.define('SALP.portlets.BrouhaPortlet', 
{
    extend: 'Ext.grid.Panel',
    alias: 'widget.brouhaportlet',
    height: 262,
    
    props:
    {
    	title: 'Brouha Events'
    },
    
    store: SALP.data.Brouha.store,
    stripeRows: true,
    columnLines: true,
    
    initComponent: function()
    {
			this.columns = this.buildColumns()
 		
    	this.callParent(arguments);
    },
    buildColumns: function()
    {
    	return [
      {
          id:'id',
          text: 'ID',
          width: 55,
          sortable : true,
          dataIndex: 'id',
          renderer : function(value)
          {
          	var t=1;
          	
          	return "<a href='javascript:void(0)' onclick='SALP.brouhaLink(" + value + ");'> " + value + "</a>";
          }
      },
      {
          text   : 'Title',
          flex: 1,
          sortable : true,
          dataIndex: 'title',
          renderer: function(value, metaData, record, rowIdx, colIdx, store) 
          {
  					metaData.tdAttr = 'data-qtip="' + value + '"';
  					
  					return value;
					}
      },
      {
          text   : 'Severity',
          width    : 50,
          sortable : true,
          dataIndex: 'severity'
      },
      {
          text   : 'Date Created',
          width    : 75,
          sortable : true,
          dataIndex: 'createDate',
          renderer: Ext.util.Format.dateRenderer('m-d-Y')
      }]
    }
});
