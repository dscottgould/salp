﻿//
//
//   FCCThresholdBreachedPortlet.js
//
//



Ext.define('SALP.portlets.FCCThresholdBreachedPortlet', 
{
  extend: 'Ext.panel.Panel',
  
  layout: 'fit',
  height: 262,
  scrollable: true,

  props:
  {
  	title: 'FCC Threshold Breached'
  },
  
  
  initComponent: function()
  {
  	this.items = this.buildItems();
  	
  	this.callParent(arguments);
  },
  
  
  buildItems: function()
  {
  	return [
  	{
  		xtype: 'grid.FCCThresholdBreachedGrid'
  	}]
  }
});