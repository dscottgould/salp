﻿//
//
//   Application & Enterprise IP Portlet
//
//



Ext.define('SALP.portlets.ApplicationEnterpriseIPPortlet', 
{
  extend: 'Ext.panel.Panel',
  
  layout: 'fit',
  height: 262,
  scrollable: true,

  props:
  {
  	title: 'Incident Review - Application & Enterprise IP(Switching/Routing)'
  },
  
  
  initComponent: function()
  {
  	this.items = this.buildItems();
  	
  	this.callParent(arguments);
  },
  
  
  buildItems: function()
  {
  	return [
  	{
  		xtype: 'chart.ApplicationEnterpriseIPChart'
  	}]
  }
});