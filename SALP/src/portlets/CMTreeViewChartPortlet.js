﻿//
//
//   CM TreeView Chart Portlet
//
//



Ext.define('SALP.portlets.CMTreeViewChartPortlet', 
{
  extend: 'Ext.panel.Panel',
  
  layout: 'fit',
  height: 262,

  props:
  {
  	title: 'CM TreeView Chart'
  },
  
  
  initComponent: function()
  {
  	this.items = this.buildItems();
  	this.listeners = this.buildListeners();
  	
  	this.callParent(arguments);
  },
  
  
  buildItems: function()
  {
  	var d3Chart = Ext.create('SALP.d3.OrganizationTreeMap')
  	
//  		nodeValue: function(node)
//    	{
//    		return node.data.value;
//    	});
    	
    	return d3Chart;
  	
//  	return [
//  	{
//			xtype: 'd3.OrganizationTreeMap',
//    	nodeValue: function(node)
//    	{
//    		return node.data.value;
//    	}
//  	}]
  },
  
  buildListeners: function()
  {
  	return {
  		'afterrender': function(object, options)
  		{
  			var chart = this.items.items[0],
  					selectedUser = Ext.util.Cookies.get('TreeMapUserName');
  			

  			chart.getStore().load();
  			
  			this.setLoading(true);
  			
  			Ext.Function.defer(function()
  			{
  				chart.getStore().load
        	({
        		params:
        		{
      				name: selectedUser
      				//month: month, 
      				//year: year
        		},
        		callback : function(records, options, success) 
        		{
      				this.setLoading(false);
            }, 
            timeout: 120000,
            scope: this
        	});
  			}, 5000, this);
  		}
  	}
  },
});