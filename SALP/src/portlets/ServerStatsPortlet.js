﻿//
//
//   Server Stats Portlet
//
//



Ext.define('SALP.portlets.ServerStatsPortlet', 
{
  extend: 'Ext.panel.Panel',
  
  layout: 'fit',
  height: 262,

  props:
  {
  	title: 'Server Stats'
  },
  
  
  initComponent: function()
  {
  	this.items = this.buildItems();
  	
  	this.callParent(arguments);
  },
  
  
  buildItems: function()
  {
  	return [
  	{
  		xtype: 'grid.ServerStatsGrid'
  	}]
  }
});