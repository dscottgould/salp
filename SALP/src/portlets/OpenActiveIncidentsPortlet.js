﻿//
//
//   Open/Active Incidents Portlet
//
//



Ext.define('SALP.portlets.OpenActiveIncidentsPortlet', 
{
  extend: 'Ext.panel.Panel',
  
  layout: 'fit',
  height: 262,

  props:
  {
  	title: 'Open/Active Incidents'
  },
  
  
  initComponent: function()
  {
  	this.items = this.buildItems();
  	
  	this.callParent(arguments);
  },
  
  
  buildItems: function()
  {
  	return [
		{
			xtype: 'grid.OpenActiveIncidentsGrid'
		}]
  }
});