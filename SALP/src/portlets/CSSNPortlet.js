//
//
//  CSSN Portlet
//
//



Ext.define('SALP.portlets.CSSNPortlet', 
{
  extend: 'Ext.panel.Panel',

  layout: 'fit',
  height: 262,
      
  props:
  {
  	title: 'National SM Reschedules'
  },
  
  
  initComponent: function()
  {
  	this.items = this.buildItems();
   	this.listeners = this.buildListeners();
  	
  	this.callParent(arguments);
  },
  
  
  buildItems: function()
  {
  	return [
  	{
    	xtype: 'image',
      src: 'http://epssaweb-po-01p/dashboard/img/screencapture/CCS_Notif.png'
    }]
  },
  buildListeners: function()
  {
  	var listener =     	
  			{
  				element  : 'el',
					scope: this,
					click: function() 
					{
						SALP.openSite('http://tableau.cable.comcast.com/#/site/InfrastructureServices/views/CCSNotificationsDailyReport/CCSNotificationsbyDay?:iid=1');
					}
  			}
  	
  	return listener
 	}
});