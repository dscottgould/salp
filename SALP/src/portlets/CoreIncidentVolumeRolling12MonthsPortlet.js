﻿//
//
//   Core Incident Volume Rolling 12 Months Portlet
//
//



Ext.define('SALP.portlets.CoreIncidentVolumeRolling12MonthsPortlet', 
{
  extend: 'Ext.panel.Panel',
  
  layout: 'fit',
  height: 262,
  
  props:
  {
  	title: 'Core Incidents-Volume Rolling 12 Months'
  },
  
  
  initComponent: function()
  {
  	this.items = this.buildItems();
  	this.listeners = this.buildListeners();
  	
  	this.callParent(arguments);
  },
  
  buildItems: function()
  {
  	return [
    {
    	xtype: 'image',
      src: '/img/screencapture/Core_Incident_Volume_Rolling_12_Months_sm.png'
    }]
  },
  buildListeners: function()
  {
  	var listener = 
  			{
					element  : 'el',
					scope: this,
					click: function() 
					{
						SALP.openSite('http://tableau.cable.comcast.com/#/site/InfrastructureServices/views/SI_CORE_Dash/IncidentsSummary?:iid=1');
					}
				}
				
		return listener
  }
});