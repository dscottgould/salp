﻿//
//
//   Cran & Backbone Fiber Cuts Portlet
//
//



Ext.define('SALP.portlets.CranBackboneFiberCutsPortlet', 
{
  extend: 'Ext.panel.Panel',
  
  layout: 'fit',
  height: 262,
  scrollable: true,

  props:
  {
  	title: 'Incidient Review - Cran & Backbone Fiber Cuts'
  },
  
  
  initComponent: function()
  {
  	this.items = this.buildItems();
  	
  	this.callParent(arguments);
  },
  
  
  buildItems: function()
  {
  	return [
  	{
  		xtype: 'chart.CRANBackboneFiberCutsChart'
  	}]
  }
});