﻿//
//
//   Incident Review Portlet
//
//



Ext.define('SALP.portlets.IncidentReviewPortlet', 
{
  extend: 'Ext.panel.Panel',
  
  layout: 'fit',
  height: 262,
  scrollable: true,

  props:
  {
  	title: 'Network Quality Incident Review'
  },
  
  
  initComponent: function()
  {
  	this.items = this.buildItems();
  	
  	this.callParent(arguments);
  },
  
  
  buildItems: function()
  {
  	return [
  	{
  		xtype: 'grid.IncidentReviewGrid'
  	}]
  }
});