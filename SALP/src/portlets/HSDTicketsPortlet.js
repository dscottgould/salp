/**
 * 
 * HSDTicketsPortlet
 * 
 */



Ext.define('SALP.portlets.HSDTicketsPortlet', 
{
  extend: 'Ext.panel.Panel',
  alias: 'widget.hsdticketsportlet',

  requires: ['Ext.chart.*'],
  
  layout: 'fit',
  height: 262,
  
  props:
  {
  	title: 'HSD Tickets (LIVE)'
  },
  
  
  initComponent: function()
  {
  	
		this.model =  Ext.define('HSD', 
		{
			extend: 'Ext.data.Model',
			fields: [
				{name: 'date', type: 'date'},
				{name: 'apps', type: 'int'},
				{name: 'connect', type: 'int'},
				{name: 'cpe', type: 'int'},
				{name: 'degradedConectivity', type: 'int'},
				{name: 'generalInquiry', type: 'int'},
				{name: 'homeNetwork', type: 'int'},
				{name: 'noConectivity', type: 'int'},
				{name: 'callCount', type: 'int'}
		]});
  	
	 	this.store = Ext.create('Ext.data.Store', 
		{
   		autoLoad: true,
   		model: this.model,
   		proxy: 
   		{
      	type: 'ajax',
       	url: 'WebServices/Service.svc/GetHSDMetrics',
       	pageParam: undefined,
 				startParam: undefined,
 				limitParam: undefined,
       	reader: 
       	{
        	type: 'json',
          root: 'data'
       	}
   		}
		});
  	
		this.items = this.buildItems();

  	this.callParent(arguments);
  },
  buildItems: function()
  {
  	return [
  	{
			xtype:'chart',
			animate: true,
			store: this.store,
			axes: [
    	{
      	type: 'numeric',
        position: 'left',
        fields: ['apps', 'connect', 'cpe', 'degradedConectivity','generalInquiry','homeNetwork','noConectivity', 'callCount'],
        title: 'Ticket Count',
        grid: true
    	},
    	{
        type: 'category',
        position: 'bottom',
        fields: ['date'],
        label: 
        {
        	rotate: 
        	{
          	degrees: 270
        	}
    		},
    		renderer: function(chart, date, object) 
    		{
    			return Ext.util.Format.date(date,'M d');
    		},
    	}],
    	series: [
    	{
    		type: 'bar',
      	axis: 'left',
       	xField: 'date',
      	yField: ['apps', 'connect', 'cpe', 'degradedConectivity','generalInquiry','homeNetwork','noConectivity'],
      	title: ['Apps/Tools/Sites', 'Connect', 'CPE', 'Degraded Connectivity', 'General Inquiry', 'Home Network','No Connectivity'],
      	stacked: true,
      	style: 
      	{
    			minGapWidth: 1
				},
      	tips: 
      	{
      		trackMouse: true,
        	width: 200,
        	height: 20,
        	renderer: function(toolTip, item, object) 
        	{
        		var currentType = object.field,
        				currentDate = Ext.Date.format(item.data.date, 'M d');
        				currentValue = item.data;
          	
         		switch(currentType)
         		{
         			case 'apps':
         				toolTip.setTitle(currentDate + ' - Apps/Tools/Sites: ' + currentValue.apps);
         				break;
         			case 'connect':
         				toolTip.setTitle(currentDate + ' - Connect: '+ currentValue.connect);
         				break;
         			case 'cpe':
         				toolTip.setTitle(currentDate + ' - CPE: ' + currentValue.cpe);
         				break;
         			case 'degradedConectivity':
         				toolTip.setTitle(currentDate + ' - Degraded Connectivity: ' + currentValue.degradedConectivity);
         				break;
         			case 'generalInquiry':
         				toolTip.setTitle(currentDate + ' - General Inquiry: '+ currentValue.generalInquiry);
         				break;
         			case 'homeNetwork':
         				toolTip.setTitle(currentDate + ' - Home Network: ' + currentValue.homeNetwork);
         				break;
         			case 'noConectivity':
         				toolTip.setTitle(currentDate + ' - No Connectivity: ' + currentValue.noConectivity);
         				break;
          	}
          }
        }
   		},
   		{
    		type: 'line',
      	axis: 'left',
      	xField: 'date',
      	yField: 'callCount',
      	smooth: true,
      	style: 
      	{
        	'stroke-width': 2,
          stroke: 'rgb(65, 65, 65)'
        },
        tips: 
      	{
      		trackMouse: true,
       	 	width: 120,
       	 	height: 20,
       	 	renderer: function(toolTip, item, object) 
       	 	{
       	 		var currrentValue = item.data.callCount;
       	 	
       	 		toolTip.setTitle('Call Count: '+ currrentValue);
       	 	}
     	 	}
   	 	}]
		}]
  }
});