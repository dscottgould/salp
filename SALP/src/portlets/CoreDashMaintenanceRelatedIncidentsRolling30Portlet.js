﻿//
//
//   Core Dash Maintenance Related Incidents Rolling 30 Portlet
//
//



Ext.define('SALP.portlets.CoreDashMaintenanceRelatedIncidentsRolling30Portlet', 
{
    extend: 'Ext.panel.Panel',

    layout: 'fit',
    height: 262,

    props:
    {
    	title: 'Core Dash-Maintenance Related Incidents Rolling 30 Days'
    },
    
    
    initComponent: function()
    {
    	this.items = this.buildItems();
			this.listeners = this.buildListeners();
    	
    	this.callParent(arguments);
    },
    
    
    buildItems: function()
    {
    	return [
      {
        xtype: 'image',
      	src: '/img/screencapture/Core_Dash_Maintenance_Related_Incidents_Rolling_30.png'
      }]
    },
    buildListeners: function()
    {
    	var listener =
    			{
						element  : 'el',
						scope: this,
						click: function() 
						{
							SALP.openSite('http://tableau.cable.comcast.com/#/site/InfrastructureServices/views/SI_CORE_Dash/IncidentsRolling30Days?:iid=1');
						}
					}
					
			return listener;
    }
});