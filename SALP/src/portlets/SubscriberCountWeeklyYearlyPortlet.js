//
//
//   SubscriberCount Weekly/Yearly
//
//



Ext.define('SALP.portlets.SubscriberCountWeeklyYearlyPortlet', 
{
    extend: 'Ext.panel.Panel',

    layout: 'fit',
    height: 262,

    props:
    {
    	title: 'Weekly/Yearly Subscriber Changes'
    },
    
    
    initComponent: function()
    {
    	this.items = this.buildItems();
  		this.listeners = this.buildListeners();
    	
    	this.callParent(arguments);
    },
    
    
    buildItems: function()
    {
    	return [
    	{
      	xtype: 'image',
        src: '/img/screencapture/Subscriber_Count_OBIEE_sm.png'
      }]
    },
    buildListeners: function()
  	{
  		var listener =     	
  		{
  			element  : 'el',
				scope: this,
				click: function() 
				{
					SALP.openSite('http://epssabi-wc-01p:9502/analytics/saw.dll?Dashboard&PortalPath=%2Fshared%2FNTO_BI%2F_portal%2FTicket%20Counts&Page=Subscriber%20Counts%20by%20LOB&nQUser=!OBIRead&nQPassword=!OB1R3@d');
				}
  		}
  	
  		return listener
 		}
});