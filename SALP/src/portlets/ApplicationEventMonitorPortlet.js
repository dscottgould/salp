﻿//
//
//   ApplicationEventMonitorPortlet.js
//
//



Ext.define('SALP.portlets.ApplicationEventMonitorPortlet', 
{
  extend: 'Ext.panel.Panel',
  
	layout: 
	{
  	type: 'vbox',
    align: 'stretch',
    pack: 'center'
	},
  height: 262,

  props:
  {
  	title: 'Application Event Monitor'
  },
  
  
  initComponent: function()
  {
  	this.items = this.buildItems();
  	
  	this.callParent(arguments);
  },
  
  
  buildItems: function()
  {
  	return [
  	{
  		xtype: 'chart.ApplicationEventMonitor01Chart',
			flex: 1
  	},
  	{
  		xtype: 'chart.ApplicationEventMonitor02Chart',
			flex: 1
  	},
  	{
  		xtype: 'chart.ApplicationEventMonitor03Chart',
			flex: 1
  	},
  	{
  		xtype: 'chart.ApplicationEventMonitor04Chart',
			flex: 1
  	}]
  }
});