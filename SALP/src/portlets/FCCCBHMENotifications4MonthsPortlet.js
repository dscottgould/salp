//
//
//  FCC CBH ME Notifications 4Months Portlet
//
//



Ext.define('SALP.portlets.FCCCBHMENotifications4MonthsPortlet', 
{

    extend: 'Ext.panel.Panel',
    alias: 'widget.FCCCBHMENotifications4MonthsPortlet',

    props:
    {
    	title: 'FCC Reporting CBH/ME Notifications Past 4 Months'
    },
    
    
    initComponent: function()
    {
    	Ext.apply(this,
			{
      	layout: 'fit',
        height: 262,
        //html: 'TEST',
        items:[
        {
        	xtype: 'image',
        	src: 'http://epssaweb-po-01p/dashboard/img/screencapture/FCC_CBH_ME_Notifications_4_Months_sm.png'
        }],
				listeners: 
				{
					element  : 'el',
					scope: this,
					click: function() 
					{
						SALP.openSite('http://obivrs-wc-01p/Reports/Pages/Report.aspx?ItemPath=%2fPortal%2fFCC+Potential+vs+Reported');
					}
				}
			});
    	
    	
    	this.callParent(arguments);
    }
});