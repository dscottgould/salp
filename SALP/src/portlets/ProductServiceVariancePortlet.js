//
//
//   ProductS ervice Variance Portlet
//
//



Ext.define('SALP.portlets.ProductServiceVariancePortlet', 
{
  extend: 'Ext.panel.Panel',

  layout: 'fit',
  height: 262,

  props:
  {
  	title: 'Product/Service Customer Contact Rate'
  },
  
  
  initComponent: function()
  {
  	this.items = this.buildItems();
  	this.listeners = this.buildListeners();
  	
  	this.callParent(arguments);
  },
  
  
  buildItems: function()
  {
  	return [
  	{
    	xtype: 'image',
      src: 'http://epssaweb-po-01p/dashboard/img/screencapture/Product_Service_Variance_sm.png'
    }]
  },
  buildListeners: function()
  {
  	var listener =     	
  			{
  				element  : 'el',
					scope: this,
					click: function() 
					{
						SALP.openSite('http://xnocobi/ReportServer/Pages/ReportViewer.aspx?%2fDeploy%2fService_Rhythm%2fService_Rhythm_YOY-MOM_Variance&rs%3AParameterLanguage=en-US&rc:Section=2');
					}
  			}
  	
  	return listener
 	}
});