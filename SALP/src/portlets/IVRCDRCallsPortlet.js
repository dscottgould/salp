﻿//
//
//   IVR CDR Calls Portlet
//
//



Ext.define('SALP.portlets.IVRCDRCallsPortlet', 
{
  extend: 'Ext.panel.Panel',
  
  layout: 'fit',
  height: 262,
  scrollable: true,

  props:
  {
  	title: 'IVR CDR Calls'
  },
  
  
  initComponent: function()
  {
  	this.items = this.buildItems();
  	
  	this.callParent(arguments);
  },
  
  
  buildItems: function()
  {
  	return [
  	{
  		xtype: 'chart.IVRCDRCallsChart'
  	}]
  }
});