﻿//
//
//   Core Incidents by Category Trend Porlet
//
//

Ext.define('SALP.portlets.CoreIncidentsbyCategoryTrendPorlet', 
{
    extend: 'Ext.panel.Panel',

    layout: 'fit',
    height: 262,

    props:
    {
    	title: 'Core Incidents by Category Trend'
    },
    
    
    initComponent: function()
    {
    	this.items = this.buildItems();
    	this.listeners = this.buildListeners();
    	
    	this.callParent(arguments);
    },
    
    buildItems: function()
    {
    	return [
      {
      	xtype: 'image',
        src: '/img/screencapture/Core_Incidents_by_Category_Trend_sm.png'
      }]
    },
    buildListeners: function()
    {
    	var listener = 
    			{
						element  : 'el',
						scope: this,
						click: function() 
						{
							SALP.openSite('http://tableau.cable.comcast.com/#/site/InfrastructureServices/views/SI_CORE_Dash/IncidentsSummary?:iid=1');
						}
					}
					
			return listener
    }
});