﻿//
//
//   Application & Enterprise Facilities Portlet
//
//



Ext.define('SALP.portlets.ApplicationEnterpriseFacilitiesPortlet', 
{
  extend: 'Ext.panel.Panel',
  
  layout: 'fit',
  height: 262,
  scrollable: true,

  props:
  {
  	title: 'Incident Review - Application & Enterprise - Facilities/Transport/Power'
  },
  
  
  initComponent: function()
  {
  	this.items = this.buildItems();
  	
  	this.callParent(arguments);
  },
  
  
  buildItems: function()
  {
  	return [
  	{
  		xtype: 'chart.ApplicationEnterpriseFacilitiesChart'
  	}]
  }
});