﻿//
//
//   Favorite Links Portlet
//
//



Ext.define('SALP.portlets.FavoriteLinksPortlet', 
{
  extend: 'Ext.panel.Panel',
  
  layout: 'fit',
  height: 262,

  props:
  {
  	title: 'Favorite Links'
  },
  
  
  initComponent: function()
  {
  	this.items = this.buildItems();
  	
  	this.callParent(arguments);
  },
  
  
  buildItems: function()
  {
  	return [
  	{
			xtype: 'grid.FavoriteLinksGrid'
  	}]
  }
});