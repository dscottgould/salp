﻿//
//
//   Haystack Breakdown Truck Rolls Portlet
//
//



Ext.define('SALP.portlets.HaystackBreakdownTruckRollsPortlet', 
{
  extend: 'Ext.panel.Panel',
  
  layout: 'fit',
  height: 262,
  scrollable: true,

  props:
  {
  	title: 'Haystack Breakdown- Truck Rolls'
  },
  
  
  initComponent: function()
  {
  	this.tbar = this.buildToolbar();
  	
  	this.items = this.buildItems();
  	
  	this.listeners = this.buildListeners();
  	
  	this.callParent(arguments);
  },
  
  buildToolbar: function()
  {
  	return [
  	{
  		xtype: 'container',
  		flex: 1
  	},
  	{
  		xtype: 'checkbox',
  		checked: false,
  		boxLabel: '-Show Others',
  		listeners:
  		{
  			scope: this,
  			'change' : function(checkbox, value)
  			{
  				this.loadStore();
  			}
  		}
  	},
  	{
  		xtype: 'container',
  		width: 10
  	},
  	{
  		xtype: 'combobox',
  		fieldLabel: 'Breakdown',
  		labelWidth: 75,
  		store: Ext.data.StoreManager.lookup('selectBreakdown'),
  		queryMode: 'local',
    	displayField: 'displayName',
    	valueField: 'submitValue',
    	value: 'level1',
    	listeners: 
    	{
    		scope: this,
    		'select': function(comboBox, value)
    		{
					this.loadStore();
    		}
    	}
  	}]
  	
  },
  
  buildItems: function()
  {
  	return [
  	{
  		//html: 'HERE'
  		xtype: 'chart.HaystackBreakdownTruckRollsChart'
  	}]
  },
  
  buildListeners: function()
  {
  	return {
  		afterrender: function()
  		{
				this.dockedItems.items[0].setStyle({background: '#FF8300'});
				
				this.loadStore();
  		},
  		scope: this
  	}
  },
  
  loadStore: function()
  {
  	var chart = this.items.items[0],
  			breakdown = this.dockedItems.items[0].items.items[3].getValue()
  			showOthers = this.dockedItems.items[0].items.items[1].getValue();
  			
  	chart.filters.breakdown = breakdown;
  	chart.filters.showOthers = showOthers;
		
		chart.getStore().clearData();
		chart.series[0].clearSprites();
		
		chart.setLoading(true);
		
  	chart.store.load
  	({
  		params:
  		{
  			breakdown: chart.filters.breakdown,
  			segment: chart.filters.segment,
  			filter: chart.filters.filter,
  			username: chart.filters.username,
  			password: chart.filters.password,
  			showOthers: chart.filters.showOthers,
  			fromDate: chart.filters.fromDate,
  			toDate: chart.filters.toDate
  		},
  		callback : function(records, options, success) 
  		{
  			var chart = this.items.items[0];
  			
  			chart.series[0].clearSprites()
  			
  			if(records.length > 0)
  			{
  				chart.axes[0].setFields(records[0].data.fields)
					chart.series[0].setYField(records[0].data.fields);
	     		
					chart.series[0].setTitle(records[0].data.fieldNames);
  			}
	
				chart.setColors(["#94ae0a", "#115fa6", "#a61120", "#ff8809", "#ffd13e", "#a61187", "#24ad9a", "#7c7474", "#a66111"])
  			
  			chart.setLoading(false);
  			
  			//this.up().setTitle(this.props.title);
  			
        if (success) 
        {
  				chart.redraw();
        }
      },
      scope: this
  	});
  }
});