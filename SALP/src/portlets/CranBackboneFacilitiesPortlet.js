﻿//
//
//   Cran & Backbone Facilities Portlet
//
//



Ext.define('SALP.portlets.CranBackboneFacilitiesPortlet', 
{
  extend: 'Ext.panel.Panel',
  
  layout: 'fit',
  height: 262,
  scrollable: true,

  props:
  {
  	title: 'Incidient Review - Cran & Backbone Facilities/Transport/Power'
  },
  
  
  initComponent: function()
  {
  	this.items = this.buildItems();
  	
  	this.callParent(arguments);
  },
  
  
  buildItems: function()
  {
  	return [
  	{
  		xtype: 'chart.CRANBackboneFacilityChart'
  	}]
  }
});