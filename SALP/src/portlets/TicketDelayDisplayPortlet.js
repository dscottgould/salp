﻿//
//
//   Ticket Delay Display Portlet
//
//



Ext.define('SALP.portlets.TicketDelayDisplayPortlet', 
{
  extend: 'Ext.panel.Panel',
  
  layout: 'fit',
  height: 262,

  props:
  {
  	title: 'CR Ticket Delay Display'
  },
  
  
  initComponent: function()
  {
  	this.items = this.buildItems();
  	
  	this.callParent(arguments);
  },
  
  
  buildItems: function()
  {
  	return [
  	{
			xtype: 'grid.TicketDelayDisplayGrid'
  	}]
  },
  
});