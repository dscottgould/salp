﻿//
//
//   Cran & Backbone IP Portlet
//
//



Ext.define('SALP.portlets.CranBackboneIPPortlet', 
{
  extend: 'Ext.panel.Panel',
  
  layout: 'fit',
  height: 262,
  scrollable: true,

  props:
  {
  	title: 'Incident Review - Cran & Backbone IP(Switching/Routing)'
  },
  
  
  initComponent: function()
  {
  	this.items = this.buildItems();
  	
  	this.callParent(arguments);
  },
  
  
  buildItems: function()
  {
  	return [
  	{
  		xtype: 'chart.CRANBackboneIPChart'
  	}]
  }
});