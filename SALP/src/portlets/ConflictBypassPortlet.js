﻿//
//
//   Conflict Bypass Portlet
//
//



Ext.define('SALP.portlets.ConflictBypassPortlet', 
{
  extend: 'Ext.panel.Panel',
  
  layout: 'fit',
  height: 262,

  props:
  {
  	title: 'Conflict Bypass Report'
  },
  
  
  initComponent: function()
  {
  	this.items = this.buildItems();
  	
  	this.callParent(arguments);
  },
  
  
  buildItems: function()
  {
  	return [
		{
			xtype : 'grid.ConflictBypassGrid'
		}]
  }
});