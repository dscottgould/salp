﻿//
//
//   Band-Aid Portlet
//
//



Ext.define('SALP.portlets.BandAidPortlet', 
{
  extend: 'Ext.panel.Panel',
  
  layout: 'vbox',
  height: 262,
  bodyStyle: "background: url(img/BandAid.png) no-repeat !important",

  props:
  {
  	title: 'Band-Aid Tickets'
  },
  
  
  initComponent: function()
  {
  	this.items = this.buildItems();
  	
  	this.callParent(arguments);
  },
  
  
  buildItems: function()
  {
  	return [
  	{
  		xtype: 'container',
  		height: 80
  	},
		{
			id: 'TicketNumberField',
			xtype: 'textfield',
			fieldLabel: 'Ticket #',
			labelWidth: 60,
			width: 175,
			padding: '10 10 10 10',
     	listeners: 
     	{
      	specialkey: function(field, event)
      	{
        	if (event.getKey() == event.ENTER) 
        	{
						this.getTicket();
          }
        },
        scope: this
      }
			//value: 'SI012939939'
		},
		{
			xtype: 'container',
			layout: 'hbox',
			items: [
			{
				xtype: 'container',
				width: 125
			},
			{
				xtype: 'button',
        text: 'Get Ticket',
        listeners: 
				{
        	click: function(button, event, object) 
          {
          	this.getTicket();
          },
          scope: this
        }
			}]
		}]
  },
  getTicket: function()
  {
  	var ticketNumber = Ext.getCmp('TicketNumberField').getValue();
  	
  	if (ticketNumber.length > 0)
  	{
  		SALP.getTicket(ticketNumber);
  	}
		else
		{
			Ext.toast('Please Enter Ticket Number.');
		}
  }	
});