﻿//
//
//   Maintenance Caused Sev1 Month Over Month Portlet
//
//



Ext.define('SALP.portlets.MaintenanceCausedSev1MonthOverMonthPortlet', 
{
  extend: 'Ext.panel.Panel',
  
  layout: 'fit',
  height: 262,

  props:
  {
  	title: 'All Maintenance Success/Failure Month Over Month'
  },
  
  
  initComponent: function()
  {
  	this.items = this.buildItems();
  	
  	this.callParent(arguments);
  },
  
  
  buildItems: function()
  {
		return [
		{
			xtype: 'grid.MaintenanceCausedSev1MonthOverMonthGrid'
		}]
  }
});