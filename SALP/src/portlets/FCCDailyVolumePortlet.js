//
//
//   FCC Daily Volume Portlet
//
//



Ext.define('SALP.portlets.FCCDailyVolumePortlet', 
{
  extend: 'Ext.panel.Panel',
  
  layout: 'fit',
  height: 262,

 props:
  {
  	title: 'FCC 30 Day Volume'
  },
  
  initComponent: function()
  {
  	this.items = this.buildItems();
  	this.listeners = this.buildListeners();
  	
  	this.callParent(arguments);
  },
  
  
  buildItems: function()
  {
  	return [
  	{
    	xtype: 'image',
      src: 'http://epssaweb-po-01p/dashboard/img/screencapture/FCC_Daily_Volume_sm.png'
    }]
  },
  buildListeners: function()
  {
  	var listener =     	
  			{
  				element  : 'el',
					scope: this,
					click: function() 
					{
						SALP.openSite('http://obivrs-wc-01p/Reports/Pages/Report.aspx?ItemPath=/Deploy/FCC+Daily+Volume+Snapshot+IFW+Rolling+30+Only');
					}
  			}
  	
  	return listener
 	}
});