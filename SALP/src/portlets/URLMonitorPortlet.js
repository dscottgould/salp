﻿//
//
//   ULRMonitorPortlet.js
//
//



Ext.define('SALP.portlets.URLMonitorPortlet', 
{
  extend: 'Ext.panel.Panel',
  
	layout: 
	{
  	type: 'vbox',
    align: 'stretch',
    pack: 'center'
	},
  height: 262,

  props:
  {
  	title: 'URL Monitor'
  },
  
  
  initComponent: function()
  {
  	this.items = this.buildItems();
  	
  	this.callParent(arguments);
  },
  
  
  buildItems: function()
  {
  	return [
  	{
			xtype: 'chart.URLMonitor01Chart',
			flex: 1
  	},
  	{
  		xtype: 'chart.URLMonitor02Chart',
  		flex: 1
  	},
  	{
  		xtype: 'chart.URLMonitor03Chart',
  		flex: 1
  	}]
  }
});