//
//
//   HSD Portlet
//
//


Ext.define('SALP.portlets.HSDPortlet', 
{
  extend: 'Ext.panel.Panel',
  alias: 'widget.hsdportlet',

 	layout: 'fit',
  height: 262,
      
  props:
  {
  	title: 'HSD Trouble Tickets & Calls'
  },
  
  
  initComponent: function()
  {
  	this.items = this.buildItems();
  	this.listeners = this.buildListeners();
  	
  	this.callParent(arguments);
  },
  
  
  buildItems: function()
  {
  	return [
  	{
    	xtype: 'image',
      src: 'http://epssaweb-po-01p/dashboard/img/screencapture/HSD_dash.png'
    }]
  },
  buildListeners: function()
  {
  	var listener =     	
		{
			element  : 'el',
			scope: this,
			click: function() 
			{
				SALP.openSite('http://xnocobi/ReportServer/Pages/ReportViewer.aspx?%2fDeploy%2fService_Rhythm%2fService_Rhythm_Metrics&rs:Command=Render&Selected=HSD&TimeFrame=WP1');
			}
		}
  	
  	return listener
  }
});