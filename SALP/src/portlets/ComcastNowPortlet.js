﻿//
//
//   Comcast Now Portlet
//
//



Ext.define('SALP.portlets.ComcastNowPortlet', 
{
    extend: 'Ext.grid.Panel',
    alias: 'widget.comcastnowportlet',
    height: 262,

    props:
    {
    	title: 'Comcast Now'
    },
    
    store: SALP.data.ComcastNow.store,
    stripeRows: true,
    columnLines: true,
    header: 
    {
    	itemPosition: 1, // after title before collapse tool
    	items: [
    	{
    		id: 'StockTicker',
    		xtype: 'container'
    	}]
    },
    
    initComponent: function()
    {
			this.columns = this.buildColumns();
      this.plugins = this.buildPlugins();
      this.tools = this.buildTools();
    	
    	
    	SALP.updateStocks();
    	
    	this.task = new Ext.util.DelayedTask(function()
			{
				Ext.getCmp('StockTicker').setHtml("<b>Updating....</b>");
				
    		SALP.updateStocks();
    	
    		this.task.delay(900000);
    		
			}, this);
		
			this.task.delay(900000);
			
			SALP.data.ComcastNow.store.load();
    	
      this.callParent(arguments);
    },
    buildColumns: function()
    {
    	return [
      {
     		text   : 'Articles',
        sortable : true,
        dataIndex: 'title',
        hideable: false,
        resizable: false,
        flex: 1,
        renderer : function(title, data)
        {
        	var link = "<a href='javascript:void(0)' onclick='SALP.renderLink(\"" + data.record.data.url + "\");'>" + title + "</a>";
        			
              	
          return link
          return "<a href='javascript:void(0)' onclick='SALP.renderLink(" + data.record.data.url + ");'> " + title + "</a>";
        }
      }]
    },
    buildPlugins: function()
    {
    	return [
      {
    		ptype: 'rowexpander',
      	rowBodyTpl : new Ext.XTemplate
      	(
       		'Tags: ',
       		'<a href="javascript:void(0);" onClick="SALP.renderLink(\'{[values.tags[0].url]}\');">{[values.tags[0].name]}</a>',
       		'<tpl if="[values.tags.length] &gt; 1">',
      		', <a href="javascript:void(0);" onClick="SALP.renderLink(\'{[values.tags[1].url]}\');">{[values.tags[1].name]}</a>',
      		'</tpl>',
      		'<tpl if="[values.tags.length] &gt; 2">',
          	', <a href="javascript:void(0);" onClick="SALP.renderLink(\'{[values.tags[2].url]}\');">{[values.tags[2].name]}</a>',
      		'</tpl>',
      		'<tpl if="[values.tags.length] &gt; 3">',
          	', <a href="javascript:void(0);" onClick="SALP.renderLink(\'{[values.tags[3].url]}\');">{[values.tags[3].name]}</a>',
      		'</tpl>'
      	)
 			}]
    },
    buildTools: function()
    {
    	return [
 			{
 				type: 'refresh',
 				tooltip: 'Refresh Stock',
 				handler: function(event, toolEl, panelHeader) 
 				{
 					Ext.getCmp('StockTicker').setHtml('<b>Refreshing....</b>');
 					
    			Ext.Ajax.request(
    			{
						url: '/WebServices/Service.svc/GetComcastStock',
						success: function(response, opts) 
						{
  						var stock = Ext.decode(response.responseText).data,
  								message = '<b>Stock: ' + stock.symbol + ", last: " + stock.last + ", change: " + stock.change + '</b>';
  			
  			
  						Ext.getCmp('StockTicker').setHtml(message);
						},
						failure: function(form, action) 
						{
							var t=1;
						}
					});
  			}
 			}]
    }
});