﻿//
//
//   Core Dash Incidents Rolling 30 by Category Portlet
//
//



Ext.define('SALP.portlets.CoreDashIncidentsRolling30byCategoryPortlet', 
{
    extend: 'Ext.panel.Panel',

		layout: 'fit',
    height: 262,

    props:
    {
    	title: 'Core Dash-Incidents by Category Rolling 30 Days'
    },
    
    
    initComponent: function()
    {
    	this.items = this.buildItems();
    	this.listeners = this.buildListeners()
    	
    	this.callParent(arguments);
    },
    
    
    buildItems: function()
    {
    	return [
      {
      	xtype: 'image',
        src: '/img/screencapture/Core_Dash_Incidents_Rolling_30_by_Category.png'
      }]
    },
    buildListeners: function()
    {
    	var listener = 
    			{
						element  : 'el',
						scope: this,
						click: function() 
						{
							SALP.openSite('http://tableau.cable.comcast.com/#/site/InfrastructureServices/views/SI_CORE_Dash/IncidentsRolling30Days?:iid=1');
						}
					}
					
			return listener;
    }
});