﻿//
//
//   Favorite Links Tree Portlet
//
//



Ext.define('SALP.portlets.FavoriteLinksTreePortlet', 
{
	itemId: 'FavoriteLinksTree',
  extend: 'Ext.panel.Panel',
  
  layout: 'fit',
  height: 262,

  props:
  {
  	title: 'Favorite Links'
  },
  
  
  initComponent: function()
  {
  	this.tbar = this.buildToolbar();
  	
  	this.store = this.buildStore();
  	
  	this.callParent(arguments);
  },
  
  
  buildToolbar: function()
  {
  	return [
  	'->',
  	{
  		text: 'Manage',
  		iconCls: 'linkIcon',
  		tooltip: 'Manage Links',
  		handler: function(button, event)
  		{
  			this.manageLinks();
  		},
  		scope: this
  	},
  	{
  		text: 'Add',
  		iconCls: 'addLink',
  		tooltip: 'Add Link',
  		handler: function(button, event)
  		{
  			this.addLink();
  		},
  		scope: this
  	},
  	{
  		text: 'Edit',
  		iconCls: 'editLink',
  		tooltip: 'Edit Link',
  		handler: function(button, event)
  		{
  			this.editLink();
  		},
  		scope: this
  	},
  	{
  		text: 'Delete',
  		iconCls: 'deleteLink',
  		tooltip: 'Delete Link',
  		handler: function(button, event)
  		{
  			this.deleteLink();
  		},
  		scope: this
  	}]
  },
 
  buildStore: function()
  {
  	return Ext.create('Ext.data.Store', 
    {
    	storeId: 'FavoriteLinksStore',
    	autoLoad: true,
    	model: SALP.data.FavoriteLinks.model,
    	scope: this,
    	proxy: 
    	{
      	type: 'ajax',
       	url: 'WebServices/Service.svc/GetUserFavoriteLinks',
       	extraParams:
       	{
   		  	accountName: SALP.user.accountName
       	},
       	pageParam: undefined,
    		startParam: undefined,
    		limitParam: undefined,
       	reader: 
       	{
        	type: 'json',
          root: 'data'
       	}
    	},
    	listeners: 
    	{
				'load': function(store, records, success, operation, object)
				{
					var currentGroup, treePanel, rootNode, groupRoot;
					
					this.add(Ext.create('Ext.tree.Panel',
					{
						listeners: 
						{
        			itemdblclick: function(source, record, item, index, event) 
        			{
        				if(record.data.leaf)
        				{
        					SALP.openSite(record.data.link);
        				}
        			}
        		}
					}));
					
					treePanel = this.items.items[0];
					
					treePanel.setRootNode
        	({
        		text:	'Favorite Links',
        		expanded: true
        	})
					
					rootNode = treePanel.getRootNode();
					
					for(var index = 0; index <= records.length - 1; index ++)
					{
						var currentRecord = records[index];
						
						if (currentGroup != currentRecord.data.group)
						{
							currentGroup = currentRecord.data.group;
							
							groupRoot = rootNode.appendChild
  						({
  								text: currentGroup
  						})
						}
						
						if(groupRoot)
						{
							groupRoot.appendChild(
							{
								text: currentRecord.data.description,
								link: currentRecord.data.link,
								record: currentRecord,
								iconCls: 'linkIcon',
								leaf: true
							})
						}
					}
					
					this.setLoading(false);
				},
				scope: this
			}
    });
  },
  
    
  manageLinks: function()
  {
  	SALP.manageLinks();
  },
  
  addLink: function()
  {
  	SALP.view.AddEditFavoriteLinkWindow = Ext.create('SALP.view.AddEditFavoriteLinkWindow',
  	{
  		mode: 'Add'
  	}).show();	
  },
  
  editLink: function()
  {
  	var selection = this.items.items[0].getSelection()[0];
  	
  	if(selection)
  	{
  		if(selection.data.leaf)
  		{
  			SALP.view.AddEditFavoriteLinkWindow = Ext.create('SALP.view.AddEditFavoriteLinkWindow',
  			{
  				mode: 'Edit',
  				record: selection.data.record
  			}).show();
  		}
  	}
  },
  deleteLink: function()
  {
  	var selection = this.items.items[0].getSelection()[0];
  	
  	if(selection)
  	{
  		if(selection.data.leaf)
  		{
  			SALP.view.DeleteFavoriteLinkWindow = Ext.create('SALP.view.DeleteFavoriteLinkWindow',
  			{
  				record: selection.data.record
  			}).show();
  		}
  	}
  }
});