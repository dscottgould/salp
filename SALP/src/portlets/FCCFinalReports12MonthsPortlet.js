//
//
//   FCC Final Reports 12 Months
//
//



Ext.define('SALP.portlets.FCCFinalReports12MonthsPortlet', 
{
  extend: 'Ext.panel.Panel',

  layout: 'fit',
  height: 262,

  props:
  {
  	title: 'FCC Final Incidents Reported � Rolling 12 Months'
  },
  
  
  initComponent: function()
  {
  	this.items = this.buildItems();
  	this.listeners = this.buildListeners();
  	
  	this.callParent(arguments);
  },
  
  
  buildItems: function()
  {
  	return [
  	{
    	xtype: 'image',
      src: 'http://epssaweb-po-01p/dashboard/img/screencapture/FCC_Final_Reports_12_Months_sm.png'
    }]
  },
  buildListeners: function()
  {
  	var listener =     	
  			{
  				element  : 'el',
					scope: this,
					click: function() 
					{
						SALP.openSite('http://obivrs-wc-01p/Reports/Pages/Report.aspx?ItemPath=%2fDeploy%2fFCC+Final+Reported+Rolling+Months');
					}
  			}
  	
  	return listener
 	}
});