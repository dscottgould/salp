﻿//
//
//   TempDBLogStatusPortlet.js
//
//



Ext.define('SALP.portlets.TempDBLogStatusPortlet', 
{
  extend: 'Ext.panel.Panel',
  
  layout: 'fit',
  height: 262,
  scrollable: true,

  props:
  {
  	title: 'TempDB Log Status'
  },
  
  
  initComponent: function()
  {
  	this.items = this.buildItems();
  	
  	this.callParent(arguments);
  },
  
  
  buildItems: function()
  {
  	return [
  	{
			xtype: 'chart.TempDBLogStatusChart'
  	}]
  }
});