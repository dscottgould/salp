﻿//
//
//   CPU Usage Portlet
//
//



Ext.define('SALP.portlets.ServerCPUUsagePortlet', 
{
  extend: 'Ext.panel.Panel',
  
  layout: 'fit',
  height: 262,

  props:
  {
  	title: 'Server CPU Usage - obidb-wc-p01',
  	server: 'obidb-wc-p01'
  },
  
  
  initComponent: function()
  {
  	this.items = this.buildItems();
  	
  	this.callParent(arguments);
  },
  
  
  buildItems: function()
  {
  	return new Ext.create('SALP.chart.ServerCPUUsageChart',
  	{
  		server: this.props.server
  	})
  }
});