﻿//
//
//   Network Quality Maint Related Incidents by Div Portlet
//
//



Ext.define('SALP.portlets.NetworkQualityMaintRelatedIncidentsbyDivPortlet', 
{

    extend: 'Ext.panel.Panel',
    alias: 'widget.networkqualitymaintrelatedincidentsbydivportlet',

    props:
    {
    	title: 'Network Quality-Maintenance Related Incidents by Division'
    },
    
    
    initComponent: function()
    {
    	Ext.apply(this,
			{
      	layout: 'fit',
        height: 262,
        //html: 'TEST',
        items:[
        {
        	xtype: 'image',
        	src: '/img/screencapture/NetworkQuality_Maint_Related_Incidents_by_Div.png'
        }],
				listeners: 
				{
					element  : 'el',
					scope: this,
					click: function() 
					{
						SALP.openSite('http://tableau.cable.comcast.com/#/site/InfrastructureServices/views/NetworkQualityDashboard/IncidentSummary?:iid=1');
					}
				}
			});
    	
    	
    	this.callParent(arguments);
    }
});