//
//
//   DailyNational1682 Portlet
//
//



Ext.define('SALP.portlets.DailyNational1682Portlet', 
{

    extend: 'Ext.panel.Panel',
    
    props:
    {
    	title: 'Arris 862'
    },
    
    
    initComponent: function()
    {
    	Ext.apply(this,
			{
      	layout: 'fit',
        height: 262,
        //html: 'TEST',
        items:[
        {
        	xtype: 'image',
        	src: 'http://epssaweb-po-01p/dashboard/img/screencapture/daily_national_1682.png'
        }],
				listeners: 
				{
					element  : 'el',
					scope: this,
					click: function() 
					{
						SALP.openSite('http://epssabi-wc-01p:9502/analytics/saw.dll?Answers&Path=%2Fshared%2FCPE%2FAnalysis%2FTicket%20Count%20By%20Problem%20Code%2FTG1682G%20Ticket%20Count%20By%20Overall%20Problem%20Code');
					}
				}
			});
    	
    	
    	this.callParent(arguments);
    }
});