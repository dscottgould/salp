Ext.define('SALP.portlets.GridPortlet', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.gridportlet',
    height: 262,

    initComponent: function()
    {
      Dashboard.data.subCounts = new Object();
    	Dashboard.data.subCounts.model = new Object();
    	Dashboard.data.subCounts.store = new Object();
    	
  		Dashboard.data.subCounts.model =  Ext.define('HSD', 
  		{
  			extend: 'Ext.data.Model',
  			fields: [
  				{name: 'countDate', type: 'date'},
  				{name: 'productService', type: 'string'},
  				{name: 'subCount', type: 'int'},
  				{name: 'comp', type: 'string'}
  		]});
    	
 			Dashboard.data.subCounts.store = Ext.create('Ext.data.Store', 
 			{
     		autoLoad: true,
     		model: Dashboard.data.subCounts.model,
     		proxy: 
     		{
        	type: 'ajax',
         	url: 'WebServices/Service.svc/GetSubscriberCounts',
         	pageParam: undefined,
   				startParam: undefined,
   				limitParam: undefined,
         	reader: 
         	{
          	type: 'json',
            root: 'data'
         	}
     		}
 			});
        
			Dashboard.displayImage = function(value)
			{
				var returnPath = '<img src="';
				
				if(value == '~/slides/StockUp.png') 
				{
					returnPath += 'img/StockUp.png" />' 
				}
				else
				{
						returnPath += 'img/StockDown.png" />' 
				}

				return returnPath
			}


        SALP.portletsly(this, {
            //height: 262,
            height: this.height,
            store: Dashboard.data.subCounts.store,
            stripeRows: true,
            columnLines: true,
            columns: [
            {
                id       :'company',
                text   : 'LOB',
                //width: 120,
                flex: 1,
                sortable : true,
                dataIndex: 'productService'
            },
            {
                text   : 'Date',
                width    : 75,
                sortable : true,
                renderer : this.change,
                dataIndex: 'countDate',
                renderer: Ext.util.Format.dateRenderer('m-d-Y')
            },
            {
                text   : 'SubCount',
                width    : 85,
                sortable : true,
                dataIndex: 'subCount'
            },
            {
                text   : 'Change',
                width    : 55,
                sortable : true,
                renderer : Dashboard.displayImage,
                dataIndex: 'comp'
            }]
        });

        this.callParent(arguments);
    }
});
