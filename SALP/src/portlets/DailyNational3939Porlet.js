//
//
//   Daily National 3939 Porlet
//
//


Ext.define('SALP.portlets.DailyNational3939Porlet', 
{
  extend: 'Ext.panel.Panel',

  layout: 'fit',
  height: 262,

  props:
  {
  	title: 'Daily National 3939'
  },
  
  
  initComponent: function()
  {
  	this.items = this.buildItems();
  	this.listeners = this.buildListeners();
  	
  	this.callParent(arguments);
  },
  
  
  buildItems: function()
  {
  	return [
  	{
    	xtype: 'image',
      src: '/img/screencapture/daily_national_3939.png'
    }]
  },
  buildListeners: function()
  {
  	var listener =     	
  			{
  				element  : 'el',
					scope: this,
					click: function() 
					{
						SALP.openSite('http://epssabi-wc-01p:9502/analytics/saw.dll?Answers&Path=%2Fshared%2FCPE%2FAnalysis%2FTicket%20Count%20By%20Problem%20Code%2FDPC3939%20Ticket%20Count%20By%20Overall%20Problem%20Code');
					}
  			}
  	
  	return listener
 	}
});