﻿//
//
// Sev1 Exec Incident Totals By Month Portlet
//
//



Ext.define('SALP.portlets.Sev1ExecIncidentTotalsByMonthPortlet', 
{
  extend: 'Ext.panel.Panel',
  
  layout: 'fit',
  height: 262,

  props:
  {
  	title: 'Sev1 Exec Incident Totals By Month'
  },
  
  
  initComponent: function()
  {
  	this.items = this.buildItems();
  	this.listeners = this.buildListeners();
  	
  	this.callParent(arguments);
  },
  
  
  buildItems: function()
  {
  	return [
  	{
  		xtype: 'image',
    	src: 'http://salp/img/screencapture/Sev1ExecIncidentTotalsByMonth.png'
  	}]
  },
  
  buildListeners: function()
  {
  	var listener =     	
  			{
  				element  : 'el',
					scope: this,
					click: function() 
					{
						SALP.openSite('https://tableau.comcast.com/t/InfrastructureServices/views/Severity1ExecutiveOverview/Sev1ExecView?%3Aembed=y&%3AshowShareOptions=true&%3Adisplay_count=no&%3AshowVizHome=no');
					}
  			}
  	
  	return listener
 	}
});