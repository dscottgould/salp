Ext.define('SALP.portlets.SubscriberCountsPortlet', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.gridportlet',
    height: 262,
    
    props:
    {
    	title: 'Subscriber Counts'
    },

    initComponent: function()
    {
      SALP.data.subCounts = new Object();
    	SALP.data.subCounts.model = new Object();
    	SALP.data.subCounts.store = new Object();
    	
  		SALP.data.subCounts.model =  Ext.define('HSD', 
  		{
  			extend: 'Ext.data.Model',
  			fields: [
  				{name: 'countDate', type: 'date'},
  				{name: 'productService', type: 'string'},
  				{name: 'subCount', type: 'int'},
  				{name: 'comp', type: 'string'}
  		]});
    	
 			SALP.data.subCounts.store = Ext.create('Ext.data.Store', 
 			{
     		autoLoad: true,
     		model: SALP.data.subCounts.model,
     		proxy: 
     		{
        	type: 'ajax',
         	url: 'WebServices/Service.svc/GetSubscriberCounts',
         	pageParam: undefined,
   				startParam: undefined,
   				limitParam: undefined,
         	reader: 
         	{
          	type: 'json',
            root: 'data'
         	}
     		}
 			});
        
			SALP.displayImage = function(value)
			{
				var returnPath = '<img src="';
				
				if(value == '~/slides/StockUp.png') 
				{
					returnPath += 'img/StockUp.png" />' 
				}
				else
				{
						returnPath += 'img/StockDown.png" />' 
				}

				return returnPath
			}


        Ext.apply(this, {
            //height: 262,
            height: this.height,
            store: SALP.data.subCounts.store,
            stripeRows: true,
            columnLines: true,
            columns: [
            {
                id       :'company',
                text   : 'Line Of Business',
                //width: 120,
                flex: 1,
                sortable : true,
                dataIndex: 'productService'
            },
            {
                text   : 'Date',
                width    : 75,
                sortable : true,
                renderer : this.change,
                dataIndex: 'countDate',
                renderer: Ext.util.Format.dateRenderer('m-d-Y')
            },
            {
                text   : 'SubCount',
                width    : 85,
                sortable : true,
                dataIndex: 'subCount'
            },
            {
                text   : 'Change',
                width    : 55,
                sortable : true,
                renderer : SALP.displayImage,
                dataIndex: 'comp'
            }]
        });

        this.callParent(arguments);
    }
});
