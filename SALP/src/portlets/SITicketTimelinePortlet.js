﻿//
//
//   SI Ticket Timeline Portlet
//
//



Ext.define('SALP.portlets.SITicketTimelinePortlet', 
{
  extend: 'Ext.panel.Panel',
  
  layout: 'vbox',
  height: 262,

  props:
  {
  	title: 'SI Ticket Timeline'
  },
  
  
  initComponent: function()
  {
  	this.items = this.buildItems();
  	
  	this.callParent(arguments);
  },
  
  
  buildItems: function()
  {
  	return [
  	{
  		xtype: 'chart.SITicketTimelineChart',
  		height: 220,
  		width: '100%'
  	},
  	{
  		xtype: 'panel',
  		layout: 'hbox',
  		bodyPadding : '10 10 10 10',
  		width: '100%',
  		items: [
  		{
				xtype: 'textfield',
				fieldLabel: 'Ticket #',
				labelWidth: 60,
				width: 175,
				//value: 'SI010921124',
				listeners: 
     		{
      		specialkey: function(field, event)
      		{
        		if (event.getKey() == event.ENTER) 
        		{
								SALP.getSITicket();
          	}
        	},
        	scope: this
      	}
  		},
  		{
  			xtype: 'container',
  			width: 10
  		},
  		{
  			xtype: 'button',
        text: 'Get Ticket',
        listeners: 
				{
        	click: function(button, event, object) 
          {
          	SALP.getSITicket(this);
          },
          scope: this
        }
  		}]
  	}]
  }
});