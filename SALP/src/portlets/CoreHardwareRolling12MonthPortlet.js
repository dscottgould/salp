﻿//
//
//   Core Hardware Rolling 12 Month Portlet
//
//



Ext.define('SALP.portlets.CoreHardwareRolling12MonthPortlet', 
{
    extend: 'Ext.panel.Panel',
    
    layout: 'fit',
    height: 262,

    props:
    {
    	title: 'Core Incidents-Hardware Rolling 12 Months'
    },
    
    
    initComponent: function()
    {
    	this.items = this.buildItems();
    	this.listeners = this.buildListeners();
    
    	this.callParent(arguments);
    },
    
    buildItems: function()
    {
    	return [
      {
      	xtype: 'image',
        src: '/img/screencapture/Core_Hardware_Rolling_12_Month_sm.png'
      }]
    },
    buildListeners: function()
    {
    	var listener = 
    			{
						element  : 'el',
						scope: this,
						click: function() 
						{
							SALP.openSite('http://tableau.cable.comcast.com/#/site/InfrastructureServices/views/SI_CORE_Dash/IncidentsSummary?:iid=1');
						}
					}
				
			return listener
    }
});