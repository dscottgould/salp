﻿//
//
//   Network Quality Maint Related Incidents by CatPortlet
//
//



Ext.define('SALP.portlets.NetworkQualityMaintRelatedIncidentsbyCatPortlet', 
{
  extend: 'Ext.panel.Panel',

  layout: 'fit',
  height: 262,

  props:
  {
  	title: 'Network Quality-Maintenance Related Incidents by Category'
  },
  
  
  initComponent: function()
  {
  	this.items = this.buildItems();
    this.listeners = this.buildListeners();
  	
  	this.callParent(arguments);
  },
  
  
  buildItems: function()
  {
  	return [
  	{
    	xtype: 'image',
      src: '/img/screencapture/NetworkQuality_Maint_Related_Incidents_by_Cat.png'
    }]
  },
  buildListeners: function()
  {
  	var listener =     	
  			{
  				element  : 'el',
					scope: this,
					click: function() 
					{
						SALP.openSite('http://tableau.cable.comcast.com/#/site/InfrastructureServices/views/NetworkQualityDashboard/IncidentSummary?:iid=1');
					}
  			}
  	
  	return listener
 	}
});