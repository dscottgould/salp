﻿//
//
//   CurrentServerProcessesPortlet.js
//
//



Ext.define('SALP.portlets.CurrentServerProcessesPortlet', 
{
  extend: 'Ext.panel.Panel',
  
  layout: 'fit',
  height: 262,

  props:
  {
  	title: 'Server Current Processes - obidb-wc-p01',
  	server: 'obidb-wc-p01'  
  },
  
  
  initComponent: function()
  {
  	this.items = this.buildItems();
  	
  	this.callParent(arguments);
  },
  
  
  buildItems: function()
  {
  	return [
  	{
  		xtype: 'grid.ServerProcessesBasicGrid',
  		server: this.props.server 
  	}]
  }
});