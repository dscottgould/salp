﻿//
//
//  Ticket Window
//
//

Ext.define('SALP.view.TicketWindowNew', 
{
	alias: 'widget.view.ticketWindowNew',
	extend: 'Ext.window.Window',

	id: 'ticketWindow',
  title: 'Get Ticket Details',
  height: 152,
  width: 209,
  resizable: true,
  modal: true,
  layout: 'fit',
	constrain: true,
	
	initComponent: function()
  {
  	this.items = this.buildItems();
  	this.buttons = this.buildButtons();
    	
    this.callParent(arguments);
  },
  
  
  buildItems: function()
  {
  	return [
  	{
  		xtype: 'container',
  		layout: 'vbox',
  		items:[
  		{
  			xtype: 'container',
  			padding: '10 0 0 10',
  			html: 'Ticket Number:'
  		},
  		{
  			id: 'ticketNumber',
  			xtype: 'textfield',
  			padding: '5 0 0 20',
  			//value: 'SI017730413',
  			//value: 'SI017729210',
  			//value: 'SI000780874',
  			//value: 'SI000780874',
  			afterRender: function()
  			{
  				this.focus();
  			}
  		}]
  	}];
	},
	
	buildButtons: function()
  {
  	return [
  	{
  		text: 'Ok',
  		listeners: 
			{
      	click: function(button, event, object) 
        {
					this.getTicketDetails();
					
					//this.submitTicketNumber();
        },
        scope: this
      }
  	}]
  },
  
  getTicketDetails: function()
  {
  	var ticketNumber = Ext.getCmp('ticketNumber').getValue()
  	
  	this.close()
  	
  	SALP.getTicketDetails(ticketNumber);
  },
  
  submitTicketNumber: function()
  {
  	var ticketNumber = Ext.getCmp('ticketNumber').getValue(),
  			site = 'http://xnocobi/ReportServer/Pages/ReportViewer.aspx?%2fTicket+Lookup&rs:Command=Render&TicketID=' + ticketNumber;
		
		var tab = window.open(site, '_blank');
	
		if(tab) tab.focus();
		
		this.close()
  }
});