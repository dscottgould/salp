﻿//
//
//	Incident Tickets Window
//
//

Ext.define('SALP.view.TicketDetailsWindow', 
{
	extend: 'Ext.window.Window',

  height: 600,
  width: 600,
  resizable: false,
  constrain: true,
  //maximizable: true,
  autoScroll: true,
  collapsible: true,
  modal: false,
  closable: true,
  frame: false,
  border: false,
  layout: 
  {
  	type: 'vbox',
    align: 'stretch'
  },
  
  
  initComponent: function()
  {
		this.items = this.buildItems();
		
		this.listeners = this.buildListeners();
		
    this.callParent(arguments);
  },
  
  buildItems: function()
  {
  	return [
  	{
  		xtype: 'view.TicketDetailsHeader',
  		ticketNumber: this.ticketNumber,
  		height: 90
  	},
  	{
  		xtype: 'chart.SITicketTimelineChart',
  		height: 220,
  		width: '100%'
  	},
  	{
  		xtype: 'form.TicketDetailsAForm',
  		height: 490
  	},
  	{
  		xtype: 'form.TicketDetailsBForm',
  		height: 370
  	},
  	{
  		xtype: 'grid.CustomerTicketsGrid',
  		ticketNumber: this.ticketNumber,
  		height: 400
  	},
  	{
  		xtype: 'form.TicketDetailsCForm',
  		height: 40
  	},
  	{
  		xtype: 'grid.DetailsAffectedGrid',
  		ticketNumber: this.ticketNumber,
  		height: 400
  	},
  	{
  		xtype: 'form.TicketDetailsDForm',
  		height: 40
  	},
  	{
  		xtype: 'grid.WorkLogGrid',
  		ticketNumber: this.ticketNumber,
  		height: 400
  	}]
  },
  
  
  buildListeners: function()
  {
  	return {
  		'show': function(object, options)
  		{
  			chart = this.items.items[1];
  	
      	chart.store.load
      	({
      		params:
      		{
      			ticketNumber: this.ticketNumber
      		},
      		callback : function(records, options, success) 
      		{
            if (success) 
            {
      				chart.redraw();
            }
          }
      	});
  		},
  		scope: this
  	}
  }
});