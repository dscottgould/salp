﻿//
//
//  Work Log Details Window
//
//


Ext.define('SALP.view.WorkLogDetailsWindow', 
{
	alias: 'widget.view.WorkLogDetailsWindow',
	extend: 'Ext.window.Window',

	id: 'WorkLogDetailsWindow',
  title: 'Work Log Details',
  height: 455,
  width: 470,
  resizable: true,
  modal: true,
  //autoScroll: true,
  layout: 'fit',
  //bodyPadding: '10 10 10 10',
	constrain: true,
  
  initComponent: function()
  {
		//this.html = this.details;
		
		this.items = this.buildItems();
		
    this.buttons = this.buildButtons();	
    this.callParent(arguments);
  },
  
  buildItems: function()
  {
  	return [
  	{
  		xtype: 'panel',
  		autoScroll: true,
  		html: this.details
  	}]
  },
  
  buildButtons: function()
  {
  	return [
  	{
  		text: 'Close',
  		listeners: 
			{
      	click: function(button, event, object) 
        {
					this.close();
        },
        scope: this
      }
  	}]
  },
});