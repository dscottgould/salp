﻿//
//
//  CM Detail By Hierarchy Window
//
//



Ext.define('SALP.view.CMDetailByHierarchyWindow', 
{
	extend: 'Ext.window.Window',

	iconCls: 'TreeMapDetails',
	height: 510,
  width: 1115,
  resizable: true,
  layout: 'fit',
	constrain: true,
  
  initComponent: function()
  {
  	this.setKeyMap(
   	{	
   		scope: this,
   		'ENTER': function()
   		{
    		this.close();
    	}
    });
    
    this.setTitle('CM Detail By Hierarchy - ' + this.userName);
    
    this.header = this.buildHeader();
    
  	this.items = this.buildItems();
  	
  	this.buttons = this.buildButtons();
  	
  	this.callParent(arguments);
  },
  
  buildHeader: function()
  {
  	return   {
    	itemPosition: 1, // after title before collapse tool
    	items: [
    	{
        xtype: 'button',
        text: 'Download',
       	listeners: 
				{
    			click: function() 
     			{
						this.items.items[0].saveDocumentAs(
						{
    					type: 'xlsx',
    					title: 'CM Detail By Hierarchy',
    					fileName: 'CMDetailByHierarchy.xlsx'
						});
      		},
      		scope: this
				}
    	},
    	{
  			xtype: 'container',
  			width: 50
  		}]
    }
  },
	
	
	buildItems: function()
	{
		return [
		{
			xtype: 'grid.CMDetailByHierarchyGrid',
			userName: this.userName,
			month: this.month,
			year: this.year,
			reportLevel: this.reportLevel
		}]
	},
	
	buildButtons: function()
  {
  	return [
  	{
  		text: 'Close',
  		listeners: 
			{
      	click: function(button, event, object) 
        {
					this.close()
        },
        scope: this
      }
  	}]
  },
	
});