//
//
//  SALP Parameter Window
//
//

Ext.define('SALP.view.ParamWindow', 
{
	alias: 'widget.view.paramWindow',
	extend: 'Ext.window.Window',

	id: 'paramWindow',
  title: 'Set Filter',
  height: 185,
  width: 358,
  resizable: false,
  modal: true,
  layout: 'fit',
	constrain: true,
	
	initComponent: function()
  {
    	this.items = this.buildItems();
    	
    	this.callParent(arguments);
  },
  buildItems: function()
  {
  	return [
  	{   
    	xtype: 'container',
    	layout: 'vbox',
    	border: 1,
    	padding : '10 10 10 10',
    	items:[
    	{
    		xtype: 'container',
    		layout: 'hbox',
    		items:[
    		{
    			xtype: 'combobox',
    			id: 'segmentField',
    			store: SALP.data.selectSegment.store,
    			fieldLabel: 'Segment:',
    			labelWidth: 55,
    			queryMode: 'local',
    			width: 325,
    			displayField: 'name',
    			valueField: 'value'
    		}]
    	},
    	{
    		xtype: 'container',
    		height: 5
    	},
    	{
    		xtype: 'container',
    		layout: 'hbox',
    		items:[
    		{
    			id: 'userNameField',
    			xtype: 'textfield',
    			fieldLabel: 'Username:',
    			labelWidth: 55,
    			width: 250
    		}]
    	},
    	{
    		xtype: 'container',
    		height: 5
    	},
    	{
    		xtype: 'container',
    		layout: 'hbox',
    		items:[
    		{
    			id: 'userPasswordField',
    			xtype: 'textfield',
    			inputType: 'password',
    			fieldLabel: 'Password:',
    			labelWidth: 55,
    			width: 250
    		},
    		{
    			xtype: 'container',
    			width: 10
    		},
    		{
    			xtype: 'button',
    			text: 'Get Filters',
    			listeners:
    			{
    				click : function(button, event, object) 
    				{
    					var username = Ext.getCmp('userNameField').value,
    							userpassword = Ext.getCmp('userPasswordField').value
    							
    					SALP.data.selectFilter.store.load
    					({
    						params:
    						{
    							username: username,
    							password: userpassword
    						}
    					})
    				}
    			}
    		}]
    	},
    	{
    		xtype: 'container',
    		layout: 'hbox',
    		height: 5
    	},
    	{
    		xtype: 'container',
    		layout: 'hbox',
    		items:[
    		{
    			xtype: 'combobox',
    			id: 'filterField',
    			fieldLabel: 'Filter:',
    			labelWidth: 55,
    			store: SALP.data.selectFilter.store,
    			queryMode: 'local',
    			width: 250,
    			displayField: 'userFilter',
    			valueField: 'userFilter'
    		}]
    	},
    	{
    		xtype: 'container',
    		height: 5
    	},
    	{
    		xtype:'container',
    		layout: 'hbox',
    		items: [
    		{
    			xtype: 'container',
    			width: 260
    		},
    		{
    			xtype: 'button',
    			text: 'Ok',
    			width: 62,
    			listeners: 
    			{
           	click: Ext.bind(SALP.setHaystackFilter, this)
    			}
    		}]
    	}]
    }];
	}
});