﻿//
//
//  BandAid Ticket Window
//
//

Ext.define('SALP.view.BandAidTicketWindow', 
{
	extend: 'Ext.window.Window',

  height: 535,
  width: 830,
  resizable: true,
  modal: false,
  closable: true,
  collapsible: true,
  layout: 'fit',
	constrain: true,
	

  
  initComponent: function()
  {
  	this.setKeyMap(
   	{	
   		scope: this,
   		'ENTER': function()
   		{
    		this.submitTicket();
    	}
    });
    
		this.items = this.buildItems();
		
    this.buttons = this.buildButtons();	
    
    this.callParent(arguments);
  },

	buildItems: function()
	{
		return [
		{
			xtype: 'container',
			layout: 'hbox',
			items: [
			{
				xtype: 'form.oldticketform',
				flex: 1
			},
			{
				xtype: 'form.newticketform',
				flex: 1
			}]
		}]
	},
  buildButtons: function()
  {
  	return [
  	{
  		text: 'Cancel',
  		listeners: 
			{
      	click: function(button, event, object) 
        {
        	this.close();
        },
        scope: this
      }
  	},
  	{
  		text: 'Ok',
  		listeners: 
			{
      	click: function(button, event, object) 
        {
					this.submitTicket();
        },
        scope: this
      }
  	}]
  },
  submitTicket: function()
  {
  	var form = this.items.items[0].items.items[1];
  	
  	if (form.isValid)
  	{
    	form.submit
    	({
  			url: '/WebServices/Service.svc/UpdateTicket',
    		method: 'GET',
    		scope: this,
    		success: function()
  			{
  				Ext.toast('SUCCESS: Ticket Updated.');
  				
  				this.close();
  			},	
  			failure: function()
  			{
  				Ext.toast('FAILED: Unable To Update Ticket.');
  			}
    	})
    	
    	// Update Incident Tickets Window
    	Ext.getCmp('IncidentTicketsGrid').store.reload();
    }
  }
});