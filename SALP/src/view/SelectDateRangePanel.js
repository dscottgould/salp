﻿//
//
//  Select Date Range Panel
//
//

Ext.define('SALP.view.SelectDateRangePanel', 
{
	id: 'SelectDateRangePanel',
	alias: 'widget.view.selectdaterangepanel',
	extend: 'Ext.panel.Panel',
	
	height: '100%',
	layout: 'anchor',
	bodyPadding: '10 10 10 10',
	//bodyStyle: "background: url(img/haystack.jpg) no-repeat right top !important",
	
	defaults: 
	{
  	//anchor: '100%'
  	labelWidth: 120
	},
	
	 initComponent: function()
  {
		this.items = this.buildItems();
		
    
    this.callParent(arguments);
  },
	
	
	buildItems: function()
	{
		return [
		{
			xtype: 'container',
			height: 75
		},
		{
			xtype: 'container',
			layout: 'hbox',
			padding: '0 10 5 0',
			items: [
			{
				id: 'FromDateField',
				xtype: 'datefield',
				fieldLabel: 'From Date',
				flex: 1,
				value: this.getYesterdayDate(),
				listeners: 
				{
					select: function(dateField, value)
					{
						var slider = Ext.getCmp('DateSlider'),
								fromDate = slider.convertToUnix(Ext.Date.format(value, 'n/j/Y'));
							
					
						Ext.getCmp('DateSlider').setValue(0, fromDate, true)
					}
				}
			},
			{
				id: 'FromTimeField',
				xtype: 'timefield',
				width: 90,
				value: this.getYesterdayTime()
			}]
		},
		{
			xtype: 'container',
			layout: 'hbox',
			padding: '0 10 5 0',
			items: [
			{
				id: 'ToDateField',
				xtype: 'datefield',
				fieldLabel: 'To Date',
				flex: 1,
				value: this.getTodaysDate(),
				listeners: 
				{
					select: function(dateField, value)
					{
						var slider = Ext.getCmp('DateSlider'),
								toDate = slider.convertToUnix(Ext.Date.format(value, 'n/j/Y'));
							
					
						Ext.getCmp('DateSlider').setValue(1, toDate, true)
					},
//					change: function(dateField, value)
//					{
//						var slider = Ext.getCmp('DateSlider'),
//								toDate = slider.convertToUnix(Ext.Date.format(value, 'n/j/Y'));
//							
//					
//						Ext.getCmp('DateSlider').setValue(1, toDate, true)
//					}
				}
			},
			{
				id: 'ToTimeField',
				xtype: 'timefield',
				width: 90,
				value: this.getCurrentTime()
			}]
		},
		{
			xtype: 'container',
			height: 30
		},
		{
			id: 'DateSlider',
			xtype: 'dateslider',
			fieldLabel: 'Select Date Range',
			width: 300,
    	minDate: this.get13MonthsAgo(),
 			maxDate: this.getTodaysDate(),
 			values: [this.get4MonthsAgo(), this.getTodaysDate()],
 			listeners:
 			{
 				change: function(slider, value, thumb)
 				{
 					if(thumb.index == 0)
 					{
 						var fromDate = slider.convertFromUnix(value);
 						
 						Ext.getCmp('FromDateField').setValue(fromDate);
 					}
 					else
 					{
 						var toDate = slider.convertFromUnix(value);
 						
 						Ext.getCmp('ToDateField').setValue(toDate);
 					}
 				}
 			}
		},
		{
			xtype: 'container',
			height: 20
		},
		{
			xtype: 'container',
			layout: 'hbox',
			items: [
			{
				xtype: 'container',
				width: 260
			},
			{
				xtype: 'button',
				text: 'Set Date',
				listeners: 
				{
      		click: function(button, event, object) 
        	{
        		this.setDates()
        		//SALP.setHaystackDate();
        	},
        	scope: this
      	}
			}]
		}]
	},
	
	setDates: function()
	{
		var fromDate = Ext.getCmp('FromDateField').rawValue.toString(),
				fromTime = Ext.getCmp('FromTimeField').rawValue.toString(),
				toDate = Ext.getCmp('ToDateField').rawValue.toString(),
				toTime = Ext.getCmp('ToTimeField').rawValue.toString();
		
		
		fromDate = fromDate + ' ' + fromTime;
		toDate = toDate + ' ' + toTime;
		
		SALP.setHaystackDate(fromDate, toDate)
	},
	getDates: function()
	{
		var fromDate = Ext.getCmp('FromDateField').rawValue.toString(),
				fromTime = Ext.getCmp('FromTimeField').rawValue.toString(),
				toDate = Ext.getCmp('ToDateField').rawValue.toString(),
				toTime = Ext.getCmp('ToTimeField').rawValue.toString();
			
		var dates = new Object();
		
		dates.fromDate = fromDate + ' ' + fromTime;
		dates.toDate = toDate + ' ' + toTime;
		
		return dates
	},
	getTodaysDate: function()
	{
		return Ext.Date.format(new Date(), 'n/j/Y');
	},
	getCurrentTime: function()
	{
		return Ext.Date.format(new Date(), 'g:i A');
	},
	get13MonthsAgo: function()
	{
		var date = new Date();
    
    date.setDate(date.getDate() - 390);
    
		return Ext.Date.format(date, 'n/j/Y');
	},
	get4MonthsAgo: function()
	{
		var date = new Date();
    
    date.setDate(date.getDate() - 120);
    
		return Ext.Date.format(date, 'n/j/Y');
	},
	getYesterdayDate: function()
	{
		var date = new Date();
    
    date.setDate(date.getDate() - 1);
    
		return Ext.Date.format(date, 'n/j/Y');
	},
	getYesterdayTime: function()
	{
		var date = new Date(),
				hours = date.getHours(),
				minutes = date.getMinutes(),
				ampm = hours >= 12 ? 'PM' : 'AM',
				timeString = new String();
				
				
		hours = hours % 12;
  	hours = hours ? hours : 12; // the hour '0' should be '12'
  	minutes = minutes < 10 ? '0'+ minutes : minutes;
  	timeString = hours + ':' + minutes + ' ' + ampm;
				
		return timeString;
	}
});