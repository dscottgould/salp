﻿//
//
//  SALP Header
//
//

Ext.define('SALP.view.Header', 
{
	alias: 'widget.view.header',
	extend: 'Ext.container.Container',
	cls: 'header',
	
	layout:
  {
  	type: 'hbox',
    align: 'stretch'
  },

	//layout: 'border',
	
	
	initComponent: function()
  {
    	this.items = this.buildItems();
    	

   		this.items = this.items.concat(this.buildAdminAccess());

    	this.items = this.items.concat(this.buildhelp());
    	
    	
    	this.callParent(arguments);
  },
  buildItems: function()
  {
  	return [
  	{
  		xtype:'container',
  		html: 'Service Assurance Launchpad - ',
  		width: 320
  	},
  	{
  		id: 'dashboard-title',
  		xtype:'container',
  		flex: 1
  	},
  	{
  		id: 'user-display',
  		xtype:'container',
  		width: 300
  	}]
	},
	buildAdminAccess: function()
	{
		return [
		{
		 	xtype: 'image',
			cls: 'handCursor',
			src: '/img/extanim32.gif',
			width: 32,
			listeners: 
			{
				el: 
				{
					click: function() 
					{
						if(!Ext.getCmp('adminWindow'))
						{
							SALP.view.AdminWindow =	Ext.create('SALP.view.AdminWindow').show(); 
						}
						else
						{
							SALP.view.AdminWindow.show();
						}						
					},
					mouseover: function()
					{
						Ext.QuickTips.register
						({
      				target: this,
      				text: 'Configuration'
    				});
					}
				}
			}
		}]
	},
	buildhelp: function()
	{
		return [
		{
			xtype: 'container',
			width: 5
		},
		{
		 	xtype: 'image',
			cls: 'handCursor',
			src: '/img/help.png',
			width: 32,
			listeners: 
			{
				el: 
				{
					click: function() 
					{
						if (!Ext.getCmp('helpWindow'))
						{
							SALP.view.HelpWindow = Ext.create('SALP.view.HelpWindow').show();
						}
						else
						{
							SALP.view.HelpWindow.show();
						}
					},
					mouseover: function()
					{
						Ext.QuickTips.register
						({
      				target: this,
      				text: 'User Guide'
    				});
					}
				}
			}
		}]
	}
});