﻿//
//
//	Subordinates Search Window
//
//

Ext.define('SALP.view.SubordinatesSearchWindow', 
{
	extend: 'Ext.window.Window',
	
	//id: 'SubordinatesSearchWindow',
	
	height: 500,
  width: 700,
  resizable: true,
  maximizable: false,
	constrain: true,
	collapsible: true,
	scrollable: true,
	autoScroll: true,
	
	layout: 'fit',
	
	iconCls: 'manager',
	title: 'SAP Subordinates Lookup',
	
  
  initComponent: function()
  {
  	this.setTitle('SAP Subordinates Lookup - ' + this.searchName)
  	
  	this.items = this.buildItems();
  	
  	this.buttons = this.buildButtons();
  	
  	this.header = this.buildHeader();
  	
  	//this.listeners = this.buildListeners();
  	
  	this.callParent(arguments);
  },
  
  buildItems: function()
  {
  	return [
  	{
  		xtype: 'grid.SubordinatesSearchGrid',
  		searchName: this.searchName,
  		height: '100%'
  	}]
  },
  
  buildHeader: function()
  {
  	return   {
    	itemPosition: 1, // after title before collapse tool
    	items: [
    	{
        xtype: 'button',
        text: 'Download',
       	listeners: 
				{
    			click: function() 
     			{
						this.items.items[0].saveDocumentAs(
						{
    					type: 'xlsx',
    					title: 'Subordinates Search Results',
    					fileName: 'Subordinates.xlsx'
						});
      		},
      		scope: this
				}
    	},
    	{
  			xtype: 'container',
  			width: 50
  		}]
    }
  },
  
  buildButtons: function()
  {
  	return [
  	{
  		text: 'Close',
  		listeners: 
			{
      	click: function(button, event, object) 
        {
					this.close()
        },
        scope: this
      }
  	}]
  }
});