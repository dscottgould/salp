﻿//
//
//  Ticket Details Header
//
//

Ext.define('SALP.view.TicketDetailsHeader', 
{
	alias: 'widget.view.TicketDetailsHeader',
	extend: 'Ext.container.Container',
	
	layout:
  {
  	type: 'vbox',
    align: 'stretch'
  },
	
	
	initComponent: function()
  {
    	this.items = this.buildItems();
    	
    	this.callParent(arguments);
  },
  
  buildItems: function()
  {
  	return [
  	{
  		xtype: 'container',
  		html: '<center><h2>Service Incident Ticket Detail Report</h2><center>',
  		height: 25
  	},
  	{
  		xtype: 'container',
  		html: '<center><h3>' + this.ticketNumber + '</h3></center>'
  	},
  	{
  		xtype: 'container',
  		html: '<b>All Date/Times are MT</b>'
  	}]
  }
 });