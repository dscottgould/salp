﻿//
//
//  Sev1 Caused By Maint Details Window
//
//


Ext.define('SALP.view.Sev1CausedByMaintDetailsWindow', 
{
	alias: 'widget.view.Sev1CausedByMaintDetailsWindow',
	extend: 'Ext.window.Window',

  title: 'Maintance Ticket Details',
  height: 475,
  width: 730,
  resizable: true,
  maximizable: true,
  collapsible: true,
  modal: false,
  layout: 'fit',
	constrain: true,
  
  initComponent: function()
  {
  	this.setKeyMap(
   	{	
   		scope: this,
   		'ENTER': function()
   		{
    		this.close();
    	}
    });
  	
		this.items = this.buildItems();
    this.buttons = this.buildButtons();	
    
    this.callParent(arguments);
  },
  
  
  buildItems: function()
  {
  	return [
  	{
  		xtype: 'grid.Sev1CausedByMaintDetailsGrid',
  		directorName: this.directorName,
  		month: this.month,
  		year: this.year,
  		reportLevel: this.reportLevel
  	}]
  },
   
  buildButtons: function()
  {
  	return [
  	{
  		text: 'Close',
  		listeners: 
			{
      	click: function(button, event, object) 
        {
					this.close();
        },
        scope: this
      }
  	}]
  },
});