﻿//
//
//  urlWindow.js
//
//


Ext.define('SALP.view.UrlWindow', 
{
	alias: 'widget.view.urlwindow',
	extend: 'Ext.window.Window',

	id: 'urlWindow',
  title: 'Dashboard URL',
  height: 106,
  width: 300,
  resizable: false,
  modal: true,
  layout: 'fit',
  bodyPadding: '10 10 10 10',
  keyHandlers: 
 	{
  	ENTER: function()
  	{
  		this.close();
  	}
  },
  
  initComponent: function()
  {
  	if(!SALP.dashboardId)
		{
			this.html = "You must save your dashboard first."
		}
		else
		{
			this.html = 'http://' + window.location.host + '/?dashboard=' + SALP.dashboardId;
		}
		
    this.buttons = this.buildButtons();	
    this.callParent(arguments);
  },
  buildButtons: function()
  {
  	return [
  	{
  		text: 'Close',
  		listeners: 
			{
      	click: function(button, event, object) 
        {
					this.close();
        },
        scope: this
      }
  	}]
  }
});