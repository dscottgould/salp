﻿//
//
//  Multi Ticket Window
//
//



Ext.define('SALP.view.MultiTicketWindow', 
{
	//id: 'MultiTicketWindow',
	extend: 'Ext.window.Window',
	
	height: 735,
  width: 575,
  resizable: true,
  layout: 'fit',
	constrain: true,
  
  initComponent: function()
  {
  	this.setKeyMap(
   	{	
   		scope: this,
   		'ENTER': function()
   		{
    		this.close();
    	}
    });
    
  	this.items = this.buildItems();
  	
  	this.buttons = this.buildButtons();
  	
  	this.callParent(arguments);
  },
	
	
	buildItems: function()
	{
		var ticketType = this.ticketNumber.substring(0, 2).toUpperCase(),
				form = 'view.InvalidTicketPanel';
		
		
		switch(ticketType)
  	{
  		case 'SM':
  			form = 'form.ScheduledMaintenanceTicketForm';
  			break;
  			
  		case 'SR':
  			form = 'form.ServiceRequestTicketForm'
  			break;
  			
  		case 'CM':
  			form = 'form.ChangeManagementTicketForm'
  			break;
  			
  		case 'CR':
  			form = 'form.CustomerReportedTicketForm'
  			break;
  			
  		case 'SI':
  			form = 'form.ServiceIncidentTicketForm'
  			break;
  			
  		case 'OE':
  			form = 'form.OutageElementTicketForm'
  			break;
  	}
		
		if(form == 'view.InvalidTicketPanel')
		{
			this.modal = true;
		}
		
		return [
		{
			xtype: form,
			ticketNumber: this.ticketNumber
		}]
	},
	
	buildButtons: function()
  {
  	return [
  	{
  		text: 'Close',
  		listeners: 
			{
      	click: function(button, event, object) 
        {
					this.close()
        },
        scope: this
      }
  	}]
  },
	
});