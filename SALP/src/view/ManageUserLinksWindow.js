﻿//
//
//  Manage User Links Window
//
//


Ext.define('SALP.view.ManageUserLinksWindow', 
{
	extend: 'Ext.window.Window',

	id: 'ManageUserLinksWindow',
  title: 'Manage User Links',
  height: 500,
  width: 500,
  resizable: false,
  //modal: true,
  layout: 'fit',
	constrain: true,
	
	initComponent: function()
  {
    	this.items = this.buildItems();
    	this.buttons = this.buildButtons();
    	
    	this.callParent(arguments);
  },
  buildItems: function()
  {
  	return [
		{
			id: 'AllUserFavoritesGrid',
			xtype: 'grid',
			store: SALP.data.AllFavoriteLinks.store,
			stripeRows: true,
			columnLines: true,
      selType: 'checkboxmodel',
      //selModel: new Ext.grid.RowSelectionModel({multipleSelect:true}),
			flex: 1,
		  columns: [
//		  {
//        text: "ID",
//        dataIndex: 'linkId',
//        flex: 1
//    	}, 
    	{
        text: "Group",
        dataIndex: 'group',
        flex: .8
    	}, 
    	{
        text: "Description",
        dataIndex: 'description',
        flex: 1
      },
      {
        text: "Link",
        dataIndex: 'link',
        flex: 1
      }],
      listeners:
      {
  			afterrender:function(grid, options)
  			{
      		var selectionModel = grid.getSelectionModel();
      		
      		SALP.loadAllUserFavoriteLinks();
      		
      		var t=1;
      		//selectionModel.selectAll(true);
  			}
  		}
		}]
  },
  buildButtons: function()
  {
  	return [
  	{
  		text: 'Ok',
  		listeners: 
			{
      	click: function(button, event, object) 
        {
        	var selections = Ext.getCmp('AllUserFavoritesGrid').getSelection(),
							selectionsList = new Array();
					 
					for(var index = 0;index <= selections.length - 1; index ++)
					{
							selectionsList.push(selections[index].data.linkId)
					}
					 
					selectionsList = selectionsList.toString();
					
					SALP.saveUserFavoriteLinks(selectionsList);
					
					SALP.reloadTreeStores();
					
					SALP.data.FavoriteLinks.store.reload();
        	
					this.close();
        },
        scope: this
      }
  	}]
  }
});
