﻿//
//
//  RenameTabWindow.js
//
//


Ext.define('SALP.view.RenameTabWindow', 
{
	alias: 'widget.view.renametabwindow',
	extend: 'Ext.window.Window',

	id: 'renameTabWindow',
  title: 'Rename Tab',
  height: 124,
  width: 212,
  resizable: false,
  modal: true,
  layout: 'fit',
  keyHandlers: 
 	{
  	ENTER: function()
  	{
  		this.submitTabName();
  	}
  },
	
	
	initComponent: function()
  {
    	this.items = this.buildItems();
    	this.buttons = this.buildButtons();
    	
    	this.callParent(arguments);
  },
  buildItems: function()
  {
  	return [
  	{
  		xtype: 'container',
  		layout: 'vbox',
  		items: [
  		{
				xtype: 'container',
				height: 10,
				html: 'Tab Name:'
  		},
  		{
  			xtype: 'container',
  			layout: 'hbox',
  			items: [
  			{
  				id: 'tabNameField',
					xtype: 'textfield',
					width: 174,
  				padding: 10,
  				afterRender: function()
  				{
  					this.focus();
  				}
  			}]
  		}]
  	}]
  },
  buildButtons: function()
  {
  	return [
  	{
  		text: 'Ok',
  		listeners: 
			{
      	click: function(button, event, object) 
        {
					this.submitTabName();
        },
        scope: this
      }
  	}]
  },
  submitTabName: function()
  {
  	SALP.setTabName();
        	
		this.close()
  }
});

