﻿//
//
//  Kill Process Dialog
//
//

Ext.define('SALP.view.KillProcessDialog', 
{
	alias: 'widget.view.KillProcessDialog',
	extend: 'Ext.window.Window',

  title: 'Kill Process',
  iconCls: 'cogStop',
  height: 111,
  width: 302,
  resizable: false,
  modal: true,
  layout: 'fit',
  bodyPadding: '10 10 10 10',
	constrain: true,
	
	initComponent: function()
  {
    	this.items = this.buildItems();
    	
    	this.buttons = this.buildButtons();
    	
    	this.callParent(arguments);
  },
  
  buildItems: function()
  {
  	return [
  	{
  		xtype: 'container',
  		html: '<center>Are you sure you wish to cancel SessionID ' + this.record.data.sessionId + ' on Server ' + this.server + ' ?</center>'
  	}]
  },
  
  buildButtons: function()
  {
  	return [
  	{
  		text: 'Cancel',
  		listeners: 
			{
      	click: function(button, event, object) 
        {
					this.close()
        },
        scope: this
      }
  	},
  	{
  		text: 'Ok',
  		listeners: 
			{
      	click: function(button, event, object) 
        {
					this.killSession()
        },
        scope: this
      }
  	}]
  },
  
  killSession: function()
  {
  	var statusOnly = false;
  	
  	if(this.record.data.status == 'rollback')
  	{
  		statusOnly = true;
  	}
  	
  	Ext.Ajax.request(
    {
    	url: '/WebServices/Service.svc/KillProcess',
    	method: 'GET',
    	params:  
      { 
          serverName: this.server,
          sessionId: this.record.data.sessionId,
          statusOnly: statusOnly
      }, 
      scope: this,
    	success: function(response, opts) 
    	{
  			var results = Ext.decode(response.responseText); 
  			
				Ext.toast(
				{
					html:'Success: Process Killed.', 
					autoCloseDelay: 5000
				});
				
				this.store.reload();
    	},
    	failure: function(response, opts) 
    	{
				Ext.toast(
				{
					html:'FAILED: Unable Kill Process.', 
					autoCloseDelay: 5000
				});
				
				this.store.reload();
    	}
    });
  
  	this.close();
  }
});