﻿//
//
//  All Maintenance Success Failure Month Over Month Window
//
//


Ext.define('SALP.view.AllMaintenanceSuccessFailureMonthOverMonthWindow', 
{
	alias: 'widget.view.AllMaintenanceSuccessFailureMonthOverMonthWindow',
	extend: 'Ext.window.Window',

  title: 'All Maintenance Success Failure Month Over Month',
  height: 525,
  width: 780,
  resizable: true,
  maximizable: true,
  collapsible: true,
  modal: false,
  layout: 'fit',
	constrain: true,
	scrollable: true,
  
  initComponent: function()
  {
		this.items = this.buildItems();
    this.buttons = this.buildButtons();	
    
    this.callParent(arguments);
  },
  
  
  buildItems: function()
  {
  	return [
  	{
  		xtype: 'grid.AllMaintenanceSuccessFailureMonthOverMonthGrid',
  		month: this.month, 
  		year: this.year
  	}]
  },
   
  buildButtons: function()
  {
  	return [
  	{
  		text: 'Close',
  		listeners: 
			{
      	click: function(button, event, object) 
        {
					this.close();
        },
        scope: this
      }
  	}]
  },
});