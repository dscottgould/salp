﻿//
//
//  Ticket Lookup Dialog
//
//


Ext.define('SALP.view.TicketLookupDialog', 
{
	extend: 'Ext.window.Window',

	//id: 'TicketLookupDialog',
  title: 'Ticket Lookup',
  height: 140,
  width: 230,
  resizable: false,
  modal: true,
  layout: 'fit',
  bodyPadding: '10 10 10 10',
	constrain: true,
  

  initComponent: function()
  {
  	this.setKeyMap(
   	{	
   		scope: this,
   		'ENTER': function()
   		{
    		this.getTicket();
    	}
    });
  
  	this.items = this.buildItems();
  	
  	this.buttons = this.buildButtons();
  	
  	this.callParent(arguments);
  },
  
  buildItems: function()
  {
  	return [
  	{
  		xtype: 'container',
  		layout: 'vbox',
  		items: [
  		{
				xtype: 'container',
				height: 10,
				html: 'Ticket Number:'
  		},
  		{
  			xtype: 'container',
  			layout: 'hbox',
  			items: [
  			{
  				//id: 'TicketLookupField',
					xtype: 'textfield',
					width: 174,
  				padding: 10,
  				afterRender: function()
  				{
  					this.focus();
  				}
  				//value: 'OE067305260'
  			}]
  		}]
  	}]
  },
  
  buildButtons: function()
  {
  	return [
  	{
  		text: 'Ok',
  		listeners: 
			{
      	click: function(button, event, object) 
        {
					this.getTicket();
        },
        scope: this
      }
  	}]
  },
  
  getTicket: function()
  {
  	var ticketNumber = this.items.items[0].items.items[1].items.items[0].value;
  	
  	if(ticketNumber)
  	{
    	Ext.create('SALP.view.MultiTicketWindow',
    	{
    		ticketNumber: ticketNumber
    	}).show();
  	}

		this.destroy();
  }
});