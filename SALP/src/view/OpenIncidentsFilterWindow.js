﻿//
//
//  Open Incidents Filter Window
//
//

Ext.define('SALP.view.OpenIncidentsFilterWindow',
{
	extend: 'Ext.window.Window',
	
	id: 'OpenIncidentsFilterWindow',

  title: 'Set Incidents Filter',
  height: 250,
  width: 178,
  resizable: false,
  modal: true,
  layout: 'vbox',
	constrain: true,
	bodyPadding: '10 10 10 10',
	
	initComponent: function()
  {
  	this.buttons = this.buildButtons();
  	
   	this.items = this.buildItems();
   	
   	this.listeners = this.buildListeners();
    	
    this.callParent(arguments);
  },
  
  buildButtons: function()
  {
  	return [
  	{
  		text: 'Apply',
  		listeners: 
			{
      	click: function(button, event, object) 
        {
					this.startSearch()
					
					this.hide()
        },
        scope: this
      }
  	},
  	{
  		text: 'Cancel',
  		listeners: 
			{
      	click: function(button, event, object) 
        {
					this.hide()
        },
        scope: this
      }
  	}]
  },

  buildItems: function()
  {
  	return [
  	{  
			xtype: 'checkbox',
			boxLabel: 'Sev 1',
			checked: true
		},
		{  
			xtype: 'checkbox',
			boxLabel: 'Sev 2'
		},
		{  
			xtype: 'checkbox',
			boxLabel: 'Sev 3'
		},
		{  
			xtype: 'checkbox',
			boxLabel: 'Sev 4'
		},
		{  
			xtype: 'checkbox',
			boxLabel: 'Sev 5'
		},
		{  
			xtype: 'checkbox',
			boxLabel: 'Infomational'
		}]
	},
	
	buildListeners: function()
	{
		return {
			'show': function()
			{
				var searchString = Ext.util.Cookies.get('IncidentsSearch'),
						sev1Checkbox = this.items.items[0],
						sev2Checkbox = this.items.items[1],
						sev3Checkbox = this.items.items[2],
						sev4Checkbox = this.items.items[3],
						sev5Checkbox = this.items.items[4],
						infCheckbox = this.items.items[5];
						
						
				if(searchString.indexOf('1') >= 0)
				{
					sev5Checkbox.setValue(true);
				}
				else
				{
					sev5Checkbox.setValue(false);
				}
				
				if(searchString.indexOf('2') >= 0)
				{
					sev4Checkbox.setValue(true);
				}
				else
				{
					sev4Checkbox.setValue(false);
				}
				
				if(searchString.indexOf('3') >= 0)
				{
					sev3Checkbox.setValue(true);
				}
				else
				{
					sev3Checkbox.setValue(false);
				}
				
				if(searchString.indexOf('4') >= 0)
				{
					sev2Checkbox.setValue(true);
				}
				else
				{
					sev2Checkbox.setValue(false);
				}
				
				if(searchString.indexOf('5') >= 0)
				{
					sev1Checkbox.setValue(true);
				}
				else
				{
					sev1Checkbox.setValue(false);
				}
				
				if(searchString.indexOf('0') >= 0)
				{
					infCheckbox.setValue(true);
				}
				else
				{
					infCheckbox.setValue(false);
				}
				
				if(sev5Checkbox.value == false && sev4Checkbox.value == false && sev3Checkbox.value == false && sev2Checkbox.value == false && sev1Checkbox.value == false, infCheckbox.value == false)
				{
					sev1Checkbox.setValue(true);
				}
			}
		}
	},
	
	startSearch: function()
	{
		var searchString = "",
				sev1Checkbox = this.items.items[0],
				sev2Checkbox = this.items.items[1],
				sev3Checkbox = this.items.items[2],
				sev4Checkbox = this.items.items[3],
				sev5Checkbox = this.items.items[4],
				infCheckbox = this.items.items[5];
			
		if(sev1Checkbox.checked)
		{
			searchString += '5';
		}
		
		if(sev2Checkbox.checked)
		{
			if(searchString.length > 0)
			{
				searchString += ',';
			}
			
			searchString += '4';
		}
		
		if(sev3Checkbox.checked)
		{
			if(searchString.length > 0)
			{
				searchString += ',';
			}
			
			searchString += '3';
		}
		
		if(sev4Checkbox.checked)
		{
			if(searchString.length > 0)
			{
				searchString += ',';
			}
			
			searchString += '2';
		}
		
		if(sev5Checkbox.checked)
		{
			if(searchString.length > 0)
			{
				searchString += ',';
			}
			
			searchString += '1';
		}
		
		if(infCheckbox.checked)
		{
			if(searchString.length > 0)
			{
				searchString += ',';
			}
			
			searchString += '0';
		}
		
		this.grid.getStore().load(
		{
			params:
			{
				severity: searchString
			}
		})
		
		Ext.util.Cookies.clear('IncidentsSearch');
		
		Ext.util.Cookies.set('IncidentsSearch', searchString, new Date("January 1, 2020"));
		
		var t=1;
	}
});