﻿//
//
//  User Lookup Window
//
//



Ext.define('SALP.view.UserLookupWindow', 
{
	extend: 'Ext.window.Window',
	
	//id: 'UserLookupWindow',
	
	height: 660,
  width: 705,
  resizable: true,
  maximizable: false,
	constrain: true,
	collapsible: true,
	
	layout: 
	{
  	type: 'vbox',
    align: 'stretch'
    //pack: 'center'
	},
	
	iconCls: 'user',
	title: 'SAP User Lookup',
	
  
  initComponent: function()
  {
  	
  	this.items = this.buildItems();
  	
  	this.buttons = this.buildButtons();
  	
  	//this.listeners = this.buildListeners();
  	
  	this.callParent(arguments);
  },
  
  
  buildItems: function()
  {
  	return [
  	{
  		xtype: 'view.UserLookupPanel',
  		height: 100
  	},
  	{
  		xtype: 'grid.UserLookupGrid',
  		height: 495,
  		scrollable: true,
  	}]
  }, 
  
  buildButtons: function()
  {
  	return [
  	{
  		text: 'Close',
  		listeners: 
			{
      	click: function(button, event, object) 
        {
					this.close()
        },
        scope: this
      }
  	}]
  }
});