﻿//
//
//  DashboardNameWindow.js
//
//


Ext.define('SALP.view.DashboardNameWindow', 
{
	alias: 'widget.view.dashboardnamewindow',
	extend: 'Ext.window.Window',

	id: 'urlWindow',
  title: 'Dashboard Name',
  height: 166,
  width: 217,
  resizable: true,
  modal: true,
  layout: 'fit',
  bodyPadding: '10 10 10 10',
	constrain: true,
  
  initComponent: function()
  {
  	this.setKeyMap(
   	{	
   		scope: this,
   		'ENTER': function()
   		{
    		this.saveDashboard();
    	}
    });
    
		this.items = this.buildItems();
    this.buttons = this.buildButtons();	
    
    this.callParent(arguments);
  },
  buildItems: function()
  {
  	return [
  	{
  		xtype: 'container',
  		layout: 'vbox',
  		items: [
  		{
  			id: 'DashboardName',
  			xtype: 'textfield',
  			value: SALP.rawCookie.dashboardName,
  			afterRender: function()
  			{
  				this.focus();
  			}
  		},
  		{
  			xtype: 'container',
  			padding: '10 0 0 0',
  			html: 'Changing the name will</br> create a New Dashboard.'
  		}
  		]
  	}]
  },
  buildButtons: function()
  {
  	return [
  	{
  		text: 'Ok',
  		listeners: 
			{
      	click: function(button, event, object) 
        {
					this.saveDashboard();
        },
        scope: this
      }
  	}]
  },
  saveDashboard: function()
  {
  	var dashboardName = Ext.getCmp('DashboardName').getValue();
  	
  	SALP.rawCookie.dashboardName = dashboardName;
        	
    SALP.saveSlots(dashboardName);
        	
		this.close()
  }
});