﻿//
//
//  Delete Favorite Link Window
//
//

Ext.define('SALP.view.DeleteFavoriteLinkWindow', 
{
	extend: 'Ext.window.Window',

  height: 130,
  width: 290,
  resizable: false,
  modal: true,
  closable: true,
  layout: 'fit',
  iconCls: 'deleteLink',
  title: 'Delete Favorite Link',
	constrain: true,

  
  initComponent: function()
  {
  	this.setKeyMap(
   	{	
   		scope: this,
   		'ENTER': function()
   		{
    		this.deleteLink();
    	}
    });
    
		this.items = this.buildItems();
		
    this.buttons = this.buildButtons();	
    
    this.callParent(arguments);
  },
  

	buildItems: function()
	{
		return [
		{
			xtype: 'container',
			padding: '10 10 10 10',
			html: 'Are you sure you wish to delete ' + this.record.data.description + '?'
		}]
	},
  buildButtons: function()
  {
  	return [
  	{
  		text: 'Cancel',
  		listeners: 
			{
      	click: function(button, event, object) 
        {
        	this.close();
        },
        scope: this
      }
  	},
  	{
  		text: 'Ok',
  		listeners: 
			{
      	click: function(button, event, object) 
        {
					this.deleteLink();
        },
        scope: this
      }
  	}]
  },
  deleteLink: function()
  {
  	this.close();
  					
  	Ext.Ajax.request(
  	{
  		url: '/WebServices/Service.svc/DeleteUserFavoriteLink',
  		method: 'GET',
  		params:  
    	{ 
				linkId: this.record.data.linkId
    	}, 
  		success: function(response, opts) 
  		{
				Ext.toast('SUCCESS: Link Removed');
				
				SALP.data.FavoriteLinks.store.reload();
				
				SALP.reloadTreeStores();
				
  		},
  		failure: function(response, opts) 
  		{
  			Ext.toast('FAILED: Unable to remove link.');
  		}
  	});
  }
});