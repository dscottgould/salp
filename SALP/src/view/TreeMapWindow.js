﻿//
//
//  Org TreeMap Window
//
//



Ext.define('SALP.view.TreeMapWindow', 
{
	extend: 'Ext.window.Window',
	
	id: 'TreeMapWindow',
	
	height: 800,
  width: 900,
  resizable: true,
  maximizable: true,
  layout: 'fit',
	constrain: true,
	
	iconCls: 'D3TreeMap',
	title: 'Maintenance Success/Failure By Hierarchy',
	
  
  initComponent: function()
  {
  	this.setKeyMap(
   	{	
   		scope: this,
   		'ENTER': function()
   		{
    		this.loadStore();
    	}
    });
  	
  	this.header = this.buildHeader();
  	
  	this.items = this.buildItems();
  	
  	this.buttons = this.buildButtons();
  	
  	this.listeners = this.buildListeners();
  	
  	this.callParent(arguments);
  },
	
  buildHeader: function()
  {
  	return {
  		itemPosition: 1, // after title before collapse tool
      items: [
      {
      	xtype: 'button',
        iconCls: 'arrowDown',
        tooltip: 'Move Down',
        //text: 'Drill Down',
        listeners: 
				{
        	click: function(button, event, opts) 
         	{
						this.loadStore();
								
						var t=1;
         	},
          scope: this
				}
      },
      {
      	xtype: 'container',
      	width: 10
      },
      {
      	xtype: 'button',
      	iconCls: 'arrowUp',
      	tooltip: 'Move Up',
        //text: 'Back',
        listeners: 
				{
        	click: function(button, event, opts) 
         	{
						this.backStore();
         	},
          scope: this
				}
      },
      {
      	xtype: 'container',
      	width: 10
      },
     	{
      	xtype: 'button',
        iconCls: '',
        tooltip: 'Set Default',
        text: 'Set Default',
        listeners: 
				{
        	click: function(button, event, opts) 
         	{
						var chart = this.items.items[0],
								selection = chart.getSelection().data.text,
								date = new Date("January 1, 2020");
								
						Ext.util.Cookies.set('TreeMapUserName', selection, date);
         	},
          scope: this
				}
      },
      {
      	xtype: 'container',
      	width: 10
      },
      {
      	xtype: 'button',
        text: 'Reset',
        listeners: 
				{
        	click: function(button, event, opts) 
         	{
						this.clearStore();
         	},
          scope: this
				}
      },
      {
      	xtype: 'container',
      	width: 10
      },
      {
      	xtype: 'button',
        text: 'Get Details',
        listeners: 
				{
        	click: function(button, event, opts) 
         	{
         		var chart = this.items.items[0],
								selection = chart.getSelection(),
								month = this.getHeader().items.items[11].getValue(),
								year = this.getHeader().items.items[13].getValue();
         		
         		if(selection)
         		{         				
         			if(selection.data.text.indexOf(',') >= 0)
         			{
         				var userName = chart.getSelection().data.text
         				
           			Ext.create('SALP.view.CMDetailByHierarchyWindow',
  							{
  								userName: userName,
  								month: month, 
  								year: year,
  								reportLevel: 'All'
  							}).show();
         			}
         			
         			if(selection.data.text.indexOf('Success') >= 0)
         			{
         				Ext.create('SALP.view.CMDetailByHierarchyWindow',
  							{
  								userName: selection.data.userName,
  								month: month, 
  								year: year,
  								reportLevel: 'Success'
  							}).show();
         			}

         			if(selection.data.text.indexOf('Failed') >= 0)
         			{
         				Ext.create('SALP.view.CMDetailByHierarchyWindow',
  							{
  								userName: selection.data.userName,
  								month: month, 
  								year: year,
  								reportLevel: 'Failed'
  							}).show();
         			}
         		}
         	},
          scope: this
				}
      },
     	{
      	xtype: 'container',
      	width: 10
      },
      {
				xtype: 'combobox',
				store: Ext.data.StoreManager.lookup('selectMonth'),
				queryMode: 'local',
				displayField: 'displayName',
				valueField: 'submitValue',
				width: 75,
				value: new Date().getMonth() + 1,
    		listeners : 
    		{
    			select : function(comboBox, dataModel, object) 
    			{
						var t=1;
      		},
      		scope: this
      	}
    	},
    	{
      	xtype: 'container',
      	width: 10
      },
      {
				xtype: 'combobox',
				store: Ext.data.StoreManager.lookup('selectDay'),
				queryMode: 'local',
				displayField: 'displayName',
				valueField: 'displayName',
				width: 60,
				value: new Date().getFullYear(),
    		listeners : 
    		{
    			select : function(comboBox, dataModel, object) 
    			{
						var t=1;
      		},
      		scope: this
      	}
    	},
    	{
      	xtype: 'container',
      	width: 10
      },
      {
      	xtype: 'button',
        text: 'Update',
        listeners: 
				{
        	click: function(button, event, opts) 
         	{
						
						
						this.loadStore();
         	},
          scope: this
				}
      },
      {
      	xtype: 'container',
      	width: 50
      }]
  	}
  },
	
	buildItems: function()
	{		
		return [
		{
			xtype: 'd3.OrganizationTreeMap',
    	nodeValue: function(node)
    	{
    		return node.data.value;
    	}
		}]
	},
	
	buildButtons: function()
  {
  	return [
  	{
  		text: 'Close',
  		listeners: 
			{
      	click: function(button, event, object) 
        {
					this.close()
        },
        scope: this
      }
  	}]
  },
  
  buildListeners: function()
  {
  	return {
  		'show': function(object, options)
  		{
  			var chart = this.items.items[0],
  					selectedUser = Ext.util.Cookies.get('TreeMapUserName');
  			
  			
  			//this.setLoading(true);

				chart.getStore().load
      	({
      		params:
      		{
    				name: selectedUser
    				//month: month, 
    				//year: year
      		},
      		callback : function(records, options, success) 
      		{
    				this.setLoading(false);
          }, 
          scope: this
      	});

  		},
  		scope: this
  	}
  },
  
  clearStore: function()
  {
  	var chart = this.items.items[0];
  	
  	Ext.util.Cookies.clear('TreeMapUserName');
  	
  	this.setLoading(true);
			
  	chart.getStore().load
  	({
  		params:
  		{
  			//name: selection,
  			//month: month, 
  			//year: year
  		},
  		callback : function(records, options, success) 
  		{
        if (success) 
        {
					this.setLoading(false);
					
					var t=1;
        }
      }, 
      scope: this
  	});
  },
  
  loadStore: function()
  {
		var chart = this.items.items[0],
				selection = chart.getSelection()
				month = this.getHeader().items.items[11].getValue(),
				year = this.getHeader().items.items[13].getValue();
				
				
		if(selection)
		{
			selection = selection.data.text;
			
  		if((selection.indexOf(',')> 0) && (month > 0) && (year > 0))
  		{
  			this.setLoading(true);
  			
      	chart.getStore().load
      	({
      		params:
      		{
      			name: selection,
      			month: month, 
      			year: year
      		},
      		callback : function(records, options, success) 
      		{
            if (success) 
            {
  						this.setLoading(false);
  						
  						var t=1;
            }
          }, 
          scope: this
      	});
  		}
		}
		else
		{
			Ext.toast('Please select an User to update')		
		}	
  },
  
  backStore: function()
  {
  	var chart = this.items.items[0],
  			supervisor = chart.getStore().getRange()[0].data.supervisor,
				month = this.getHeader().items.items[11].getValue(),
				year = this.getHeader().items.items[13].getValue();
		
		

		if((supervisor.indexOf(',')> 0) && (month > 0) && (year > 0))
		{
			this.setLoading(true);
			
    	chart.getStore().load
    	({
    		params:
    		{
    			name: supervisor,
    			month: month, 
    			year: year
    		},
    		callback : function(records, options, success) 
    		{
          if (success) 
          {
						var chart = this.items.items[0];
						
						this.setLoading(false);
						
						chart.setSelection(chart.getStore().getRange()[0]);
						
						var t=1;
          }
        }, 
        scope: this
    	});
		}
  }
});