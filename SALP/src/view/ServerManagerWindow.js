﻿//
//
//  Server Manager Window
//
//


Ext.define('SALP.view.ServerManagerWindow', 
{
	extend: 'Ext.window.Window',

	title: 'SALP Server Manager',
	iconCls: 'server',
  height: 325,
  width: 522,
  resizable: true,
  modal: false,
  layout: 'fit',
  constrain: true,
  
  initComponent: function()
  {
  	this.items = this.buildItems();
    this.buttons = this.buildButtons();
    
    	
    this.callParent(arguments);
  },
  
  buildItems: function()
  {
  	return [
  	{
  		xtype: 'grid.ServerManagerGrid'
  	}]
  },
  buildButtons: function()
  {
  	return [
  	{
  		text: 'Ok',
  		listeners: 
			{
      	click: function(button, event, object) 
        {
					this.close();
        },
        scope: this
      }
  	}]
  }
  
});