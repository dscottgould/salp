﻿//
//
//  Haystack Filters Window
//
//

Ext.define('SALP.view.HaystackFiltersWindow', 
{
	alias: 'widget.view.HaystackFiltersWindow',
	extend: 'Ext.window.Window',
	
	id: 'haystackFiltersWindow',
  title: 'Haystack Filters',
  height: 410,
  width: 460,
  resizable: false,
  modal: false,
  layout: 'fit',
	constrain: true,
	
	initComponent: function()
  {
    
    this.items = this.buildItems();
    this.buttons = this.buildButtons();
    this.listeners = this.buildListeners();
    	
    this.callParent(arguments);
  },
  
	buildItems: function()
	{
		return [
		{
			xtype: 'form',
			layout: 'vbox',
			items: [
			{
				xtype: 'textfield',
				name: 'filterName',
				fieldLabel: 'Filter Name',
				allowBlank: false,
				padding: 5
			},
			{
				xtype: 'combobox',
				name: 'division',
				fieldLabel: 'Division',
				padding: 5,
				store: Ext.data.StoreManager.lookup('selectDivision'),
    		queryMode: 'local',
    		displayField: 'division',
    		valueField: 'division',
    		multiSelect: true,
    		allowBlank: false
			},
			{
				xtype: 'textfield',
				fieldLabel: 'User Name',
				name: 'userName',
				allowBlank: false,
				padding: 5
			},
			{
				xtype: 'textfield',
				fieldLabel: 'Password',
				name: 'password',
				inputType: 'password',
				allowBlank: false,
				padding: 5
			},
			{
				xtype: 'container',
				layout: 'hbox',
				items: [
				{
					xtype: 'container',
					layout: 'fit',
					width: 375,
					items: [
  				{
  					id: 'fileField',
    				xtype: 'filefield',
    				name: 'nodes',
    				fieldLabel: 'Node File',
    				padding: 5,
    				//allowBlank: false,
    				buttonText: 'Select CSV File...',
    				listeners: {
    					change: function(fileField, fileName)
    					{
    						var form = this.up('form').getForm(),
    								file = this.fileInputEl.dom.files[0],
    								reader = new FileReader();
    						
    						reader.onload = function(e)
    						{
    							var contents = e.target.result;
    							
    							contents = contents.replace(new RegExp('\r\n', 'gi'), ',');
    							contents = contents.replace(new RegExp('\r', 'gi'), ',');
    							contents = contents.replace(new RegExp(' ', 'gi'), '');
    							
    							Ext.getCmp('fileContents').setValue(contents)
    						}
    						
    						reader.readAsText(file);
    						
    						var t=1;
    					},
    					//scope: this
    				}
    			}]
				}]
			},
		  {
				id: 'fileContents',
				name: 'nodes',
				xtype: 'textarea',
				fieldLabel: 'Node List',
				width: 300,
				padding: 5,
				allowBlank: false
			}]
		}]
	},
	
  buildButtons: function()
  {
  	return [
  	{
  		text: 'Cancel',
  		listeners: 
			{
      	click: function(button, event, object) 
        {
        	this.close();
      	},
      	scope: this
      }
  	},
  	{
  		text: 'Ok',
  		listeners: 
			{
      	click: function(button, event, object) 
        {
        	var form = this.down('form').getForm(),
        			userName = form.findField('userName').getValue(),
        			password = form.findField('password').getValue(),
        			filterName = form.findField('filterName').getValue(),
        			division = form.findField('division').getValue(),
        			nodes = Ext.getCmp('fileContents').getValue();
        	
        	
        	if(form.isValid())
        	{
  					Ext.Ajax.request(
          	{
          		url: '/WebServices/Service.svc/SaveHaystackFilter',
          		method: 'GET',
          		params:  
            	{ 
              	  accountName: SALP.user.accountName,
              	  userName: userName,
              	  password: password,
              	  filterName: filterName,
              	  division: division,
              	  nodes: nodes
            	}, 
          		success: function(response, opts) 
          		{
          			var returnData = Ext.decode(response.responseText);
          			
          								
								this.close();
          			
  							var t=1;
          		},
          		failure: function(response, opts) 
          		{
          			var t=1;
          		},
          		scope: this
          	});
					}
        },
        scope: this
      }
  	}]
  },
  
  buildListeners: function()
  {
  	return {
  		'show': function(object, options)
  		{
  			//this.setLoading(true);

				Ext.data.StoreManager.lookup('selectDivision').load
      	({
      		params:
      		{
    				username: SALP.user.accountName
      		},
      		callback : function(records, options, success) 
      		{
    				this.setLoading(false);
          }, 
          scope: this
      	});
  		},
  		scope: this
  	}
  }
});