﻿//
//
//  AddTabWindow.js
//
//


Ext.define('SALP.view.AddTabWindow', 
{
	alias: 'widget.view.addtabwindow',
	extend: 'Ext.window.Window',

	id: 'addTabWindow',
  title: 'Add Tab',
  height: 124,
  width: 212,
  resizable: false,
  modal: true,
  layout: 'fit',
	constrain: true,
	
	initComponent: function()
  {
 		this.setKeyMap(
   	{	
   		scope: this,
   		'ENTER': function()
   		{
    		this.addTab();
    	}
    });
    
    this.items = this.buildItems();
    this.buttons = this.buildButtons();
    	
    this.callParent(arguments);
  },
  
  buildItems: function()
  {
  	return [
  	{
  		xtype: 'container',
  		layout: 'vbox',
  		items: [
  		{
				xtype: 'container',
				height: 10,
				html: 'Tab Name:'
  		},
  		{
  			xtype: 'container',
  			layout: 'hbox',
  			items: [
  			{
  				id: 'tabNameField',
					xtype: 'textfield',
					width: 174,
  				padding: 10,
  				afterRender: function()
  				{
  					this.focus();
  				}
  			}]
  		}
  		]
  	}]
  },
  buildButtons: function()
  {
  	return [
  	{
  		text: 'Ok',
  		listeners: 
			{
      	click: function(button, event, object) 
        {
					this.addTab(button, event, object) 
        },
        scope: this
      }
  	}]
  },
  addTab: function(button, event, object) 
  {
  	SALP.addTab(button, event, object);
        	
		this.close()
  }
});

