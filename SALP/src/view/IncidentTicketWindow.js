﻿//
//
//  Incident Ticket Window
//
//

Ext.define('SALP.view.IncidentTicketWindow', 
{
	extend: 'Ext.window.Window',

  height: 650,
  width: 800,
  resizable: true,
  modal: false,
  closable: true,
  collapsible: true,
  layout: 'fit',
  keyHandlers: 
 	{
  	ENTER: function()
  	{
  		this.submitTicket();
  	}
  },

  
  initComponent: function()
  {
  	this.setKeyMap(
   	{	
   		scope: this,
   		'ENTER': function()
   		{
    		this.submitTicket();
    	}
    });
    
		this.items = this.buildItems();
		
    this.buttons = this.buildButtons();	
    
    this.callParent(arguments);
  },

	buildItems: function()
	{
		return [
		{
			xtype: 'container',
			layout: 'hbox',
			items: [
			{
				xtype: 'form.IncidentTicketAForm',
				flex: 1
			},
			{
				xtype: 'form.IncidentTicketBForm',
				flex: 1
			}]
		}]
	},
  buildButtons: function()
  {
  	return [
  	{
  		text: 'Cancel',
  		listeners: 
			{
      	click: function(button, event, object) 
        {
        	this.close();
        },
        scope: this
      }
  	},
  	{
  		text: 'Ok',
  		listeners: 
			{
      	click: function(button, event, object) 
        {
					this.submitTicket();
        },
        scope: this
      }
  	}]
  },
  submitTicket: function()
  {
  	var form = this.items.items[0].items.items[1];
  	
  	if (form.isValid)
  	{
    	form.submit
    	({
  			url: '/WebServices/Service.svc/UpdateIncidentTicket',
    		method: 'GET',
    		scope: this,
    		success: function()
  			{
  				Ext.toast('SUCCESS: Ticket Updated.');
  				
  				Ext.getCmp('IncidentTicketsGrid').store.reload();
  				
  				this.close();
  			},	
  			failure: function()
  			{
  				Ext.toast('FAILED: Unable To Update Ticket.');
  			}
    	})
    }
  }
});