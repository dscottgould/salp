﻿//
//
//  HelpWindow.js
//
//


Ext.define('SALP.view.HelpWindow', 
{
	alias: 'widget.view.helpwindow',
	extend: 'Ext.window.Window',

	id: 'helpWindow',
  title: 'User Guide',
  height: 685,
  width: 570,
  resizable: false,
  modal: false,
  layout: 'fit',
	constrain: true,
  
  
  initComponent: function()
  {
  	this.setKeyMap(
   	{	
   		scope: this,
   		'ENTER': function()
   		{
    		this.close();
    	}
    });
  	
  	this.header = this.buildHeader();
  	
		this.items = this.buildItems();
		
    this.buttons = this.buildButtons();	
    
    this.callParent(arguments);
  },
  
  
  buildHeader: function()
  {
  	return {
  		itemPosition: 1, // after title before collapse tool
      items: [
      {
      	xtype: 'button',
        text: 'To Top',
        listeners: 
				{
        	click: function(button, event, opts) 
         	{
						var helpWindow = button.up().up();
								
						helpWindow.items.items[0].scrollTo(0,0);
         	},
          scope: this
				}
      }]
  	}
  },
  
  buildItems: function()
  {
  	return [
  	{
  		xtype: 'panel',
  		autoScroll: true,
  		loader: 
  		{
      	url: '/src/help/help.html',
        autoLoad: true,
        scripts: true
     	}
  	}]
  },
  buildButtons: function()
  {
  	return [
  	{
  		text: 'Close',
  		listeners: 
			{
      	click: function(button, event, object) 
        {
					this.close();
        },
        scope: this
      }
  	}]
  }
});