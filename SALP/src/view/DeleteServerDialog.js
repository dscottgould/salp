﻿//
//
//  Delete Server Dialog
//
//

Ext.define('SALP.view.DeleteServerDialog', 
{
	extend: 'Ext.window.Window',

  height: 130,
  width: 290,
  resizable: false,
  modal: true,
  closable: true,
  layout: 'fit',
  iconCls: 'serverDelete',
  title: 'Delete Server',
	constrain: true,

  
  initComponent: function()
  {
		this.setKeyMap(
   	{	
   		scope: this,
   		'ENTER': function()
   		{
    		this.deleteServer();
    	}
    });
		
		this.items = this.buildItems();
		
    this.buttons = this.buildButtons();	
    
    this.callParent(arguments);
  },
  

	buildItems: function()
	{
		return [
		{
			xtype: 'container',
			padding: '10 10 10 10',
			html: 'Are you sure you wish to delete ' + this.record.data.serverName + '?'
		}]
	},
  buildButtons: function()
  {
  	return [
  	{
  		text: 'Cancel',
  		listeners: 
			{
      	click: function(button, event, object) 
        {
        	this.close();
        },
        scope: this
      }
  	},
  	{
  		text: 'Ok',
  		listeners: 
			{
      	click: function(button, event, object) 
        {
					this.deleteServer();
        },
        scope: this
      }
  	}]
  },
  
  deleteServer: function()
  {
  	this.close();
  					
  	Ext.Ajax.request(
  	{
  		url: '/WebServices/Service.svc/DeleteServer',
  		method: 'GET',
  		params:  
    	{ 
				id: this.record.data.id
    	}, 
    	scope: this,
  		success: function(response, opts) 
  		{
				Ext.toast('SUCCESS: Server Removed');
				
    		this.grid.getStore().reload();
    			
    		Ext.getCmp('ServerSelect').getStore().reload();
				
  		},
  		failure: function(response, opts) 
  		{
  			Ext.toast('FAILED: Unable to remove server.');
  		}
  	});
  }
});