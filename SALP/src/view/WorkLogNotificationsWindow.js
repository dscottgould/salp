﻿//
//
//  Worklog Notifications Window
//
//


Ext.define('SALP.view.WorklogNotificationsWindow', 
{
	extend: 'Ext.window.Window',
	title: 'Notification Log',
	height: 500,
  width: 500,
  resizable: true,
  constrain: true,
  scrollable: true,
  constrain: true,
	
	initComponent: function()
  {
		//this.items = this.buildItems();
		
		this.listeners = this.buildListeners();
		
    this.callParent(arguments);
  },
	
	
	buildItems: function()
  {
  	return [
  	{
  		html: 'HERE'
  	}]
  },
  
  buildListeners: function()
  {
  	return {
  		'show': function(object, options)
  		{
  			this.setLoading('Loading...');	
  			
  			Ext.Ajax.request(
        {
        	url: '/WebServices/Service.svc/GetWorkLogNotifications',
        	method: 'GET',
        	params:  
          { 
              ticketNumber: this.ticketNumber,
              eventType: this.subject
          }, 
          scope: this,
        	success: function(response, opts) 
        	{
						var returnData = Ext.decode(response.responseText);
						
						for(var index = 0; index <= returnData.data.length - 1; index ++)
						{
							var message = returnData.data[index].message,
									messageContainer = Ext.create('Ext.panel.Panel',
									{
										html: message,
										bodyPadding: '10 10 10 10',
									});
							
							this.add(messageContainer);
							
							var t=1;
						}
						
						this.setLoading(false);
        	},
        	failure: function(response, opts) 
        	{
        		var t=1;
      		}
        });
  				
  		},
  		scope: this
  	}
  }
});
