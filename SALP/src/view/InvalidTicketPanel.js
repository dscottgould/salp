﻿//
//
//  Ticket Lookup Dialog
//
//


Ext.define('SALP.view.InvalidTicketPanel', 
{
	alias: 'widget.view.InvalidTicketPanel',
	extend: 'Ext.container.Container',
	
	cls: 'invalidTicket',
	
	html: '<center>Invalid Ticket</center>',
	constrain: true,
	
  initComponent: function()
  {
		this.up().height = 106;
		this.up().width = 200
  	
  	this.callParent(arguments);
  }
  
});