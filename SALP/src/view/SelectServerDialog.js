﻿//
//
//  Select Server Dialog
//
//

Ext.define('SALP.view.SelectServerDialog', 
{
	alias: 'widget.view.SelectServerDialog',
	extend: 'Ext.window.Window',

  title: 'Select Server',
  iconCls: 'cog',
  height: 96,
  width: 187,
  resizable: false,
  modal: true,
  //layout: 'vbox',
  bodyPadding: '10 10 10 10',
  constrain: true,
	
	
	initComponent: function()
  {
    	this.items = this.buildItems();
 
    	this.callParent(arguments);
  },
  
  buildItems: function()
  {
  	var store = SALP.data.ServersProcesses.store;
  	
  	if(this.caller.items.items[0].$className == 'SALP.portlets.LongRunningJobsPortlet')
  	{
  		store = SALP.data.ServersJobs.store;
  	}
  	
  	return [
  	{
  		id: 'ServerSelect',
  		xtype: 'combobox',
    	store: store,
    	queryMode: 'local',
    	displayField: 'serverName',
    	valueField: 'serverName',
    	listeners: [
    	{
    		scope: this,
    		select: function(combo, record, options)
    		{
    			var object = this.caller.items.items[0].items.items[0],
    					server = record.data.serverName.trim();
    			
    			
    			switch(this.caller.items.items[0].$className)
    			{
    				case 'SALP.portlets.CurrentServerProcessesPortlet':
    					this.caller.setTitle('Server Current Processes - ' + server);
    					break;
    				
    				case 'SALP.portlets.LongRunningJobsPortlet':
    					this.caller.setTitle('Long Running Jobs - ' + server);
    					break;
    					
    				case 'SALP.portlets.ServerCPUUsagePortlet':
    					this.caller.setTitle('Server CPU Usage - ' + server);
    					break;
    			}
    			
    			
    			this.caller.items.items[0].props.server = server;
    			
    			object.server = server;
    			
    			object.getStore().load
      		({
      			params:
      			{
      				serverName: server
      			}
      		});
      		
      		this.close();
    		}
    	}]
  	},
  	{
  		xtype: 'container',
  		flex: 1,
  		layout: 
  		{
				type: 'hbox',
				align: 'stretch'
			},
  		items: [
  		{
  			xtype: 'container',
  			width: 60
  		},
  		{
  			xtype: 'button',
  			text: 'Server Manager',
  			handler: function()
  			{
  				Ext.create('SALP.view.ServerManagerWindow',
  				{
  					caller: this
  				}).show();
  			}
  		}]
  	}]
  }
});