﻿//
//
//	Incident Tickets Window
//
//

Ext.define('SALP.view.IncidentTicketsWindow', 
{
	extend: 'Ext.window.Window',

	id: 'IncidentTicketWindow',
  title: 'View Incident Tickets',
  height: 500,
  width: window.innerWidth,
//  x: 75,
//  y: 605,
  resizable: true,
  maximizable: true,
  collapsible: true,
  modal: false,
  closable: true,
  layout: 'fit',
 	constrain: true,
  
  initComponent: function()
  {
		this.items = this.buildItems();
		
    this.callParent(arguments);
  },
  
  buildItems: function()
  {
  	return [
  	{
  		xtype: 'grid.IncidentTicketsGrid'
  	}]
  }
});