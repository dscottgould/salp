﻿//
//
//  User Lookup Panel
//
//

Ext.define('SALP.view.UserLookupPanel', 
{
	alias: 'widget.view.UserLookupPanel',
	extend: 'Ext.panel.Panel',
	
	height: 100,
	border: false,
	      	
	
	initComponent: function()
  {
  	this.setKeyMap(
   	{	
   		scope: this,
   		'ENTER': function()
   		{
    		this.submitSearch();
    	}
    });
    	
  	this.items = this.buildItems();
    	
    this.callParent(arguments);
  },
  
  buildItems: function()
  {
  	return [
  	{
  		xtype: 'container',
    	layout: 
    	{
      	type: 'vbox',
        align: 'stretch'
        //pack: 'center'
    	},
  		items: [
  		{
  			xtype: 'container',
  			layout: 'hbox',
  			padding: 10,
  			items: [
  			{
  				xtype: 'textfield',
  				fieldLabel: 'Name',
  				labelWidth: 50,
  				listeners: [
  				{
  					'change': function(field, newValue, oldValue, options)
  					{
  						if (newValue.length > 0)
  						{
  							var checkbox = this.items.items[0].items.items[0].items.items[1];
  							
  							checkbox.setValue(false);
  						}
  					}, 
  					scope: this
  				}]
  			},
  			{
  				xtype: 'checkbox',
  				padding: '0 5 5 5',
  				checked: true,
  				listeners: [
  				{
  					'change': function(checkbox, newValue, oldValue, options)
  					{
  						if (newValue == true)
  						{
  							var field = this.items.items[0].items.items[0].items.items[0];
  							
  							field.setValue('');
  						}
  					}, 
  					scope: this
  				}]
  			},
  			{
  				xtype: 'container',
  				html: 'NULL',
  				padding: '5 5 7 0'
  			},
  			{
  				xtype: 'container',
  				width: 10
  			},
  			{
  				xtype: 'textfield',
  				fieldLabel: 'Level (EXEC, VP, DIR, MGR...)',
  				labelWidth: 180,
  				listeners: [
  				{
  					'change': function(field, newValue, oldValue, options)
  					{
  						if (newValue.length > 0)
  						{
  							var checkbox = this.items.items[0].items.items[0].items.items[5];
  							
  							checkbox.setValue(false);
  						}
  					}, 
  					scope: this
  				}]
		  	},
		  	{
  				xtype: 'checkbox',
  				padding: '0 5 5 5',
  				checked: true,
  				listeners: [
  				{
  					'change': function(checkbox, newValue, oldValue, options)
  					{
  						if (newValue == true)
  						{
  							var field = this.items.items[0].items.items[0].items.items[4];
  							
  							field.setValue('');
  						}
  					}, 
  					scope: this
  				}]
  			},
  			{
  				xtype: 'container',
  				html: 'NULL',
  				padding: '5 5 7 0'
  			}]
  		},
  		{
  			xtype: 'container',
  			layout: 'hbox',
  			padding: 10,
  			items: [
  			{
  				xtype: 'textfield',
  				fieldLabel: 'Unit',
  				labelWidth: 50,
  				listeners: [
  				{
  					'change': function(field, newValue, oldValue, options)
  					{
  						if (newValue.length > 0)
  						{
  							var checkbox = this.items.items[0].items.items[1].items.items[1];
  							
  							checkbox.setValue(false);
  						}
  					}, 
  					scope: this
  				}]
  			},
  			{
  				xtype: 'checkbox',
  				padding: '0 5 5 5',
  				checked: true,
  				listeners: [
  				{
  					'change': function(checkbox, newValue, oldValue, options)
  					{
  						if (newValue == true)
  						{
  							var field = this.items.items[0].items.items[1].items.items[0];
  							
  							field.setValue('');
  						}
  					}, 
  					scope: this
  				}]
  			},
			 	{
  				xtype: 'container',
  				html: 'NULL',
  				padding: '5 5 7 0'
  			},
  			{
  				xtype: 'container',
  				width: 250
  			},
  			{
  				xtype: 'button',
  				text: 'Start Search',
  				listeners: [
  				{
  					'click': this.submitSearch,
  					scope: this
  				}]
  			},
  			{
  				xtype: 'container',
  				width: 10
  			},
  			{
  				xtype: 'button',
  				text: 'Download',
  				listeners: [
  				{
  					'click': function(button, event, options)
  					{
  						var grid = this.up().items.items[1];
  						
    					grid.saveDocumentAs(
  						{
      					type: 'xlsx',
      					title: 'SP Users',
      					fileName: 'SPUsers.xlsx'
  						});
  					},
  					scope: this
  				}]
  			}]
  		}]
  	}]
  },
  
  submitSearch: function(button, event, options)
	{
		var store = this.up().items.items[1].getStore(),
				name = this.items.items[0].items.items[0].items.items[0].getValue(),
				level = this.items.items[0].items.items[0].items.items[4].getValue(),
				unit = this.items.items[0].items.items[1].items.items[0].getValue();
		
		//this.up().setLoading(true);
		
		if((name.length == 0)  && (level.length == 0) && (unit.length == 0))
		{
			name = "*";
		}
		
		store.load
		({
			params:
			{
				name: name,
				level: level,
				unit: unit
			},
			callback: function(records, operation, success) 
			{
				//this.up().setLoading(false);
				
 				var t=1;
			},
			scope: this
		});	
	}
});