﻿//
//
//  Add / Edit Favorite Link
//
//


Ext.define('SALP.view.AddEditFavoriteLinkWindow', 
{
	extend: 'Ext.window.Window',

  height: 180,
  width: 340,
  resizable: true,
  modal: true,
  layout: 'fit',
	constrain: true,
  
  initComponent: function()
  {
  	this.setKeyMap(
   	{	
   		scope: this,
   		'ENTER': function()
   		{
    		this.submitForm();
    	}
    });
  	
  	this.items = this.buildItems();
    this.buttons = this.buildButtons();
    
    this.listeners = this.buildListeners();
    
    	
    this.callParent(arguments);
  },
  
  
  buildItems: function()
  {
  	return [
  	{
  		xtype: 'form.AddEditFavoriteLinkForm'
  	}]
  },
  
  buildButtons: function()
  {
  	return [
  	{
  		text: 'Cancel',
  		listeners: 
			{
      	click: function(button, event, object) 
        {
					this.close();
        },
        scope: this
      }
  	},
  	{
  		text: 'Ok',
  		listeners: 
			{
      	click: function(button, event, object) 
        {
					this.submitForm();
        },
        scope: this
      }
  	}]
  },
  
  submitForm: function()
  {
  	var form = this.items.items[0];
  	
  	if (form.isValid)
  	{
  		if(this.mode == 'Add')
  		{
    		form.submit
      	({
    			url: '/WebServices/Service.svc/AddUserFavoriteLink',
      		method: 'GET',
      		scope: this,
      		success: function()
    			{
    				Ext.toast('SUCCESS: Link Added.');
    				
    				SALP.data.FavoriteLinks.store.reload();
    				
    				SALP.reloadTreeStores();
    				
    				this.close();
    			},	
    			failure: function()
    			{
    				Ext.toast('FAILED: Unable To Add Link.');
    			}
      	})
  		}


			if(this.mode == 'Edit')
			{
				form.submit
      	({
    			url: '/WebServices/Service.svc/EditUserFavoriteLink',
      		method: 'GET',
      		scope: this,
      		success: function()
    			{
    				Ext.toast('SUCCESS: Link Edited.');
    				
    				SALP.data.FavoriteLinks.store.reload();
    				
    				SALP.reloadTreeStores();
    				
    				this.close();
    			},	
    			failure: function()
    			{
    				Ext.toast('FAILED: Unable To Edit Link.');
    			}
      	})
			}
    }
  }
});