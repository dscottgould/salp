﻿//
//
//  RenameDashboardWindow.js
//
//


Ext.define('SALP.view.RenameDashboardWindow', 
{
	alias: 'widget.view.renamedashboardwindow',
	extend: 'Ext.window.Window',

	id: 'renameDashboardWindow',
  title: 'Rename Dashboard',
  height: 124,
  width: 212,
  resizable: false,
  modal: true,
  layout: 'fit',
	constrain: true,
	
	
	initComponent: function()
  {
    	this.items = this.buildItems();
    	this.buttons = this.buildButtons();
    	
    	this.callParent(arguments);
  },
  buildItems: function()
  {
  	return [
  	{
  		xtype: 'container',
  		layout: 'vbox',
  		items: [
  		{
				xtype: 'container',
				height: 10,
				html: 'Dashboard Name:'
  		},
  		{
  			xtype: 'container',
  			layout: 'hbox',
  			items: [
  			{
  				id: 'dashboardNameField',
					xtype: 'textfield',
					width: 174,
  				padding: 10,
  				value: this.selectedRecord.dashboardName,
  				afterRender: function()
  				{
  					this.focus();
  				}
  			}]
  		}]
  	}]
  },
  buildButtons: function()
  {
  	return [
  	{
  		text: 'Ok',
  		listeners: 
			{
      	click: function(button, event, object) 
        {
					this.submitDashboardName();
        },
        scope: this
      }
  	}]
  },
  submitDashboardName: function()
  {
  	var dashboardName = Ext.getCmp('dashboardNameField').value
  	
  	SALP.setDashboardName(this.selectedRecord, dashboardName);
        	
		this.close()
  }
});

