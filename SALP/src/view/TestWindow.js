﻿//
//
//  TestWindow.js
//
//


Ext.define('SALP.view.TestWindow', 
{
	extend: 'Ext.window.Window',

  height: 300,
  width: 400,
  resizable: true,
  modal: false,
  layout: 'fit',
	
	initComponent: function()
  {
    this.items = this.buildItems();
    	
    this.callParent(arguments);
  },
  
  buildItems: function()
  {
  	return [
  	{
  		xtype: 'chart.SIEventFunnelChart',
  		ticketId: 'SI020998948',
  		height: 220
  	}]
  },
});