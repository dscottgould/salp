//
//
//  SALP Viewport
//
//

Ext.define('SALP.view.Viewport', 
{
	extend: 'Ext.container.Viewport',
	alias : 'widget.Viewport',
	
	
	layout:
  {
  	type: 'border',
  	padding: '0 5 5 5'
  },
  
	
	initComponent: function()
  {
    	this.items = this.buildItems();
    	
    	//this.listeners = this.buildListeners();
    	
    	this.callParent(arguments);
  },
  buildItems: function()
  {
  	return [
  	{
  		id: 'app-header',
  		xtype: 'view.header',
  		region: 'north',
  		height: 40
  	},
  	{
  		id: 'app-tabPanel',
  		xtype: 'tabpanel',
  		region: 'center'
  	},
  	{
  		xtype: 'nav.navigation',
  		title: 'Portal Toolbox',
  		region: 'west',
  		width: 200,
      header: 
      {
        itemPosition: 1, // after title before collapse tool
        items: [
        {
            xtype: 'button',
            text: 'Save Layout',
           	listeners: 
						{
        			click: function() 
         			{
								SALP.view.DashboardNameWindow = Ext.create('SALP.view.DashboardNameWindow').show()
								
								//SALP.saveSlots();
          		}
						}
        }]
      }
  	}];
  },
  
  buildListeners : function()
  {
		return 	{
	    'contextmenu' :
	    {
				fn : SALP.onContextMenu,
				element : 'el',
				scope : this
	    }
		};
  }
});