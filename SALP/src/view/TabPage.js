//
//
//  TabPage.js
//
//


Ext.define('SALP.view.TabPage', 
{
	alias: 'widget.view.tabpage',
	extend: 'Ext.dashboard.Dashboard',

	columnWidths: [0.25, 0.25, 0.25, 0.25],
  maxColumns: 4,
  
//	layout:
//	{
//    type:'hbox',
//    align: 'stretch'
//  },
//  autoScroll: true,
  
  parts:
  {
  	haystack:
  	{
  		viewTemplate:
  		{
  			title: 'Haystack',
  			items: [
  			{
  				xtype: 'haystackportlet'
  			}]
  		}
  	},
  	default:
  	{
  		viewTemplate:
  		{
  			xtype: 'component',
  			height: 1,
  			width: 1
  		}
  	}
  },
  
  defaultContent: [
  {
        type: 'default',
        columnIndex: 0
  }, 
  {
        type: 'default',
        columnIndex: 1
  },
  {
        type: 'default',
        columnIndex: 2
  }, 
  {
        type: 'default',
        columnIndex: 3
  }],
  
  initComponent: function()
  {
    //this.items = this.buildItems();
    	
    this.callParent(arguments);
  },
  
  buildItems: function()
  {
  	return [
  	{
  		xtype: 'dashboard-column',
  		width: 400
  	},
  	{
  		xtype: 'columnsplitter'
  	},
  	{
  		xtype: 'dashboard-column',
  		width: 400
  	},
  	{
  		xtype: 'columnsplitter'
  	},
  	{
  		xtype: 'dashboard-column',
  		width: 400
  	},
  	{
  		xtype: 'columnsplitter'
  	},
  	{
  		xtype: 'dashboard-column',
  		width: 400
  	}]
  }
});