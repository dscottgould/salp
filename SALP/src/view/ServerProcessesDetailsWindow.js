﻿//
//
//	Server Current Process Detail Window
//
//

Ext.define('SALP.view.ServerProcessesDetailsWindow', 
{
	extend: 'Ext.window.Window',

	//id: 'ServerProcessesDetailsWindow',
	iconCls: 'cog',
  height: 600,
  width: 630,
  resizable: true,
  maximizable: true,
  collapsible: true,
  modal: false,
  closable: true,
  layout: 'fit',
	constrain: true,
  
  
  initComponent: function()
  {
		this.setTitle('Server Process Details - ' + this.server)
		
		this.items = this.buildItems();
		
		this.buttons = this.buildButtons();
		
    this.callParent(arguments);
  },
  
  buildItems: function()
  {
  	return [
  	{
  		xtype: 'form.ServerProcessesDetailsForm',
			server: this.server,
			record: this.record
  	}]
  },
  
  buildButtons: function()
  {
  	return [
  	{
  		text: 'Close',
  		listeners: 
			{
      	click: function(button, event, object) 
        {
					this.close()
        },
        scope: this
      }
  	}]
  }
});