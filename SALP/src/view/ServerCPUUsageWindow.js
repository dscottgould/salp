﻿//
//
//	Server CPU Usage Window
//
//

Ext.define('SALP.view.ServerCPUUsageWindow', 
{
	extend: 'Ext.window.Window',

  title: 'Server Current Processes - ',
  height: 350,
  width: 835,
  resizable: true,
  maximizable: true,
  collapsible: true,
  modal: false,
  closable: true,
  layout: 'fit',
  constrain: true,
  
  
  initComponent: function()
  {
		this.setTitle('Server CPU Usage - ' + this.server)
		
		this.items = this.buildItems();

		this.tools = this.buildTools();
		
    this.callParent(arguments);
  },
  
  buildItems: function()
  {
  	return [
  	{
  		xtype: 'chart.ServerCPUUsageChart',
  		server: this.server
  	}]
  },
  
  
  buildTools: function()
  {
  	return [
  	{
  		xtype: 'tool',
    	type: 'refresh',
    	tooltip: 'Refresh Chart',
    	scope: this,
    	handler: function()
    	{
    		chart = this.items.first();
    		
    		chart.setLoading("Loading...");
    		
    		chart.getStore().reload();
    		
    		Ext.defer(function() 
        {
        	chart.setLoading(false);
        }, 2000, this);
    	}
  	}]
  }
  
 });