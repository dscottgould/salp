﻿//
//
//	Server Current Processes Window
//
//

Ext.define('SALP.view.ServerCurrentProcessesWindow', 
{
	extend: 'Ext.window.Window',

	id: 'ServerCurrentProcessesWindow',
  title: 'Server Current Processes - ',
  iconCls: 'cog',
  height: 560,
  width: 1290,
  resizable: true,
  maximizable: true,
  collapsible: true,
  modal: false,
  closable: true,
  layout: 'fit',
  constrain: true,
  
  
  initComponent: function()
  {
		this.setTitle('Server Current Processes - ' + this.server)
		
		this.items = this.buildItems();
		
		this.tools = this.buildTools();
		
    this.callParent(arguments);
  },
  
  buildItems: function()
  {
  	return [
  	{
  		xtype: 'grid.ServerProcessesFullGrid',
  		server: this.server
  	}]
  },
  
  buildTools: function()
  {
  	return [
  	{
  		xtype: 'tool',
    	type: 'refresh',
    	tooltip: 'Refresh Processes',
    	scope: this,
    	handler: function()
    	{
    		this.items.items[0].store.reload();
    	}
  	}]
  }
});