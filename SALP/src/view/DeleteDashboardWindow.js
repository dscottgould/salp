﻿//
//
//  DeleteDashboardWindow.js
//
//


Ext.define('SALP.view.DeleteDashboardWindow', 
{
	alias: 'widget.view.deletedashboardwindow',
	extend: 'Ext.window.Window',

	id: 'deleteDashboardWindow',
  title: 'Delete Dashboard',
  height: 124,
  width: 212,
  resizable: true,
  modal: true,
  layout: 'fit',
	constrain: true,
	
	
	initComponent: function()
  {
  	this.setKeyMap(
   	{	
   		scope: this,
   		'ENTER': function()
   		{
    		this.deleteDashboard();
    	}
    });
    
    this.items = this.buildItems();
    this.buttons = this.buildButtons();
    	
    this.callParent(arguments);
  },
  buildItems: function()
  {
  	return [
  	{
			xtype: 'container',
			html: '<center>Are you sure you want to Delete Dashboard ' + this.selectedRecord.dashboardName + '?</center>',
			padding: '10 10 10 10'
  	}]
  },
  buildButtons: function()
  {
  	return [
  	{
  		text: 'Ok',
  		listeners: 
			{
      	click: function(button, event, object) 
        {
					this.deleteDashboard();
        },
        scope: this
      }
  	},
  	{
  		text: 'Cancel',
  		listeners: 
			{
      	click: function(button, event, object) 
        {
					this.close();
        },
        scope: this
      }
  	}]
  },
  deleteDashboard: function()
  {
  	SALP.deleteDashboard(this.selectedRecord);
        	
		this.close()
  }
});
