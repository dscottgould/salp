﻿//
//
//  Add / Edit Servers
//
//


Ext.define('SALP.view.AddEditServersWindow', 
{
	extend: 'Ext.window.Window',

  height: 180,
  width: 340,
  resizable: true,
  modal: true,
  layout: 'fit',
	constrain: true,
  
  initComponent: function()
  {
  	this.setKeyMap(
   	{	
   		scope: this,
   		'ENTER': function()
   		{
    		this.getTicket();
    	}
    });
    
  	this.items = this.buildItems();
    this.buttons = this.buildButtons();
    
    	
    this.callParent(arguments);
  },
  
  
  buildItems: function()
  {
  	return [
  	{
  		xtype: 'form.AddEditServersForm'
  	}]
  },
  
  buildButtons: function()
  {
  	return [
  	{
  		text: 'Cancel',
  		listeners: 
			{
      	click: function(button, event, object) 
        {
					this.close();
        },
        scope: this
      }
  	},
  	{
  		text: 'Ok',
  		listeners: 
			{
      	click: function(button, event, object) 
        {
					this.submitForm();
        },
        scope: this
      }
  	}]
  },
  
  submitForm: function()
  {
  	var form = this.items.items[0];
  	
  	if (form.isValid)
  	{
  		if(this.mode == 'Add')
  		{
    		form.submit
      	({
    			url: '/WebServices/Service.svc/AddServer',
      		method: 'GET',
      		scope: this,
      		success: function()
    			{
    				Ext.toast('SUCCESS: Server Added.');
    				
    				this.grid.getStore().reload();
    				
    				Ext.getCmp('ServerSelect').getStore().reload();
    				
    				this.close();
    			},	
    			failure: function()
    			{
    				Ext.toast('FAILED: Unable To Add Server.');
    			}
      	})
  		}


			if(this.mode == 'Edit')
			{
				form.submit
      	({
    			url: '/WebServices/Service.svc/EditServer',
      		method: 'GET',
      		scope: this,
      		success: function()
    			{
    				Ext.toast('SUCCESS: Server Edited.');
    				
    				this.grid.getStore().reload();
    				
    				Ext.getCmp('ServerSelect').getStore().reload();
    				
    				this.close();
    			},	
    			failure: function()
    			{
    				Ext.toast('FAILED: Unable To Edit Server.');
    			}
      	})
			}
    }
  }
});