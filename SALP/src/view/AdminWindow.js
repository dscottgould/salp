﻿//
//
//  AdminWindow.js
//
//


Ext.define('SALP.view.AdminWindow', 
{
	alias: 'widget.view.adminwindow',
	extend: 'Ext.window.Window',

	id: 'adminWindow',
  title: 'Administration',
  height: 635,
  width: 635,
  resizable: false,
  modal: false,
  layout:
  {
  	type: 'vbox',
    align: 'stretch'
  },
	constrain: true,
	
	
	initComponent: function()
  {
    	this.items = this.buildItems();
    	this.buttons = this.buildButtons();
    	
    	this.callParent(arguments);
  },
  buildItems: function()
  {
  	return [
  	{
  		xtype: 'container',
  		padding: '5 5 5 5',
  		html: '<h3>Select portlets to be displayed:</h3>',
  		height: 50
  	},
  	{
  		xtype: 'container',
  		flex: 1,
  		layout:
  		{
  			type: 'hbox',
    		align: 'stretch'
    	},
  		items:[
			{
				id: 'Portlet-Selection',
				xtype: 'grid',
				store: SALP.data.Portlets.store,
				stripeRows: true,
				columnLines: true,
        selType: 'checkboxmodel',
        //selModel: new Ext.grid.RowSelectionModel({multipleSelect:true}),
				flex: 1,
			  columns: [
      	{
          text: "Name",
          dataIndex: 'displayName',
          flex: 1
      	}, 
      	{
          text: "Category",
          dataIndex: 'category',
          flex: .8
        }],
        listeners:
        {
    			afterrender:function(grid, options)
    			{
        		var selectionModel = grid.getSelectionModel();
        		
        		SALP.loadUserPortletList();
        		
        		var t=1;
        		//selectionModel.selectAll(true);
    			}
    		}
			}]
  	}]
  },
  buildButtons: function()
  {
  	return [
  	{
  		text: 'Ok',
  		listeners: 
			{
      	click: function(button, event, object) 
        {
					 var selections = Ext.getCmp('Portlet-Selection').getSelection(),
					 		 selectionsList = new Array();
					 
					 for(var index = 0;index <= selections.length - 1; index ++)
					 {
					 		selectionsList.push(selections[index].id)
					 }
					 
					selectionsList = selectionsList.toString();
					 
					SALP.saveUserPortlets(selectionsList);
					
					this.close();
        },
        scope: this
      }
  	}]
  }
});
