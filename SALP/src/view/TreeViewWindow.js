﻿//
//
//  TreeViewWindow.js
//
//


Ext.define('SALP.view.TreeViewWindow', 
{
	alias: 'widget.view.treeviewwindow',
	extend: 'Ext.window.Window',

	id: 'treeViewWindow',
  title: 'TreeView',
  height: 685,
  width: 570,
  resizable: false,
  modal: false,
  layout: 'fit',
  constrain: true,
  
  initComponent: function()
  {
		this.items = this.buildItems();
		
    this.buttons = this.buildButtons();	
    
    this.callParent(arguments);
  },
  
  buildItems: function()
  {
  	SALP.view.tree = Ext.create('Ext.tree.Panel');
  	
  	var rootNode = SALP.view.tree.setRootNode
  	({
  		text:	SALP.rawCookie.dashboardName,
  		expanded: true
  	})
  	
  	for(var pageIndex=0; pageIndex <= SALP.rawCookie.pages.length - 1; pageIndex ++)
  	{
  		rootNode = SALP.view.tree.getRootNode();
  		
  		var page = rootNode.appendChild
  		({
  			text:SALP.rawCookie.pages[pageIndex].title,
  			expanded: true
  		});
  		
  		for(var slotIndex = 0;slotIndex <= SALP.rawCookie.slots.length - 1;slotIndex ++)
  		{
  			var currentSlot = SALP.rawCookie.slots[slotIndex];
  			
  			if(pageIndex + 1 == currentSlot.page)
  			{
  				page.appendChild
  				({
  					text: currentSlot.title,
  					leaf: true
  				})
  			}
  		}
  	}
  	
  	return SALP.view.tree
  },
  buildButtons: function()
  {
  	return [
  	{
  		text: 'Close',
  		listeners: 
			{
      	click: function(button, event, object) 
        {
					this.close();
        },
        scope: this
      }
  	}]
  }
});