﻿//
//
//   Dashboard List
//
//


Ext.define('SALP.nav.DashboardList', 
{
	  id: 'DashboardList',
    extend: 'Ext.grid.Panel',
    alias: 'widget.nav.dashboardlist',
    height: 150,
    store: SALP.data.Dashboards.store,
    stripeRows: true,
    columnLines: true,
    
    initComponent: function()
    {
			this.columns = this.buildColumns()
 		
    	this.callParent(arguments);
    },
    buildColumns: function()
    {
    	return [
      {
        text   : 'ID',
        width    : 30,
        sortable : false,
        menuDisabled: true,
        dataIndex: 'dashboardId'
      },
      {
      	text   : 'Name',
        flex: 1,
        sortable : false,
        menuDisabled: true,
        dataIndex: 'dashboardName'
      }]
    }
});
