﻿//
//
//   Dashboard Toolbox
//
//

Ext.define('SALP.nav.DashboardToolbox', 
{
	alias: 'widget.nav.dashboardtoolbox',
	extend: 'Ext.panel.Panel',
	
	height: 200,
	border: false,
	bodyPadding :'10 0 0 10',
	
	
	initComponent: function()
  {
    	this.items = this.buildItems();
    	
    	this.callParent(arguments);
  },
  buildItems: function()
  {
  	return [
  	{
  		xtype: 'container',
  		layout: 'hbox',
  		items: [
  		{	
  			xtype: 'button',
       	text: 'Load',
       	listeners: 
  			{
        	click: function(button, event, object) 
          {
  					var dashboardList = Ext.getCmp('DashboardList'),
  							selectedRecord;
  							
  					if(dashboardList.selection)
  					{
  						selectedRecord = dashboardList.selection.data;
  					}
  							
  					if(selectedRecord)
  					{
  						//SALP.clearCurrentTab();
  						SALP.clearLayout();
  						
  						SALP.countPages = 1;
  						
  						//SALP.rawCookie = Ext.decode(selectedRecord.cookieValue);
  						
  						//SALP.loadSlots();
  						
  						SALP.getCookie(selectedRecord.dashboardId);
  						
  					}
  					
          }
        }
    	},
    	{
    		xtype: 'container',
    		width: 10
    	},
    	{
    		xtype: 'button',
       	text: 'Delete',
       	listeners: 
  			{
        	click: function(button, event, object) 
          {
          	var dashboardList = Ext.getCmp('DashboardList');
          	
          	if (dashboardList.selection)
          	{
          		var selectedRecord = dashboardList.selection.data;
          		  	
          		SALP.view.DeleteDashboardWindow = Ext.create('SALP.view.DeleteDashboardWindow',
  						{
  							selectedRecord: selectedRecord
  						}).show();
          	}
  				},
  				scope: this
        }
    	},
    	{
    		xtype: 'container',
    		width: 10
    	},
    	{
    		xtype: 'button',
       	text: 'Rename',
       	listeners: 
  			{
        	click: function(button, event, object) 
          {
  					var dashboardList = Ext.getCmp('DashboardList'),
  							selectedRecord = dashboardList.selection.data;
  							
  					if(selectedRecord != 'undefined')
  					{
							SALP.view.RenameTabWindow = Ext.create('SALP.view.RenameDashboardWindow',
							{
								selectedRecord: selectedRecord
							}).show()
  						
  					}
  					
          }
        }
    	}]
  	},
  	{
  		xtype: 'container',
  		padding: '10 0 0 0',
  		layout: 'hbox',
  		items: [
  		{
  			xtype: 'button',
       	text: 'Set Default',
       	listeners: 
  			{
        	click: function(button, event, object) 
          {
  					var dashboardList = Ext.getCmp('DashboardList'),
  							selectedRecord = dashboardList.selection.data;
  							
  					if(selectedRecord != 'undefined')
  					{
							SALP.setDefaultDashboard(selectedRecord);
  						
  					}
  					
          }
        }
  		},
  		{
				xtype: 'container',
				width: 35
			},
			{
				xtype: 'button',
				text: 'Get URL',
				listeners: 
				{
	       	click: function(button, event, object) 
	        {
						SALP.view.urlWindow = Ext.create('SALP.view.UrlWindow').show();
	        }
	      }
			}]
  	},
  	{
  		xtype: 'container',
  		padding: '10 0 0 0',
  		layout: 'hbox',
  		items: [
  		{
  			xtype: 'button',
       	text: 'Tree View',
       	listeners: 
  			{
        	click: function(button, event, object) 
          {
						if(!Ext.getCmp('treeViewWindow'))
						{
							SALP.view.TreeViewWindow =	Ext.create('SALP.view.TreeViewWindow').show(); 
						}
						else
						{
							SALP.view.TreeViewWindow.show();
						}		
          }
        }
  		}]
  	}]
  }
})
  	