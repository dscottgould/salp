﻿//
//
//  Context Menu
//
//


Ext.define('SALP.nav.ContextMenu',
{
	extend: 'Ext.menu.Menu',
    

	initComponent : function()
	{
		this.items = this.buildItems();

		this.callParent(arguments);
	},


	buildItems: function()
	{
		return [
   	{
    	iconCls :'cog',
      text : '<b>Welcome To SALP!</b>' 
    },
		{
	  	xtype : 'menuseparator'  
		},
		{
    	iconCls : 'linkIcon',
      text : 'Item 01',
      handler : this.onMenuItemSelected,
	    scope : this
	  },
	 	{
    	iconCls : 'linkIcon',
      text : 'Item 02',
      handler : this.onMenuItemSelected,
	    scope : this
	  }]
	},
	
	onMenuItemSelected: function()
	{
		var t=1;
	}
});