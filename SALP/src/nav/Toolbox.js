﻿//
//
//  Toolbox.js
//
//

Ext.define('SALP.nav.Toolbox', 
{
	alias: 'widget.nav.toolbox',
	extend: 'Ext.panel.Panel',
	
	height: 150,
	border: false,
	      	
	
	initComponent: function()
  {
    	this.items = this.buildItems();
    	
    	this.callParent(arguments);
  },
  buildItems: function()
  {
		return [
		{
			xtype: 'container',
      cls: 'navigationItem',
      padding : 10,
      height: 200,
      layout :'vbox',
      items: [
			{
				id: this.idChartSelect,
				xtype: 'combobox',
				fieldLabel: '',
				width: 175,
    		store: this.idChartSelectStore,
    		queryMode: 'local',
    		autoSelect: true,
    		displayField: 'displayName',
    		valueField: 'className',
    		value: this.selectChartValue
      },
			{	
				xtype: 'container',
				layout: 'hbox',
				items:[
				{
					xtype: 'container',
					width: 111
				},
				{
					xtype: 'button',
					text: 'Add Portlet',
					listeners: 
					{
						scope: this,
            click: function() 
            {
							SALP.addChart(this.idChartSelect);
            }
					}
				}]
			}]
		}
		]
	}
});