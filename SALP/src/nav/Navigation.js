﻿//
//
//  SALP Navigation
//
//

Ext.define('SALP.nav.Navigation', 
{
	alias: 'widget.nav.navigation',
	extend: 'Ext.panel.Panel',
	
	split: true,
  collapsible: true,
  animCollapse: true,
  width: 200,
  minWidth: 150,
  maxWidth: 400,
  
  layout:
  {
  	type: 'accordion',
    animate: true
  },
	
	initComponent: function()
  {
  	this.data = new Object();
  	
  	this.data.model =  Ext.define('Portlet', 
    {
    	extend: 'Ext.data.Model',
    	fields: [
    		{name: 'displayName', type: 'string'},
    		{name: 'className', type: 'string'}
    ]});
    
		// Customer Reported Troubles
    this.data.selectChart01 = Ext.create('Ext.data.Store', 
    {
    	autoLoad: true,
    	model: this.data.model,
    	proxy: 
    	{
      	type: 'ajax',
       	url: 'WebServices/Service.svc/GetSelectData',
       	extraParams:
       	{
       		accountName: SALP.user.accountName,
       		categoryId: 1
       	},
       	pageParam: undefined,
    		startParam: undefined,
    		limitParam: undefined,
       	reader: 
       	{
        	type: 'json',
          root: 'data'
       	}
    	},
    	listeners: 
    	{
        load: 
        {
          fn: function(store, records)
          {
            if(records.length > 0)
            {
            	Ext.getCmp('selectChart01').select(records[0].data.className);
            }
            else
            {
            	Ext.getCmp('selectChart01').clearValue();
            }
          }
        }
      }
    });    
    	
    // Change Management	
   	this.data.selectChart02 = Ext.create('Ext.data.Store', 
    {
    	autoLoad: true,
    	model: this.data.model,
    	proxy: 
    	{
      	type: 'ajax',
       	url: 'WebServices/Service.svc/GetSelectData',
       	extraParams:
       	{
       		accountName: SALP.user.accountName,
       		categoryId: 2
       	},
       	pageParam: undefined,
    		startParam: undefined,
    		limitParam: undefined,
       	reader: 
       	{
        	type: 'json',
          root: 'data'
       	}
    	},
    	listeners: 
    	{
        load: 
        {
          fn: function(store, records)
          {
            if(records.length > 0)
            {
            	Ext.getCmp('selectChart02').select(records[0].data.className);
            }
            else
            {
            	Ext.getCmp('selectChart02').clearValue();
            }
          }
        }
      }
    });    
    
    
    // Haystack	
    this.data.selectChart03 = Ext.create('Ext.data.Store', 
    {
    	autoLoad: true,
    	model: this.data.model,
    	proxy: 
    	{
      	type: 'ajax',
       	url: 'WebServices/Service.svc/GetSelectData',
       	extraParams:
       	{
       		accountName: SALP.user.accountName,
       		categoryId: 3
       	},
       	pageParam: undefined,
    		startParam: undefined,
    		limitParam: undefined,
       	reader: 
       	{
        	type: 'json',
          root: 'data'
       	}
    	},
    	listeners: 
    	{
        load: 
        {
          fn: function(store, records)
          {
            if(records.length > 0)
            {
            	Ext.getCmp('selectChart03').select(records[0].data.className);
            }
            else
            {
            	Ext.getCmp('selectChart03').clearValue();
            }
          }
        }
      }
    });   
    
    //Billing
    if(SALP.user.hasBillingAccess)
    {
      this.data.selectChart04 = Ext.create('Ext.data.Store', 
      {
      	autoLoad: true,
      	model: this.data.model,
      	proxy: 
      	{
        	type: 'ajax',
         	url: 'WebServices/Service.svc/GetSelectData',
         	extraParams:
         	{
         		accountName: SALP.user.accountName,
         		categoryId: 4
         	},
         	pageParam: undefined,
      		startParam: undefined,
      		limitParam: undefined,
         	reader: 
         	{
          	type: 'json',
            root: 'data'
         	}
      	},
      	listeners: 
      	{
          load: 
          {
            fn: function(store, records)
            {
              if(records.length > 0)
              {
              	Ext.getCmp('selectChart04').select(records[0].data.className);
              }
              else
              {
              	Ext.getCmp('selectChart04').clearValue();
              }
            }
          }
        }
      });  
    }
    	
    // Inciment Management	
    this.data.selectChart05 = Ext.create('Ext.data.Store', 
    {
    	autoLoad: true,
    	model: this.data.model,
    	proxy: 
    	{
      	type: 'ajax',
       	url: 'WebServices/Service.svc/GetSelectData',
       	extraParams:
       	{
       		accountName: SALP.user.accountName,
       		categoryId: 5
       	},
       	pageParam: undefined,
    		startParam: undefined,
    		limitParam: undefined,
       	reader: 
       	{
        	type: 'json',
          root: 'data'
       	}
    	},
    	listeners: 
    	{
        load: 
        {
          fn: function(store, records)
          {
            if(records.length > 0)
            {
            	Ext.getCmp('selectChart05').select(records[0].data.className);
            }
            else
            {
            	Ext.getCmp('selectChart05').clearValue();
            }
          }
        }
      }
    }); 
    
    
    // FCC
    if(SALP.user.hasFCCAccess)
    {
      this.data.selectChart06 = Ext.create('Ext.data.Store', 
      {
      	autoLoad: true,
      	model: this.data.model,
      	proxy: 
      	{
        	type: 'ajax',
         	url: 'WebServices/Service.svc/GetSelectData',
         	extraParams:
         	{
         		accountName: SALP.user.accountName,
         		categoryId: 6
         	},
         	pageParam: undefined,
      		startParam: undefined,
      		limitParam: undefined,
         	reader: 
         	{
          	type: 'json',
            root: 'data'
         	}
      	},
      	listeners: 
      	{
          load: 
          {
            fn: function(store, records)
            {
              if(records.length > 0)
              {
              	Ext.getCmp('selectChart06').select(records[0].data.className);
              }
              else
              {
              	Ext.getCmp('selectChart06').clearValue();
              }
            }
          }
        }
      }); 
    }
    	
    // BLR	
    if(SALP.user.hasBLRAccess)
    {
      this.data.selectChart07 = Ext.create('Ext.data.Store', 
      {
      	autoLoad: true,
      	model: this.data.model,
      	proxy: 
      	{
        	type: 'ajax',
         	url: 'WebServices/Service.svc/GetSelectData',
         	extraParams:
         	{
         		accountName: SALP.user.accountName,
         		categoryId: 8
         	},
         	pageParam: undefined,
      		startParam: undefined,
      		limitParam: undefined,
         	reader: 
         	{
          	type: 'json',
            root: 'data'
         	}
      	},
      	listeners: 
      	{
          load: 
          {
            fn: function(store, records)
            {
              if(records.length > 0)
              {
              	Ext.getCmp('selectChart07').select(records[0].data.className);
              }
              else
              {
              	Ext.getCmp('selectChart07').clearValue();
              }
            }
          }
        }
      }); 
    }
    
    
    // General
    this.data.selectChart08 = Ext.create('Ext.data.Store', 
    {
    	autoLoad: true,
    	model: this.data.model,
    	proxy: 
    	{
      	type: 'ajax',
       	url: 'WebServices/Service.svc/GetSelectData',
       	extraParams:
       	{
       		accountName: SALP.user.accountName,
       		categoryId:11
       	},
       	pageParam: undefined,
    		startParam: undefined,
    		limitParam: undefined,
       	reader: 
       	{
        	type: 'json',
          root: 'data'
       	}
    	},
    	listeners: 
    	{
        load: 
        {
          fn: function(store, records)
          {
            if(records.length > 0)
            {
            	Ext.getCmp('selectChart08').select(records[0].data.className);
            }
            else
            {
            	Ext.getCmp('selectChart08').clearValue();
            }
          }
        }
      }
    }); 
    
    
    // Maintenance Tools
    this.data.selectChart09 = Ext.create('Ext.data.Store', 
    {
    	autoLoad: true,
    	model: this.data.model,
    	proxy: 
    	{
      	type: 'ajax',
       	url: 'WebServices/Service.svc/GetSelectData',
       	extraParams:
       	{
       		accountName: SALP.user.accountName,
       		categoryId:12
       	},
       	pageParam: undefined,
    		startParam: undefined,
    		limitParam: undefined,
       	reader: 
       	{
        	type: 'json',
          root: 'data'
       	}
    	},
    	listeners: 
    	{
        load: 
        {
          fn: function(store, records)
          {
          	if(Ext.getCmp('selectChart09'))
          	{
          		if(records.length > 0)
            	{
            		Ext.getCmp('selectChart09').select(records[0].data.className);
            	}
            	else
            	{
            		Ext.getCmp('selectChart09').clearValue();
           	 	}
          	}

          }
        }
      }
    }); 
    
    this.data.selectChart10 = Ext.create('Ext.data.Store', 
    {
    	autoLoad: true,
    	model: this.data.model,
    	proxy: 
    	{
      	type: 'ajax',
       	url: 'WebServices/Service.svc/GetSelectData',
       	extraParams:
       	{
       		accountName: SALP.user.accountName,
       		categoryId: 13
       	},
       	pageParam: undefined,
    		startParam: undefined,
    		limitParam: undefined,
       	reader: 
       	{
        	type: 'json',
          root: 'data'
       	}
    	},
    	listeners: 
    	{
        load: 
        {
          fn: function(store, records)
          {
          	if(Ext.getCmp('selectChart10'))
          	{
          		if(records.length > 0)
            	{
            		Ext.getCmp('selectChart10').select(records[0].data.className);
            	}
            	else
            	{
            		Ext.getCmp('selectChart10').clearValue();
           	 	}
          	}

          }
        }
      }
    }); 
    
    this.data.selectChart11 = Ext.create('Ext.data.Store', 
    {
    	autoLoad: true,
    	model: this.data.model,
    	proxy: 
    	{
      	type: 'ajax',
       	url: 'WebServices/Service.svc/GetSelectData',
       	extraParams:
       	{
       		accountName: SALP.user.accountName,
       		categoryId: 14
       	},
       	pageParam: undefined,
    		startParam: undefined,
    		limitParam: undefined,
       	reader: 
       	{
        	type: 'json',
          root: 'data'
       	}
    	},
    	listeners: 
    	{
        load: 
        {
          fn: function(store, records)
          {
          	if(Ext.getCmp('selectChart10'))
          	{
          		if(records.length > 0)
            	{
            		Ext.getCmp('selectChart10').select(records[0].data.className);
            	}
            	else
            	{
            		Ext.getCmp('selectChart10').clearValue();
           	 	}
          	}

          }
        }
      }
    }); 
    
    	
  	this.items = this.buildItems();
  	
  	if(SALP.user.hasBillingAccess)
  	{
  		this.items = this.items.concat(this.buildBilling());
  	}
  	
  	if(SALP.user.hasFCCAccess)
  	{
  		this.items = this.items.concat(this.buildFCC());
  	}
  	
  	if(SALP.user.hasBLRAccess)
  	{
  		this.items = this.items.concat(this.buildBLR());
  	}
  	
		if(SALP.user.hasXNOCOBIAccess)
		{
			this.items = this.items.concat(this.buildXNOCOBIMonitor());
		}
  	
  	if(SALP.user.hasMaintenanceAccess)
  	{
  		this.items = this.items.concat(this.buildMaintenanceTools());
  	}
  	
  	this.items = this.items.concat(this.buildTools());
  	
  	this.callParent(arguments);
  },
  
  
  buildItems: function()
  {
  	return [
  	{
    	title:'General',
      border: false,
      autoScroll: true,
      iconCls: 'nav',
      items:[
      {
      	xtype: 'nav.toolbox',
      	idChartSelect: 'selectChart08',
      	idChartSelectStore: this.data.selectChart08,
				selectChartValue: ''
      },
      {
      	xtype: 'container',
        cls: 'navigationItem',
        padding : 10,
        html : 'User Lookup',
   			listeners : 
   			{
        	element  : 'el',
          click    : function() 
          {
   					if(!Ext.getCmp('UserLookupWindow'))
   					{
   						Ext.create('SALP.view.UserLookupWindow',
   						{
   							source: 'Navigation'
   						}).show();
   					}
   					else
   					{
   						Ext.getCmp('UserLookupWindow').toFront();
   					}
          }
        }
      },
//      {
//      	xtype: 'container',
//      	html: 'Ticket Lookup',
//      	cls: 'navigationItem',
//      	padding : 10,
//				listeners : 
//				{
//					element  : 'el',
//					click    : function() 
//					{
//						SALP.view.ticketWindow = Ext.create('SALP.view.TicketWindowOld').show();
//					}
//      	}
//      },
      {
      	xtype: 'container',
      	html: 'Ticket Lookup',
      	cls: 'navigationItem',
      	padding : 10,
				listeners : 
				{
					element  : 'el',
					click    : function() 
					{
						Ext.create('SALP.view.TicketLookupDialog').show();
					}
      	}
      }]
//      {
//      	xtype: 'container',
//      	html: 'Test Window',
//      	cls: 'navigationItem',
//      	padding : 10,
//				listeners : 
//				{
//					element  : 'el',
//					click    : function() 
//					{
//						Ext.create('SALP.view.TestWindow').show();
//					}
//      	}
//      }]
    },
    {
    	title:'Problem Management',
      border: false,
      autoScroll: true,
      iconCls: 'nav',
      items:[
      {
      	xtype: 'nav.toolbox',
      	idChartSelect: 'selectChart11',
      	idChartSelectStore: this.data.selectChart11,
				selectChartValue: ''
      }]
    },
  	{
  		title:'Customer Reported Troubles',
   		autoScroll: true,
      border: false,
      iconCls: 'nav',
      items:[
      {
      	xtype: 'nav.toolbox',
      	idChartSelect: 'selectChart01',
      	idChartSelectStore: this.data.selectChart01,
      	idPageSelect: 'pageSelect01'
      }]
  	},
  	{
  		 title:'Change Management',
       cls: 'navigationItem',
       border: false,
       autoScroll: true,
       iconCls: 'nav',
       items:[
      {
      	xtype: 'nav.toolbox',
      	idChartSelect: 'selectChart02',
      	idChartSelectStore: this.data.selectChart02
      },
      {
      	xtype: 'container',
        cls: 'navigationItem',
        padding : 10,
        html : 'Success/Fail TreeMap',
   			listeners : 
   			{
        	element  : 'el',
          click    : function() 
          {
   					if(!Ext.getCmp('TreeMapWindow'))
   					{
   						Ext.create('SALP.view.TreeMapWindow').show();
   					}
          }
        }
      }]
  	},
  	{
  		title:'Haystack',
      border: false,
      autoScroll: true,
      iconCls: 'nav',
      items:[
      {
      	xtype: 'nav.toolbox',
      	idChartSelect: 'selectChart03',
      	idChartSelectStore: this.data.selectChart03
      },
      {
      	xtype: 'container',
        cls: 'navigationItem',
        padding : 10,
        html : 'Haystack',
   			listeners : 
   			{
        	element  : 'el',
          click    : function() 
          {
   					SALP.openSite('http://Haystack');
          }
        }
      },
      {
      	xtype: 'container',
        cls: 'navigationItem',
        padding : 10,
        html : 'Node Filter Creator',
   			listeners : 
   			{
        	element  : 'el',
          click    : function() 
          {
         		if(Ext.getCmp('haystackFiltersWindow'))
          	{
          		Ext.getCmp('haystackFiltersWindow').toFront;
          	}
          	else
          	{
          		Ext.create('SALP.view.HaystackFiltersWindow').show();
          	}
          }
        }
      }]
  	},
    {
    	title:'Incident Management',
      border: false,
      autoScroll: true,
      iconCls: 'nav',
      items:[
      {
      	xtype: 'nav.toolbox',
      	idChartSelect: 'selectChart05',
      	idChartSelectStore: this.data.selectChart05
      },
      {
      	xtype: 'container',
        cls: 'navigationItem',
        padding : 10,
        html : 'Severity 1 Executive Overview',
   			listeners : 
   			{
        	element  : 'el',
          click    : function() 
          {
   					SALP.openSite('https://tableau.comcast.com/t/InfrastructureServices/views/Severity1ExecutiveOverview/Sev1ExecView?:embed=y&:showShareOptions=true&:display_count=no&:showVizHome=no');
          }
        }
      }]
    },
    {
    	title:'EPS Analytics Links',
      border: false,
      autoScroll: true,
      iconCls: 'nav',
      items: [
      {
      	xtype: 'container',
      	cls: 'navigationItem',
      	padding : 10,
      	html : 'Problem Code Analysis',
				listeners : 
				{
					element  : 'el',
					click    : function() 
					{
						SALP.openSite('http://obirs-wc-01p/Reports/Pages/Report.aspx?ItemPath=%2fDeploy%2fSPC_Wk_CRTkts&ViewMode=Detail');
					}
      	}
      },
      {
      	xtype: 'container',
      	cls: 'navigationItem',
      	padding : 10,
      	html : 'Problem Summaries- OOC Weekly',
				listeners : 
				{
					element  : 'el',
					click    : function() 
					{
					SALP.openSite('http://obirs-wc-01p/Reports/Pages/Report.aspx?ItemPath=%2fDeploy%2fSPC_Wk_CRTkts_Alarm&ViewMode=Detail');
					}
      	}
      },
     	{
      	xtype: 'container',
      	cls: 'navigationItem',
      	padding : 10,
      	html : 'Problem Summaries- OOC By Day of Week',
				listeners : 
				{
					element  : 'el',
					click    : function() 
					{
						SALP.openSite('http://obirs-wc-01p/Reports/Pages/Report.aspx?ItemPath=%2fDeploy%2fSPC_Wk_CRTkts_Alarm_Pct&ViewMode=Detail');
					}
      	}
      },
      {
      	xtype: 'container',
      	cls: 'navigationItem',
      	padding : 10,
      	html : 'CR Tkt Detail by Day/Problem/Code/Cause/Solution',
				listeners : 
				{
					element  : 'el',
					click    : function() 
					{
						SALP.openSite('http://obirs-wc-01p/Reports/Pages/Report.aspx?ItemPath=%2fTesting%2fCRTktByDay_ProblemCode&ViewMode=Detail');
					}
      	}
      },
      {
      	xtype: 'container',
      	cls: 'navigationItem',
      	padding : 10,
      	html : 'CR Tkt By Day Trend Analysis',
				listeners : 
				{
					element  : 'el',
					click    : function() 
					{
						SALP.openSite('http://obirs-wc-01p/Reports/Pages/Report.aspx?ItemPath=%2fTesting%2fCRTktByDay_Summary&ViewMode=Detail');
					}
      	}
      }]
    }]
  },
  buildBilling: function()
  {
  	return [  	
  	{
  		title:'Billing',
      border: false,
      autoScroll: true,
      iconCls: 'nav',
     	items:[
      {
      	xtype: 'nav.toolbox',
      	idChartSelect: 'selectChart04',
      	idChartSelectStore: this.data.selectChart04
      }]
    }]
  },
  buildFCC: function()
  {
  	return [
  	{
    	title:'FCC',
      border: false,
      autoScroll: true,
      iconCls: 'nav',
      items:[
      {
      	xtype: 'nav.toolbox',
      	idChartSelect: 'selectChart06',
      	idChartSelectStore: this.data.selectChart06
      },
      {
      	xtype: 'container',
      	cls: 'navigationItem',
      	padding : 10,
      	html : 'RCOR',
				listeners : 
				{
					element  : 'el',
					click    : function() 
					{
						SALP.openSite('http://obiweb-po-01p/RCOR/FCCMain.aspx');
					}
      	}
      },
      {
      	xtype: 'container',
      	cls: 'navigationItem',
      	padding : 10,
      	html : 'RC Dashboard',
				listeners : 
				{
					element  : 'el',
					click    : function() 
					{
						SALP.openSite('http://pacdcdnetopta01:88/RCDashboard');
					}
      	}
      }]
    }]
  },
	buildBLR: function()
	{
		return [
    {
    	title:'BLR',
      border: false,
      autoScroll: true,
      iconCls: 'nav',
      items:[
      {
      	xtype: 'nav.toolbox',
      	idChartSelect: 'selectChart07',
      	idChartSelectStore: this.data.selectChart07
      }]
    }]
  },
  buildMaintenanceTools: function()
  {
  	return [
  	{
  		title: 'Maintenance Tools',
  		autoScroll: true,
      border: false,
      iconCls: 'tools',
  		items: [
  		{
      	xtype: 'nav.toolbox',
      	idChartSelect: 'selectChart09',
      	idChartSelectStore: this.data.selectChart09
  		},
  		{
    		xtype: 'container',
      	cls: 'navigationItem',
      	padding : 10,
      	html : 'National Docsis Telemetry',
				listeners : 
				{
					element  : 'el',
					click    : function() 
					{
						SALP.openSite('http://ip-edge-portal.cable.comcast.com/index.php');
					}
      	}
  		},
  		{
    		xtype: 'container',
      	cls: 'navigationItem',
      	padding : 10,
      	html : 'Customer Timeline',
				listeners : 
				{
					element  : 'el',
					click    : function() 
					{
						SALP.openSite('https://customertimeline.cable.comcast.com/#/');
					}
      	}
  		},
  		{
    		xtype: 'container',
      	cls: 'navigationItem',
      	padding : 10,
      	html : 'Network Hygiene',
				listeners : 
				{
					element  : 'el',
					click    : function() 
					{
						SALP.openSite('http://sa-metrics/network_hygiene/HTMLClient/default.htm');
					}
      	}
  		},
  		{
    		xtype: 'container',
      	cls: 'navigationItem',
      	padding : 10,
      	html : 'National CMTS',
				listeners : 
				{
					element  : 'el',
					click    : function() 
					{
						SALP.openSite('http://CMTS.comcast.net');
					}
      	}
  		},
  		{
    		xtype: 'container',
      	cls: 'navigationItem',
      	padding : 10,
      	html : 'BNOC SM Log',
				listeners : 
				{
					element  : 'el',
					click    : function() 
					{
						SALP.openSite('http://sa-metrics/BNOC_SM_Log/HTMLClient/default.htm');
					}
      	}
  		},
  		{
      	xtype: 'container',
      	html: 'SI Ticket Lookup',
      	cls: 'navigationItem',
      	padding : 10,
				listeners : 
				{
					element  : 'el',
					click    : function() 
					{
						SALP.view.ticketWindow = Ext.create('SALP.view.TicketWindowNew').show();
					}
      	}
      },
      {
    		xtype: 'container',
      	cls: 'navigationItem',
      	padding : 10,
      	html : 'Core Dash Incident Details',
				listeners : 
				{
					element  : 'el',
					click    : function() 
					{
						SALP.openSite('https://tableau.cable.comcast.com/#/site/InfrastructureServices/views/NetworkQualityDashboard/IncidentDetails?:iid=1');
					}
      	}
  		},
  		{
    		xtype: 'container',
      	cls: 'navigationItem',
      	padding : 10,
      	html : 'BNOC Outage Details',
				listeners : 
				{
					element  : 'el',
					click    : function() 
					{
						SALP.openSite('https://tableau.cable.comcast.com/#/site/InfrastructureServices/views/SI_CBH_Dash/OutageSummaryDetail?:iid=1');
					}
      	}
  		},
  		{
    		xtype: 'container',
      	cls: 'navigationItem',
      	padding : 10,
      	html : 'PEM',
				listeners : 
				{
					element  : 'el',
					click    : function() 
					{
						SALP.openSite('https://pem.cable.comcast.com/pem/Quality_scorecard_V3_1/Executive_Dashboard');
					}
      	}
  		}]
  	}]
  },
  buildXNOCOBIMonitor: function()
	{
		return [
    {
    	title:'XNOCOBI',
      border: false,
      autoScroll: true,
      iconCls: 'nav',
      items:[
      {
      	xtype: 'nav.toolbox',
      	idChartSelect: 'selectChart10',
      	idChartSelectStore: this.data.selectChart10
      }]
    }]
  },
  buildTools: function()
  {
  	return [
  	{
  		title:'Dashboards',
   		autoScroll: true,
      border: false,
      iconCls: 'tools',
      items:[
      {
      	xtype: 'nav.dashboardlist'
      },
      {
      	xtype: 'nav.dashboardtoolbox'
      }]
  	},
    {
  		title:'Tab Tools',
   		autoScroll: true,
      border: false,
      iconCls: 'tools',
      items:[
      {
      	xtype: 'nav.tabtoolbox'
      }]
  	}]
  }
});