﻿//
//
//   Tab Toolbox
//
//

Ext.define('SALP.nav.TabToolbox', 
{
	alias: 'widget.nav.tabtoolbox',
	extend: 'Ext.panel.Panel',
	
	height: 200,
	border: false,
	
	keyHandlers: 
 	{
  	ENTER: function()
  	{
  		SALP.clearCookies();
  	}
  },
	
	initComponent: function()
  {
    	this.items = this.buildItems();
    	
    	this.callParent(arguments);
  },
  buildItems: function()
  {
  	return [
		{
			xtype: 'container',
			layout: 'vbox',
			padding: 5,
			items:[
			{
				xtype: 'container',
				html: 'Select Tab:'
			},
			{
				id: 'idPageSelect',
				xtype: 'combobox',
				fieldLabel: '',
				labelWidth: 70,
				width: 185,
    		store: SALP.data.selectPage,
    		queryMode: 'local',
    		autoSelect: true,
    		editable: false,
    		displayField: 'displayName',
    		valueField: 'value'
			},
//			{
//				id: 'tabNameField',
//				xtype: 'textfield',
//				width: 174
//			},
			{
				xtype: 'container',
				layout: 'hbox',
				items: [
				{
					xtype: 'button',
         	text: 'Add Tab',
         	listeners: 
					{
          	click: function(button, event, object) 
            {
            	SALP.openAddTabWindow();
            	
            	//SALP.addTab(button, event, object);
            }
          }
				},
				{
					xtype: 'container',
					width: 8
				},
				{
					xtype: 'button',
         	text: 'Delete Tab',
         	listeners: 
					{
          	click: function(button, event, object) 
            {
            	SALP.removeTab(button, event, object);
            }
          }
				},
				{
					xtype: 'container',
					width: 8
				},
				{
					xtype: 'button',
         	text: 'Rename',
         	listeners: 
					{
          	click: function(button, event, object) 
            {
            	var selectedTabIndex = Ext.getCmp('idPageSelect').getValue();
            	
            	if (selectedTabIndex != null)
            	{
            		SALP.openRenameTabWindow();
            	}
            }
          }
				}]
			},
			{
				xtype: 'container',
				height: 10
			},
			{
				xtype: 'container',
				layout: 'hbox',
				items: [
				{
    			xtype: 'container',
    			layout: 'hbox',
    			items: [
    			{
    				xtype: 'button',
    				text: 'Clear Layout',
    				listeners: 
    				{
    	       	click: function(button, event, object) 
    	        {
    						SALP.clearCurrentTab();
    	        }
    	      }
    			}]
				},
				{
					xtype: 'container',
					width: 53
				},
				{
					xtype: 'button',
					text: 'Get URL',
					listeners: 
					{
	         	click: function(button, event, object) 
	          {
							SALP.view.urlWindow = Ext.create('SALP.view.UrlWindow').show();
	          }
	        }
				}]
			}]
		}]
  }
	
});