﻿//
//
//   Get Ticket Toolbox
//
//

Ext.define('SALP.nav.GetTicketToolbox', 
{
	alias: 'widget.nav.gettickettoolbox',
	extend: 'Ext.panel.Panel',
	
	height: 200,
	border: false,
	
	
	initComponent: function()
  {
    	this.items = this.buildItems();
    	
    	this.callParent(arguments);
  },
  
  
  buildItems: function()
  {
		return [
		{
			id: 'TicketNumberField',
			xtype: 'textfield',
			fieldLabel: 'Ticket #',
			labelWidth: 60,
			width: 175,
			padding: '10 10 10 10',
			value: 'SI012939939'
		},
		{
			xtype: 'container',
			layout: 'hbox',
			items: [
			{
				xtype: 'container',
				width: 125
			},
			{
				xtype: 'button',
        text: 'Get Ticket',
        listeners: 
				{
        	click: function(button, event, object) 
          {
          	this.getTicket();
          },
          scope: this
        }
			}
			]
		}]
	},
	
	getTicket: function()
  {
  	var ticketNumber = Ext.getCmp('TicketNumberField').getValue();
  	
		SALP.getTicket(ticketNumber);
  }	
});