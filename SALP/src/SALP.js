﻿//
//
//   Service Assurance Launchpad 
//
//


//  Disable Caching


Ext.data.Connection.disableCaching = false;
Ext.data.proxy.Server.prototype.noCache = false;
Ext.Ajax.disableCaching = false;
Ext.Loader.setConfig({enabled:true, disableCaching: false});        

// Set Source Path
Ext.Loader.setPath('Ext.app', 'src');


Ext.application
({
	name: 'SALP',
	appFolder : 'src',
	
	requires:
  [
		'SALP.view.Viewport','SALP.view.Header', 'SALP.nav.Navigation', 'SALP.nav.Toolbox', 'SALP.nav.TabToolbox', 'SALP.view.TabPage',
		'SALP.view.AddTabWindow', 'SALP.view.RenameTabWindow', 
		//'SALP.portlets.ComcastNowPortlet', 
		'SALP.view.UrlWindow', 'SALP.view.DashboardNameWindow', 'SALP.nav.DashboardList', 'SALP.nav.DashboardToolbox',
		'SALP.view.DeleteDashboardWindow', 'SALP.view.RenameDashboardWindow', 'SALP.view.AdminWindow', 'SALP.view.HelpWindow',
		'SALP.view.TreeViewWindow', 'SALP.nav.GetTicketToolbox', 'SALP.view.BandAidTicketWindow',
		'SALP.form.OldTicketForm', 'SALP.form.NewTicketForm', 'SALP.view.SelectDateRangePanel', 'SALP.ux.DateSlider',
		'SALP.portlets.HaystackCallsPortlet', 'SALP.portlets.NetworkLensIncidentsPortlet', 'SALP.form.IncidentTicketAForm',
		'SALP.form.IncidentTicketBForm', 'SALP.portlets.BandAidPortlet', 'SALP.grid.IncidentTicketsGrid',
		'SALP.view.TicketDetailsWindow', 'SALP.view.TicketDetailsHeader', 'SALP.form.TicketDetailsAForm',
		'SALP.form.TicketDetailsBForm', 'SALP.grid.CustomerTicketsGrid', 'SALP.form.TicketDetailsCForm',
		'SALP.grid.DetailsAffectedGrid', 'SALP.view.TicketWindowOld', 'SALP.view.TicketWindowNew',
		'SALP.grid.WorkLogGrid', 'SALP.form.TicketDetailsDForm', 'SALP.chart.SITicketTimelineChart',
		'SALP.view.WorkLogDetailsWindow', 'SALP.grid.FavoriteLinksGrid', 'SALP.form.AddEditFavoriteLinkForm',
		'SALP.view.AddEditFavoriteLinkWindow', 'SALP.view.DeleteFavoriteLinkWindow', 'SALP.portlets.FavoriteLinksTreePortlet',
		'SALP.view.WorklogNotificationsWindow', 'SALP.portlets.ConflictBypassPortlet', 'SALP.grid.ConflictBypassGrid',
		'SALP.view.ManageUserLinksWindow','SALP.grid.IncidentReviewGrid', 'SALP.chart.CRANBackboneIPChart',
		'SALP.chart.CRANBackboneFacilityChart', 'SALP.chart.CRANBackboneFiberCutsChart', 'SALP.chart.ApplicationEnterpriseIPChart',
		'SALP.chart.ApplicationEnterpriseFacilitiesChart', 'SALP.chart.ApplicationEnterpriseFiberCutsChart',
		'SALP.chart.CRTicketStatusChart', 'SALP.chart.SubscriptionErrorStatusChart', 'SALP.chart.TempDBLogStatusChart',
		'SALP.chart.CRTicketsChart', 'SALP.chart.IVRCDRCallsChart', 'SALP.portlets.TicketDelayDisplayPortlet', 
		'SALP.grid.TicketDelayDisplayGrid', 'SALP.chart.ApplicationEventMonitor01Chart', 'SALP.chart.ApplicationEventMonitor02Chart',
		'SALP.chart.ApplicationEventMonitor03Chart', 'SALP.chart.ApplicationEventMonitor04Chart', 'SALP.chart.URLMonitor01Chart',
		'SALP.chart.URLMonitor02Chart', 'SALP.chart.URLMonitor03Chart', 'SALP.grid.FCCThresholdBreachedGrid',
		'SALP.grid.ServerProcessesBasicGrid', 'SALP.grid.ServerProcessesFullGrid', 'SALP.view.ServerCurrentProcessesWindow',
		'SALP.view.ServerProcessesDetailsWindow', 'SALP.form.ServerProcessesDetailsForm', 'SALP.view.KillProcessDialog',
		'SALP.portlets.CurrentServerProcessesPortlet', 'SALP.view.SelectServerDialog', 'SALP.grid.LongRunningJobsGrid',
		'SALP.grid.ServerManagerGrid', 'SALP.form.AddEditServersForm', 'SALP.view.AddEditServersWindow',
		'SALP.view.DeleteServerDialog', 'SALP.grid.ServerStatsGrid', 'SALP.chart.ServerCPUUsageChart',
		'SALP.view.ServerCPUUsageWindow', 'SALP.grid.MaintenanceResolveStatusGrid', 'SALP.grid.MaintenanceCausedSev1MonthOverMonthGrid',
		'SALP.view.AllMaintenanceSuccessFailureMonthOverMonthWindow', 'SALP.grid.AllMaintenanceSuccessFailureMonthOverMonthGrid',
		'SALP.view.Sev1CausedByMaintDetailsWindow', 'SALP.grid.Sev1CausedByMaintDetailsGrid', 'SALP.view.TicketLookupDialog',
		'SALP.view.MultiTicketWindow', 'SALP.form.ScheduledMaintenanceTicketForm', 'SALP.grid.SMTicketAffectedElementsGrid',
		'SALP.grid.SMTicketWorklogGrid', 'SALP.form.ServiceRequestTicketForm', 'SALP.grid.SRTicketAttributesGrid',
		'SALP.grid.SRTicketWorklogGrid', 'SALP.form.ChangeManagementTicketForm', 'SALP.grid.CMContactDetailsGrid',
		'SALP.grid.CMTicketAttributesGrid', 'SALP.grid.CMTicketStepsGrid', 'SALP.grid.CMTicketAffectedElementsGrid', 
		'SALP.grid.CMTicketWorklogGrid', 'SALP.form.CustomerReportedTicketForm', 'SALP.grid.CRTicketAttributesGrid',
		'SALP.grid.CRTicketWorklogGrid', 'SALP.form.ServiceIncidentTicketForm', 'SALP.grid.SITicketAttributesGrid', 
		'SALP.grid.SITicketAffectedElementsGrid', 'SALP.grid.SITicketCustomerTicketsGrid', 'SALP.grid.SITicketWorklogGrid', 
		'SALP.view.InvalidTicketPanel', 'SALP.form.OutageElementTicketForm', 'SALP.grid.OETicketAffectedElementsGrid',
		'SALP.grid.OETicketWorklogGrid', 'SALP.grid.OETicketAttributesGrid', 'SALP.form.OETicketAffectedElementsForm',
		'SALP.d3.OrganizationTreeMap', 'SALP.view.TreeMapWindow', 'SALP.view.CMDetailByHierarchyWindow', 
		'SALP.grid.CMDetailByHierarchyGrid', 'SALP.portlets.CMTreeViewChartPortlet', 'SALP.view.UserLookupWindow', 
		'SALP.view.UserLookupPanel', 'SALP.grid.UserLookupGrid', 'SALP.view.SubordinatesSearchWindow', 
		'SALP.grid.SubordinatesSearchGrid', 'SALP.grid.OpenActiveIncidentsGrid', 'SALP.view.OpenIncidentsFilterWindow',
		'SALP.view.HaystackFiltersWindow','SALP.portlets.HaystackBreakdownCallsPortlet', 'SALP.portlets.HaystackBreakdownTicketsPortlet',
		'SALP.portlets.HaystackBreakdownTruckRollsPortlet', 'SALP.chart.HaystackBreakdownCallsChart', 'SALP.chart.HaystackBreakdownTicketsChart',
		'SALP.chart.HaystackBreakdownTruckRollsChart', 'SALP.chart.SIEventFunnelChart', 'SALP.view.TestWindow'
  ],
	
	
	launch: function() 
	{
		Ext.apply(Ext.tip.QuickTipManager.getQuickTip(), 
  	{
    	hideDelay: 0,
    	dismissDelay: 0
		});
  	
		
		//Decode User
    SALP.user = Ext.decode(Ext.util.Cookies.get('SALP-User'));
		
		SALP.viewport = Ext.create('SALP.view.Viewport');
	

		Ext.getCmp('user-display').update('Welcome ' + SALP.user.displayName + '!');
		
		// Set Default Counts
		SALP.countPages = 1;
		SALP.countColumns = 4
		SALP.countPortlets = 0;
		
		// Load Dashboard Store
		SALP.data.Dashboards.store.load
		({
			params:
			{
				accountName: SALP.user.accountName
			}
		}); 
		
		var dashboardID = SALP.getURLParams();
		
		SALP.rawCookie = SALP.getCookie(dashboardID);
		
		SALP.data.FavoriteLinks.store.load
		({
			params:
			{
				accountName: SALP.user.accountName
			}
		});
		
		
	}
});


//document.getElementById('fileField-inputEl').addEventListener('change', onFileUpload, false);
//	
//onFileUpload= function()
//{
//	var t=1;
//}