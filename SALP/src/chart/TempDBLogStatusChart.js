﻿//
//
//  TempDB Log Status Chart
//
//


Ext.define('SALP.chart.TempDBLogStatusChart', 
{
	alias: 'widget.chart.TempDBLogStatusChart',
	extend: 'Ext.chart.CartesianChart',
	
	
	legend: 
	{
    docked: 'bottom'
  },
  
  
  store: Ext.create('Ext.data.Store', 
  {
  	autoLoad: true,
  	model: SALP.data.TempDBLogStatus.model,
  	proxy: 
  	{
    	type: 'ajax',
     	url: 'WebServices/Service.svc/GetTempDBLogStatus',
     	extraParams:
     	{
  	  	//lens: 'CRAN & Backbone',
  	  	//lensGroup: 'IP(Switching/Routing)'
     	},
     	pageParam: undefined,
  		startParam: undefined,
  		limitParam: undefined,
     	reader: 
     	{
      	type: 'json',
        root: 'data'
     	}
  	}
  }),
  
  axes: [
  {
    type: 'numeric',
    position: 'left',
    grid: true,
    fields: ['tempDBLogPct'],
    maximum: 100,
    minimum: 0,
    title: 
  	{
      text: 'Uptime %'
  	}
  }, 
  {
    type: 'category',
    position: 'bottom',
    grid: true,
    fields: 'date',
    renderer: function(chart, date, object) 
    {
    	var returnDate = Ext.util.Format.date(date, 'n/j/Y');
    
    	return returnDate;
    }
  },
  {
    type: 'numeric',
    position: 'right',
    grid: true,
    fields: ['tempDBLogStatus'],
    maximum: 100,
    minimum: 0,
    title: 
  	{
      text: 'Status Uptime %'
  	}
  }, ],

	series: [
  {
  	type: 'line',
    xField: 'date',
    yField: 'tempDBLogPct',
    title: 'tempDBLogPct',
    showMarkers: true,
    colors: ['red'],
    style: 
    {
      stroke: "red",
      miterLimit: 3,
      lineCap: 'miter',
      lineWidth: 2
    },
    highlight: 
    {
    	fillStyle: 'red',
      radius: 3,
      lineWidth: 2,
    	strokeStyle: '#fff'
    },
    marker: 
    {
    	radius: 1,
    },
    tips: 
    {
    	trackMouse: true,
      width: 150,
      height: 20,
      renderer: function(toolTip, item, object) 
      {
      	toolTip.setTitle(object.field + ': ' + item.data.tempDBLogPct);
      }
    }
  },
  {
  	type: 'line',
    xField: 'date',
    yField: 'tempDBLogStatus',
    title: 'tempDBLogStatus',
    showMarkers: true,
    colors: ['blue'],
    style: 
    {
      stroke: 'blue',
      miterLimit: 3,
      lineCap: 'miter',
      lineWidth: 2
    },
    highlight: 
    {
    	fillStyle: 'blue',
      radius: 3,
      lineWidth: 2,
    	strokeStyle: '#fff'
    },
    marker: 
    {
    	radius: 1,
    },
    tips: 
    {
    	trackMouse: true,
      width: 150,
      height: 20,
      renderer: function(toolTip, item, object) 
      {
      	toolTip.setTitle(object.field + ': ' + item.data.tempDBLogPct);
      }
    }
  }]
});