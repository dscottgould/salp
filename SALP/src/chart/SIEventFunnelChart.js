﻿//
//
//  SI Event Funnel Chart
//
//

Ext.define('SALP.chart.SIEventFunnelChart', 
{
	alias: 'widget.chart.SIEventFunnelChart',
	extend: 'Ext.chart.CartesianChart',
	
	legend: false,
	
  
	axes: [
	{
    type: 'numeric',
    position: 'left',
    grid: true,
    fields: ['agentOffered', 'deflected']
  }, 
 	{
    type: 'numeric',
    position: 'right',
    fields: ['fiveWeekBaseline'],
    maximum: 50
  }, 
  {
  	type: 'category',
    position: 'bottom',
    grid: true,
    fields: ['timeFrame'],
    renderer: function(chart, date, object) 
    {
    	var returnDate = Ext.util.Format.date(date, 'h:i');
    
    	return returnDate;
    },
    label: 
    {
    	rotate: 
    	{
      	degrees: -90
      }
    }
  }],
  
  series: [
  {
  	type: 'bar',
    title: [ 'Agent Offered', 'Deflected/Abandoned'],
    xField: 'timeFrame',
    yField: ['agentOffered', 'deflected'],
    stacked: true,
    tooltip: 
  	{
    	trackMouse: true,
      renderer: 'onBarTipRender'
    },
   	style:
		{
      //minGapWidth: 0,
      //lineWidth: 5,
      //maxBarWidth: 15,
      //opacity: 0.6
 		},
 		highlight: 
 		{
    	fillStyle: 'yellow'
    }
  },
  {
  	type: 'line',
    title: [ 'Five Week Baseline'],
    xField: 'timeFrame',
    yField: ['fiveWeekBaseline'],
    stacked: true,
    tooltip: 
  	{
    	trackMouse: true,
      renderer: 'onBaselineRender'
    },
    style: 
    {
    	stroke: '#000000',
      lineWidth: 2
    },
    marker: 
    {
    	radius: 4
    },
    highlight: 
    {
    	fillStyle: '#000',
      radius: 5,
      lineWidth: 2,
      strokeStyle: '#fff'
    }
  }],
  
		
//	store: Ext.create('Ext.data.Store', 
//  {
//  	autoLoad: false,
//  	model: 'SIEventFunnel',
//  	proxy: 
//  	{
//    	type: 'ajax',
//     	url: 'WebServices/Service.svc/GetSIEventTunnel',
//     	extraParams:
//     	{
//  	  	ticketId: 'SI020998948',
//     	},
//     	pageParam: undefined,
//  		startParam: undefined,
//  		limitParam: undefined,
//     	reader: 
//     	{
//      	type: 'json',
//        root: 'data'
//     	}
//  	}
//  }),
  
  
  initComponent: function()
  {
  	this.setStore(Ext.create('Ext.data.Store', 
    {
    	autoLoad: false,
    	model: 'SIEventFunnel',
    	proxy: 
    	{
      	type: 'ajax',
       	url: 'WebServices/Service.svc/GetSIEventTunnel',
//       	extraParams:
//       	{
//    	  	ticketId: 'SI020998948',
//       	},
       	pageParam: undefined,
    		startParam: undefined,
    		limitParam: undefined,
       	reader: 
       	{
        	type: 'json',
          root: 'data'
       	}
    	}
    }))

  	
		this.store.load
  	({
  		params:
  		{
  			ticketId: this.ticketId
  		},
  		callback : function(records, options, success) 
  		{
				if(records.length == 0)
				{
					this.getSurface('main').add
  				({
          	type: 'text',
            x: 100,
            y: 50,
            text: 'No Data Found',
            fontSize: 18,
            fillStyle: 'red' 
          });
				}
      },
      scope: this
  	});
  	
    this.callParent(arguments);
  },
  
  onBarTipRender: function (tooltip, record, item) 
  {
    var fieldIndex = Ext.Array.indexOf(item.series.getYField(), item.field),
        fieldName = item.series.getTitle()[fieldIndex],
        eventDate = Ext.util.Format.date(record.data.eventDate, 'n/j h:i');
        
    tooltip.setHtml('<b>' + fieldName + '</b>: ' + record.get(item.field) + '<br/>' + eventDate);
  },
  
  onBaselineRender: function (tooltip, record, item)
  {
  	tooltip.setHtml('<b>Five Week Baseline:</b> ' + record.data.fiveWeekBaseline)
  	
  	var t=1
  }
});