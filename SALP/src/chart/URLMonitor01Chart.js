﻿//
//
//  URL Monitor Chart
//
//


Ext.define('SALP.chart.URLMonitor01Chart', 
{
	alias: 'widget.chart.URLMonitor01Chart',
	extend: 'Ext.chart.CartesianChart',	
  
  
  store: Ext.create('Ext.data.Store', 
  {
  	autoLoad: true,
  	model: SALP.data.URLMonitor.model,
  	proxy: 
  	{
    	type: 'ajax',
     	url: 'WebServices/Service.svc/GetURLMonitor',
     	extraParams:
     	{
  	  	//lens: 'CRAN & Backbone',
  	  	//lensGroup: 'IP(Switching/Routing)'
     	},
     	pageParam: undefined,
  		startParam: undefined,
  		limitParam: undefined,
     	reader: 
     	{
      	type: 'json',
        root: 'data'
     	}
  	}
  }),
  
  axes: [
  {
    type: 'numeric',
    position: 'left',
    grid: true,
    fields: ['bnocTools'],
    //fields: ['bnocTools', 'etsTools', 'rcor'],
    maximum: 100,
    minimum: 50,
    title: 
  	{
      text: ''
  	}
  }, 
  {
    type: 'category',
    position: 'bottom',
    fields: 'date',
    renderer: function(chart, date, object) 
    {
    	var returnDate = Ext.util.Format.date(date, 'H:i');
    
    	return "";
    }
  }],

	series: [
  {
  	type: 'line',
    xField: 'date',
    yField: 'bnocTools',
    title: 'bnoctools',
    showMarkers: true,
    colors: ['blue'],
    style: 
    {
      stroke: "blue",
      miterLimit: 3,
      lineCap: 'miter',
      lineWidth: 5
    },
    highlight: 
    {
    	fillStyle: 'blue',
      radius: 3,
      lineWidth: 2,
    	strokeStyle: '#fff'
    },
    marker: 
    {
    	radius: 1,
    },
    tips: 
    {
    	trackMouse: true,
      width: 100,
      height: 20,
      renderer: function(toolTip, item, object) 
      {
      	toolTip.setTitle(object.field + ': ' + item.data.bnocTools);
      }
    }
  }],
  
  sprites: 
  {
    type: 'text',
    text: 'bnocTools',
    fontSize: 12,
    x: 50, 
    y: 30 
  },
  
  initComponent: function()
  {
		this.task = new Ext.util.DelayedTask(function()
		{
  		this.store.reload();
  	
  		Ext.defer(function() 
      {
      	this.setLoading(false);
      }, 2000, this);
  	
  		this.task.delay(900000);
  		
		}, this);
		

		this.task.delay(900000);
		
    this.callParent(arguments);
  }
});