﻿//
//
//  CR Ticket Status Chart
//
//


Ext.define('SALP.chart.CRTicketStatusChart', 
{
	alias: 'widget.chart.CRTicketStatusChart',
	extend: 'Ext.chart.CartesianChart',
	
	
	legend: 
	{
    docked: 'bottom'
  },
  
  
  store: Ext.create('Ext.data.Store', 
  {
  	autoLoad: true,
  	model: SALP.data.CRTicketStatus.model,
  	proxy: 
  	{
    	type: 'ajax',
     	url: 'WebServices/Service.svc/GetCRTicketStatus',
     	extraParams:
     	{
  	  	//lens: 'CRAN & Backbone',
  	  	//lensGroup: 'IP(Switching/Routing)'
     	},
     	pageParam: undefined,
  		startParam: undefined,
  		limitParam: undefined,
     	reader: 
     	{
      	type: 'json',
        root: 'data'
     	}
  	}
  }),
  
  axes: [
  {
    type: 'numeric',
    position: 'left',
    grid: true,
    fields: ['currentWeek', 'previousWeek'],
    title: 
  	{
      text: 'Ticket Count'
  	}
  }, 
  {
    type: 'category',
    position: 'bottom',
    grid: true,
    fields: 'date',
    label: 
    {
    	rotate: 
    	{
      	degrees: -45
      },
      font: '9px Helvetica, sans-serif'
    },
    renderer: function(chart, date, object) 
    {
    	var returnDate = Ext.util.Format.date(date, 'n/j/Y H:i');
    	
    	return returnDate
    }
  }],

	series: [
  {
  	type: 'line',
    xField: 'date',
    yField: 'currentWeek',
    title: 'Current Week',
    showMarkers: true,
    colors: ['red'],
    style: 
    {
      stroke: "red",
      miterLimit: 3,
      lineCap: 'miter',
      lineWidth: 2
    },
    highlight: 
    {
    	fillStyle: 'red',
      radius: 3,
      lineWidth: 2,
    	strokeStyle: '#fff'
    },
    marker: 
    {
    	radius: 1,
    },
    tips: 
    {
    	trackMouse: true,
      width: 150,
      height: 20,
      renderer: function(toolTip, item, object) 
      {
      	toolTip.setTitle(object.field + ': ' + item.data.currentWeek);
      }
    }
  },
  {
  	type: 'line',
    xField: 'date',
    yField: 'previousWeek',
    title: 'Previous Week',
    showMarkers: true,
    colors: ['blue'],
    style: 
    {
      stroke: "blue",
      miterLimit: 3,
      lineCap: 'miter',
      lineWidth: 2
    },
    highlight: 
    {
    	fillStyle: 'blue',
      radius: 3,
      lineWidth: 2,
    	strokeStyle: '#fff'
    },
    marker: 
    {
    	radius: 1,
    },
    tips: 
    {
    	trackMouse: true,
      width: 150,
      height: 20,
      renderer: function(toolTip, item, object) 
      {
      	toolTip.setTitle(object.field + ': ' + item.data.previousWeek);
      }
    }
  }]
});