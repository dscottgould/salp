﻿//
//
//  URL Monitor Chart 02
//
//


Ext.define('SALP.chart.URLMonitor02Chart', 
{
	alias: 'widget.chart.URLMonitor02Chart',
	extend: 'Ext.chart.CartesianChart',	
  
  
  store: Ext.create('Ext.data.Store', 
  {
  	autoLoad: true,
  	model: SALP.data.URLMonitor.model,
  	proxy: 
  	{
    	type: 'ajax',
     	url: 'WebServices/Service.svc/GetURLMonitor',
     	extraParams:
     	{
  	  	//lens: 'CRAN & Backbone',
  	  	//lensGroup: 'IP(Switching/Routing)'
     	},
     	pageParam: undefined,
  		startParam: undefined,
  		limitParam: undefined,
     	reader: 
     	{
      	type: 'json',
        root: 'data'
     	}
  	}
  }),
  
  axes: [
  {
    type: 'numeric',
    position: 'left',
    grid: true,
    fields: ['etsTools'],
    //fields: ['bnocTools', 'etsTools', 'rcor'],
    maximum: 100,
    minimum: 50,
    title: 
  	{
      text: ''
  	}
  }, 
  {
    type: 'category',
    position: 'bottom',
    fields: 'date',
    renderer: function(chart, date, object) 
    {
    	var returnDate = Ext.util.Format.date(date, 'H:i');
    
    	return "";
    }
  }],

	series: [
  {
  	type: 'line',
    xField: 'date',
    yField: 'etsTools',
    title: 'etsTools',
    showMarkers: true,
    colors: ['yellow'],
    style: 
    {
      stroke: "yellow",
      miterLimit: 3,
      lineCap: 'miter',
      lineWidth: 5
    },
    highlight: 
    {
    	fillStyle: 'yellow',
      radius: 3,
      lineWidth: 2,
    	strokeStyle: '#fff'
    },
    marker: 
    {
    	radius: 1,
    },
    tips: 
    {
    	trackMouse: true,
      width: 100,
      height: 20,
      renderer: function(toolTip, item, object) 
      {
      	toolTip.setTitle(object.field + ': ' + item.data.etsTools);
      }
    }
  }],
  
  sprites: 
  {
    type: 'text',
    text: 'etsTools',
    fontSize: 12,
    x: 50, 
    y: 30 
  },
  
  initComponent: function()
  {
		this.task = new Ext.util.DelayedTask(function()
		{
  		this.store.reload();
  	
  		Ext.defer(function() 
      {
      	this.setLoading(false);
      }, 2000, this);
  	
  		this.task.delay(900000);
  		
		}, this);
		

		this.task.delay(900000);
		
    this.callParent(arguments);
  }
});