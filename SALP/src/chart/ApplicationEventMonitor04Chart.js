﻿//
//
//  Application Event Monitor Chart 04
//
//


Ext.define('SALP.chart.ApplicationEventMonitor04Chart', 
{
	alias: 'widget.chart.ApplicationEventMonitor04Chart',
	extend: 'Ext.chart.CartesianChart',
	
  
  store: Ext.create('Ext.data.Store', 
  {
  	autoLoad: true,
  	model: SALP.data.ApplicationEventMonitor.model,
  	proxy: 
  	{
    	type: 'ajax',
     	url: 'WebServices/Service.svc/GetApplicationEventMonitor',
     	extraParams:
     	{
  	  	//lens: 'CRAN & Backbone',
  	  	//lensGroup: 'IP(Switching/Routing)'
     	},
     	pageParam: undefined,
  		startParam: undefined,
  		limitParam: undefined,
     	reader: 
     	{
      	type: 'json',
        root: 'data'
     	}
  	}
  }),
  
  axes: [
  {
    type: 'numeric',
    position: 'left',
    grid: true,
    fields: ['oBIWEB_WC_P01'],
    //fields: ['oBIDR_po_P01', 'oBIPROC_WC_P01', 'oBIPROC_WC_P01_APP', 'oBIWEB_WC_P01'],
    maximum: 100,
    minimum: 50,
    title: 
  	{
      text: ''
  	}
  }, 
  {
    type: 'category',
    position: 'bottom',
    grid: true,
    fields: 'date',
    label: 
    {
//    	rotate: 
//    	{
//      	degrees: -45
//      },
      font: '9px Helvetica, sans-serif'
    },
    renderer: function(chart, date, object) 
    {
    	var returnDate = Ext.util.Format.date(date, 'H:i');
    	
    	return returnDate
    }
  }],

	series: [
  {
  	type: 'line',
    xField: 'date',
    yField: ['oBIWEB_WC_P01'],
    title: ['OBIWEB_WC_P01'],
    showMarkers: true,
    colors: ['green'],
    style: 
    {
      stroke: "green",
      miterLimit: 3,
      lineCap: 'miter',
      lineWidth: 5
    },
    highlight: 
    {
    	fillStyle: 'green',
      radius: 3,
      lineWidth: 2,
    	strokeStyle: '#fff'
    },
    marker: 
    {
    	radius: 1,
    },
    tips: 
    {
    	trackMouse: true,
      width: 150,
      height: 20,
      renderer: function(toolTip, item, object) 
      {
      	toolTip.setTitle(object.field + ': ' + item.data.oBIDR_po_P01);
      }
    }
  }],
  
  sprites: 
  {
    type: 'text',
    text: 'OBIWEB_WC_P01',
    fontSize: 12,
    x: 50, 
    y: 30 
  },
  
  initComponent: function()
  {
		this.task = new Ext.util.DelayedTask(function()
		{
  		this.store.reload();
  	
  		Ext.defer(function() 
      {
      	this.setLoading(false);
      }, 2000, this);
  	
  		this.task.delay(900000);
  		
		}, this);
		

		this.task.delay(900000);
		
    this.callParent(arguments);
  }
});