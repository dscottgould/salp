﻿//
//
//  Haystack Breakdown Calls Chart
//
//

Ext.define('SALP.chart.HaystackBreakdownCallsChart', 
{
	alias: 'widget.chart.HaystackBreakdownCallsChart',
	extend: 'Ext.chart.CartesianChart',
	
	legend: false,
	
//	legend: 
//	{
//		//type: 'sprite',
//    docked: 'bottom',
//  },
  
  filters:
  {
  	filter: '',
  	segment: '',
  	showOthers: false
  },
  
//  store: new Ext.create('Ext.data.Store', 
//  {
//  	autoLoad: false,
//  	model: 'BreakdownCalls',
//  	proxy: 
//  	{
//    	type: 'ajax',
//     	url: 'WebServices/Service.svc/GetHaystackBinsByBreakdownCalls',
////     	extraParams:
////     	{
////  	  	lens: 'App & Enterprise',
////  	  	lensGroup: 'Facilities/Transport/Power'
////     	},
//     	pageParam: undefined,
//  		startParam: undefined,
//  		limitParam: undefined,
//     	reader: 
//     	{
//      	type: 'json',
//        root: 'data'
//     	}
//  	}
//  }),
  
  axes:[
  {
  	type: 'numeric',
    position: 'left',
    grid: true,
    //fields: ['data1'],
    //maximum: 10000
  },
  {
  	type: 'category',
    position: 'bottom',
    grid: true,
    fields: ['eventDate'],
    renderer: function(chart, date, object) 
    {
    	var returnDate = Ext.util.Format.date(date, 'h:i');
    
    	return returnDate;
    },
    label: 
    {
    	rotate: 
    	{
      	degrees: -90
      }
    },
   	renderer: function(chart, date, object) 
		{
			var oneDay = 24*60*60*1000,
			    firstDate = new Date(chart.config.chart.getStore().getAt(0).data.eventDate),
					secondDate = new Date(chart.config.chart.getStore().getAt(chart.config.chart.getStore().getTotalCount() - 1).data.eventDate),
					
					diffDays = Math.round(Math.abs((firstDate.getTime() - secondDate.getTime())/(oneDay))),
			    format = Ext.util.Format.date(date,'h:i');
			
			if(diffDays >= 2)
			{
				format = Ext.util.Format.date(date, 'n/j');
			}
			
			
			return format;
		}
  }],
  
  series:[
  {
  	type: 'bar',
  	//title: [ 'Central', 'Northeast', 'West', 'NullValue' ],
  	xField: 'eventDate',
  	//yField: [ 'data1', 'data2', 'data3', 'data4', 'data5' ],
  	stacked: true,
  	tooltip: 
  	{
    	trackMouse: true,
      renderer: 'onBarTipRender'
    },
   	style:
		{
      minGapWidth: 0,
      //lineWidth: 5,
      //maxBarWidth: 15,
      //opacity: 0.6
 		},
 		highlight: 
 		{
    	fillStyle: 'yellow'
    }
  }],
  
  initComponent: function()
  {
  	this.store = new Ext.create('Ext.data.Store', 
    {
    	autoLoad: false,
    	model: 'BreakdownCalls',
    	proxy: 
    	{
      	type: 'ajax',
       	url: 'WebServices/Service.svc/GetHaystackBinsByBreakdownCalls',
  //     	extraParams:
  //     	{
  //  	  	lens: 'App & Enterprise',
  //  	  	lensGroup: 'Facilities/Transport/Power'
  //     	},
       	pageParam: undefined,
    		startParam: undefined,
    		limitParam: undefined,
       	reader: 
       	{
        	type: 'json',
          root: 'data'
       	}
    	}
    });
  	
		this.task = new Ext.util.DelayedTask(function()
		{
  		this.setLoading('Loading...');
  		
  		this.store.reload();
  	
 			Ext.defer(function()       
 			{
      	this.setLoading(false);
      }, 2000, this);
      
      this.redraw();
            
      this.task.delay(900000);
  	
  		//this.task.delay(900000);
  		
		}, this);
	
		this.task.delay(900000);
		
		
    this.callParent(arguments);
  },
  
  onBarTipRender: function (tooltip, record, item) 
  {
    var fieldIndex = Ext.Array.indexOf(item.series.getYField(), item.field),
        fieldName = item.series.getTitle()[fieldIndex],
        eventDate = Ext.util.Format.date(record.data.eventDate, 'n/j h:i');
        
    tooltip.setHtml('<b>' + fieldName + '</b>: ' + record.get(item.field) + '<br/>' + eventDate);
  }
});