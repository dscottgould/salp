﻿//
//
//  SI Ticket Timeline
//
//


Ext.define('SALP.chart.SITicketTimelineChart', 
{
	alias: 'widget.chart.SITicketTimelineChart',
	extend: 'Ext.chart.CartesianChart',

  width: '100%',
  height: 500,
  store: 
  {
  	xtype:'store.store',
  	model: SALP.data.SITimeline.model,
  	proxy: 
  	{
    	type: 'ajax',
     	url: 'WebServices/Service.svc/GetSITicketTimeline',
     	pageParam: undefined,
  		startParam: undefined,
  		limitParam: undefined,
     	reader: 
     	{
      	type: 'json',
        root: 'data'
     	}
  	}
  },
  insetPadding: 40,
  flipXY: true,
  sprites: [
  {
      type: 'text',
      text: 'Incident Timeline in Duration Minutes',
      fontSize: 22,
      width: 100,
      height: 30,
      x: 20, 
      y: 28  
  }],
  axes: [
  {
      type: 'numeric',
      position: 'bottom',
      adjustByMajorUnit: true,
      fields: 'start',
      grid: true,
      renderer: SALP.onAxisLabelRender,
      minimum: 0
  }, 
  {
      type: 'category',
      position: 'left',
      fields: 'name',
      grid: true
  }],
  series: [
  {
    type: 'bar',
    axis: 'bottom',
    title: [ 'Prior', 'Duration Minutes', 'Invalid Duration Minutes' ],
    xField: 'name',
    yField: ['start', 'duration', 'durationNeg'],
    stacked: true,
    colors: ['', 'Orange', 'Red'],
    style: 
    {
    	opacity: 0.8,
 			stroke: 'white'
    },
    highlight: 
    {
        fillStyle: 'yellow'
    },
    tooltip: 
    {
        trackMouse: true,
        renderer: SALP.onSeriesTooltipRender
    }
  }],
  
  initComponent: function()
  {
		this.callParent(arguments);
		
		var t=1;
  }
});