﻿//
//
//  Subscription Error Status Chart
//
//


Ext.define('SALP.chart.SubscriptionErrorStatusChart', 
{
	alias: 'widget.chart.SubscriptionErrorStatusChart',
	extend: 'Ext.chart.CartesianChart',
	
	
	legend: 
	{
    docked: 'bottom'
  },
  
  
  store: Ext.create('Ext.data.Store', 
  {
  	autoLoad: true,
  	model: SALP.data.CRTicketStatus.model,
  	proxy: 
  	{
    	type: 'ajax',
     	url: 'WebServices/Service.svc/GetSubscriptionErrorStatus',
     	extraParams:
     	{
  	  	//lens: 'CRAN & Backbone',
  	  	//lensGroup: 'IP(Switching/Routing)'
     	},
     	pageParam: undefined,
  		startParam: undefined,
  		limitParam: undefined,
     	reader: 
     	{
      	type: 'json',
        root: 'data'
     	}
  	}
  }),
  
  axes: [
  {
    type: 'numeric',
    position: 'left',
    grid: true,
    fields: ['successRate'],
    minimum: 0,
    title: 
  	{
      text: 'Success Rate %'
  	}
  }, 
  {
    type: 'category',
    position: 'bottom',
    grid: true,
    fields: 'date',
    renderer: function(chart, date, object) 
    {
    	var returnDate = Ext.util.Format.date(date, 'n/j/Y');
    
    	return returnDate;
    }
  }],

	series: [
  {
  	type: 'line',
    xField: 'date',
    yField: 'successRate',
    title: 'Success Rate',
    showMarkers: true,
    colors: ['blue'],
    style: 
    {
      stroke: "blue",
      miterLimit: 3,
      lineCap: 'miter',
      lineWidth: 2
    },
    highlight: 
    {
    	fillStyle: 'blue',
      radius: 3,
      lineWidth: 2,
    	strokeStyle: '#fff'
    },
    marker: 
    {
    	radius: 1,
    },
    tips: 
    {
    	trackMouse: true,
      width: 150,
      height: 20,
      renderer: function(toolTip, item, object) 
      {
      	toolTip.setTitle(object.field + ': ' + item.data.currentWeek);
      }
    }
  }]
});