﻿//
//
//  IVR CDR Calls Chart
//
//


Ext.define('SALP.chart.IVRCDRCallsChart', 
{
	alias: 'widget.chart.IVRCDRCallsChart',
	extend: 'Ext.chart.CartesianChart',
	
	
	legend: 
	{
    docked: 'bottom'
  },
  
  
  store: Ext.create('Ext.data.Store', 
  {
  	//autoLoad: true,
  	model: SALP.data.CRTickets.model,
  	sorters: [
  	{
    	property: 'date',
      direction: 'ASC'
    }],
  	proxy: 
  	{
    	type: 'ajax',
     	url: 'WebServices/Service.svc/GetIVRCDRCalls',
     	timeout: 60000,
     	extraParams:
     	{
  	  	//lens: 'CRAN & Backbone',
  	  	//lensGroup: 'IP(Switching/Routing)'
     	},
     	pageParam: undefined,
  		startParam: undefined,
  		limitParam: undefined,
     	reader: 
     	{
      	type: 'json',
        root: 'data'
     	}
  	},
  	listeners: [
  	{
  		beforeload: function()
  		{
  			this.scope.setLoading('Loading...');
  		},
  		load: function()
  		{
  			this.scope.setLoading(false);
  		}
  	}]
  }),
  
  axes: [
  {
    type: 'numeric',
    position: 'left',
    grid: true,
    fields: ['callCount', 'baseline'],
   	minimum: 0,
//    title: 
//  	{
//      text: 'Restoration min'
//  	}
  }, 
  {
    type: 'category',
    position: 'bottom',
    fields: 'date',
    renderer: function(chart, date, object) 
    {
    	var returnDate = Ext.util.Format.date(date, 'g:i a');
    
    	return returnDate;
    }
  }],

	series: [
	{
  	type: 'bar',
  	xField: 'date',
    yField: ['callCount'],
    title: ['Call Count'],
    stacked: true,
    colors: ['#5177FF'],
//		label: 
//		{
//    	field: ['detect', 'engage', 'restore'],
//      display: 'insideEnd',
//      orientation: 'horizontal'
//    },
//    style: 
//    {
//    	minGapWidth: 20
//    },
    tips: 
    {
    	trackMouse: true,
      width: 100,
      height: 20,
      renderer: function(toolTip, item, object) 
      {
     		var currentType = object.field;
      	
      	toolTip.setTitle(currentType + ': ' + item.data.callCount);
      }
    }
  },
  {
  	type: 'line',
    xField: 'date',
    yField: 'baseline',
    title: '5-Week Baseline',
    showMarkers: true,
    colors: ['orange'],
    style: 
    {
      stroke: "orange",
      miterLimit: 3,
      lineCap: 'miter',
      lineWidth: 2
    },
    highlight: 
    {
    	fillStyle: 'orange',
      radius: 4,
      lineWidth: 2,
    	strokeStyle: 'blue'
    },
    marker: 
    {
    	radius: 2,
    },
//    label: 
//		{
//    	field: ['baseline'],
//      display: 'over',
//      orientation: 'horizontal'
//    },
    tips: 
    {
    	trackMouse: true,
      width: 100,
      height: 20,
      renderer: function(toolTip, item, object) 
      {
      	toolTip.setTitle(object.field + ': ' + item.data.baseline);
      }
    }
  }],
  
  initComponent: function()
  {
  	this.store.scope = this
  	
  	this.store.load();
  	
  	this.task = new Ext.util.DelayedTask(function()
		{
  		this.setLoading('Loading...');
  		
  		this.store.reload();
  	
  		Ext.defer(function() 
      {
      	this.setLoading(false);
      }, 2000, this);
  	
  		this.task.delay(900000);
  		
		}, this);
		

		this.task.delay(900000);
  	
  	this.callParent(arguments);
  }
});