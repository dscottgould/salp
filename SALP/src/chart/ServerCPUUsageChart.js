﻿//
//
//  Server CPU Usage Chart
//
//


Ext.define('SALP.chart.ServerCPUUsageChart', 
{
	alias: 'widget.chart.ServerCPUUsageChart',
	extend: 'Ext.chart.CartesianChart',
	
  axes: [
  {
    type: 'numeric',
    position: 'left',
    grid: true,
    fields: 'cpu',
    minimum: 0,
    maximum: 100,
    title: 
  	{
      text: 'CPU Usage'
  	}
  }, 
  {
    type: 'category',
    position: 'bottom',
    fields: 'recordTime',
    renderer: function(chart, date, object) 
    {
    	var returnDate = Ext.util.Format.date(date, 'g:i a');
    
    	return returnDate;
    }
  }],

	series: [
  {
  	type: 'line',
    xField: 'recordTime',
    yField: 'cpu',
    title: 'Frequency',
    showMarkers: true,
    colors: ['red'],
    style: 
    {
      stroke: "red",
      miterLimit: 3,
      lineCap: 'miter',
      lineWidth: 2
    },
    highlight: 
    {
    	fillStyle: '#FFC000',
      radius: 5,
      lineWidth: 2,
    	strokeStyle: '#fff'
    },
//    marker: 
//    {
//    	radius: 4,
//    },
//    label: 
//		{
//    	field: ['frequency'],
//      display: 'over',
//      orientation: 'horizontal'
//    },
    tips: 
    {
    	trackMouse: true,
      width: 60,
      height: 20,
      renderer: function(toolTip, item, object) 
      {
      	toolTip.setTitle(object.field + ': ' + item.data.cpu);
      }
    }
  }],
  
  
  initComponent: function()
  {
  	this.listeners = this.buildListeners();
  
		this.task = new Ext.util.DelayedTask(function()
		{
			this.setLoading(true);
			
  		this.store.reload();
  	
  		Ext.defer(function() 
      {
      	this.setLoading(false);
      }, 2000, this);
  	
  		this.task.delay(60000);
  		
		}, this);
		

		this.task.delay(60000);
		
    this.callParent(arguments);
  },
  
  
  buildListeners: function()
  {
  	return {
  		afterrender: function()
  		{
  			this.setStore(Ext.create('Ext.data.Store', 
        {
        	autoLoad: true,
        	model: SALP.data.Servers.CPUUsage.model,
        	proxy: 
        	{
          	type: 'ajax',
           	url: 'WebServices/Service.svc/GetServerCPUUsage',
           	extraParams:
           	{
      				serverName: this.server
           	},
           	pageParam: undefined,
        		startParam: undefined,
        		limitParam: undefined,
           	reader: 
           	{
            	type: 'json',
              root: 'data'
           	}
        	}
        }))
  		},
  		scope: this
  	}
  }
});