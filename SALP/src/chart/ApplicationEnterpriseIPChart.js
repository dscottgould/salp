﻿//
//
//  Application & Enterprise IP
//
//


Ext.define('SALP.chart.ApplicationEnterpriseIPChart', 
{
	alias: 'widget.chart.ApplicationEnterpriseIPChart',
	extend: 'Ext.chart.CartesianChart',
	
	
	legend: 
	{
    docked: 'bottom'
  },
  
  
  store: Ext.create('Ext.data.Store', 
  {
  	autoLoad: true,
  	model: SALP.data.WeeklyReviewMeasure.model,
  	proxy: 
  	{
    	type: 'ajax',
     	url: 'WebServices/Service.svc/GetWeeklyReviewMeasures',
     	extraParams:
     	{
  	  	lens: 'App & Enterprise',
  	  	lensGroup: 'IP(Switching/Routing)'
     	},
     	pageParam: undefined,
  		startParam: undefined,
  		limitParam: undefined,
     	reader: 
     	{
      	type: 'json',
        root: 'data'
     	}
  	}
  }),
  
  axes: [
  {
    type: 'numeric',
    position: 'left',
    grid: true,
    fields: ['detect', 'engage', 'restore'],
    minimum: 0,
    title: 
  	{
      text: 'Restoration min'
  	}
  }, 
  {
    type: 'category',
    position: 'bottom',
    fields: 'weekStarting',
    renderer: function(chart, date, object) 
    {
    	var returnDate = Ext.util.Format.date(date, 'n/j/Y');
    
    	return returnDate;
    }
  },
  {
    type: 'numeric',
    titleMargin: 20,
    position: 'right',
    fields: 'frequency',
    maximum: 10,
    minimum: 0,
    title: 
    {
        text: 'Incident Frequency'
    }
  }],

	series: [
	{
  	type: 'bar',
  	xField: 'weekStarting',
    yField: ['detect', 'engage', 'restore'],
    title: ['Detect', 'Engage', 'Restore'],
    stacked: true,
    colors: ['#569BDB', '#F67B2A', '#9D9D9D'],
    style: 
    {
    	minGapWidth: 20
    },
    label:
    {
    	field: ['restore'],
    	display: 'over',
    	orientation: 'horizontal',
    	renderer: function(text, sprite, config, store, index)
    	{
      	var data = store.store.getAt(index).data,
      			value = data.detect + data.engage + data.restore;
      			
      	
      	if (!this.displayCount)
      	{
      		this.displayCount = 1
      	}
      	else 
      	{
      		this.displayCount ++
      	}
      	
      	if(this.displayCount >= 13)
      	{
      		value = ""
      	}
      	
      	if(this.displayCount >= 16)
      	{
      		this.displayCount = 1
      	}
      			
      	return value.toString()
    	}
    },
    tips: 
    {
    	trackMouse: true,
      width: 100,
      height: 20,
      renderer: function(toolTip, item, object) 
      {
     		var currentType = object.field,
        		currentValue;
      	
      	switch(currentType)
      	{
      		case 'detect':
      		  currentValue = item.data.detect
      			break;
      			
      		case 'engage':
      		  currentValue = item.data.engage
      			break;
      			
      		case 'restore':
      		  currentValue = item.data.restore
      			break;

      	}
      	
      	toolTip.setTitle(currentType + ': ' + currentValue);
      }
    }
  },
  {
  	type: 'line',
    xField: 'weekStarting',
    yField: 'frequency',
    title: 'Frequency',
    showMarkers: true,
    colors: ['#FFC000'],
    style: 
    {
      stroke: "#FFE605",
      miterLimit: 3,
      lineCap: 'miter',
      lineWidth: 2
    },
    highlight: 
    {
    	fillStyle: '#FFC000',
      radius: 5,
      lineWidth: 2,
    	strokeStyle: '#fff'
    },
    marker: 
    {
    	radius: 4,
    },
    label: 
		{
    	field: ['frequency'],
      display: 'over',
      orientation: 'horizontal'
    },
    tips: 
    {
    	trackMouse: true,
      width: 100,
      height: 20,
      renderer: function(toolTip, item, object) 
      {
      	toolTip.setTitle(object.field + ': ' + item.data.frequency);
      }
    }
  }],
  
  initComponent: function()
  {
		this.task = new Ext.util.DelayedTask(function()
		{
  		this.store.reload();
  	
  		Ext.defer(function() 
      {
      	this.setLoading(false);
      }, 2000, this);
  	
  		this.task.delay(900000);
  		
		}, this);
		

		this.task.delay(900000);
		
    this.callParent(arguments);
  }
});