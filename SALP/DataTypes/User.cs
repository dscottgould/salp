﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SALP.DataTypes
{
    public class User
    {
        String _accountName;
        String _domainName;
        String _displayName;
        Boolean _hasFCCAccess = false;
        Boolean _hasBillingAccess = false;
        Boolean _hasBLRAccess = false;
        Boolean _hasMaintenanceAccess = false;
        Boolean _hasXNOCOBIAccess = false;

        public User()
        {
            // Base Constructor
        }

        public String accountName
        {
            get { return this._accountName; }
            set { this._accountName = value; }
        }

        public String domainName
        {
            get { return this._domainName; }
            set { this._domainName = value; }
        }


        public String displayName
        {
            get { return this._displayName; }
            set { this._displayName = value; }
        }

        public Boolean hasFCCAccess
        {
            get { return this._hasFCCAccess; }
            set { this._hasFCCAccess = value; }
        }

        public Boolean hasBillingAccess
        {
            get { return this._hasBillingAccess; }
            set { this._hasBillingAccess = value; }
        }

        public Boolean hasBLRAccess
        {
            get { return this._hasBLRAccess; }
            set { this._hasBLRAccess = value; }
        }

        public Boolean hasMaintenanceAccess
        {
            get { return this._hasMaintenanceAccess; }
            set { this._hasMaintenanceAccess = value; }
        }
        public Boolean hasXNOCOBIAccess
        {
            get { return this._hasXNOCOBIAccess; }
            set { this._hasXNOCOBIAccess = value; }
        }
    }
}